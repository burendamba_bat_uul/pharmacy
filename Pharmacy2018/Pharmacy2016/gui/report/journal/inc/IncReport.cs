﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;

namespace Pharmacy2016.gui.report.journal.inc
{
    /*
     * Орлогын журналын тайлан
     * 
     */ 

    public partial class IncReport : DevExpress.XtraReports.UI.XtraReport
    {
        
        #region Тайлан анх үүсгэх функц.

            private static IncReport INSTANCE = new IncReport();

            public static IncReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private IncReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = JournalDataSet.Instance;
                DataAdapter = JournalDataSet.IncJournalTableAdapter;
                DataMember = JournalDataSet.Instance.IncJournal.ToString();

                DetailReport.DataSource = JournalDataSet.Instance;
                DetailReport.DataAdapter = JournalDataSet.IncDetailTableAdapter;
                DetailReport.DataMember = "IncJournal.IncJournal_IncDetail";

                initBinding();
            }            

            private void initBinding()
            {
                this.xrTableCellID.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Id", "")});
                this.xrTableCellDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Date", ReportUtil.DisplayFormatDate)});
                this.xrTableCellDocNumber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DocNumber", "")});
                this.xrTableCellFullName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UserName", "")});
                this.xrTableCellWardName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".WarehouseName", "")});
                this.xrTableCellRoleName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".RoleName", "")});
                this.xrTableCellProducerName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SupplierName", "")});

                this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".MedicineName", "")});
                this.xrTableCellTypeName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".MedicineTypeName", "")});
                this.xrTableCellUnit.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".UnitType", "")});
                this.xrTableCellCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Price", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".SumPrice", ReportUtil.DisplayFormatFloat)});

                this.xrTableCellTotalCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellTotalSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
            }


        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(int type, string filter)
            {
                try
                {
                    if (!isInit)
                    {
                       initForm();                     
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                    if (type == 1)
                    {
                        xrLabelReportHeader.Text = "ОРЛОГЫН ЖУРНАЛЫН ГҮЙЛГЭЭ";
                    }
                    else                    
                    if (type == 2)
                    {
                        xrLabelReportHeader.Text = "ОРЛОГЫН ЖУРНАЛЫН ГҮЙЛГЭЭ";
                    }
                    else
                    {
                        xrLabelReportHeader.Text = "ОРЛОГЫН ЖУРНАЛЫН ТАЙЛАН";
                    }

                    FilterString = filter;
                    CreateDocument(true);

                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}
