﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Pharmacy2016.util;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.ds;
using System.Data.SqlClient;

namespace Pharmacy2016.report.journal.inc
{
    /*
     * Эмийн үлдэгдлийн тайлан
     * 
     */ 

    public partial class IncDocReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

            private static IncDocReport INSTANCE = new IncDocReport();

            public static IncDocReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private IncDocReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = JournalDataSet.Instance;
                DataAdapter = JournalDataSet.IncJournalTableAdapter;
                DataMember = JournalDataSet.Instance.IncJournal.ToString();

                DetailReport.DataSource = JournalDataSet.Instance;
                DetailReport.DataAdapter = JournalDataSet.IncDetailTableAdapter;
                DetailReport.DataMember = "IncJournal.IncJournal_IncDetail";
                
                initBinding();
            }

            private void initBinding()
            {
                this.xrLabelUserName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UserName", "")});
                this.xrLabelWarehouseName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".WarehouseName", "")});
                this.xrLabelDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Date", ReportUtil.DisplayFormatDate)});
                this.xrLabelProducerName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SupplierName", "")});
                this.xrLabelDocNum.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DocNumber", "")});

                this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".MedicineName", "")});                
                this.xrTableCellSerial.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Serial", "")});                
                this.xrTableCellUnitType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".UnitType", "")});
                this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Quantity", "")});
                this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Price", ReportUtil.DisplayFormatCurrency)});
                this.xrTableCellBal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});

              //  this.xrTableCellTotalPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Price", ReportUtil.DisplayFormatCurrency)});
              //  this.xrTableCellTotalBal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellTotalSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(string filter)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                    xrLabelPageHeader.Text = "Орлогын баримт № " + filter;
                    xrLabelCompany.Text = HospitalUtil.getHospitalName();

                    SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                    myConn.Open();
                    string query = "SELECT * FROM [ReportSetting]";
                    SqlCommand oCmd = new SqlCommand(query, myConn);
                    using (SqlDataReader oReader = oCmd.ExecuteReader())
                    {
                        while (oReader.Read())
                        {
                            if (oReader.HasRows)
                            {
                                if (oReader["code"].ToString() == "0")
                                {
                                    xrLabel10.Text = oReader["name"].ToString();
                                }
                                else if (oReader["code"].ToString() == "1" && !string.IsNullOrEmpty(oReader["employ"].ToString()))
                                {
                                    xrLabel6.Text = oReader["employ"].ToString();
                                    xrLabelStoreman.Text = "................................................../ " + oReader["name"].ToString();
                                }
                                else if (oReader["code"].ToString() == "2" && !string.IsNullOrEmpty(oReader["employ"].ToString()))
                                {
                                    xrLabel9.Text = oReader["employ"].ToString();
                                    xrLabelAccount.Text = "................................................../ " + oReader["name"].ToString();
                                }
                                else if (oReader["code"].ToString() == "3" && !string.IsNullOrEmpty(oReader["employ"].ToString()))
                                {
                                    xrLabel12.Text = oReader["employ"].ToString();
                                    xrLabel13.Text = "................................................../ " + oReader["name"].ToString();
                                }

                            }
                        }
                    }
                    myConn.Close();

                    FilterString = "id = '" + filter + "'";

                    CreateDocument(true);
                    
                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

            private void xrLabelUserName_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
            {
                string userName = xrLabelUserName.Text;
                string[] value = userName.Split(' ');
                //for (int i = 1; i < value.Length; i++)
                //{
                //    if (value[i].Length > 0)
                //    {
                //        xrLabelUserName.Text = value[i].Substring(0, 1) + "." + value[0];
                //        xrLabelStoreman.Text = "................................................../ " + value[i].Substring(0, 1) + "." + value[0] + " /";
                //        break;
                //    }
                //}

                string temp_date = xrLabelDate.Text;
                xrLabel5.Text = temp_date.Substring(0, 4) + " он " + temp_date.Substring(5, 2) + " сар " + temp_date.Substring(8, 2)+" өдөр";
            }

    }
}
