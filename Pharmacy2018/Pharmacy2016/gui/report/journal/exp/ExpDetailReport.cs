﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.report.ds;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.ds;

namespace Pharmacy2016.gui.report.journal.exp
{
    /*
     * Тасаг нэгжийн тайлан
     * 
     */ 

    public partial class ExpDetailReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

            private static ExpDetailReport INSTANCE = new ExpDetailReport();

            public static ExpDetailReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private ExpDetailReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = JournalDataSet.Instance;
                DataAdapter = JournalDataSet.ExpDetailTableAdapter;
                DataMember = JournalDataSet.Instance.ExpDetail.ToString();
                
                initBinding();
            }

            private void initBinding()
            {
                
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow()
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                    

                    CreateDocument(true);

                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    //Pharmacy2016.gui.core.journal.exp.MedicineExpForm.Instance.changeShowType();
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}
