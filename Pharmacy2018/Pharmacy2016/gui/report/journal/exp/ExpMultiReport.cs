﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;
using System.Collections.Generic;
using DevExpress.XtraReports.UI.PivotGrid;
using System.Data;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.gui.report.journal.exp
{
    /*
     * Зарлагын журналын тайлан
     * 
     */

    public partial class ExpMultiReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

        private static ExpMultiReport INSTANCE = new ExpMultiReport();

        public static ExpMultiReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        int maxHeight = 20;

        private ExpMultiReport()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;

            DataSource = JournalDataSet.Instance;
            DataAdapter = JournalDataSet.ExpDetailTableAdapter;
            DataMember = JournalDataSet.Instance.ExpDetail.ToString();
        }

        private void replaceNames()
        {
            for (int i = 0; i < JournalDataSet.Instance.ExpDetail.Rows.Count; i++)
            {
                DataRow drug = JournalDataSet.Instance.ExpDetail.Rows[i];
                if (!drug["userName"].ToString().Contains("Х.Ө:"))
                {
                    string[] userName = drug["userName"].ToString().Split(' ');
                    string user = "";
                    for (int a = 1; a < userName.Length; a++)
                    {
                        if (userName[a].Length > 0)
                        {
                            user = "Х.Ө: (" + userName[a].Substring(0, 1) + "." + userName[0] + ")";
                            break;
                        }
                    }
                    drug["userName"] = user;
                }
                if (!drug["expUserName"].ToString().Contains("Х.A:"))
                {
                    string[] userName = drug["expUserName"].ToString().Split(' ');
                    string user = "";
                    for (int a = 1; a < userName.Length; a++)
                    {
                        if (userName[a].Length > 0)
                        {
                            user = "Х.A: (" + userName[a].Substring(0, 1) + "." + userName[0] + ")";
                            break;
                        }
                    }
                    drug["expUserName"] = user;
                }
            }
        }

        #endregion

        #region Тайлан харуулах функц.

        public void initAndShow(List<string> columnVis, List<string> filter, string start, string end, int location)
        {
            try
            {
                if (!isInit)
                {
                    initForm();
                }

                ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);
                showOrHide(columnVis);
                xrPivotGrid1.Prefilter.CriteriaString = "(userID = '" + filter[0] + "' OR userID = '" + filter[1] + "') OR ( expUserID = '" + filter[1] + "' OR expUserID = '" + filter[0] + "')";
                xrLabelTreatmentDirector.Text = "Батлав: ЭЭО ....................................../ " + HospitalUtil.getHospitalTreatmentDirectorShortName() + " /";
                xrLabelTreatmentSign.Text = "Батлав: ЭЭО ....................................../ " + HospitalUtil.getHospitalTreatmentDirectorShortName() + " /";
                switch (location)
                {
                    case 0:
                        xrLabelTreatmentSign.Visible = false;
                        xrLabelTreatmentDirector.Visible = true;
                        xrLabelTreatmentDirector.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                        break;
                    case 1:
                        xrLabelTreatmentSign.Visible = false;
                        xrLabelTreatmentDirector.Visible = true;
                        xrLabelTreatmentDirector.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        break;
                    case 2:
                        xrLabelTreatmentSign.Visible = false;
                        xrLabelTreatmentDirector.Visible = true;
                        xrLabelTreatmentDirector.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
                        break;
                    case 3:
                        xrLabelTreatmentSign.Visible = true;
                        xrLabelTreatmentDirector.Visible = false;
                        xrLabelTreatmentSign.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                        break;
                    case 4:
                        xrLabelTreatmentSign.Visible = true;
                        xrLabelTreatmentDirector.Visible = false;
                        xrLabelTreatmentSign.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        break;
                    case 5:
                        xrLabelTreatmentSign.Visible = true;
                        xrLabelTreatmentDirector.Visible = false;
                        xrLabelTreatmentSign.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
                        break;
                    default:
                        break;
                }
                xrLabelCompany.Text = HospitalUtil.getHospitalName();
                xrLabelReportHeader.Text = "ЗАРЛАГЫН БАРИМТ";
                xrLabelStart.Text = start;
                xrLabelEnd.Text = end;
                //FilterString = "latinName = 'Malinda'";
                replaceNames();
                PrintingSystem.Document.AutoFitToPagesWidth = 1;
                xrLabelReportHeader.Width = xrPivotGrid1.ActualWidth;
                xrLabelTreatmentDirector.Width = xrLabelReportHeader.Width;
                xrLabelTreatmentSign.Width = xrLabelReportHeader.Width;
                xrLabelStart.Location = new Point((xrLabelReportHeader.Location.X + xrLabelReportHeader.Width) - xrLabelStart.Width, xrLabelStart.Location.Y);
                xrLabelEnd.Location = new Point((xrLabelReportHeader.Location.X + xrLabelReportHeader.Width) - xrLabelEnd.Width, xrLabelEnd.Location.Y);
                xrLabel2.Location = new Point(xrLabelStart.Location.X - xrLabel2.Width, xrLabel2.Location.Y);
                xrLabel4.Location = new Point(xrLabelEnd.Location.X - xrLabel4.Width, xrLabel4.Location.Y);
                CreateDocument(false);
                ProgramUtil.closeWaitDialog();
                ReportViewForm.Instance.initAndShow(INSTANCE);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                xrPivotGrid1.Prefilter.CriteriaString = "";
                ProgramUtil.closeWaitDialog();
            }
        }

        private void showOrHide(List<string> showAndHide)
        {
            showAndHide.Add("count");
            showAndHide.Add("price");
            showAndHide.Add("sumPrice");
            foreach (DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField field in xrPivotGrid1.Fields)
            {
                if (showAndHide.Contains(field.FieldName))
                    field.Visible = true;
                else
                    field.Visible = false;
            }
        }

        private void xrPivotGrid1_CustomRowHeight(object sender, DevExpress.XtraReports.UI.PivotGrid.PivotCustomRowHeightEventArgs e)
        {
            for (int i = 0; i < xrPivotGrid1.ColumnCount - 1; i++)
            {

                PivotCellBaseEventArgs CellInfo = xrPivotGrid1.GetCellInfo(i, e.RowIndex);
                foreach (DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField field in xrPivotGrid1.Fields)
                {
                    object some = CellInfo.GetFieldValue(field);
                    if (some != null)
                        maxHeight = getHeight(some.ToString());

                    if (maxHeight > e.RowHeight)
                        e.RowHeight = maxHeight;
                }
            }
        }

        private static int getHeight(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                int ir = str.Length / 14;
                if (ir > 0 && str.Length % 14 > 0)
                {
                    return 20 * (ir + 1);
                }
                else if (ir > 0 && str.Length % 14 == 0)
                {
                    return 20 * ir;
                }
                else
                {
                    return 20;
                }
                //return (int)(DevExpress.XtraPrinting.Native.Measurement.MeasureString(str, font, System.Drawing.GraphicsUnit.Pixel).Height) + 2;
            }
            else
            {
                return 0;
            }
        }

        #endregion
    }
}
