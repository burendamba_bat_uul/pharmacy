﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;
using System.Data;
using System.Data.SqlClient;

namespace Pharmacy2016.gui.report.journal.exp
{
    public partial class ExpDoctorReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static ExpDoctorReport INSTANCE = new ExpDoctorReport();

        public static ExpDoctorReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        public ExpDoctorReport()
        {
        }

 

        #region Тайлан харуулах функц.

        public void initAndShow(string id, string start, string end)
        {
            try
            {
                InitializeComponent();
                DataSet ds = new DataSet();
                SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                myConn.Open();
                string query = "Select [m].[name] as name, [m].[validDate] as valdate, [s].[name] as shape, " +
                               "[u].[name] as unit, [q].[name] as quantity, [e].[count] as counts, [e].[price] as price, [e].[count]*[e].[price] as sumprice " +
                               "from [ExpDetail] as [e] " +
                                "inner join " +
                                 "[ExpJournal] as [ex] on [ex].[id] = [e].[expJournalID] " +
                                 "left join " +
                                 "[Medicine] as [m] on [e].[medicineID] = [m].[id] " +
                                 "left join " +
                                 "[MedicineShape] as [s] on [s].[id] = [m].[shapeID] " +
                                 "left join " +
                                 "[MedicineUnit] as [u] on [u].[id] =[m].[unitID] " +
                                 "left join " +
                                 "[MedicineQuantity] as [q] on [q].[id] = [m].[quantityID] " +
                                "where [e].[hospitalID] = '" + HospitalUtil.getHospitalId() + "' and [ex].[date] between '" + start + "' and '" + end + "' " +
                                "and [ex].[doctorID] = '" + id + "' ";
                SqlCommand objComman = new SqlCommand(query, myConn);
                SqlDataAdapter da = new SqlDataAdapter(objComman);
                da.Fill(ds);

                this.DataSource = ds;
                this.xrTableCellMedicineNameD.DataBindings.Add("Text", ds, "name");
                this.xrTableCellDateD.DataBindings.Add("Text", ds, "valdate");
                this.xrTableCellShapeNameD.DataBindings.Add("Text", ds, "shape");
                this.xrTableCellQuantityD.DataBindings.Add("Text", ds, "unit");
                this.xrTableCellUnitD.DataBindings.Add("Text", ds, "quantity");
                this.xrTableCellCountD.DataBindings.Add("Text", ds, "counts");
                this.xrTableCellPriceD.DataBindings.Add("Text", ds, "price");
                this.xrTableCellSumPriceD.DataBindings.Add("Text", ds, "sumprice");

                ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);
                xrDateStart.Text = start;
                xrDateEnd.Text = end;
                string qry = "SELECT LEFT(lastname, 1) + '. ' + firstname as name " +
                             "FROM [Users] where id = '"+ id +"' ";
                SqlCommand oCmd = new SqlCommand(qry, myConn);
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        if (oReader.HasRows)
                        {
                            xrDoctor.Text = oReader["name"].ToString();
                        }
                    }
                }


                CreateDocument(true);
                myConn.Close();
                ProgramUtil.closeWaitDialog();
                ReportViewForm.Instance.initAndShow(INSTANCE);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Зарлагын гүйлгээ хэвлэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        #endregion
    }


}
