﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;

namespace Pharmacy2016.gui.report.journal.exp
{
    public partial class ExpBillingReport : DevExpress.XtraReports.UI.XtraReport
    {
        #region Нэхэмжлэх хэвлэх анх үүсгэх функц.

        private static ExpBillingReport INSTANCE = new ExpBillingReport();

        string userN;

        public static ExpBillingReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

       private ExpBillingReport()
       {
                
       }

       private void initForm()
       {
          InitializeComponent();
          this.isInit = true;

          DataSource = JournalDataSet.Instance;
          DataAdapter = JournalDataSet.ExpJournalTableAdapter;
          DataMember = JournalDataSet.Instance.ExpJournal.ToString();

          DetailReport.DataSource = JournalDataSet.Instance;
          DetailReport.DataAdapter = JournalDataSet.ExpDetailTableAdapter;
          DetailReport.DataMember = "ExpJournal.ExpJournal_ExpDetail";
           
           initBinding();
       }

       private void initBinding()
       {

           this.xrLabelDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
              new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Date", ReportUtil.DisplayFormatDate)});
           this.xrLabelExpWarehouse.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ExpWarehouseName", "")});              
          this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
              new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".MedicineName", "")});          
          this.xrTableCellCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
              new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
          this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
              new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Price", ReportUtil.DisplayFormatFloat)});
          this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
              new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".SumPrice", ReportUtil.DisplayFormatFloat)});

          this.xrLabelSUM.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
              new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
      
     }
        
        
        #endregion


       #region Нэхэмжлэх хэвлэх харуулах функц.

       public void initAndShow(int type, string userName,string filter,string bill_id,string accountant_name,string money_letter,string paydate)
       {
           try
           {
               if (!isInit)
               {
                   initForm();
               }

               xrLabelCompany.Text = HospitalUtil.getHospitalName();
               xrLabelCompanyAddress.Text = HospitalUtil.getHospitalAddress();
               xrLabelHospitalRegister.Text = HospitalUtil.getHospitalRegister();
               xrLabelBankName.Text = HospitalUtil.getHospitalBank();
               xrLabelBankAccount.Text = HospitalUtil.getHospitalAccount();
               xrLabelBillingID.Text = bill_id;
               if (HospitalUtil.getHospitalPhone() == "")
               {
                   xrLabelPhone.Text = HospitalUtil.getHospitalFax();
               }
               else
               {
                   xrLabelPhone.Text = HospitalUtil.getHospitalPhone() + " ; " + HospitalUtil.getHospitalFax();
               }
               xrLabelEmail.Text = HospitalUtil.getHospitalEmail();
               if (HospitalUtil.getHospitalDirectorShortName() == "")
               {
                   xrLabelDirector.Text = "/                                       /";
               }
               else
               {                   
                   xrLabelDirector.Text = "/ "+HospitalUtil.getHospitalDirectorShortName() + " /";
               }


               if (accountant_name == "")
               {
                   xrLabelAccountant.Text = "/                                       /";
               }
               else
               {
                   xrLabelAccountant.Text = "/ " + accountant_name + " /";
               }

               string[] value = money_letter.Split(' ');
               string temp = "";
               string row1 = "";
               string row2 = "";
               for (int i = 0; i < value.Length; i++)
               {
                   temp = temp + value[i]+' ';
                   if (temp.Length < 70)
                   {
                       row1 = row1 + value[i]+' ';
                   }
                   else
                   {
                       row2 = row2 + value[i]+' ';
                   }                   
               }
               xrLabelMoneyLetter1.Text = row1;
               xrLabelMoneyLetter2.Text = row2;
               xrLabelPayDate.Text = paydate;
               ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

               FilterString = filter;
               CreateDocument(true);

               ProgramUtil.closeWaitDialog();
               ReportViewForm.Instance.initAndShow(INSTANCE);
           }
           catch (Exception ex)
           {
               XtraMessageBox.Show("Нэхэмжлэх хэвлэхэд алдаа гарлаа " + ex.Message);
           }
           finally
           {
               ProgramUtil.closeWaitDialog();
           }
       }

       #endregion
    }
}
