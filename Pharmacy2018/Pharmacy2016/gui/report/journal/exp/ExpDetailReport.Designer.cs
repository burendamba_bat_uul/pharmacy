﻿namespace Pharmacy2016.gui.report.journal.exp
{
    partial class ExpDetailReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPivotGrid1 = new DevExpress.XtraReports.UI.XRPivotGrid();
            this.pivotGridFieldMedicineName = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldUnit = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldPrice = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldPatientName = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldCount = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldSumPrice = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.pivotGridField1 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridField2 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPivotGrid1
            // 
            this.xrPivotGrid1.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrPivotGrid1.Dpi = 254F;
            this.xrPivotGrid1.Fields.AddRange(new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField[] {
            this.pivotGridFieldMedicineName,
            this.pivotGridFieldUnit,
            this.pivotGridFieldPrice,
            this.pivotGridFieldPatientName,
            this.pivotGridFieldCount,
            this.pivotGridFieldSumPrice,
            this.pivotGridField1,
            this.pivotGridField2});
            this.xrPivotGrid1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 201.0833F);
            this.xrPivotGrid1.Name = "xrPivotGrid1";
            this.xrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3;
            this.xrPivotGrid1.OptionsView.ShowDataHeaders = false;
            this.xrPivotGrid1.SizeF = new System.Drawing.SizeF(2870F, 378.3542F);
            // 
            // pivotGridFieldMedicineName
            // 
            this.pivotGridFieldMedicineName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldMedicineName.AreaIndex = 0;
            this.pivotGridFieldMedicineName.Caption = "Эмийн нэр";
            this.pivotGridFieldMedicineName.FieldName = "medicineName";
            this.pivotGridFieldMedicineName.Name = "pivotGridFieldMedicineName";
            this.pivotGridFieldMedicineName.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            // 
            // pivotGridFieldUnit
            // 
            this.pivotGridFieldUnit.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldUnit.AreaIndex = 1;
            this.pivotGridFieldUnit.Caption = "Тун хэмжээ";
            this.pivotGridFieldUnit.FieldName = "unit";
            this.pivotGridFieldUnit.Name = "pivotGridFieldUnit";
            this.pivotGridFieldUnit.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            // 
            // pivotGridFieldPrice
            // 
            this.pivotGridFieldPrice.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldPrice.AreaIndex = 2;
            this.pivotGridFieldPrice.Caption = "Үнэ";
            this.pivotGridFieldPrice.FieldName = "price";
            this.pivotGridFieldPrice.Name = "pivotGridFieldPrice";
            this.pivotGridFieldPrice.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            // 
            // pivotGridFieldPatientName
            // 
            this.pivotGridFieldPatientName.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldPatientName.AreaIndex = 2;
            this.pivotGridFieldPatientName.Caption = "Эмч, Тасаг";
            this.pivotGridFieldPatientName.FieldName = "expTypeName";
            this.pivotGridFieldPatientName.Name = "pivotGridFieldPatientName";
            // 
            // pivotGridFieldCount
            // 
            this.pivotGridFieldCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldCount.AreaIndex = 0;
            this.pivotGridFieldCount.Caption = "Тоо хэмжээ";
            this.pivotGridFieldCount.CellFormat.FormatString = "n0";
            this.pivotGridFieldCount.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.FieldName = "count";
            this.pivotGridFieldCount.GrandTotalCellFormat.FormatString = "n0";
            this.pivotGridFieldCount.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Name = "pivotGridFieldCount";
            this.pivotGridFieldCount.TotalCellFormat.FormatString = "n0";
            this.pivotGridFieldCount.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.TotalValueFormat.FormatString = "n0";
            this.pivotGridFieldCount.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.ValueFormat.FormatString = "n0";
            this.pivotGridFieldCount.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Width = 80;
            // 
            // pivotGridFieldSumPrice
            // 
            this.pivotGridFieldSumPrice.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldSumPrice.AreaIndex = 1;
            this.pivotGridFieldSumPrice.Caption = "Нийт дүн";
            this.pivotGridFieldSumPrice.CellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.FieldName = "sumPrice";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.Name = "pivotGridFieldSumPrice";
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.ValueFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 99F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 99F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrPivotGrid1});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 579.4375F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabel1
            // 
            this.xrLabel1.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 58.10252F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(2870F, 58.42F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "ЗАРЛАГЫН ЖУРНАЛЫН ДЭЛГЭРЭНГҮЙ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField1.AreaIndex = 0;
            this.pivotGridField1.Caption = "Сар";
            this.pivotGridField1.FieldName = "date";
            this.pivotGridField1.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.pivotGridField1.Name = "pivotGridField1";
            this.pivotGridField1.UnboundFieldName = "pivotGridField1";
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField2.AreaIndex = 1;
            this.pivotGridField2.Caption = "Өдөр";
            this.pivotGridField2.FieldName = "date";
            this.pivotGridField2.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateDay;
            this.pivotGridField2.Name = "pivotGridField2";
            this.pivotGridField2.UnboundFieldName = "pivotGridField2";
            // 
            // ExpDetailReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader});
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(50, 50, 99, 99);
            this.PageHeight = 2100;
            this.PageWidth = 2970;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "15.1";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPivotGrid xrPivotGrid1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldMedicineName;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldUnit;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldPrice;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldPatientName;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldCount;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldSumPrice;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridField1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridField2;
    }
}
