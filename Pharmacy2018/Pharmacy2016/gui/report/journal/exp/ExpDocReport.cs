﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Pharmacy2016.util;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.ds;
using System.Data.SqlClient;

namespace Pharmacy2016.report.journal.exp
{
    /*
     * Эмийн үлдэгдлийн тайлан
     * 
     */ 

    public partial class ExpDocReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

            private static ExpDocReport INSTANCE = new ExpDocReport();

            public static ExpDocReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private ExpDocReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = JournalDataSet.Instance;
                DataAdapter = JournalDataSet.ExpJournalTableAdapter;
                DataMember = JournalDataSet.Instance.ExpJournal.ToString();

                DetailReport.DataSource = JournalDataSet.Instance;
                DetailReport.DataAdapter = JournalDataSet.ExpDetailTableAdapter;
                DetailReport.DataMember = "ExpJournal.ExpJournal_ExpDetail";
                
                initBinding();
            }

            private void initBinding()
            {
                this.xrLabelWarehouseName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".WarehouseName", "")});
                this.xrLabelDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Date", ReportUtil.DisplayFormatDate)});
                this.xrLabelExpWarehouse.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ExpWarehouseName", "")});
                this.xrLabelDocNum.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DocNumber", "")});

                this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".MedicineName", "")});
                //this.xrTableCellMedicineTypeName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                  //  new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".MedicineTypeName", "")});
                this.xrTableCellSerial.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Serial", "")});
                //this.xrTableCellValidDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                 //   new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".ValidDate", ReportUtil.DisplayFormatDate)});
                this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".UnitType", "")});
                this.xrTableCellQuantity.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Quantity", "")});
                this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Price", ReportUtil.DisplayFormatCurrency)});
                this.xrTableCellBal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});

              //  this.xrTableCellTotalPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
              //      new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Price", ReportUtil.DisplayFormatCurrency)});
            //    this.xrTableCellTotalBal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
             //       new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellTotalSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(string filter, string userName, string expUserName)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                    xrLabelPageHeader.Text = "Зарлагын баримт № " + filter;
                    xrLabelCompany.Text = HospitalUtil.getHospitalName();

                    SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                    myConn.Open();
                    string query = "SELECT * FROM [ReportSetting]";
                    SqlCommand oCmd = new SqlCommand(query, myConn);
                    using (SqlDataReader oReader = oCmd.ExecuteReader())
                    {
                        while (oReader.Read())
                        {
                            if (oReader.HasRows)
                            {
                                if (oReader["code"].ToString() == "0") {
                                    xrLabel2.Text = oReader["name"].ToString();
                                }
                                else if (oReader["code"].ToString() == "1") {
                                    xrLabel9.Text = oReader["employ"].ToString();
                                    xrLabelUserName.Text = "................................................../ " + oReader["name"].ToString();
                                }
                                else if (oReader["code"].ToString() == "2")
                                {
                                    xrLabel6.Text = oReader["employ"].ToString();
                                    xrLabelExpUserName.Text = "................................................../ " + oReader["name"].ToString();
                                }
                                else {
                                    xrLabel10.Text = oReader["employ"].ToString();
                                    xrLabelAccountant.Text = "................................................../ " + oReader["name"].ToString();
                                }
 
                            }
                        }
                    }
                    myConn.Close();

                    //string[] value = userName.Split(' ');
                    //for (int i = 1; i < value.Length; i++)
                    //{
                    //    if (value[i].Length > 0)
                    //    {
                    //        xrLabelUserName.Text = "................................................../ " + value[i].Substring(0, 1) + "." + value[0] + " /";
                    //        break;
                    //    }
                    //}
                    //value = expUserName.Split(' ');
                    //for (int i = 1; i < value.Length; i++)
                    //{
                    //    if (value[i].Length > 0)
                    //    {
                    //        xrLabelExpUserName.Text = "................................................../ " + value[i].Substring(0, 1) + "." + value[0] + " /";
                    //        break;
                    //    }
                    //}
                    
                    //if(HospitalUtil.getHospitalAccountantName().Length > 0)
                    //{
                    //    xrLabelAccountant.Text = "................................................../ " + HospitalUtil.getHospitalAccountantName() + " /";
                    //}                    

                    FilterString = "id = '" + filter + "'";

                    CreateDocument(true);
                    
                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

            private void xrLabelDate_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
            {
                string temp_date = xrLabelDate.Text;
                xrLabelDate.Text = temp_date.Substring(0, 4) + " он " + temp_date.Substring(5, 2) + " сар " + temp_date.Substring(8, 2) + " өдөр";
            }

    }
}
