﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Pharmacy2016.util;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.ds;
using System.Collections.Generic;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.report.journal.exp
{
    /*
     * Эмийн үлдэгдлийн тайлан
     * 
     */

    public partial class ExpDetialReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

        private static ExpDetialReport INSTANCE = new ExpDetialReport();

        public static ExpDetialReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private ExpDetialReport()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;

            DataSource = JournalDataSet.Instance;
            DataAdapter = JournalDataSet.ExpJournalTableAdapter;
            DataMember = JournalDataSet.Instance.ExpJournal.ToString();

            DetailReport.DataSource = JournalDataSet.Instance;
            DetailReport.DataAdapter = JournalDataSet.ExpDetailTableAdapter;
            DetailReport.DataMember = "ExpJournal.ExpJournal_ExpDetail";
            initBinding();
        }

        private void initBinding()
        {
            this.xrLabelUserName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UserName", "")});
            this.xrLabelWareHouse.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".WareHouseName", ReportUtil.DisplayFormatDate)});
            this.xrLabelDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Date", ReportUtil.DisplayFormatDate)});
            this.xrLabelExpUser.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ExpUserName", "")});
            this.xrLabelDocNum.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DocNumber", "")});
            this.xrTableCellSerial.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Serial", "")});
            this.xrTableCellDetialDesc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".DetialDesc", "")});
            this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".MedicineName", "")});
            this.xrTableCellLatinName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".LatinName", "")});
            this.xrTableCellMedicineType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".MedicineTypeName", "")});
            this.xrTableCellShape.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Shape", "")});
            this.xrTableCellValidDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".ValidDate", ReportUtil.DisplayFormatDate)});
            this.xrTableCellUnitType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".UnitType", "")});
            this.xrTableCellQuantity.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Quantity", "")});
            this.xrTableCellCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Price", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
            this.xrTableCellTotalCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellTotalPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Price", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellDesc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Description", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellTotalSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
        }

        #endregion

        #region Тайлан харуулах функц.

        public void initAndShow(List<string> columns, DataRow filter)
        {
            try
            {
                if (!isInit)
                {
                    initForm();
                }

                ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);
                
                hideColumns(columns);
                xrLabelHeaderNo.Text = "№ " + filter["id"];
                xrLabelCompany.Text = HospitalUtil.getHospitalName();
                string[] value = filter["userName"].ToString().Split(' ');
                for (int i = 1; i < value.Length; i++)
                {
                    if (value[i].Length > 0)
                    {
                        xrLabelAccount.Text = "................................................../ " + value[i].Substring(0, 1) + "." + value[0] + " /";
                        break;
                    }
                }
                value = filter["expUserName"].ToString().Split(' ');
                for (int i = 1; i < value.Length; i++)
                {
                    if (value[i].Length > 0)
                    {
                        xrLabelExpUserName.Text = "................................................../ " + value[i].Substring(0, 1) + "." + value[0] + " /";
                        break;
                    }
                }
                FilterString = "id = '" + filter["id"] + "'";
                CreateDocument(true);
                ProgramUtil.closeWaitDialog();
                ReportViewForm.Instance.initAndShow(INSTANCE);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void hideColumns(List<string> visibleCol)
        {           
            xrTable2.SuspendLayout();
            xrTableDetail.SuspendLayout();
            xrTable1.SuspendLayout();

            if (xrTableRow1.Cells.Contains(xrTableCellCount))
                xrTableRow1.Cells.Remove(xrTableCellCount);
            if (xrTableRow1.Cells.Contains(xrTableCellPrice))
                xrTableRow1.Cells.Remove(xrTableCellPrice);
            if (xrTableRow1.Cells.Contains(xrTableCellSumPrice))
                xrTableRow1.Cells.Remove(xrTableCellSumPrice);
            if (xrTableRow4.Cells.Contains(xrTableCellcn))
                xrTableRow4.Cells.Remove(xrTableCellcn);
            if (xrTableRow4.Cells.Contains(xrTableCellconc))
                xrTableRow4.Cells.Remove(xrTableCellconc);
            if (xrTableRow4.Cells.Contains(xrTableCellsPrice))
                xrTableRow4.Cells.Remove(xrTableCellsPrice);
            if (visibleCol.Contains("shape"))
            {                 
                if (!xrTableRow4.Cells.Contains(xrTableCellShapes))
                    xrTableRow4.Cells.Add(xrTableCellShapes);
                if (!xrTableRow1.Cells.Contains(xrTableCellShape))
                    xrTableRow1.Cells.Add(xrTableCellShape);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellShapes))
                    xrTableRow4.Cells.Remove(xrTableCellShapes);
                if (xrTableRow1.Cells.Contains(xrTableCellShape))
                    xrTableRow1.Cells.Remove(xrTableCellShape);
            }
            if (visibleCol.Contains("validDate"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellDate))
                    xrTableRow4.Cells.Add(xrTableCellDate);
                if (!xrTableRow1.Cells.Contains(xrTableCellValidDate))
                    xrTableRow1.Cells.Add(xrTableCellValidDate);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellDate))
                    xrTableRow4.Cells.Remove(xrTableCellDate);
                if (xrTableRow1.Cells.Contains(xrTableCellValidDate))
                    xrTableRow1.Cells.Remove(xrTableCellValidDate);
            }
            if (visibleCol.Contains("quantity"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellQuant))
                    xrTableRow4.Cells.Add(xrTableCellQuant);
                if (!xrTableRow1.Cells.Contains(xrTableCellQuantity))
                    xrTableRow1.Cells.Add(xrTableCellQuantity);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellQuant))
                    xrTableRow4.Cells.Remove(xrTableCellQuant);
                if (xrTableRow1.Cells.Contains(xrTableCellQuantity))
                    xrTableRow1.Cells.Remove(xrTableCellQuantity);
            }
            if (visibleCol.Contains("latinName"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCelllat))
                    xrTableRow4.Cells.Add(xrTableCelllat);
                if (!xrTableRow1.Cells.Contains(xrTableCellLatinName))
                    xrTableRow1.Cells.Add(xrTableCellLatinName);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCelllat))
                    xrTableRow4.Cells.Remove(xrTableCelllat);
                if (xrTableRow1.Cells.Contains(xrTableCellLatinName))
                    xrTableRow1.Cells.Remove(xrTableCellLatinName);
            }
            if (visibleCol.Contains("unitType"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellUni))
                    xrTableRow4.Cells.Add(xrTableCellUni);
                if (!xrTableRow1.Cells.Contains(xrTableCellUnitType))
                    xrTableRow1.Cells.Add(xrTableCellUnitType);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellUni))
                    xrTableRow4.Cells.Remove(xrTableCellUni);
                if (xrTableRow1.Cells.Contains(xrTableCellUnitType))
                    xrTableRow1.Cells.Remove(xrTableCellUnitType);
            }
            if (visibleCol.Contains("medicineTypeName"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellMtype))
                    xrTableRow4.Cells.Add(xrTableCellMtype);
                if (!xrTableRow1.Cells.Contains(xrTableCellMedicineType))
                    xrTableRow1.Cells.Add(xrTableCellMedicineType);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellMtype))
                    xrTableRow4.Cells.Remove(xrTableCellMtype);
                if (xrTableRow1.Cells.Contains(xrTableCellMedicineType))
                    xrTableRow1.Cells.Remove(xrTableCellMedicineType);
            }
            if (visibleCol.Contains("medicineName"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellMedName))
                    xrTableRow4.Cells.Add(xrTableCellMedName);
                if (!xrTableRow1.Cells.Contains(xrTableCellMedicineName))
                    xrTableRow1.Cells.Add(xrTableCellMedicineName);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellMedName))
                    xrTableRow4.Cells.Remove(xrTableCellMedName);
                if (xrTableRow1.Cells.Contains(xrTableCellMedicineName))
                    xrTableRow1.Cells.Remove(xrTableCellMedicineName);
            }
            if (visibleCol.Contains("serial"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellser))
                    xrTableRow4.Cells.Add(xrTableCellser);
                if (!xrTableRow1.Cells.Contains(xrTableCellSerial))
                    xrTableRow1.Cells.Add(xrTableCellSerial);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellser))
                    xrTableRow4.Cells.Remove(xrTableCellser);
                if (xrTableRow1.Cells.Contains(xrTableCellSerial))
                    xrTableRow1.Cells.Remove(xrTableCellSerial);

            }
            if (visibleCol.Contains("description"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellDescript))
                    xrTableRow4.Cells.Add(xrTableCellDescript);
                if (!xrTableRow1.Cells.Contains(xrTableCellDesc))
                    xrTableRow1.Cells.Add(xrTableCellDesc);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellDescript))
                    xrTableRow4.Cells.Remove(xrTableCellDescript);
                if (xrTableRow1.Cells.Contains(xrTableCellDesc))
                    xrTableRow1.Cells.Remove(xrTableCellDesc);

            }
            if (visibleCol.Contains("detialDesc"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellDdesc))
                    xrTableRow4.Cells.Add(xrTableCellDdesc);
                if (!xrTableRow1.Cells.Contains(xrTableCellDetialDesc))
                    xrTableRow1.Cells.Add(xrTableCellDetialDesc);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellDdesc))
                    xrTableRow4.Cells.Remove(xrTableCellDdesc);
                if (xrTableRow1.Cells.Contains(xrTableCellDetialDesc))
                    xrTableRow1.Cells.Remove(xrTableCellDetialDesc);

            }
            if (visibleCol.Contains("date"))
            {
                xrLabeldt.Visible = true;
                xrLabelDate.Visible = true;
            }
            else
            {
                xrLabeldt.Visible = false;
                xrLabelDate.Visible = false;
            }
            if (visibleCol.Contains("expUserName"))
            {
                xrLabelsupp.Visible = true;
                xrLabelExpUser.Visible = true;
            }
            else
            {
                xrLabelsupp.Visible = false;
                xrLabelExpUser.Visible = false;
            }
            if (visibleCol.Contains("docNumber"))
            {
                xrLabeldoc.Visible = true;
                xrLabelDocNum.Visible = true;
            }
            else
            {
                xrLabeldoc.Visible = false;
                xrLabelDocNum.Visible = false;
            }
            if (visibleCol.Contains("warehouseName"))
            {
                xrLabelware.Visible = true;
                xrLabelWareHouse.Visible = true;
            }
            else
            {
                xrLabelware.Visible = false;
                xrLabelWareHouse.Visible = false;
            }
            if (visibleCol.Contains("userName"))
            {
                xrLabelUser.Visible = true;
                xrLabelUserName.Visible = true;
            }
            else
            {
                xrLabelUser.Visible = false;
                xrLabelUserName.Visible = false;
            }
            if (!xrTableRow1.Cells.Contains(xrTableCellCount))
                xrTableRow1.Cells.Add(xrTableCellCount);
            if (!xrTableRow1.Cells.Contains(xrTableCellPrice))
                xrTableRow1.Cells.Add(xrTableCellPrice);
            if (!xrTableRow1.Cells.Contains(xrTableCellSumPrice))
                xrTableRow1.Cells.Add(xrTableCellSumPrice);
            if (!xrTableRow4.Cells.Contains(xrTableCellcn))
                xrTableRow4.Cells.Add(xrTableCellcn);
            if (!xrTableRow4.Cells.Contains(xrTableCellconc))
                xrTableRow4.Cells.Add(xrTableCellconc);
            if (!xrTableRow4.Cells.Contains(xrTableCellsPrice))
                xrTableRow4.Cells.Add(xrTableCellsPrice);
            xrTable2.PerformLayout();
            xrTableDetail.PerformLayout();
            xrTable1.PerformLayout();
            xrTableDetail.BeginInit();
            xrTable1.BeginInit();
            xrTableCellTotalCount.WidthF = xrTableCellCount.WidthF;
            xrTableCellTotalPrice.WidthF = xrTableCellPrice.WidthF;
            xrTableCellTotalSumPrice.WidthF = xrTableCellSumPrice.WidthF;
            xrTable1.EndInit();
            xrTableDetail.EndInit();
        }

        #endregion

    }
}
