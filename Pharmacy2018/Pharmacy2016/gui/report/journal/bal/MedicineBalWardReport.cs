﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Pharmacy2016.util;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.report.ds;
using Pharmacy2016.gui.core.journal.ds;

namespace Pharmacy2016.report.journal.bal
{
    /*
     * Тасгийн хэрэглэсэн эмийн тайлан
     * Эхний үлдэгдлийн тайлан
     * 
     */ 

    public partial class MedicineBalWardReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

            private static MedicineBalWardReport INSTANCE = new MedicineBalWardReport();

            public static MedicineBalWardReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private MedicineBalWardReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(int type, string start, string end, string wardID, string filter)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                    initBinding(type);
                    if (type == 1)
                    {
                        xrLabel1.Visible = true;
                        xrLabel4.Visible = true;
                        xrLabelEnd.Visible = true;
                        xrLabelStart.Visible = true;

                        xrTableCell12.Text = "Тасгийн дүн : ";
                        xrLabelPageHeader.Text = "ТАСГИЙН ХЭРЭГЛЭСЭН ЭМИЙН ТАЙЛАН";
                        xrLabelStart.Text = start;
                        xrLabelEnd.Text = end;
                        xrLabel5.Text = UserUtil.getUserLastName();
                        xrLabel6.Text = UserUtil.getUserFirstName();

                        ReportDataSet.WardTableAdapter.Fill(ReportDataSet.Instance.Ward, HospitalUtil.getHospitalId(), start, end, wardID);
                    }
                    else
                    {
                        xrLabel1.Visible = false;
                        xrLabel4.Visible = false;
                        xrLabelEnd.Visible = false;
                        xrLabelStart.Visible = false;

                        xrTableCell12.Text = "Нярав, эм зүйчийн дүн : ";
                        xrLabelPageHeader.Text = "ЭХНИЙ ҮЛДЭГДЛИЙН ТАЙЛАН";
                    }

                    FilterString = filter;

                    CreateDocument(true);

                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void initBinding(int type)
            {
                if (type == 1)
                {
                    DataSource = ReportDataSet.Instance;
                    DataAdapter = ReportDataSet.WardTableAdapter;
                    DataMember = ReportDataSet.Instance.Ward.ToString();

                    this.GroupHeaderWard.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                        new DevExpress.XtraReports.UI.GroupField("WardID", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});

                    this.xrTableCellGroupHeader.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".WardName", "")});

                    this.xrTableCellUnit.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Unit", "")});
                }
                else 
                {
                    DataSource = JournalDataSet.Instance;
                    DataAdapter = JournalDataSet.BegBalTableAdapter;
                    DataMember = JournalDataSet.Instance.BegBal.ToString();

                    this.GroupHeaderWard.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                        new DevExpress.XtraReports.UI.GroupField("UserID", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});

                    this.xrTableCellGroupHeader.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".groupHeader", "")});

                    this.xrTableCellUnit.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UnitType", "")});
                }
                
                this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MedicineName", "")});
                this.xrTableCellTypeName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MedicineTypeName", "")});
                this.xrTableCellShape.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Shape", "")});
                
                this.xrTableCellValiDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ValidDate", ReportUtil.DisplayFormatDate)});
                this.xrTableCellCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Price", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatFloat)});

                this.xrTableCellGroupCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellGroupSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});

                this.xrTableCellTotalCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellTotalSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
            }

        #endregion

    }
}
