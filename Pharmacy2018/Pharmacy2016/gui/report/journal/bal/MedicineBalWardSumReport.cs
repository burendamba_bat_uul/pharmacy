﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Pharmacy2016.util;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.report.journal.bal
{
    /*
     * Тасгийн нийт хэрэглэсэн эмийн тайлан
     * 
     */ 

    public partial class MedicineBalWardSumReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

            private static MedicineBalWardSumReport INSTANCE = new MedicineBalWardSumReport();

            public static MedicineBalWardSumReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private MedicineBalWardSumReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = ReportDataSet.Instance;
                DataAdapter = ReportDataSet.WardSumTableAdapter;
                DataMember = ReportDataSet.Instance.WardSum.ToString();
                
                initBinding();
            }

            private void initBinding()
            {
                this.xrTableCellWardName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Name", "")});
                this.xrTableCellExpCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ExpCount", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellExpSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ExpSumPrice", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellPatientCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PatientCount", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellPatientSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PatientSumPrice", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatFloat)});

                this.xrTableCellTotalExpCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ExpCount", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellTotalExpSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ExpSumPrice", ReportUtil.DisplayFormatCurrency)});
                this.xrTableCellTotalPatientCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PatientCount", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellTotalPatientSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PatientSumPrice", ReportUtil.DisplayFormatCurrency)});
                this.xrTableCellTotalCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellTotalSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(string start, string end, string wardID)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);
                    xrLabelStart.Text = start;
                    xrLabelEnd.Text = end;
                    xrLabel5.Text = UserUtil.getUserLastName();
                    xrLabel6.Text = UserUtil.getUserFirstName();
                    
                    ReportDataSet.WardSumTableAdapter.Fill(ReportDataSet.Instance.WardSum, HospitalUtil.getHospitalId(), start, end, wardID);

                    CreateDocument(true);

                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}
