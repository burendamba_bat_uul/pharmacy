﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;
using System.Collections.Generic;
using DevExpress.XtraReports.UI.PivotGrid;
using System.Data;
using System.Linq;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.gui.report.journal.bal
{
    /*
     * Зарлагын журналын тайлан
     * 
     */

    public partial class BalMultiReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

        private static BalMultiReport INSTANCE = new BalMultiReport();

        public static BalMultiReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private BalMultiReport()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
           
        }

        private void groupByMedicine()
        {
            DataTable dt = JournalDataSet.Instance.Bal;
            var result = dt.AsEnumerable()
                                .GroupBy(row => new { medicineID = row["medicineID"], medicineName = row["medicineName"], latinName = row["latinName"], medicineTypeName = row["medicineTypeName"], unitType = row["unitType"], shape = row["shape"], quantity = row["quantity"], validDate = row.Field<DateTime>("validDate"), barcode = row["barcode"], serial = row["serial"], price = row.Field<Double>("price") })
                                .Select(grp => new { medicineID = grp.Key.medicineID, medicineName = grp.Key.medicineName, latinName = grp.Key.latinName, medicineTypeName = grp.Key.medicineTypeName, unitType = grp.Key.unitType, shape = grp.Key.shape, quantity = grp.Key.quantity, validDate = grp.Key.validDate, barcode = grp.Key.barcode, serial = grp.Key.serial, price = grp.Key.price, begBal = grp.Sum(x => decimal.Parse(x["begbal"].ToString())), 
                                    begSumPrice = grp.Sum(x => decimal.Parse(x["begSumPrice"].ToString())), 
                                    totalIncCount = grp.Sum(x => decimal.Parse(x["totalIncCount"].ToString())),      totalIncSumPrice = grp.Sum(x => decimal.Parse(x["totalIncSumPrice"].ToString())),        totalExpCount = grp.Sum(x => decimal.Parse(x["totalExpCount"].ToString())), 
                                    totalExpSumPrice = grp.Sum(x => decimal.Parse(x["totalExpSumPrice"].ToString())), 
                                    endBal = grp.Sum(x => decimal.Parse(x["endBal"].ToString())), 
                                    endSumPrice = grp.Sum(x => decimal.Parse(x["endSumPrice"].ToString())) });  
            //grp["medicineName"] &&
            //grp.Sum(x=> decimal.Parse(x["begbal"].ToString())))}
            //incCount = grp.Sum(x => x.Field<int>("incCount")),
            //expCount = grp.Sum(x => x.Field<int>("expCount")),
            //endBal = grp.Sum(x => x.Field<int>("endBal")))
            //JournalDataSet.Instance.Bal.Rows.Clear();
            ReportDataSet.Instance.Bal.Rows.Clear();
            foreach (var t in result)
            {
                DataRow newRow = ReportDataSet.Instance.Bal.NewRow();
                newRow["medicineID"] = t.medicineID;
                newRow["medicineName"] = t.medicineName;
                newRow["latinName"] = t.latinName;
                newRow["unitType"] = t.unitType;
                newRow["shape"] = t.shape;
                newRow["quantity"] = t.quantity;
                newRow["validDate"] = t.validDate;
                newRow["barcode"] = t.barcode;
                newRow["serial"] = t.serial;
                newRow["price"] = t.price;
                newRow["begbal"] = t.begBal;
                newRow["begSumPrice"] = t.begSumPrice;
                newRow["totalIncCount"] = t.totalIncCount;
                newRow["totalIncSumPrice"] = t.totalIncSumPrice;
                newRow["totalExpCount"] = t.totalExpCount;
                newRow["totalExpSumPrice"] = t.totalExpSumPrice;
                newRow["endBal"] = t.endBal;
                newRow["endSumPrice"] = t.endSumPrice;
                ReportDataSet.Instance.Bal.Rows.Add(newRow);
            }
        }

        private void initBinding()
        {
            this.xrTableCellFullName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UserName", "")});
            this.xrTableCellRoleName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".RoleName", "")});
            this.xrTableCellwarehouse.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".WarehouseName", "")});
            this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MedicineName", "")});
            this.xrTableCellShape.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Shape", "")});
            this.xrTableCellUnit.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UnitType", "")});
            this.xrTableCellQuantity.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Quantity", "")});
            this.xrTableCellValidDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ValidDate", ReportUtil.DisplayFormatDate)});
            this.xrTableCellBarCode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BarCode", ReportUtil.DisplayFormatDate)});
            this.xrTableCellSerial.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Serial", ReportUtil.DisplayFormatDate)});
            this.xrTableCellBegBal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BegBal", ReportUtil.DisplayFormatNumber)});
            this.xrTableCellBegSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BegSumPrice", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Price", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellTotalPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Price", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellIncCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalIncCount", ReportUtil.DisplayFormatNumber)});
            this.xrTableCellIncSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalIncSumPrice", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellExpCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalExpCount", ReportUtil.DisplayFormatNumber)});
            this.xrTableCellExpSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalExpSumPrice", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellEndBal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndBal", ReportUtil.DisplayFormatNumber)});
            this.xrTableCellEndSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndSumPrice", ReportUtil.DisplayFormatFloat)});


            this.xrTableCellTotalBegCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BegBal", ReportUtil.DisplayFormatNumber)});
            this.xrTableCellTotalBegSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BegSumPrice", ReportUtil.DisplayFormatCurrency)});
            this.xrTableCellTotalIncCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalIncCount", ReportUtil.DisplayFormatNumber)});
            this.xrTableCellTotalIncSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalIncSumPrice", ReportUtil.DisplayFormatCurrency)});
            this.xrTableCellTotalExpCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalExpCount", ReportUtil.DisplayFormatNumber)});
            this.xrTableCellTotalExpSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalExpSumPrice", ReportUtil.DisplayFormatCurrency)});
            this.xrTableCellTotalEndBal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndBal", ReportUtil.DisplayFormatNumber)});
            this.xrTableCellTotalEndSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndSumPrice", ReportUtil.DisplayFormatCurrency)});
        }

        #endregion

        #region Тайлан харуулах функц.

        public void initAndShow(List<string> columnVis, List<string> filter, string start, string end, int type, int location)
        {
            try
            {
                if (!isInit)
                {
                    initForm();
                }
                if (type == 0) {
                    DataSource = ReportDataSet.Instance;
                    //DataAdapter = ReportDataSet.BalDataTable;
                    DataMember = ReportDataSet.Instance.Bal.ToString();
                    groupByMedicine();
                }
                else
                {
                    DataSource = JournalDataSet.Instance;
                    DataAdapter = JournalDataSet.BalTableAdapter;
                    DataMember = JournalDataSet.Instance.Bal.ToString();
                }
                initBinding();  

                ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);
                hideColumns(columnVis);
                xrLabelTreatmentDirector.Text = "Батлав: ЭЭО ........................................../ " + HospitalUtil.getHospitalTreatmentDirectorShortName() + " /";
                xrLabelTreatmentSign.Text = "Батлав: ЭЭО ........................................../ " + HospitalUtil.getHospitalTreatmentDirectorShortName() + " /";
                switch (location)
                {
                    case 0:
                        xrLabelTreatmentSign.Visible = true;
                        xrLabelTreatmentDirector.Visible = false;
                        xrLabelTreatmentSign.LocationF = new PointF(104.98F, 187.77F);
                        break;
                    case 1:
                        xrLabelTreatmentSign.Visible = true;
                        xrLabelTreatmentDirector.Visible = false;
                        xrLabelTreatmentSign.LocationF = new PointF(851.96F, 187.77F);
                        break;
                    case 2:
                        xrLabelTreatmentSign.Visible = true;
                        xrLabelTreatmentDirector.Visible = false;
                        xrLabelTreatmentSign.LocationF = new PointF(1551.84F, 187.77F);
                        break;
                    case 3:
                        xrLabelTreatmentSign.Visible = false;
                        xrLabelTreatmentDirector.Visible = true;
                        xrLabelTreatmentDirector.LocationF = new PointF(104.98F, 106.17F);
                        break;
                    case 4:
                        xrLabelTreatmentSign.Visible = false;
                        xrLabelTreatmentDirector.Visible = true;
                        xrLabelTreatmentDirector.LocationF = new PointF(926.05F, 106.17F);
                        break;
                    case 5:
                        xrLabelTreatmentSign.Visible = false;
                        xrLabelTreatmentDirector.Visible = true;
                        xrLabelTreatmentDirector.LocationF = new PointF(926.05F, 106.17F);
                        break;
                    default:
                        break;
                }
                xrLabelCompany.Text = HospitalUtil.getHospitalName();
                xrLabelReportHeader.Text = "БАРАА МАТЕРИАЛЫН ТАЙЛАН";
                xrLabelStart.Text = start;
                xrLabelEnd.Text = end;
                CreateDocument(false);
                ProgramUtil.closeWaitDialog();
                ReportViewForm.Instance.initAndShow(INSTANCE);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void hideColumns(List<string> visibleCol)
        {
            xrTable2.SuspendLayout();
            xrTableDetail.SuspendLayout();
            xrTable1.SuspendLayout();
            xrTableHeader.SuspendLayout();
            if (xrTableRow1.Cells.Contains(xrTableCellBegBal))
                xrTableRow1.Cells.Remove(xrTableCellBegBal);
            if (xrTableRow1.Cells.Contains(xrTableCellPrice))
                xrTableRow1.Cells.Remove(xrTableCellPrice);
            if (xrTableRow1.Cells.Contains(xrTableCellBegSumPrice))
                xrTableRow1.Cells.Remove(xrTableCellBegSumPrice);
            if (xrTableRow1.Cells.Contains(xrTableCellIncCount))
                xrTableRow1.Cells.Remove(xrTableCellIncCount);
            if (xrTableRow1.Cells.Contains(xrTableCellIncSumPrice))
                xrTableRow1.Cells.Remove(xrTableCellIncSumPrice);
            if (xrTableRow1.Cells.Contains(xrTableCellExpCount))
                xrTableRow1.Cells.Remove(xrTableCellExpCount);
            if (xrTableRow1.Cells.Contains(xrTableCellExpSumPrice))
                xrTableRow1.Cells.Remove(xrTableCellExpSumPrice);
            if (xrTableRow1.Cells.Contains(xrTableCellEndBal))
                xrTableRow1.Cells.Remove(xrTableCellEndBal);
            if (xrTableRow1.Cells.Contains(xrTableCellEndSumPrice))
                xrTableRow1.Cells.Remove(xrTableCellEndSumPrice);
            if (xrTableRow4.Cells.Contains(xrTableCellcnt))
                xrTableRow4.Cells.Remove(xrTableCellcnt);
            if (xrTableRow4.Cells.Contains(xrTableCellpr))
                xrTableRow4.Cells.Remove(xrTableCellpr);
            if (xrTableRow4.Cells.Contains(xrTableCellspr))
                xrTableRow4.Cells.Remove(xrTableCellspr);
            if (xrTableRow4.Cells.Contains(xrTableCellocnt))
                xrTableRow4.Cells.Remove(xrTableCellocnt);
            if (xrTableRow4.Cells.Contains(xrTableCellopr))
                xrTableRow4.Cells.Remove(xrTableCellopr);
            if (xrTableRow4.Cells.Contains(xrTableCellzcnt))
                xrTableRow4.Cells.Remove(xrTableCellzcnt);
            if (xrTableRow4.Cells.Contains(xrTableCellzpr))
                xrTableRow4.Cells.Remove(xrTableCellzpr);
            if (xrTableRow4.Cells.Contains(xrTableCelllcnt))
                xrTableRow4.Cells.Remove(xrTableCelllcnt);
            if (xrTableRow4.Cells.Contains(xrTableCelllpr))
                xrTableRow4.Cells.Remove(xrTableCelllpr);
            if (xrTableRow4.Cells.Contains(xrTableCelluser))
                xrTableRow4.Cells.Remove(xrTableCelluser);
            if (xrTableRow1.Cells.Contains(xrTableCellFullName))
                xrTableRow1.Cells.Remove(xrTableCellFullName);
            if (xrTableRow4.Cells.Contains(xrTableCellrole))
                xrTableRow4.Cells.Remove(xrTableCellrole);
            if (xrTableRow1.Cells.Contains(xrTableCellRoleName))
                xrTableRow1.Cells.Remove(xrTableCellRoleName);
            if (xrTableRow4.Cells.Contains(xrTableCellwr))
                xrTableRow4.Cells.Remove(xrTableCellwr);
            if (xrTableRow1.Cells.Contains(xrTableCellwarehouse))
                xrTableRow1.Cells.Remove(xrTableCellwarehouse);
            if (xrTableRow4.Cells.Contains(xrTableCellmed))
                xrTableRow4.Cells.Remove(xrTableCellmed);
            if (xrTableRow1.Cells.Contains(xrTableCellMedicineName))
                xrTableRow1.Cells.Remove(xrTableCellMedicineName);
            if (xrTableRow4.Cells.Contains(xrTableCellsha))
                xrTableRow4.Cells.Remove(xrTableCellsha);
            if (xrTableRow1.Cells.Contains(xrTableCellShape))
                xrTableRow1.Cells.Remove(xrTableCellShape);
            if (xrTableRow4.Cells.Contains(xrTableCelluni))
                xrTableRow4.Cells.Remove(xrTableCelluni);
            if (xrTableRow1.Cells.Contains(xrTableCellUnit))
                xrTableRow1.Cells.Remove(xrTableCellUnit);
            if (xrTableRow4.Cells.Contains(xrTableCellser))
                xrTableRow4.Cells.Remove(xrTableCellser);
            if (xrTableRow1.Cells.Contains(xrTableCellSerial))
                xrTableRow1.Cells.Remove(xrTableCellSerial);
            if (xrTableRow4.Cells.Contains(xrTableCellbar))
                xrTableRow4.Cells.Remove(xrTableCellbar);
            if (xrTableRow1.Cells.Contains(xrTableCellBarCode))
                xrTableRow1.Cells.Remove(xrTableCellBarCode);
            if (xrTableRow4.Cells.Contains(xrTableCellquant))
                xrTableRow4.Cells.Remove(xrTableCellquant);
            if (xrTableRow1.Cells.Contains(xrTableCellQuantity))
                xrTableRow1.Cells.Remove(xrTableCellQuantity);
            if (xrTableRow4.Cells.Contains(xrTableCellvalid))
                xrTableRow4.Cells.Remove(xrTableCellvalid);
            if (xrTableRow1.Cells.Contains(xrTableCellValidDate))
                xrTableRow1.Cells.Remove(xrTableCellValidDate);
            if (visibleCol.Contains("userName"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCelluser))
                    xrTableRow4.Cells.Add(xrTableCelluser);
                if (!xrTableRow1.Cells.Contains(xrTableCellFullName))
                    xrTableRow1.Cells.Add(xrTableCellFullName);
            }
            if (visibleCol.Contains("roleName"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellrole))
                    xrTableRow4.Cells.Add(xrTableCellrole);
                if (!xrTableRow1.Cells.Contains(xrTableCellRoleName))
                    xrTableRow1.Cells.Add(xrTableCellRoleName);
            }
            if (visibleCol.Contains("warehouseName"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellwr))
                    xrTableRow4.Cells.Add(xrTableCellwr);
                if (!xrTableRow1.Cells.Contains(xrTableCellwarehouse))
                    xrTableRow1.Cells.Add(xrTableCellwarehouse);
            }
            if (visibleCol.Contains("medicineName"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellmed))
                    xrTableRow4.Cells.Add(xrTableCellmed);
                if (!xrTableRow1.Cells.Contains(xrTableCellMedicineName))
                    xrTableRow1.Cells.Add(xrTableCellMedicineName);
            }
            if (visibleCol.Contains("shape"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellsha))
                    xrTableRow4.Cells.Add(xrTableCellsha);
                if (!xrTableRow1.Cells.Contains(xrTableCellShape))
                    xrTableRow1.Cells.Add(xrTableCellShape);
            }
            if (visibleCol.Contains("unitType"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCelluni))
                    xrTableRow4.Cells.Add(xrTableCelluni);
                if (!xrTableRow1.Cells.Contains(xrTableCellUnit))
                    xrTableRow1.Cells.Add(xrTableCellUnit);
            }
            if (visibleCol.Contains("serial"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellser))
                    xrTableRow4.Cells.Add(xrTableCellser);
                if (!xrTableRow1.Cells.Contains(xrTableCellSerial))
                    xrTableRow1.Cells.Add(xrTableCellSerial);
            }
            if (visibleCol.Contains("barcode"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellbar))
                    xrTableRow4.Cells.Add(xrTableCellbar);
                if (!xrTableRow1.Cells.Contains(xrTableCellBarCode))
                    xrTableRow1.Cells.Add(xrTableCellBarCode);
            }
            if (visibleCol.Contains("quantity"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellquant))
                    xrTableRow4.Cells.Add(xrTableCellquant);
                if (!xrTableRow1.Cells.Contains(xrTableCellQuantity))
                    xrTableRow1.Cells.Add(xrTableCellQuantity);
            }
            if (visibleCol.Contains("validDate"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellvalid))
                    xrTableRow4.Cells.Add(xrTableCellvalid);
                if (!xrTableRow1.Cells.Contains(xrTableCellValidDate))
                    xrTableRow1.Cells.Add(xrTableCellValidDate);
            }
           
            if (!xrTableRow1.Cells.Contains(xrTableCellBegBal))
                xrTableRow1.Cells.Add(xrTableCellBegBal);
            if (!xrTableRow1.Cells.Contains(xrTableCellPrice))
                xrTableRow1.Cells.Add(xrTableCellPrice);
            if (!xrTableRow1.Cells.Contains(xrTableCellBegSumPrice))
                xrTableRow1.Cells.Add(xrTableCellBegSumPrice);
            if (!xrTableRow1.Cells.Contains(xrTableCellIncCount))
                xrTableRow1.Cells.Add(xrTableCellIncCount);
            if (!xrTableRow1.Cells.Contains(xrTableCellIncSumPrice))
                xrTableRow1.Cells.Add(xrTableCellIncSumPrice);
            if (!xrTableRow1.Cells.Contains(xrTableCellExpCount))
                xrTableRow1.Cells.Add(xrTableCellExpCount);
            if (!xrTableRow1.Cells.Contains(xrTableCellExpSumPrice))
                xrTableRow1.Cells.Add(xrTableCellExpSumPrice);
            if (!xrTableRow1.Cells.Contains(xrTableCellEndBal))
                xrTableRow1.Cells.Add(xrTableCellEndBal);
            if (!xrTableRow1.Cells.Contains(xrTableCellEndSumPrice))
                xrTableRow1.Cells.Add(xrTableCellEndSumPrice);
            if (!xrTableRow4.Cells.Contains(xrTableCellcnt))
                xrTableRow4.Cells.Add(xrTableCellcnt);
            if (!xrTableRow4.Cells.Contains(xrTableCellpr))
                xrTableRow4.Cells.Add(xrTableCellpr);
            if (!xrTableRow4.Cells.Contains(xrTableCellspr))
                xrTableRow4.Cells.Add(xrTableCellspr);
            if (!xrTableRow4.Cells.Contains(xrTableCellocnt))
                xrTableRow4.Cells.Add(xrTableCellocnt);
            if (!xrTableRow4.Cells.Contains(xrTableCellopr))
                xrTableRow4.Cells.Add(xrTableCellopr);
            if (!xrTableRow4.Cells.Contains(xrTableCellzcnt))
                xrTableRow4.Cells.Add(xrTableCellzcnt);
            if (!xrTableRow4.Cells.Contains(xrTableCellzpr))
                xrTableRow4.Cells.Add(xrTableCellzpr);
            if (!xrTableRow4.Cells.Contains(xrTableCelllcnt))
                xrTableRow4.Cells.Add(xrTableCelllcnt);
            if (!xrTableRow4.Cells.Contains(xrTableCelllpr))
                xrTableRow4.Cells.Add(xrTableCelllpr);
            xrTable2.PerformLayout();
            xrTableDetail.PerformLayout();
            xrTable1.PerformLayout();
            xrTableHeader.PerformLayout();
            xrTableDetail.BeginInit();
            xrTableHeader.BeginInit();
            xrTable1.BeginInit();
            xrTable2.BeginInit();
            xrTableCellBegBal.WidthF = 133.5F;
            xrTableCellPrice.WidthF = 133.5F;
            xrTableCellBegSumPrice.WidthF = 141.84F;
            xrTableCellIncCount.WidthF = 133.5F;
            xrTableCellIncSumPrice.WidthF = 141.84F;
            xrTableCellExpCount.WidthF = 133.5F;
            xrTableCellExpSumPrice.WidthF = 141.84F;
            xrTableCellEndBal.WidthF = 133.5F;
            xrTableCellEndSumPrice.WidthF = 141.84F;
            xrTableCellTotalBegCount.WidthF = xrTableCellBegBal.WidthF;
            xrTableCellTotalPrice.WidthF = xrTableCellPrice.WidthF;
            xrTableCellTotalBegSumPrice.WidthF = xrTableCellBegSumPrice.WidthF;
            xrTableCellTotalIncCount.WidthF = xrTableCellIncCount.WidthF;
            xrTableCellTotalIncSumPrice.WidthF = xrTableCellIncSumPrice.WidthF;
            xrTableCellTotalExpSumPrice.WidthF = xrTableCellExpSumPrice.WidthF;
            xrTableCellTotalExpCount.WidthF = xrTableCellExpCount.WidthF;
            xrTableCellTotalEndBal.WidthF = xrTableCellEndBal.WidthF;
            xrTableCellTotalEndSumPrice.WidthF = xrTableCellEndSumPrice.WidthF;
            xrTableCellcnt.WidthF = xrTableCellBegBal.WidthF;
            xrTableCellpr.WidthF = xrTableCellPrice.WidthF;
            xrTableCellspr.WidthF = xrTableCellBegSumPrice.WidthF;
            xrTableCellocnt.WidthF = xrTableCellIncCount.WidthF;
            xrTableCellopr.WidthF = xrTableCellIncSumPrice.WidthF;
            xrTableCellzpr.WidthF = xrTableCellExpSumPrice.WidthF;
            xrTableCellzcnt.WidthF = xrTableCellExpCount.WidthF;
            xrTableCelllcnt.WidthF = xrTableCellEndBal.WidthF;
            xrTableCelllpr.WidthF = xrTableCellEndSumPrice.WidthF;
            xrTableCellBeg.WidthF = xrTableCellBegBal.WidthF + xrTableCellPrice.WidthF + xrTableCellBegSumPrice.WidthF;
            xrTableCellInc.WidthF = xrTableCellIncCount.WidthF + xrTableCellIncSumPrice.WidthF;
            xrTableCellExp.WidthF = xrTableCellExpSumPrice.WidthF + xrTableCellExpCount.WidthF;
            xrTableCellEnd.WidthF = xrTableCellEndBal.WidthF + xrTableCellEndSumPrice.WidthF;
            xrTableCellMedicine.WidthF = xrTableHeader.WidthF - (xrTableCellBeg.WidthF + xrTableCellInc.WidthF + xrTableCellExp.WidthF + xrTableCellEnd.WidthF);
            xrTableCellNumber.WidthF = xrTableCellNo.WidthF;
            xrTable1.EndInit();
            xrTableDetail.EndInit();
            xrTableHeader.EndInit();
            xrTable2.EndInit();
        }

        #endregion
    }
}
