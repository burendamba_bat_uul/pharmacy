﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Pharmacy2016.util;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.ds;
using System.Data.SqlClient;

namespace Pharmacy2016.report.journal.bal
{
    /*
     * Эмийн үлдэгдлийн тайлан
     * 
     */ 

    public partial class MedicineBalReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

            private static MedicineBalReport INSTANCE = new MedicineBalReport();

            public static MedicineBalReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private MedicineBalReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = JournalDataSet.Instance;
                DataAdapter = JournalDataSet.BalTableAdapter;
                DataMember = JournalDataSet.Instance.Bal.ToString();
                
                initBinding();
            }

            private void initBinding()
            {
                this.xrTableCellFullName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UserName", "")});
                this.xrTableCellRoleName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".RoleName", "")});
                this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MedicineName", "")});
                this.xrTableCellShape.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Shape", "")});
                this.xrTableCellUnit.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UnitType", "")});
                this.xrTableCellValidDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ValidDate", ReportUtil.DisplayFormatDate)});
                this.xrTableCellBegBal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BegBal", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellBegSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BegSumPrice", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Price", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellIncCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalIncCount", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellIncSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalIncSumPrice", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellExpCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalExpCount", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellExpSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalExpSumPrice", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellEndBal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndBal", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellEndSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndSumPrice", ReportUtil.DisplayFormatFloat)});


                this.xrTableCellTotalBegCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BegBal", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellTotalBegSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BegSumPrice", ReportUtil.DisplayFormatCurrency)});
                this.xrTableCellTotalIncCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalIncCount", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellTotalIncSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalIncSumPrice", ReportUtil.DisplayFormatCurrency)});
                this.xrTableCellTotalExpCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalExpCount", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellTotalExpSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TotalExpSumPrice", ReportUtil.DisplayFormatCurrency)});
                this.xrTableCellTotalEndBal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndBal", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellTotalEndSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndSumPrice", ReportUtil.DisplayFormatCurrency)});
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(string start, string end, string filter)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                    xrLabelStart.Text = start;
                    xrLabelEnd.Text = end;

                    SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                    myConn.Open();
                    string query = "SELECT * FROM [ReportSetting]";
                    SqlCommand oCmd = new SqlCommand(query, myConn);
                    using (SqlDataReader oReader = oCmd.ExecuteReader())
                    {
                        while (oReader.Read())
                        {
                            if (oReader.HasRows)
                            {
                                if (oReader["code"].ToString() == "0")
                                {
                                    xrLabel3.Text = oReader["name"].ToString();
                                }
                                //else if (oReader["code"].ToString() == "1" && !string.IsNullOrEmpty(oReader["employ"].ToString()))
                                //{
                                //    xrLabel6.Text = oReader["employ"].ToString();
                                //    xrLabelStoreman.Text = "................................................../ " + oReader["name"].ToString();
                                //}
                                //else if (oReader["code"].ToString() == "2" && !string.IsNullOrEmpty(oReader["employ"].ToString()))
                                //{
                                //    xrLabel9.Text = oReader["employ"].ToString();
                                //    xrLabelAccount.Text = "................................................../ " + oReader["name"].ToString();
                                //}
                                //else if (oReader["code"].ToString() == "3" && !string.IsNullOrEmpty(oReader["employ"].ToString()))
                                //{
                                //    xrLabel12.Text = oReader["employ"].ToString();
                                //    xrLabel13.Text = "................................................../ " + oReader["name"].ToString();
                                //}

                            }
                        }
                    }
                    myConn.Close();

                    FilterString = filter;

                    CreateDocument(true);
                    
                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}
