﻿namespace Pharmacy2016.gui.report.journal.bal
{
    partial class BalMultiReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTableDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellFullName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellRoleName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellwarehouse = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellMedicineName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellShape = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellUnit = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellSerial = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellBarCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellQuantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellValidDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellBegBal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellBegSumPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIncCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIncSumPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellExpCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellExpSumPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellEndBal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellEndSumPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTableHeader = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellMedicine = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellBeg = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellInc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellExp = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellEnd = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCelluser = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellrole = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellwr = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellmed = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellsha = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCelluni = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellser = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellbar = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellquant = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellvalid = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellcnt = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellpr = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellspr = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellocnt = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellopr = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellzcnt = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellzpr = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCelllcnt = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCelllpr = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabelReportHeader = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCompany = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelStart = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelEnd = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTreatmentDirector = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabelTreatmentSign = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalBegCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalBegSumPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalIncCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalIncSumPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalExpCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalExpSumPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalEndBal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalEndSumPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.filter = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableDetail});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 63.5F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableDetail
            // 
            this.xrTableDetail.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTableDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableDetail.Dpi = 254F;
            this.xrTableDetail.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableDetail.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTableDetail.Name = "xrTableDetail";
            this.xrTableDetail.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTableDetail.SizeF = new System.Drawing.SizeF(2770F, 63.5F);
            this.xrTableDetail.StylePriority.UseBorders = false;
            this.xrTableDetail.StylePriority.UseFont = false;
            this.xrTableDetail.StylePriority.UsePadding = false;
            this.xrTableDetail.StylePriority.UseTextAlignment = false;
            this.xrTableDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellNumber,
            this.xrTableCellFullName,
            this.xrTableCellRoleName,
            this.xrTableCellwarehouse,
            this.xrTableCellMedicineName,
            this.xrTableCellShape,
            this.xrTableCellUnit,
            this.xrTableCellSerial,
            this.xrTableCellBarCode,
            this.xrTableCellQuantity,
            this.xrTableCellValidDate,
            this.xrTableCellBegBal,
            this.xrTableCellPrice,
            this.xrTableCellBegSumPrice,
            this.xrTableCellIncCount,
            this.xrTableCellIncSumPrice,
            this.xrTableCellExpCount,
            this.xrTableCellExpSumPrice,
            this.xrTableCellEndBal,
            this.xrTableCellEndSumPrice});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCellNumber
            // 
            this.xrTableCellNumber.Dpi = 254F;
            this.xrTableCellNumber.Multiline = true;
            this.xrTableCellNumber.Name = "xrTableCellNumber";
            this.xrTableCellNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrTableCellNumber.StylePriority.UsePadding = false;
            xrSummary1.FormatString = "{0:#}";
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellNumber.Summary = xrSummary1;
            this.xrTableCellNumber.Weight = 0.18389065101724708D;
            // 
            // xrTableCellFullName
            // 
            this.xrTableCellFullName.Dpi = 254F;
            this.xrTableCellFullName.Multiline = true;
            this.xrTableCellFullName.Name = "xrTableCellFullName";
            this.xrTableCellFullName.ProcessDuplicatesMode = DevExpress.XtraReports.UI.ProcessDuplicatesMode.Merge;
            this.xrTableCellFullName.Text = "Эм, эмнэлгийн хэрэгсэл\r\n";
            this.xrTableCellFullName.Weight = 0.74734758659290823D;
            // 
            // xrTableCellRoleName
            // 
            this.xrTableCellRoleName.Dpi = 254F;
            this.xrTableCellRoleName.Multiline = true;
            this.xrTableCellRoleName.Name = "xrTableCellRoleName";
            this.xrTableCellRoleName.ProcessDuplicatesMode = DevExpress.XtraReports.UI.ProcessDuplicatesMode.Merge;
            this.xrTableCellRoleName.Text = "Ангилал";
            this.xrTableCellRoleName.Weight = 0.39062908333927648D;
            // 
            // xrTableCellwarehouse
            // 
            this.xrTableCellwarehouse.Dpi = 254F;
            this.xrTableCellwarehouse.Name = "xrTableCellwarehouse";
            this.xrTableCellwarehouse.Text = "xrTableCellwarehouse";
            this.xrTableCellwarehouse.Weight = 0.51703944741357388D;
            // 
            // xrTableCellMedicineName
            // 
            this.xrTableCellMedicineName.Dpi = 254F;
            this.xrTableCellMedicineName.Name = "xrTableCellMedicineName";
            this.xrTableCellMedicineName.Text = "Хэлбэр";
            this.xrTableCellMedicineName.Weight = 0.80674871247928026D;
            // 
            // xrTableCellShape
            // 
            this.xrTableCellShape.Dpi = 254F;
            this.xrTableCellShape.Name = "xrTableCellShape";
            this.xrTableCellShape.Text = "Тун хэмжээ";
            this.xrTableCellShape.Weight = 0.48771065975736533D;
            // 
            // xrTableCellUnit
            // 
            this.xrTableCellUnit.CanShrink = true;
            this.xrTableCellUnit.Dpi = 254F;
            this.xrTableCellUnit.Multiline = true;
            this.xrTableCellUnit.Name = "xrTableCellUnit";
            this.xrTableCellUnit.Text = "Хүчинтэй хугацаа";
            this.xrTableCellUnit.Weight = 0.4618741504566598D;
            // 
            // xrTableCellSerial
            // 
            this.xrTableCellSerial.Dpi = 254F;
            this.xrTableCellSerial.Name = "xrTableCellSerial";
            this.xrTableCellSerial.Text = "xrTableCellSerial";
            this.xrTableCellSerial.Weight = 0.50398493727914628D;
            // 
            // xrTableCellBarCode
            // 
            this.xrTableCellBarCode.Dpi = 254F;
            this.xrTableCellBarCode.Name = "xrTableCellBarCode";
            this.xrTableCellBarCode.Text = "xrTableCellBarCode";
            this.xrTableCellBarCode.Weight = 0.45533062062249985D;
            // 
            // xrTableCellQuantity
            // 
            this.xrTableCellQuantity.Dpi = 254F;
            this.xrTableCellQuantity.Name = "xrTableCellQuantity";
            this.xrTableCellQuantity.Text = "xrTableCellQuantity";
            this.xrTableCellQuantity.Weight = 0.52344678274879275D;
            // 
            // xrTableCellValidDate
            // 
            this.xrTableCellValidDate.Dpi = 254F;
            this.xrTableCellValidDate.Name = "xrTableCellValidDate";
            this.xrTableCellValidDate.Text = "Тоо ширхэг";
            this.xrTableCellValidDate.Weight = 0.56800524288851062D;
            // 
            // xrTableCellBegBal
            // 
            this.xrTableCellBegBal.Dpi = 254F;
            this.xrTableCellBegBal.Name = "xrTableCellBegBal";
            this.xrTableCellBegBal.StylePriority.UseTextAlignment = false;
            this.xrTableCellBegBal.Text = "Үнэ";
            this.xrTableCellBegBal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellBegBal.Weight = 0.4909763477839601D;
            // 
            // xrTableCellPrice
            // 
            this.xrTableCellPrice.Dpi = 254F;
            this.xrTableCellPrice.Name = "xrTableCellPrice";
            this.xrTableCellPrice.StylePriority.UseTextAlignment = false;
            this.xrTableCellPrice.Text = "xrTableCellPrice";
            this.xrTableCellPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellPrice.Weight = 0.49097558203274583D;
            // 
            // xrTableCellBegSumPrice
            // 
            this.xrTableCellBegSumPrice.Dpi = 254F;
            this.xrTableCellBegSumPrice.Name = "xrTableCellBegSumPrice";
            this.xrTableCellBegSumPrice.StylePriority.UseTextAlignment = false;
            this.xrTableCellBegSumPrice.Text = "xrTableCellBegSumPrice";
            this.xrTableCellBegSumPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellBegSumPrice.Weight = 0.52166444591178884D;
            // 
            // xrTableCellIncCount
            // 
            this.xrTableCellIncCount.Dpi = 254F;
            this.xrTableCellIncCount.Name = "xrTableCellIncCount";
            this.xrTableCellIncCount.StylePriority.UseTextAlignment = false;
            this.xrTableCellIncCount.Text = "xrTableCellIncCount";
            this.xrTableCellIncCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellIncCount.Weight = 0.49097542365794355D;
            // 
            // xrTableCellIncSumPrice
            // 
            this.xrTableCellIncSumPrice.Dpi = 254F;
            this.xrTableCellIncSumPrice.Name = "xrTableCellIncSumPrice";
            this.xrTableCellIncSumPrice.StylePriority.UseTextAlignment = false;
            this.xrTableCellIncSumPrice.Text = "xrTableCellIncSumPrice";
            this.xrTableCellIncSumPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellIncSumPrice.Weight = 0.52165859234275258D;
            // 
            // xrTableCellExpCount
            // 
            this.xrTableCellExpCount.Dpi = 254F;
            this.xrTableCellExpCount.Name = "xrTableCellExpCount";
            this.xrTableCellExpCount.StylePriority.UseTextAlignment = false;
            this.xrTableCellExpCount.Text = "xrTableCellExpCount";
            this.xrTableCellExpCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellExpCount.Weight = 0.49097991442530264D;
            // 
            // xrTableCellExpSumPrice
            // 
            this.xrTableCellExpSumPrice.Dpi = 254F;
            this.xrTableCellExpSumPrice.Name = "xrTableCellExpSumPrice";
            this.xrTableCellExpSumPrice.StylePriority.UseTextAlignment = false;
            this.xrTableCellExpSumPrice.Text = "xrTableCellExpSumPrice";
            this.xrTableCellExpSumPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellExpSumPrice.Weight = 0.52166577682065229D;
            // 
            // xrTableCellEndBal
            // 
            this.xrTableCellEndBal.Dpi = 254F;
            this.xrTableCellEndBal.Name = "xrTableCellEndBal";
            this.xrTableCellEndBal.StylePriority.UseTextAlignment = false;
            this.xrTableCellEndBal.Text = "xrTableCellEndBal";
            this.xrTableCellEndBal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellEndBal.Weight = 0.49097183178857223D;
            // 
            // xrTableCellEndSumPrice
            // 
            this.xrTableCellEndSumPrice.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellEndSumPrice.Dpi = 254F;
            this.xrTableCellEndSumPrice.Name = "xrTableCellEndSumPrice";
            this.xrTableCellEndSumPrice.StylePriority.UseBorders = false;
            this.xrTableCellEndSumPrice.StylePriority.UseTextAlignment = false;
            this.xrTableCellEndSumPrice.Text = "Нийт үнэ";
            this.xrTableCellEndSumPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellEndSumPrice.Weight = 0.52166654109270738D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 73.54166F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 113.2512F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableHeader,
            this.xrTable2,
            this.xrTable3,
            this.xrLabelReportHeader,
            this.xrLabelCompany,
            this.xrLabel1,
            this.xrLabelStart,
            this.xrLabel4,
            this.xrLabelEnd,
            this.xrLabel2,
            this.xrLabelTreatmentDirector});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 527.019F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTableHeader
            // 
            this.xrTableHeader.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTableHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableHeader.Dpi = 254F;
            this.xrTableHeader.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableHeader.LocationFloat = new DevExpress.Utils.PointFloat(50.00009F, 382.0607F);
            this.xrTableHeader.Name = "xrTableHeader";
            this.xrTableHeader.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTableHeader.SizeF = new System.Drawing.SizeF(2720F, 63.5F);
            this.xrTableHeader.StylePriority.UseBorders = false;
            this.xrTableHeader.StylePriority.UseFont = false;
            this.xrTableHeader.StylePriority.UseTextAlignment = false;
            this.xrTableHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellMedicine,
            this.xrTableCellBeg,
            this.xrTableCellInc,
            this.xrTableCellExp,
            this.xrTableCellEnd});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCellMedicine
            // 
            this.xrTableCellMedicine.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellMedicine.Dpi = 254F;
            this.xrTableCellMedicine.Multiline = true;
            this.xrTableCellMedicine.Name = "xrTableCellMedicine";
            this.xrTableCellMedicine.StylePriority.UseBorders = false;
            this.xrTableCellMedicine.Text = "Бараа материал";
            this.xrTableCellMedicine.Weight = 2.1328173662284997D;
            // 
            // xrTableCellBeg
            // 
            this.xrTableCellBeg.Dpi = 254F;
            this.xrTableCellBeg.Name = "xrTableCellBeg";
            this.xrTableCellBeg.Text = "Эхний үлдэгдэл";
            this.xrTableCellBeg.Weight = 0.58712347339280491D;
            // 
            // xrTableCellInc
            // 
            this.xrTableCellInc.Dpi = 254F;
            this.xrTableCellInc.Name = "xrTableCellInc";
            this.xrTableCellInc.Text = "Орлого";
            this.xrTableCellInc.Weight = 0.39540917049849145D;
            // 
            // xrTableCellExp
            // 
            this.xrTableCellExp.Dpi = 254F;
            this.xrTableCellExp.Name = "xrTableCellExp";
            this.xrTableCellExp.Text = "Зарлага";
            this.xrTableCellExp.Weight = 0.39540989362840895D;
            // 
            // xrTableCellEnd
            // 
            this.xrTableCellEnd.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellEnd.Dpi = 254F;
            this.xrTableCellEnd.Name = "xrTableCellEnd";
            this.xrTableCellEnd.StylePriority.UseBorders = false;
            this.xrTableCellEnd.Text = "Эцсийн үлдэгдэл";
            this.xrTableCellEnd.Weight = 0.39541086384342133D;
            // 
            // xrTable2
            // 
            this.xrTable2.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(50.0001F, 445.5607F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2720F, 80F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCelluser,
            this.xrTableCellrole,
            this.xrTableCellwr,
            this.xrTableCellmed,
            this.xrTableCellsha,
            this.xrTableCelluni,
            this.xrTableCellser,
            this.xrTableCellbar,
            this.xrTableCellquant,
            this.xrTableCellvalid,
            this.xrTableCellcnt,
            this.xrTableCellpr,
            this.xrTableCellspr,
            this.xrTableCellocnt,
            this.xrTableCellopr,
            this.xrTableCellzcnt,
            this.xrTableCellzpr,
            this.xrTableCelllcnt,
            this.xrTableCelllpr});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCelluser
            // 
            this.xrTableCelluser.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCelluser.Dpi = 254F;
            this.xrTableCelluser.Multiline = true;
            this.xrTableCelluser.Name = "xrTableCelluser";
            this.xrTableCelluser.StylePriority.UseBorders = false;
            this.xrTableCelluser.Text = "Нэр";
            this.xrTableCelluser.Weight = 0.48303772456182026D;
            // 
            // xrTableCellrole
            // 
            this.xrTableCellrole.Dpi = 254F;
            this.xrTableCellrole.Multiline = true;
            this.xrTableCellrole.Name = "xrTableCellrole";
            this.xrTableCellrole.Text = "Эрх";
            this.xrTableCellrole.Weight = 0.25247784275771362D;
            // 
            // xrTableCellwr
            // 
            this.xrTableCellwr.Dpi = 254F;
            this.xrTableCellwr.Name = "xrTableCellwr";
            this.xrTableCellwr.Text = "Тасаг";
            this.xrTableCellwr.Weight = 0.33418152832694081D;
            // 
            // xrTableCellmed
            // 
            this.xrTableCellmed.Dpi = 254F;
            this.xrTableCellmed.Name = "xrTableCellmed";
            this.xrTableCellmed.Text = "Нэр";
            this.xrTableCellmed.Weight = 0.52143100991993263D;
            // 
            // xrTableCellsha
            // 
            this.xrTableCellsha.Dpi = 254F;
            this.xrTableCellsha.Name = "xrTableCellsha";
            this.xrTableCellsha.Text = "Хэлбэр";
            this.xrTableCellsha.Weight = 0.31522518890158885D;
            // 
            // xrTableCelluni
            // 
            this.xrTableCelluni.Dpi = 254F;
            this.xrTableCelluni.Name = "xrTableCelluni";
            this.xrTableCelluni.Text = "Тун хэмжээ";
            this.xrTableCelluni.Weight = 0.29852609789884349D;
            // 
            // xrTableCellser
            // 
            this.xrTableCellser.Dpi = 254F;
            this.xrTableCellser.Name = "xrTableCellser";
            this.xrTableCellser.Text = "Сериал";
            this.xrTableCellser.Weight = 0.32574373898952363D;
            // 
            // xrTableCellbar
            // 
            this.xrTableCellbar.Dpi = 254F;
            this.xrTableCellbar.Name = "xrTableCellbar";
            this.xrTableCellbar.Text = "Бар код";
            this.xrTableCellbar.Weight = 0.29429668630898265D;
            // 
            // xrTableCellquant
            // 
            this.xrTableCellquant.CanShrink = true;
            this.xrTableCellquant.Dpi = 254F;
            this.xrTableCellquant.Multiline = true;
            this.xrTableCellquant.Name = "xrTableCellquant";
            this.xrTableCellquant.Text = "Хэмжих нэгж";
            this.xrTableCellquant.Weight = 0.33832274654807526D;
            // 
            // xrTableCellvalid
            // 
            this.xrTableCellvalid.Dpi = 254F;
            this.xrTableCellvalid.Name = "xrTableCellvalid";
            this.xrTableCellvalid.Text = "Хүчинтэй хугацаа";
            this.xrTableCellvalid.Weight = 0.36712250702678617D;
            // 
            // xrTableCellcnt
            // 
            this.xrTableCellcnt.Dpi = 254F;
            this.xrTableCellcnt.Multiline = true;
            this.xrTableCellcnt.Name = "xrTableCellcnt";
            this.xrTableCellcnt.Text = "Тоо хэмжээ";
            this.xrTableCellcnt.Weight = 0.31733592192015941D;
            // 
            // xrTableCellpr
            // 
            this.xrTableCellpr.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellpr.Dpi = 254F;
            this.xrTableCellpr.Name = "xrTableCellpr";
            this.xrTableCellpr.StylePriority.UseBorders = false;
            this.xrTableCellpr.Text = "Үнэ";
            this.xrTableCellpr.Weight = 0.31733592181602588D;
            // 
            // xrTableCellspr
            // 
            this.xrTableCellspr.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellspr.Dpi = 254F;
            this.xrTableCellspr.Name = "xrTableCellspr";
            this.xrTableCellspr.StylePriority.UseBorders = false;
            this.xrTableCellspr.Text = "Нийт үнэ";
            this.xrTableCellspr.Weight = 0.33717076278039593D;
            // 
            // xrTableCellocnt
            // 
            this.xrTableCellocnt.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellocnt.Dpi = 254F;
            this.xrTableCellocnt.Name = "xrTableCellocnt";
            this.xrTableCellocnt.StylePriority.UseBorders = false;
            this.xrTableCellocnt.Text = "Тоо хэмжээ";
            this.xrTableCellocnt.Weight = 0.31733534146822118D;
            // 
            // xrTableCellopr
            // 
            this.xrTableCellopr.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellopr.Dpi = 254F;
            this.xrTableCellopr.Name = "xrTableCellopr";
            this.xrTableCellopr.StylePriority.UseBorders = false;
            this.xrTableCellopr.Text = "Нийт үнэ";
            this.xrTableCellopr.Weight = 0.33716815121526733D;
            // 
            // xrTableCellzcnt
            // 
            this.xrTableCellzcnt.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellzcnt.Dpi = 254F;
            this.xrTableCellzcnt.Name = "xrTableCellzcnt";
            this.xrTableCellzcnt.StylePriority.UseBorders = false;
            this.xrTableCellzcnt.Text = "Тоо хэмжээ";
            this.xrTableCellzcnt.Weight = 0.31733709010339867D;
            // 
            // xrTableCellzpr
            // 
            this.xrTableCellzpr.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellzpr.Dpi = 254F;
            this.xrTableCellzpr.Name = "xrTableCellzpr";
            this.xrTableCellzpr.StylePriority.UseBorders = false;
            this.xrTableCellzpr.Text = "Нийт үнэ";
            this.xrTableCellzpr.Weight = 0.33716872101894885D;
            // 
            // xrTableCelllcnt
            // 
            this.xrTableCelllcnt.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCelllcnt.Dpi = 254F;
            this.xrTableCelllcnt.Name = "xrTableCelllcnt";
            this.xrTableCelllcnt.StylePriority.UseBorders = false;
            this.xrTableCelllcnt.Text = "Тоо хэмжээ";
            this.xrTableCelllcnt.Weight = 0.317337082511622D;
            // 
            // xrTableCelllpr
            // 
            this.xrTableCelllpr.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCelllpr.Dpi = 254F;
            this.xrTableCelllpr.Name = "xrTableCelllpr";
            this.xrTableCelllpr.StylePriority.UseBorders = false;
            this.xrTableCelllpr.Text = "Нийт үнэ";
            this.xrTableCelllpr.Weight = 0.33717048821172219D;
            // 
            // xrTable3
            // 
            this.xrTable3.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Dpi = 254F;
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(8.074442E-05F, 382.0607F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable3.SizeF = new System.Drawing.SizeF(50F, 144.9583F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellNo});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.Weight = 1.0101620345165505D;
            // 
            // xrTableCellNo
            // 
            this.xrTableCellNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellNo.Dpi = 254F;
            this.xrTableCellNo.Multiline = true;
            this.xrTableCellNo.Name = "xrTableCellNo";
            this.xrTableCellNo.StylePriority.UseBorders = false;
            this.xrTableCellNo.Text = "№";
            this.xrTableCellNo.Weight = 1.058585616203942D;
            // 
            // xrLabelReportHeader
            // 
            this.xrLabelReportHeader.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrLabelReportHeader.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelReportHeader.Dpi = 254F;
            this.xrLabelReportHeader.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabelReportHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 237.0816F);
            this.xrLabelReportHeader.Name = "xrLabelReportHeader";
            this.xrLabelReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelReportHeader.SizeF = new System.Drawing.SizeF(2770F, 59.75F);
            this.xrLabelReportHeader.StylePriority.UseBorders = false;
            this.xrLabelReportHeader.StylePriority.UseFont = false;
            this.xrLabelReportHeader.Text = "ЗАХИАЛГА ТАЙЛАН";
            this.xrLabelReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelCompany
            // 
            this.xrLabelCompany.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelCompany.Dpi = 254F;
            this.xrLabelCompany.LocationFloat = new DevExpress.Utils.PointFloat(359.4167F, 46.16676F);
            this.xrLabelCompany.Name = "xrLabelCompany";
            this.xrLabelCompany.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelCompany.SizeF = new System.Drawing.SizeF(562F, 60F);
            this.xrLabelCompany.StylePriority.UseFont = false;
            this.xrLabelCompany.StylePriority.UseTextAlignment = false;
            this.xrLabelCompany.Text = "xrLabel1";
            this.xrLabelCompany.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(104.9791F, 46.16668F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(254.4375F, 59.99999F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Байгууллага : ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelStart
            // 
            this.xrLabelStart.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelStart.Dpi = 254F;
            this.xrLabelStart.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabelStart.LocationFloat = new DevExpress.Utils.PointFloat(2347.602F, 46.16676F);
            this.xrLabelStart.Name = "xrLabelStart";
            this.xrLabelStart.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelStart.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabelStart.StylePriority.UseFont = false;
            this.xrLabelStart.StylePriority.UseTextAlignment = false;
            this.xrLabelStart.Text = "xrLabel1";
            this.xrLabelStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(2037.602F, 106.1667F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Дуусах огноо :";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelEnd
            // 
            this.xrLabelEnd.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right;
            this.xrLabelEnd.Dpi = 254F;
            this.xrLabelEnd.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabelEnd.LocationFloat = new DevExpress.Utils.PointFloat(2347.602F, 106.1667F);
            this.xrLabelEnd.Name = "xrLabelEnd";
            this.xrLabelEnd.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelEnd.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabelEnd.StylePriority.UseFont = false;
            this.xrLabelEnd.StylePriority.UseTextAlignment = false;
            this.xrLabelEnd.Text = "xrLabel1";
            this.xrLabelEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(2037.601F, 46.16676F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Эхлэх огноо : ";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelTreatmentDirector
            // 
            this.xrLabelTreatmentDirector.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelTreatmentDirector.AutoWidth = true;
            this.xrLabelTreatmentDirector.Dpi = 254F;
            this.xrLabelTreatmentDirector.LocationFloat = new DevExpress.Utils.PointFloat(104.9791F, 106.1666F);
            this.xrLabelTreatmentDirector.Multiline = true;
            this.xrLabelTreatmentDirector.Name = "xrLabelTreatmentDirector";
            this.xrLabelTreatmentDirector.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelTreatmentDirector.SizeF = new System.Drawing.SizeF(965.5358F, 59.99996F);
            this.xrLabelTreatmentDirector.StylePriority.UseFont = false;
            this.xrLabelTreatmentDirector.StylePriority.UseTextAlignment = false;
            this.xrLabelTreatmentDirector.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabelTreatmentDirector.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelTreatmentSign,
            this.xrTable1});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 342.6245F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabelTreatmentSign
            // 
            this.xrLabelTreatmentSign.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelTreatmentSign.Dpi = 254F;
            this.xrLabelTreatmentSign.LocationFloat = new DevExpress.Utils.PointFloat(851.9645F, 187.7708F);
            this.xrLabelTreatmentSign.Multiline = true;
            this.xrLabelTreatmentSign.Name = "xrLabelTreatmentSign";
            this.xrLabelTreatmentSign.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelTreatmentSign.SizeF = new System.Drawing.SizeF(1105.765F, 59.99998F);
            this.xrLabelTreatmentSign.StylePriority.UseFont = false;
            this.xrLabelTreatmentSign.StylePriority.UseTextAlignment = false;
            this.xrLabelTreatmentSign.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabelTreatmentSign.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.SizeF = new System.Drawing.SizeF(2770F, 90F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCellTotalBegCount,
            this.xrTableCellTotalPrice,
            this.xrTableCellTotalBegSumPrice,
            this.xrTableCellTotalIncCount,
            this.xrTableCellTotalIncSumPrice,
            this.xrTableCellTotalExpCount,
            this.xrTableCellTotalExpSumPrice,
            this.xrTableCellTotalEndBal,
            this.xrTableCellTotalEndSumPrice});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.CanShrink = true;
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Multiline = true;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "Нийт дүн : ";
            this.xrTableCell24.Weight = 2.8034224462852677D;
            // 
            // xrTableCellTotalBegCount
            // 
            this.xrTableCellTotalBegCount.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalBegCount.Dpi = 254F;
            this.xrTableCellTotalBegCount.Multiline = true;
            this.xrTableCellTotalBegCount.Name = "xrTableCellTotalBegCount";
            this.xrTableCellTotalBegCount.StylePriority.UseBorders = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalBegCount.Summary = xrSummary2;
            this.xrTableCellTotalBegCount.Weight = 0.24378562918199759D;
            // 
            // xrTableCellTotalPrice
            // 
            this.xrTableCellTotalPrice.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalPrice.Dpi = 254F;
            this.xrTableCellTotalPrice.Multiline = true;
            this.xrTableCellTotalPrice.Name = "xrTableCellTotalPrice";
            this.xrTableCellTotalPrice.StylePriority.UseBorders = false;
            xrSummary3.FormatString = "{0:c2}";
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalPrice.Summary = xrSummary3;
            this.xrTableCellTotalPrice.Weight = 0.24378542965230321D;
            // 
            // xrTableCellTotalBegSumPrice
            // 
            this.xrTableCellTotalBegSumPrice.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalBegSumPrice.Dpi = 254F;
            this.xrTableCellTotalBegSumPrice.Name = "xrTableCellTotalBegSumPrice";
            this.xrTableCellTotalBegSumPrice.StylePriority.UseBorders = false;
            xrSummary4.FormatString = "{0:c2}";
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalBegSumPrice.Summary = xrSummary4;
            this.xrTableCellTotalBegSumPrice.Weight = 0.25902305782219148D;
            // 
            // xrTableCellTotalIncCount
            // 
            this.xrTableCellTotalIncCount.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalIncCount.Dpi = 254F;
            this.xrTableCellTotalIncCount.Name = "xrTableCellTotalIncCount";
            this.xrTableCellTotalIncCount.StylePriority.UseBorders = false;
            xrSummary5.FormatString = "{0:n2}";
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalIncCount.Summary = xrSummary5;
            this.xrTableCellTotalIncCount.Weight = 0.24378521667001629D;
            // 
            // xrTableCellTotalIncSumPrice
            // 
            this.xrTableCellTotalIncSumPrice.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalIncSumPrice.Dpi = 254F;
            this.xrTableCellTotalIncSumPrice.Name = "xrTableCellTotalIncSumPrice";
            this.xrTableCellTotalIncSumPrice.StylePriority.UseBorders = false;
            xrSummary6.FormatString = "{0:c2}";
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalIncSumPrice.Summary = xrSummary6;
            this.xrTableCellTotalIncSumPrice.Weight = 0.259021497389939D;
            // 
            // xrTableCellTotalExpCount
            // 
            this.xrTableCellTotalExpCount.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalExpCount.Dpi = 254F;
            this.xrTableCellTotalExpCount.Name = "xrTableCellTotalExpCount";
            this.xrTableCellTotalExpCount.StylePriority.UseBorders = false;
            xrSummary7.FormatString = "{0:n2}";
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalExpCount.Summary = xrSummary7;
            this.xrTableCellTotalExpCount.Weight = 0.24378585756196472D;
            // 
            // xrTableCellTotalExpSumPrice
            // 
            this.xrTableCellTotalExpSumPrice.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalExpSumPrice.Dpi = 254F;
            this.xrTableCellTotalExpSumPrice.Name = "xrTableCellTotalExpSumPrice";
            this.xrTableCellTotalExpSumPrice.StylePriority.UseBorders = false;
            xrSummary8.FormatString = "{0:c2}";
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalExpSumPrice.Summary = xrSummary8;
            this.xrTableCellTotalExpSumPrice.Weight = 0.25902272344378252D;
            // 
            // xrTableCellTotalEndBal
            // 
            this.xrTableCellTotalEndBal.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalEndBal.Dpi = 254F;
            this.xrTableCellTotalEndBal.Name = "xrTableCellTotalEndBal";
            this.xrTableCellTotalEndBal.StylePriority.UseBorders = false;
            xrSummary9.FormatString = "{0:n2}";
            xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalEndBal.Summary = xrSummary9;
            this.xrTableCellTotalEndBal.Weight = 0.24378407421098744D;
            // 
            // xrTableCellTotalEndSumPrice
            // 
            this.xrTableCellTotalEndSumPrice.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalEndSumPrice.Dpi = 254F;
            this.xrTableCellTotalEndSumPrice.Name = "xrTableCellTotalEndSumPrice";
            this.xrTableCellTotalEndSumPrice.StylePriority.UseBorders = false;
            xrSummary10.FormatString = "{0:c2}";
            xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalEndSumPrice.Summary = xrSummary10;
            this.xrTableCellTotalEndSumPrice.Weight = 0.25902355938962068D;
            // 
            // filter
            // 
            this.filter.Description = "filter";
            this.filter.Name = "filter";
            this.filter.Visible = false;
            // 
            // BalMultiReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 74, 113);
            this.PageHeight = 2100;
            this.PageWidth = 2970;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.filter});
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "15.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTableDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.Parameters.Parameter filter;
        private DevExpress.XtraReports.UI.XRLabel xrLabelStart;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabelEnd;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCompany;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTreatmentDirector;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCelluser;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellrole;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellmed;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellsha;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellquant;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellvalid;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellcnt;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellpr;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellspr;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellocnt;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellopr;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellzcnt;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellzpr;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCelllcnt;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCelllpr;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellNo;
        private DevExpress.XtraReports.UI.XRTable xrTableDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellNumber;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellFullName;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellRoleName;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellMedicineName;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellShape;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellUnit;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellValidDate;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellBegBal;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellPrice;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellBegSumPrice;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIncCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIncSumPrice;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellExpCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellExpSumPrice;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellEndBal;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellEndSumPrice;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalBegCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalPrice;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalBegSumPrice;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalIncCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalIncSumPrice;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalExpCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalExpSumPrice;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalEndBal;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalEndSumPrice;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCelluni;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellser;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellbar;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellSerial;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellBarCode;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellQuantity;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellwarehouse;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellwr;
        private DevExpress.XtraReports.UI.XRTable xrTableHeader;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellMedicine;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellBeg;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellInc;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellExp;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellEnd;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTreatmentSign;
    }
}
