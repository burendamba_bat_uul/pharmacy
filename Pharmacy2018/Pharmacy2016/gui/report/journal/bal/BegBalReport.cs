﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Pharmacy2016.util;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.report;

namespace Pharmacy2016.gui.report.journal.bal
{
    public partial class BegBalReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

        private static BegBalReport INSTANCE = new BegBalReport();

        public static BegBalReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private BegBalReport()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;

            DataSource = JournalDataSet.Instance;
            DataAdapter = JournalDataSet.BegBalTableAdapter;
            DataMember = JournalDataSet.Instance.BegBal.ToString();

            initBinding();
        }

        private void initBinding()
        {
            this.xrTableCellName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".userName", "")});
            this.xrTableCellWarehouse.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".warehouseName", "")});
            this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MedicineName", "")});
            this.xrTableCellMedicineTypeName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MedicineTypeName", "")});
            this.xrTableCellSerial.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Serial", "")});
            this.xrTableCellUnitType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UnitType", "")});
            this.xrTableCellQuantity.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Quantity", "")});
            this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Price", ReportUtil.DisplayFormatCurrency)});
            this.xrTableCellBal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
                        
            this.xrTableCellTotalSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
        }

        #endregion

        #region Тайлан харуулах функц.

        public void initAndShow(int type, string filter)
        {
            try
            {
                if (!isInit)
                {
                    initForm();
                }

                ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                DateTime now = DateTime.Now;

                xrLabelCompany.Text = HospitalUtil.getHospitalName();
                //xrLabelUserName.Text = userName;
                //xrLabelWarehouseName.Text = warehouseName;

                xrLabelYear.Text = now.Year.ToString();
                xrLabelDate.Text = now.ToString("yyyy-MM-dd");

                FilterString = filter;

                CreateDocument(true);

                ProgramUtil.closeWaitDialog();
                ReportViewForm.Instance.initAndShow(INSTANCE);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        #endregion

    }
}
