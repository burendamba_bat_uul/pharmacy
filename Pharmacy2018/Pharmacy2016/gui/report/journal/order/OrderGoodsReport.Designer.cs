﻿namespace Pharmacy2016.report.journal.order
{
    partial class OrderGoodsReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellMedName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCelllat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellMtype = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellShapes = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellUni = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellQuant = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDescript = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabelPageHeader = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTreatmentDirector = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelWareHouse = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCompany = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellMedicineName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellLatinName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellMedicineType = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellShape = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellValidDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellUnitType = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellQuantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellConfirmCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellGiveCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabelexpRole = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelConfRole = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelUserRole = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelexpUser = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelConfUser = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelSignatureUserName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCelltype = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTableCellcn = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellconc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellgcnt = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalConfirmCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalGiveCount = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2,
            this.xrLabelPageHeader,
            this.xrLabel9,
            this.xrLabelTreatmentDirector});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 434.4775F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 294.4775F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2840.765F, 140F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCellMedName,
            this.xrTableCelllat,
            this.xrTableCellMtype,
            this.xrTableCellShapes,
            this.xrTableCellDate,
            this.xrTableCellUni,
            this.xrTableCellQuant,
            this.xrTableCellDescript,
            this.xrTableCellcn,
            this.xrTableCellconc,
            this.xrTableCellgcnt});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "№";
            this.xrTableCell1.Weight = 0.1097836507741199D;
            // 
            // xrTableCellMedName
            // 
            this.xrTableCellMedName.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellMedName.Dpi = 254F;
            this.xrTableCellMedName.Multiline = true;
            this.xrTableCellMedName.Name = "xrTableCellMedName";
            this.xrTableCellMedName.StylePriority.UseBorders = false;
            this.xrTableCellMedName.Text = "Эмийн нэр";
            this.xrTableCellMedName.Weight = 0.31740157497395272D;
            // 
            // xrTableCelllat
            // 
            this.xrTableCelllat.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCelllat.Dpi = 254F;
            this.xrTableCelllat.Name = "xrTableCelllat";
            this.xrTableCelllat.StylePriority.UseBorders = false;
            this.xrTableCelllat.Text = "Эмийн О.У нэр";
            this.xrTableCelllat.Weight = 0.31740197030284656D;
            // 
            // xrTableCellMtype
            // 
            this.xrTableCellMtype.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellMtype.Dpi = 254F;
            this.xrTableCellMtype.Name = "xrTableCellMtype";
            this.xrTableCellMtype.StylePriority.UseBorders = false;
            this.xrTableCellMtype.Text = "Ангилал";
            this.xrTableCellMtype.Weight = 0.26450101931226727D;
            // 
            // xrTableCellShapes
            // 
            this.xrTableCellShapes.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellShapes.Dpi = 254F;
            this.xrTableCellShapes.Name = "xrTableCellShapes";
            this.xrTableCellShapes.StylePriority.UseBorders = false;
            this.xrTableCellShapes.Text = "Хэлбэр";
            this.xrTableCellShapes.Weight = 0.26450134192544777D;
            // 
            // xrTableCellDate
            // 
            this.xrTableCellDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellDate.Dpi = 254F;
            this.xrTableCellDate.Name = "xrTableCellDate";
            this.xrTableCellDate.StylePriority.UseBorders = false;
            this.xrTableCellDate.Text = "Хүчинтэй хугацаа";
            this.xrTableCellDate.Weight = 0.26450163251564018D;
            // 
            // xrTableCellUni
            // 
            this.xrTableCellUni.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellUni.CanShrink = true;
            this.xrTableCellUni.Dpi = 254F;
            this.xrTableCellUni.Multiline = true;
            this.xrTableCellUni.Name = "xrTableCellUni";
            this.xrTableCellUni.StylePriority.UseBorders = false;
            this.xrTableCellUni.Text = "Тун хэмжээ";
            this.xrTableCellUni.Weight = 0.26450121810541033D;
            // 
            // xrTableCellQuant
            // 
            this.xrTableCellQuant.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellQuant.Dpi = 254F;
            this.xrTableCellQuant.Name = "xrTableCellQuant";
            this.xrTableCellQuant.StylePriority.UseBorders = false;
            this.xrTableCellQuant.Text = "Хэмжих нэгж";
            this.xrTableCellQuant.Weight = 0.26450134049474872D;
            // 
            // xrTableCellDescript
            // 
            this.xrTableCellDescript.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellDescript.Dpi = 254F;
            this.xrTableCellDescript.Name = "xrTableCellDescript";
            this.xrTableCellDescript.StylePriority.UseBorders = false;
            this.xrTableCellDescript.Text = "Тайлбар";
            this.xrTableCellDescript.Weight = 0.30365312458269078D;
            // 
            // xrLabelPageHeader
            // 
            this.xrLabelPageHeader.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrLabelPageHeader.Dpi = 254F;
            this.xrLabelPageHeader.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold);
            this.xrLabelPageHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 45F);
            this.xrLabelPageHeader.Name = "xrLabelPageHeader";
            this.xrLabelPageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelPageHeader.SizeF = new System.Drawing.SizeF(2842F, 58.41999F);
            this.xrLabelPageHeader.StylePriority.UseFont = false;
            this.xrLabelPageHeader.StylePriority.UseTextAlignment = false;
            this.xrLabelPageHeader.Text = "Захиалгын хуудас";
            this.xrLabelPageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel9
            // 
            this.xrLabel9.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(0.0002422333F, 171.6092F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(554.2918F, 59.99999F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Батлав: Эмчилгээ эрхэлсэн орлогч";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelTreatmentDirector
            // 
            this.xrLabelTreatmentDirector.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelTreatmentDirector.Dpi = 254F;
            this.xrLabelTreatmentDirector.LocationFloat = new DevExpress.Utils.PointFloat(554.2922F, 171.6092F);
            this.xrLabelTreatmentDirector.Multiline = true;
            this.xrLabelTreatmentDirector.Name = "xrLabelTreatmentDirector";
            this.xrLabelTreatmentDirector.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelTreatmentDirector.SizeF = new System.Drawing.SizeF(750F, 60F);
            this.xrLabelTreatmentDirector.StylePriority.UseFont = false;
            this.xrLabelTreatmentDirector.StylePriority.UseTextAlignment = false;
            this.xrLabelTreatmentDirector.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabelTreatmentDirector.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0.0002422333F, 129.9792F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Тасаг : ";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelWareHouse
            // 
            this.xrLabelWareHouse.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelWareHouse.Dpi = 254F;
            this.xrLabelWareHouse.LocationFloat = new DevExpress.Utils.PointFloat(310.0001F, 129.9792F);
            this.xrLabelWareHouse.Name = "xrLabelWareHouse";
            this.xrLabelWareHouse.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelWareHouse.SizeF = new System.Drawing.SizeF(561.9999F, 60F);
            this.xrLabelWareHouse.StylePriority.UseFont = false;
            this.xrLabelWareHouse.StylePriority.UseTextAlignment = false;
            this.xrLabelWareHouse.Text = "xrLabel1";
            this.xrLabelWareHouse.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 69.97917F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Байгууллага : ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelCompany
            // 
            this.xrLabelCompany.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelCompany.Dpi = 254F;
            this.xrLabelCompany.LocationFloat = new DevExpress.Utils.PointFloat(310F, 69.97926F);
            this.xrLabelCompany.Name = "xrLabelCompany";
            this.xrLabelCompany.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelCompany.SizeF = new System.Drawing.SizeF(562F, 60F);
            this.xrLabelCompany.StylePriority.UseFont = false;
            this.xrLabelCompany.StylePriority.UseTextAlignment = false;
            this.xrLabelCompany.Text = "xrLabel1";
            this.xrLabelCompany.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelDate
            // 
            this.xrLabelDate.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelDate.Dpi = 254F;
            this.xrLabelDate.LocationFloat = new DevExpress.Utils.PointFloat(2405.479F, 69.97917F);
            this.xrLabelDate.Name = "xrLabelDate";
            this.xrLabelDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelDate.SizeF = new System.Drawing.SizeF(434.3542F, 60F);
            this.xrLabelDate.StylePriority.UseFont = false;
            this.xrLabelDate.StylePriority.UseTextAlignment = false;
            this.xrLabelDate.Text = "xrLabel1";
            this.xrLabelDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(2047.854F, 69.97917F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(357.6248F, 60F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Огноо : ";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableDetail
            // 
            this.xrTableDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableDetail.Dpi = 254F;
            this.xrTableDetail.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableDetail.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTableDetail.Name = "xrTableDetail";
            this.xrTableDetail.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTableDetail.SizeF = new System.Drawing.SizeF(2839.833F, 70F);
            this.xrTableDetail.StylePriority.UseBorders = false;
            this.xrTableDetail.StylePriority.UseFont = false;
            this.xrTableDetail.StylePriority.UsePadding = false;
            this.xrTableDetail.StylePriority.UseTextAlignment = false;
            this.xrTableDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellNumber,
            this.xrTableCellMedicineName,
            this.xrTableCellLatinName,
            this.xrTableCellMedicineType,
            this.xrTableCellShape,
            this.xrTableCellValidDate,
            this.xrTableCellUnitType,
            this.xrTableCellQuantity,
            this.xrTableCellDesc,
            this.xrTableCellCount,
            this.xrTableCellConfirmCount,
            this.xrTableCellGiveCount});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCellNumber
            // 
            this.xrTableCellNumber.Dpi = 254F;
            this.xrTableCellNumber.Name = "xrTableCellNumber";
            this.xrTableCellNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrTableCellNumber.StylePriority.UsePadding = false;
            xrSummary1.FormatString = "{0:#}";
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellNumber.Summary = xrSummary1;
            this.xrTableCellNumber.Weight = 0.15848743504516427D;
            // 
            // xrTableCellMedicineName
            // 
            this.xrTableCellMedicineName.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellMedicineName.Dpi = 254F;
            this.xrTableCellMedicineName.Name = "xrTableCellMedicineName";
            this.xrTableCellMedicineName.StylePriority.UseBorders = false;
            this.xrTableCellMedicineName.Text = "Эм, эмнэлгийн хэрэгсэл\r\n";
            this.xrTableCellMedicineName.Weight = 0.45821185158729338D;
            // 
            // xrTableCellLatinName
            // 
            this.xrTableCellLatinName.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellLatinName.Dpi = 254F;
            this.xrTableCellLatinName.Name = "xrTableCellLatinName";
            this.xrTableCellLatinName.StylePriority.UseBorders = false;
            this.xrTableCellLatinName.Text = " ";
            this.xrTableCellLatinName.Weight = 0.45821221980177584D;
            // 
            // xrTableCellMedicineType
            // 
            this.xrTableCellMedicineType.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellMedicineType.Dpi = 254F;
            this.xrTableCellMedicineType.Name = "xrTableCellMedicineType";
            this.xrTableCellMedicineType.StylePriority.UseBorders = false;
            this.xrTableCellMedicineType.Text = "xrTableCellMedicineType";
            this.xrTableCellMedicineType.Weight = 0.38184298806889955D;
            // 
            // xrTableCellShape
            // 
            this.xrTableCellShape.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellShape.Dpi = 254F;
            this.xrTableCellShape.Name = "xrTableCellShape";
            this.xrTableCellShape.StylePriority.UseBorders = false;
            this.xrTableCellShape.Text = "xrTableCellShape";
            this.xrTableCellShape.Weight = 0.38184308129235556D;
            // 
            // xrTableCellValidDate
            // 
            this.xrTableCellValidDate.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellValidDate.Dpi = 254F;
            this.xrTableCellValidDate.Name = "xrTableCellValidDate";
            this.xrTableCellValidDate.StylePriority.UseBorders = false;
            this.xrTableCellValidDate.Text = "xrTableCellValidDate";
            this.xrTableCellValidDate.Weight = 0.38184364063309079D;
            // 
            // xrTableCellUnitType
            // 
            this.xrTableCellUnitType.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellUnitType.Dpi = 254F;
            this.xrTableCellUnitType.Name = "xrTableCellUnitType";
            this.xrTableCellUnitType.StylePriority.UseBorders = false;
            this.xrTableCellUnitType.StylePriority.UseTextAlignment = false;
            this.xrTableCellUnitType.Text = "Тун хэмжээ";
            this.xrTableCellUnitType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCellUnitType.Weight = 0.38184290819330319D;
            // 
            // xrTableCellQuantity
            // 
            this.xrTableCellQuantity.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellQuantity.Dpi = 254F;
            this.xrTableCellQuantity.Name = "xrTableCellQuantity";
            this.xrTableCellQuantity.StylePriority.UseBorders = false;
            this.xrTableCellQuantity.Text = "Хүчинтэй хугацаа";
            this.xrTableCellQuantity.Weight = 0.38184344944512866D;
            // 
            // xrTableCellDesc
            // 
            this.xrTableCellDesc.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellDesc.Dpi = 254F;
            this.xrTableCellDesc.Name = "xrTableCellDesc";
            this.xrTableCellDesc.StylePriority.UseBorders = false;
            this.xrTableCellDesc.Text = "xrTableCellDesc";
            this.xrTableCellDesc.Weight = 0.43836421987667562D;
            // 
            // xrTableCellCount
            // 
            this.xrTableCellCount.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellCount.CanPublish = false;
            this.xrTableCellCount.Dpi = 254F;
            this.xrTableCellCount.Name = "xrTableCellCount";
            this.xrTableCellCount.StylePriority.UseBorders = false;
            this.xrTableCellCount.StylePriority.UseTextAlignment = false;
            this.xrTableCellCount.Text = "xrTableCellCount";
            this.xrTableCellCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellCount.Weight = 0.30547535787974461D;
            // 
            // xrTableCellConfirmCount
            // 
            this.xrTableCellConfirmCount.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellConfirmCount.CanPublish = false;
            this.xrTableCellConfirmCount.Dpi = 254F;
            this.xrTableCellConfirmCount.Name = "xrTableCellConfirmCount";
            this.xrTableCellConfirmCount.StylePriority.UseBorders = false;
            this.xrTableCellConfirmCount.StylePriority.UseTextAlignment = false;
            this.xrTableCellConfirmCount.Text = "xrTableCellConfirmCount";
            this.xrTableCellConfirmCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellConfirmCount.Weight = 0.30547573098120967D;
            // 
            // xrTableCellGiveCount
            // 
            this.xrTableCellGiveCount.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellGiveCount.CanPublish = false;
            this.xrTableCellGiveCount.Dpi = 254F;
            this.xrTableCellGiveCount.Name = "xrTableCellGiveCount";
            this.xrTableCellGiveCount.StylePriority.UseBorders = false;
            this.xrTableCellGiveCount.StylePriority.UseTextAlignment = false;
            this.xrTableCellGiveCount.Text = "xrTableCellGiveCount";
            this.xrTableCellGiveCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellGiveCount.Weight = 0.30405201896450718D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 64F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 64F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabelCompany,
            this.xrLabelWareHouse,
            this.xrLabel2,
            this.xrLabel5,
            this.xrLabelDate});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 189.9792F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelexpRole,
            this.xrLabelConfRole,
            this.xrLabelUserRole,
            this.xrLabelexpUser,
            this.xrLabelConfUser,
            this.xrLabelSignatureUserName});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 342.3644F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabelexpRole
            // 
            this.xrLabelexpRole.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelexpRole.Dpi = 254F;
            this.xrLabelexpRole.LocationFloat = new DevExpress.Utils.PointFloat(650.834F, 211.38F);
            this.xrLabelexpRole.Name = "xrLabelexpRole";
            this.xrLabelexpRole.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelexpRole.SizeF = new System.Drawing.SizeF(500.0001F, 59.99998F);
            this.xrLabelexpRole.StylePriority.UseFont = false;
            this.xrLabelexpRole.StylePriority.UseTextAlignment = false;
            this.xrLabelexpRole.Text = "Олгосон";
            this.xrLabelexpRole.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelConfRole
            // 
            this.xrLabelConfRole.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelConfRole.Dpi = 254F;
            this.xrLabelConfRole.LocationFloat = new DevExpress.Utils.PointFloat(650.834F, 151.3801F);
            this.xrLabelConfRole.Name = "xrLabelConfRole";
            this.xrLabelConfRole.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelConfRole.SizeF = new System.Drawing.SizeF(500.0001F, 59.99998F);
            this.xrLabelConfRole.StylePriority.UseFont = false;
            this.xrLabelConfRole.StylePriority.UseTextAlignment = false;
            this.xrLabelConfRole.Text = "Зөвшөөрсөн";
            this.xrLabelConfRole.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelUserRole
            // 
            this.xrLabelUserRole.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelUserRole.Dpi = 254F;
            this.xrLabelUserRole.LocationFloat = new DevExpress.Utils.PointFloat(650.834F, 91.38008F);
            this.xrLabelUserRole.Name = "xrLabelUserRole";
            this.xrLabelUserRole.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelUserRole.SizeF = new System.Drawing.SizeF(500F, 60F);
            this.xrLabelUserRole.StylePriority.UseFont = false;
            this.xrLabelUserRole.StylePriority.UseTextAlignment = false;
            this.xrLabelUserRole.Text = "Захиалсан";
            this.xrLabelUserRole.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelexpUser
            // 
            this.xrLabelexpUser.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelexpUser.Dpi = 254F;
            this.xrLabelexpUser.LocationFloat = new DevExpress.Utils.PointFloat(1150.835F, 211.38F);
            this.xrLabelexpUser.Multiline = true;
            this.xrLabelexpUser.Name = "xrLabelexpUser";
            this.xrLabelexpUser.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelexpUser.SizeF = new System.Drawing.SizeF(750F, 59.99997F);
            this.xrLabelexpUser.StylePriority.UseFont = false;
            this.xrLabelexpUser.StylePriority.UseTextAlignment = false;
            this.xrLabelexpUser.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabelexpUser.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelConfUser
            // 
            this.xrLabelConfUser.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelConfUser.Dpi = 254F;
            this.xrLabelConfUser.LocationFloat = new DevExpress.Utils.PointFloat(1150.834F, 151.3801F);
            this.xrLabelConfUser.Multiline = true;
            this.xrLabelConfUser.Name = "xrLabelConfUser";
            this.xrLabelConfUser.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelConfUser.SizeF = new System.Drawing.SizeF(750F, 59.99997F);
            this.xrLabelConfUser.StylePriority.UseFont = false;
            this.xrLabelConfUser.StylePriority.UseTextAlignment = false;
            this.xrLabelConfUser.Text = "................................................../                              " +
    "/\r\n";
            this.xrLabelConfUser.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelSignatureUserName
            // 
            this.xrLabelSignatureUserName.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelSignatureUserName.Dpi = 254F;
            this.xrLabelSignatureUserName.LocationFloat = new DevExpress.Utils.PointFloat(1150.835F, 91.38008F);
            this.xrLabelSignatureUserName.Multiline = true;
            this.xrLabelSignatureUserName.Name = "xrLabelSignatureUserName";
            this.xrLabelSignatureUserName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelSignatureUserName.SizeF = new System.Drawing.SizeF(749.9994F, 59.99997F);
            this.xrLabelSignatureUserName.StylePriority.UseFont = false;
            this.xrLabelSignatureUserName.StylePriority.UseTextAlignment = false;
            this.xrLabelSignatureUserName.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabelSignatureUserName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(8.971604E-05F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.SizeF = new System.Drawing.SizeF(2839.833F, 90F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCelltype,
            this.xrTableCellTotalCount,
            this.xrTableCellTotalConfirmCount,
            this.xrTableCellTotalGiveCount});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.CanShrink = true;
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.xrTableCell24.Multiline = true;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Weight = 0.17250533588839617D;
            // 
            // xrTableCelltype
            // 
            this.xrTableCelltype.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCelltype.Dpi = 254F;
            this.xrTableCelltype.Font = new System.Drawing.Font("Times New Roman", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.xrTableCelltype.Name = "xrTableCelltype";
            this.xrTableCelltype.StylePriority.UseBorders = false;
            this.xrTableCelltype.StylePriority.UseFont = false;
            this.xrTableCelltype.StylePriority.UseTextAlignment = false;
            this.xrTableCelltype.Text = "Нийт :";
            this.xrTableCelltype.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCelltype.Weight = 3.5526920714884076D;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportFooter1});
            this.DetailReport.Dpi = 254F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableDetail});
            this.Detail1.Dpi = 254F;
            this.Detail1.HeightF = 70F;
            this.Detail1.Name = "Detail1";
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.ReportFooter1.Dpi = 254F;
            this.ReportFooter1.HeightF = 90F;
            this.ReportFooter1.Name = "ReportFooter1";
            // 
            // xrTableCellcn
            // 
            this.xrTableCellcn.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellcn.Dpi = 254F;
            this.xrTableCellcn.Name = "xrTableCellcn";
            this.xrTableCellcn.StylePriority.UseBorders = false;
            this.xrTableCellcn.Text = "Захиалсан тоо";
            this.xrTableCellcn.Weight = 0.21160156757882975D;
            // 
            // xrTableCellconc
            // 
            this.xrTableCellconc.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellconc.Dpi = 254F;
            this.xrTableCellconc.Name = "xrTableCellconc";
            this.xrTableCellconc.StylePriority.UseBorders = false;
            this.xrTableCellconc.Text = "Зөвшөөрсөн тоо";
            this.xrTableCellconc.Weight = 0.21160150300315625D;
            // 
            // xrTableCellgcnt
            // 
            this.xrTableCellgcnt.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellgcnt.Dpi = 254F;
            this.xrTableCellgcnt.Name = "xrTableCellgcnt";
            this.xrTableCellgcnt.StylePriority.UseBorders = false;
            this.xrTableCellgcnt.Text = "Олгосон тоо";
            this.xrTableCellgcnt.Weight = 0.21160150300315625D;
            // 
            // xrTableCellTotalCount
            // 
            this.xrTableCellTotalCount.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalCount.Dpi = 254F;
            this.xrTableCellTotalCount.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellTotalCount.Name = "xrTableCellTotalCount";
            this.xrTableCellTotalCount.StylePriority.UseBorders = false;
            this.xrTableCellTotalCount.StylePriority.UseFont = false;
            this.xrTableCellTotalCount.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalCount.Summary = xrSummary2;
            this.xrTableCellTotalCount.Text = "xrTableCellTotalCount";
            this.xrTableCellTotalCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellTotalCount.Weight = 0.33249296434574083D;
            // 
            // xrTableCellTotalConfirmCount
            // 
            this.xrTableCellTotalConfirmCount.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalConfirmCount.Dpi = 254F;
            this.xrTableCellTotalConfirmCount.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellTotalConfirmCount.Name = "xrTableCellTotalConfirmCount";
            this.xrTableCellTotalConfirmCount.StylePriority.UseBorders = false;
            this.xrTableCellTotalConfirmCount.StylePriority.UseFont = false;
            this.xrTableCellTotalConfirmCount.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalConfirmCount.Summary = xrSummary3;
            this.xrTableCellTotalConfirmCount.Text = "xrTableCellTotalConfirmCount";
            this.xrTableCellTotalConfirmCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellTotalConfirmCount.Weight = 0.3324941819729843D;
            // 
            // xrTableCellTotalGiveCount
            // 
            this.xrTableCellTotalGiveCount.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalGiveCount.Dpi = 254F;
            this.xrTableCellTotalGiveCount.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellTotalGiveCount.Name = "xrTableCellTotalGiveCount";
            this.xrTableCellTotalGiveCount.StylePriority.UseBorders = false;
            this.xrTableCellTotalGiveCount.StylePriority.UseFont = false;
            this.xrTableCellTotalGiveCount.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:n2}";
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalGiveCount.Summary = xrSummary4;
            this.xrTableCellTotalGiveCount.Text = "xrTableCellTotalGiveCount";
            this.xrTableCellTotalGiveCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellTotalGiveCount.Weight = 0.3309434748073603D;
            // 
            // OrderGoodsReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter,
            this.DetailReport});
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(64, 64, 64, 64);
            this.PageHeight = 2100;
            this.PageWidth = 2970;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "15.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabelPageHeader;
        private DevExpress.XtraReports.UI.XRTable xrTableDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellNumber;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellMedicineName;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellLatinName;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellUnitType;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellQuantity;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCompany;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellMedName;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellUni;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellQuant;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabelDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabelSignatureUserName;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTreatmentDirector;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCelllat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabelWareHouse;
        private DevExpress.XtraReports.UI.XRLabel xrLabelConfUser;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellMtype;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellShapes;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellDate;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellMedicineType;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellShape;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellValidDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabelexpRole;
        private DevExpress.XtraReports.UI.XRLabel xrLabelConfRole;
        private DevExpress.XtraReports.UI.XRLabel xrLabelexpUser;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCelltype;
        private DevExpress.XtraReports.UI.XRLabel xrLabelUserRole;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellDesc;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellConfirmCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellGiveCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellDescript;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellcn;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellconc;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellgcnt;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalConfirmCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalGiveCount;
    }
}
