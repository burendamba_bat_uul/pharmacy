﻿namespace Pharmacy2016.report.journal.order
{
    partial class OrderDocReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelWareHouse = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCompany = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelExpUserName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelUserName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableDetail = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellMedicineName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellLatinName = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellUnitType = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellQuantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellConfirmCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellGiveCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabelPageHeader = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelSignatureUserName = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelRole = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTreatmentDirector = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalConfirmCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTotalGiveCount = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrLabelWareHouse,
            this.xrLabel1,
            this.xrLabelCompany,
            this.xrLabel4,
            this.xrLabelExpUserName,
            this.xrTable2,
            this.xrLabel3,
            this.xrLabelUserName,
            this.xrLabelDate,
            this.xrLabel5});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 355.1025F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 120F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Тасаг : ";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelWareHouse
            // 
            this.xrLabelWareHouse.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelWareHouse.Dpi = 254F;
            this.xrLabelWareHouse.LocationFloat = new DevExpress.Utils.PointFloat(309.9999F, 120F);
            this.xrLabelWareHouse.Name = "xrLabelWareHouse";
            this.xrLabelWareHouse.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelWareHouse.SizeF = new System.Drawing.SizeF(561.9999F, 60F);
            this.xrLabelWareHouse.StylePriority.UseFont = false;
            this.xrLabelWareHouse.StylePriority.UseTextAlignment = false;
            this.xrLabelWareHouse.Text = "xrLabel1";
            this.xrLabelWareHouse.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Байгууллага : ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelCompany
            // 
            this.xrLabelCompany.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelCompany.Dpi = 254F;
            this.xrLabelCompany.LocationFloat = new DevExpress.Utils.PointFloat(310F, 8.074442E-05F);
            this.xrLabelCompany.Name = "xrLabelCompany";
            this.xrLabelCompany.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelCompany.SizeF = new System.Drawing.SizeF(562F, 60F);
            this.xrLabelCompany.StylePriority.UseFont = false;
            this.xrLabelCompany.StylePriority.UseTextAlignment = false;
            this.xrLabelCompany.Text = "xrLabel1";
            this.xrLabelCompany.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(1180.021F, 60.00005F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(357.6251F, 59.99998F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Захиалга өгсөн :";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelExpUserName
            // 
            this.xrLabelExpUserName.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelExpUserName.Dpi = 254F;
            this.xrLabelExpUserName.LocationFloat = new DevExpress.Utils.PointFloat(1537.646F, 60.00005F);
            this.xrLabelExpUserName.Name = "xrLabelExpUserName";
            this.xrLabelExpUserName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelExpUserName.SizeF = new System.Drawing.SizeF(434.354F, 59.99999F);
            this.xrLabelExpUserName.StylePriority.UseFont = false;
            this.xrLabelExpUserName.StylePriority.UseTextAlignment = false;
            this.xrLabelExpUserName.Text = "xrLabel1";
            this.xrLabelExpUserName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 254F;
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 215.1025F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1972F, 140F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell7,
            this.xrTableCell5,
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "№";
            this.xrTableCell1.Weight = 0.11162706855799577D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "Эмийн нэр";
            this.xrTableCell7.Weight = 0.62015036956162817D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Эмийн олон улсын нэршил";
            this.xrTableCell5.Weight = 0.62015040504691776D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.CanShrink = true;
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Multiline = true;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "Тун хэмжээ";
            this.xrTableCell13.Weight = 0.31007519577523029D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "Хэмжих нэгж";
            this.xrTableCell14.Weight = 0.23255639427321734D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "Захиалгын тоо";
            this.xrTableCell2.Weight = 0.38759418554019975D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "Зөвшөөрсөн тоо";
            this.xrTableCell3.Weight = 0.3875937943509149D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.Text = "Олгосон тоо";
            this.xrTableCell4.Weight = 0.38759398360579306D;
            // 
            // xrLabel3
            // 
            this.xrLabel3.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(4.037221E-05F, 60.00005F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Эд хариуцагч : ";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelUserName
            // 
            this.xrLabelUserName.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelUserName.Dpi = 254F;
            this.xrLabelUserName.LocationFloat = new DevExpress.Utils.PointFloat(310F, 60.00005F);
            this.xrLabelUserName.Name = "xrLabelUserName";
            this.xrLabelUserName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelUserName.SizeF = new System.Drawing.SizeF(561.9999F, 60F);
            this.xrLabelUserName.StylePriority.UseFont = false;
            this.xrLabelUserName.StylePriority.UseTextAlignment = false;
            this.xrLabelUserName.Text = "xrLabel1";
            this.xrLabelUserName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelDate
            // 
            this.xrLabelDate.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelDate.Dpi = 254F;
            this.xrLabelDate.LocationFloat = new DevExpress.Utils.PointFloat(1537.646F, 0F);
            this.xrLabelDate.Name = "xrLabelDate";
            this.xrLabelDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelDate.SizeF = new System.Drawing.SizeF(434.3542F, 60F);
            this.xrLabelDate.StylePriority.UseFont = false;
            this.xrLabelDate.StylePriority.UseTextAlignment = false;
            this.xrLabelDate.Text = "xrLabel1";
            this.xrLabelDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(1180.021F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(357.6248F, 60F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Огноо : ";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableDetail
            // 
            this.xrTableDetail.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTableDetail.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableDetail.Dpi = 254F;
            this.xrTableDetail.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableDetail.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTableDetail.Name = "xrTableDetail";
            this.xrTableDetail.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableDetail.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTableDetail.SizeF = new System.Drawing.SizeF(1972F, 63.5F);
            this.xrTableDetail.StylePriority.UseBorders = false;
            this.xrTableDetail.StylePriority.UseFont = false;
            this.xrTableDetail.StylePriority.UsePadding = false;
            this.xrTableDetail.StylePriority.UseTextAlignment = false;
            this.xrTableDetail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellNumber,
            this.xrTableCellMedicineName,
            this.xrTableCellLatinName,
            this.xrTableCellUnitType,
            this.xrTableCellQuantity,
            this.xrTableCellCount,
            this.xrTableCellConfirmCount,
            this.xrTableCellGiveCount});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCellNumber
            // 
            this.xrTableCellNumber.Dpi = 254F;
            this.xrTableCellNumber.Multiline = true;
            this.xrTableCellNumber.Name = "xrTableCellNumber";
            this.xrTableCellNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.xrTableCellNumber.StylePriority.UsePadding = false;
            xrSummary1.FormatString = "{0:#}";
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellNumber.Summary = xrSummary1;
            this.xrTableCellNumber.Weight = 0.15848743504516427D;
            // 
            // xrTableCellMedicineName
            // 
            this.xrTableCellMedicineName.Dpi = 254F;
            this.xrTableCellMedicineName.Multiline = true;
            this.xrTableCellMedicineName.Name = "xrTableCellMedicineName";
            this.xrTableCellMedicineName.Text = "Эм, эмнэлгийн хэрэгсэл\r\n";
            this.xrTableCellMedicineName.Weight = 0.88048566285414021D;
            // 
            // xrTableCellLatinName
            // 
            this.xrTableCellLatinName.Dpi = 254F;
            this.xrTableCellLatinName.Multiline = true;
            this.xrTableCellLatinName.Name = "xrTableCellLatinName";
            this.xrTableCellLatinName.Text = "Ангилал";
            this.xrTableCellLatinName.Weight = 0.88048499556196846D;
            // 
            // xrTableCellUnitType
            // 
            this.xrTableCellUnitType.Dpi = 254F;
            this.xrTableCellUnitType.Name = "xrTableCellUnitType";
            this.xrTableCellUnitType.Text = "Тун хэмжээ";
            this.xrTableCellUnitType.Weight = 0.44024334252021674D;
            // 
            // xrTableCellQuantity
            // 
            this.xrTableCellQuantity.CanShrink = true;
            this.xrTableCellQuantity.Dpi = 254F;
            this.xrTableCellQuantity.Multiline = true;
            this.xrTableCellQuantity.Name = "xrTableCellQuantity";
            this.xrTableCellQuantity.Text = "Хүчинтэй хугацаа";
            this.xrTableCellQuantity.Weight = 0.33018212867025809D;
            // 
            // xrTableCellCount
            // 
            this.xrTableCellCount.Dpi = 254F;
            this.xrTableCellCount.Name = "xrTableCellCount";
            this.xrTableCellCount.StylePriority.UseTextAlignment = false;
            this.xrTableCellCount.Text = "Үнэ";
            this.xrTableCellCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellCount.Weight = 0.55030378296764992D;
            // 
            // xrTableCellConfirmCount
            // 
            this.xrTableCellConfirmCount.Dpi = 254F;
            this.xrTableCellConfirmCount.Name = "xrTableCellConfirmCount";
            this.xrTableCellConfirmCount.StylePriority.UseTextAlignment = false;
            this.xrTableCellConfirmCount.Text = "xrTableCellConfirmCount";
            this.xrTableCellConfirmCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellConfirmCount.Weight = 0.55030324619221338D;
            // 
            // xrTableCellGiveCount
            // 
            this.xrTableCellGiveCount.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellGiveCount.Dpi = 254F;
            this.xrTableCellGiveCount.Name = "xrTableCellGiveCount";
            this.xrTableCellGiveCount.StylePriority.UseBorders = false;
            this.xrTableCellGiveCount.StylePriority.UseTextAlignment = false;
            this.xrTableCellGiveCount.Text = "xrTableCellGiveCount";
            this.xrTableCellGiveCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCellGiveCount.Weight = 0.55030357915300121D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 64F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 64F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelPageHeader});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 147.9359F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabelPageHeader
            // 
            this.xrLabelPageHeader.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrLabelPageHeader.Dpi = 254F;
            this.xrLabelPageHeader.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold);
            this.xrLabelPageHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 45F);
            this.xrLabelPageHeader.Name = "xrLabelPageHeader";
            this.xrLabelPageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelPageHeader.SizeF = new System.Drawing.SizeF(1972F, 58.41999F);
            this.xrLabelPageHeader.StylePriority.UseFont = false;
            this.xrLabelPageHeader.StylePriority.UseTextAlignment = false;
            this.xrLabelPageHeader.Text = "Захиалгын хуудас";
            this.xrLabelPageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel8,
            this.xrLabelSignatureUserName,
            this.xrLabel9,
            this.xrLabelRole,
            this.xrLabelTreatmentDirector});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 318.5519F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel8
            // 
            this.xrLabel8.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(116.9792F, 80.47926F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(199.75F, 59.99998F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "(Тэмдэг)";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelSignatureUserName
            // 
            this.xrLabelSignatureUserName.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelSignatureUserName.Dpi = 254F;
            this.xrLabelSignatureUserName.LocationFloat = new DevExpress.Utils.PointFloat(964.0624F, 115.1926F);
            this.xrLabelSignatureUserName.Multiline = true;
            this.xrLabelSignatureUserName.Name = "xrLabelSignatureUserName";
            this.xrLabelSignatureUserName.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelSignatureUserName.SizeF = new System.Drawing.SizeF(750F, 59.99997F);
            this.xrLabelSignatureUserName.StylePriority.UseFont = false;
            this.xrLabelSignatureUserName.StylePriority.UseTextAlignment = false;
            this.xrLabelSignatureUserName.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabelSignatureUserName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(409.7707F, 55.19253F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(554.2918F, 59.99999F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Батлав: Эмчилгээ эрхэлсэн орлогч";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelRole
            // 
            this.xrLabelRole.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelRole.Dpi = 254F;
            this.xrLabelRole.LocationFloat = new DevExpress.Utils.PointFloat(409.7707F, 115.1926F);
            this.xrLabelRole.Name = "xrLabelRole";
            this.xrLabelRole.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelRole.SizeF = new System.Drawing.SizeF(554.2917F, 59.99997F);
            this.xrLabelRole.StylePriority.UseFont = false;
            this.xrLabelRole.StylePriority.UseTextAlignment = false;
            this.xrLabelRole.Text = "Захиалсан : Эмийн сангийн эрхлэгч";
            this.xrLabelRole.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelTreatmentDirector
            // 
            this.xrLabelTreatmentDirector.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelTreatmentDirector.Dpi = 254F;
            this.xrLabelTreatmentDirector.LocationFloat = new DevExpress.Utils.PointFloat(964.0625F, 55.19253F);
            this.xrLabelTreatmentDirector.Multiline = true;
            this.xrLabelTreatmentDirector.Name = "xrLabelTreatmentDirector";
            this.xrLabelTreatmentDirector.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelTreatmentDirector.SizeF = new System.Drawing.SizeF(750F, 60F);
            this.xrLabelTreatmentDirector.StylePriority.UseFont = false;
            this.xrLabelTreatmentDirector.StylePriority.UseTextAlignment = false;
            this.xrLabelTreatmentDirector.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabelTreatmentDirector.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrTable1.Dpi = 254F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(8.074442E-05F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1972F, 90F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCellTotalCount,
            this.xrTableCellTotalConfirmCount,
            this.xrTableCellTotalGiveCount});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.CanShrink = true;
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Multiline = true;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "Дүн : ";
            this.xrTableCell24.Weight = 2.014284198411787D;
            // 
            // xrTableCellTotalCount
            // 
            this.xrTableCellTotalCount.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalCount.Dpi = 254F;
            this.xrTableCellTotalCount.Multiline = true;
            this.xrTableCellTotalCount.Name = "xrTableCellTotalCount";
            this.xrTableCellTotalCount.StylePriority.UseBorders = false;
            xrSummary2.FormatString = "{0:n2}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalCount.Summary = xrSummary2;
            this.xrTableCellTotalCount.Weight = 0.41208781892910851D;
            // 
            // xrTableCellTotalConfirmCount
            // 
            this.xrTableCellTotalConfirmCount.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCellTotalConfirmCount.Dpi = 254F;
            this.xrTableCellTotalConfirmCount.Name = "xrTableCellTotalConfirmCount";
            this.xrTableCellTotalConfirmCount.StylePriority.UseBorders = false;
            xrSummary3.FormatString = "{0:n2}";
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalConfirmCount.Summary = xrSummary3;
            this.xrTableCellTotalConfirmCount.Weight = 0.4120876413826981D;
            // 
            // xrTableCellTotalGiveCount
            // 
            this.xrTableCellTotalGiveCount.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellTotalGiveCount.Dpi = 254F;
            this.xrTableCellTotalGiveCount.Name = "xrTableCellTotalGiveCount";
            this.xrTableCellTotalGiveCount.StylePriority.UseBorders = false;
            xrSummary4.FormatString = "{0:n2}";
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.xrTableCellTotalGiveCount.Summary = xrSummary4;
            this.xrTableCellTotalGiveCount.Weight = 0.41208757329365692D;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportFooter1});
            this.DetailReport.Dpi = 254F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableDetail});
            this.Detail1.Dpi = 254F;
            this.Detail1.HeightF = 63.5F;
            this.Detail1.Name = "Detail1";
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.ReportFooter1.Dpi = 254F;
            this.ReportFooter1.HeightF = 90F;
            this.ReportFooter1.Name = "ReportFooter1";
            // 
            // xrLabel6
            // 
            this.xrLabel6.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(409.7708F, 175.1926F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(554.2917F, 59.99997F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Захиалсан : Эмийн сангийн эрхлэгч";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel7
            // 
            this.xrLabel7.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(964.0625F, 175.1926F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(750F, 59.99997F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // OrderDocReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter,
            this.DetailReport});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(64, 64, 64, 64);
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "15.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabelPageHeader;
        private DevExpress.XtraReports.UI.XRTable xrTableDetail;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellNumber;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellMedicineName;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellLatinName;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellUnitType;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellQuantity;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellCount;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalConfirmCount;
        private DevExpress.XtraReports.UI.XRLabel xrLabelExpUserName;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCompany;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellConfirmCount;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTotalGiveCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellGiveCount;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelUserName;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabelDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabelSignatureUserName;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabelRole;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTreatmentDirector;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabelWareHouse;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
    }
}
