﻿namespace Pharmacy2016.gui.report.journal.order
{
    partial class OrderMultiReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPivotGrid1 = new DevExpress.XtraReports.UI.XRPivotGrid();
            this.pivotGridFieldName = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldShape = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldLatinName = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldUnit = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldType = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldValidDate = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldDescription = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldQuantity = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldPatientName = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridField1 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldExpUserName = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridField4 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridField2 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridField6 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridField5 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldCount = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabelTreatmentSign = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelReportHeader = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCompany = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelStart = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelEnd = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabelTreatmentDirector = new DevExpress.XtraReports.UI.XRLabel();
            this.filter = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPivotGrid1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 235.4792F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPivotGrid1
            // 
            this.xrPivotGrid1.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrPivotGrid1.Appearance.Cell.WordWrap = true;
            this.xrPivotGrid1.Appearance.CustomTotalCell.WordWrap = true;
            this.xrPivotGrid1.Appearance.FieldHeader.WordWrap = true;
            this.xrPivotGrid1.Appearance.FieldValue.WordWrap = true;
            this.xrPivotGrid1.Appearance.FieldValueTotal.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.xrPivotGrid1.Appearance.Lines.WordWrap = true;
            this.xrPivotGrid1.Dpi = 254F;
            this.xrPivotGrid1.Fields.AddRange(new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField[] {
            this.pivotGridFieldName,
            this.pivotGridFieldShape,
            this.pivotGridFieldLatinName,
            this.pivotGridFieldUnit,
            this.pivotGridFieldType,
            this.pivotGridFieldValidDate,
            this.pivotGridFieldDescription,
            this.pivotGridFieldQuantity,
            this.pivotGridFieldPatientName,
            this.pivotGridField1,
            this.pivotGridFieldExpUserName,
            this.pivotGridField4,
            this.pivotGridField2,
            this.pivotGridField6,
            this.pivotGridField5,
            this.pivotGridFieldCount});
            this.xrPivotGrid1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPivotGrid1.Name = "xrPivotGrid1";
            this.xrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3;
            this.xrPivotGrid1.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.xrPivotGrid1.OptionsPrint.PrintHorzLines = DevExpress.Utils.DefaultBoolean.True;
            this.xrPivotGrid1.OptionsPrint.PrintRowHeaders = DevExpress.Utils.DefaultBoolean.True;
            this.xrPivotGrid1.OptionsPrint.PrintVertLines = DevExpress.Utils.DefaultBoolean.True;
            this.xrPivotGrid1.OptionsView.ShowDataHeaders = false;
            this.xrPivotGrid1.SizeF = new System.Drawing.SizeF(2770F, 235.4792F);
            this.xrPivotGrid1.CustomRowHeight += new System.EventHandler<DevExpress.XtraReports.UI.PivotGrid.PivotCustomRowHeightEventArgs>(this.xrPivotGrid1_CustomRowHeight);
            // 
            // pivotGridFieldName
            // 
            this.pivotGridFieldName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldName.AreaIndex = 0;
            this.pivotGridFieldName.Caption = "Эмийн нэр";
            this.pivotGridFieldName.FieldName = "medicineName";
            this.pivotGridFieldName.Name = "pivotGridFieldName";
            // 
            // pivotGridFieldShape
            // 
            this.pivotGridFieldShape.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldShape.AreaIndex = 6;
            this.pivotGridFieldShape.Caption = "Хэлбэр";
            this.pivotGridFieldShape.FieldName = "shape";
            this.pivotGridFieldShape.Name = "pivotGridFieldShape";
            // 
            // pivotGridFieldLatinName
            // 
            this.pivotGridFieldLatinName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldLatinName.AreaIndex = 1;
            this.pivotGridFieldLatinName.Caption = "Эмийн олон улсын нэршил";
            this.pivotGridFieldLatinName.FieldName = "latinName";
            this.pivotGridFieldLatinName.MinWidth = 100;
            this.pivotGridFieldLatinName.Name = "pivotGridFieldLatinName";
            this.pivotGridFieldLatinName.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            this.pivotGridFieldLatinName.Width = 165;
            // 
            // pivotGridFieldUnit
            // 
            this.pivotGridFieldUnit.Appearance.Cell.WordWrap = true;
            this.pivotGridFieldUnit.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldUnit.AreaIndex = 4;
            this.pivotGridFieldUnit.Caption = "Тун хэмжээ";
            this.pivotGridFieldUnit.FieldName = "unitType";
            this.pivotGridFieldUnit.Name = "pivotGridFieldUnit";
            this.pivotGridFieldUnit.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            // 
            // pivotGridFieldType
            // 
            this.pivotGridFieldType.Appearance.Cell.WordWrap = true;
            this.pivotGridFieldType.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldType.AreaIndex = 2;
            this.pivotGridFieldType.Caption = "Ангилал";
            this.pivotGridFieldType.FieldName = "medicineTypeName";
            this.pivotGridFieldType.Name = "pivotGridFieldType";
            // 
            // pivotGridFieldValidDate
            // 
            this.pivotGridFieldValidDate.Appearance.Cell.WordWrap = true;
            this.pivotGridFieldValidDate.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldValidDate.AreaIndex = 3;
            this.pivotGridFieldValidDate.Caption = "Хүчинтэй хугацаа";
            this.pivotGridFieldValidDate.CellFormat.FormatString = "yyyy-MM-dd";
            this.pivotGridFieldValidDate.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.pivotGridFieldValidDate.FieldName = "validDate";
            this.pivotGridFieldValidDate.Name = "pivotGridFieldValidDate";
            this.pivotGridFieldValidDate.ValueFormat.FormatString = "yyyy-MM-dd";
            this.pivotGridFieldValidDate.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.pivotGridFieldValidDate.Width = 105;
            // 
            // pivotGridFieldDescription
            // 
            this.pivotGridFieldDescription.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldDescription.AreaIndex = 7;
            this.pivotGridFieldDescription.Caption = "Тайлбар";
            this.pivotGridFieldDescription.FieldName = "description";
            this.pivotGridFieldDescription.Name = "pivotGridFieldDescription";
            this.pivotGridFieldDescription.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            // 
            // pivotGridFieldQuantity
            // 
            this.pivotGridFieldQuantity.Appearance.Cell.WordWrap = true;
            this.pivotGridFieldQuantity.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldQuantity.AreaIndex = 5;
            this.pivotGridFieldQuantity.Caption = "Хэмжих нэгж";
            this.pivotGridFieldQuantity.FieldName = "quantity";
            this.pivotGridFieldQuantity.Name = "pivotGridFieldQuantity";
            // 
            // pivotGridFieldPatientName
            // 
            this.pivotGridFieldPatientName.Appearance.Cell.WordWrap = true;
            this.pivotGridFieldPatientName.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldPatientName.AreaIndex = 0;
            this.pivotGridFieldPatientName.Caption = "Захиалсан";
            this.pivotGridFieldPatientName.FieldName = "userName";
            this.pivotGridFieldPatientName.Name = "pivotGridFieldPatientName";
            this.pivotGridFieldPatientName.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField1.AreaIndex = 1;
            this.pivotGridField1.Caption = "Баталсан";
            this.pivotGridField1.FieldName = "confirmUserID";
            this.pivotGridField1.Name = "pivotGridField1";
            // 
            // pivotGridFieldExpUserName
            // 
            this.pivotGridFieldExpUserName.Appearance.Cell.WordWrap = true;
            this.pivotGridFieldExpUserName.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldExpUserName.AreaIndex = 2;
            this.pivotGridFieldExpUserName.Caption = "Олгосон";
            this.pivotGridFieldExpUserName.FieldName = "expUserName";
            this.pivotGridFieldExpUserName.Name = "pivotGridFieldExpUserName";
            // 
            // pivotGridField4
            // 
            this.pivotGridField4.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField4.AreaIndex = 3;
            this.pivotGridField4.FieldName = "expUserID";
            this.pivotGridField4.Name = "pivotGridField4";
            this.pivotGridField4.Visible = false;
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField2.AreaIndex = 3;
            this.pivotGridField2.FieldName = "userID";
            this.pivotGridField2.Name = "pivotGridField2";
            this.pivotGridField2.Visible = false;
            // 
            // pivotGridField6
            // 
            this.pivotGridField6.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField6.AreaIndex = 2;
            this.pivotGridField6.Caption = "О";
            this.pivotGridField6.FieldName = "giveCount";
            this.pivotGridField6.GrandTotalCellFormat.FormatString = "n0";
            this.pivotGridField6.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField6.Name = "pivotGridField6";
            this.pivotGridField6.TotalCellFormat.FormatString = "n0";
            this.pivotGridField6.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField6.TotalValueFormat.FormatString = "n0";
            this.pivotGridField6.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField6.ValueFormat.FormatString = "n0";
            this.pivotGridField6.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField6.Width = 40;
            // 
            // pivotGridField5
            // 
            this.pivotGridField5.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField5.AreaIndex = 1;
            this.pivotGridField5.Caption = "Б";
            this.pivotGridField5.CellFormat.FormatString = "n0";
            this.pivotGridField5.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField5.FieldName = "confirmCount";
            this.pivotGridField5.GrandTotalCellFormat.FormatString = "n0";
            this.pivotGridField5.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField5.Name = "pivotGridField5";
            this.pivotGridField5.TotalCellFormat.FormatString = "n0";
            this.pivotGridField5.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField5.TotalValueFormat.FormatString = "n0";
            this.pivotGridField5.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField5.ValueFormat.FormatString = "n0";
            this.pivotGridField5.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridField5.Width = 40;
            // 
            // pivotGridFieldCount
            // 
            this.pivotGridFieldCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldCount.AreaIndex = 0;
            this.pivotGridFieldCount.Caption = "З";
            this.pivotGridFieldCount.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.FieldName = "count";
            this.pivotGridFieldCount.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Name = "pivotGridFieldCount";
            this.pivotGridFieldCount.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Width = 40;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 73.54166F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 113.2512F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelTreatmentSign,
            this.xrLabelReportHeader,
            this.xrLabelCompany,
            this.xrLabel1,
            this.xrLabelStart,
            this.xrLabel4,
            this.xrLabelEnd,
            this.xrLabel2});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 480.5815F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabelTreatmentSign
            // 
            this.xrLabelTreatmentSign.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelTreatmentSign.Dpi = 254F;
            this.xrLabelTreatmentSign.LocationFloat = new DevExpress.Utils.PointFloat(0F, 234.3958F);
            this.xrLabelTreatmentSign.Multiline = true;
            this.xrLabelTreatmentSign.Name = "xrLabelTreatmentSign";
            this.xrLabelTreatmentSign.Padding = new DevExpress.XtraPrinting.PaddingInfo(69, 69, 0, 0, 254F);
            this.xrLabelTreatmentSign.SizeF = new System.Drawing.SizeF(2770F, 60F);
            this.xrLabelTreatmentSign.StylePriority.UseFont = false;
            this.xrLabelTreatmentSign.StylePriority.UsePadding = false;
            this.xrLabelTreatmentSign.StylePriority.UseTextAlignment = false;
            this.xrLabelTreatmentSign.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabelTreatmentSign.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelReportHeader
            // 
            this.xrLabelReportHeader.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrLabelReportHeader.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabelReportHeader.Dpi = 254F;
            this.xrLabelReportHeader.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabelReportHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 366.7274F);
            this.xrLabelReportHeader.Name = "xrLabelReportHeader";
            this.xrLabelReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelReportHeader.SizeF = new System.Drawing.SizeF(2770F, 59.75F);
            this.xrLabelReportHeader.StylePriority.UseBorders = false;
            this.xrLabelReportHeader.StylePriority.UseFont = false;
            this.xrLabelReportHeader.Text = "ЗАХИАЛГА ТАЙЛАН";
            this.xrLabelReportHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabelCompany
            // 
            this.xrLabelCompany.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelCompany.Dpi = 254F;
            this.xrLabelCompany.LocationFloat = new DevExpress.Utils.PointFloat(359.4167F, 46.16676F);
            this.xrLabelCompany.Name = "xrLabelCompany";
            this.xrLabelCompany.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelCompany.SizeF = new System.Drawing.SizeF(562F, 60F);
            this.xrLabelCompany.StylePriority.UseFont = false;
            this.xrLabelCompany.StylePriority.UseTextAlignment = false;
            this.xrLabelCompany.Text = "xrLabel1";
            this.xrLabelCompany.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(49.41656F, 46.16668F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Байгууллага : ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelStart
            // 
            this.xrLabelStart.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelStart.Dpi = 254F;
            this.xrLabelStart.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabelStart.LocationFloat = new DevExpress.Utils.PointFloat(2347.602F, 46.16676F);
            this.xrLabelStart.Name = "xrLabelStart";
            this.xrLabelStart.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelStart.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabelStart.StylePriority.UseFont = false;
            this.xrLabelStart.StylePriority.UseTextAlignment = false;
            this.xrLabelStart.Text = "xrLabel1";
            this.xrLabelStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(2037.602F, 106.1667F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Дуусах огноо :";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelEnd
            // 
            this.xrLabelEnd.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right;
            this.xrLabelEnd.Dpi = 254F;
            this.xrLabelEnd.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabelEnd.LocationFloat = new DevExpress.Utils.PointFloat(2347.602F, 106.1667F);
            this.xrLabelEnd.Name = "xrLabelEnd";
            this.xrLabelEnd.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelEnd.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabelEnd.StylePriority.UseFont = false;
            this.xrLabelEnd.StylePriority.UseTextAlignment = false;
            this.xrLabelEnd.Text = "xrLabel1";
            this.xrLabelEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(2037.601F, 46.16676F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Эхлэх огноо : ";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelTreatmentDirector});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 191.8119F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabelTreatmentDirector
            // 
            this.xrLabelTreatmentDirector.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelTreatmentDirector.Dpi = 254F;
            this.xrLabelTreatmentDirector.LocationFloat = new DevExpress.Utils.PointFloat(0F, 87.07253F);
            this.xrLabelTreatmentDirector.Multiline = true;
            this.xrLabelTreatmentDirector.Name = "xrLabelTreatmentDirector";
            this.xrLabelTreatmentDirector.Padding = new DevExpress.XtraPrinting.PaddingInfo(69, 69, 0, 0, 254F);
            this.xrLabelTreatmentDirector.SizeF = new System.Drawing.SizeF(2770F, 60F);
            this.xrLabelTreatmentDirector.StylePriority.UseFont = false;
            this.xrLabelTreatmentDirector.StylePriority.UsePadding = false;
            this.xrLabelTreatmentDirector.StylePriority.UseTextAlignment = false;
            this.xrLabelTreatmentDirector.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabelTreatmentDirector.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // filter
            // 
            this.filter.Description = "filter";
            this.filter.Name = "filter";
            this.filter.Visible = false;
            // 
            // OrderMultiReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 74, 113);
            this.PageHeight = 2100;
            this.PageWidth = 2970;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.filter});
            this.ReportPrintOptions.DetailCount = 1;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "15.1";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.Parameters.Parameter filter;
        private DevExpress.XtraReports.UI.XRLabel xrLabelStart;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabelEnd;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCompany;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRPivotGrid xrPivotGrid1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldUnit;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldPatientName;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldCount;
        private DevExpress.XtraReports.UI.XRLabel xrLabelReportHeader;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldExpUserName;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridField6;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridField5;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldName;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldShape;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldLatinName;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldType;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldValidDate;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldQuantity;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldDescription;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridField1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridField2;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridField4;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTreatmentDirector;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTreatmentSign;
    }
}
