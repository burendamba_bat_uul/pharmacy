﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;
using System.Collections.Generic;
using DevExpress.XtraReports.UI.PivotGrid;
using System.Data;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.gui.report.journal.order
{
    /*
     * Зарлагын журналын тайлан
     * 
     */

    public partial class OrderMultiReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

        private static OrderMultiReport INSTANCE = new OrderMultiReport();

        public static OrderMultiReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        int maxHeight = 20;

        private OrderMultiReport()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;

            DataSource = JournalDataSet.Instance;
            DataAdapter = JournalDataSet.OrderGoodsDetailTableAdapter;
            DataMember = JournalDataSet.Instance.OrderGoodsDetail.ToString();
        }

        private void findUsersOut()
        {
            int maxStage = 0;
            for (int i = 0; i < JournalDataSet.Instance.OrderGoodsDetail.Rows.Count; i++)
            {
                DataRow detial = JournalDataSet.Instance.OrderGoodsDetail.Rows[i];
                for (int a = 5; a > 0; a--)
                {
                    if (detial["" + a + ""] != DBNull.Value)
                    {
                        maxStage = a;
                        break;
                    }
                }
                DataRow[] stage = JournalDataSet.Instance.OrderStage.Select("orderGoodsDetialID = '" + detial["id"] + "' AND confStage = '" + maxStage + "'");
                if (stage.Length > 0 && ReportUpForm.Instance.returnRoleUserName(stage[0]["userID"].ToString(), 1) != "")
                {
                    string[] confirmUser = ReportUpForm.Instance.returnRoleUserName(stage[0]["userID"].ToString(), 1).Split(' ');
                    for (int o = 1; o < confirmUser.Length; o++)
                    {
                        if (confirmUser[o].Length > 0)
                        {
                            detial["confirmUserID"] = "Б: (" + confirmUser[o].Substring(0, 1) + "." + confirmUser[0] + ")";
                            break;
                        }
                    }
                }
                else
                {
                    detial["confirmUserID"] = "";
                }
                if (!detial["userName"].ToString().Contains("З:"))
                {  
                    string[] userName = detial["userName"].ToString().Split(' ');
                    string user = "";           
                    for (int a = 1; a < userName.Length; a++)
                    {
                        if (userName[a].Length > 0)
                        {
                            user = "З: (" + userName[a].Substring(0, 1) + "." + userName[0] + ")";
                            break;
                        }
                    }
                    detial["userName"] = user;
                }
                string[] expUserName = detial["expUserName"].ToString().Split(' ');
                string exp = "";
                for (int j = 1; j < expUserName.Length; j++)
                {
                    if (expUserName[j].Length > 0)
                    {
                        exp = "О: (" + expUserName[j].Substring(0, 1) + "." + expUserName[0] + ")";
                        break;
                    }
                }
                if (!detial["expUserName"].ToString().Contains("О:"))
                    detial["expUserName"] = exp;
                //if (!detial["userName"].ToString().Contains(exp))
                //    detial["userName"] = detial["userName"] + "\r\n" + exp;
                //if (!pivotGridFieldExpUserName.Visible)
                //{
                //    int removeIndex = detial["userName"].ToString().IndexOf("\r\n" + exp);
                //    detial["userName"] = detial["userName"].ToString().Remove(removeIndex);
                //}
            }
        }

        #endregion

        #region Тайлан харуулах функц.

        public void initAndShow(List<string> columnVis, List<string> filter, string start, string end, int location)
        {
            try
            {
                if (!isInit)
                {
                    initForm();
                }

                ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);
                showOrHide(columnVis);
                xrPivotGrid1.Prefilter.CriteriaString = "(userID = '" + filter[0] + "' OR userID = '" + filter[1] + "') OR ( expUserID = '" + filter[1] + "' OR expUserID = '" + filter[0] + "')";
                findUsersOut();
                //pivotGridFieldExpUserName.Visible = false;
                xrLabelTreatmentDirector.Text = "Батлав: ЭЭО ....................................../ " + HospitalUtil.getHospitalTreatmentDirectorShortName() + " /";
                xrLabelTreatmentSign.Text = "Батлав: ЭЭО ....................................../ " + HospitalUtil.getHospitalTreatmentDirectorShortName() + " /";

                switch (location)
                {
                    case 0:
                        xrLabelTreatmentSign.Visible = false;
                        xrLabelTreatmentDirector.Visible = true;
                        xrLabelTreatmentDirector.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                        break;
                    case 1:
                        xrLabelTreatmentSign.Visible = false;
                        xrLabelTreatmentDirector.Visible = true;
                        xrLabelTreatmentDirector.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        break;
                    case 2:
                        xrLabelTreatmentSign.Visible = false;
                        xrLabelTreatmentDirector.Visible = true;
                        xrLabelTreatmentDirector.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
                        break;
                    case 3:
                        xrLabelTreatmentSign.Visible = true;
                        xrLabelTreatmentDirector.Visible = false;
                        xrLabelTreatmentSign.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                        break;
                    case 4:
                        xrLabelTreatmentSign.Visible = true;
                        xrLabelTreatmentDirector.Visible = false;
                        xrLabelTreatmentSign.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        break;
                    case 5:
                        xrLabelTreatmentSign.Visible = true;
                        xrLabelTreatmentDirector.Visible = false;
                        xrLabelTreatmentSign.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
                        break;
                    default:
                        break;
                }

                xrLabelCompany.Text = HospitalUtil.getHospitalName();
                xrLabelReportHeader.Text = HospitalUtil.getHospitalName() + " клиникийн захиалгын хуудас";
                xrLabelStart.Text = start;
                xrLabelEnd.Text = end;
                //FilterString = "latinName = 'Malinda'";
                PrintingSystem.Document.AutoFitToPagesWidth = 1;
                xrLabelReportHeader.Width = xrPivotGrid1.ActualWidth;
                xrLabelTreatmentSign.Width = xrLabelReportHeader.Width;
                xrLabelTreatmentDirector.Width = xrLabelReportHeader.Width;
                xrLabelStart.Location = new Point((xrLabelReportHeader.Location.X + xrLabelReportHeader.Width) - xrLabelStart.Width, xrLabelStart.Location.Y);
                xrLabelEnd.Location = new Point((xrLabelReportHeader.Location.X + xrLabelReportHeader.Width) - xrLabelEnd.Width, xrLabelEnd.Location.Y);
                xrLabel2.Location = new Point(xrLabelStart.Location.X - xrLabel2.Width, xrLabel2.Location.Y);
                xrLabel4.Location = new Point(xrLabelEnd.Location.X - xrLabel4.Width, xrLabel4.Location.Y);
                CreateDocument(false);
                ProgramUtil.closeWaitDialog();
                ReportViewForm.Instance.initAndShow(INSTANCE);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                xrPivotGrid1.Prefilter.CriteriaString = "";
                ProgramUtil.closeWaitDialog();
            }
        }

        private void showOrHide(List<string> showAndHide)
        {
            showAndHide.Add("confirmCount");
            showAndHide.Add("giveCount");
            showAndHide.Add("count");
            //showAndHide.Add("confirmUserID");
            //showAndHide.Add("expUserName");
            foreach (DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField field in xrPivotGrid1.Fields)
            {
                if (showAndHide.Contains(field.FieldName))
                    field.Visible = true;
                else
                    field.Visible = false;
            }
        }

        private void xrPivotGrid1_CustomRowHeight(object sender, DevExpress.XtraReports.UI.PivotGrid.PivotCustomRowHeightEventArgs e)
        {
            for (int i = 0; i < xrPivotGrid1.ColumnCount - 1; i++)
            {

                PivotCellBaseEventArgs CellInfo = xrPivotGrid1.GetCellInfo(i, e.RowIndex);
                foreach (DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField field in xrPivotGrid1.Fields)
                {
                    object some = CellInfo.GetFieldValue(field);
                    if (some != null)
                        maxHeight = getHeight(some.ToString());

                    if (maxHeight > e.RowHeight)
                        e.RowHeight = maxHeight;
                }
            }
        }

        private static int getHeight(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                int ir = str.Length / 14;
                if (ir > 0 && str.Length % 14 > 0)
                {
                    return 20 * (ir + 1);
                }
                else if (ir > 0 && str.Length % 14 == 0)
                {
                    return 20 * ir;
                }
                else
                {
                    return 20;
                }
                //return (int)(DevExpress.XtraPrinting.Native.Measurement.MeasureString(str, font, System.Drawing.GraphicsUnit.Pixel).Height) + 2;
            }
            else
            {
                return 0;
            }
        }
        #endregion
    }
}
