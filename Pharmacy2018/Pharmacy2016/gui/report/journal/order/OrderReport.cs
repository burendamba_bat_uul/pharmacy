﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.gui.report.journal.order
{
    /*
     * Эмийн захиалгын хуудасны тайлан
     * 
     */ 

    public partial class OrderReport : DevExpress.XtraReports.UI.XtraReport
    {
        
        #region Тайлан анх үүсгэх функц.

            private static OrderReport INSTANCE = new OrderReport();

            public static OrderReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private OrderReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = ReportDataSet.Instance;
                DataAdapter = ReportDataSet.MedicineOrderDetailTableAdapter;
                DataMember = ReportDataSet.Instance.MedicineOrderDetail.ToString();
                
                initBinding();
            }

            private void initBinding()
            {
                this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MedicineName", "")});
                this.xrTableCellUnit.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Unit", "")});
                this.xrTableCellCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellGiveCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".GiveCount", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Price", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatFloat)});

                this.xrTableCellTotalGiveCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".GiveCount", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellTotalSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(string orderId, string motJournalId, string sum)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                    xrLabelHeader1.Text = string.Format("Батлав. Клиник эрхэлсэн дэд захирал ................................................................ /{0}/", HospitalUtil.getHospitalSubDirectorShortName());
                    xrLabelHeader2.Text = string.Format("................................................................ -ийн тасгийн {0} оны ..... сарын эм, эмнэлгийн хэрэгслийн захиалга", DateTime.Now.Year);

                    ReportDataSet.MedicineOrderDetailTableAdapter.Fill(ReportDataSet.Instance.MedicineOrderDetail, HospitalUtil.getHospitalId(), orderId, motJournalId);
                    xrTableCellTotalCount.Text = sum;

                    CreateDocument(true);

                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}
