﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Pharmacy2016.util;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.ds;
using System.Collections.Generic;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.report.journal.order
{
    /*
     * Эмийн үлдэгдлийн тайлан
     * 
     */

    public partial class OrderGoodsReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

        private static OrderGoodsReport INSTANCE = new OrderGoodsReport();

        public static OrderGoodsReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private OrderGoodsReport()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;

            DataSource = JournalDataSet.Instance;
            DataAdapter = JournalDataSet.OrderGoodsTableAdapter;
            DataMember = JournalDataSet.Instance.OrderGoods.ToString();

            DetailReport.DataSource = JournalDataSet.Instance;
            DetailReport.DataAdapter = JournalDataSet.OrderGoodsDetailTableAdapter;
            DetailReport.DataMember = "OrderGoods.OrderGoods_OrderGoodsDetail";
            initBinding();
        }

        private void initBinding()
        {
            //this.xrLabelUserName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            //new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UserName", "")});
            this.xrLabelWareHouse.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".WareHouseName", ReportUtil.DisplayFormatDate)});
            this.xrLabelDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Date", ReportUtil.DisplayFormatDate)});
            //this.xrLabelExpUserName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            //new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ExpUserName", "")});

            this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".MedicineName", "")});
            this.xrTableCellLatinName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".LatinName", "")});
            this.xrTableCellMedicineType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".MedicineTypeName", "")});
            this.xrTableCellShape.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Shape", "")});
            this.xrTableCellValidDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".ValidDate", ReportUtil.DisplayFormatDate)});
            this.xrTableCellUnitType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".UnitType", "")});
            this.xrTableCellQuantity.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Quantity", "")});
            this.xrTableCellCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellConfirmCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".ConfirmCount", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellGiveCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".GiveCount", ReportUtil.DisplayFormatFloat)});

            this.xrTableCellTotalCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellTotalConfirmCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".ConfirmCount", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellTotalGiveCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".GiveCount", ReportUtil.DisplayFormatFloat)});
            this.xrTableCellDesc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Description", ReportUtil.DisplayFormatFloat)});
        }

        #endregion

        #region Тайлан харуулах функц.

        public void initAndShow(List<string> columns, DataRow filter, string expUser)
        {
            try
            {
                if (!isInit)
                {
                    initForm();
                }

                ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);
                
                hideColumns(columns);
                xrLabelPageHeader.Text = HospitalUtil.getHospitalName() + " клиникийн захиалгын хуудас";
                xrLabelUserRole.Text = "Захиалсан: (" + filter["roleName"].ToString() + ")";
                if (ReportUpForm.Instance.returnRoleUserName(expUser, 0) != "")
                {
                    xrLabelConfRole.Text = "Зөвшөөрсөн: (" + ReportUpForm.Instance.returnRoleUserName(expUser, 0) + ")";
                }
                else
                {
                    xrLabelConfRole.Visible = false;
                }
                xrLabelexpRole.Text = "Олгосон: (" + ReportUpForm.Instance.returnRoleUserName(filter["expUserID"].ToString(), 0) + ")";
                xrLabelCompany.Text = HospitalUtil.getHospitalName();
                xrLabelTreatmentDirector.Text = "................................................../ " + HospitalUtil.getHospitalTreatmentDirectorShortName() + " /";
                string[] value = filter["userName"].ToString().Split(' ');
                for (int i = 1; i < value.Length; i++)
                {
                    if (value[i].Length > 0)
                    {
                        xrLabelSignatureUserName.Text = "................................................../ " + value[i].Substring(0, 1) + "." + value[0] + " /";
                        break;
                    }
                }
                string[] exp = filter["expUserName"].ToString().Split(' ');
                for (int j = 1; j < exp.Length; j++)
                {
                    if (exp[j].Length > 0)
                    {
                        xrLabelexpUser.Text = "................................................../ " + exp[j].Substring(0, 1) + "." + exp[0] + " /";
                        break;
                    }
                }
                string[] conf = ReportUpForm.Instance.returnRoleUserName(expUser, 1).Split(' ');               
                for (int a = 1; a < conf.Length; a++)
                {
                    if (conf[a].Length > 0)
                    {
                        xrLabelConfUser.Text = "................................................../ " + conf[a].Substring(0, 1) + "." + conf[0] + " /";
                        break;
                    }
                }
                if (ReportUpForm.Instance.returnRoleUserName(expUser, 1) == "")
                {
                    xrLabelConfUser.Visible = false;
                }
                FilterString = "id = '" + filter["id"] + "'";
                CreateDocument(true);
                ProgramUtil.closeWaitDialog();
                ReportViewForm.Instance.initAndShow(INSTANCE);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void hideColumns(List<string> visibleCol)
        {           
            xrTable2.SuspendLayout();
            xrTableDetail.SuspendLayout();
            xrTable1.SuspendLayout();

            if (visibleCol.Contains("userName"))
            {
                xrLabelUserRole.Visible = true;
                xrLabelSignatureUserName.Visible = true;
            }else
            {
                xrLabelUserRole.Visible = false;
                xrLabelSignatureUserName.Visible = false;
            }
            if (visibleCol.Contains("expUserName"))
            {
                xrLabelexpRole.Visible = true;
                xrLabelexpUser.Visible = true;
            }
            else
            {
                xrLabelexpRole.Visible = false;
                xrLabelexpUser.Visible = false;
            }

            if (xrTableRow1.Cells.Contains(xrTableCellCount))
                xrTableRow1.Cells.Remove(xrTableCellCount);
            if (xrTableRow1.Cells.Contains(xrTableCellConfirmCount))
                xrTableRow1.Cells.Remove(xrTableCellConfirmCount);
            if (xrTableRow1.Cells.Contains(xrTableCellGiveCount))
                xrTableRow1.Cells.Remove(xrTableCellGiveCount);
            if (xrTableRow4.Cells.Contains(xrTableCellcn))
                xrTableRow4.Cells.Remove(xrTableCellcn);
            if (xrTableRow4.Cells.Contains(xrTableCellconc))
                xrTableRow4.Cells.Remove(xrTableCellconc);
            if (xrTableRow4.Cells.Contains(xrTableCellgcnt))
                xrTableRow4.Cells.Remove(xrTableCellgcnt);
            if (visibleCol.Contains("shape"))
            {                 
                if (!xrTableRow4.Cells.Contains(xrTableCellShapes))
                    xrTableRow4.Cells.Add(xrTableCellShapes);
                if (!xrTableRow1.Cells.Contains(xrTableCellShape))
                    xrTableRow1.Cells.Add(xrTableCellShape);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellShapes))
                    xrTableRow4.Cells.Remove(xrTableCellShapes);
                if (xrTableRow1.Cells.Contains(xrTableCellShape))
                    xrTableRow1.Cells.Remove(xrTableCellShape);
            }
            if (visibleCol.Contains("validDate"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellDate))
                    xrTableRow4.Cells.Add(xrTableCellDate);
                if (!xrTableRow1.Cells.Contains(xrTableCellValidDate))
                    xrTableRow1.Cells.Add(xrTableCellValidDate);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellDate))
                    xrTableRow4.Cells.Remove(xrTableCellDate);
                if (xrTableRow1.Cells.Contains(xrTableCellValidDate))
                    xrTableRow1.Cells.Remove(xrTableCellValidDate);
            }
            if (visibleCol.Contains("quantity"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellQuant))
                    xrTableRow4.Cells.Add(xrTableCellQuant);
                if (!xrTableRow1.Cells.Contains(xrTableCellQuantity))
                    xrTableRow1.Cells.Add(xrTableCellQuantity);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellQuant))
                    xrTableRow4.Cells.Remove(xrTableCellQuant);
                if (xrTableRow1.Cells.Contains(xrTableCellQuantity))
                    xrTableRow1.Cells.Remove(xrTableCellQuantity);
            }
            if (visibleCol.Contains("latinName"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCelllat))
                    xrTableRow4.Cells.Add(xrTableCelllat);
                if (!xrTableRow1.Cells.Contains(xrTableCellLatinName))
                    xrTableRow1.Cells.Add(xrTableCellLatinName);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCelllat))
                    xrTableRow4.Cells.Remove(xrTableCelllat);
                if (xrTableRow1.Cells.Contains(xrTableCellLatinName))
                    xrTableRow1.Cells.Remove(xrTableCellLatinName);
            }
            if (visibleCol.Contains("unitType"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellUni))
                    xrTableRow4.Cells.Add(xrTableCellUni);
                if (!xrTableRow1.Cells.Contains(xrTableCellUnitType))
                    xrTableRow1.Cells.Add(xrTableCellUnitType);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellUni))
                    xrTableRow4.Cells.Remove(xrTableCellUni);
                if (xrTableRow1.Cells.Contains(xrTableCellUnitType))
                    xrTableRow1.Cells.Remove(xrTableCellUnitType);
            }
            if (visibleCol.Contains("medicineTypeName"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellMtype))
                    xrTableRow4.Cells.Add(xrTableCellMtype);
                if (!xrTableRow1.Cells.Contains(xrTableCellMedicineType))
                    xrTableRow1.Cells.Add(xrTableCellMedicineType);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellMtype))
                    xrTableRow4.Cells.Remove(xrTableCellMtype);
                if (xrTableRow1.Cells.Contains(xrTableCellMedicineType))
                    xrTableRow1.Cells.Remove(xrTableCellMedicineType);
            }
            if (visibleCol.Contains("medicineName"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellMedName))
                    xrTableRow4.Cells.Add(xrTableCellMedName);
                if (!xrTableRow1.Cells.Contains(xrTableCellMedicineName))
                    xrTableRow1.Cells.Add(xrTableCellMedicineName);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellMedName))
                    xrTableRow4.Cells.Remove(xrTableCellMedName);
                if (xrTableRow1.Cells.Contains(xrTableCellMedicineName))
                    xrTableRow1.Cells.Remove(xrTableCellMedicineName);
            }
            if (visibleCol.Contains("description"))
            {
                if (!xrTableRow4.Cells.Contains(xrTableCellDescript))
                    xrTableRow4.Cells.Add(xrTableCellDescript);
                if (!xrTableRow1.Cells.Contains(xrTableCellDesc))
                    xrTableRow1.Cells.Add(xrTableCellDesc);
            }
            else
            {
                if (xrTableRow4.Cells.Contains(xrTableCellDescript))
                    xrTableRow4.Cells.Remove(xrTableCellDescript);
                if (xrTableRow1.Cells.Contains(xrTableCellDesc))
                    xrTableRow1.Cells.Remove(xrTableCellDesc);

            }
            if (!xrTableRow1.Cells.Contains(xrTableCellCount))
                xrTableRow1.Cells.Add(xrTableCellCount);
            if (!xrTableRow1.Cells.Contains(xrTableCellConfirmCount))
                xrTableRow1.Cells.Add(xrTableCellConfirmCount);
            if (!xrTableRow1.Cells.Contains(xrTableCellGiveCount))
                xrTableRow1.Cells.Add(xrTableCellGiveCount);
            if (!xrTableRow4.Cells.Contains(xrTableCellcn))
                xrTableRow4.Cells.Add(xrTableCellcn);
            if (!xrTableRow4.Cells.Contains(xrTableCellconc))
                xrTableRow4.Cells.Add(xrTableCellconc);
            if (!xrTableRow4.Cells.Contains(xrTableCellgcnt))
                xrTableRow4.Cells.Add(xrTableCellgcnt);
            xrTable2.PerformLayout();
            xrTableDetail.PerformLayout();
            xrTable1.PerformLayout();
            xrTableDetail.BeginInit();
            xrTable1.BeginInit();
            xrTableCellTotalCount.WidthF = xrTableCellCount.WidthF;
            xrTableCellTotalConfirmCount.WidthF = xrTableCellConfirmCount.WidthF;
            xrTableCellTotalGiveCount.WidthF = xrTableCellGiveCount.WidthF;
            xrTable1.EndInit();
            xrTableDetail.EndInit();
        }

        #endregion

    }
}
