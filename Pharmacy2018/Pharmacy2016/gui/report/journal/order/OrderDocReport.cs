﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Pharmacy2016.util;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.ds;
using System.Data.SqlClient;

namespace Pharmacy2016.report.journal.order
{
    /*
     * Эмийн үлдэгдлийн тайлан
     * 
     */

    public partial class OrderDocReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

            private static OrderDocReport INSTANCE = new OrderDocReport();

            public static OrderDocReport Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private OrderDocReport()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = JournalDataSet.Instance;
                DataAdapter = JournalDataSet.OrderGoodsTableAdapter;
                DataMember = JournalDataSet.Instance.OrderGoods.ToString();

                DetailReport.DataSource = JournalDataSet.Instance;
                DetailReport.DataAdapter = JournalDataSet.OrderGoodsDetailTableAdapter;
                DetailReport.DataMember = "OrderGoods.OrderGoods_OrderGoodsDetail";

                initBinding();
            }

            private void initBinding()
            {
                this.xrLabelUserName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UserName", "")});
                this.xrLabelWareHouse.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".WareHouseName", ReportUtil.DisplayFormatDate)});
                this.xrLabelDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Date", ReportUtil.DisplayFormatDate)});
                this.xrLabelExpUserName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ExpUserName", "")});

                this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".MedicineName", "")});
                this.xrTableCellLatinName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".LatinName", "")});
                this.xrTableCellUnitType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".UnitType", "")});
                this.xrTableCellQuantity.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Quantity", "")});
                this.xrTableCellCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellConfirmCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".ConfirmCount", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellGiveCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".GiveCount", ReportUtil.DisplayFormatFloat)});

                this.xrTableCellTotalCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellTotalConfirmCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".ConfirmCount", ReportUtil.DisplayFormatFloat)});
                this.xrTableCellTotalGiveCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".GiveCount", ReportUtil.DisplayFormatFloat)});
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(string filter, string userName, string roleName, bool isConfirm)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                    if (isConfirm == false)
                    {
                        xrTableCellGiveCount.DataBindings.Clear();
                        xrTableCellGiveCount.Text = "";
                        xrTableCellConfirmCount.DataBindings.Clear();
                        xrTableCellConfirmCount.Text = "";
                        xrTableCellTotalConfirmCount.DataBindings.Clear();
                        xrTableCellTotalConfirmCount.Summary = new XRSummary(SummaryRunning.None, SummaryFunc.Custom, "");
                        xrTableCellTotalGiveCount.DataBindings.Clear();
                        xrTableCellTotalGiveCount.Summary = new XRSummary(SummaryRunning.None, SummaryFunc.Custom, "");
                    }
                    else
                    {
                        this.xrTableCellConfirmCount.DataBindings.Add(new XRBinding("Text", null, DetailReport.DataMember + ".ConfirmCount", ReportUtil.DisplayFormatFloat));
                        this.xrTableCellGiveCount.DataBindings.Add(new XRBinding("Text", null, DetailReport.DataMember + ".GiveCount", ReportUtil.DisplayFormatFloat));
                        this.xrTableCellTotalConfirmCount.DataBindings.Add(new XRBinding("Text", null, DetailReport.DataMember + ".ConfirmCount", ReportUtil.DisplayFormatFloat));
                        xrTableCellTotalConfirmCount.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n2}");
                        this.xrTableCellTotalGiveCount.DataBindings.Add(new XRBinding("Text", null, DetailReport.DataMember + ".GiveCount", ReportUtil.DisplayFormatFloat));
                        xrTableCellTotalGiveCount.Summary = new XRSummary(SummaryRunning.Report, SummaryFunc.Sum, "{0:n2}");
                    }
                    //xrLabelRole.Text = "Захиалсан : " + roleName + "";
                    //xrLabelCompany.Text = HospitalUtil.getHospitalName();
                    //xrLabelTreatmentDirector.Text = "................................................../ " + HospitalUtil.getHospitalTreatmentDirectorShortName() + " /";
                    //string[] value = userName.Split(' ');
                    //for (int i = 1; i < value.Length; i++)
                    //{
                    //    if (value[i].Length > 0)
                    //    {
                    //        xrLabelSignatureUserName.Text = "................................................../ " + value[i].Substring(0, 1) + "." + value[0] + " /";
                    //        break;
                    //    }
                    //}

                    SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                    myConn.Open();
                    string query = "SELECT * FROM [ReportSetting]";
                    SqlCommand oCmd = new SqlCommand(query, myConn);
                    using (SqlDataReader oReader = oCmd.ExecuteReader())
                    {
                        while (oReader.Read())
                        {
                            if (oReader.HasRows)
                            {
                               if (oReader["code"].ToString() == "1" && !string.IsNullOrEmpty(oReader["employ"].ToString()))
                                {
                                    xrLabel9.Text = oReader["employ"].ToString();
                                    xrLabelTreatmentDirector.Text = "................................................../ " + oReader["name"].ToString();
                                }
                                else if (oReader["code"].ToString() == "2" && !string.IsNullOrEmpty(oReader["employ"].ToString()))
                                {
                                    xrLabelRole.Text = oReader["employ"].ToString();
                                    xrLabelSignatureUserName.Text = "................................................../ " + oReader["name"].ToString();
                                }
                                else if (oReader["code"].ToString() == "3" && !string.IsNullOrEmpty(oReader["employ"].ToString()))
                                {
                                    xrLabel6.Text = oReader["employ"].ToString();
                                    xrLabel7.Text = "................................................../ " + oReader["name"].ToString();
                                }

                            }
                        }
                    }
                    myConn.Close();

                    FilterString = "id = '" + filter + "'";

                    CreateDocument(true);

                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}
