﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Pharmacy2016.util;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.report.journal.patient
{
    /*
     * Эмчлүүлэгчийн хэрэглэсэн эмийн тайлан
     * 
     */ 

    public partial class PatientMedicineUserReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

            private static PatientMedicineUserReport INSTANCE = new PatientMedicineUserReport();

            public static PatientMedicineUserReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private PatientMedicineUserReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = ReportDataSet.Instance;
                DataAdapter = ReportDataSet.PatientMedicineUserTableAdapter;
                DataMember = ReportDataSet.Instance.PatientMedicineUser.ToString();
                
                initBinding();
            }

            private void initBinding()
            {
                this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MedicineName", "")});
                this.xrTableCellTypeName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MedicineTypeName", "")});
                this.xrTableCellShape.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Shape", "")});
                this.xrTableCellUnit.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Unit", "")});
                this.xrTableCellValiDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ValidDate", ReportUtil.DisplayFormatDate)});
                this.xrTableCellIncTypeName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IncTypeName", "")});
                
                this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Price", ReportUtil.DisplayFormatFloat)});

                this.xrTableCellCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatFloat)});

                this.xrTableCellTotalCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                this.xrTableCellTotalSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(DataRowView view, int type)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);
                    xrLabelLastName.Text = view["lastName"].ToString();
                    xrLabelFirstName.Text = view["firstName"].ToString();
                    xrLabelRegister.Text = view["id"].ToString();
                    xrLabelEmdNumber.Text = view["emdNumber"].ToString();
                    xrLabelNumber.Text = view["number"].ToString();
                    DateTime start = Convert.ToDateTime(view["beginDate"]);
                    DateTime end = DateTime.Now;
                    if (view["endDate"] != null && view["endDate"] != DBNull.Value)
                        end = Convert.ToDateTime(view["endDate"]);
                    xrLabelStartDate.Text = start.ToString("yyyy-MM-dd");
                    xrLabelEndDate.Text = end.ToString("yyyy-MM-dd");
                    xrLabelDate.Text = Convert.ToInt32(Math.Round((end - start).TotalDays)).ToString();

                    ReportDataSet.PatientMedicineUserTableAdapter.Fill(ReportDataSet.Instance.PatientMedicineUser, HospitalUtil.getHospitalId(), view["id"].ToString(), view["storeID"].ToString(), type);

                    CreateDocument(true);

                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}
