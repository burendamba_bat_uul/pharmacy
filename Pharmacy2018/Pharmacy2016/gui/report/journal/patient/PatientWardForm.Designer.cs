﻿namespace Pharmacy2016.gui.report.journal.patient
{
    partial class PatientWardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatientWardForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonConvert = new DevExpress.XtraEditors.SimpleButton();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridFieldPatientName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMedicineName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldUnit = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPrice = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSumPrice = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonConvert);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 50);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButtonConvert
            // 
            this.simpleButtonConvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonConvert.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonConvert.Image")));
            this.simpleButtonConvert.Location = new System.Drawing.Point(682, 10);
            this.simpleButtonConvert.Name = "simpleButtonConvert";
            this.simpleButtonConvert.Size = new System.Drawing.Size(90, 23);
            this.simpleButtonConvert.TabIndex = 38;
            this.simpleButtonConvert.Text = "Хөрвүүлэх";
            this.simpleButtonConvert.Click += new System.EventHandler(this.simpleButtonConvert_Click);
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldPatientName,
            this.pivotGridFieldMedicineName,
            this.pivotGridFieldUnit,
            this.pivotGridFieldPrice,
            this.pivotGridFieldCount,
            this.pivotGridFieldSumPrice});
            this.pivotGridControl.Location = new System.Drawing.Point(0, 50);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.Size = new System.Drawing.Size(784, 511);
            this.pivotGridControl.TabIndex = 1;
            // 
            // pivotGridFieldPatientName
            // 
            this.pivotGridFieldPatientName.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldPatientName.AreaIndex = 0;
            this.pivotGridFieldPatientName.Caption = "Эмчлүүлэгч";
            this.pivotGridFieldPatientName.FieldName = "patientName";
            this.pivotGridFieldPatientName.Name = "pivotGridFieldPatientName";
            // 
            // pivotGridFieldMedicineName
            // 
            this.pivotGridFieldMedicineName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldMedicineName.AreaIndex = 0;
            this.pivotGridFieldMedicineName.Caption = "Эм";
            this.pivotGridFieldMedicineName.FieldName = "medicineName";
            this.pivotGridFieldMedicineName.Name = "pivotGridFieldMedicineName";
            // 
            // pivotGridFieldUnit
            // 
            this.pivotGridFieldUnit.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldUnit.AreaIndex = 1;
            this.pivotGridFieldUnit.Caption = "Тун хэмжээ";
            this.pivotGridFieldUnit.FieldName = "unit";
            this.pivotGridFieldUnit.Name = "pivotGridFieldUnit";
            // 
            // pivotGridFieldPrice
            // 
            this.pivotGridFieldPrice.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldPrice.AreaIndex = 2;
            this.pivotGridFieldPrice.Caption = "Үнэ";
            this.pivotGridFieldPrice.FieldName = "price";
            this.pivotGridFieldPrice.Name = "pivotGridFieldPrice";
            this.pivotGridFieldPrice.Width = 80;
            // 
            // pivotGridFieldCount
            // 
            this.pivotGridFieldCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldCount.AreaIndex = 0;
            this.pivotGridFieldCount.Caption = "Тоо хэмжээ";
            this.pivotGridFieldCount.CellFormat.FormatString = "n0";
            this.pivotGridFieldCount.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.FieldName = "count";
            this.pivotGridFieldCount.GrandTotalCellFormat.FormatString = "n0";
            this.pivotGridFieldCount.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Name = "pivotGridFieldCount";
            this.pivotGridFieldCount.Options.ReadOnly = true;
            this.pivotGridFieldCount.TotalCellFormat.FormatString = "n0";
            this.pivotGridFieldCount.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.TotalValueFormat.FormatString = "n0";
            this.pivotGridFieldCount.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.ValueFormat.FormatString = "n0";
            this.pivotGridFieldCount.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Width = 70;
            // 
            // pivotGridFieldSumPrice
            // 
            this.pivotGridFieldSumPrice.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldSumPrice.AreaIndex = 1;
            this.pivotGridFieldSumPrice.Caption = "Нийт дүн";
            this.pivotGridFieldSumPrice.CellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.FieldName = "sumPrice";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.Name = "pivotGridFieldSumPrice";
            this.pivotGridFieldSumPrice.Options.ReadOnly = true;
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.ValueFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // PatientWardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pivotGridControl);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PatientWardForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Тасгийн нэгжийн тайлан";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonConvert;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPatientName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMedicineName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldUnit;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCount;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSumPrice;
    }
}