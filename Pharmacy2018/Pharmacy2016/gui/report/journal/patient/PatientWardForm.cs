﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.report.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.util;

namespace Pharmacy2016.gui.report.journal.patient
{
    public partial class PatientWardForm : DevExpress.XtraEditors.XtraForm
    {
        
        #region Форм анх ачааллах функц

            private static PatientWardForm INSTANCE = new PatientWardForm();

            public static PatientWardForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private PatientWardForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                
                pivotGridControl.OptionsView.ShowDataHeaders = false;
                pivotGridControl.OptionsView.ShowFilterHeaders = false;
                
                pivotGridControl.DataSource = ReportDataSet.PatientWardBindingSource;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(string start, string end, string wardID)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    ReportDataSet.PatientWardTableAdapter.Fill(ReportDataSet.Instance.PatientWard, HospitalUtil.getHospitalId(), start, end, wardID);
                    
                    ShowDialog();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                ReportDataSet.Instance.PatientWard.Clear();
            }

        #endregion


        #region Формын event

            private void simpleButtonConvert_Click(object sender, EventArgs e)
            {
                ProgramUtil.convertGrid(pivotGridControl, "Тасгийн нэгжийн тайлан");
            }

        #endregion

    }
}