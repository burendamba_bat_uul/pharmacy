﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.report.ds;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;

namespace Pharmacy2016.gui.report.journal.patient
{
    /*
     * Тасаг нэгжийн тайлан
     * 
     */ 

    public partial class PatientWardReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

            private static PatientWardReport INSTANCE = new PatientWardReport();

            public static PatientWardReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private PatientWardReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = ReportDataSet.Instance;
                DataAdapter = ReportDataSet.PatientWardTableAdapter;
                DataMember = ReportDataSet.Instance.PatientWard.ToString();
                
                initBinding();
            }

            private void initBinding()
            {
                //this.pivotGridFieldMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MedicineName", "")});
                //this.pivotGridFieldUnit.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Unit", "")});
                //this.xrTableCellShape.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Shape", "")});
                //this.xrTableCellUnit.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Unit", "")});
                
                //this.xrTableCellValiDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ValidDate", ReportUtil.DisplayFormatDate)});
                //this.xrTableCellCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                //this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Price", ReportUtil.DisplayFormatFloat)});
                //this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatFloat)});

                //this.xrTableCellTotalCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatNumber)});
                //this.xrTableCellTotalSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatCurrency)});
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(string start, string end, string wardID)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                    ReportDataSet.PatientWardTableAdapter.Fill(ReportDataSet.Instance.PatientWard, HospitalUtil.getHospitalId(), start, end, wardID);
                    //ReportDataSet.PatientWardTableAdapter.Fill(ReportDataSet.Instance.PatientWard, HospitalUtil.getHospitalId(), start, end, wardID);
                    CreateDocument(true);

                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}
