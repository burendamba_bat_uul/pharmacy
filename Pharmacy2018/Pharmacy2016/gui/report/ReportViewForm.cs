﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using DevExpress.LookAndFeel;
using Pharmacy2016.gui.core.main;

namespace Pharmacy2016.report
{
    public partial class ReportViewForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх үүсгэх функц.

            private static ReportViewForm INSTANCE = new ReportViewForm();

            public static ReportViewForm Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private ReportPrintTool printTool;

            private XtraReport reports;

            private ReportViewForm()
            {
            
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;
            }

        #endregion

        #region Форм харуулах функц.

            public void initAndShow(XtraReport report)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }
                    reports = report;
                    DevExpress.XtraBars.BarManager bar = new DevExpress.XtraBars.BarManager();
                    
                    using (printTool = new ReportPrintTool(report))
                    {
                        DevExpress.XtraBars.BarManager barManager = new DevExpress.XtraBars.BarManager();
                        barManager = printTool.PreviewForm.PrintBarManager;
                        barManager.BeginUpdate();
                        DevExpress.XtraBars.BarSubItem subMenuFile = new DevExpress.XtraBars.BarSubItem(barManager, "Тохиргоо");
                        DevExpress.XtraBars.BarButtonItem buttonEdit = new DevExpress.XtraBars.BarButtonItem(barManager, "Засварлах");
                        buttonEdit.Caption = "Засварлах";
                        buttonEdit.Glyph = Pharmacy2016.PrintRibbonControllerResources.RibbonPrintPreview_EditPageHF;
                        subMenuFile.AddItem(buttonEdit);
                        barManager.MainMenu.AddItem(subMenuFile);
                        buttonEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(buttonEdit_ItemClick);
                        barManager.EndUpdate();
                        printTool.ShowPreviewDialog();
                        printTool.ShowPreview();
                        
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void buttonEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                // Show the End-User Designer form modally. 
                //reports.ShowDesignerDialog();

                // Re-generate the report's print document. 
                //reports.CreateDocument();

            }
        #endregion

    }
}