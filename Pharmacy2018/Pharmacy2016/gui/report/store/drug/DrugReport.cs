﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;

namespace Pharmacy2016.gui.report.store.drug
{
    public partial class DrugReport : DevExpress.XtraReports.UI.XtraReport
    {    
        #region Тайлан анх үүсгэх функц.

            private static DrugReport INSTANCE = new DrugReport();

            public static DrugReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private DrugReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = PlanDataSet.Instance;
                DataAdapter = PlanDataSet.DrugDetailTableAdapter;
                DataMember = PlanDataSet.Instance.DrugDetail.ToString();
                
                initBinding();
            }

            private void initBinding()
            {
                //this.xrTableCellPatient.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UserName", "")});


                //this.xrTableCellMedicineName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MedicineName", "")});
                //this.xrTableCellLatinName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".LatinName", "")});
                //this.xrTableCellUnitType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UnitType", "")});
                //this.xrTableCellQuantity.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Quantity", "")});
                //this.xrTableCellCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
                //this.xrTableCellPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Price", ReportUtil.DisplayFormatFloat)});
                //this.xrTableCellSumCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
                //this.xrTableCellTotalCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Count", ReportUtil.DisplayFormatFloat)});
                //this.xrTableCellSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatFloat)});
                //this.xrTableCellTotalSumPrice.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                //    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SumPrice", ReportUtil.DisplayFormatFloat)});
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(string start, string end, int type)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                    xrLabelHospital.Text = HospitalUtil.getHospitalName();
                    xrLabelStart.Text = start;
                    xrLabelEnd.Text = end;

                    //FilterString = filter;
                    if (type == 1)
                    {
                        xrLabelHeader.Text = "Эмийн зарцуулалтын түүвэр /Сувилагчаар/";
                        pivotGridFieldPatientName.FieldName = "userName";
                        pivotGridFieldSumPrice.Visible = true;
                    }
                    else if (type == 2)
                    {
                        xrLabelHeader.Text = "Эмийн зарцуулалтын түүвэр /Эмчлүүлэгчээр/";
                        pivotGridFieldPatientName.FieldName = "patientName";
                        pivotGridFieldSumPrice.Visible = false;
                    }
                    else if (type == 3)
                    {
                        xrLabelHeader.Text = "Эмийн зарцуулалтын түүвэр /Эмчээр/";
                        pivotGridFieldPatientName.FieldName = "doctorName";
                        pivotGridFieldSumPrice.Visible = true;
                    }    

                    CreateDocument(true);
                    
                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}
