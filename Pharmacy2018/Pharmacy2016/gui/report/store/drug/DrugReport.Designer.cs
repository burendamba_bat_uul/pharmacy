﻿namespace Pharmacy2016.gui.report.store.drug
{
    partial class DrugReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPivotGrid1 = new DevExpress.XtraReports.UI.XRPivotGrid();
            this.pivotGridFieldMedicineName = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldUnit = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldPrice = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldPatientName = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldCount = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.pivotGridFieldSumPrice = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabelHospital = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelStart = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelEnd = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelHeader = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabelStoreman = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelAccount = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPivotGrid1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 235.4792F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPivotGrid1
            // 
            this.xrPivotGrid1.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrPivotGrid1.Appearance.Cell.WordWrap = true;
            this.xrPivotGrid1.Appearance.CustomTotalCell.WordWrap = true;
            this.xrPivotGrid1.Appearance.FieldHeader.WordWrap = true;
            this.xrPivotGrid1.Appearance.FieldValue.WordWrap = true;
            this.xrPivotGrid1.Appearance.Lines.WordWrap = true;
            this.xrPivotGrid1.Dpi = 254F;
            this.xrPivotGrid1.Fields.AddRange(new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField[] {
            this.pivotGridFieldMedicineName,
            this.pivotGridFieldUnit,
            this.pivotGridFieldPrice,
            this.pivotGridFieldPatientName,
            this.pivotGridFieldCount,
            this.pivotGridFieldSumPrice});
            this.xrPivotGrid1.LocationFloat = new DevExpress.Utils.PointFloat(206.375F, 0F);
            this.xrPivotGrid1.Name = "xrPivotGrid1";
            this.xrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3;
            this.xrPivotGrid1.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.xrPivotGrid1.OptionsPrint.PrintHorzLines = DevExpress.Utils.DefaultBoolean.True;
            this.xrPivotGrid1.OptionsPrint.PrintRowHeaders = DevExpress.Utils.DefaultBoolean.True;
            this.xrPivotGrid1.OptionsPrint.PrintVertLines = DevExpress.Utils.DefaultBoolean.True;
            this.xrPivotGrid1.OptionsView.ShowDataHeaders = false;
            this.xrPivotGrid1.SizeF = new System.Drawing.SizeF(2569.014F, 235.4792F);
            // 
            // pivotGridFieldMedicineName
            // 
            this.pivotGridFieldMedicineName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldMedicineName.AreaIndex = 0;
            this.pivotGridFieldMedicineName.Caption = "Эмийн олон улсын нэршил";
            this.pivotGridFieldMedicineName.FieldName = "medicineName";
            this.pivotGridFieldMedicineName.MinWidth = 100;
            this.pivotGridFieldMedicineName.Name = "pivotGridFieldMedicineName";
            this.pivotGridFieldMedicineName.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            this.pivotGridFieldMedicineName.Width = 165;
            // 
            // pivotGridFieldUnit
            // 
            this.pivotGridFieldUnit.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldUnit.AreaIndex = 1;
            this.pivotGridFieldUnit.Caption = "Тун хэмжээ";
            this.pivotGridFieldUnit.FieldName = "unitType";
            this.pivotGridFieldUnit.MinWidth = 50;
            this.pivotGridFieldUnit.Name = "pivotGridFieldUnit";
            this.pivotGridFieldUnit.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            // 
            // pivotGridFieldPrice
            // 
            this.pivotGridFieldPrice.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldPrice.AreaIndex = 2;
            this.pivotGridFieldPrice.Caption = "Нэг бүрийн үнэ";
            this.pivotGridFieldPrice.CellFormat.FormatString = "n2";
            this.pivotGridFieldPrice.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldPrice.FieldName = "price";
            this.pivotGridFieldPrice.MinWidth = 100;
            this.pivotGridFieldPrice.Name = "pivotGridFieldPrice";
            this.pivotGridFieldPrice.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            this.pivotGridFieldPrice.ValueFormat.FormatString = "n2";
            this.pivotGridFieldPrice.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldPatientName
            // 
            this.pivotGridFieldPatientName.Appearance.Cell.WordWrap = true;
            this.pivotGridFieldPatientName.Appearance.FieldValue.WordWrap = true;
            this.pivotGridFieldPatientName.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldPatientName.AreaIndex = 0;
            this.pivotGridFieldPatientName.Caption = "Эмчлүүлэгч";
            this.pivotGridFieldPatientName.FieldName = "patientName";
            this.pivotGridFieldPatientName.Name = "pivotGridFieldPatientName";
            this.pivotGridFieldPatientName.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            // 
            // pivotGridFieldCount
            // 
            this.pivotGridFieldCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldCount.AreaIndex = 0;
            this.pivotGridFieldCount.Caption = "Тоо хэмжээ";
            this.pivotGridFieldCount.CellFormat.FormatString = "n0";
            this.pivotGridFieldCount.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.FieldName = "count";
            this.pivotGridFieldCount.GrandTotalCellFormat.FormatString = "n0";
            this.pivotGridFieldCount.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Name = "pivotGridFieldCount";
            this.pivotGridFieldCount.TotalCellFormat.FormatString = "n0";
            this.pivotGridFieldCount.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.TotalValueFormat.FormatString = "n0";
            this.pivotGridFieldCount.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.ValueFormat.FormatString = "n0";
            this.pivotGridFieldCount.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Width = 80;
            // 
            // pivotGridFieldSumPrice
            // 
            this.pivotGridFieldSumPrice.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldSumPrice.AreaIndex = 1;
            this.pivotGridFieldSumPrice.Caption = "Нийт дүн";
            this.pivotGridFieldSumPrice.CellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.FieldName = "sumPrice";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.Name = "pivotGridFieldSumPrice";
            this.pivotGridFieldSumPrice.Options.ShowValues = false;
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.ValueFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 76F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 168F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelHospital,
            this.xrLabel2,
            this.xrLabelStart,
            this.xrLabel4,
            this.xrLabelEnd,
            this.xrLabelHeader});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 396.875F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabelHospital
            // 
            this.xrLabelHospital.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelHospital.Dpi = 254F;
            this.xrLabelHospital.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabelHospital.LocationFloat = new DevExpress.Utils.PointFloat(50.27083F, 46.08335F);
            this.xrLabelHospital.Name = "xrLabelHospital";
            this.xrLabelHospital.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelHospital.SizeF = new System.Drawing.SizeF(701.5833F, 54.70835F);
            this.xrLabelHospital.StylePriority.UseFont = false;
            this.xrLabelHospital.StylePriority.UseTextAlignment = false;
            this.xrLabelHospital.Text = "xrLabelHospital";
            this.xrLabelHospital.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(232.8333F, 251.0209F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Эхлэх огноо : ";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelStart
            // 
            this.xrLabelStart.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelStart.Dpi = 254F;
            this.xrLabelStart.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabelStart.LocationFloat = new DevExpress.Utils.PointFloat(542.8333F, 251.0209F);
            this.xrLabelStart.Name = "xrLabelStart";
            this.xrLabelStart.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelStart.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabelStart.StylePriority.UseFont = false;
            this.xrLabelStart.StylePriority.UseTextAlignment = false;
            this.xrLabelStart.Text = "xrLabel1";
            this.xrLabelStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(1993.994F, 251.0209F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Дуусах огноо :";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelEnd
            // 
            this.xrLabelEnd.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right;
            this.xrLabelEnd.Dpi = 254F;
            this.xrLabelEnd.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabelEnd.LocationFloat = new DevExpress.Utils.PointFloat(2303.994F, 251.0209F);
            this.xrLabelEnd.Name = "xrLabelEnd";
            this.xrLabelEnd.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelEnd.SizeF = new System.Drawing.SizeF(310F, 60F);
            this.xrLabelEnd.StylePriority.UseFont = false;
            this.xrLabelEnd.StylePriority.UseTextAlignment = false;
            this.xrLabelEnd.Text = "xrLabel1";
            this.xrLabelEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelHeader
            // 
            this.xrLabelHeader.AnchorHorizontal = ((DevExpress.XtraReports.UI.HorizontalAnchorStyles)((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left | DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right)));
            this.xrLabelHeader.Dpi = 254F;
            this.xrLabelHeader.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold);
            this.xrLabelHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 137.5833F);
            this.xrLabelHeader.Name = "xrLabelHeader";
            this.xrLabelHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelHeader.SizeF = new System.Drawing.SizeF(2845F, 58.42001F);
            this.xrLabelHeader.StylePriority.UseFont = false;
            this.xrLabelHeader.StylePriority.UseTextAlignment = false;
            this.xrLabelHeader.Text = "Эмийн зарцуулалтын түүвэр";
            this.xrLabelHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelStoreman,
            this.xrLabel6,
            this.xrLabel9,
            this.xrLabelAccount});
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 243.4167F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabelStoreman
            // 
            this.xrLabelStoreman.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelStoreman.Dpi = 254F;
            this.xrLabelStoreman.LocationFloat = new DevExpress.Utils.PointFloat(1240F, 49.80219F);
            this.xrLabelStoreman.Multiline = true;
            this.xrLabelStoreman.Name = "xrLabelStoreman";
            this.xrLabelStoreman.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelStoreman.SizeF = new System.Drawing.SizeF(757.9374F, 60F);
            this.xrLabelStoreman.StylePriority.UseFont = false;
            this.xrLabelStoreman.StylePriority.UseTextAlignment = false;
            this.xrLabelStoreman.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabelStoreman.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(802.1251F, 49.80219F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(437.875F, 59.99998F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Хөтөлсөн сувилагч : ";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel9
            // 
            this.xrLabel9.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(802.125F, 109.8021F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(437.8751F, 60F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Шалгасан эм зүйч : ";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabelAccount
            // 
            this.xrLabelAccount.AnchorHorizontal = DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left;
            this.xrLabelAccount.Dpi = 254F;
            this.xrLabelAccount.LocationFloat = new DevExpress.Utils.PointFloat(1240F, 109.8021F);
            this.xrLabelAccount.Multiline = true;
            this.xrLabelAccount.Name = "xrLabelAccount";
            this.xrLabelAccount.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelAccount.SizeF = new System.Drawing.SizeF(757.9374F, 59.99995F);
            this.xrLabelAccount.StylePriority.UseFont = false;
            this.xrLabelAccount.StylePriority.UseTextAlignment = false;
            this.xrLabelAccount.Text = "................................................../                              " +
    "  /\r\n";
            this.xrLabelAccount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DrugReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(64, 61, 76, 168);
            this.PageHeight = 2100;
            this.PageWidth = 2970;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportPrintOptions.DetailCount = 1;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "15.1";
            this.VerticalContentSplitting = DevExpress.XtraPrinting.VerticalContentSplitting.Smart;
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabelHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabelStart;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabelEnd;
        private DevExpress.XtraReports.UI.XRPivotGrid xrPivotGrid1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldMedicineName;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldUnit;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldPrice;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldPatientName;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldCount;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField pivotGridFieldSumPrice;
        private DevExpress.XtraReports.UI.XRLabel xrLabelHospital;
        private DevExpress.XtraReports.UI.XRLabel xrLabelStoreman;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabelAccount;
    }
}
