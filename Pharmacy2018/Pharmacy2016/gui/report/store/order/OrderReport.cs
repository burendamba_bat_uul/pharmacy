﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;

namespace Pharmacy2016.gui.report.store.order
{
    /*
     * Хүлээлгийн жагсаалтын тайлан
     * 
     */ 

    public partial class OrderReport : DevExpress.XtraReports.UI.XtraReport
    {
        
        #region Тайлан анх үүсгэх функц.

            private static OrderReport INSTANCE = new OrderReport();

            public static OrderReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private OrderReport()
            {
                
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = StoreDataSet.Instance;
                DataAdapter = StoreDataSet.OrderTableAdapter;
                DataMember = StoreDataSet.Instance.Order.ToString();
                
                initBinding();
            }

            private void initBinding()
            {
                this.GroupHeader.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                        new DevExpress.XtraReports.UI.GroupField("OrderState", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
                
                this.xrTableCellGroup.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".OrderStateText", "")});

                this.xrTableCellNumber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Number", "")});
                this.xrTableCellDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Date", ReportUtil.DisplayFormatDate)});
                this.xrTableCellLastName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".LastName", "")});
                this.xrTableCellFirstName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".FirstName", "")});
                this.xrTableCellRegister.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UserID", "")});
                this.xrTableCellDescription.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Description", "")});
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(string start, string end,string filter)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);

                    xrLabelStart.Text = start;
                    xrLabelEnd.Text = end;

                    FilterString = filter;
                    CreateDocument(true);
                    
                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}
