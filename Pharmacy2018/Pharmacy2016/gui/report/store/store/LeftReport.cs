﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.store.store;
using System.Data;

namespace Pharmacy2016.gui.report.store.store
{
    /*
     * Гарах үеийн эпикризийн тайлан харах report.
     * 
     */

    public partial class LeftReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static LeftReport INSTANCE = new LeftReport();

        public static LeftReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private LeftReport()
        {

        }

        private void initReport()
        {
            InitializeComponent();
            isInit = true;

            DataSource = StoreDataSet.Instance;
            DataAdapter = StoreDataSet.StoreLeftTableAdapter;
            DataMember = StoreDataSet.Instance.StoreLeft.ToString();

            initBinding();
        }

        private void initBinding()
        {
            this.xrTableCellEndHurt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndHurtText", "")});
            this.xrTableCellLeaveHospital.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".LeaveHospitalText", "")});
            this.xrTableCellEndDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndDate", ReportUtil.DisplayFormatDate)});
            this.xrTableCellDuration.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Duration", "")});
            this.xrTableCellBill.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Bill", "")});
            this.xrLabelBilloFMedic.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BillOfMedic", "")});
            this.xrTableCellInCome.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".InComeText", "")});
            this.xrTableCellInBack.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".InBackText", "")});
            this.xrTableCellAsTreat.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".AnalysisTreatText", "")});
            this.xrTableCellAsLight.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".AnalysisLightText", "")});
            this.xrTableCellAnalysisChanged.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".AnalysisChanged", "")});
            this.xrTableCellAnalysisGeneral.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".AnalysisGeneral", "")});
            this.xrTableCellAnalysisBlood.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".AnalysisBlood", "")});
            this.xrTableCellAnalysisVirus.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".AnalysisVirus", "")});
            this.xrTableCellTreat.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TreatText", "")});
            this.xrTableCellOtherTreat.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".OtherTreat", "")});
            this.xrTableCellBlood.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BloodText", "")});
            this.xrTableCellIsSurgery.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsSurgeryText", "")});
            this.xrTableCellSurgery.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Surgery", "")});
            this.xrTableCellIsHeal.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsHealText", "")});
            this.xrTableCellIsAfterSurgery.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsAfterSurgeryText", "")});
            this.xrTableCellAfterSurgery.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".AfterSurgery", "")});
            this.xrLabelUserID.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".FullName", "")});         

            this.xrLabeLeftDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndDate", ReportUtil.DisplayFormatDate)});

            this.xrLabelDiagnoseType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DiagnoseTypeName", ReportUtil.DisplayFormatDate)});

            //this.xrLabelFirstName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            //    new DevExpress.XtraReports.UI.XRBinding("Text", null, ReportHeader.Report.DataMember + ".FirstName", ReportUtil.DisplayFormatDate)});

            //this.xrLabelLastName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            //    new DevExpress.XtraReports.UI.XRBinding("Text", null, ReportHeader.Report.DataMember + ".LastName", ReportUtil.DisplayFormatDate)});

            //this.xrLabelGender.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            //    new DevExpress.XtraReports.UI.XRBinding("Text", null, ReportHeader.Report.DataMember + ".Gender", ReportUtil.DisplayFormatDate)});

            //this.xrLabelAge.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            //    new DevExpress.XtraReports.UI.XRBinding("Text", null, ReportHeader.Report.DataMember + ".Birthday", ReportUtil.DisplayFormatDate)});

        }

        public void initAndShow(string storeID, DataRowView view)
        {
            if (!isInit)
            {
                initReport();
            }            
            xrLabelFirstName.Text = view["firstName"].ToString();
            xrLabelLastName.Text = view["lastName"].ToString();
            xrLabelGender.Text = view["genderText"].ToString();
            DateTime start = Convert.ToDateTime(view["birthday"]);
            DateTime end = DateTime.Now;
            xrLabelAge.Text = (end.Year - start.Year).ToString();

            FilterString = "storeID = '" + storeID + "'";
            CreateDocument(true);
        }
    }
}
