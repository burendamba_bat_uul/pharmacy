﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.util;
using Pharmacy2016.report;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.store.store;

namespace Pharmacy2016.gui.report.store.store
{
    /*
     * Эмчлүүлэгчийн анамнэз тайлан харах цонх.
     * 
     */

    public partial class AnamnezReport : DevExpress.XtraReports.UI.XtraReport
    {

        #region Тайлан анх үүсгэх функц.

            private static AnamnezReport INSTANCE = new AnamnezReport();

            public static AnamnezReport Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private AnamnezReport()
            {

            }

            private void initBinding()
            {

                this.xrTableCellHigh.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".High", "")});
                this.xrTableCellWeight.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Weight", "")});
                this.xrTableCellBodyIndex.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BodyIndex", "")});
                this.xrTableCellPressure.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BloodPressureText", "")});
                this.xrTableCellPain.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Pain", "")});
                this.xrTableCellHistory.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".History", "")});
                this.xrTableCellLifeStore.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".LifeStore", "")});
                this.xrTableCellLifeTerm.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".LifeTermText", "")});
                this.xrTableCellWorkTerm.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".WorkTermText", "")});
                this.xrTableCellInfection.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".InfectionText", "")});
                this.xrTableCellSurgery.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsSurgeryText", "")});
                this.xrTableCellAccident.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Accident", "")});
                this.xrTableCellAllergy.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".AllergyText", "")});
                this.xrTableCellInheritance.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Inheritance", "")});
                this.xrTableCellDiet.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DietText", "")});
                this.xrTableCellIsVodka.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsVodkaText", "")});
                this.xrTableCellIsSmoke.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsSmokeText", "")});
                this.xrTableCellHowLong.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".HowLong", "")});
            }

            private void initReport()
            {
                InitializeComponent();
                isInit = true;

                DataSource = StoreDataSet.Instance;
                DataAdapter = StoreDataSet.StoreHistoryTableAdapter;
                DataMember = StoreDataSet.Instance.StoreHistory.ToString();

                initBinding();
            }

            public void initAndShow(string storeID) 
            {
                if (!isInit)
                {
                    initReport();
                }

                FilterString = "storeID = '" + storeID + "'";
                CreateDocument(true);
            }

        #endregion

    }
}
