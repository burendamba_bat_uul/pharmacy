﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.util;

namespace Pharmacy2016.gui.report.store.store
{
    /*
     * Эмчлүүлэгчийн тасаг өрөөний тайлан харах report.
     * 
     */

    public partial class WardRoomReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static WardRoomReport INSTANCE = new WardRoomReport();

        public static WardRoomReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private WardRoomReport()
        {

        }

        private void initReport()
        {
            InitializeComponent();
            isInit = true;

            DataSource = StoreDataSet.Instance;
            DataAdapter = StoreDataSet.StoreWardRoomTableAdapter;
            DataMember = StoreDataSet.Instance.StoreWardRoom.ToString();

            initBinding();
        }

        private void initBinding()
        {

            this.xrTableCellBeginDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".StartDate", ReportUtil.DisplayFormatDate)});
            this.xrTableCellEndDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EndDate", ReportUtil.DisplayFormatDate)});
            this.xrTableCellWardName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".WardName", "")});
            this.xrTableCellBedName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BedName", "")});
            this.xrTableCellDesc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Description", "")});
            this.xrTableCellIsCur.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsCurText", "")});
        }

        public void initAndShow(string storeID)
        {
            if (!isInit)
            {
                initReport();
            }

            FilterString = "storeID = '" + storeID + "'";
            CreateDocument(true);
        }

    }
}
