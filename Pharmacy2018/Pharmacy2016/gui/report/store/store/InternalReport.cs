﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;

namespace Pharmacy2016.gui.report.store.store
{
    /*
     * Дотрын үзлэгийн тайлан харах report.
     * 
     */

    public partial class InternalReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static InternalReport INSTANCE = new InternalReport();

        public static InternalReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private InternalReport()
        {

        }

        private void initReport()
        {
            InitializeComponent();
            isInit = true;

            DataSource = StoreDataSet.Instance;
            DataAdapter = StoreDataSet.StoreInternalinspectionTableAdapter;
            DataMember = StoreDataSet.Instance.StoreInternalinspection.ToString();

            initBinding();
        }

        private void initBinding()
        {
            this.xrTableCellIsBreathing.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsBreathingText", "")});
            this.xrTableCellIsCyanosis.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsCyanosisText", "")});
            this.xrTableCellIfCyanosis.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IfCyanosisText", "")});
            this.xrTableCellIsBreathMuscles.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsBreathMusclesText", "")});
            this.xrTableCellBreathCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BreathCount", "")});
            this.xrTableCellIsCount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsCountText", "")});
            this.xrTableCellChestShape.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ChestShapeText", "")});
            this.xrTableCellBreathShape.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BreathShapeText", "")});
            this.xrTableCellBreathChest.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BreathChestText", "")});
            this.xrTableCellRightLeft.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".RightLeftText", "")});
            this.xrTableCellInternalConcern.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ConcernText", "")});
            this.xrTableCellIsFlexible.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsFlexibleText", "")});
            this.xrTableCellSoundVib.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SoundVibText", "")});
            this.xrTableCellPercussion.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PercussionText", "")});
            this.xrTableCellPartPercussion.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PartPercussionText", "")});
            this.xrTableCellBreathDouble.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BreathDoubleText", "")});
            this.xrTableCellIsDesease.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsDeseaseText", "")});
            this.xrTableCellIfDesease.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IfDeseaseText", "")});
            this.xrTableCellIsNoisy.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsNoisyText", "")});
            this.xrTableCellIfNoisy.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IfNoisyText", "")});
            this.xrTableCellBronkhofoni.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BronkhofoniText", "")});
        }

        public void initAndShow(string storeID)
        {
            if (!isInit)
            {
                initReport();
            }

            FilterString = "storeID = '" + storeID + "'";
            CreateDocument(true);
        }
    }
}
