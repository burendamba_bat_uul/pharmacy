﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;

namespace Pharmacy2016.gui.report.store.store
{
    /*
     * Хоол боловсруулах тогтолцоо тайлан харах report.
     * 
     */

    public partial class DigestiveReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static DigestiveReport INSTANCE = new DigestiveReport();

        public static DigestiveReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private DigestiveReport()
        {

        }

        private void initReport()
        {
            InitializeComponent();
            isInit = true;

            DataSource = StoreDataSet.Instance;
            DataAdapter = StoreDataSet.StoreDigestiveTableAdapter;
            DataMember = StoreDataSet.Instance.StoreDigestive.ToString();

            initBinding();
        }

        private void initBinding()
        {
            this.xrTableCellIsTongue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsTongueText", "")});
            this.xrTableCellIsSkingWet.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsSkingWetText", "")});
            this.xrTableCellColor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Color", "")});
            this.xrTableCellIsTuck.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsTuckText", "")});
            this.xrTableCellTongueShape.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TongueShape", "")});
            this.xrTableCellTuckConcern.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TuckConcernText", "")});
            this.xrTableCellIsTough.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsToughText", "")});
            this.xrTableCellIsTympanic.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsTympanicText", "")});
            this.xrTableCellIsPercussion.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsPercussionText", "")});
            this.xrTableCellIsMoving.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsMovingText", "")});
            this.xrTableCellBellyConcern.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BellyConcernText", "")});
            this.xrTableCellIsBellyStable.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsBellyStableText", "")});
            this.xrTableCellIsBellyPos.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsBellyPosText", "")});
            this.xrTableCellPosConcern.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PosConcernText", "")});
            this.xrTableCellIsPosStable.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsPosStableText", "")});
            this.xrTableCellIsPos.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsPosText", "")});
            this.xrTableCellCrossConcern.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".CrossConcernText", "")});
            this.xrTableCellIsCrossStable.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsCrossStableText", "")});
            this.xrTableCellIsCrossPos.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsCrossPosText", "")});
            this.xrTableCellTideConcern.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TideConcernText", "")});
            this.xrTableCellIsTideStable.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsTideStableText", "")});
            this.xrTableCellIsTidePos.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsTidePosText", "")});
            this.xrTableCellLiver.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".LiverText", "")});
            this.xrTableCellDuringDay.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DuringDay", "")});
            this.xrTableCellLiverSize.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".LiverSizeText", "")});
            this.xrTableCellLiverDirection.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".LiverDirectionText", "")});
            this.xrTableCellSpleen.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SpleenText", "")});
        }

        public void initAndShow(string storeID)
        {
            if (!isInit)
            {
                initReport();
            }

            FilterString = "storeID = '" + storeID + "'";
            CreateDocument(true);
        }

    }
}
