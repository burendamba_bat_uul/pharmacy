﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.util;
using DevExpress.XtraEditors;
using Pharmacy2016.report;
using Pharmacy2016.gui.core.store.store;
using System.Data;

namespace Pharmacy2016.gui.report.store.store
{
    /*
     * Өвчний түүхийн тайлан харах report.
     * Гарах үеийн онош тайлан харах report.
     */

    public partial class StoreReport : DevExpress.XtraReports.UI.XtraReport
    {
        
        #region Тайлан анх үүсгэх функц.

            private static StoreReport INSTANCE = new StoreReport();

            public static StoreReport Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private StoreReport()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                DataSource = StoreDataSet.Instance;
                DataAdapter = StoreDataSet.StoreTableAdapter;
                DataMember = StoreDataSet.Instance.Store.ToString();

                DetailReport.DataSource = StoreDataSet.Instance;
                DetailReport.DataAdapter = StoreDataSet.StoreLeftDiagnoseTableAdapter;
                DetailReport.DataMember = "Store.Store_StoreLeftDiagnose";

                this.xrSubreportAnamnez.ReportSource = AnamnezReport.Instance;
                this.xrSubreportGeneral.ReportSource = GeneralReport.Instance;
                this.xrSubreportInternal.ReportSource = InternalReport.Instance;
                this.xrSubreportHeart.ReportSource = HeartReport.Instance;
                this.xrSubreportDigestive.ReportSource = DigestiveReport.Instance;
                this.xrSubreportUrine.ReportSource = UrineReport.Instance;
                this.xrSubreportDiagnose.ReportSource = DiagnoseReport.Instance;
                this.xrSubreportInspection.ReportSource = InspectionReport.Instance;
                this.xrSubreportLeft.ReportSource = LeftReport.Instance;
                this.xrSubreportWardRoom.ReportSource = WardRoomReport.Instance;
                this.xrSubreportValidity.ReportSource = ValidityReport.Instance;


                initBinding();
                
            } 

            private void initBinding()
            {
                this.xrLabelNumber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Number", "")});

                this.xrTableCellRegister.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PatientID", "")});

                this.xrTableCellEmdNumber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EmdNumber", "")});
                this.xrTableCellStartDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BeginDate", ReportUtil.DisplayFormatDate)});

                this.xrTableCellWardName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".WardName", "")});

                this.xrTableCellfirstName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".FirstName", "")});
                this.xrTableCellLastName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".LastName", "")});
                this.xrTableCellBirthDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Birthday", ReportUtil.DisplayFormatDate)});
                this.xrTableCellGender.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".GenderText", "")});
                this.xrTableCellIsMarried.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsMarriedText", "")});
                this.xrTableCellEducation.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EducationText", "")});
                this.xrTableCellProvince.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ProvinceName", "")});
                this.xrTableCellAddress.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Address", "")});
                this.xrTableCellOgranization.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".OrganizationName", "")});
                this.xrTableCellPosition.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PositionName", "")});
                this.xrTableCellSpecialty.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SpecialtyName", "")});
                this.xrTableCellEmergencyPhone.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EmergencyPhone", "")});
                this.xrTableCellEmergencyName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EmergencyName", "")});
                this.xrTableCellPaymentType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PaymentTypeText", "")});
                this.xrTableCellIsUnder.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsUnderText", "")});
                this.xrTableCellAllergyType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".AlergyTypeText", "")});
                this.xrTableCellLoopState.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsLoopText", "")});
                this.xrTableCellDiagnose.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BeginDiagnose", "")});

                this.xrTableCellDiagnoseType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".DiagnoseTypeName", "")});
                this.xrTableCellDescription.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Description", "")});
                this.xrTableCellCode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Code", "")});
            }

        #endregion

        #region Тайлан харуулах функц.

            public void initAndShow(int type, string storeID, DataRowView view)
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }

                    ProgramUtil.showWaitDialog(ProgramUtil.PRINT_TITLE);


                    this.xrPictureBoxHospitalLogo.Image = HospitalUtil.getHospitalPicture();

                    FilterString = "id = '" + storeID + "'";

                    AnamnezReport.Instance.initAndShow(storeID);
                    GeneralReport.Instance.initAndShow(storeID);
                    InternalReport.Instance.initAndShow(storeID);
                    HeartReport.Instance.initAndShow(storeID);
                    DigestiveReport.Instance.initAndShow(storeID);
                    UrineReport.Instance.initAndShow(storeID);
                    DiagnoseReport.Instance.initAndShow(storeID);
                    InspectionReport.Instance.initAndShow(storeID);
                    LeftReport.Instance.initAndShow(storeID, view);
                    WardRoomReport.Instance.initAndShow(storeID);
                    ValidityReport.Instance.initAndShow(storeID);
                   
                    CreateDocument(true);
                               
                    ProgramUtil.closeWaitDialog();
                    ReportViewForm.Instance.initAndShow(INSTANCE);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Тайлан хэвлэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion
  
    }
}
