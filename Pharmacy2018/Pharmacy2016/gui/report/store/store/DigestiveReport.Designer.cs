﻿namespace Pharmacy2016.gui.report.store.store
{
    partial class DigestiveReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellIsTongue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTuckConcern = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIsTympanic = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIsMoving = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellIsSkingWet = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIsTough = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIsPercussion = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellColor = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIsTuck = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTongueShape = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellBellyConcern = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellPosConcern = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellCrossConcern = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellTideConcern = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellIsBellyStable = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIsPosStable = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIsCrossStable = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIsTideStable = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellIsBellyPos = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIsPos = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIsCrossPos = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIsTidePos = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellLiver = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDuringDay = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellLiverSize = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellSpleen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellLiverDirection = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 1304.083F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow38,
            this.xrTableRow34});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1700F, 1304.083F);
            this.xrTable2.StylePriority.UseBorders = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell47});
            this.xrTableRow18.Dpi = 254F;
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = " Хоол боловсруулах эрхтэн тогтолцоо";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell47.Weight = 3D;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.xrTableCell50,
            this.xrTableCell52,
            this.xrTableCell49});
            this.xrTableRow19.Dpi = 254F;
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseFont = false;
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.Text = " Харж ажиглах:";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell51.Weight = 0.75D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseFont = false;
            this.xrTableCell50.StylePriority.UseTextAlignment = false;
            this.xrTableCell50.Text = " Өнгөц тэмтрэлтээр:";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell50.Weight = 0.74999994614545429D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseFont = false;
            this.xrTableCell52.StylePriority.UseTextAlignment = false;
            this.xrTableCell52.Text = " Тогшилтоор:";
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell52.Weight = 0.74999973072727111D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseFont = false;
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.Text = " Чагналтаар";
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell49.Weight = 0.7500003231272746D;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell56});
            this.xrTableRow20.Dpi = 254F;
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1.7499998451023755D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StylePriority.UseFont = false;
            this.xrTableCell53.StylePriority.UseTextAlignment = false;
            this.xrTableCell53.Text = " Хэл өнгөртэй эсэх:";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell53.Weight = 0.75D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseFont = false;
            this.xrTableCell54.StylePriority.UseTextAlignment = false;
            this.xrTableCell54.Text = " Хэвлий эмзэглэлтэй эсэх:";
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell54.Weight = 0.74999994614545429D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.Text = " Хэвлийн хэнгэрэгэн чимээ:";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 0.74999973072727111D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseFont = false;
            this.xrTableCell56.StylePriority.UseTextAlignment = false;
            this.xrTableCell56.Text = " Гэдэсний гүрвэлзэх хөдөлгөөн:";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell56.Weight = 0.7500003231272746D;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellIsTongue,
            this.xrTableCellTuckConcern,
            this.xrTableCellIsTympanic,
            this.xrTableCellIsMoving});
            this.xrTableRow21.Dpi = 254F;
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1.7499998451023755D;
            // 
            // xrTableCellIsTongue
            // 
            this.xrTableCellIsTongue.Dpi = 254F;
            this.xrTableCellIsTongue.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsTongue.Name = "xrTableCellIsTongue";
            this.xrTableCellIsTongue.StylePriority.UseFont = false;
            this.xrTableCellIsTongue.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsTongue.Text = "xrTableCellIsTongue";
            this.xrTableCellIsTongue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsTongue.Weight = 0.75D;
            // 
            // xrTableCellTuckConcern
            // 
            this.xrTableCellTuckConcern.Dpi = 254F;
            this.xrTableCellTuckConcern.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellTuckConcern.Name = "xrTableCellTuckConcern";
            this.xrTableCellTuckConcern.StylePriority.UseFont = false;
            this.xrTableCellTuckConcern.StylePriority.UseTextAlignment = false;
            this.xrTableCellTuckConcern.Text = "xrTableCellTuckConcern";
            this.xrTableCellTuckConcern.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellTuckConcern.Weight = 0.74999994614545429D;
            // 
            // xrTableCellIsTympanic
            // 
            this.xrTableCellIsTympanic.Dpi = 254F;
            this.xrTableCellIsTympanic.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsTympanic.Name = "xrTableCellIsTympanic";
            this.xrTableCellIsTympanic.StylePriority.UseFont = false;
            this.xrTableCellIsTympanic.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsTympanic.Text = "xrTableCellIsTympanic";
            this.xrTableCellIsTympanic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsTympanic.Weight = 0.74999973072727111D;
            // 
            // xrTableCellIsMoving
            // 
            this.xrTableCellIsMoving.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCellIsMoving.Dpi = 254F;
            this.xrTableCellIsMoving.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsMoving.Name = "xrTableCellIsMoving";
            this.xrTableCellIsMoving.StylePriority.UseBorders = false;
            this.xrTableCellIsMoving.StylePriority.UseFont = false;
            this.xrTableCellIsMoving.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsMoving.Text = "xrTableCellIsMoving";
            this.xrTableCellIsMoving.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsMoving.Weight = 0.7500003231272746D;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64});
            this.xrTableRow22.Dpi = 254F;
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 2.0000002268096084D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseFont = false;
            this.xrTableCell61.StylePriority.UseTextAlignment = false;
            this.xrTableCell61.Text = " Арьс, салст-чийглэг";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell61.Weight = 0.75D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell62.Multiline = true;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseFont = false;
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            this.xrTableCell62.Text = "Булчингийн чангарал\r\nбайгаа эсэх:";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell62.Weight = 0.74999994614545429D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell63.Multiline = true;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseFont = false;
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.Text = "Ихэссэн хэсэгт\r\nтогшилтын дуу";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell63.Weight = 0.74999973072727111D;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.StylePriority.UseFont = false;
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell64.Weight = 0.7500003231272746D;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellIsSkingWet,
            this.xrTableCellIsTough,
            this.xrTableCellIsPercussion,
            this.xrTableCell68});
            this.xrTableRow23.Dpi = 254F;
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1.1000002313960788D;
            // 
            // xrTableCellIsSkingWet
            // 
            this.xrTableCellIsSkingWet.Dpi = 254F;
            this.xrTableCellIsSkingWet.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsSkingWet.Name = "xrTableCellIsSkingWet";
            this.xrTableCellIsSkingWet.StylePriority.UseFont = false;
            this.xrTableCellIsSkingWet.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsSkingWet.Text = "xrTableCellIsSkingWet";
            this.xrTableCellIsSkingWet.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsSkingWet.Weight = 0.75D;
            // 
            // xrTableCellIsTough
            // 
            this.xrTableCellIsTough.Dpi = 254F;
            this.xrTableCellIsTough.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsTough.Name = "xrTableCellIsTough";
            this.xrTableCellIsTough.StylePriority.UseFont = false;
            this.xrTableCellIsTough.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsTough.Text = "xrTableCellIsTough";
            this.xrTableCellIsTough.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsTough.Weight = 0.74999994614545429D;
            // 
            // xrTableCellIsPercussion
            // 
            this.xrTableCellIsPercussion.Dpi = 254F;
            this.xrTableCellIsPercussion.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsPercussion.Name = "xrTableCellIsPercussion";
            this.xrTableCellIsPercussion.StylePriority.UseFont = false;
            this.xrTableCellIsPercussion.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsPercussion.Text = "xrTableCellIsPercussion";
            this.xrTableCellIsPercussion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsPercussion.Weight = 0.74999973072727111D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell68.Multiline = true;
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.StylePriority.UseFont = false;
            this.xrTableCell68.StylePriority.UseTextAlignment = false;
            this.xrTableCell68.Text = "\r\n";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell68.Weight = 0.7500003231272746D;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.xrTableCellColor,
            this.xrTableCell74,
            this.xrTableCellIsTuck,
            this.xrTableCell71,
            this.xrTableCellTongueShape});
            this.xrTableRow24.Dpi = 254F;
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1.1000002313960788D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseFont = false;
            this.xrTableCell73.StylePriority.UseTextAlignment = false;
            this.xrTableCell73.Text = " Өнгө";
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell73.Weight = 0.1932353206330911D;
            // 
            // xrTableCellColor
            // 
            this.xrTableCellColor.Dpi = 254F;
            this.xrTableCellColor.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellColor.Name = "xrTableCellColor";
            this.xrTableCellColor.StylePriority.UseFont = false;
            this.xrTableCellColor.StylePriority.UseTextAlignment = false;
            this.xrTableCellColor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellColor.Weight = 0.55676449087599877D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Dpi = 254F;
            this.xrTableCell74.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseFont = false;
            this.xrTableCell74.StylePriority.UseTextAlignment = false;
            this.xrTableCell74.Text = " Хэвлийн - хэм";
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell74.Weight = 0.44503684045863684D;
            // 
            // xrTableCellIsTuck
            // 
            this.xrTableCellIsTuck.Dpi = 254F;
            this.xrTableCellIsTuck.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsTuck.Name = "xrTableCellIsTuck";
            this.xrTableCellIsTuck.StylePriority.UseFont = false;
            this.xrTableCellIsTuck.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsTuck.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsTuck.Weight = 0.65808873556060887D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Dpi = 254F;
            this.xrTableCell71.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.StylePriority.UseTextAlignment = false;
            this.xrTableCell71.Text = " Хэлбэр";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell71.Weight = 0.3968741816352982D;
            // 
            // xrTableCellTongueShape
            // 
            this.xrTableCellTongueShape.Dpi = 254F;
            this.xrTableCellTongueShape.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellTongueShape.Name = "xrTableCellTongueShape";
            this.xrTableCellTongueShape.StylePriority.UseFont = false;
            this.xrTableCellTongueShape.StylePriority.UseTextAlignment = false;
            this.xrTableCellTongueShape.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellTongueShape.Weight = 0.75000043083636625D;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell80});
            this.xrTableRow25.Dpi = 254F;
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1.1000002313960788D;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Dpi = 254F;
            this.xrTableCell80.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseFont = false;
            this.xrTableCell80.StylePriority.UseTextAlignment = false;
            this.xrTableCell80.Text = " Гүнзгий тэмтрэлтээр:";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell80.Weight = 3D;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.xrTableCell76,
            this.xrTableCell78,
            this.xrTableCell75});
            this.xrTableRow26.Dpi = 254F;
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1.899999851874034D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Dpi = 254F;
            this.xrTableCell77.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseFont = false;
            this.xrTableCell77.StylePriority.UseTextAlignment = false;
            this.xrTableCell77.Text = "Тахир гэдэс - байрлал";
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell77.Weight = 0.75D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Dpi = 254F;
            this.xrTableCell76.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell76.Multiline = true;
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseFont = false;
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.Text = "Өсгөх болон уруудах\r\nгэдэс: - Байрлал";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell76.Weight = 0.75D;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Dpi = 254F;
            this.xrTableCell78.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell78.Multiline = true;
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.StylePriority.UseFont = false;
            this.xrTableCell78.StylePriority.UseTextAlignment = false;
            this.xrTableCell78.Text = "Хөндлөн гэдэс:\r\nБайрлал";
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell78.Weight = 0.75D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Dpi = 254F;
            this.xrTableCell75.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell75.Multiline = true;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseFont = false;
            this.xrTableCell75.StylePriority.UseTextAlignment = false;
            this.xrTableCell75.Text = "Цутгалан гэдэс:\r\nБайрлал";
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell75.Weight = 0.75D;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellBellyConcern,
            this.xrTableCellPosConcern,
            this.xrTableCellCrossConcern,
            this.xrTableCellTideConcern});
            this.xrTableRow27.Dpi = 254F;
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1.0393707263228456D;
            // 
            // xrTableCellBellyConcern
            // 
            this.xrTableCellBellyConcern.Dpi = 254F;
            this.xrTableCellBellyConcern.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellBellyConcern.Name = "xrTableCellBellyConcern";
            this.xrTableCellBellyConcern.StylePriority.UseFont = false;
            this.xrTableCellBellyConcern.StylePriority.UseTextAlignment = false;
            this.xrTableCellBellyConcern.Text = "xrTableCellBellyConcern";
            this.xrTableCellBellyConcern.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellBellyConcern.Weight = 0.75D;
            // 
            // xrTableCellPosConcern
            // 
            this.xrTableCellPosConcern.Dpi = 254F;
            this.xrTableCellPosConcern.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellPosConcern.Name = "xrTableCellPosConcern";
            this.xrTableCellPosConcern.StylePriority.UseFont = false;
            this.xrTableCellPosConcern.StylePriority.UseTextAlignment = false;
            this.xrTableCellPosConcern.Text = "xrTableCellPosConcern";
            this.xrTableCellPosConcern.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellPosConcern.Weight = 0.75D;
            // 
            // xrTableCellCrossConcern
            // 
            this.xrTableCellCrossConcern.Dpi = 254F;
            this.xrTableCellCrossConcern.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellCrossConcern.Name = "xrTableCellCrossConcern";
            this.xrTableCellCrossConcern.StylePriority.UseFont = false;
            this.xrTableCellCrossConcern.StylePriority.UseTextAlignment = false;
            this.xrTableCellCrossConcern.Text = "xrTableCellCrossConcern";
            this.xrTableCellCrossConcern.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellCrossConcern.Weight = 0.75D;
            // 
            // xrTableCellTideConcern
            // 
            this.xrTableCellTideConcern.Dpi = 254F;
            this.xrTableCellTideConcern.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellTideConcern.Name = "xrTableCellTideConcern";
            this.xrTableCellTideConcern.StylePriority.UseFont = false;
            this.xrTableCellTideConcern.StylePriority.UseTextAlignment = false;
            this.xrTableCellTideConcern.Text = "xrTableCellTideConcern";
            this.xrTableCellTideConcern.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellTideConcern.Weight = 0.75D;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell84,
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell87});
            this.xrTableRow28.Dpi = 254F;
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1.0393707263228456D;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Dpi = 254F;
            this.xrTableCell84.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseFont = false;
            this.xrTableCell84.StylePriority.UseTextAlignment = false;
            this.xrTableCell84.Text = "Тогтоц";
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell84.Weight = 0.75D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Dpi = 254F;
            this.xrTableCell85.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseFont = false;
            this.xrTableCell85.StylePriority.UseTextAlignment = false;
            this.xrTableCell85.Text = "Тогтоц";
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell85.Weight = 0.75D;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Dpi = 254F;
            this.xrTableCell86.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseFont = false;
            this.xrTableCell86.StylePriority.UseTextAlignment = false;
            this.xrTableCell86.Text = "Тогтоц";
            this.xrTableCell86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell86.Weight = 0.75D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Dpi = 254F;
            this.xrTableCell87.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseFont = false;
            this.xrTableCell87.StylePriority.UseTextAlignment = false;
            this.xrTableCell87.Text = "Тогтоц";
            this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell87.Weight = 0.75D;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellIsBellyStable,
            this.xrTableCellIsPosStable,
            this.xrTableCellIsCrossStable,
            this.xrTableCellIsTideStable});
            this.xrTableRow29.Dpi = 254F;
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1.0393707263228456D;
            // 
            // xrTableCellIsBellyStable
            // 
            this.xrTableCellIsBellyStable.Dpi = 254F;
            this.xrTableCellIsBellyStable.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsBellyStable.Name = "xrTableCellIsBellyStable";
            this.xrTableCellIsBellyStable.StylePriority.UseFont = false;
            this.xrTableCellIsBellyStable.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsBellyStable.Text = "xrTableCellIsBellyStable";
            this.xrTableCellIsBellyStable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsBellyStable.Weight = 0.75D;
            // 
            // xrTableCellIsPosStable
            // 
            this.xrTableCellIsPosStable.Dpi = 254F;
            this.xrTableCellIsPosStable.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsPosStable.Name = "xrTableCellIsPosStable";
            this.xrTableCellIsPosStable.StylePriority.UseFont = false;
            this.xrTableCellIsPosStable.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsPosStable.Text = "xrTableCellIsPosStable";
            this.xrTableCellIsPosStable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsPosStable.Weight = 0.75D;
            // 
            // xrTableCellIsCrossStable
            // 
            this.xrTableCellIsCrossStable.Dpi = 254F;
            this.xrTableCellIsCrossStable.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsCrossStable.Name = "xrTableCellIsCrossStable";
            this.xrTableCellIsCrossStable.StylePriority.UseFont = false;
            this.xrTableCellIsCrossStable.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsCrossStable.Text = "xrTableCellIsCrossStable";
            this.xrTableCellIsCrossStable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsCrossStable.Weight = 0.75D;
            // 
            // xrTableCellIsTideStable
            // 
            this.xrTableCellIsTideStable.Dpi = 254F;
            this.xrTableCellIsTideStable.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsTideStable.Name = "xrTableCellIsTideStable";
            this.xrTableCellIsTideStable.StylePriority.UseFont = false;
            this.xrTableCellIsTideStable.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsTideStable.Text = "xrTableCellIsTideStable";
            this.xrTableCellIsTideStable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsTideStable.Weight = 0.75D;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell92,
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95});
            this.xrTableRow30.Dpi = 254F;
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1.0393707263228456D;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Dpi = 254F;
            this.xrTableCell92.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseFont = false;
            this.xrTableCell92.StylePriority.UseTextAlignment = false;
            this.xrTableCell92.Text = "Хөдөлгөөн";
            this.xrTableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell92.Weight = 0.75D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Dpi = 254F;
            this.xrTableCell93.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseFont = false;
            this.xrTableCell93.StylePriority.UseTextAlignment = false;
            this.xrTableCell93.Text = "Хөдөлгөөн";
            this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell93.Weight = 0.75D;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Dpi = 254F;
            this.xrTableCell94.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseFont = false;
            this.xrTableCell94.StylePriority.UseTextAlignment = false;
            this.xrTableCell94.Text = "Хөдөлгөөн";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell94.Weight = 0.75D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Dpi = 254F;
            this.xrTableCell95.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseFont = false;
            this.xrTableCell95.StylePriority.UseTextAlignment = false;
            this.xrTableCell95.Text = "Хөдөлгөөн";
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell95.Weight = 0.75D;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellIsBellyPos,
            this.xrTableCellIsPos,
            this.xrTableCellIsCrossPos,
            this.xrTableCellIsTidePos});
            this.xrTableRow31.Dpi = 254F;
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1.0393707263228456D;
            // 
            // xrTableCellIsBellyPos
            // 
            this.xrTableCellIsBellyPos.Dpi = 254F;
            this.xrTableCellIsBellyPos.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsBellyPos.Name = "xrTableCellIsBellyPos";
            this.xrTableCellIsBellyPos.StylePriority.UseFont = false;
            this.xrTableCellIsBellyPos.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsBellyPos.Text = "xrTableCellIsBellyPos";
            this.xrTableCellIsBellyPos.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsBellyPos.Weight = 0.75D;
            // 
            // xrTableCellIsPos
            // 
            this.xrTableCellIsPos.Dpi = 254F;
            this.xrTableCellIsPos.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsPos.Name = "xrTableCellIsPos";
            this.xrTableCellIsPos.StylePriority.UseFont = false;
            this.xrTableCellIsPos.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsPos.Text = "xrTableCellIsPos";
            this.xrTableCellIsPos.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsPos.Weight = 0.75D;
            // 
            // xrTableCellIsCrossPos
            // 
            this.xrTableCellIsCrossPos.Dpi = 254F;
            this.xrTableCellIsCrossPos.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsCrossPos.Name = "xrTableCellIsCrossPos";
            this.xrTableCellIsCrossPos.StylePriority.UseFont = false;
            this.xrTableCellIsCrossPos.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsCrossPos.Text = "xrTableCellIsCrossPos";
            this.xrTableCellIsCrossPos.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsCrossPos.Weight = 0.75D;
            // 
            // xrTableCellIsTidePos
            // 
            this.xrTableCellIsTidePos.Dpi = 254F;
            this.xrTableCellIsTidePos.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellIsTidePos.Name = "xrTableCellIsTidePos";
            this.xrTableCellIsTidePos.StylePriority.UseFont = false;
            this.xrTableCellIsTidePos.StylePriority.UseTextAlignment = false;
            this.xrTableCellIsTidePos.Text = "xrTableCellIsTidePos";
            this.xrTableCellIsTidePos.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIsTidePos.Weight = 0.75D;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103});
            this.xrTableRow32.Dpi = 254F;
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1.0393707263228456D;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Dpi = 254F;
            this.xrTableCell103.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseFont = false;
            this.xrTableCell103.StylePriority.UseTextAlignment = false;
            this.xrTableCell103.Text = "Элэг цөс, дэлүү";
            this.xrTableCell103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell103.Weight = 3D;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell102,
            this.xrTableCell101,
            this.xrTableCell104,
            this.xrTableCell100});
            this.xrTableRow33.Dpi = 254F;
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1.8842536216164105D;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Dpi = 254F;
            this.xrTableCell102.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.StylePriority.UseFont = false;
            this.xrTableCell102.StylePriority.UseTextAlignment = false;
            this.xrTableCell102.Text = "Элэгний шинж тэмдэгүүд:";
            this.xrTableCell102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell102.Weight = 0.88235287781818139D;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Dpi = 254F;
            this.xrTableCell101.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseFont = false;
            this.xrTableCell101.StylePriority.UseTextAlignment = false;
            this.xrTableCell101.Text = "Хүч/Хугацаа";
            this.xrTableCell101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell101.Weight = 0.61764712218181861D;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Dpi = 254F;
            this.xrTableCell104.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseFont = false;
            this.xrTableCell104.StylePriority.UseTextAlignment = false;
            this.xrTableCell104.Text = "Элэгний хэмжээ тэмтрэлтээр:";
            this.xrTableCell104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell104.Weight = 0.75D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Dpi = 254F;
            this.xrTableCell100.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell100.Multiline = true;
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseFont = false;
            this.xrTableCell100.StylePriority.UseTextAlignment = false;
            this.xrTableCell100.Text = "Дэлүүний хэмжээ тэмтрэлтээр:";
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell100.Weight = 0.75D;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellLiver,
            this.xrTableCellDuringDay,
            this.xrTableCellLiverSize,
            this.xrTableCellSpleen});
            this.xrTableRow38.Dpi = 254F;
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1.0342536433290872D;
            // 
            // xrTableCellLiver
            // 
            this.xrTableCellLiver.Dpi = 254F;
            this.xrTableCellLiver.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellLiver.Name = "xrTableCellLiver";
            this.xrTableCellLiver.StylePriority.UseFont = false;
            this.xrTableCellLiver.StylePriority.UseTextAlignment = false;
            this.xrTableCellLiver.Text = "xrTableCellLiver";
            this.xrTableCellLiver.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellLiver.Weight = 0.8823528778181815D;
            // 
            // xrTableCellDuringDay
            // 
            this.xrTableCellDuringDay.Dpi = 254F;
            this.xrTableCellDuringDay.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellDuringDay.Name = "xrTableCellDuringDay";
            this.xrTableCellDuringDay.StylePriority.UseFont = false;
            this.xrTableCellDuringDay.StylePriority.UseTextAlignment = false;
            this.xrTableCellDuringDay.Text = "xrTableCellDuringDay";
            this.xrTableCellDuringDay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellDuringDay.Weight = 0.6176471221818185D;
            // 
            // xrTableCellLiverSize
            // 
            this.xrTableCellLiverSize.Dpi = 254F;
            this.xrTableCellLiverSize.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellLiverSize.Name = "xrTableCellLiverSize";
            this.xrTableCellLiverSize.StylePriority.UseFont = false;
            this.xrTableCellLiverSize.StylePriority.UseTextAlignment = false;
            this.xrTableCellLiverSize.Text = "xrTableCellLiverSize";
            this.xrTableCellLiverSize.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellLiverSize.Weight = 0.75D;
            // 
            // xrTableCellSpleen
            // 
            this.xrTableCellSpleen.Dpi = 254F;
            this.xrTableCellSpleen.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellSpleen.Name = "xrTableCellSpleen";
            this.xrTableCellSpleen.StylePriority.UseFont = false;
            this.xrTableCellSpleen.StylePriority.UseTextAlignment = false;
            this.xrTableCellSpleen.Text = "xrTableCellSpleen";
            this.xrTableCellSpleen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellSpleen.Weight = 0.75D;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCellLiverDirection});
            this.xrTableRow34.Dpi = 254F;
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1.0393662262172088D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Dpi = 254F;
            this.xrTableCell106.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseFont = false;
            this.xrTableCell106.StylePriority.UseTextAlignment = false;
            this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell106.Weight = 1.5D;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Dpi = 254F;
            this.xrTableCell107.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseFont = false;
            this.xrTableCell107.StylePriority.UseTextAlignment = false;
            this.xrTableCell107.Text = " Дэлбэн:";
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell107.Weight = 0.35312544138288138D;
            // 
            // xrTableCellLiverDirection
            // 
            this.xrTableCellLiverDirection.Dpi = 254F;
            this.xrTableCellLiverDirection.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableCellLiverDirection.Name = "xrTableCellLiverDirection";
            this.xrTableCellLiverDirection.StylePriority.UseFont = false;
            this.xrTableCellLiverDirection.StylePriority.UseTextAlignment = false;
            this.xrTableCellLiverDirection.Text = "xrTableCellLiverDirection";
            this.xrTableCellLiverDirection.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellLiverDirection.Weight = 1.1468745586171187D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 254F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 254F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 254F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 254F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // DigestiveReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(98, 95, 254, 254);
            this.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 254F);
            this.PageHeight = 2970;
            this.PageWidth = 2100;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "14.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsTongue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTuckConcern;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsTympanic;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsMoving;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsSkingWet;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsTough;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsPercussion;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellColor;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsTuck;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTongueShape;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellBellyConcern;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellPosConcern;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellCrossConcern;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellTideConcern;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsBellyStable;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsPosStable;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsCrossStable;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsTideStable;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsBellyPos;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsPos;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsCrossPos;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIsTidePos;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellLiver;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellDuringDay;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellLiverSize;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellSpleen;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellLiverDirection;
    }
}
