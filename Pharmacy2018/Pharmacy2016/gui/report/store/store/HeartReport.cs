﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;

namespace Pharmacy2016.gui.report.store.store
{
    /*
     * Зүрх судасны тогтолцооний тайлан харах report.
     * 
     */

    public partial class HeartReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static HeartReport INSTANCE = new HeartReport();

        public static HeartReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private HeartReport()
        {
 
        }

        private void initReport()
        {
            InitializeComponent();
            isInit = true;

            DataSource = StoreDataSet.Instance;
            DataAdapter = StoreDataSet.StoreHeartTableAdapter;
            DataMember = StoreDataSet.Instance.StoreHeart.ToString();

            initBinding();
        }

        private void initBinding()
        {
            this.xrTableCellHeartRisk.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".HeartRiskText", "")});
            this.xrTableCellSkinBlue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SkinBlueText", "")});
            this.xrTableCellVenous.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".VenousText", "")});
            this.xrTableCellIsEdema.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsEdemaText", "")});
            this.xrTableCellHeartPush.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".HeartPushText", "")});
            this.xrTableCellHeartPlace.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".heartPlaceText", "")});
            this.xrTableCellFrequency.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Frequency", "")});
            this.xrTableCellStrong.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".StrongText", "")});
            this.xrTableCellRate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".RateText", "")});
            this.xrTableCellVoltage.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".VoltageText", "")});
            this.xrTableCellFilled.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".FilledText", "")});
            this.xrTableCellSameDouble.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SameDoubleText", "")});
            this.xrTableCellHeartLimit.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".HeartLimitText", "")});
            this.xrTableCellDirection.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DirectionText", "")});
            this.xrTableCellHeartSound.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".HeartSoundText", "")});
            this.xrTableCellHeartFrequency.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".HeartFrequency", "")});            
            this.xrTableCellFirstSound.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".FirstSoundText", "")});
            this.xrTableCellSecondSound.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SecondSoundText", "")});
            this.xrTableCellThirdSound.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ThirdSoundText", "")});
            this.xrTableCellRealNoise.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".RealNoiseText", "")});
            this.xrTableCellPos.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PosText", "")});
            this.xrTableCellSis.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SisText", "")});
            this.xrTableCellDis.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DisText", "")});
            this.xrTableCellTransmission.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TransmissionText", "")});
            this.xrTableCellTransStrong.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".TransStrongText", "")});
            this.xrTableCellIsTouch.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsTouchText", "")});

        }

        public void initAndShow(string storeID)
        {
            if (!isInit)
            {
                initReport();
            }

            FilterString = "storeID = '" + storeID + "'";
            CreateDocument(true);
        }
    }
}
