﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.util;

namespace Pharmacy2016.gui.report.store.store
{
    /*
     * Үзлэгийн тайлан харах report.
     * 
     */

    public partial class InspectionReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static InspectionReport INSTANCE = new InspectionReport();

        public static InspectionReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private InspectionReport()
        {

        }

        private void initReport()
        {
            InitializeComponent();
            isInit = true;

            DataSource = StoreDataSet.Instance;
            DataAdapter = StoreDataSet.StoreInspectionTableAdapter;
            DataMember = StoreDataSet.Instance.StoreInspection.ToString();

            DetailReport.DataSource = StoreDataSet.Instance;
            DetailReport.DataAdapter = StoreDataSet.StoreTreatmentTableAdapter;
            DetailReport.DataMember = "StoreInspection.StoreInspection_StoreTreatment";

            initBinding();
        }

        private void initBinding()
        {
            this.xrTableCellDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Date", ReportUtil.DisplayFormatDate)});
            this.xrTableCellDoctorName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".FullName", "")});
            this.xrTableCellInspection.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Inspection", "")});
            this.xrTableCellDescription.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Description", "")});
            this.xrTableCellIsBreak.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsBreakText", "")});
            this.xrTableCellBreakDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".BreakDate", ReportUtil.DisplayFormatDate)});

            this.xrTableCellStartDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".StartDate", ReportUtil.DisplayFormatDate)});
            this.xrTableCellEndDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".EndDate", ReportUtil.DisplayFormatDate)});
            this.xrTableCellDuration.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Duration", "")});
            this.xrTableCellEachDay.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".EachDay", "")});
            this.xrTableCellMedicineID.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Name", "")});
            this.xrTableCellTDescription.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".Description", "")});
            this.xrTableCellTisBreak.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".IsBreakText", "")});
            this.xrTableCellTBreakDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DetailReport.DataMember + ".BreakDate", ReportUtil.DisplayFormatDate)});
        }

        public void initAndShow(string storeID)
        {
            if (!isInit)
            {
                initReport();
            }

            FilterString = "storeID = '" + storeID + "'";
            CreateDocument(true);
        }
    }
}
