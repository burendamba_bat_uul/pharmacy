﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.gui.core.store.store;

namespace Pharmacy2016.gui.report.store.store
{
    /*
     * Ерөнхий үзлэгийн тайлан харах report.
     * 
     */

    public partial class GeneralReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static GeneralReport INSTANCE = new GeneralReport();

        public static GeneralReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private GeneralReport()
        {       

        }

        private void initReport()
        {
            InitializeComponent();
            isInit = true;

            DataSource = StoreDataSet.Instance.StoreGeneralinspection;
            DataAdapter = StoreDataSet.StoreGeneralinspectionTableAdapter;
            DataMember = StoreDataSet.Instance.StoreGeneralinspection.ToString();

            initBinding();
        }

        private void initBinding()
        {
            //Ерөнхий үзлэг.
            this.xrTableCellGeneralCondition.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".GeneralConditionText", "")});
            this.xrTableCellMind.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MindText", "")});
            this.xrTableCellSurround.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SurroundText", "")});
            this.xrTableCellPosition.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PositionText", "")});
            this.xrTableCellSkinSalst.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SkinSalstText", "")});
            this.xrTableCellSkinFlexible.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SkinFlexibleText", "")});
            this.xrTableCellSkinWet.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SkinWetText", "")});
            this.xrTableCellSkinRash.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SkinRashText", "")});
            this.xrTableCellEdema.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".EdemaText", "")});
            this.xrTableCellGeneralPart.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".GeneralPartText", "")});   
            this.xrTableCellSize.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SizeText", "")});
            this.xrTableCellPlace.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PlaceText", "")});
            this.xrTableCellConcern.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ConcernText", "")});
            this.xrTableCellShape.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ShapeText", "")});
            this.xrTableCellMove.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MoveText", "")});
        }

        public void initAndShow(string storeID)
        {
            if (!isInit)
            {
                initReport();
            }

            FilterString = "storeID = '" + storeID + "'";
            CreateDocument(true);
        }
    }
}
