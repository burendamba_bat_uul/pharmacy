﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.util;

namespace Pharmacy2016.gui.report.store.store
{
    public partial class ValidityReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static ValidityReport INSTANCE = new ValidityReport();

        public static ValidityReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private ValidityReport()
        {

        }

        private void initReport()
        {
            InitializeComponent();
            isInit = true;

            DataSource = StoreDataSet.Instance;
            DataAdapter = StoreDataSet.StoreValidityTableAdapter;
            DataMember = StoreDataSet.Instance.StoreValidity.ToString();

            initBinding();
        }

        private void initBinding()
        {

            this.xrTableCellDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Date", ReportUtil.DisplayFormatDate)});
            this.xrTableCellDiagnoseType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DiagnoseTypeText", "")});
            this.xrTableCellDesc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Description", "")});
            this.xrTableCellDoctorName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                        new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".FullName", "")});
        }

        public void initAndShow(string storeID)
        {
            if (!isInit)
            {
                initReport();
            }

            FilterString = "storeID = '" + storeID + "'";
            CreateDocument(true);
        }
    }
}
