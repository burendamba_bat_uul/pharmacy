﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.util;

namespace Pharmacy2016.gui.report.store.store
{
    /*
     * Эмчлүүлэгчийн оношийн тайлан харах report.
     * 
     */

    public partial class DiagnoseReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static DiagnoseReport INSTANCE = new DiagnoseReport();

        public static DiagnoseReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private DiagnoseReport()
        {

        }

        private void initReport()
        {
            InitializeComponent();
            isInit = true;

            DataSource = StoreDataSet.Instance;
            DataAdapter = StoreDataSet.StoreDiagnoseTableAdapter;
            DataMember = StoreDataSet.Instance.StoreDiagnose.ToString();

            initBinding();
        }

        private void initBinding()
        {
            //Клиникийн урьдчилсан оношийн мастер дэтайл.
            this.xrTableCellDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Date", ReportUtil.DisplayFormatDate)});
            this.xrTableCellDiagnoseType.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DiagnoseTypeName", "")});
            this.xrTableCellDiagnose.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Diagnose", "")});
            this.xrTableCellDescription.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Description", "")});
            this.xrTableCellDoctorName.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                    new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".FullName", "")});
        }

        public void initAndShow(string storeID)
        {
            if (!isInit)
            {
                initReport();
            }

            FilterString = "storeID = '" + storeID + "'";
            CreateDocument(true);
        }
    }
}
