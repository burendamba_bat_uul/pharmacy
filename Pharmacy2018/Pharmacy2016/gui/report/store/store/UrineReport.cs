﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Pharmacy2016.gui.core.store.ds;

namespace Pharmacy2016.gui.report.store.store
{
    /*
     * Шээс бэлгийн тогтолцооний тайлан харах report.
     * 
     */
    public partial class UrineReport : DevExpress.XtraReports.UI.XtraReport
    {
        private static UrineReport INSTANCE = new UrineReport();

        public static UrineReport Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private UrineReport()
        {

        }

        private void initReport()
        {
            InitializeComponent();
            isInit = true;

            DataSource = StoreDataSet.Instance;
            DataAdapter = StoreDataSet.StoreUrineTableAdapter;
            DataMember = StoreDataSet.Instance.StoreUrine.ToString();

            initBinding();
        }

        private void initBinding()
        {
            this.xrTableCellUrineOutput.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UrineOutputText", "")});
            this.xrTableCellUrineColor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".UrineColorText", "")});
            this.xrTableCellIsUrineNight.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsUrineNightText", "")});
            this.xrTableCellIsUrineTrue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".IsUrineTrue", "")});
            this.xrTableCellCutUrine.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".CutUrineText", "")});
            this.xrTableCellMissUrine.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MissUrineText", "")});
            this.xrTableCellPushUrine.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PushUrineText", "")});
            this.xrTableCellHurtUrine.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".HurtUrineText", "")});
            this.xrTableCellKidneyConcern.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".KidneyConcernText", "")});
            this.xrTableCellPastyernatskii.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".PastyernatskiiText", "")});
            this.xrTableCellSmell.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".SmellText", "")});
            this.xrTableCellHear.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".HearText", "")});
            this.xrTableCellFaceDouble.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".FaceDoubleText", "")});
            this.xrTableCellReflex.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ReflexText", "")});
            this.xrTableCellShallow.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".ShallowText", "")});
            this.xrTableCellDeep.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".DeepText", "")});
            this.xrTableCellQuiteDeep.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".QuiteDeepText", "")});
            this.xrTableCellMentalStatus.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".MentalStatus", "")});
            this.xrTableCellOther.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
                new DevExpress.XtraReports.UI.XRBinding("Text", null, DataMember + ".Other", "")});          
        }

        public void initAndShow(string storeID)
        {
            if (!isInit)
            {
                initReport();
            }

            FilterString = "storeID = '" + storeID + "'";
            CreateDocument(true);
        }
    }
}
