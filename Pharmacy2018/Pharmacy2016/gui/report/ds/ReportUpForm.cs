﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.report.journal.order;
using DevExpress.XtraEditors.Controls;
using Pharmacy2016.gui.report.journal.order;
using Pharmacy2016.gui.core.journal.order;
using DevExpress.XtraEditors.Popup;
using DevExpress.Utils.Win;
using Pharmacy2016.gui.core.journal.inc;
using Pharmacy2016.report.journal.inc;
using Pharmacy2016.gui.report.journal.inc;
using Pharmacy2016.gui.core.journal.exp;
using Pharmacy2016.report.journal.exp;
using Pharmacy2016.gui.report.journal.exp;
using Pharmacy2016.gui.core.journal.bal;
using Pharmacy2016.gui.report.journal.bal;
using Pharmacy2016.gui.core.store.drug;
using Pharmacy2016.gui.report.store.drug;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.gui.core.journal.ds;
using System.Drawing;

namespace Pharmacy2016.gui.report.ds
{
    /*
     * Эмчлүүлэгчийн хэрэглэх эмийг өвчний түүхээс нь татах цонх.
     * 
     */

    public partial class ReportUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static ReportUpForm INSTANCE = new ReportUpForm();

        public static ReportUpForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;
        private DataRow currentRow;
        private string confUserID;
        private int maxCheckedItemNumber = 2;
        private XtraForm xrForm;

        private ReportUpForm()
        {

        }


        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            checkedComboBoxEditCol.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            checkedComboBoxEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            dateEditEnd.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            checkedComboBoxEditCol.Properties.DataSource = ReportDataSet.Instance.ReportColumns;
            checkedComboBoxEditUser.Properties.DataSource = ReportDataSet.SystemUserOrderBindingSource;
            treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
            reload();
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm(List<string[]> columns, XtraForm form, DataRow current, DateTime start, DateTime end, string confUser)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }
                xrForm = form;
                addToDataTable(columns);             
                currentRow = current;
                dateEditStart.DateTime = start;
                dateEditEnd.DateTime = end;
                dateEditStart.Enabled = false;
                dateEditEnd.Enabled = false;
                checkedComboBoxEditUser.Enabled = false;
                checkedComboBoxEditCol.CheckAll();
                radioGroupBal.SelectedIndex = 0;
                confUserID = confUser;
                switchRadios();
                radioBalChanges();
                comboBoxEditSignLoc.Location = new Point(174, 174);
                labelControlSignLoc.Location = new Point(61, 177);
                comboBoxEditSignLoc.SelectedIndex = 0;
                treeListLookUpEditWard.Visible = false;
                labelControlSignLoc.Visible = false;
                comboBoxEditSignLoc.Visible = false;
                if (xrForm == MedicineBalForm.Instance || xrForm == DrugForm.Instance)
                {
                    radioGroupType.SelectedIndex = 1;
                    checkedComboBoxEditUser.Enabled = false;
                    radioGroupType.Enabled = false;
                    dateEditStart.Enabled = true;
                    dateEditEnd.Enabled = true;
                }
                if (xrForm == DrugForm.Instance)
                {
                    labelControl5.Visible = true;
                    treeListLookUpEditWard.Visible = true;
                }else
                {
                    comboBoxEditSignLoc.Location = new Point(174, 148);
                    labelControlSignLoc.Location = new Point(61, 151);
                }
                ShowDialog();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clearData();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void clearData()
        {
            clearErrorText();
            ReportDataSet.Instance.ReportColumns.Clear();
            checkedComboBoxEditCol.SetEditValue("");
            checkedComboBoxEditUser.SetEditValue("");
            radioGroupType.Enabled = true;
            radioGroupType.SelectedIndex = 0;
        }

        private void clearErrorText()
        {
            checkedComboBoxEditCol.ErrorText = "";
            checkedComboBoxEditUser.ErrorText = "";
            dateEditStart.ErrorText = "";
            dateEditEnd.ErrorText = "";
        }

        private void ReportUpForm_Shown(object sender, EventArgs e)
        {
            checkedComboBoxEditCol.Focus();
            foreach (CheckedListBoxItem item in checkedComboBoxEditCol.Properties.Items)
            {
                if ((string)item.Value == "confirmUserID")
                {
                    item.CheckState = CheckState.Unchecked;
                }
                if ((string)item.Value == "incTypeName")
                {
                    item.CheckState = CheckState.Unchecked;
                }
                if ((string)item.Value == "medicineName")
                {
                    item.Enabled = false;
                }
                if ((string)item.Value == "unitType")
                {
                    item.Enabled = false;
                }
                if ((string)item.Value == "medicineTypeName")
                {
                    item.Enabled = false;
                }
            }
        }

        #endregion

        #region Формын event

        private void simpleButtonSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioGroupType.SelectedIndex == 0)
            {
                dateEditStart.Enabled = false;
                dateEditEnd.Enabled = false;
                checkedComboBoxEditUser.Enabled = false;
                labelControlSignLoc.Visible = false;
                comboBoxEditSignLoc.Visible = false;
            }
            else
            {
                dateEditStart.Enabled = true;
                dateEditEnd.Enabled = true;
                checkedComboBoxEditUser.Enabled = true;
                labelControlSignLoc.Visible = true;
                comboBoxEditSignLoc.Visible = true;
            }
        }

        private void radioGroupBal_SelectedIndexChanged(object sender, EventArgs e)
        {
            radioBalChanges();
        }

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            if (check())
            {
                if (xrForm == MedicineOrderForm.Instance)
                    MedicineOrderForm.Instance.reloadOuter(dateEditStart.Text, dateEditEnd.Text);
                else if (xrForm == DrugForm.Instance)
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                    {
                        PlanDataSet.DrugJournalTableAdapter.Fill(PlanDataSet.Instance.DrugJournal, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                        PlanDataSet.DrugDetailTableAdapter.Fill(PlanDataSet.Instance.DrugDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                    }
                    else
                    {
                        PlanDataSet.DrugJournalTableAdapter.Fill(PlanDataSet.Instance.DrugJournal, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                        PlanDataSet.DrugDetailTableAdapter.Fill(PlanDataSet.Instance.DrugDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    }     
                }
                else if (xrForm == MedicineExpForm.Instance)
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.ACCOUNTANT_ID)
                    {
                        JournalDataSet.ExpJournalTableAdapter.Fill(JournalDataSet.Instance.ExpJournal, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                        JournalDataSet.ExpDetailTableAdapter.Fill(JournalDataSet.Instance.ExpDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                    }
                    else
                    {
                        JournalDataSet.ExpJournalTableAdapter.Fill(JournalDataSet.Instance.ExpJournal, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                        JournalDataSet.ExpDetailTableAdapter.Fill(JournalDataSet.Instance.ExpDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    }
                }
                else if (xrForm == MedicineIncForm.Instance)
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.ACCOUNTANT_ID)
                    {
                        JournalDataSet.IncJournalTableAdapter.Fill(JournalDataSet.Instance.IncJournal, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                        JournalDataSet.IncDetailTableAdapter.Fill(JournalDataSet.Instance.IncDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                    }
                    else
                    {
                        JournalDataSet.IncJournalTableAdapter.Fill(JournalDataSet.Instance.IncJournal, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                        JournalDataSet.IncDetailTableAdapter.Fill(JournalDataSet.Instance.IncDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    }
                }
                else if (xrForm == MedicineBalForm.Instance)
                {
                    var userID = "";
                    for (var i = 0; i < JournalDataSet.Instance.SystemUserJournal.Rows.Count; i++)
                        userID += JournalDataSet.Instance.SystemUserJournal.Rows[i]["id"] + "~";

                    if (!userID.Equals(""))
                        userID = userID.Substring(0, userID.Length - 1);

                    JournalDataSet.BalTableAdapter.Fill(JournalDataSet.Instance.Bal, HospitalUtil.getHospitalId(), userID, dateEditStart.Text, dateEditEnd.Text);
                }
            }          
        }

        private void checkedComboBoxEditUser_Popup(object sender, EventArgs e)
        {
            var f = (sender as IPopupControl).PopupWindow as CheckedPopupContainerForm;
            var listBox = f.ActiveControl as CheckedListBoxControl;
            if (listBox != null)
            {
                listBox.ItemChecking += OnItemChecking;
            }
        }

        private void OnItemChecking(object sender, ItemCheckingEventArgs e)
        {
            if (e.NewValue == CheckState.Unchecked)
            {
                return;
            }
            var editor = sender as CheckedListBoxControl;
            e.Cancel = editor.CheckedItemsCount >= maxCheckedItemNumber ? true : false;
        }

        private void checkedComboBoxEditUser_CloseUp(object sender, CloseUpEventArgs e)
        {
            var f = (sender as IPopupControl).PopupWindow as CheckedPopupContainerForm;
            var listBox = f.ActiveControl as CheckedListBoxControl;
            if (listBox != null)
            {
                listBox.ItemChecking -= OnItemChecking;
            }
        }

        #endregion

        #region Формын функц

        private void addToDataTable(List<string[]> col)
        {
            if (ReportDataSet.Instance.ReportColumns.Rows.Count == 0)
            {
                for (int i = 0; i < col.Count; i++)
                {
                    DataRow row = ReportDataSet.Instance.ReportColumns.NewRow();
                    row["Id"] = col[i][0];
                    row["name"] = col[i][1];
                    row.EndEdit();
                    ReportDataSet.Instance.ReportColumns.Rows.Add(row);
                }
            }
        }

        private void reload()
        {
            if (ReportDataSet.Instance.SystemUser.Rows.Count == 0)
            {
                ReportDataSet.SystemUserTableAdapter.Fill(ReportDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.ALL_USER);
                ReportDataSet.SystemUserOrderTableAdapter.Fill(ReportDataSet.Instance.SystemUserOrder, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                if (JournalDataSet.Instance.WardOther.Rows.Count == 0)
                    JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
            }
        }

        private bool check()
        {
            clearErrorText();
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();

            if (checkedComboBoxEditCol.EditValue == DBNull.Value || checkedComboBoxEditCol.EditValue == null || checkedComboBoxEditCol.EditValue.ToString().Equals(""))
            {
                errorText = "Багана сонгоно уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                checkedComboBoxEditCol.ErrorText = errorText;
                isRight = false;
                checkedComboBoxEditCol.Focus();
            }
            if (radioGroupType.SelectedIndex == 1 && dateEditStart.Text == "")
            {
                errorText = "Эхлэх огноог оруулна уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }
            if (radioGroupType.SelectedIndex == 1 && dateEditEnd.Text == "")
            {
                errorText = "Дуусах огноог оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditEnd.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    dateEditEnd.Focus();
                }
            }
            if (radioGroupType.SelectedIndex == 1 && isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
            {
                errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }
            if (radioGroupType.SelectedIndex == 0 && currentRow.RowState == DataRowState.Detached)
            {
                errorText = "Сонгосон мөр олдсонгүй";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //checkedComboBoxEditCol.ErrorText = errorText;
                isRight = false;
                //checkedComboBoxEditCol.Focus();
            }
            List<object> ids = new List<object>();
            foreach (CheckedListBoxItem item in checkedComboBoxEditUser.Properties.Items)
            {
                if (item.CheckState == CheckState.Checked)
                    ids.Add(item);
            }
            if (radioGroupType.SelectedIndex == 1 && ids.Count < 2 && xrForm != DrugForm.Instance && xrForm != MedicineBalForm.Instance)
            {
                errorText = "Хамгийн багадаа 2 хэрэглэгч сонгоно уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                checkedComboBoxEditUser.ErrorText = errorText;
                isRight = false;
                checkedComboBoxEditUser.Focus();
            }
            if (!isRight)
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
            return isRight;
        }

        private void save()
        {
            if (check())
            {
                List<string> print = new List<string>();
                List<string> filter = new List<string>();
                List<string> ward = new List<string>();
                foreach (CheckedListBoxItem item in checkedComboBoxEditCol.Properties.Items)
                {
                    if (item.CheckState == CheckState.Checked)
                    {
                        print.Add(item.Value.ToString());
                    }
                }
                foreach (CheckedListBoxItem item in checkedComboBoxEditUser.Properties.Items)
                {
                    if (item.CheckState == CheckState.Checked)
                    {
                        filter.Add(item.Value.ToString());
                    }
                }
                if(treeListLookUpEditWard.EditValue != null)
                {
                    if (treeListLookUpEditWard.Properties.TreeList.FocusedNode.HasChildren)
                    {
                        DataRow[] wards = JournalDataSet.Instance.WardOther.Select("pid = '" + treeListLookUpEditWard.EditValue + "' AND hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                        for (int i = 0; i < wards.Length; i++)
                        {
                            ward.Add(wards[i]["id"].ToString());
                        }
                        ward.Add(treeListLookUpEditWard.EditValue.ToString());
                    }
                    else
                        ward.Add(treeListLookUpEditWard.EditValue.ToString());
                    
                }
                
                if(xrForm == MedicineOrderForm.Instance){
                    if (radioGroupType.SelectedIndex == 0)
                        OrderGoodsReport.Instance.initAndShow(print, currentRow, confUserID);
                    else
                        OrderMultiReport.Instance.initAndShow(print, filter, dateEditStart.Text, dateEditEnd.Text, comboBoxEditSignLoc.SelectedIndex);
                }
                else if (xrForm == MedicineIncForm.Instance)
                {
                    if (radioGroupType.SelectedIndex == 0)
                        IncDetialReport.Instance.initAndShow(print, currentRow);
                    else
                        IncMultiReport.Instance.initAndShow(print, filter, dateEditStart.Text, dateEditEnd.Text, comboBoxEditSignLoc.SelectedIndex);
                }
                else if (xrForm == MedicineExpForm.Instance)
                {
                    if (radioGroupType.SelectedIndex == 0)
                        ExpDetialReport.Instance.initAndShow(print, currentRow);
                    else
                        ExpMultiReport.Instance.initAndShow(print, filter, dateEditStart.Text, dateEditEnd.Text, comboBoxEditSignLoc.SelectedIndex);
                }
                else if (xrForm == MedicineBalForm.Instance)
                {
                    //if (radioGroupType.SelectedIndex == 0)
                        BalMultiReport.Instance.initAndShow(print, filter, dateEditStart.Text, dateEditEnd.Text, radioGroupBal.SelectedIndex, comboBoxEditSignLoc.SelectedIndex);
                }
                else if (xrForm == DrugForm.Instance)
                {
                    //if (radioGroupType.SelectedIndex == 0)
                    string wardsName = "";
                    if (treeListLookUpEditWard.EditValue != null)
                        wardsName = treeListLookUpEditWard.Text;
                    DrugMultiReport.Instance.initAndShow(print, filter, dateEditStart.Text, dateEditEnd.Text, ward, wardsName, comboBoxEditSignLoc.SelectedIndex);
                }
            }
        }

        public string returnRoleUserName(string expUser, int ur)
        {
            string roleName = "";
            for (int i = 0; i < ReportDataSet.Instance.SystemUser.Rows.Count; i++)
            {
                DataRow role = ReportDataSet.Instance.SystemUser.Rows[i];
                if (role["id"].ToString().Contains(expUser) && ur == 0)
                {
                    roleName = role["roleName"].ToString();
                    break;
                }
                else if (role["id"].ToString().Contains(expUser) && ur == 1)
                {
                    roleName = role["fullName"].ToString();
                    break;
                }
            }
            return roleName;
        }

        private void switchRadios()
        {
            if (xrForm == MedicineBalForm.Instance)
            {
                radioGroupBal.Visible = true;
                radioGroupBal.Location = new Point(174, 16);
                radioGroupType.Visible = false;
            }
            else
            {
                radioGroupBal.Visible = false;
                radioGroupBal.Location = new Point(12, 16);
                radioGroupType.Visible = true;
            }
        }

        private void radioBalChanges()
        {
            if (xrForm == MedicineBalForm.Instance)
            {
                foreach (CheckedListBoxItem item in checkedComboBoxEditCol.Properties.Items)
                {
                    if ((string)item.Value == "userName")
                    {
                        item.CheckState = radioGroupBal.SelectedIndex == 0 ? CheckState.Unchecked : CheckState.Checked;
                        item.Enabled = radioGroupBal.SelectedIndex == 0 ? false : true;
                    }
                    if ((string)item.Value == "roleName")
                    {
                        item.CheckState = radioGroupBal.SelectedIndex == 0 ? CheckState.Unchecked : CheckState.Checked;
                        item.Enabled = radioGroupBal.SelectedIndex == 0 ? false : true;
                    }
                    if ((string)item.Value == "warehouseName")
                    {
                        item.CheckState = radioGroupBal.SelectedIndex == 0 ? CheckState.Unchecked : CheckState.Checked;
                        item.Enabled = radioGroupBal.SelectedIndex == 0 ? false : true;
                    }
                }
            } 
        }

        #endregion       

    }
}