﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
namespace Pharmacy2016.gui.report.ds {
    
    public partial class ReportDataSet {

        #region DataSet-н обьект

            private static ReportDataSet INSTANCE = new ReportDataSet();

            public static ReportDataSet Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

        #endregion

        #region TableAdapter, BindingSource-ууд

            // Эмчлүүлэгчийн хэрэглэсэн эм 
            private static readonly ReportDataSetTableAdapters.PatientMedicineUserTableAdapter INSTANCE_PATIENT_MEDICINE_USER_TABLE_ADAPTER = new ReportDataSetTableAdapters.PatientMedicineUserTableAdapter();
            public static ReportDataSetTableAdapters.PatientMedicineUserTableAdapter PatientMedicineUserTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_MEDICINE_USER_TABLE_ADAPTER;
                }
            }


            // Нийт эмчлүүлэгчийн хэрэглэсэн эм 
            private static readonly ReportDataSetTableAdapters.PatientMedicineTableAdapter INSTANCE_PATIENT_MEDICINE_TABLE_ADAPTER = new ReportDataSetTableAdapters.PatientMedicineTableAdapter();
            public static ReportDataSetTableAdapters.PatientMedicineTableAdapter PatientMedicineTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_MEDICINE_TABLE_ADAPTER;
                }
            }


            // Тасгийн хэрэглэсэн эмийн тайлан 
            private static readonly ReportDataSetTableAdapters.WardTableAdapter INSTANCE_WARD_TABLE_ADAPTER = new ReportDataSetTableAdapters.WardTableAdapter();
            public static ReportDataSetTableAdapters.WardTableAdapter WardTableAdapter
            {
                get
                {
                    return INSTANCE_WARD_TABLE_ADAPTER;
                }
            }


            // Тасгийн хэрэглэсэн нэгдсэн эмийн тайлан 
            private static readonly ReportDataSetTableAdapters.WardSumTableAdapter INSTANCE_WARD_SUM_TABLE_ADAPTER = new ReportDataSetTableAdapters.WardSumTableAdapter();
            public static ReportDataSetTableAdapters.WardSumTableAdapter WardSumTableAdapter
            {
                get
                {
                    return INSTANCE_WARD_SUM_TABLE_ADAPTER;
                }
            }


            // Тасгийн эмчлүүлэгч нарт олгосон дүн
            private static readonly ReportDataSetTableAdapters.PatientWardTableAdapter INSTANCE_PATIENT_WARD_TABLE_ADAPTER = new ReportDataSetTableAdapters.PatientWardTableAdapter();
            public static ReportDataSetTableAdapters.PatientWardTableAdapter PatientWardTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_WARD_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_WARD_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientWardBindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_WARD_BINDING_SOURCE;
                }
            }


            // Захиалгын хуудасны тайлан 
            private static readonly ReportDataSetTableAdapters.MedicineOrderDetailTableAdapter INSTANCE_MEDICINE_ORDER_DETAIL_TABLE_ADAPTER = new ReportDataSetTableAdapters.MedicineOrderDetailTableAdapter();
            public static ReportDataSetTableAdapters.MedicineOrderDetailTableAdapter MedicineOrderDetailTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINE_ORDER_DETAIL_TABLE_ADAPTER;
                }
            }

            // Тасгийн эмчлүүлэгч нарт олгосон дүн
            private static readonly ReportDataSetTableAdapters.SystemUserTableAdapter INSTANCE_SYSTEM_USER_ADAPTER = new ReportDataSetTableAdapters.SystemUserTableAdapter();
            public static ReportDataSetTableAdapters.SystemUserTableAdapter SystemUserTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_USER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUserBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_BINDING_SOURCE;
                }
            }
            // Тасгийн эмчлүүлэгч нарт олгосон дүн
            private static readonly ReportDataSetTableAdapters.SystemUserOrderTableAdapter INSTANCE_SYSTEM_USER_ORDER_ADAPTER = new ReportDataSetTableAdapters.SystemUserOrderTableAdapter();
            public static ReportDataSetTableAdapters.SystemUserOrderTableAdapter SystemUserOrderTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_ORDER_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_USER_ORDER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUserOrderBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_ORDER_BINDING_SOURCE;
                }
            }

            // Тасаг агуулахтай холбоотой дугаар авах
            private static readonly ReportDataSetTableAdapters.WardConnTableAdapter INSTANCE_WARD_CONN_ADAPTER = new ReportDataSetTableAdapters.WardConnTableAdapter();
            public static ReportDataSetTableAdapters.WardConnTableAdapter WardConnTableAdapter
            {
                get
                {
                    return INSTANCE_WARD_CONN_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_WARD_CONN_BINDING_SOURCE = new BindingSource();
            public static BindingSource WardConnBindingSource
            {
                get
                {
                    return INSTANCE_WARD_CONN_BINDING_SOURCE;
                }
            }

            // Интеграцын холболт
            private static readonly ReportDataSetTableAdapters.AMOUNT_BALTableAdapter INSTANCE_AMOUNT_BAL_ADAPTER = new ReportDataSetTableAdapters.AMOUNT_BALTableAdapter();
            public static ReportDataSetTableAdapters.AMOUNT_BALTableAdapter AMOUNT_BALTableAdapter
            {
                get
                {
                    return INSTANCE_AMOUNT_BAL_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_AMOUNT_BAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource AMOUNT_BALBindingSource
            {
                get
                {
                    return INSTANCE_AMOUNT_BAL_BINDING_SOURCE;
                }
            }
        #endregion.


        // DataSet-ийг ачаалах функц.
        public static void initialize()
        {
            ((ISupportInitialize)(Instance)).BeginInit();

            Instance.DataSetName = "ReportDataSet";
            Instance.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;

            PatientMedicineUserTableAdapter.Initialize();
            PatientMedicineTableAdapter.Initialize();
            WardTableAdapter.Initialize();
            WardSumTableAdapter.Initialize();
            MedicineOrderDetailTableAdapter.Initialize();

            // Тасгийн эмчлүүлэгч нарт олгосон дүн
            PatientWardTableAdapter.Initialize();
            ((ISupportInitialize)(PatientWardBindingSource)).BeginInit();
            PatientWardBindingSource.DataMember = "PatientWard";
            PatientWardBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientWardBindingSource)).EndInit();

            SystemUserTableAdapter.Initialize();
            ((ISupportInitialize)(SystemUserBindingSource)).BeginInit();
            SystemUserBindingSource.DataMember = "SystemUser";
            SystemUserBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUserBindingSource)).EndInit();

            SystemUserOrderTableAdapter.Initialize();
            ((ISupportInitialize)(SystemUserOrderBindingSource)).BeginInit();
            SystemUserOrderBindingSource.DataMember = "SystemUserOrder";
            SystemUserOrderBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUserOrderBindingSource)).EndInit();

            WardConnTableAdapter.Initialize();
            ((ISupportInitialize)(WardConnBindingSource)).BeginInit();
            WardConnBindingSource.DataMember = "WardConn";
            WardConnBindingSource.DataSource = Instance;
            ((ISupportInitialize)(WardConnBindingSource)).EndInit();

            AMOUNT_BALTableAdapter.Initialize();
            ((ISupportInitialize)(AMOUNT_BALBindingSource)).BeginInit();
            AMOUNT_BALBindingSource.DataMember = "AMOUNT_BAL";
            AMOUNT_BALBindingSource.DataSource = Instance;
            ((ISupportInitialize)(AMOUNT_BALBindingSource)).EndInit();

            ((ISupportInitialize)(Instance)).EndInit();
        }
        

        public static void changeConnectionString()
        {
            PatientMedicineUserTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PatientMedicineTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            WardTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            WardSumTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MedicineOrderDetailTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PatientWardTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserOrderTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            WardConnTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            AMOUNT_BALTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
        }

    }
}
