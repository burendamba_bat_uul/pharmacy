﻿using Pharmacy2016.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacy2016.gui.report.ds.ReportDataSetTableAdapters
{

    public partial class PatientMedicineUserTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class PatientMedicineTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class WardTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class WardSumTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class PatientWardTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class MedicineOrderDetailTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class SystemUserTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class SystemUserOrderTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class WardConnTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class AMOUNT_BALTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }
}
