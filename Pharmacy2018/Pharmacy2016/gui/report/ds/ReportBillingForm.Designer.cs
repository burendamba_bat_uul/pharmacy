﻿namespace Pharmacy2016.gui.report.ds
{
    partial class ReportBillingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelBilling = new System.Windows.Forms.Label();
            this.labelAccountant = new System.Windows.Forms.Label();
            this.labelMoneyByLetter = new System.Windows.Forms.Label();
            this.textBoxBilling = new System.Windows.Forms.TextBox();
            this.textBoxAccountant = new System.Windows.Forms.TextBox();
            this.textBoxMoneyByLetter = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBoxAccountant = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // labelBilling
            // 
            this.labelBilling.AutoSize = true;
            this.labelBilling.Location = new System.Drawing.Point(62, 28);
            this.labelBilling.Name = "labelBilling";
            this.labelBilling.Size = new System.Drawing.Size(82, 13);
            this.labelBilling.TabIndex = 0;
            this.labelBilling.Text = "Нэхэмжлэх №:";
            // 
            // labelAccountant
            // 
            this.labelAccountant.AutoSize = true;
            this.labelAccountant.Location = new System.Drawing.Point(55, 54);
            this.labelAccountant.Name = "labelAccountant";
            this.labelAccountant.Size = new System.Drawing.Size(89, 13);
            this.labelAccountant.TabIndex = 1;
            this.labelAccountant.Text = "Нягтлан бодогч:";
            // 
            // labelMoneyByLetter
            // 
            this.labelMoneyByLetter.AutoSize = true;
            this.labelMoneyByLetter.Location = new System.Drawing.Point(39, 110);
            this.labelMoneyByLetter.Name = "labelMoneyByLetter";
            this.labelMoneyByLetter.Size = new System.Drawing.Size(105, 13);
            this.labelMoneyByLetter.TabIndex = 2;
            this.labelMoneyByLetter.Text = "Мөнгөн дүн үсгээр:";
            // 
            // textBoxBilling
            // 
            this.textBoxBilling.Location = new System.Drawing.Point(153, 25);
            this.textBoxBilling.Name = "textBoxBilling";
            this.textBoxBilling.Size = new System.Drawing.Size(183, 20);
            this.textBoxBilling.TabIndex = 1;
            // 
            // textBoxAccountant
            // 
            this.textBoxAccountant.Location = new System.Drawing.Point(42, 154);
            this.textBoxAccountant.Name = "textBoxAccountant";
            this.textBoxAccountant.Size = new System.Drawing.Size(183, 20);
            this.textBoxAccountant.TabIndex = 2;
            this.textBoxAccountant.Visible = false;
            // 
            // textBoxMoneyByLetter
            // 
            this.textBoxMoneyByLetter.Location = new System.Drawing.Point(153, 77);
            this.textBoxMoneyByLetter.Multiline = true;
            this.textBoxMoneyByLetter.Name = "textBoxMoneyByLetter";
            this.textBoxMoneyByLetter.Size = new System.Drawing.Size(225, 85);
            this.textBoxMoneyByLetter.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(153, 180);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Хэвлэх";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(278, 180);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Гарах";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // comboBoxAccountant
            // 
            this.comboBoxAccountant.FormattingEnabled = true;
            this.comboBoxAccountant.Location = new System.Drawing.Point(153, 51);
            this.comboBoxAccountant.Name = "comboBoxAccountant";
            this.comboBoxAccountant.Size = new System.Drawing.Size(183, 21);
            this.comboBoxAccountant.TabIndex = 6;
            // 
            // ReportBillingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 217);
            this.Controls.Add(this.comboBoxAccountant);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxMoneyByLetter);
            this.Controls.Add(this.textBoxAccountant);
            this.Controls.Add(this.textBoxBilling);
            this.Controls.Add(this.labelMoneyByLetter);
            this.Controls.Add(this.labelAccountant);
            this.Controls.Add(this.labelBilling);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ReportBillingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Нэхэмжлэх хэвлэх";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelBilling;
        private System.Windows.Forms.Label labelAccountant;
        private System.Windows.Forms.Label labelMoneyByLetter;
        private System.Windows.Forms.TextBox textBoxBilling;
        private System.Windows.Forms.TextBox textBoxAccountant;
        private System.Windows.Forms.TextBox textBoxMoneyByLetter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboBoxAccountant;
    }
}