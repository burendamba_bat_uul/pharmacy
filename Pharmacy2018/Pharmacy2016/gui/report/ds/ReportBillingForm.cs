﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.report.journal.exp;
using Pharmacy2016.gui.core.info.ds;



namespace Pharmacy2016.gui.report.ds
{
    public partial class ReportBillingForm : Form
    {
        #region Форм анх ачааллах функц

        private static ReportBillingForm INSTANCE = new ReportBillingForm();

        public static ReportBillingForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;
        private string userName1;
        private string filter1;
        private string pdate;
        
        private ReportBillingForm()
        {

        }


        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;
            textBoxBilling.Text = "";
            textBoxAccountant.Text = "";
            textBoxMoneyByLetter.Text = "";
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm(int type,string userN,string filt,string paydate)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }
                userName1 = userN;
                filter1 = filt;
                pdate = paydate;
                textBoxBilling.Text = "";
                textBoxAccountant.Text = "";
                textBoxMoneyByLetter.Text = "";
                comboBoxAccountant.Items.Clear();
                InformationDataSet.Instance.User.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                InformationDataSet.Instance.User.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                InformationDataSet.UserTableAdapter.Fill(InformationDataSet.Instance.User, HospitalUtil.getHospitalId());
                DataRow[] rows = InformationDataSet.Instance.User.Select("roleID = 3");
                bool b = false;
                if (rows.Length > 0)
                {
                    string name = "";
                    int j;
                    for (j = 0; j < rows.Length; j++)
                    {
                        name=rows[j]["lastName"].ToString().Substring(0,1)+"."+rows[j]["firstName"].ToString();
                        if (name == HospitalUtil.getHospitalAccountantName())
                        {
                            b = true;
                            comboBoxAccountant.Items.Add(name);
                            comboBoxAccountant.SelectedIndex = j;
                        }
                        else comboBoxAccountant.Items.Add(name);
                    }
                    if (!b)
                    {                        
                        if (HospitalUtil.getHospitalAccountantName().Length > 0)
                        {
                            comboBoxAccountant.Items.Add(HospitalUtil.getHospitalAccountantName());
                            comboBoxAccountant.SelectedIndex = j;
                        }
                        else comboBoxAccountant.SelectedIndex = 0;
                    }
                }
                else XtraMessageBox.Show("Бүртгэгдсэн нягтлан байхгүй байна.");

                ShowDialog();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clearData();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void clearData()
        {
            clearErrorText();            
        }

        private void clearErrorText()
        {
            
        }

        
        #endregion

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExpBillingReport.Instance.initAndShow(1, userName1, filter1,textBoxBilling.Text,comboBoxAccountant.Text,textBoxMoneyByLetter.Text,pdate);
        }

    }
}
