﻿using DevExpress.XtraEditors;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;



namespace Pharmacy2016.gui.core.info.database
{
    public class BackupService
    {
        private readonly string _connectionString;
        private readonly string _backupFolderFullPath;
        private readonly string[] _systemDatabaseNames = { "master", "tempdb", "model", "msdb" };

        public BackupService(string connectionString, string backupFolderFullPath)
        {
            _connectionString = connectionString;
            _backupFolderFullPath = backupFolderFullPath;
        }

        public void backupAllUserDatabases()
        {
            foreach (string databaseName in GetAllUserDatabases())
            {
                backupDatabase(databaseName);
            }
        }

        public void backupDatabase(string databaseName)
        {
            //if (!checkFolder(_backupFolderFullPath))
            //    return;

            string filePath = buildBackupPathWithFilename(databaseName);

            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    var query = String.Format("BACKUP DATABASE [{0}] TO DISK='{1}'", databaseName, filePath);

                    using (var command = new SqlCommand(query, connection))
                    {
                        connection.Open();
                        command.ExecuteNonQuery();

                        XtraMessageBox.Show("Өгөгдлийн санг архивлаж дууслаа.");
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Өгөгдлийн санг архивлаж чадсангүй "+ex.ToString());
            }
        }

        private bool checkFolder(string folder)
        {
            bool isRight = true;

            if (!File.Exists(folder))
            {
                isRight = false;
                XtraMessageBox.Show(folder + " хавтас алга байна.\n Та ерөнхий тохиргоог өөрчилнө үү.");
            }
            return isRight;
        }

        private IEnumerable<string> GetAllUserDatabases()
        {
            var databases = new List<String>();

            DataTable databasesTable;

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                databasesTable = connection.GetSchema("Databases");

                connection.Close();
            }

            foreach (DataRow row in databasesTable.Rows)
            {
                string databaseName = row["database_name"].ToString();

                if (_systemDatabaseNames.Contains(databaseName))
                    continue;

                databases.Add(databaseName);
            }

            return databases;
        }

        private string buildBackupPathWithFilename(string databaseName)
        {
            string filename = string.Format("{0}-{1}.bak", databaseName, DateTime.Now.ToString("(yyyy_MM_dd_HH_mm_ss)"));

            return Path.Combine(_backupFolderFullPath, filename);
        }
    }
}
