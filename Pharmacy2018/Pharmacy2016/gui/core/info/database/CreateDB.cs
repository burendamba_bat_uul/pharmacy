﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Management.Smo;
using System.ComponentModel;
using System.Windows.Forms;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using System.IO;



namespace Pharmacy2016.gui.core.info.database
{

    public class SqlObject
    {
        public string Name { get; set; }
        [Browsable(false)]
        public NamedSmoObject Object { get; set; }
        public string Type { get; set; }
        public string Error { get; set; }

        public SqlObject()
        {
            
        }

    }

    public class DataObject
    {
        public string Table { get; set; }
        public string SqlCommand { get; set; }
        public string Error { get; set; }

        public DataObject()
        {
            
        }
    }

    public class CreateDB
    {
        private readonly string _connectionString;

        List<string> tables;

        public CreateDB(string connectionString)
        {
            _connectionString = connectionString;
            initCopyTable();
        }

        private void initCopyTable()
        {
            tables = new List<string>();

            using (SqlConnection myConnection = new SqlConnection(_connectionString))
            {
                string oString = "Select name from InfoTable";
                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                myConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        //if (checkedTable.ToString().Equals(oReader["name"].ToString()))
                        //{
                            tables.Add(oReader["name"].ToString());
                        //}                        
                    }

                    myConnection.Close();
                }
            }
        }

        private void makeReadOnly(bool isReadOnly)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(_connectionString);
            string database = builder.InitialCatalog;

            DialogResult dialogResult = XtraMessageBox.Show(" Өмнөх " + database + " өгөгдлийн санг идэвхгүй болгох эсэх ?", "Өгөгдлийн сан", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                var querySingle = "ALTER DATABASE "+ database +" SET SINGLE_USER WITH ROLLBACK IMMEDIATE";
                var connSingle = new SqlConnection(_connectionString);
                var commandSingle = new SqlCommand(querySingle, connSingle);
                {
                    connSingle.Open();
                    using (SqlDataReader oReader = commandSingle.ExecuteReader())
                    {
                        if (oReader.Read())
                        {
                            //XtraMessageBox.Show("Success.");
                        }
                        connSingle.Close();
                    }
                }

                var queryReadOnly = "ALTER  DATABASE " + database + " SET " + (isReadOnly ? "READ_ONLY" : "READ_WRITE") + " WITH NO_WAIT;";

                var connReadOnly = new SqlConnection(_connectionString);
                var commandReadOnly = new SqlCommand(queryReadOnly, connReadOnly);
                connReadOnly.Open();
                using (SqlDataReader oReader = commandReadOnly.ExecuteReader())
                {
                    if (oReader.Read())
                    {
                        //XtraMessageBox.Show("Success.");
                    }
                    connReadOnly.Close();
                }
            }
        }

        private void changeDatabaseName(string initialCatalog)
        {
            string[] lines = _connectionString.Split(';');
            Pharmacy2016.Properties.Settings.Default.conMedicine = lines[0] + ";" + "Initial Catalog=" + initialCatalog + ";" + lines[2] + ";" + lines[3] + ";";
            Properties.Settings.Default.Save();

            //string conf = "dbConf.txt";
            //string file = AppDomain.CurrentDomain.BaseDirectory + conf;
            //if (File.Exists(file))
            //{
            //    string[] lines = File.ReadAllLines(file);
            //    if (lines.Length > 3)
            //    {
            //        string[] apply = { lines[0], initialCatalog, lines[2], lines[3] };       
            //        File.WriteAllLines(AppDomain.CurrentDomain.BaseDirectory + SystemUtil.dbFile, apply);
            //        Pharmacy2016.Properties.Settings.Default.conMedicine = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", lines[0], initialCatalog, lines[2], lines[3]);
            //        Properties.Settings.Default.Save();                   
            //    }
            //}
        }

        public bool createDB(string dbName, DateTime date)
        {
            if (checkDatabaseExists(dbName))
            {
                DialogResult dialogResult = XtraMessageBox.Show(dbName + " өгөгдлийн санг үүсгэсэн байна. Устгах уу?", "Өгөгдлийн санг устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.DELETE_DB_TITLE);

                    var tmp = "DROP DATABASE " + dbName + ";";

                    var del_conn = new SqlConnection(_connectionString);
                    var del_command = new SqlCommand(tmp, del_conn);

                    try
                    {
                        del_conn.Open();
                        del_command.ExecuteNonQuery();

                        //XtraMessageBox.Show("Өгөгдлийн санг устгалаа.");
                    }
                    catch (Exception ex)
                    {

                        XtraMessageBox.Show(ex.ToString());
                    }
                    finally
                    {
                        if ((del_conn.State == ConnectionState.Open))
                        {
                            del_conn.Close();
                        }
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            ProgramUtil.showWaitDialog(ProgramUtil.CREATE_DB_TITLE);
            var query = "CREATE DATABASE " + dbName + ";";

            var conn = new SqlConnection(_connectionString);
            var command = new SqlCommand(query, conn);
            bool isRight = true;
            try
            {
                conn.Open();
                command.ExecuteNonQuery();        

                string[] tmp = _connectionString.Split(';');
                string copyConn = tmp[0] + ";" + "Initial Catalog=" + dbName + ";" + tmp[2] + ";" + tmp[3] + ";";

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(_connectionString);
                string dbNameOld = builder.InitialCatalog;

                isRight = copySchema(dbName, copyConn);
                if (isRight)
                    isRight = copyIndexKeys(copyConn);
                if (isRight)
                    isRight = copyData(dbName, copyConn);
                if (isRight)
                    isRight = copyBegBal(dbName, copyConn, date);
                if (isRight)
                    isRight = insertNewDatabase(dbName, date);
                if(isRight)
                    makeReadOnly(true);
                if (isRight)
                    changeDatabaseName(dbName);
            }
            catch (Exception)
            {
                //XtraMessageBox.Show("Exception:" + ex.ToString());
                isRight = false;
            }
            finally
            {
                if ((conn.State == ConnectionState.Open))
                {
                    conn.Close();
                }

                ProgramUtil.closeWaitDialog();
            }         
            return isRight;
        }

        public bool checkDatabaseExists(string databaseName)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(string.Format("SELECT db_id('{0}')", databaseName), connection))
                {
                    connection.Open();
                    return (command.ExecuteScalar() != DBNull.Value);
                }
            }
        }

        private bool copySchema(string databaseName, string copyConn)
        {
            SqlTransfer transfer = new SqlTransfer(_connectionString, copyConn);
            List<SqlObject> list = transfer.SourceObjects;
            
            foreach (var item in list)
            {
                try
                {
                    transfer.DropAndCreateObject(item.Object);
                }
                catch (Exception exc)
                {
                    //XtraMessageBox.Show("schema : "+exc.ToString());
                    item.Error = exc.Message;
                    if (exc.InnerException != null)
                        item.Error = exc.InnerException.Message;
                    //return false;
                }
            }
            return true;
        }

        private bool copyIndexKeys(string copyConn)
        {
            bool isRight = true;
            SqlTransfer transfer = new SqlTransfer(_connectionString, copyConn);
            List<SqlObject> list = transfer.SourceObjects;

            foreach (var item in list.Where(i => i.Type == "Table"))
            {
                isRight = transfer.ApplyIndexes(item.Object);
                //if (!isRight)
                //    return false;
            }

            foreach (var item in list.Where(i => i.Type == "Table"))
            {
                isRight = transfer.ApplyForeignKeys(item.Object);
                //if (!isRight)
                //    return false;
            }

            foreach (var item in list.Where(i => i.Type == "Table"))
            {
                isRight = transfer.ApplyChecks(item.Object);
                //if (!isRight)
                //    return false;
            }
            return isRight;
        }

        private bool copyData(string databaseName, string copyConn)
        {
            DataTransfer transfer = new DataTransfer(_connectionString, copyConn);
            List<DataObject> items = transfer.SourceObjects;

            foreach (DataObject item in items)
            {
                try
                {
                    if (tables.IndexOf(item.Table) > -1)
                    {
                        if (item.SqlCommand == null || string.IsNullOrWhiteSpace(item.SqlCommand))
                        {
                            item.SqlCommand = "SELECT * FROM [dbo].[" + item.Table + "] WITH(NOLOCK)";
                        }
                        transfer.TrasnferData(item.Table, item.SqlCommand);
                    }
                }
                catch (Exception exc)
                {
                    XtraMessageBox.Show("data = "+exc.ToString());
                    item.Error = exc.Message;
                    if (exc.InnerException != null)
                        item.Error = exc.InnerException.Message;
                    //return false;
                }
            }
            return true;
        }

        private bool copyBegBal(string databaseName, string copyConn, DateTime date)
        {
            try
            {
                DateTime before = date.AddDays(-1);
                long id = Convert.ToInt64(JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId()));
                JournalDataSet.SystemUserJournalTableAdapter.Fill(JournalDataSet.Instance.SystemUserJournal, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);

                string userID = "";
                for (var i = 0; i < JournalDataSet.Instance.SystemUserJournal.Rows.Count; i++)
                    userID += JournalDataSet.Instance.SystemUserJournal.Rows[i]["id"] + "~";
                if (!userID.Equals(""))
                    userID = userID.Substring(0, userID.Length - 1);

                JournalDataSet.BalTableAdapter.Fill(JournalDataSet.Instance.Bal, HospitalUtil.getHospitalId(), userID, before.ToString("yyyy-MM-dd"), date.ToString("yyyy-MM-dd"));

                string query = "";
                for (int i = 0; i < JournalDataSet.Instance.Bal.Rows.Count; i++, id++)
                {
                    DataRow row = JournalDataSet.Instance.Bal.Rows[i];
                    query += "('" + HospitalUtil.getHospitalId() + "', '" + id + "', '" + row["userID"] + "', " + row["type"] + ", '" + (Convert.ToInt32(row["type"]) == 0 ? row["warehouseID"] : DBNull.Value) + "', '" + (Convert.ToInt32(row["type"]) == 1 ? row["warehouseID"] : DBNull.Value) + "', " + row["incTypeID"] + ", '" + row["medicineID"] + "', " + row["price"] + ", " + row["endBal"] + "), ";
                }
                if (!query.Equals(""))
                    query = query.Substring(0, query.Length - 2);

                string insert = "INSERT INTO [dbo].[BegBal] ([hospitalID],[id],[userID],[type],[warehouseID],[wardID],[incTypeID],[medicineID],[price],[count]) VALUES " + query + ";";

                using (SqlConnection myConnection = new SqlConnection(copyConn))
                {
                    SqlCommand oCmd = new SqlCommand(insert, myConnection);
                    myConnection.Open();
                    oCmd.ExecuteNonQuery();
                    myConnection.Close();
                }
            }
            catch (Exception exc)
            {
                XtraMessageBox.Show("begbal : "+exc.ToString());
                //return false;
            }
            return true;
        }

        private bool insertNewDatabase(string newDatabase, DateTime date)
        {
            try
            {
                string[] line = Pharmacy2016.Properties.Settings.Default.conDB.Split(';');
                string[] tmp = line[1].Split('=');

                string update = "USE " + tmp[1] + " UPDATE [dbo].[MainDB] SET [isCurrDB] = 0 WHERE [isCurrDB] = 1;";
                using (SqlConnection myConnectionNew = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conDB))
                {
                    SqlCommand oCmdNew = new SqlCommand(update, myConnectionNew);
                    myConnectionNew.Open();
                    oCmdNew.ExecuteNonQuery();
                    myConnectionNew.Close();
                }

                string insert = "USE " + tmp[1] + " INSERT INTO [dbo].[MainDB] ([name],[begBalDate],[isCurrDB]) VALUES ('" + newDatabase + "', '" + date + "', 1);";
                using (SqlConnection myConnectionOld = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conDB))
                {
                    SqlCommand oCmdOld = new SqlCommand(insert, myConnectionOld);
                    myConnectionOld.Open();
                    oCmdOld.ExecuteNonQuery();
                    myConnectionOld.Close();
                }
            }
            catch (Exception exc)
            {
                XtraMessageBox.Show("NewDataBase : " + exc.ToString());
                //return false;
            }
            return true;
        }

        //private bool insertNewDatabase(string newDatabase, DateTime date, bool isNew, string copyConn, bool isOld, string OldDatabase)
        //{
        //    try
        //    {
        //        string queryOld = "('" + OldDatabase + "', '" + date + "', '" + isOld + "')";

        //        string insertOld = "USE " + newDatabase + " INSERT INTO [dbo].[NewDataBase] ([dataBaseName],[begBalDate],[isNewDatabase]) VALUES " + queryOld + ";";

        //        using (SqlConnection myConnectionOld = new SqlConnection(copyConn))
        //        {
        //            SqlCommand oCmdOld = new SqlCommand(insertOld, myConnectionOld);
        //            myConnectionOld.Open();
        //            oCmdOld.ExecuteNonQuery();
        //            myConnectionOld.Close();
        //        }

        //        string queryNew = "('" + newDatabase + "', '" + date + "', '" + isNew + "')";

        //        string insertNew = "USE " + OldDatabase + " INSERT INTO [dbo].[NewDataBase] ([dataBaseName],[begBalDate],[isNewDatabase]) VALUES " + queryNew + ";";

        //        using (SqlConnection myConnectionNew = new SqlConnection(copyConn))
        //        {
        //            SqlCommand oCmdNew = new SqlCommand(insertNew, myConnectionNew);
        //            myConnectionNew.Open();
        //            oCmdNew.ExecuteNonQuery();
        //            myConnectionNew.Close();
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        XtraMessageBox.Show("NewDataBase : " + exc.ToString());
        //        //return false;
        //    }
        //    return true;
        //}
    

    }

}
