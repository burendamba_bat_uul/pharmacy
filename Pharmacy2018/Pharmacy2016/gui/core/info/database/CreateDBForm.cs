﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;



namespace Pharmacy2016.gui.core.info.database
{
    public partial class CreateDBForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static CreateDBForm INSTANCE = new CreateDBForm();

            public static CreateDBForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private CreateDBForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    dateEditEndBal.DateTime = DateTime.Now;

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                textEditName.ErrorText = "";
                textEditName.Text = "";
            }

            private void CreateDBForm_Shown(object sender, EventArgs e)
            {
                textEditName.Focus();
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }

        #endregion

        #region Формын event

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if ((textEditName.EditValue = textEditName.Text.Trim()).Equals(""))
                {
                    errorText = "Нэрийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditName.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.CREATE_DB_TITLE);
                 
                    string conStr = Pharmacy2016.Properties.Settings.Default.conMedicine;
                    CreateDB backup = new CreateDB(conStr);
                    bool isRight = backup.createDB(textEditName.Text, dateEditEndBal.DateTime);
                    
                    ProgramUtil.closeWaitDialog();

                    if (isRight)
                    {
                        XtraMessageBox.Show("Өгөгдлийн санг шинээр үүсгэлээ.");
                        SystemUtil.IsConnect = false;
                        ProgramUtil.EXITFULLY();                        
                    }
                    else
                    {
                        XtraMessageBox.Show("Өгөгдлийн санг үүсгэхэд алдаа гарлаа.");
                    }        
                    Close();
                }
            }

        #endregion         

    }
}