﻿using Pharmacy2016.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacy2016.gui.core.info.ds.InformationDataSetTableAdapters
{
    
    public partial class OrganizationTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class PositionTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class ProvinceTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class HospitalTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class RoleTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class UserTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class PatientTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class WindowTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class WindowRoleTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class LoginUserInfoTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class UserInfoTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class CurrHospitalTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class UserWindowRoleTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class SystemUserTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class SystemUser1TableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class SystemUserOtherTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class SpecialtyTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class IncTypeTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class IncTypeOtherTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class SystemLogTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class ProgramLogTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class UserWarehouseTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class UserWardTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class ExpTypeTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class ExpTypeOtherTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class MainConfTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class SystemUserAnotherTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

}
