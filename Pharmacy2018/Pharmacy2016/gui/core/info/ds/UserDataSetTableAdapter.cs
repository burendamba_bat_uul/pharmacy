﻿using Pharmacy2016.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacy2016.gui.core.info.ds.UserDataSetTableAdapters
{
    
    public partial class WardTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class RoomTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

   

    public partial class MedicineTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }


    public partial class PackageMedicineTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class MedicineTypeTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class MedicineTypeInternationalTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class MedicineShapeTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class MedicineUnitTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class ProducerCountryTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class ProducerTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class PatientRoomBedWardTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class PatientWardRoomBedTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class WareHouseTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class HouseUserTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class UserSkinColorTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class UserLayoutTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class MedicineQuantityTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class UserWardTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class PackageTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class PackageDetialTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class MedicinePackageTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class MedicineRelativesTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class SupplierTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class LockMedicineTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }
}
