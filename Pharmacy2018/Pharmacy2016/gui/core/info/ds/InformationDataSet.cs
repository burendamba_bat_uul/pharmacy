﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
namespace Pharmacy2016.gui.core.info.ds
{
    
    public partial class InformationDataSet {
        partial class MainConfDataTable
        {
        }
        
        
        #region DataSet-н обьект

            private static InformationDataSet INSTANCE = new InformationDataSet();

            public static InformationDataSet Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private static string[] education = {
                        "Бага", 
                        "Дунд", 
                        "Мэргэжлийн-техникийн", 
                        "Дээд",
                        "Боловсролгүй"
                };

        #endregion

        #region TableAdapter, BindingSource-ууд

            // QueriesTableAdapter
            private static readonly InformationDataSetTableAdapters.QueriesTableAdapter INSTANCE_QUERY_TABLE_ADAPTER = new InformationDataSetTableAdapters.QueriesTableAdapter();
            public static InformationDataSetTableAdapters.QueriesTableAdapter QueriesTableAdapter
            {
                get
                {
                    return INSTANCE_QUERY_TABLE_ADAPTER;
                }
            }


            // Ажлын газар 
            private static readonly InformationDataSetTableAdapters.OrganizationTableAdapter INSTANCE_ORGANIZATION_TABLE_ADAPTER = new InformationDataSetTableAdapters.OrganizationTableAdapter();
            public static InformationDataSetTableAdapters.OrganizationTableAdapter OrganizationTableAdapter
            {
                get
                {
                    return INSTANCE_ORGANIZATION_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ORGANIZATION_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrganizationBindingSource
            {
                get
                {
                    return INSTANCE_ORGANIZATION_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_ORGANIZATION_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrganizationOtherBindingSource
            {
                get
                {
                    return INSTANCE_ORGANIZATION_OTHER_BINDING_SOURCE;
                }
            }


            // Албан тушаал 
            private static readonly InformationDataSetTableAdapters.PositionTableAdapter INSTANCE_POSITION_TABLE_ADAPTER = new InformationDataSetTableAdapters.PositionTableAdapter();
            public static InformationDataSetTableAdapters.PositionTableAdapter PositionTableAdapter
            {
                get
                {
                    return INSTANCE_POSITION_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_POSITION_BINDING_SOURCE = new BindingSource();
            public static BindingSource PositionBindingSource
            {
                get
                {
                    return INSTANCE_POSITION_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_POSITION_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource PositionOtherBindingSource
            {
                get
                {
                    return INSTANCE_POSITION_OTHER_BINDING_SOURCE;
                }
            }


            // Хот, аймаг, дүүрэг, сум 
            private static readonly InformationDataSetTableAdapters.ProvinceTableAdapter INSTANCE_PROVINCE_TABLE_ADAPTER = new InformationDataSetTableAdapters.ProvinceTableAdapter();
            public static InformationDataSetTableAdapters.ProvinceTableAdapter ProvinceTableAdapter
            {
                get
                {
                    return INSTANCE_PROVINCE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PROVINCE_BINDING_SOURCE = new BindingSource();
            public static BindingSource ProvinceBindingSource
            {
                get
                {
                    return INSTANCE_PROVINCE_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_PROVINCE_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource ProvinceOtherBindingSource
            {
                get
                {
                    return INSTANCE_PROVINCE_OTHER_BINDING_SOURCE;
                }
            }


            // Эмнэлэг
            private static readonly InformationDataSetTableAdapters.HospitalTableAdapter INSTANCE_HOSPITAL_TABLE_ADAPTER = new InformationDataSetTableAdapters.HospitalTableAdapter();
            public static InformationDataSetTableAdapters.HospitalTableAdapter HospitalTableAdapter
            {
                get
                {
                    return INSTANCE_HOSPITAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_HOSPITAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource HospitalBindingSource
            {
                get
                {
                    return INSTANCE_HOSPITAL_BINDING_SOURCE;
                }
            }


            // Эрх
            private static readonly InformationDataSetTableAdapters.RoleTableAdapter INSTANCE_ROLE_TABLE_ADAPTER = new InformationDataSetTableAdapters.RoleTableAdapter();
            public static InformationDataSetTableAdapters.RoleTableAdapter RoleTableAdapter
            {
                get
                {
                    return INSTANCE_ROLE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ROLE_BINDING_SOURCE = new BindingSource();
            public static BindingSource RoleBindingSource
            {
                get
                {
                    return INSTANCE_ROLE_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_ROLE_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource RoleOtherBindingSource
            {
                get
                {
                    return INSTANCE_ROLE_OTHER_BINDING_SOURCE;
                }
            }


            // Цонх
            private static readonly InformationDataSetTableAdapters.WindowTableAdapter INSTANCE_WINDOW_TABLE_ADAPTER = new InformationDataSetTableAdapters.WindowTableAdapter();
            public static InformationDataSetTableAdapters.WindowTableAdapter WindowTableAdapter
            {
                get
                {
                    return INSTANCE_WINDOW_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_WINDOW_BINDING_SOURCE = new BindingSource();
            public static BindingSource WindowBindingSource
            {
                get
                {
                    return INSTANCE_WINDOW_BINDING_SOURCE;
                }
            }


            // Цонхны эрх
            private static readonly InformationDataSetTableAdapters.WindowRoleTableAdapter INSTANCE_WINDOW_ROLE_TABLE_ADAPTER = new InformationDataSetTableAdapters.WindowRoleTableAdapter();
            public static InformationDataSetTableAdapters.WindowRoleTableAdapter WindowRoleTableAdapter
            {
                get
                {
                    return INSTANCE_WINDOW_ROLE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_WINDOW_ROLE_BINDING_SOURCE = new BindingSource();
            public static BindingSource WindowRoleBindingSource
            {
                get
                {
                    return INSTANCE_WINDOW_ROLE_BINDING_SOURCE;
                }
            }


            // Хэрэглэгч
            private static readonly InformationDataSetTableAdapters.UserTableAdapter INSTANCE_USER_TABLE_ADAPTER = new InformationDataSetTableAdapters.UserTableAdapter();
            public static InformationDataSetTableAdapters.UserTableAdapter UserTableAdapter
            {
                get
                {
                    return INSTANCE_USER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_USER_BINDING_SOURCE = new BindingSource();
            public static BindingSource UserBindingSource
            {
                get
                {
                    return INSTANCE_USER_BINDING_SOURCE;
                }
            }


            // Өвчтөн
            private static readonly InformationDataSetTableAdapters.PatientTableAdapter INSTANCE_PATIENT_TABLE_ADAPTER = new InformationDataSetTableAdapters.PatientTableAdapter();
            public static InformationDataSetTableAdapters.PatientTableAdapter PatientTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientBindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_BINDING_SOURCE;
                }
            }


            // Нэвтэрч орсон хэрэглэгчийн мэдээлэл
            private static readonly InformationDataSetTableAdapters.LoginUserInfoTableAdapter INSTANCE_LOGIN_USER_INFO_TABLE_ADAPTER = new InformationDataSetTableAdapters.LoginUserInfoTableAdapter();
            public static InformationDataSetTableAdapters.LoginUserInfoTableAdapter LoginUserInfoTableAdapter
            {
                get
                {
                    return INSTANCE_LOGIN_USER_INFO_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_LOGIN_USER_INFO_BINDING_SOURCE = new BindingSource();
            public static BindingSource LoginUserInfoBindingSource
            {
                get
                {
                    return INSTANCE_LOGIN_USER_INFO_BINDING_SOURCE;
                }
            }


            // Хэрэглэгчийн мэдээлэл
            private static readonly InformationDataSetTableAdapters.UserInfoTableAdapter INSTANCE_USER_INFO_TABLE_ADAPTER = new InformationDataSetTableAdapters.UserInfoTableAdapter();
            public static InformationDataSetTableAdapters.UserInfoTableAdapter UserInfoTableAdapter
            {
                get
                {
                    return INSTANCE_USER_INFO_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_USER_INFO_BINDING_SOURCE = new BindingSource();
            public static BindingSource UserInfoBindingSource
            {
                get
                {
                    return INSTANCE_USER_INFO_BINDING_SOURCE;
                }
            }


            // Тухайн эмнэлэгийн мэдээлэл
            private static readonly InformationDataSetTableAdapters.CurrHospitalTableAdapter INSTANCE_CURR_HOSPITAL_TABLE_ADAPTER = new InformationDataSetTableAdapters.CurrHospitalTableAdapter();
            public static InformationDataSetTableAdapters.CurrHospitalTableAdapter CurrHospitalTableAdapter
            {
                get
                {
                    return INSTANCE_CURR_HOSPITAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_CURR_HOSPITAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource CurrHospitalBindingSource
            {
                get
                {
                    return INSTANCE_CURR_HOSPITAL_BINDING_SOURCE;
                }
            }


            // Хэрэглэгчийн цонхны эрх
            private static readonly InformationDataSetTableAdapters.UserWindowRoleTableAdapter INSTANCE_USER_WINDOW_ROLE_TABLE_ADAPTER = new InformationDataSetTableAdapters.UserWindowRoleTableAdapter();
            public static InformationDataSetTableAdapters.UserWindowRoleTableAdapter UserWindowRoleTableAdapter
            {
                get
                {
                    return INSTANCE_USER_WINDOW_ROLE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_USER_WINDOW_ROLE_BINDING_SOURCE = new BindingSource();
            public static BindingSource UserWindowRoleBindingSource
            {
                get
                {
                    return INSTANCE_USER_WINDOW_ROLE_BINDING_SOURCE;
                }
            }


            // Програмын хэрэглэгч
            private static readonly InformationDataSetTableAdapters.SystemUserTableAdapter INSTANCE_SYSTEM_USER_TABLE_ADAPTER = new InformationDataSetTableAdapters.SystemUserTableAdapter();
            public static InformationDataSetTableAdapters.SystemUserTableAdapter SystemUserTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_USER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUserBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_BINDING_SOURCE;
                }
            }

            private static readonly InformationDataSetTableAdapters.SystemUser1TableAdapter INSTANCE_SYSTEM_USER_1_TABLE_ADAPTER = new InformationDataSetTableAdapters.SystemUser1TableAdapter();
            public static InformationDataSetTableAdapters.SystemUser1TableAdapter SystemUser1TableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_1_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_USER_1_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUser1BindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_1_BINDING_SOURCE;
                }
            }


            // Програмын хэрэглэгч бусад
            private static readonly InformationDataSetTableAdapters.SystemUserOtherTableAdapter INSTANCE_SYSTEM_USER_OTHER_TABLE_ADAPTER = new InformationDataSetTableAdapters.SystemUserOtherTableAdapter();
            public static InformationDataSetTableAdapters.SystemUserOtherTableAdapter SystemUserOtherTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_USER_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUserOtherBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_OTHER_BINDING_SOURCE;
                }
            }

            // Програмын хэрэглэгч бусад
            private static readonly InformationDataSetTableAdapters.SystemUserAnotherTableAdapter INSTANCE_SYSTEM_USER_ANOTHER_TABLE_ADAPTER = new InformationDataSetTableAdapters.SystemUserAnotherTableAdapter();
            public static InformationDataSetTableAdapters.SystemUserAnotherTableAdapter SystemUserAnotherTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_ANOTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_USER_ANOTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUserAnotherBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_ANOTHER_BINDING_SOURCE;
                }
            }

            // Мэргэжил
            private static readonly InformationDataSetTableAdapters.SpecialtyTableAdapter INSTANCE_SPECIALTY_TABLE_ADAPTER = new InformationDataSetTableAdapters.SpecialtyTableAdapter();
            public static InformationDataSetTableAdapters.SpecialtyTableAdapter SpecialtyTableAdapter
            {
                get
                {
                    return INSTANCE_SPECIALTY_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SPECIALTY_BINDING_SOURCE = new BindingSource();
            public static BindingSource SpecialtyBindingSource
            {
                get
                {
                    return INSTANCE_SPECIALTY_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_SPECIALTY_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SpecialtyOtherBindingSource
            {
                get
                {
                    return INSTANCE_SPECIALTY_OTHER_BINDING_SOURCE;
                }
            }


            // Орлогын төрөл
            private static readonly InformationDataSetTableAdapters.IncTypeTableAdapter INSTANCE_INC_TYPE_TABLE_ADAPTER = new InformationDataSetTableAdapters.IncTypeTableAdapter();
            public static InformationDataSetTableAdapters.IncTypeTableAdapter IncTypeTableAdapter
            {
                get
                {
                    return INSTANCE_INC_TYPE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_INC_TYPE_BINDING_SOURCE = new BindingSource();
            public static BindingSource IncTypeBindingSource
            {
                get
                {
                    return INSTANCE_INC_TYPE_BINDING_SOURCE;
                }
            }


            // Ашиглах орлогын төрөл
            private static readonly InformationDataSetTableAdapters.IncTypeOtherTableAdapter INSTANCE_INC_TYPE_OTHER_TABLE_ADAPTER = new InformationDataSetTableAdapters.IncTypeOtherTableAdapter();
            public static InformationDataSetTableAdapters.IncTypeOtherTableAdapter IncTypeOtherTableAdapter
            {
                get
                {
                    return INSTANCE_INC_TYPE_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_INC_TYPE_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource IncTypeOtherBindingSource
            {
                get
                {
                    return INSTANCE_INC_TYPE_OTHER_BINDING_SOURCE;
                }
            }


            // Системийн лог
            private static readonly InformationDataSetTableAdapters.SystemLogTableAdapter INSTANCE_SYSTEM_LOG_TABLE_ADAPTER = new InformationDataSetTableAdapters.SystemLogTableAdapter();
            public static InformationDataSetTableAdapters.SystemLogTableAdapter SystemLogTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_LOG_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_LOG_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemLogBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_LOG_BINDING_SOURCE;
                }
            }


            // Програмын лог
            private static readonly InformationDataSetTableAdapters.ProgramLogTableAdapter INSTANCE_PROGRAM_LOG_TABLE_ADAPTER = new InformationDataSetTableAdapters.ProgramLogTableAdapter();
            public static InformationDataSetTableAdapters.ProgramLogTableAdapter ProgramLogTableAdapter
            {
                get
                {
                    return INSTANCE_PROGRAM_LOG_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PROGRAM_LOG_BINDING_SOURCE = new BindingSource();
            public static BindingSource ProgramLogBindingSource
            {
                get
                {
                    return INSTANCE_PROGRAM_LOG_BINDING_SOURCE;
                }
            }


            // Хэрэглэгчийн агуулах
            private static readonly InformationDataSetTableAdapters.UserWarehouseTableAdapter INSTANCE_USER_WAREHOUSE_TABLE_ADAPTER = new InformationDataSetTableAdapters.UserWarehouseTableAdapter();
            public static InformationDataSetTableAdapters.UserWarehouseTableAdapter UserWarehouseTableAdapter
            {
                get
                {
                    return INSTANCE_USER_WAREHOUSE_TABLE_ADAPTER;
                }
            }


            // Хэрэглэгчийн тасаг
            private static readonly InformationDataSetTableAdapters.UserWardTableAdapter INSTANCE_USER_WARD_TABLE_ADAPTER = new InformationDataSetTableAdapters.UserWardTableAdapter();
            public static InformationDataSetTableAdapters.UserWardTableAdapter UserWardTableAdapter
            {
                get
                {
                    return INSTANCE_USER_WARD_TABLE_ADAPTER;
                }
            }


            // Зарлагын төрөл
            private static readonly InformationDataSetTableAdapters.ExpTypeTableAdapter INSTANCE_EXP_TYPE_TABLE_ADAPTER = new InformationDataSetTableAdapters.ExpTypeTableAdapter();
            public static InformationDataSetTableAdapters.ExpTypeTableAdapter ExpTypeTableAdapter
            {
                get
                {
                    return INSTANCE_EXP_TYPE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_EXP_TYPE_BINDING_SOURCE = new BindingSource();
            public static BindingSource ExpTypeBindingSource
            {
                get
                {
                    return INSTANCE_EXP_TYPE_BINDING_SOURCE;
                }
            }


            // Ашиглах зарлагын төрөл
            private static readonly InformationDataSetTableAdapters.ExpTypeOtherTableAdapter INSTANCE_EXP_TYPE_OTHER_TABLE_ADAPTER = new InformationDataSetTableAdapters.ExpTypeOtherTableAdapter();
            public static InformationDataSetTableAdapters.ExpTypeOtherTableAdapter ExpTypeOtherTableAdapter
            {
                get
                {
                    return INSTANCE_EXP_TYPE_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_EXP_TYPE_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource ExpTypeOtherBindingSource
            {
                get
                {
                    return INSTANCE_EXP_TYPE_OTHER_BINDING_SOURCE;
                }
            }


            // Ерөнхий тохиргоо
            private static readonly InformationDataSetTableAdapters.MainConfTableAdapter INSTANCE_MAIN_CONF_TABLE_ADAPTER = new InformationDataSetTableAdapters.MainConfTableAdapter();
            public static InformationDataSetTableAdapters.MainConfTableAdapter MainConfTableAdapter
            {
                get
                {
                    return INSTANCE_MAIN_CONF_TABLE_ADAPTER;
                }
            }

            
        #endregion

        // DataSet-ийг ачаалах функц.
        public static void initialize()
        {
            ((ISupportInitialize)(Instance)).BeginInit();

            Instance.DataSetName = "InformationDataSet";
            Instance.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;

            QueriesTableAdapter.InitCommandCollection();

            // Ажлын газар
            OrganizationTableAdapter.Initialize();
            ((ISupportInitialize)(OrganizationBindingSource)).BeginInit();
            OrganizationBindingSource.DataMember = "Organization";
            OrganizationBindingSource.DataSource = Instance;
            ((ISupportInitialize)(OrganizationBindingSource)).EndInit();

            ((ISupportInitialize)(OrganizationOtherBindingSource)).BeginInit();
            OrganizationOtherBindingSource.DataMember = "Organization";
            OrganizationOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(OrganizationOtherBindingSource)).EndInit();


            // Албан тушаал 
            PositionTableAdapter.Initialize();
            ((ISupportInitialize)(PositionBindingSource)).BeginInit();
            PositionBindingSource.DataMember = "Position";
            PositionBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PositionBindingSource)).EndInit();

            ((ISupportInitialize)(PositionOtherBindingSource)).BeginInit();
            PositionOtherBindingSource.DataMember = "Position";
            PositionOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PositionOtherBindingSource)).EndInit();


            // Хот, аймаг, дүүрэг, сум 
            ProvinceTableAdapter.Initialize();
            ((ISupportInitialize)(ProvinceBindingSource)).BeginInit();
            ProvinceBindingSource.DataMember = "Province";
            ProvinceBindingSource.DataSource = Instance;
            ((ISupportInitialize)(ProvinceBindingSource)).EndInit();

            ((ISupportInitialize)(ProvinceOtherBindingSource)).BeginInit();
            ProvinceOtherBindingSource.DataMember = "Province";
            ProvinceOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(ProvinceOtherBindingSource)).EndInit();


            // Эмнэлэг
            HospitalTableAdapter.Initialize();
            ((ISupportInitialize)(HospitalBindingSource)).BeginInit();
            HospitalBindingSource.DataMember = "Hospital";
            HospitalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(HospitalBindingSource)).EndInit();


            // Эрх
            RoleTableAdapter.Initialize();
            ((ISupportInitialize)(RoleBindingSource)).BeginInit();
            RoleBindingSource.DataMember = "Role";
            RoleBindingSource.DataSource = Instance;
            ((ISupportInitialize)(RoleBindingSource)).EndInit();

            ((ISupportInitialize)(RoleOtherBindingSource)).BeginInit();
            RoleOtherBindingSource.DataMember = "Role";
            RoleOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(RoleOtherBindingSource)).EndInit();


            // Хэрэглэгчийн эрх
            UserTableAdapter.Initialize();
            ((ISupportInitialize)(UserBindingSource)).BeginInit();
            UserBindingSource.DataMember = "User";
            UserBindingSource.DataSource = Instance;
            ((ISupportInitialize)(UserBindingSource)).EndInit();


            // Өвчтөн
            PatientTableAdapter.Initialize();
            ((ISupportInitialize)(PatientBindingSource)).BeginInit();
            PatientBindingSource.DataMember = "Patient";
            PatientBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientBindingSource)).EndInit();


            // Цонх
            WindowTableAdapter.Initialize();
            ((ISupportInitialize)(WindowBindingSource)).BeginInit();
            WindowBindingSource.DataMember = "Window";
            WindowBindingSource.DataSource = Instance;
            ((ISupportInitialize)(WindowBindingSource)).EndInit();


            // Цонхны эрх
            WindowRoleTableAdapter.Initialize();
            ((ISupportInitialize)(WindowRoleBindingSource)).BeginInit();
            WindowRoleBindingSource.DataMember = "WindowRole";
            WindowRoleBindingSource.DataSource = Instance;
            ((ISupportInitialize)(WindowRoleBindingSource)).EndInit();


            // Нэвтэрч орсон хэрэглэгчийн мэдээлэл
            LoginUserInfoTableAdapter.Initialize();
            ((ISupportInitialize)(LoginUserInfoBindingSource)).BeginInit();
            LoginUserInfoBindingSource.DataMember = "LoginUserInfo";
            LoginUserInfoBindingSource.DataSource = Instance;
            ((ISupportInitialize)(LoginUserInfoBindingSource)).EndInit();


            // Хэрэглэгчийн мэдээлэл
            UserInfoTableAdapter.Initialize();
            ((ISupportInitialize)(UserInfoBindingSource)).BeginInit();
            UserInfoBindingSource.DataMember = "UserInfo";
            UserInfoBindingSource.DataSource = Instance;
            ((ISupportInitialize)(UserInfoBindingSource)).EndInit();


            // Тухайн эмнэлэгийн мэдээлэл
            CurrHospitalTableAdapter.Initialize();
            ((ISupportInitialize)(CurrHospitalBindingSource)).BeginInit();
            CurrHospitalBindingSource.DataMember = "CurrHospital";
            CurrHospitalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(CurrHospitalBindingSource)).EndInit();


            // Хэрэглэгчийн цонхны эрх
            UserWindowRoleTableAdapter.Initialize();
            ((ISupportInitialize)(UserWindowRoleBindingSource)).BeginInit();
            UserWindowRoleBindingSource.DataMember = "UserWindowRole";
            UserWindowRoleBindingSource.DataSource = Instance;
            ((ISupportInitialize)(UserWindowRoleBindingSource)).EndInit();


            // Програмын хэрэглэгч
            SystemUserTableAdapter.Initialize();
            ((ISupportInitialize)(SystemUserBindingSource)).BeginInit();
            SystemUserBindingSource.DataMember = "SystemUser";
            SystemUserBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUserBindingSource)).EndInit();
            SystemUser1TableAdapter.Initialize();
            ((ISupportInitialize)(SystemUser1BindingSource)).BeginInit();
            SystemUser1BindingSource.DataMember = "SystemUser1";
            SystemUser1BindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUser1BindingSource)).EndInit();


            // Програмын хэрэглэгч бусад
            SystemUserOtherTableAdapter.Initialize();
            ((ISupportInitialize)(SystemUserOtherBindingSource)).BeginInit();
            SystemUserOtherBindingSource.DataMember = "SystemUserOther";
            SystemUserOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUserOtherBindingSource)).EndInit();

            // Програмын хэрэглэгч бусад
            SystemUserAnotherTableAdapter.Initialize();
            ((ISupportInitialize)(SystemUserAnotherBindingSource)).BeginInit();
            SystemUserAnotherBindingSource.DataMember = "SystemUserAnother";
            SystemUserAnotherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUserAnotherBindingSource)).EndInit();

            // Мэргэжил
            SpecialtyTableAdapter.Initialize();
            ((ISupportInitialize)(SpecialtyBindingSource)).BeginInit();
            SpecialtyBindingSource.DataMember = "Specialty";
            SpecialtyBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SpecialtyBindingSource)).EndInit();

            ((ISupportInitialize)(SpecialtyOtherBindingSource)).BeginInit();
            SpecialtyOtherBindingSource.DataMember = "Specialty";
            SpecialtyOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SpecialtyOtherBindingSource)).EndInit();


            // Орлогын төрөл
            IncTypeTableAdapter.Initialize();
            ((ISupportInitialize)(IncTypeBindingSource)).BeginInit();
            IncTypeBindingSource.DataMember = "IncType";
            IncTypeBindingSource.DataSource = Instance;
            ((ISupportInitialize)(IncTypeBindingSource)).EndInit();


            // Ашиглах орлогын төрөл
            IncTypeOtherTableAdapter.Initialize();
            ((ISupportInitialize)(IncTypeOtherBindingSource)).BeginInit();
            IncTypeOtherBindingSource.DataMember = "IncTypeOther";
            IncTypeOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(IncTypeOtherBindingSource)).EndInit();


            // Системийн лог
            SystemLogTableAdapter.Initialize();
            ((ISupportInitialize)(SystemLogBindingSource)).BeginInit();
            SystemLogBindingSource.DataMember = "SystemLog";
            SystemLogBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemLogBindingSource)).EndInit();


            // Програмын лог
            ProgramLogTableAdapter.Initialize();
            ((ISupportInitialize)(ProgramLogBindingSource)).BeginInit();
            ProgramLogBindingSource.DataMember = "ProgramLog";
            ProgramLogBindingSource.DataSource = Instance;
            ((ISupportInitialize)(ProgramLogBindingSource)).EndInit();


            // Хэрэглэгчийн агуулах
            UserWarehouseTableAdapter.Initialize();


            // Хэрэглэгчийн тасаг
            UserWardTableAdapter.Initialize();


            // Зарлагын төрөл
            ExpTypeTableAdapter.Initialize();
            ((ISupportInitialize)(ExpTypeBindingSource)).BeginInit();
            ExpTypeBindingSource.DataMember = "ExpType";
            ExpTypeBindingSource.DataSource = Instance;
            ((ISupportInitialize)(ExpTypeBindingSource)).EndInit();


            // Ашиглах зарлагын төрөл
            ExpTypeOtherTableAdapter.Initialize();
            ((ISupportInitialize)(ExpTypeOtherBindingSource)).BeginInit();
            ExpTypeOtherBindingSource.DataMember = "ExpTypeOther";
            ExpTypeOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(ExpTypeOtherBindingSource)).EndInit();


            // Ерөнхий тохиргоо
            MainConfTableAdapter.Initialize();



            if (Instance.Education.Rows.Count == 0)
            {
                for (int i = 0; i < education.Length; i++)
                {
                    DataRow row = Instance.Education.NewRow();
                    row["id"] = i + 1;
                    row["name"] = education[i];
                    row.EndEdit();
                    Instance.Education.Rows.Add(row);
                }
            }

            ((ISupportInitialize)(Instance)).EndInit();
        }


        // Боловсролын нэрийг буцаах функц
        public static string getEducationName(int i)
        {
            if (i <= education.Length)
                return education[i-1];

            return "";
        }

        public static void changeConnectionString()
        {
            OrganizationTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PositionTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            ProvinceTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            HospitalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            RoleTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            UserTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PatientTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            WindowTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            WindowRoleTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            LoginUserInfoTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            UserInfoTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            CurrHospitalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            UserWindowRoleTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUser1TableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SpecialtyTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            IncTypeTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            IncTypeTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemLogTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            ProgramLogTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            UserWarehouseTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            UserWardTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            ExpTypeTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            ExpTypeOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MainConfTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserAnotherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
        }
    }
}
