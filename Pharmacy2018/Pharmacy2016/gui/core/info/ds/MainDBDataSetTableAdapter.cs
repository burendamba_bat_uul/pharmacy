﻿using Pharmacy2016.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacy2016.gui.core.info.ds.MainDBDataSetTableAdapters
{
    
    public partial class MainDBTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conDB;
            ClearBeforeFill = true;
        }
    }

}
