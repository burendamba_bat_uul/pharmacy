﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

namespace Pharmacy2016.gui.core.info.ds {
    
    
    public partial class MainDBDataSet {

        #region DataSet-н обьект

            private static MainDBDataSet INSTANCE = new MainDBDataSet();

            public static MainDBDataSet Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

        #endregion

        #region TableAdapter, BindingSource-ууд

            // Өгөгдлийн сангийн бүртгэл тохиргоо
            private static readonly MainDBDataSetTableAdapters.MainDBTableAdapter INSTANCE_MAIN_DB_TABLE_ADAPTER = new MainDBDataSetTableAdapters.MainDBTableAdapter();
            public static MainDBDataSetTableAdapters.MainDBTableAdapter MainDBTableAdapter
            {
                get
                {
                    return INSTANCE_MAIN_DB_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MAIN_DB_BINDING_SOURCE = new BindingSource();
            public static BindingSource MainDBBindingSource
            {
                get
                {
                    return INSTANCE_MAIN_DB_BINDING_SOURCE;
                }
            }

        #endregion

        // DataSet-ийг ачаалах функц.
        public static void initialize()
        {
            ((ISupportInitialize)(Instance)).BeginInit();

            Instance.DataSetName = "MainDBDataSet";
            Instance.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;


            // Өгөгдлийн сангийн тохиргоо
            MainDBTableAdapter.Initialize();
            ((ISupportInitialize)(MainDBBindingSource)).BeginInit();
            MainDBBindingSource.DataMember = "MainDB";
            MainDBBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MainDBBindingSource)).EndInit();

            ((ISupportInitialize)(Instance)).EndInit();
        }

        public static void changeConnectionString()
        {
            MainDBTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conDB;

        }

    }
}
