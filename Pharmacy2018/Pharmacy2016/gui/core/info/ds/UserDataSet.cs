﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
namespace Pharmacy2016.gui.core.info.ds
{
    
    
    public partial class UserDataSet {
    
        partial class LockMedicineDataTable
        {
        }
    
        partial class SupplierDataTable
        {
        }
    
        partial class MedicineDataTable
        {
        }
        
        #region DataSet-н обьект

            private static UserDataSet INSTANCE = new UserDataSet();

            public static UserDataSet Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

        #endregion

        #region TableAdapter, BindingSource-ууд

            // QueriesTableAdapter
            private static readonly UserDataSetTableAdapters.QueriesTableAdapter INSTANCE_QUERY_TABLE_ADAPTER = new UserDataSetTableAdapters.QueriesTableAdapter();
            public static UserDataSetTableAdapters.QueriesTableAdapter QueriesTableAdapter
            {
                get
                {
                    return INSTANCE_QUERY_TABLE_ADAPTER;
                }
            }

            // Тасаг 
            private static readonly UserDataSetTableAdapters.WardTableAdapter INSTANCE_WARD_TABLE_ADAPTER = new UserDataSetTableAdapters.WardTableAdapter();
            public static UserDataSetTableAdapters.WardTableAdapter WardTableAdapter
            {
                get
                {
                    return INSTANCE_WARD_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_WARD_BINDING_SOURCE = new BindingSource();
            public static BindingSource WardBindingSource
            {
                get
                {
                    return INSTANCE_WARD_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_WARD_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource WardOtherBindingSource
            {
                get
                {
                    return INSTANCE_WARD_OTHER_BINDING_SOURCE;
                }
            }


            // Өрөө
            private static readonly UserDataSetTableAdapters.RoomTableAdapter INSTANCE_ROOM_TABLE_ADAPTER = new UserDataSetTableAdapters.RoomTableAdapter();
            public static UserDataSetTableAdapters.RoomTableAdapter RoomTableAdapter
            {
                get
                {
                    return INSTANCE_ROOM_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ROOM_BINDING_SOURCE = new BindingSource();
            public static BindingSource RoomBindingSource
            {
                get
                {
                    return INSTANCE_ROOM_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_ROOM_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource RoomOtherBindingSource
            {
                get
                {
                    return INSTANCE_ROOM_OTHER_BINDING_SOURCE;
                }
            }


            // Эмийн төрөл 
            private static readonly UserDataSetTableAdapters.MedicineTypeTableAdapter INSTANCE_MEDICINETYPE_TABLE_ADAPTER = new UserDataSetTableAdapters.MedicineTypeTableAdapter();
            public static UserDataSetTableAdapters.MedicineTypeTableAdapter MedicineTypeTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINETYPE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINETYPE_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineTypeBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINETYPE_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINETYPE_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineTypeOtherBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINETYPE_OTHER_BINDING_SOURCE;
                }
            }


            // Эмийн олон улсын төрөл 
            private static readonly UserDataSetTableAdapters.MedicineTypeInternationalTableAdapter INSTANCE_MEDICINETYPEINTER_TABLE_ADAPTER = new UserDataSetTableAdapters.MedicineTypeInternationalTableAdapter();
            public static UserDataSetTableAdapters.MedicineTypeInternationalTableAdapter MedicineTypeInternationalTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINETYPEINTER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINETYPEINTER_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineTypeInternationalBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINETYPEINTER_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINETYPEINTER_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineTypeInterOtherBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINETYPEINTER_OTHER_BINDING_SOURCE;
                }
            }

            // Эм багц
            private static readonly UserDataSetTableAdapters.PackageMedicineTableAdapter INSTANCE_PACKAGEMEDICINE_TABLE_ADAPTER = new UserDataSetTableAdapters.PackageMedicineTableAdapter();
            public static UserDataSetTableAdapters.PackageMedicineTableAdapter PackageMedicineTableAdapter
            {
                get
                {
                    return INSTANCE_PACKAGEMEDICINE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PACKAGEMEDICINE_BINDING_SOURCE = new BindingSource();
            public static BindingSource PackageMedicineBindingSource
            {
                get
                {
                    return INSTANCE_PACKAGEMEDICINE_BINDING_SOURCE;
                }
            }
           
            // Эм 
            private static readonly UserDataSetTableAdapters.MedicineTableAdapter INSTANCE_MEDICINE_TABLE_ADAPTER = new UserDataSetTableAdapters.MedicineTableAdapter();
            public static UserDataSetTableAdapters.MedicineTableAdapter MedicineTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINE_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINE_BINDING_SOURCE;
                }
            }


            // Улс 
            private static readonly UserDataSetTableAdapters.ProducerCountryTableAdapter INSTANCE_PRODUCERCOUNTRY_TABLE_ADAPTER = new UserDataSetTableAdapters.ProducerCountryTableAdapter();
            public static UserDataSetTableAdapters.ProducerCountryTableAdapter ProducerCountryTableAdapter
            {
                get
                {
                    return INSTANCE_PRODUCERCOUNTRY_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PRODUCERCOUNTRY_BINDING_SOURCE = new BindingSource();
            public static BindingSource ProducerCountryBindingSource
            {
                get
                {
                    return INSTANCE_PRODUCERCOUNTRY_BINDING_SOURCE;
                }
            }


            // Үйлдвэрлэгч 
            private static readonly UserDataSetTableAdapters.ProducerTableAdapter INSTANCE_PRODUCER_TABLE_ADAPTER = new UserDataSetTableAdapters.ProducerTableAdapter();
            public static UserDataSetTableAdapters.ProducerTableAdapter ProducerTableAdapter
            {
                get
                {
                    return INSTANCE_PRODUCER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PRODUCER_BINDING_SOURCE = new BindingSource();
            public static BindingSource ProducerBindingSource
            {
                get
                {
                    return INSTANCE_PRODUCER_BINDING_SOURCE;
                }
            }

            // Бэлтгэн нийлүүлэгч 
            private static readonly UserDataSetTableAdapters.SupplierTableAdapter INSTANCE_SUPPLIER_TABLE_ADAPTER = new UserDataSetTableAdapters.SupplierTableAdapter();
            public static UserDataSetTableAdapters.SupplierTableAdapter SupplierTableAdapter
            {
                get
                {
                    return INSTANCE_SUPPLIER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SUPPLIER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SupplierBindingSource
            {
                get
                {
                    return INSTANCE_SUPPLIER_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_PRODUCEROTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource ProducerOtherBindingSource
            {
                get
                {
                    return INSTANCE_PRODUCEROTHER_BINDING_SOURCE;
                }
            }


            // Эмийн хэлбэр
            private static readonly UserDataSetTableAdapters.MedicineShapeTableAdapter INSTANCE_MEDICINESHAPE_TABLE_ADAPTER = new UserDataSetTableAdapters.MedicineShapeTableAdapter();
            public static UserDataSetTableAdapters.MedicineShapeTableAdapter MedicineShapeTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINESHAPE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINESHAPE_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineShapeBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINESHAPE_BINDING_SOURCE;
                }
            }


            // Эм тун хэмжээ
            private static readonly UserDataSetTableAdapters.MedicineUnitTableAdapter INSTANCE_MEDICINEUNIT_TABLE_ADAPTER = new UserDataSetTableAdapters.MedicineUnitTableAdapter();
            public static UserDataSetTableAdapters.MedicineUnitTableAdapter MedicineUnitTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINEUNIT_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINEUNIT_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineUnitBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINEUNIT_BINDING_SOURCE;
                }
            }

            //Эм хэмжих нэгж
            private static readonly UserDataSetTableAdapters.MedicineQuantityTableAdapter INSTANCE_MEDICINE_QUANTITY_TABLE_ADAPTER = new UserDataSetTableAdapters.MedicineQuantityTableAdapter();
            public static UserDataSetTableAdapters.MedicineQuantityTableAdapter MedicineQuantityTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINE_QUANTITY_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINE_QUANTITY_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineQuantityBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINE_QUANTITY_BINDING_SOURCE;
                }
            }


            // Эмчлүүлэгчийн тасаг өрөө
            private static readonly UserDataSetTableAdapters.PatientRoomBedWardTableAdapter INSTANCE_PATIENT_ROOM_BED_WARD_TABLE_ADAPTER = new UserDataSetTableAdapters.PatientRoomBedWardTableAdapter();
            public static UserDataSetTableAdapters.PatientRoomBedWardTableAdapter PatientRoomBedWardTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_ROOM_BED_WARD_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_ROOM_BED_WARD_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientRoomBedWardBindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_ROOM_BED_WARD_BINDING_SOURCE;
                }
            }


            // Эмчлүүлэгчийн тасаг өрөө
            private static readonly UserDataSetTableAdapters.PatientWardRoomBedTableAdapter INSTANCE_PATIENT_WARD_ROOM_BED_TABLE_ADAPTER = new UserDataSetTableAdapters.PatientWardRoomBedTableAdapter();
            public static UserDataSetTableAdapters.PatientWardRoomBedTableAdapter PatientWardRoomBedTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_WARD_ROOM_BED_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_WARD_ROOM_BED_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientWardRoomBedBindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_WARD_ROOM_BED_BINDING_SOURCE;
                }
            }


            // Хэрэглэгчийн skin, өнгө
            private static readonly UserDataSetTableAdapters.UserSkinColorTableAdapter INSTANCE_USER_SKIN_COLOR_TABLE_ADAPTER = new UserDataSetTableAdapters.UserSkinColorTableAdapter();
            public static UserDataSetTableAdapters.UserSkinColorTableAdapter UserSkinColorTableAdapter
            {
                get
                {
                    return INSTANCE_USER_SKIN_COLOR_TABLE_ADAPTER;
                }
            }


            // Хэрэглэгчийн layout
            private static readonly UserDataSetTableAdapters.UserLayoutTableAdapter INSTANCE_USER_LAYOUT_TABLE_ADAPTER = new UserDataSetTableAdapters.UserLayoutTableAdapter();
            public static UserDataSetTableAdapters.UserLayoutTableAdapter UserLayoutTableAdapter
            {
                get
                {
                    return INSTANCE_USER_LAYOUT_TABLE_ADAPTER;
                }
            }

            // Агуулах
            private static readonly UserDataSetTableAdapters.WareHouseTableAdapter INSTANCE_WARE_HOUSE_TABLE_ADAPTER = new UserDataSetTableAdapters.WareHouseTableAdapter();
            public static UserDataSetTableAdapters.WareHouseTableAdapter WareHouseTableAdapter
            {
                get
                {
                    return INSTANCE_WARE_HOUSE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_WARE_HOUSE_BINDING_SOURCE = new BindingSource();
            public static BindingSource WareHouseBindingSource
            {
                get
                {
                    return INSTANCE_WARE_HOUSE_BINDING_SOURCE;
                }
            }
            private static readonly BindingSource INSTANCE_WARE_HOUSE_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource WareHouseOtherBindingSource
            {
                get
                {
                    return INSTANCE_WARE_HOUSE_OTHER_BINDING_SOURCE;
                }
            }

            // Агуулах хэрэглэгч
            private static readonly UserDataSetTableAdapters.HouseUserTableAdapter INSTANCE_HOUSE_USER_TABLE_ADAPTER = new UserDataSetTableAdapters.HouseUserTableAdapter();
            public static UserDataSetTableAdapters.HouseUserTableAdapter HouseUserTableAdapter
            {
                get
                {
                    return INSTANCE_HOUSE_USER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_HOUSE_USER_BINDING_SOURCE = new BindingSource();
            public static BindingSource HouseUserBindingSource
            {
                get
                {
                    return INSTANCE_HOUSE_USER_BINDING_SOURCE;
                }
            }


            // Эм зүйч, тасаг
            private static readonly UserDataSetTableAdapters.UserWardTableAdapter INSTANCE_USER_WARD_TABLE_ADAPTER = new UserDataSetTableAdapters.UserWardTableAdapter();
            public static UserDataSetTableAdapters.UserWardTableAdapter UserWardTableAdapter
            {
                get
                {
                    return INSTANCE_USER_WARD_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_USER_WARD_BINDING_SOURCE = new BindingSource();
            public static BindingSource UserWardBindingSource
            {
                get
                {
                    return INSTANCE_USER_WARD_BINDING_SOURCE;
                }
            }

            // Багц
            private static readonly UserDataSetTableAdapters.PackageTableAdapter INSTANCE_PACKAGE_TABLE_ADAPTER = new UserDataSetTableAdapters.PackageTableAdapter();
            public static UserDataSetTableAdapters.PackageTableAdapter PackageTableAdapter
            {
                get
                {
                    return INSTANCE_PACKAGE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PACKAGE_BINDING_SOURCE = new BindingSource();
            public static BindingSource PackageBindingSource
            {
                get
                {
                    return INSTANCE_PACKAGE_BINDING_SOURCE;
                }
            }

            // Багцын эм
            private static readonly UserDataSetTableAdapters.PackageDetialTableAdapter INSTANCE_PACKAGE_DETIAL_TABLE_ADAPTER = new UserDataSetTableAdapters.PackageDetialTableAdapter();
            public static UserDataSetTableAdapters.PackageDetialTableAdapter PackageDetialTableAdapter
            {
                get
                {
                    return INSTANCE_PACKAGE_DETIAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PACKAGE_DETIAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource PackageDetialBindingSource
            {
                get
                {
                    return INSTANCE_PACKAGE_DETIAL_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_PACKAGE_DETIAL_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource PackageDetialOtherBindingSource
            {
                get
                {
                    return INSTANCE_PACKAGE_DETIAL_OTHER_BINDING_SOURCE;
                }
            }

            // Эмийн мэдээлэл үзүүлэх
            private static readonly UserDataSetTableAdapters.MedicinePackageTableAdapter INSTANCE_MEDICINE_PACKAGE_TABLE_ADAPTER = new UserDataSetTableAdapters.MedicinePackageTableAdapter();
            public static UserDataSetTableAdapters.MedicinePackageTableAdapter MedicinePackageTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINE_PACKAGE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINE_PACKAGE_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicinePackageBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINE_PACKAGE_BINDING_SOURCE;
                }
            }

            // Холбоотой эм үзүүлэх
            private static readonly UserDataSetTableAdapters.MedicineRelativesTableAdapter INSTANCE_MEDICINE_RELATIVE_TABLE_ADAPTER = new UserDataSetTableAdapters.MedicineRelativesTableAdapter();
            public static UserDataSetTableAdapters.MedicineRelativesTableAdapter MedicineRelativesTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINE_RELATIVE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINE_RELATIVE_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineRelativeBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINE_RELATIVE_BINDING_SOURCE;
                }
            }

            private static readonly UserDataSetTableAdapters.LockMedicineTableAdapter INSTANCE_LOCK_MEDICINE_TABLE_ADAPTER = new UserDataSetTableAdapters.LockMedicineTableAdapter();
            public static UserDataSetTableAdapters.LockMedicineTableAdapter LockMedicineTableAdapter
            {
                get
                {
                    return INSTANCE_LOCK_MEDICINE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_LOCK_MEDICINE_BINDING_SOURCE = new BindingSource();
            public static BindingSource LockMedicineBindingSource
            {
                get
                {
                    return INSTANCE_LOCK_MEDICINE_BINDING_SOURCE;
                }
            }


        #endregion

        // DataSet-ийг ачаалах функц.
        public static void initialize()
        {
            ((ISupportInitialize)(Instance)).BeginInit();
            
            Instance.DataSetName = "UserDataSet";
            Instance.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;


            // Хэрвээ доорх нэг мөр кодчлол нь алдаатай болсон тохиолдолд тухайн
            // DataSet-ийн designer-ийн код руу нь орж InitCommandCollection() функцийн 
            // 'private' -> 'public' болгон өөрчилнө.
            QueriesTableAdapter.InitCommandCollection();

            // Тасаг
            WardTableAdapter.Initialize();
            ((ISupportInitialize)(WardBindingSource)).BeginInit();
            WardBindingSource.DataMember = "Ward";
            WardBindingSource.DataSource = Instance;
            ((ISupportInitialize)(WardBindingSource)).EndInit();

            ((ISupportInitialize)(WardOtherBindingSource)).BeginInit();
            WardOtherBindingSource.DataMember = "Ward";
            WardOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(WardOtherBindingSource)).EndInit();


            // Өрөө
            RoomTableAdapter.Initialize();
            ((ISupportInitialize)(RoomBindingSource)).BeginInit();
            RoomBindingSource.DataMember = "Room";
            RoomBindingSource.DataSource = Instance;
            ((ISupportInitialize)(RoomBindingSource)).EndInit();

            ((ISupportInitialize)(RoomOtherBindingSource)).BeginInit();
            RoomOtherBindingSource.DataMember = "Room";
            RoomOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(RoomOtherBindingSource)).EndInit();


            // Эмийн төрөл
            MedicineTypeTableAdapter.Initialize();
            ((ISupportInitialize)(MedicineTypeBindingSource)).BeginInit();
            MedicineTypeBindingSource.DataMember = "MedicineType";
            MedicineTypeBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineTypeBindingSource)).EndInit();

            ((ISupportInitialize)(MedicineTypeOtherBindingSource)).BeginInit();
            MedicineTypeOtherBindingSource.DataMember = "MedicineType";
            MedicineTypeOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineTypeOtherBindingSource)).EndInit();


            // Эмийн олон улсын төрөл
            MedicineTypeInternationalTableAdapter.Initialize();
            ((ISupportInitialize)(MedicineTypeInterOtherBindingSource)).BeginInit();
            MedicineTypeInterOtherBindingSource.DataMember = "MedicineTypeInternational";
            MedicineTypeInterOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineTypeInterOtherBindingSource)).EndInit();

            ((ISupportInitialize)(MedicineTypeInternationalBindingSource)).BeginInit();
            MedicineTypeInternationalBindingSource.DataMember = "MedicineTypeInternational";
            MedicineTypeInternationalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineTypeInternationalBindingSource)).EndInit();


            // Эм багц 
            PackageMedicineTableAdapter.Initialize();
            ((ISupportInitialize)(PackageMedicineBindingSource)).BeginInit();
            PackageMedicineBindingSource.DataMember = "PackageMedicine";
            PackageMedicineBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PackageMedicineBindingSource)).EndInit();
                      
            // Эм
            MedicineTableAdapter.Initialize();
            ((ISupportInitialize)(MedicineBindingSource)).BeginInit();
            MedicineBindingSource.DataMember = "Medicine";
            MedicineBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineBindingSource)).EndInit();

            
            // Улс
            ProducerCountryTableAdapter.Initialize();
            ((ISupportInitialize)(ProducerCountryBindingSource)).BeginInit();
            ProducerCountryBindingSource.DataMember = "Producercountry";
            ProducerCountryBindingSource.DataSource = Instance;
            ((ISupportInitialize)(ProducerCountryBindingSource)).EndInit();


            // Үйлдвэрлэгч
            ProducerTableAdapter.Initialize();
            ((ISupportInitialize)(ProducerBindingSource)).BeginInit();
            ProducerBindingSource.DataMember = "Producer";
            ProducerBindingSource.DataSource = Instance;
            ((ISupportInitialize)(ProducerBindingSource)).EndInit();

            ((ISupportInitialize)(ProducerOtherBindingSource)).BeginInit();
            ProducerOtherBindingSource.DataMember = "Producer";
            ProducerOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(ProducerOtherBindingSource)).EndInit();

            // Үйлдвэрлэгч
            SupplierTableAdapter.Initialize();
            ((ISupportInitialize)(SupplierBindingSource)).BeginInit();
            SupplierBindingSource.DataMember = "Supplier";
            SupplierBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SupplierBindingSource)).EndInit();

            // Эмийн хэлбэр
            MedicineShapeTableAdapter.Initialize();
            ((ISupportInitialize)(MedicineShapeBindingSource)).BeginInit();
            MedicineShapeBindingSource.DataMember = "MedicineShape";
            MedicineShapeBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineShapeBindingSource)).EndInit();


            // Эмийн тун хэмжээ
            MedicineUnitTableAdapter.Initialize();
            ((ISupportInitialize)(MedicineUnitBindingSource)).BeginInit();
            MedicineUnitBindingSource.DataMember = "MedicineUnit";
            MedicineUnitBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineUnitBindingSource)).EndInit();

            // Эмийн хэмжих нэгж 
            MedicineQuantityTableAdapter.Initialize();
            ((ISupportInitialize)(MedicineQuantityBindingSource)).BeginInit();
            MedicineQuantityBindingSource.DataMember = "MedicineQuantity";
            MedicineQuantityBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineQuantityBindingSource)).EndInit();



            // Эмчлүүлэгчийн тасаг өрөө
            PatientRoomBedWardTableAdapter.Initialize();
            ((ISupportInitialize)(PatientRoomBedWardBindingSource)).BeginInit();
            PatientRoomBedWardBindingSource.DataMember = "PatientRoomBedWard";
            PatientRoomBedWardBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientRoomBedWardBindingSource)).EndInit();


            // Эмчлүүлэгчийн тасаг өрөө
            PatientWardRoomBedTableAdapter.Initialize();
            ((ISupportInitialize)(PatientWardRoomBedBindingSource)).BeginInit();
            PatientWardRoomBedBindingSource.DataMember = "PatientWardRoomBed";
            PatientWardRoomBedBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientWardRoomBedBindingSource)).EndInit();

            // Агуулах
            WareHouseTableAdapter.Initialize();
            ((ISupportInitialize)(WareHouseBindingSource)).BeginInit();
            WareHouseBindingSource.DataMember = "WareHouse";
            WareHouseBindingSource.DataSource = Instance;
            ((ISupportInitialize)(WareHouseBindingSource)).EndInit();

            ((ISupportInitialize)(WareHouseOtherBindingSource)).BeginInit();
            WareHouseOtherBindingSource.DataMember = "WareHouse";
            WareHouseOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(WareHouseOtherBindingSource)).EndInit();

            // Агуулах хэрэглэгч
            HouseUserTableAdapter.Initialize();
            ((ISupportInitialize)(HouseUserBindingSource)).BeginInit();
            HouseUserBindingSource.DataMember = "HouseUser";
            HouseUserBindingSource.DataSource = Instance;
            ((ISupportInitialize)(HouseUserBindingSource)).EndInit();

            // Багц
            PackageTableAdapter.Initialize();
            ((ISupportInitialize)(PackageBindingSource)).BeginInit();
            PackageBindingSource.DataMember = "Package";
            PackageBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PackageBindingSource)).EndInit();

            // Багцын эм
            PackageDetialTableAdapter.Initialize();
            ((ISupportInitialize)(PackageDetialBindingSource)).BeginInit();
            PackageDetialBindingSource.DataMember = "Package_PackageDetial";
            PackageDetialBindingSource.DataSource = PackageBindingSource;
            ((ISupportInitialize)(PackageDetialBindingSource)).EndInit();

            ((ISupportInitialize)(PackageDetialOtherBindingSource)).BeginInit();
            PackageDetialOtherBindingSource.DataMember = "PackageDetial";
            PackageDetialOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PackageDetialOtherBindingSource)).EndInit();

            // Эм авах
            MedicinePackageTableAdapter.Initialize();
            ((ISupportInitialize)(MedicinePackageBindingSource)).BeginInit();
            MedicinePackageBindingSource.DataMember = "MedicinePackage";
            MedicinePackageBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicinePackageBindingSource)).EndInit();

            // Холбоотой эм
            MedicineRelativesTableAdapter.Initialize();
            ((ISupportInitialize)(MedicineRelativeBindingSource)).BeginInit();
            MedicineRelativeBindingSource.DataMember = "MedicineRelatives";
            MedicineRelativeBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineRelativeBindingSource)).EndInit();

            // Хэрэглэгчийн skin, өнгө
            UserSkinColorTableAdapter.Initialize();

            // Хэрэглэгчийн layout
            UserLayoutTableAdapter.Initialize();            

            // Эм зүйч
            UserWardTableAdapter.Initialize();
            ((ISupportInitialize)(UserWardBindingSource)).BeginInit();
            UserWardBindingSource.DataMember = "UserWard";
            UserWardBindingSource.DataSource = Instance;
            ((ISupportInitialize)(UserWardBindingSource)).EndInit();

            // Гүйлгээ түгжих
            LockMedicineTableAdapter.Initialize();
            ((ISupportInitialize)(LockMedicineBindingSource)).BeginInit();
            LockMedicineBindingSource.DataMember = "lockMedicine";
            LockMedicineBindingSource.DataSource = Instance;
            ((ISupportInitialize)(LockMedicineBindingSource)).EndInit();


            if (Instance.IsSpecial.Rows.Count == 0)
            {
                string[] isSpecial = {
                        "Энгийн",
                        "Мансууруулах үйлчилгээтэй",
                        "Харшил өгдөг"                      
                };

                for (int i = 0; i < isSpecial.Length; i++)
                {
                    DataRow row = Instance.IsSpecial.NewRow();
                    row["id"] = i + 1;
                    row["name"] = isSpecial[i];
                    row.EndEdit();
                    Instance.IsSpecial.Rows.Add(row);
                }
            }

            ((ISupportInitialize)(Instance)).EndInit();
        }


        public static void changeConnectionString()
        {
            WardTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            RoomTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MedicineTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MedicineTypeTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MedicineTypeInternationalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MedicineShapeTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MedicineUnitTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MedicineQuantityTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            ProducerCountryTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            ProducerTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SupplierTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PatientRoomBedWardTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PatientWardRoomBedTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            WareHouseTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            HouseUserTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            UserSkinColorTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            UserLayoutTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            UserWardTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PackageTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PackageDetialTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MedicinePackageTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MedicineRelativesTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PackageMedicineTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            //LockMedicineTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
        }

    }
}


namespace Pharmacy2016.gui.core.info.ds.UserDataSetTableAdapters {
    partial class MedicineTableAdapter
    {
    }
    
    
    public partial class PackageMedicineTableAdapter {
    }
}
