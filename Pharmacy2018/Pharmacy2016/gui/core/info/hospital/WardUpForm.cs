﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.util;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.gui.core.info.hospital
{
    public partial class WardUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static WardUpForm INSTANCE = new WardUpForm();

            public static WardUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private DataRow[] wardConns;

            private WardUpForm()
            {

            }

            
            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                initBinding();
                reload();
            }

            private void reload()
            {
                //if (UserDataSet.Instance.WareHouse.Rows.Count == 0)
                //    UserDataSet.WareHouseTableAdapter.Fill(UserDataSet.Instance.WareHouse, HospitalUtil.getHospitalId());
            }

            private void initBinding()
            {
                treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", UserDataSet.WardBindingSource, "pid", true));
                textEditName.DataBindings.Add(new Binding("EditValue", UserDataSet.WardBindingSource, "name", true));
                checkEditIsUse.DataBindings.Add(new Binding("EditValue", UserDataSet.WardBindingSource, "isUseMedice", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", UserDataSet.WardBindingSource, "description", true));
            }

            private void initDataSource()
            {
                treeListLookUpEditWard.Properties.DataSource = UserDataSet.WardOtherBindingSource;
            }

            private void initError()
            {
                //treeListLookUpEditWareHouses.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insertWareHouse();
                    }
                    else if(actionType == 2)
                    {
                        editWareHouse();
                    }
                    ShowDialog(WardRegisterForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                clearErrorText();
                UserDataSet.WardBindingSource.CancelEdit();
                UserDataSet.Instance.Ward.RejectChanges();
            }

            private void clearErrorText()
            {
                textEditName.ErrorText = "";
                //treeListLookUpEditWareHouses.ErrorText = "";
                treeListLookUpEditWard.ErrorText = "";
            }

            private void WardUpForm_Shown(object sender, EventArgs e)
            { 
                treeListLookUpEditWard.Focus();
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }

            private void insertWareHouse()
            {
                this.Text = "Тасаг нэмэх цонх";
            }

            private void editWareHouse()
            {
                wardConns = ReportDataSet.Instance.WardConn.Select("pharmacyID = '" + WardRegisterForm.Instance.currentWardView["id"] + "'");
                if (wardConns.Length > 0)
                    textEditAcoulusID.EditValue = wardConns[0]["acoulusID"];
                this.Text = "Тасаг засах цонх";
            }

            private void addRow()
            {
                WardRegisterForm.Instance.currentWardView = (DataRowView)UserDataSet.WardBindingSource.AddNew();
                WardRegisterForm.Instance.currentWardView["id"] = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                textEditAcoulusID.EditValue = null;
            }

        #endregion

        #region Формын event

            private void treeListLookUpEditWard_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditWard.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        textEditName.Focus();
                    }
                }
            }

            private void saveButton_Click(object sender, EventArgs e)
            {
                save();
            }

            private void treeListLookUpEditWareHouses_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "clear"))
                {
                    treeListLookUpEditWard.Text = "0";
                }
            }

        #endregion

        #region Формын функц.

            private bool validation()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.EditValue == DBNull.Value || (treeListLookUpEditWard.EditValue = treeListLookUpEditWard.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Үндсэн тасгыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (textEditName.EditValue == null || textEditName.EditValue == DBNull.Value || (textEditName.EditValue = textEditName.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Тасгийн нэрийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditName.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (validation())
                {

                    UserDataSet.WardBindingSource.EndEdit();
                    UserDataSet.WardTableAdapter.Update(UserDataSet.Instance.Ward);
                    if (textEditAcoulusID.EditValue != null && textEditAcoulusID.EditValue != DBNull.Value)
                    {
                        if (wardConns.Length == 0)
                        {
                            ReportDataSet.Instance.WardConn.idColumn.DefaultValue = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                            DataRowView wardConn = (DataRowView)ReportDataSet.WardConnBindingSource.AddNew();
                            wardConn["acoulusID"] = textEditAcoulusID.EditValue;
                            wardConn["pharmacyID"] = WardRegisterForm.Instance.currentWardView["id"];
                            wardConn.EndEdit();
                        }else
                        {
                            wardConns[0]["acoulusID"] = textEditAcoulusID.EditValue;
                            wardConns[0].EndEdit();
                        }
                        
                        ReportDataSet.WardConnTableAdapter.Update(ReportDataSet.Instance.WardConn);
                    }
                    addRow();
                    //clearData();
                    //WareHouseForm.Instance.setDefaultHouse();
                    //WareHouseForm.Instance.addDoubleHouse();                 
                }
            }

        #endregion                         
        
    }
}