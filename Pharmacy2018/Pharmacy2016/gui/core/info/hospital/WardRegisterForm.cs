﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using DevExpress.XtraEditors.Controls;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.gui.core.info.hospital
{
    /*
     * Эмнэлэгийн тасаг, өрөө, орыг харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class WardRegisterForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц.

            private static WardRegisterForm INSTANCE = new WardRegisterForm();

            public static WardRegisterForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private readonly int FORM_ID = 3002;

            private bool isEditTree = false;

            private bool isExpandTree = false;

            private bool isInsertPharmacist = false;

            private List<string> deleteRows = new List<string>();

            public DataRowView currentWardView;

            public DataRowView currentRoomView;

            private System.IO.Stream layout_room = null;
            private System.IO.Stream layout_pharmacist_conf = null;

            private WardRegisterForm()
            {
 
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initLayout();
                RoleUtil.addForm(FORM_ID, this);

                treeListWard.DataSource = UserDataSet.WardBindingSource;
                treeListRoom.DataSource = UserDataSet.RoomBindingSource;
                gridControlUserWard.DataSource = UserDataSet.UserWardBindingSource;

                repositoryItemGridLookUpEdit1.DataSource = InformationDataSet.SystemUserOtherBindingSource;
                repositoryItemTreeListLookUpEdit1.DataSource = UserDataSet.WardBindingSource;
            }

            private void initLayout()
            {
                layout_room = new System.IO.MemoryStream();
                treeListRoom.SaveLayoutToStream(layout_room);
                layout_room.Seek(0, System.IO.SeekOrigin.Begin);

                layout_pharmacist_conf = new System.IO.MemoryStream();
                gridViewUserWard.SaveLayoutToStream(layout_pharmacist_conf);
                layout_pharmacist_conf.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion

        #region Форм хаах, нээх функц.

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        checkRole();
                        isUseStore();
                        xtraTabControl1.SelectedTabPage = xtraTabPageRoomBed;

                        UserDataSet.Instance.Ward.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.Ward.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.Room.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.Room.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.UserWard.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.UserWard.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        reload();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                if (roles[1] && UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                {
                    controlNavigatorWard.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                    controlNavigatorWard.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                    controlNavigatorWard.Buttons.CustomButtons[3].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                }
                else
                {
                    controlNavigatorWard.Buttons.CustomButtons[1].Visible = false;
                    controlNavigatorWard.Buttons.CustomButtons[2].Visible = false;
                    controlNavigatorWard.Buttons.CustomButtons[3].Visible = false;
                }

                simpleButtonAddBed.Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                simpleButtonAddRoom.Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                simpleButtonEdit.Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                simpleButtonDelete.Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                simpleButtonSave.Visible = !SystemUtil.SELECTED_DB_READONLY && (roles[1] || roles[2] || roles[3]);

                gridControlUserWard.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlUserWard.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridViewUserWard.OptionsBehavior.Editable = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;

                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("room");
                if (layout != null)
                {
                    treeListRoom.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    treeListRoom.RestoreLayoutFromStream(layout_room);
                    layout_room.Seek(0, System.IO.SeekOrigin.Begin);
                }
                layout = UserUtil.getLayout("userWard");
                if (layout != null)
                {
                    gridViewUserWard.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewUserWard.RestoreLayoutFromStream(layout_pharmacist_conf);
                    layout_pharmacist_conf.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }
            
            private void WardRegisterForm_FormClosing(object sender, FormClosingEventArgs e)
            {   
                e.Cancel = true;
                clearData();
                isInsertPharmacist = false;
                Hide();
            }

            private void clearData()
            {
                deleteRows.Clear();
                UserDataSet.Instance.Ward.Clear();
                UserDataSet.Instance.Room.Clear();
            }

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                UserDataSet.WardTableAdapter.Fill(UserDataSet.Instance.Ward, HospitalUtil.getHospitalId());
                UserDataSet.RoomTableAdapter.Fill(UserDataSet.Instance.Room, HospitalUtil.getHospitalId(), (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID ? "" : UserUtil.getUserWardID()));
                UserDataSet.UserWardTableAdapter.Fill(UserDataSet.Instance.UserWard, HospitalUtil.getHospitalId());
                InformationDataSet.SystemUserOtherTableAdapter.Fill(InformationDataSet.Instance.SystemUserOther, HospitalUtil.getHospitalId(), SystemUtil.ALL_USER);
                gridViewUserWard.ActiveFilterString = "";
                ReportDataSet.WardConnTableAdapter.Fill(ReportDataSet.Instance.WardConn);

                treeListWard.ExpandAll();
                treeListRoom.ExpandAll();
                isExpandTree = true;
                controlNavigatorWard.Buttons.CustomButtons[0].ImageIndex = 14;
                ProgramUtil.closeWaitDialog();
            }

            public void isUseStore()
            {
                if (simpleButtonAddBed != null)
                    simpleButtonAddBed.Visible = RoleUtil.getIsUseStoreOrder();
                if (simpleButtonAddRoom != null)
                    simpleButtonAddRoom.Visible = RoleUtil.getIsUseStoreOrder();
                if (simpleButtonEdit != null)
                    simpleButtonEdit.Visible = RoleUtil.getIsUseStoreOrder();
                if (simpleButtonDelete != null)
                    simpleButtonDelete.Visible = RoleUtil.getIsUseStoreOrder();
                if (simpleButtonSave != null)
                    simpleButtonSave.Visible = RoleUtil.getIsUseStoreOrder();
                if (xtraTabPageRoomBed != null)
                    xtraTabPageRoomBed.PageVisible = RoleUtil.getIsUseStoreOrder();
                if(Visible)
                   xtraTabControl1.SelectedTabPage = xtraTabPageRoomBed;
            }

        #endregion

        #region Формын эвэнтүүд.

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                saveLayout();
            }


            private void controlNavigatorWard_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addWard();
                    }
                    else if (String.Equals(e.Button.Tag, "delete"))
                    {
                        deleteWard();
                    }
                    else if (String.Equals(e.Button.Tag, "edit"))
                    {
                        editWard();
                    }
                    else if (String.Equals(e.Button.Tag, "expand"))
                    {
                        expandTreeWard();
                    }
                }
            }

            // өрөө, ор устгах эвэнт
            private void ButtonDelete_Click(object sender, EventArgs e)
            {
                deleteRoom();
            }

            // ор нэмэх функц event. 
            private void AddButtonRoom_Click(object sender, EventArgs e)
            {
                addRoom();
            }

            // өрөө нэмэх эвэнт.
            private void AddButtonBed_Click(object sender, EventArgs e)
            {
                addBed();
            }

            // өрөө, ор хадгалах эвэнт
            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                saveRoom();
            }

            private void RoomButtonEdit_Click(object sender, EventArgs e)
            {
                editRoom();
            }

            private void treeListRoom_BeforeFocusNode(object sender, BeforeFocusNodeEventArgs e)
            {
                if (e.OldNode != null && (e.OldNode.GetValue("name") == DBNull.Value))
                {
                    XtraMessageBox.Show("Эхлээд сонгосон өрөө эсвэл ороо хадгална уу!");
                    e.CanFocus = false;
                }
            }

            private void treeListWard_BeforeFocusNode(object sender, BeforeFocusNodeEventArgs e)
            {
                if (e.OldNode != null && (e.OldNode.GetValue("name") == DBNull.Value))
                {                 
                    XtraMessageBox.Show("Сонгосон тасгаа хадгална уу!");
                    e.CanFocus = false;
                }
            }

            private void treeListWard_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
            {
                if (xtraTabControl1.SelectedTabPage == xtraTabPageRoomBed)
                {
                    if (treeListWard.FocusedNode != null)
                        UserDataSet.RoomBindingSource.Filter = "wardID = '" + treeListWard.FocusedNode.GetValue("id") + "'";
                }
                else if (xtraTabControl1.SelectedTabPage == xtraTabPageUser)
                {
                    if (treeListWard.FocusedNode != null)
                        gridViewUserWard.ActiveFilterString = "wardID = '" + treeListWard.FocusedNode.GetValue("id") + "'";
                }
                
            }

            private void gridControlPharmacist_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addUserWard();
                    }
                    else if (String.Equals(e.Button.Tag, "delete"))
                    {
                        try
                        {
                            deleteUserWard();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                }
            }

            private void gridViewUserWard_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
            {
                if (gridViewUserWard.GetFocusedRowCellValue(gridColumnPharmacist).ToString().Trim().Equals(""))
                {
                    e.Valid = false;
                    gridViewUserWard.SetColumnError(gridColumnPharmacist, "Хэрэглэгчийг сонгоно уу.");
                }
                if (gridViewUserWard.GetFocusedRowCellValue(gridColumnWard).ToString().Trim().Equals(""))
                {
                    e.Valid = false;
                    gridViewUserWard.SetColumnError(gridColumnWard, "Тасгийг оруулна уу.");
                }

                if (e.Valid)
                {

                    DataRow[] rows = UserDataSet.Instance.UserWard.Select("userID = '" + gridViewUserWard.GetFocusedRowCellValue(gridColumnPharmacist).ToString() + "' AND wardID = '" + gridViewUserWard.GetFocusedRowCellValue(gridColumnWard).ToString() + "'");
                    if (rows.Length != 0)
                    {
                        if (isInsertPharmacist)
                        {
                            e.Valid = false;
                            gridViewUserWard.SetColumnError(gridColumnWard, "Хэрэглэгч, тасаг давхацсан байна.");
                        }
                        else
                        {
                            DataRow row = gridViewUserWard.GetFocusedDataRow();
                            if (rows[0]["id"] != row["id"])
                            {
                                e.Valid = false;
                                gridViewUserWard.SetColumnError(gridColumnWard, "Хэрэглэгч, тасаг давхацсан байна.");
                            }
                        }
                    }
                }
            }

            private void gridViewUserWard_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                e.ExceptionMode = ExceptionMode.NoAction;
            }

            private void gridViewUserWard_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
            {
                try
                {
                    if (!gridViewUserWard.HasColumnErrors)
                        saveUserWard();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
            {
                if (xtraTabControl1.SelectedTabPage == xtraTabPageRoomBed)
                {
                    simpleButtonAddRoom.Visible = true;
                    simpleButtonAddBed.Visible = true;
                    simpleButtonDelete.Visible = true;
                    simpleButtonEdit.Visible = true;
                    simpleButtonSave.Visible = true;
                }
                else if (xtraTabControl1.SelectedTabPage == xtraTabPageUser)
                {
                    simpleButtonAddRoom.Visible = false;
                    simpleButtonAddBed.Visible = false;
                    simpleButtonDelete.Visible = false;
                    simpleButtonEdit.Visible = false;
                    simpleButtonSave.Visible = false;
                }
            }

        #endregion

        #region Тасаг нэмэх, засах, хадгалах, устгах функцууд.

            private void addWard()
            {
                UserDataSet.Instance.Ward.idColumn.DefaultValue = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                if (treeListWard.FocusedNode == null)
                    UserDataSet.Instance.Ward.pidColumn.DefaultValue = 0;
                else if (treeListWard.FocusedNode != null && treeListWard.FocusedNode.Level == 0)
                    UserDataSet.Instance.Ward.pidColumn.DefaultValue = treeListWard.FocusedNode.GetValue("id"); 
      
                currentWardView = (DataRowView)UserDataSet.WardBindingSource.AddNew();
                WardUpForm.Instance.showForm(1);
            }

            private void editWard()
            {
                if (treeListWard.FocusedNode.Level == 0)
                    return;

                currentWardView = (DataRowView)UserDataSet.WardBindingSource.Current;
                WardUpForm.Instance.showForm(2);
            }

            private void deleteWard()
            {
                if (treeListWard.FocusedNode != null)
                {
                    TreeListNode node = treeListWard.FocusedNode;
                    if (node.HasChildren)
                    {
                        XtraMessageBox.Show("Тасаг дэд мөчиртэй тул устгаж болохгүй!");
                        return;
                    }

                    object ret = UserDataSet.QueriesTableAdapter.can_delete_ward(HospitalUtil.getHospitalId(), node.GetValue("id").ToString());
                    if (ret != DBNull.Value && ret != null)
                    {
                        string msg = ret.ToString();
                        XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                        return;
                    }
                    DialogResult dialogResult = XtraMessageBox.Show("Тасгийг устгах уу!", "Тасаг устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        treeListWard.DeleteNode(treeListWard.FocusedNode);
                        UserDataSet.WardBindingSource.EndEdit();
                        UserDataSet.WardTableAdapter.Update(UserDataSet.Instance.Ward);
                    }
                }
                else
                {
                    XtraMessageBox.Show("Устгах тасгаа сонгоно уу!");
                }
            }

            private void expandTreeWard()
            {
                if (isExpandTree)
                {
                    treeListWard.CollapseAll();
                    controlNavigatorWard.Buttons.CustomButtons[0].ImageIndex = 17;
                }
                else
                {
                    treeListWard.ExpandAll();
                    controlNavigatorWard.Buttons.CustomButtons[0].ImageIndex = 14;
                }
                isExpandTree = !isExpandTree;
            }

        #endregion

        #region Өрөө болон ор нэмэх, засах, хадгалах, устгах функцууд.

            private void addRoom()
            {
                if (Convert.ToBoolean(treeListWard.FocusedNode.GetValue("isUseMedice")) == false)
                {
                    XtraMessageBox.Show("Хэвтэн эмчлүүлэгчгүй тасаг дээр өрөө нэмж болохгүй");
                    return;
                }

                if (treeListWard.FocusedNode.Level == 0)
                {
                    XtraMessageBox.Show("Үндсэн тасаг дээр өрөө нэмж болохгүй");
                    return;
                }

                UserDataSet.Instance.Room.idColumn.DefaultValue = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                if(treeListRoom.FocusedNode == null)
                {   
                    UserDataSet.Instance.Room.pidColumn.DefaultValue = '0';
                    currentRoomView = (DataRowView)UserDataSet.RoomBindingSource.AddNew();
                    if (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                    {
                        currentRoomView["wardID"] = treeListWard.FocusedNode.GetValue("id");
                    }
                    else
                    {
                        currentRoomView["wardID"] = UserUtil.getUserWardID();
                    }
                    
                    treeListRoom.OptionsBehavior.Editable = true;
                }
                else if (treeListRoom.FocusedNode != null)
                {   
                    DataRowView view = (DataRowView)UserDataSet.RoomBindingSource.Current;
                    if (view.IsNew)
                    {
                        UserDataSet.RoomBindingSource.CancelEdit();
                        UserDataSet.Instance.Room.RejectChanges();
                    }
                    UserDataSet.Instance.Room.pidColumn.DefaultValue = '0';
                    currentRoomView = (DataRowView)UserDataSet.RoomBindingSource.AddNew();
                    if (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                    {
                        currentRoomView["wardID"] = treeListWard.FocusedNode.GetValue("id");
                    }
                    else
                    {
                        currentRoomView["wardID"] = UserUtil.getUserWardID();
                    }
                    treeListRoom.OptionsBehavior.Editable = true;
                }
            }

            private void addBed()
            {
                if (treeListWard.FocusedNode.Level == 0)
                {
                    XtraMessageBox.Show("Үндсэн тасаг дээр ор нэмж болохгүй");
                    return;
                }

                if (Convert.ToBoolean(treeListWard.FocusedNode.GetValue("isUseMedice")) == false)
                {
                    XtraMessageBox.Show("Хэвтэн эмчлүүлэгчгүй тасаг дээр ор нэмж болохгүй");
                    return;
                }
                
                if (treeListRoom.FocusedNode != null) 
                { 
                    if( treeListRoom.FocusedNode.Level == 0)
                    {
                        UserDataSet.Instance.Room.pidColumn.DefaultValue = treeListRoom.FocusedNode.GetValue("id");
                        UserDataSet.Instance.Room.idColumn.DefaultValue = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                        currentRoomView = (DataRowView)UserDataSet.RoomBindingSource.AddNew();
                        if (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                        {
                            currentRoomView["wardID"] = treeListWard.FocusedNode.GetValue("id");
                        }
                        else
                        {
                            currentRoomView["wardID"] = UserUtil.getUserWardID();
                        }
                        treeListRoom.OptionsBehavior.Editable = true;
                    }

                    else if (treeListRoom.FocusedNode.Level != 0)
                    {
                        if (!isValidateTreeNodeEmptyRoom())
                        {
                            DataRowView view = (DataRowView)UserDataSet.RoomBindingSource.Current;
                            if (view.IsNew)
                            {
                                UserDataSet.RoomBindingSource.CancelEdit();
                                UserDataSet.Instance.Room.RejectChanges();
                            }
                        }    
                        XtraMessageBox.Show("Өрөө сонгоно уу!");
                        return;
                    }
                }
                else
                {
                    XtraMessageBox.Show("Өрөө нэмнэ үү!");
                }
            }

            private void editRoom()
            {
                if (!isEditTree)
                {
                    treeListRoom.OptionsBehavior.Editable = true;
                    treeListColumnRoom.AppearanceCell.BackColor = Color.Yellow;
                    treeListColumnIsUse.AppearanceCell.BackColor = Color.Yellow;
                }
                else
                {
                    treeListRoom.OptionsBehavior.Editable = false;
                    treeListColumnRoom.AppearanceCell.BackColor = Color.Transparent;
                    treeListColumnIsUse.AppearanceCell.BackColor = Color.Transparent;
                }
                isEditTree = !isEditTree;
            }

            private void deleteRoom()
            {
                if(treeListRoom.FocusedNode != null)
                {
                    TreeListNode node = treeListRoom.FocusedNode;
                    if (node.HasChildren)
                    {
                        XtraMessageBox.Show("Энэ өрөө ортой тул устгаж болохгүй!");
                        return;
                    }

                    if (node.GetValue("pid").ToString().Equals("0"))
                    {
                        object ret = UserDataSet.QueriesTableAdapter.can_delete_room(HospitalUtil.getHospitalId(), node.GetValue("id").ToString());
                        if (ret != DBNull.Value && ret != null)
                        {
                            string msg = ret.ToString();
                            XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                            return;
                        }
                    }
                    else
                    {
                        object ret = UserDataSet.QueriesTableAdapter.can_delete_bed(HospitalUtil.getHospitalId(), node.GetValue("id").ToString());
                        if (ret != DBNull.Value && ret != null)
                        {
                            string msg = ret.ToString();
                            XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                            return;
                        }
                    }

                    DialogResult dialogResult = XtraMessageBox.Show("Өрөөг устгах уу!", "Өрөө, ор устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        treeListRoom.DeleteNode(treeListRoom.FocusedNode);
                        UserDataSet.RoomBindingSource.EndEdit();
                        UserDataSet.RoomTableAdapter.Update(UserDataSet.Instance.Room);
                    }
                }    
                else
                {
                    XtraMessageBox.Show("Устгах өрөө, ор алга!");
                }
            }

            private void saveRoom()
            {
                try
                {   
                    if (treeListRoom.Nodes != null)
                    {
                        foreach (TreeListNode childNode in treeListRoom.Nodes)
                        {
                            if (childNode.HasChildren)
                            {
                                foreach (TreeListNode node in childNode.Nodes)
                                {
                                    if (node.GetValue("name").ToString().Trim().Equals(""))
                                    {
                                        XtraMessageBox.Show("Орны нэрийг оруулна уу!");
                                        treeListRoom.FocusedNode = node;
                                        return;
                                    }
                                }
                            }
                            if (childNode.GetValue("name").ToString().Trim().Equals(""))
                            {
                                XtraMessageBox.Show("Өрөөний нэрийг оруулна уу!");
                                treeListRoom.FocusedNode = childNode;
                                return;
                            }
                        }
                        UserDataSet.RoomBindingSource.EndEdit();
                        UserDataSet.RoomTableAdapter.Update(UserDataSet.Instance.Room);
                        treeListColumnRoom.AppearanceCell.BackColor = Color.Transparent;
                        treeListColumnIsUse.AppearanceCell.BackColor = Color.Transparent;
                    }
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                    return;
                }
            }

            private bool isValidateTreeNodeEmptyRoom()
            {
                if (treeListRoom.FocusedNode != null && treeListRoom.FocusedNode.GetValue("name").ToString().Trim().Equals(""))
                {
                    treeListRoom.FocusedNode.SetValue("name", "");
                    return false;
                }
                return true;
            }

        #endregion

        #region Хэрэглэгч, тасгийн тохиргоо нэмэх, засах, устгах функц

            private void addUserWard()
            {
                //if (treeListWard.FocusedNode.Level == 0 && treeListWard.FocusedNode.GetValue("id") != null)
                //{
                //    XtraMessageBox.Show("Үндсэн тасаг дээр ажилтан нэмж болохгүй");
                //    return;
                //}


                UserDataSet.Instance.UserWard.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                DataRowView view = (DataRowView)UserDataSet.UserWardBindingSource.AddNew();
                isInsertPharmacist = true;
                gridViewUserWard.ShowEditForm();
            }

            private void deleteUserWard()
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Хэрэглэгч, тасаг устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    DataRowView view = (DataRowView)gridViewUserWard.GetFocusedRow();
                    if (view != null)
                    {
                        UserUtil.removeUserWard(view);
                    }       
                    gridViewUserWard.DeleteSelectedRows();
                    saveUserWard();
                }
            }

            private void saveUserWard()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                UserDataSet.UserWardBindingSource.EndEdit();
                UserDataSet.UserWardTableAdapter.Update(UserDataSet.Instance.UserWard);
                isInsertPharmacist = false;
                gridViewUserWard.ClearColumnErrors();
                ProgramUtil.closeWaitDialog();
               
                for (int i = 0; gridViewUserWard != null && i < gridViewUserWard.RowCount; i++)
                {    
                    DataRow row = gridViewUserWard.GetDataRow(i);
                    if (row != null)
                    {
                        UserUtil.addUserWard(row);
                    }
                }
            }

        #endregion

        #region Хэвлэх, хөрвүүлэх функц

            private void barButtonItemWard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemWard.Caption;
                dropDownButtonPrint.Image = barButtonItemWard.Glyph;

                ProgramUtil.convertGrid(treeListWard, "Тасагийн бүртгэл");
            }

            private void barButtonItemRoomBed_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemRoomBed.Caption;
                dropDownButtonPrint.Image = barButtonItemRoomBed.Glyph;

                ProgramUtil.convertGrid(treeListRoom, "Өрөө, орны бүртгэл");
            }

            private void barButtonItemEndBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemEndBal.Caption;
                dropDownButtonPrint.Image = barButtonItemEndBal.Glyph;
            }

            private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemConvert.Caption;
                dropDownButtonPrint.Image = barButtonItemConvert.Glyph;
            }

            private void saveLayout()
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    System.IO.StreamReader reader = null;
                    if (xtraTabControl1.SelectedTabPage == xtraTabPageRoomBed)
                    {
                        treeListRoom.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        string layout = reader.ReadToEnd();
                        UserUtil.saveUserLayout("room", layout);
                    }
                    else if (xtraTabControl1.SelectedTabPage == xtraTabPageUser)
                    {
                        gridViewUserWard.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        reader = new System.IO.StreamReader(str);
                        UserUtil.saveUserLayout("userWard", reader.ReadToEnd());
                        reader.Close();
                    }
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion                        

    }
}