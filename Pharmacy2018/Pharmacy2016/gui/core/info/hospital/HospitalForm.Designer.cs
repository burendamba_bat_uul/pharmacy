﻿namespace Pharmacy2016.gui.core.info.hospital
{
    partial class HospitalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HospitalForm));
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageAddress = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditAddress = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.textEditFax = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.textEditPostBox = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.textEditPhone = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPageLink = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.textEditTwitter = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.textEditFacebook = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.textEditEmail = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEditWebsite = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.textEditAccount = new DevExpress.XtraEditors.TextEdit();
            this.textEditBank = new DevExpress.XtraEditors.TextEdit();
            this.textEditRegister = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gridLookUpEditDirector = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.gridLookUpEditSubDirector = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit = new DevExpress.XtraEditors.PictureEdit();
            this.gridLookUpEditTreatment = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelAccountant = new System.Windows.Forms.Label();
            this.textEditAccountant = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPostBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhone.Properties)).BeginInit();
            this.xtraTabPageLink.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTwitter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFacebook.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWebsite.Properties)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegister.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDirector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSubDirector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTreatment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAccountant.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // textEditName
            // 
            this.textEditName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditName.EnterMoveNextControl = true;
            this.textEditName.Location = new System.Drawing.Point(266, 30);
            this.textEditName.Name = "textEditName";
            this.textEditName.Properties.MaxLength = 50;
            this.textEditName.Size = new System.Drawing.Size(200, 20);
            this.textEditName.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(169, 33);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(91, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Эмнэлэгийн нэр*: ";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditDescription.EnterMoveNextControl = true;
            this.memoEditDescription.Location = new System.Drawing.Point(162, 134);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(405, 64);
            this.memoEditDescription.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(205, 59);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(55, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Захирал*: ";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(63, 135);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(93, 13);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "Нэмэлт мэдээлэл : ";
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(497, 426);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 52;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(416, 426);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 51;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl.Location = new System.Drawing.Point(12, 204);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageAddress;
            this.xtraTabControl.Size = new System.Drawing.Size(560, 216);
            this.xtraTabControl.TabIndex = 4;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageAddress,
            this.xtraTabPageLink,
            this.xtraTabPage1});
            // 
            // xtraTabPageAddress
            // 
            this.xtraTabPageAddress.Controls.Add(this.labelControl7);
            this.xtraTabPageAddress.Controls.Add(this.memoEditAddress);
            this.xtraTabPageAddress.Controls.Add(this.labelControl6);
            this.xtraTabPageAddress.Controls.Add(this.textEditFax);
            this.xtraTabPageAddress.Controls.Add(this.labelControl5);
            this.xtraTabPageAddress.Controls.Add(this.textEditPostBox);
            this.xtraTabPageAddress.Controls.Add(this.labelControl4);
            this.xtraTabPageAddress.Controls.Add(this.textEditPhone);
            this.xtraTabPageAddress.Name = "xtraTabPageAddress";
            this.xtraTabPageAddress.Size = new System.Drawing.Size(554, 188);
            this.xtraTabPageAddress.Text = "Холбоо барих хаяг";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(89, 91);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(54, 13);
            this.labelControl7.TabIndex = 10;
            this.labelControl7.Text = "Байршил : ";
            // 
            // memoEditAddress
            // 
            this.memoEditAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditAddress.EnterMoveNextControl = true;
            this.memoEditAddress.Location = new System.Drawing.Point(149, 88);
            this.memoEditAddress.Name = "memoEditAddress";
            this.memoEditAddress.Properties.MaxLength = 200;
            this.memoEditAddress.Size = new System.Drawing.Size(402, 97);
            this.memoEditAddress.TabIndex = 14;
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl6.Location = new System.Drawing.Point(108, 65);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(35, 13);
            this.labelControl6.TabIndex = 7;
            this.labelControl6.Text = "Факс : ";
            // 
            // textEditFax
            // 
            this.textEditFax.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditFax.EnterMoveNextControl = true;
            this.textEditFax.Location = new System.Drawing.Point(149, 62);
            this.textEditFax.Name = "textEditFax";
            this.textEditFax.Properties.MaxLength = 50;
            this.textEditFax.Size = new System.Drawing.Size(200, 20);
            this.textEditFax.TabIndex = 13;
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(25, 39);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(118, 13);
            this.labelControl5.TabIndex = 5;
            this.labelControl5.Text = "Шуудангийн хайрцаг : ";
            // 
            // textEditPostBox
            // 
            this.textEditPostBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditPostBox.EnterMoveNextControl = true;
            this.textEditPostBox.Location = new System.Drawing.Point(149, 36);
            this.textEditPostBox.Name = "textEditPostBox";
            this.textEditPostBox.Properties.MaxLength = 50;
            this.textEditPostBox.Size = new System.Drawing.Size(200, 20);
            this.textEditPostBox.TabIndex = 12;
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(109, 13);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(34, 13);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Утас : ";
            // 
            // textEditPhone
            // 
            this.textEditPhone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditPhone.EnterMoveNextControl = true;
            this.textEditPhone.Location = new System.Drawing.Point(149, 10);
            this.textEditPhone.Name = "textEditPhone";
            this.textEditPhone.Properties.MaxLength = 50;
            this.textEditPhone.Size = new System.Drawing.Size(200, 20);
            this.textEditPhone.TabIndex = 11;
            // 
            // xtraTabPageLink
            // 
            this.xtraTabPageLink.Controls.Add(this.labelControl11);
            this.xtraTabPageLink.Controls.Add(this.textEditTwitter);
            this.xtraTabPageLink.Controls.Add(this.labelControl8);
            this.xtraTabPageLink.Controls.Add(this.textEditFacebook);
            this.xtraTabPageLink.Controls.Add(this.labelControl9);
            this.xtraTabPageLink.Controls.Add(this.textEditEmail);
            this.xtraTabPageLink.Controls.Add(this.labelControl10);
            this.xtraTabPageLink.Controls.Add(this.textEditWebsite);
            this.xtraTabPageLink.Name = "xtraTabPageLink";
            this.xtraTabPageLink.Size = new System.Drawing.Size(554, 188);
            this.xtraTabPageLink.Text = "Интернет хаяг";
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl11.Location = new System.Drawing.Point(101, 91);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(42, 13);
            this.labelControl11.TabIndex = 15;
            this.labelControl11.Text = "twitter : ";
            // 
            // textEditTwitter
            // 
            this.textEditTwitter.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditTwitter.EnterMoveNextControl = true;
            this.textEditTwitter.Location = new System.Drawing.Point(149, 88);
            this.textEditTwitter.Name = "textEditTwitter";
            this.textEditTwitter.Properties.MaxLength = 50;
            this.textEditTwitter.Size = new System.Drawing.Size(200, 20);
            this.textEditTwitter.TabIndex = 24;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(98, 39);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(45, 13);
            this.labelControl8.TabIndex = 13;
            this.labelControl8.Text = "И-мейл : ";
            // 
            // textEditFacebook
            // 
            this.textEditFacebook.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditFacebook.EnterMoveNextControl = true;
            this.textEditFacebook.Location = new System.Drawing.Point(149, 62);
            this.textEditFacebook.Name = "textEditFacebook";
            this.textEditFacebook.Properties.MaxLength = 50;
            this.textEditFacebook.Size = new System.Drawing.Size(200, 20);
            this.textEditFacebook.TabIndex = 23;
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl9.Location = new System.Drawing.Point(89, 65);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(54, 13);
            this.labelControl9.TabIndex = 11;
            this.labelControl9.Text = "facebook : ";
            // 
            // textEditEmail
            // 
            this.textEditEmail.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditEmail.EnterMoveNextControl = true;
            this.textEditEmail.Location = new System.Drawing.Point(149, 36);
            this.textEditEmail.Name = "textEditEmail";
            this.textEditEmail.Properties.MaxLength = 50;
            this.textEditEmail.Size = new System.Drawing.Size(200, 20);
            this.textEditEmail.TabIndex = 22;
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl10.Location = new System.Drawing.Point(115, 13);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(28, 13);
            this.labelControl10.TabIndex = 9;
            this.labelControl10.Text = "Веб : ";
            // 
            // textEditWebsite
            // 
            this.textEditWebsite.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditWebsite.EnterMoveNextControl = true;
            this.textEditWebsite.Location = new System.Drawing.Point(149, 10);
            this.textEditWebsite.Name = "textEditWebsite";
            this.textEditWebsite.Properties.MaxLength = 50;
            this.textEditWebsite.Size = new System.Drawing.Size(200, 20);
            this.textEditWebsite.TabIndex = 21;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.textEditAccountant);
            this.xtraTabPage1.Controls.Add(this.labelAccountant);
            this.xtraTabPage1.Controls.Add(this.textEditAccount);
            this.xtraTabPage1.Controls.Add(this.textEditBank);
            this.xtraTabPage1.Controls.Add(this.textEditRegister);
            this.xtraTabPage1.Controls.Add(this.label3);
            this.xtraTabPage1.Controls.Add(this.label2);
            this.xtraTabPage1.Controls.Add(this.label1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(554, 188);
            this.xtraTabPage1.Text = "Нэмэлт";
            // 
            // textEditAccount
            // 
            this.textEditAccount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditAccount.EnterMoveNextControl = true;
            this.textEditAccount.Location = new System.Drawing.Point(213, 91);
            this.textEditAccount.Name = "textEditAccount";
            this.textEditAccount.Properties.MaxLength = 50;
            this.textEditAccount.Size = new System.Drawing.Size(200, 20);
            this.textEditAccount.TabIndex = 65;
            // 
            // textEditBank
            // 
            this.textEditBank.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditBank.EnterMoveNextControl = true;
            this.textEditBank.Location = new System.Drawing.Point(213, 60);
            this.textEditBank.Name = "textEditBank";
            this.textEditBank.Properties.MaxLength = 50;
            this.textEditBank.Size = new System.Drawing.Size(200, 20);
            this.textEditBank.TabIndex = 64;
            // 
            // textEditRegister
            // 
            this.textEditRegister.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditRegister.EnterMoveNextControl = true;
            this.textEditRegister.Location = new System.Drawing.Point(213, 27);
            this.textEditRegister.Name = "textEditRegister";
            this.textEditRegister.Properties.MaxLength = 50;
            this.textEditRegister.Size = new System.Drawing.Size(200, 20);
            this.textEditRegister.TabIndex = 63;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(122, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Дансны дугаар";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(142, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Банкны нэр";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(122, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Регистрийн №";
            // 
            // gridLookUpEditDirector
            // 
            this.gridLookUpEditDirector.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditDirector.EditValue = "";
            this.gridLookUpEditDirector.Location = new System.Drawing.Point(266, 56);
            this.gridLookUpEditDirector.Name = "gridLookUpEditDirector";
            this.gridLookUpEditDirector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDirector.Properties.DisplayMember = "fullName";
            this.gridLookUpEditDirector.Properties.NullText = "";
            this.gridLookUpEditDirector.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditDirector.Properties.ValueMember = "id";
            this.gridLookUpEditDirector.Properties.View = this.gridView2;
            this.gridLookUpEditDirector.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditDirector.TabIndex = 2;
            this.gridLookUpEditDirector.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditDirector_KeyUp);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Овог";
            this.gridColumn8.CustomizationCaption = "Овог";
            this.gridColumn8.FieldName = "lastName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 1;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Нэр";
            this.gridColumn9.CustomizationCaption = "Нэр";
            this.gridColumn9.FieldName = "firstName";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Регистер";
            this.gridColumn10.CustomizationCaption = "Регистер";
            this.gridColumn10.FieldName = "register";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 3;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Эрх";
            this.gridColumn11.CustomizationCaption = "Эрх";
            this.gridColumn11.FieldName = "roleName";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 4;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Тасаг";
            this.gridColumn12.CustomizationCaption = "Тасаг";
            this.gridColumn12.FieldName = "wardName";
            this.gridColumn12.MinWidth = 75;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 5;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Ажлын газар";
            this.gridColumn13.CustomizationCaption = "Ажлын газар";
            this.gridColumn13.FieldName = "organizationName";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 6;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Албан тушаал";
            this.gridColumn14.CustomizationCaption = "Албан тушаал";
            this.gridColumn14.FieldName = "positionName";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 7;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Дугаар";
            this.gridColumn15.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn15.FieldName = "id";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 426);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 53;
            this.simpleButtonReload.ToolTip = "Сонгох талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // gridLookUpEditSubDirector
            // 
            this.gridLookUpEditSubDirector.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditSubDirector.EditValue = "";
            this.gridLookUpEditSubDirector.Location = new System.Drawing.Point(266, 82);
            this.gridLookUpEditSubDirector.Name = "gridLookUpEditSubDirector";
            this.gridLookUpEditSubDirector.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSubDirector.Properties.DisplayMember = "fullName";
            this.gridLookUpEditSubDirector.Properties.NullText = "";
            this.gridLookUpEditSubDirector.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditSubDirector.Properties.ValueMember = "id";
            this.gridLookUpEditSubDirector.Properties.View = this.gridView1;
            this.gridLookUpEditSubDirector.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditSubDirector.TabIndex = 3;
            this.gridLookUpEditSubDirector.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditSubDirector_KeyUp);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn16});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Нэр";
            this.gridColumn2.CustomizationCaption = "Нэр";
            this.gridColumn2.FieldName = "firstName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Регистер";
            this.gridColumn3.CustomizationCaption = "Регистер";
            this.gridColumn3.FieldName = "register";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Эрх";
            this.gridColumn4.CustomizationCaption = "Эрх";
            this.gridColumn4.FieldName = "roleName";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Тасаг";
            this.gridColumn5.CustomizationCaption = "Тасаг";
            this.gridColumn5.FieldName = "wardName";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Ажлын газар";
            this.gridColumn6.CustomizationCaption = "Ажлын газар";
            this.gridColumn6.FieldName = "organizationName";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 6;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Албан тушаал";
            this.gridColumn7.CustomizationCaption = "Албан тушаал";
            this.gridColumn7.FieldName = "positionName";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Дугаар";
            this.gridColumn16.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn16.FieldName = "id";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 0;
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl12.Location = new System.Drawing.Point(186, 85);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(74, 13);
            this.labelControl12.TabIndex = 59;
            this.labelControl12.Text = "Дэд захирал : ";
            // 
            // pictureEdit
            // 
            this.pictureEdit.EnterMoveNextControl = true;
            this.pictureEdit.Location = new System.Drawing.Point(12, 12);
            this.pictureEdit.Name = "pictureEdit";
            this.pictureEdit.Properties.NullText = "Зураггүй";
            this.pictureEdit.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit.Size = new System.Drawing.Size(90, 90);
            this.pictureEdit.TabIndex = 60;
            this.pictureEdit.EditValueChanged += new System.EventHandler(this.pictureEdit_EditValueChanged);
            // 
            // gridLookUpEditTreatment
            // 
            this.gridLookUpEditTreatment.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditTreatment.EditValue = "";
            this.gridLookUpEditTreatment.Location = new System.Drawing.Point(266, 108);
            this.gridLookUpEditTreatment.Name = "gridLookUpEditTreatment";
            this.gridLookUpEditTreatment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditTreatment.Properties.DisplayMember = "fullName";
            this.gridLookUpEditTreatment.Properties.NullText = "";
            this.gridLookUpEditTreatment.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditTreatment.Properties.ValueMember = "id";
            this.gridLookUpEditTreatment.Properties.View = this.gridView3;
            this.gridLookUpEditTreatment.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditTreatment.TabIndex = 4;
            this.gridLookUpEditTreatment.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditTreatment_KeyUp);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Овог";
            this.gridColumn17.CustomizationCaption = "Овог";
            this.gridColumn17.FieldName = "lastName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 1;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Нэр";
            this.gridColumn18.CustomizationCaption = "Нэр";
            this.gridColumn18.FieldName = "firstName";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 2;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Регистер";
            this.gridColumn19.CustomizationCaption = "Регистер";
            this.gridColumn19.FieldName = "register";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 3;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Эрх";
            this.gridColumn20.CustomizationCaption = "Эрх";
            this.gridColumn20.FieldName = "roleName";
            this.gridColumn20.MinWidth = 75;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 4;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Тасаг";
            this.gridColumn21.CustomizationCaption = "Тасаг";
            this.gridColumn21.FieldName = "wardName";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 5;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Ажлын газар";
            this.gridColumn22.CustomizationCaption = "Ажлын газар";
            this.gridColumn22.FieldName = "organizationName";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 6;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Албан тушаал";
            this.gridColumn23.CustomizationCaption = "Албан тушаал";
            this.gridColumn23.FieldName = "positionName";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 7;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Дугаар";
            this.gridColumn24.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn24.FieldName = "id";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 0;
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl13.Location = new System.Drawing.Point(113, 111);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(147, 13);
            this.labelControl13.TabIndex = 62;
            this.labelControl13.Text = "Эмчилгээ эрхэлсэн захирал : ";
            // 
            // labelAccountant
            // 
            this.labelAccountant.AutoSize = true;
            this.labelAccountant.Location = new System.Drawing.Point(115, 125);
            this.labelAccountant.Name = "labelAccountant";
            this.labelAccountant.Size = new System.Drawing.Size(88, 13);
            this.labelAccountant.TabIndex = 66;
            this.labelAccountant.Text = "Нягтлан бодогч";
            // 
            // textEditAccountant
            // 
            this.textEditAccountant.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditAccountant.EnterMoveNextControl = true;
            this.textEditAccountant.Location = new System.Drawing.Point(213, 122);
            this.textEditAccountant.Name = "textEditAccountant";
            this.textEditAccountant.Properties.MaxLength = 50;
            this.textEditAccountant.Size = new System.Drawing.Size(200, 20);
            this.textEditAccountant.TabIndex = 67;
            // 
            // HospitalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(584, 461);
            this.Controls.Add(this.gridLookUpEditTreatment);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.pictureEdit);
            this.Controls.Add(this.gridLookUpEditSubDirector);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.gridLookUpEditDirector);
            this.Controls.Add(this.xtraTabControl);
            this.Controls.Add(this.simpleButtonSave);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.memoEditDescription);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.textEditName);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HospitalForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Эмнэлэгийн бүртгэл";
            this.Shown += new System.EventHandler(this.HospitalForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageAddress.ResumeLayout(false);
            this.xtraTabPageAddress.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPostBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhone.Properties)).EndInit();
            this.xtraTabPageLink.ResumeLayout(false);
            this.xtraTabPageLink.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTwitter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFacebook.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWebsite.Properties)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegister.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDirector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSubDirector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTreatment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAccountant.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAddress;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLink;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.MemoEdit memoEditAddress;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit textEditFax;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit textEditPostBox;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit textEditPhone;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit textEditTwitter;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit textEditFacebook;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit textEditEmail;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit textEditWebsite;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDirector;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSubDirector;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PictureEdit pictureEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditTreatment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEditAccount;
        private DevExpress.XtraEditors.TextEdit textEditBank;
        private DevExpress.XtraEditors.TextEdit textEditRegister;
        private DevExpress.XtraEditors.TextEdit textEditAccountant;
        private System.Windows.Forms.Label labelAccountant;

    }
}