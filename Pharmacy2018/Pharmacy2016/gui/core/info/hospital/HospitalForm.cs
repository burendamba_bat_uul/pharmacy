﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.main;

namespace Pharmacy2016.gui.core.info.hospital
{
    /*
     * Эмнэлэгийн бүртгэлийг засах цонх.
     * 
     */

    public partial class HospitalForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static HospitalForm INSTANCE = new HospitalForm();

            public static HospitalForm Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private readonly int FORM_ID = 3001;

            private HospitalForm()
            {
            
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                RoleUtil.addForm(FORM_ID, this);

                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditDirector.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                pictureEdit.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;

                gridLookUpEditDirector.Properties.DataSource = InformationDataSet.SystemUserOtherBindingSource;
                gridLookUpEditSubDirector.Properties.DataSource = InformationDataSet.SystemUserOtherBindingSource;
                gridLookUpEditTreatment.Properties.DataSource = InformationDataSet.SystemUserBindingSource;

                initBinding();
                reload();
            }

            private void initBinding()
            {
                textEditName.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "name", true));
                gridLookUpEditDirector.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "directorID", true));
                gridLookUpEditSubDirector.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "subDirectorID", true));
                gridLookUpEditTreatment.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "treatmentDirectorID", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "description", true));
                textEditPhone.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "phone", true));
                textEditPostBox.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "postBox", true));
                textEditFax.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "fax", true));
                memoEditAddress.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "address", true));
                textEditWebsite.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "website", true));
                textEditEmail.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "email", true));
                textEditFacebook.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "facebook", true));
                textEditTwitter.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "twitter", true));
                pictureEdit.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "picture", true));
                textEditRegister.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "register_id", true));
                textEditBank.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "bank_name", true));
                textEditAccount.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "bank_account", true));
                textEditAccountant.DataBindings.Add(new Binding("EditValue", InformationDataSet.HospitalBindingSource, "accountant_name", true));
            }

            private void HospitalForm_Shown(object sender, EventArgs e)
            {
                textEditName.Focus();
                xtraTabControl.SelectedTabPage = xtraTabPageAddress;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }
                    ProgramUtil.IsShowPopup = true;
                    ProgramUtil.IsSaveEvent = false;
                    checkRole();
                    InformationDataSet.HospitalTableAdapter.Fill(InformationDataSet.Instance.Hospital, HospitalUtil.getHospitalId());
                    ShowDialog(MainForm.Instance);
                    
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                simpleButtonSave.Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
            }

        #endregion

        #region Форм event

            private void gridLookUpEditDirector_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditDirector.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditSubDirector.Focus();
                    }
                }
            }

            private void gridLookUpEditSubDirector_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditSubDirector.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditTreatment.Focus();
                    }
                }
            }

            private void gridLookUpEditTreatment_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditTreatment.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        memoEditDescription.Focus();
                    }
                }
            }

            private void pictureEdit_EditValueChanged(object sender, EventArgs e)
            {
                if (pictureEdit.EditValue == DBNull.Value || pictureEdit.EditValue == null)
                {
                    return;
                }

                Image tmpImage = pictureEdit.Image;

                //if (tmpImage.Width >= 300 && tmpImage.Height >= 400)
                //{
                //    if (tmpImage.Width / 3 > tmpImage.Height / 4)
                //    {
                //        tmpImage = ProgramUtil.resizeImage(tmpImage, new Size(10 * Convert.ToInt32(Math.Round(40d * tmpImage.Width / tmpImage.Height)), 400));
                //    }
                //    else
                //    {
                //        tmpImage = ProgramUtil.resizeImage(tmpImage, new Size(300, 10 * Convert.ToInt32(Math.Round(30d * tmpImage.Height / tmpImage.Width))));
                //    }

                //    tmpImage = ProgramUtil.cropImage(tmpImage, new Rectangle(new Point(tmpImage.Width / 2 - 150, tmpImage.Height / 2 - 200), new Size(300, 400)));
                //}

                DataRowView view = (DataRowView)InformationDataSet.HospitalBindingSource.Current;
                view["picture"] = ProgramUtil.imageToByteArray(tmpImage, System.Drawing.Imaging.ImageFormat.Jpeg);
                view.EndEdit();
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.SystemUserOtherTableAdapter.Fill(InformationDataSet.Instance.SystemUserOther, HospitalUtil.getHospitalId(), SystemUtil.DIRECTOR);
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.TREATMENT_DIRECTOR);
                ProgramUtil.closeWaitDialog();
            }

            private bool check()
            {
                Instance.Validate();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (textEditName.EditValue.ToString().Trim().Equals(""))
                {
                    errorText = "Эмнэлэгийн нэрийг оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    isRight = false;
                    textEditName.Focus();
                }
                if (gridLookUpEditDirector.EditValue == DBNull.Value || gridLookUpEditDirector.EditValue == null || gridLookUpEditDirector.EditValue.ToString().Equals(""))
                {
                    errorText = "Захиралыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditDirector.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditDirector.Focus();
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    DataRowView view = (DataRowView)InformationDataSet.HospitalBindingSource.Current;
                    view["actionUserID"] = UserUtil.getUserId();
                    InformationDataSet.HospitalBindingSource.EndEdit();
                    InformationDataSet.HospitalTableAdapter.Update(InformationDataSet.Instance.Hospital);

                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }
            
        #endregion       
      

    }
}