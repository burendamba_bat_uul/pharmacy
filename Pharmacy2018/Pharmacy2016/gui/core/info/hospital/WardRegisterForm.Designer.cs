﻿namespace Pharmacy2016.gui.core.info.hospital
{
    partial class WardRegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WardRegisterForm));
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.treeListWard = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnWard = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnUse = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEditIsUse = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.controlNavigatorWard = new DevExpress.XtraEditors.ControlNavigator();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemWard = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRoomBed = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEndBal = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.simpleButtonEdit = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAddBed = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAddRoom = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.treeListRoom = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnRoom = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnIsUse = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageRoomBed = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageUser = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlUserWard = new DevExpress.XtraGrid.GridControl();
            this.gridViewUserWard = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnPharmacist = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnWard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTreeListLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditIsUse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageRoomBed.SuspendLayout();
            this.xtraTabPageUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUserWard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUserWard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTreeListLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("70012a3e-769a-46ee-bc87-32b00302aeaf");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(300, 200);
            this.dockPanel1.Size = new System.Drawing.Size(300, 461);
            this.dockPanel1.Text = "Тасаг";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.treeListWard);
            this.dockPanel1_Container.Controls.Add(this.panelControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(292, 434);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // treeListWard
            // 
            this.treeListWard.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnWard,
            this.treeListColumnUse});
            this.treeListWard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListWard.KeyFieldName = "id";
            this.treeListWard.Location = new System.Drawing.Point(0, 0);
            this.treeListWard.Name = "treeListWard";
            this.treeListWard.OptionsBehavior.Editable = false;
            this.treeListWard.OptionsBehavior.PopulateServiceColumns = true;
            this.treeListWard.ParentFieldName = "pid";
            this.treeListWard.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEditIsUse});
            this.treeListWard.Size = new System.Drawing.Size(292, 409);
            this.treeListWard.TabIndex = 8;
            this.treeListWard.BeforeFocusNode += new DevExpress.XtraTreeList.BeforeFocusNodeEventHandler(this.treeListWard_BeforeFocusNode);
            this.treeListWard.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeListWard_FocusedNodeChanged);
            // 
            // treeListColumnWard
            // 
            this.treeListColumnWard.Caption = "Тасаг";
            this.treeListColumnWard.FieldName = "name";
            this.treeListColumnWard.MinWidth = 52;
            this.treeListColumnWard.Name = "treeListColumnWard";
            this.treeListColumnWard.OptionsColumn.AllowMove = false;
            this.treeListColumnWard.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumnWard.Visible = true;
            this.treeListColumnWard.VisibleIndex = 0;
            this.treeListColumnWard.Width = 409;
            // 
            // treeListColumnUse
            // 
            this.treeListColumnUse.Caption = "Хэвтэн эмчлүүлэгчтэй";
            this.treeListColumnUse.ColumnEdit = this.repositoryItemCheckEditIsUse;
            this.treeListColumnUse.FieldName = "isUseMedice";
            this.treeListColumnUse.Name = "treeListColumnUse";
            this.treeListColumnUse.Visible = true;
            this.treeListColumnUse.VisibleIndex = 1;
            this.treeListColumnUse.Width = 259;
            // 
            // repositoryItemCheckEditIsUse
            // 
            this.repositoryItemCheckEditIsUse.AutoHeight = false;
            this.repositoryItemCheckEditIsUse.Name = "repositoryItemCheckEditIsUse";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.controlNavigatorWard);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 409);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(292, 25);
            this.panelControl2.TabIndex = 8;
            // 
            // controlNavigatorWard
            // 
            this.controlNavigatorWard.Buttons.Append.Visible = false;
            this.controlNavigatorWard.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorWard.Buttons.Edit.Visible = false;
            this.controlNavigatorWard.Buttons.EndEdit.Visible = false;
            this.controlNavigatorWard.Buttons.First.Visible = false;
            this.controlNavigatorWard.Buttons.Last.Visible = false;
            this.controlNavigatorWard.Buttons.Next.Visible = false;
            this.controlNavigatorWard.Buttons.NextPage.Visible = false;
            this.controlNavigatorWard.Buttons.Prev.Visible = false;
            this.controlNavigatorWard.Buttons.PrevPage.Visible = false;
            this.controlNavigatorWard.Buttons.Remove.Visible = false;
            this.controlNavigatorWard.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 17, true, true, "Задлах", "expand"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах/Хадгалах", "edit")});
            this.controlNavigatorWard.Dock = System.Windows.Forms.DockStyle.Left;
            this.controlNavigatorWard.Location = new System.Drawing.Point(2, 2);
            this.controlNavigatorWard.Name = "controlNavigatorWard";
            this.controlNavigatorWard.ShowToolTips = true;
            this.controlNavigatorWard.Size = new System.Drawing.Size(104, 21);
            this.controlNavigatorWard.TabIndex = 4;
            this.controlNavigatorWard.Text = "controlNavigator1";
            this.controlNavigatorWard.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.controlNavigatorWard_ButtonClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonLayout);
            this.panelControl1.Controls.Add(this.dropDownButtonPrint);
            this.panelControl1.Controls.Add(this.simpleButtonEdit);
            this.panelControl1.Controls.Add(this.simpleButtonAddBed);
            this.panelControl1.Controls.Add(this.simpleButtonAddRoom);
            this.panelControl1.Controls.Add(this.simpleButtonSave);
            this.panelControl1.Controls.Add(this.simpleButtonReload);
            this.panelControl1.Controls.Add(this.simpleButtonDelete);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(300, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(534, 45);
            this.panelControl1.TabIndex = 3;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(499, 12);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 11;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.DropDownControl = this.popupMenu;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(363, 12);
            this.dropDownButtonPrint.MenuManager = this.barManager;
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.barManager.SetPopupContextMenu(this.dropDownButtonPrint, this.popupMenu);
            this.dropDownButtonPrint.Size = new System.Drawing.Size(130, 23);
            this.dropDownButtonPrint.TabIndex = 10;
            this.dropDownButtonPrint.Text = "Хөрвүүлэх";
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemWard),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemRoomBed),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemEndBal),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // barButtonItemWard
            // 
            this.barButtonItemWard.Caption = "Тасагийн бүртгэл";
            this.barButtonItemWard.Description = "Тасагийн бүртгэл хөрвүүлэх";
            this.barButtonItemWard.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemWard.Glyph")));
            this.barButtonItemWard.Id = 0;
            this.barButtonItemWard.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemWard.LargeGlyph")));
            this.barButtonItemWard.Name = "barButtonItemWard";
            this.barButtonItemWard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemWard_ItemClick);
            // 
            // barButtonItemRoomBed
            // 
            this.barButtonItemRoomBed.Caption = "Өрөө, орны бүртгэл";
            this.barButtonItemRoomBed.Description = "Өрөө, орны бүртгэл хөрвүүлэх";
            this.barButtonItemRoomBed.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRoomBed.Glyph")));
            this.barButtonItemRoomBed.Id = 1;
            this.barButtonItemRoomBed.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRoomBed.LargeGlyph")));
            this.barButtonItemRoomBed.Name = "barButtonItemRoomBed";
            this.barButtonItemRoomBed.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRoomBed_ItemClick);
            // 
            // barButtonItemEndBal
            // 
            this.barButtonItemEndBal.Caption = "Эцсийн үлдэгдэл";
            this.barButtonItemEndBal.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.Glyph")));
            this.barButtonItemEndBal.Id = 2;
            this.barButtonItemEndBal.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.LargeGlyph")));
            this.barButtonItemEndBal.Name = "barButtonItemEndBal";
            this.barButtonItemEndBal.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemEndBal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEndBal_ItemClick);
            // 
            // barButtonItemConvert
            // 
            this.barButtonItemConvert.Caption = "Эмийн бүртгэл";
            this.barButtonItemConvert.Description = "Эмийн бүртгэл хөрвүүлэх";
            this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
            this.barButtonItemConvert.Id = 3;
            this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
            this.barButtonItemConvert.Name = "barButtonItemConvert";
            this.barButtonItemConvert.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemWard,
            this.barButtonItemRoomBed,
            this.barButtonItemEndBal,
            this.barButtonItemConvert});
            this.barManager.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(834, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 461);
            this.barDockControlBottom.Size = new System.Drawing.Size(834, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 461);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(834, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 461);
            // 
            // simpleButtonEdit
            // 
            this.simpleButtonEdit.Location = new System.Drawing.Point(213, 12);
            this.simpleButtonEdit.Name = "simpleButtonEdit";
            this.simpleButtonEdit.Size = new System.Drawing.Size(55, 23);
            this.simpleButtonEdit.TabIndex = 9;
            this.simpleButtonEdit.Text = "Засах";
            this.simpleButtonEdit.ToolTip = "Өгөгдлийг засах төлөвт оруулах";
            this.simpleButtonEdit.Click += new System.EventHandler(this.RoomButtonEdit_Click);
            // 
            // simpleButtonAddBed
            // 
            this.simpleButtonAddBed.Location = new System.Drawing.Point(78, 12);
            this.simpleButtonAddBed.Name = "simpleButtonAddBed";
            this.simpleButtonAddBed.Size = new System.Drawing.Size(66, 23);
            this.simpleButtonAddBed.TabIndex = 8;
            this.simpleButtonAddBed.Text = "Ор нэмэх";
            this.simpleButtonAddBed.ToolTip = "Ор нэмэх";
            this.simpleButtonAddBed.Click += new System.EventHandler(this.AddButtonBed_Click);
            // 
            // simpleButtonAddRoom
            // 
            this.simpleButtonAddRoom.Location = new System.Drawing.Point(6, 12);
            this.simpleButtonAddRoom.Name = "simpleButtonAddRoom";
            this.simpleButtonAddRoom.Size = new System.Drawing.Size(66, 23);
            this.simpleButtonAddRoom.TabIndex = 7;
            this.simpleButtonAddRoom.Text = "Өрөө нэмэх";
            this.simpleButtonAddRoom.ToolTip = "Өрөө нэмэх";
            this.simpleButtonAddRoom.Click += new System.EventHandler(this.AddButtonRoom_Click);
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Location = new System.Drawing.Point(274, 12);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(60, 23);
            this.simpleButtonSave.TabIndex = 6;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(282, 12);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonReload.TabIndex = 4;
            this.simpleButtonReload.Text = "Сэргээх";
            this.simpleButtonReload.ToolTip = "Тасаг, өрөө, орны мэдээллийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // simpleButtonDelete
            // 
            this.simpleButtonDelete.Location = new System.Drawing.Point(150, 12);
            this.simpleButtonDelete.Name = "simpleButtonDelete";
            this.simpleButtonDelete.Size = new System.Drawing.Size(57, 23);
            this.simpleButtonDelete.TabIndex = 2;
            this.simpleButtonDelete.Text = "Устгах";
            this.simpleButtonDelete.ToolTip = "Сонгосон өрөө, орыг устгах";
            this.simpleButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // treeListRoom
            // 
            this.treeListRoom.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnRoom,
            this.treeListColumnIsUse});
            this.treeListRoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListRoom.KeyFieldName = "id";
            this.treeListRoom.Location = new System.Drawing.Point(0, 0);
            this.treeListRoom.Name = "treeListRoom";
            this.treeListRoom.OptionsBehavior.Editable = false;
            this.treeListRoom.OptionsBehavior.EnableFiltering = true;
            this.treeListRoom.ParentFieldName = "pid";
            this.treeListRoom.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.treeListRoom.Size = new System.Drawing.Size(528, 388);
            this.treeListRoom.TabIndex = 13;
            this.treeListRoom.BeforeFocusNode += new DevExpress.XtraTreeList.BeforeFocusNodeEventHandler(this.treeListRoom_BeforeFocusNode);
            // 
            // treeListColumnRoom
            // 
            this.treeListColumnRoom.Caption = "Өрөө";
            this.treeListColumnRoom.CustomizationCaption = "Өрөө";
            this.treeListColumnRoom.FieldName = "name";
            this.treeListColumnRoom.MinWidth = 70;
            this.treeListColumnRoom.Name = "treeListColumnRoom";
            this.treeListColumnRoom.OptionsColumn.AllowMove = false;
            this.treeListColumnRoom.Visible = true;
            this.treeListColumnRoom.VisibleIndex = 0;
            this.treeListColumnRoom.Width = 482;
            // 
            // treeListColumnIsUse
            // 
            this.treeListColumnIsUse.Caption = "Ашиглах эсэх";
            this.treeListColumnIsUse.ColumnEdit = this.repositoryItemCheckEdit1;
            this.treeListColumnIsUse.CustomizationCaption = "Ашиглах эсэх";
            this.treeListColumnIsUse.FieldName = "isUse";
            this.treeListColumnIsUse.Name = "treeListColumnIsUse";
            this.treeListColumnIsUse.Visible = true;
            this.treeListColumnIsUse.VisibleIndex = 1;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(300, 45);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageRoomBed;
            this.xtraTabControl1.Size = new System.Drawing.Size(534, 416);
            this.xtraTabControl1.TabIndex = 19;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageRoomBed,
            this.xtraTabPageUser});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPageRoomBed
            // 
            this.xtraTabPageRoomBed.Controls.Add(this.treeListRoom);
            this.xtraTabPageRoomBed.Name = "xtraTabPageRoomBed";
            this.xtraTabPageRoomBed.Size = new System.Drawing.Size(528, 388);
            this.xtraTabPageRoomBed.Text = "Өрөө, ор";
            // 
            // xtraTabPageUser
            // 
            this.xtraTabPageUser.Controls.Add(this.gridControlUserWard);
            this.xtraTabPageUser.Name = "xtraTabPageUser";
            this.xtraTabPageUser.Size = new System.Drawing.Size(528, 388);
            this.xtraTabPageUser.Text = "Тасаг, ажилтан";
            // 
            // gridControlUserWard
            // 
            this.gridControlUserWard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlUserWard.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlUserWard.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlUserWard.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlUserWard.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlUserWard.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlUserWard.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, false, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, false, false, "", "save")});
            this.gridControlUserWard.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlPharmacist_EmbeddedNavigator_ButtonClick);
            this.gridControlUserWard.Location = new System.Drawing.Point(0, 0);
            this.gridControlUserWard.MainView = this.gridViewUserWard;
            this.gridControlUserWard.Name = "gridControlUserWard";
            this.gridControlUserWard.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemTreeListLookUpEdit1});
            this.gridControlUserWard.Size = new System.Drawing.Size(528, 388);
            this.gridControlUserWard.TabIndex = 3;
            this.gridControlUserWard.UseEmbeddedNavigator = true;
            this.gridControlUserWard.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewUserWard});
            // 
            // gridViewUserWard
            // 
            this.gridViewUserWard.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnPharmacist,
            this.gridColumnWard});
            this.gridViewUserWard.GridControl = this.gridControlUserWard;
            this.gridViewUserWard.Name = "gridViewUserWard";
            this.gridViewUserWard.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewUserWard.OptionsBehavior.ReadOnly = true;
            this.gridViewUserWard.OptionsCustomization.AllowGroup = false;
            this.gridViewUserWard.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewUserWard.OptionsFind.AllowFindPanel = false;
            this.gridViewUserWard.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewUserWard.OptionsView.ShowAutoFilterRow = true;
            this.gridViewUserWard.OptionsView.ShowGroupPanel = false;
            this.gridViewUserWard.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewUserWard_InvalidRowException);
            this.gridViewUserWard.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewUserWard_ValidateRow);
            this.gridViewUserWard.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewUserWard_RowUpdated);
            // 
            // gridColumnPharmacist
            // 
            this.gridColumnPharmacist.Caption = "Тасаг хариуцсан ажилтан";
            this.gridColumnPharmacist.ColumnEdit = this.repositoryItemGridLookUpEdit1;
            this.gridColumnPharmacist.CustomizationCaption = "Эм зүйч";
            this.gridColumnPharmacist.FieldName = "userID";
            this.gridColumnPharmacist.Name = "gridColumnPharmacist";
            this.gridColumnPharmacist.OptionsEditForm.Caption = "Ажилтан";
            this.gridColumnPharmacist.Visible = true;
            this.gridColumnPharmacist.VisibleIndex = 0;
            this.gridColumnPharmacist.Width = 352;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DisplayMember = "fullName";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.ValueMember = "id";
            this.repositoryItemGridLookUpEdit1.View = this.gridView1;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn5,
            this.gridColumn7,
            this.gridColumn8});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Нэр";
            this.gridColumn6.CustomizationCaption = "Нэр";
            this.gridColumn6.FieldName = "firstName";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Овог";
            this.gridColumn5.CustomizationCaption = "Овог";
            this.gridColumn5.FieldName = "lastName";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Тасаг";
            this.gridColumn7.CustomizationCaption = "Тасаг";
            this.gridColumn7.FieldName = "wardName";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Эрх";
            this.gridColumn8.CustomizationCaption = "Эрх";
            this.gridColumn8.FieldName = "roleName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            // 
            // gridColumnWard
            // 
            this.gridColumnWard.Caption = "Тасаг";
            this.gridColumnWard.ColumnEdit = this.repositoryItemTreeListLookUpEdit1;
            this.gridColumnWard.CustomizationCaption = "Тасаг";
            this.gridColumnWard.FieldName = "wardID";
            this.gridColumnWard.Name = "gridColumnWard";
            this.gridColumnWard.Visible = true;
            this.gridColumnWard.VisibleIndex = 1;
            this.gridColumnWard.Width = 344;
            // 
            // repositoryItemTreeListLookUpEdit1
            // 
            this.repositoryItemTreeListLookUpEdit1.AutoHeight = false;
            this.repositoryItemTreeListLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTreeListLookUpEdit1.DisplayMember = "name";
            this.repositoryItemTreeListLookUpEdit1.Name = "repositoryItemTreeListLookUpEdit1";
            this.repositoryItemTreeListLookUpEdit1.NullText = "";
            this.repositoryItemTreeListLookUpEdit1.TreeList = this.treeList1;
            this.repositoryItemTreeListLookUpEdit1.ValueMember = "id";
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(0, 0);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Нэр";
            this.treeListColumn1.CustomizationCaption = "Нэр";
            this.treeListColumn1.FieldName = "name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // WardRegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 461);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "WardRegisterForm";
            this.ShowInTaskbar = false;
            this.Text = "Тасаг, Өрөө";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WardRegisterForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditIsUse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageRoomBed.ResumeLayout(false);
            this.xtraTabPageUser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUserWard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUserWard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTreeListLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDelete;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraTreeList.TreeList treeListWard;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnWard;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.ControlNavigator controlNavigatorWard;
        private DevExpress.XtraTreeList.TreeList treeListRoom;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnRoom;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAddBed;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAddRoom;
        private DevExpress.XtraEditors.SimpleButton simpleButtonEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnIsUse;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemWard;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRoomBed;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEndBal;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnUse;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditIsUse;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageRoomBed;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageUser;
        private DevExpress.XtraGrid.GridControl gridControlUserWard;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewUserWard;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPharmacist;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnWard;
        private DevExpress.XtraEditors.Repository.RepositoryItemTreeListLookUpEdit repositoryItemTreeListLookUpEdit1;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
    }
}