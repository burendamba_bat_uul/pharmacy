﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Configuration;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.gui.core.info.conf
{
    /* 
     * Програм холбогдох өгөгдлийн санг тохируулах цонх.
     * Өгөгдлийн сангийн тохиргоог файлд хадгалж тухайн файлаасаа татаж уншина.
     */

    public partial class ConfDBForm : DevExpress.XtraEditors.XtraForm
    {

        #region Програм анх ачааллах функц

            private static ConfDBForm INSTANCE = new ConfDBForm();

            public static ConfDBForm Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private ConfDBForm()
            {
            
            }

            private void initForm()
            {
                if (!this.isInit)
                {
                    InitializeComponent();
                    this.isInit = true;
                    Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;
                    
                    textEditServerName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                    textEditPassword.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                    textEditUserName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                    textEditDBName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                }
            }

        #endregion

        #region Програм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    ShowDialog(MainForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                textEditServerName.Text = "";
                textEditUserName.Text = "";
                textEditPassword.Text = "";
                textEditDBName.Text = "";

                //if (SystemUtil.IsConnect)
                //{
                //    MainForm.Instance.showLoginMenu();
                //}
                //else
                //{
                //    MainForm.Instance.showDbConfMenu();
                //}
            }

            private void ConfDBForm_Shown(object sender, EventArgs e)
            {
                textEditServerName.Focus();
            }

        #endregion
            
        #region Формын event
        
            private void textEditPassword_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    changeDBConf();
                }
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                changeDBConf();
            }

            private void simpleButtonTest_Click(object sender, EventArgs e)
            {
                if (check())
                {
                    if (!testConnection(textEditServerName.Text, textEditDBName.Text, textEditUserName.Text, textEditPassword.Text))
                    {
                        XtraMessageBox.Show(INSTANCE, "Өгөгдлийн сантай холбогдож чадсангүй.");
                    }
                    else
                    {
                        XtraMessageBox.Show(INSTANCE, "Өгөгдлийн сантай холбогдлоо.");
                    }
                }
            }

        #endregion

        #region Формын функц

            // Өгөгдлийн сантай холбогдож  байгаа эсэхийг шалгах функц
            public static bool testConnection(string serverName, string dbName, string userName, string password)
            {
                bool isConnect = false;
                    // integratedSecurity ? string.Format("Provider={0};Data Source={1};Initial Catalog={2};Integrated Security=SSPI;", provider, serverName, initialCatalog)
                var connectionString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", serverName, dbName, userName, password);

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    try
                    {
                        //connection.ConnectionTimeout = 15;
                        connection.Open();
                        if (connection.State == ConnectionState.Open)
                        {
                            isConnect = true;
                        }
                    }
                    catch (SqlException) 
                    {
                        //XtraMessageBox.Show(Instance, "Өгөгдлийн сантай холбогдож чадсангүй.");
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                return isConnect;
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if ((textEditServerName.Text = textEditServerName.Text.Trim()).Equals(""))
                {
                    isRight = false;
                    textEditServerName.Focus();
                    errorText = "Серверийн нэрийг оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditServerName.ErrorText = errorText;
                }
                if ((textEditDBName.Text = textEditDBName.Text.Trim()).Equals(""))
                {
                    errorText = "Өгөгдлийн сангийн нэрийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditDBName.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditDBName.Focus();
                    }
                }
                if ((textEditUserName.Text = textEditUserName.Text.Trim()).Equals(""))
                {
                    errorText = "Серверийн нэрийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditUserName.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditUserName.Focus();
                    }
                }
                if ((textEditPassword.Text = textEditPassword.Text.Trim()).Equals(""))
                {
                    errorText = "Нууц үгийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditPassword.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditPassword.Focus();
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void changeDBConf()
            {
                if (check())
                {
                    string[] lines = { textEditServerName.Text, textEditDBName.Text, textEditUserName.Text, SystemUtil.encrypt(textEditPassword.Text) };
                    //!SystemUtil.IsConnect && 
                    if(!testConnection(textEditServerName.Text, textEditDBName.Text, textEditUserName.Text, textEditPassword.Text))
                    {
                        XtraMessageBox.Show(INSTANCE, "Өгөгдлийн сантай холбогдохгүй байна.", "Өгөгдлийн сантай холбогдох");
                        return;
                    }

                    File.WriteAllLines(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/" + SystemUtil.dbFile, lines);

                    //XtraMessageBox.Show(INSTANCE, "Өгөдлийн санг тохирууллаа. Програм хаагдсаны дараа дахин нэвтрэн орно уу.");
                    //Close();
                    //SystemUtil.IsConnect = false;
                    //ProgramUtil.EXIT(true);


                    XtraMessageBox.Show(INSTANCE, "Өгөдлийн санг тохирууллаа. Програмын цонхнууд хаагдах болохыг анхаарна уу.");
                    RoleUtil.closeForm();
                    Pharmacy2016.Properties.Settings.Default.conMedicine = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", lines[0], lines[1], lines[2], textEditPassword.Text);
                    Properties.Settings.Default.Save();
                    SystemUtil.IsConnect = true;

                    InformationDataSet.changeConnectionString();
                    UserDataSet.changeConnectionString();
                    JournalDataSet.changeConnectionString();
                    StoreDataSet.changeConnectionString();
                    ReportDataSet.changeConnectionString();
                    MainDBDataSet.changeConnectionString();
                    //ProgramUtil.loadHospital();
                    SystemUtil.IsConnect = false;
                    ProgramUtil.EXITFULLY();   
                    Close();
                }
            }

        #endregion

    }
}