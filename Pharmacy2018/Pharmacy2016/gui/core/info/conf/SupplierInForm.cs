﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.patient;
using Pharmacy2016.gui.core.journal.inc;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.gui.core.journal.tender;
using Pharmacy2016.gui.core.journal.order;
using Pharmacy2016.gui.core.info.medicine;

namespace Pharmacy2016.gui.core.info.conf
{
    /*
     * Бэлтгэн нийлүүлэгч нэмэх цонх.
     * 
     */

    public partial class SupplierInForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static SupplierInForm INSTANCE = new SupplierInForm();

            public static SupplierInForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int formType;

            private SupplierInForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int fType, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    formType = fType;
                    
                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                textEditName.ErrorText = "";
                textEditName.Text = "";
                textEditConNo.Text = "";
            }

            private void UserConfInUpForm_Shown(object sender, EventArgs e)
            {
                textEditName.Focus();
            }

        #endregion

        #region Формын event

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                textEditConNo.Text = textEditConNo.Text.Trim();
                if ((textEditName.Text = textEditName.Text.Trim()).Equals(""))
                {
                    errorText = "Нэрийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditName.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                int exist = Convert.ToInt32(UserDataSet.QueriesTableAdapter.exist_supplier_name(textEditName.EditValue.ToString(), Int32.Parse(textEditConNo.Text)));
                if (exist == 2)
                {
                    errorText = "Бэлтгэн нийлүүлэгчийн нэр бүртгэгдсэн байна";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    isRight = false;
                    textEditName.Focus();
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    string newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());

                    UserDataSet.Instance.Supplier.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    UserDataSet.Instance.Supplier.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                    UserDataSet.Instance.Supplier.idColumn.DefaultValue = newID;

                    DataRowView view = (DataRowView)UserDataSet.SupplierBindingSource.AddNew();
                    view["name"] = textEditName.Text;
                    view["conNo"] = textEditConNo.Text;
                    view.EndEdit();
                    UserDataSet.SupplierTableAdapter.Update(UserDataSet.Instance.Supplier);

                    if (formType == 1)
                    {
                        MedicineIncUpForm.Instance.addProducer(newID);               
                    }
                    else if (formType == 2)
                    {
                        PackageInUpForm.Instance.addProducer(newID);
                    }
                    else if (formType == 4)
                    {
                        TenderUpForm.Instance.addProducer(newID);
                    }
                    else if (formType == 5)
                    {
                        MedicineOrderUpForm.Instance.addProducer(newID);
                    }

                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }

        #endregion         

    }
}