﻿namespace Pharmacy2016.gui.core.info.conf
{
    partial class ProgramLogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProgramLogForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControlAll = new DevExpress.XtraEditors.LabelControl();
            this.checkEditAll = new DevExpress.XtraEditors.CheckEdit();
            this.gridLookUpEditUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButtonExport = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageSystemLog = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlSystemLog = new DevExpress.XtraGrid.GridControl();
            this.gridViewSystemLog = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageProgramLog = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlProgramLog = new DevExpress.XtraGrid.GridControl();
            this.gridViewProgramLog = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageSystemLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSystemLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSystemLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            this.xtraTabPageProgramLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProgramLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProgramLog)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControlAll);
            this.panelControl1.Controls.Add(this.checkEditAll);
            this.panelControl1.Controls.Add(this.gridLookUpEditUser);
            this.panelControl1.Controls.Add(this.simpleButtonExport);
            this.panelControl1.Controls.Add(this.dateEditEnd);
            this.panelControl1.Controls.Add(this.dateEditStart);
            this.panelControl1.Controls.Add(this.simpleButtonReload);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 50);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControlAll
            // 
            this.labelControlAll.Location = new System.Drawing.Point(284, 15);
            this.labelControlAll.Name = "labelControlAll";
            this.labelControlAll.Size = new System.Drawing.Size(30, 13);
            this.labelControlAll.TabIndex = 11;
            this.labelControlAll.Text = "Бүгд :";
            // 
            // checkEditAll
            // 
            this.checkEditAll.Location = new System.Drawing.Point(320, 12);
            this.checkEditAll.Name = "checkEditAll";
            this.checkEditAll.Properties.Caption = "";
            this.checkEditAll.Size = new System.Drawing.Size(19, 19);
            this.checkEditAll.TabIndex = 9;
            this.checkEditAll.Click += new System.EventHandler(this.checkEditAll_CheckedChanged);
            // 
            // gridLookUpEditUser
            // 
            this.gridLookUpEditUser.EditValue = "";
            this.gridLookUpEditUser.Location = new System.Drawing.Point(345, 11);
            this.gridLookUpEditUser.Name = "gridLookUpEditUser";
            this.gridLookUpEditUser.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("gridLookUpEditUser.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "reload", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditUser.Properties.NullText = "Хэрэглэгч сонгох";
            this.gridLookUpEditUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditUser.Properties.ValueMember = "id";
            this.gridLookUpEditUser.Properties.View = this.gridView1;
            this.gridLookUpEditUser.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditUser_Properties_ButtonClick);
            this.gridLookUpEditUser.Size = new System.Drawing.Size(150, 22);
            this.gridLookUpEditUser.TabIndex = 10;
            this.gridLookUpEditUser.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.gridLookUpEditUser_CustomDisplayText);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Нэр";
            this.gridColumn2.CustomizationCaption = "Нэр";
            this.gridColumn2.FieldName = "firstName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Дугаар";
            this.gridColumn4.CustomizationCaption = "Дугаар";
            this.gridColumn4.FieldName = "id";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Эрх";
            this.gridColumn5.CustomizationCaption = "Эрх";
            this.gridColumn5.FieldName = "roleName";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тасаг";
            this.gridColumn6.CustomizationCaption = "Тасаг";
            this.gridColumn6.FieldName = "wardName";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Ажлын газар";
            this.gridColumn7.CustomizationCaption = "Ажлын газар";
            this.gridColumn7.FieldName = "organizationName";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Албан тушаал";
            this.gridColumn8.CustomizationCaption = "Албан тушаал";
            this.gridColumn8.FieldName = "positionName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            // 
            // simpleButtonExport
            // 
            this.simpleButtonExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonExport.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonExport.Image")));
            this.simpleButtonExport.Location = new System.Drawing.Point(682, 9);
            this.simpleButtonExport.Name = "simpleButtonExport";
            this.simpleButtonExport.Size = new System.Drawing.Size(90, 25);
            this.simpleButtonExport.TabIndex = 8;
            this.simpleButtonExport.Text = "Хөрвүүлэх";
            this.simpleButtonExport.ToolTip = "Өгөгдлийг экспортлох";
            this.simpleButtonExport.Click += new System.EventHandler(this.simpleButtonExport_Click);
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(148, 12);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.EditFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.dateEditEnd.Size = new System.Drawing.Size(130, 20);
            this.dateEditEnd.TabIndex = 6;
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(12, 12);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.dateEditStart.Size = new System.Drawing.Size(130, 20);
            this.dateEditStart.TabIndex = 5;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(501, 9);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 25);
            this.simpleButtonReload.TabIndex = 7;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Хэрэглэгчийн хандалтын мэдээллийг шүүх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl.Location = new System.Drawing.Point(0, 50);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageSystemLog;
            this.xtraTabControl.Size = new System.Drawing.Size(784, 411);
            this.xtraTabControl.TabIndex = 1;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageSystemLog,
            this.xtraTabPageProgramLog});
            // 
            // xtraTabPageSystemLog
            // 
            this.xtraTabPageSystemLog.Controls.Add(this.gridControlSystemLog);
            this.xtraTabPageSystemLog.Name = "xtraTabPageSystemLog";
            this.xtraTabPageSystemLog.Size = new System.Drawing.Size(778, 383);
            this.xtraTabPageSystemLog.Text = "Хэрэглэгчийн хандалт";
            // 
            // gridControlSystemLog
            // 
            this.gridControlSystemLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSystemLog.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlSystemLog.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlSystemLog.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlSystemLog.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlSystemLog.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlSystemLog.Location = new System.Drawing.Point(0, 0);
            this.gridControlSystemLog.MainView = this.gridViewSystemLog;
            this.gridControlSystemLog.Name = "gridControlSystemLog";
            this.gridControlSystemLog.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridControlSystemLog.Size = new System.Drawing.Size(778, 383);
            this.gridControlSystemLog.TabIndex = 0;
            this.gridControlSystemLog.UseEmbeddedNavigator = true;
            this.gridControlSystemLog.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSystemLog});
            // 
            // gridViewSystemLog
            // 
            this.gridViewSystemLog.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn3,
            this.gridColumn9,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn27,
            this.gridColumn14,
            this.gridColumn15});
            this.gridViewSystemLog.GridControl = this.gridControlSystemLog;
            this.gridViewSystemLog.Name = "gridViewSystemLog";
            this.gridViewSystemLog.OptionsBehavior.ReadOnly = true;
            this.gridViewSystemLog.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Дугаар";
            this.gridColumn10.CustomizationCaption = "Дугаар";
            this.gridColumn10.FieldName = "userID";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 107;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Овог";
            this.gridColumn3.CustomizationCaption = "Овог";
            this.gridColumn3.FieldName = "lastName";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 150;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Нэр";
            this.gridColumn9.CustomizationCaption = "Нэр";
            this.gridColumn9.FieldName = "firstName";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            this.gridColumn9.Width = 152;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Эрх";
            this.gridColumn11.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.gridColumn11.FieldName = "roleName";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            this.gridColumn11.Width = 116;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Тасаг";
            this.gridColumn12.CustomizationCaption = "Тасгийн нэр";
            this.gridColumn12.FieldName = "wardName";
            this.gridColumn12.MinWidth = 75;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            this.gridColumn12.Width = 123;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Огноо";
            this.gridColumn13.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn13.CustomizationCaption = "Огноо";
            this.gridColumn13.FieldName = "date";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            this.gridColumn13.Width = 94;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Үйлдэл";
            this.gridColumn27.CustomizationCaption = "Үйлдэл";
            this.gridColumn27.FieldName = "actionText";
            this.gridColumn27.MinWidth = 75;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 6;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Үйлдэл";
            this.gridColumn14.CustomizationCaption = "Үйлдэл";
            this.gridColumn14.FieldName = "action";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Width = 107;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Дэлгэрэнгүй";
            this.gridColumn15.CustomizationCaption = "Дэлгэрэнгүй";
            this.gridColumn15.FieldName = "description";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 7;
            this.gridColumn15.Width = 303;
            // 
            // xtraTabPageProgramLog
            // 
            this.xtraTabPageProgramLog.Controls.Add(this.gridControlProgramLog);
            this.xtraTabPageProgramLog.Name = "xtraTabPageProgramLog";
            this.xtraTabPageProgramLog.Size = new System.Drawing.Size(778, 383);
            this.xtraTabPageProgramLog.Text = "Програмын хандалт";
            // 
            // gridControlProgramLog
            // 
            this.gridControlProgramLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlProgramLog.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlProgramLog.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlProgramLog.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlProgramLog.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlProgramLog.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlProgramLog.Location = new System.Drawing.Point(0, 0);
            this.gridControlProgramLog.MainView = this.gridViewProgramLog;
            this.gridControlProgramLog.Name = "gridControlProgramLog";
            this.gridControlProgramLog.Size = new System.Drawing.Size(778, 383);
            this.gridControlProgramLog.TabIndex = 1;
            this.gridControlProgramLog.UseEmbeddedNavigator = true;
            this.gridControlProgramLog.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProgramLog});
            // 
            // gridViewProgramLog
            // 
            this.gridViewProgramLog.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn25,
            this.gridColumn24,
            this.gridColumn21,
            this.gridColumn26,
            this.gridColumn22,
            this.gridColumn23});
            this.gridViewProgramLog.GridControl = this.gridControlProgramLog;
            this.gridViewProgramLog.Name = "gridViewProgramLog";
            this.gridViewProgramLog.OptionsBehavior.ReadOnly = true;
            this.gridViewProgramLog.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Дугаар";
            this.gridColumn16.CustomizationCaption = "Дугаар";
            this.gridColumn16.FieldName = "actionUserID";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 0;
            this.gridColumn16.Width = 107;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Овог";
            this.gridColumn17.CustomizationCaption = "Овог";
            this.gridColumn17.FieldName = "lastName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 2;
            this.gridColumn17.Width = 150;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Нэр";
            this.gridColumn18.CustomizationCaption = "Нэр";
            this.gridColumn18.FieldName = "firstName";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 1;
            this.gridColumn18.Width = 152;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Эрх";
            this.gridColumn19.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.gridColumn19.FieldName = "roleName";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 3;
            this.gridColumn19.Width = 116;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Тасаг";
            this.gridColumn20.CustomizationCaption = "Тасгийн нэр";
            this.gridColumn20.FieldName = "wardName";
            this.gridColumn20.MinWidth = 75;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 4;
            this.gridColumn20.Width = 123;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Цонх";
            this.gridColumn25.FieldName = "tableNameText";
            this.gridColumn25.MinWidth = 100;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            this.gridColumn25.Width = 100;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Хүснэгт";
            this.gridColumn24.CustomizationCaption = "Хүснэгт";
            this.gridColumn24.FieldName = "tableName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Огноо";
            this.gridColumn21.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn21.CustomizationCaption = "Огноо";
            this.gridColumn21.FieldName = "date";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 6;
            this.gridColumn21.Width = 94;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Үйлдэл";
            this.gridColumn26.CustomizationCaption = "Үйлдэл";
            this.gridColumn26.FieldName = "actionText";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 7;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Үйлдэл";
            this.gridColumn22.CustomizationCaption = "Үйлдэл";
            this.gridColumn22.FieldName = "action";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Width = 107;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Дэлгэрэнгүй";
            this.gridColumn23.CustomizationCaption = "Дэлгэрэнгүй";
            this.gridColumn23.FieldName = "description";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 8;
            this.gridColumn23.Width = 303;
            // 
            // ProgramLogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.xtraTabControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "ProgramLogForm";
            this.ShowInTaskbar = false;
            this.Text = "Програмын хандалт";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProgramLogForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageSystemLog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSystemLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSystemLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            this.xtraTabPageProgramLog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProgramLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProgramLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageSystemLog;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageProgramLog;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.SimpleButton simpleButtonExport;
        private DevExpress.XtraGrid.GridControl gridControlSystemLog;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSystemLog;
        private DevExpress.XtraEditors.LabelControl labelControlAll;
        private DevExpress.XtraEditors.CheckEdit checkEditAll;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.GridControl gridControlProgramLog;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProgramLog;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
    }
}