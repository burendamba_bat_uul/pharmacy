﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.util;

namespace Pharmacy2016.gui.core.info.conf
{
    /*
     * Програм руу нэвтэрсэн хэрэглэгч, хүснэгтэнд өөрчлөлт оруулсан мэдээллийг харуулах цонх.
     * 
     */

    public partial class ProgramLogForm : DevExpress.XtraEditors.XtraForm
    {
        
        #region Форм анх ачааллах функц

            private static ProgramLogForm INSTANCE = new ProgramLogForm();

            public static ProgramLogForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private readonly int FORM_ID = 5001;

            private ProgramLogForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;
                RoleUtil.addForm(FORM_ID, this);

                gridControlProgramLog.DataSource = InformationDataSet.ProgramLogBindingSource;
                gridControlSystemLog.DataSource = InformationDataSet.SystemLogBindingSource;
                gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserAnotherBindingSource;

                dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditEnd.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;

                dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                DateTime date = DateTime.Now;
                dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1, 00, 00, 01);
                dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month), 23, 59, 59);
                reloadUser();
            }         

        #endregion

        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        bool[] roles = UserUtil.getWindowRole(FORM_ID);
                        getRoleAndShow();
                        if (UserUtil.getUserRoleId() != RoleUtil.ADMIN_ID)
                        {
                            reload();
                        }

                        Show();
                    }
                }
                catch(Exception ex)
                {
                    XtraMessageBox.Show("Цонх ачааллахад алдаа гарлаа "+ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void ProgramLogForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                InformationDataSet.Instance.SystemLog.Clear();
                InformationDataSet.Instance.ProgramLog.Clear();
            }

        #endregion
        
        #region Формын event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkEditAll_CheckedChanged(object sender, EventArgs e)
            {
                editUser();
            }

            private void gridLookUpEditUser_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
            {
                e.DisplayText = "Хэрэглэгч сонгох";
            }

            private void gridLookUpEditUser_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "reload"))
                {
                    try
                    {
                        reloadUser();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

        #endregion
        
        #region Формын функц

            private void editUser()
            {
                if (checkEditAll.Checked)
                {
                    gridLookUpEditUser.Enabled = true;
                }
                else
                {
                    gridLookUpEditUser.Enabled = false;
                }
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if(dateEditStart.Text == "")
                {
                    isRight = false;
                    dateEditStart.Focus();
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditStart.Focus();
                    }
                }
                if (!checkEditAll.Checked && checkEditAll.Visible && gridLookUpEditUser.Visible && gridLookUpEditUser.Properties.View.GetSelectedRows().Length == 0)
                {
                    errorText = "Хэрэглэгчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditUser.Focus();
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void reloadUser()
            {   
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.SystemUserAnotherTableAdapter.Fill(InformationDataSet.Instance.SystemUserAnother, HospitalUtil.getHospitalId(), SystemUtil.ALL_USER);
                ProgramUtil.closeWaitDialog();
            }

            private void reload()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    if (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                    {
                        var userID = "";
                        if (checkEditAll.Checked)
                        {
                            for (var i = 0; i < InformationDataSet.Instance.SystemUserAnother.Rows.Count; i++)
                                userID += InformationDataSet.Instance.SystemUserAnother.Rows[i]["id"] + "~";
                        }
                        else
                        {
                            int[] rowID = gridLookUpEditUser.Properties.View.GetSelectedRows();
                            for (int i = 0; i < rowID.Length; i++)
                                userID += gridLookUpEditUser.Properties.View.GetDataRow(rowID[i])["id"] + "~";
                        }
                        if (!userID.Equals(""))
                            userID = userID.Substring(0, userID.Length - 1);
                        InformationDataSet.SystemLogTableAdapter.Fill(InformationDataSet.Instance.SystemLog, HospitalUtil.getHospitalId(), userID, dateEditStart.Text, dateEditEnd.Text);
                        InformationDataSet.ProgramLogTableAdapter.Fill(InformationDataSet.Instance.ProgramLog, HospitalUtil.getHospitalId(), userID, dateEditStart.Text, dateEditEnd.Text);
                    }
                    else
                    {
                        InformationDataSet.SystemLogTableAdapter.Fill(InformationDataSet.Instance.SystemLog, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                        InformationDataSet.ProgramLogTableAdapter.Fill(InformationDataSet.Instance.ProgramLog, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                    }
                    

                    ProgramUtil.closeWaitDialog();
                }
            }

            private void getRoleAndShow()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                {
                    simpleButtonReload.Location = new Point(501, 9);
                    checkEditAll.Visible = true;
                    labelControlAll.Visible = true;
                    gridLookUpEditUser.Visible = true;
                }
                else
                {
                    simpleButtonReload.Location = new Point(284, 9);
                    checkEditAll.Visible = false;
                    labelControlAll.Visible = false;
                    gridLookUpEditUser.Visible = false;
                }
                
            }

        #endregion

        #region Хэвлэх, хөрвүүлэх функц

            private void simpleButtonExport_Click(object sender, EventArgs e)
            {
                if(xtraTabControl.SelectedTabPage == xtraTabPageSystemLog)
                    ProgramUtil.convertGrid(gridViewSystemLog, "Хэрэглэгчийн хандалт");
                else if (xtraTabControl.SelectedTabPage == xtraTabPageProgramLog)
                    ProgramUtil.convertGrid(gridViewProgramLog, "Програмын хандалт");
            }

        #endregion

    }
}