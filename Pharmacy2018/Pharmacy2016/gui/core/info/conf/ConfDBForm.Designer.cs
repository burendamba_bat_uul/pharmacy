﻿namespace Pharmacy2016.gui.core.info.conf
{
    partial class ConfDBForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfDBForm));
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.textEditServerName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textEditUserName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.textEditDBName = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonTest = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.textEditServerName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDBName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonSave.Image")));
            this.simpleButtonSave.Location = new System.Drawing.Point(206, 154);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(80, 25);
            this.simpleButtonSave.TabIndex = 11;
            this.simpleButtonSave.Text = "Өөрчлөх";
            this.simpleButtonSave.ToolTip = "Өгөгдлийн сантай холбогдох тохиргоог өөрчлөх";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(292, 154);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(80, 25);
            this.simpleButtonCancel.TabIndex = 12;
            this.simpleButtonCancel.Text = "Болих";
            this.simpleButtonCancel.ToolTip = "Болих";
            // 
            // textEditServerName
            // 
            this.textEditServerName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditServerName.Location = new System.Drawing.Point(129, 30);
            this.textEditServerName.Name = "textEditServerName";
            this.textEditServerName.Properties.MaxLength = 50;
            this.textEditServerName.Size = new System.Drawing.Size(200, 20);
            this.textEditServerName.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(73, 33);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(50, 13);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Сервер*: ";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(72, 111);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(51, 13);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Нууц үг*: ";
            // 
            // textEditPassword
            // 
            this.textEditPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditPassword.Location = new System.Drawing.Point(129, 108);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Properties.MaxLength = 50;
            this.textEditPassword.Properties.UseSystemPasswordChar = true;
            this.textEditPassword.Size = new System.Drawing.Size(200, 20);
            this.textEditPassword.TabIndex = 4;
            this.textEditPassword.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textEditPassword_KeyUp);
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(49, 85);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(74, 13);
            this.labelControl3.TabIndex = 7;
            this.labelControl3.Text = "Нэвтрэх нэр*: ";
            // 
            // textEditUserName
            // 
            this.textEditUserName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditUserName.Location = new System.Drawing.Point(129, 82);
            this.textEditUserName.Name = "textEditUserName";
            this.textEditUserName.Properties.MaxLength = 50;
            this.textEditUserName.Size = new System.Drawing.Size(200, 20);
            this.textEditUserName.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(35, 59);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(88, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Өгөгдлийн сан*: ";
            // 
            // textEditDBName
            // 
            this.textEditDBName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditDBName.Location = new System.Drawing.Point(129, 56);
            this.textEditDBName.Name = "textEditDBName";
            this.textEditDBName.Properties.MaxLength = 50;
            this.textEditDBName.Size = new System.Drawing.Size(200, 20);
            this.textEditDBName.TabIndex = 2;
            // 
            // simpleButtonTest
            // 
            this.simpleButtonTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonTest.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonTest.Image")));
            this.simpleButtonTest.Location = new System.Drawing.Point(12, 154);
            this.simpleButtonTest.Name = "simpleButtonTest";
            this.simpleButtonTest.Size = new System.Drawing.Size(80, 25);
            this.simpleButtonTest.TabIndex = 10;
            this.simpleButtonTest.Text = "Шалгах";
            this.simpleButtonTest.ToolTip = "Өгөгдлийн сантай холбогдож байгаа  эсэхийг шалгах";
            this.simpleButtonTest.Click += new System.EventHandler(this.simpleButtonTest_Click);
            // 
            // ConfDBForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(384, 191);
            this.Controls.Add(this.simpleButtonTest);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.textEditDBName);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.textEditUserName);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.textEditPassword);
            this.Controls.Add(this.textEditServerName);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonSave);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfDBForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Өгөгдлийн сангийн тохиргоо";
            this.TopMost = true;
            this.Shown += new System.EventHandler(this.ConfDBForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.textEditServerName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDBName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.TextEdit textEditServerName;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEditPassword;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit textEditUserName;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit textEditDBName;
        private DevExpress.XtraEditors.SimpleButton simpleButtonTest;
    }
}