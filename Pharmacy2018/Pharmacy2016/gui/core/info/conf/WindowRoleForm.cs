﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.hospital;
using Pharmacy2016.gui.core.info.conf;
using System.IO;
using System.Data.SqlClient;
using Pharmacy2016.gui.core.journal.exp;



namespace Pharmacy2016.gui.core.info.conf
{
    /*
     * Програм цонх, хэрэглэгчийн эрх, цонх-хэрэглэгчийн эрх, эм зүйч-тасгийг тохируулах цонх.
     * 
     */

    public partial class WindowRoleForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static WindowRoleForm INSTANCE = new WindowRoleForm();

        public static WindowRoleForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private readonly int FORM_ID = 3008;

        private bool isInit = false;

        private bool isEdit = false;

        private System.IO.Stream layout_role = null;
        private System.IO.Stream layout_window = null;
        private System.IO.Stream layout_window_role = null;
        private System.IO.Stream layout_inc_type = null;
        private System.IO.Stream layout_exp_type = null;
        private System.IO.Stream layout_supplier = null;
        private System.IO.Stream layout_main_db = null;

        private WindowRoleForm()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            this.MdiParent = MainForm.Instance;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            initLayout();
            RoleUtil.addForm(FORM_ID, this);

            gridControlRole.DataSource = InformationDataSet.RoleBindingSource;
            gridControlWindow.DataSource = InformationDataSet.WindowBindingSource;
            gridControlWindowRole.DataSource = InformationDataSet.WindowRoleBindingSource;
            gridControlIncType.DataSource = InformationDataSet.IncTypeBindingSource;
            gridControlExpType.DataSource = InformationDataSet.ExpTypeBindingSource;
            gridControlSupplier.DataSource = UserDataSet.SupplierBindingSource;
            gridControlMainDB.DataSource = MainDBDataSet.MainDBBindingSource;
            gridControlLock.DataSource = UserDataSet.LockMedicineBindingSource;
            checkedComboBoxEditRole.Properties.DataSource = InformationDataSet.Instance.Role;

            gridLookUpEditAccount.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
            gridLookUpEditExpType.Properties.DataSource = InformationDataSet.ExpTypeBindingSource;
            gridLookUpEditMedicineExpType.Properties.DataSource = InformationDataSet.ExpTypeBindingSource;
            gridLookUpEdit1.Properties.DataSource = JournalDataSet.Instance.SystemUserJournal;

            lockButton.Visible = false;
        }

        private void initLayout()
        {
            layout_role = new System.IO.MemoryStream();
            gridViewRole.SaveLayoutToStream(layout_role);
            layout_role.Seek(0, System.IO.SeekOrigin.Begin);

            layout_window = new System.IO.MemoryStream();
            gridViewWindow.SaveLayoutToStream(layout_window);
            layout_window.Seek(0, System.IO.SeekOrigin.Begin);

            layout_window_role = new System.IO.MemoryStream();
            gridViewWindowRole.SaveLayoutToStream(layout_window_role);
            layout_window_role.Seek(0, System.IO.SeekOrigin.Begin);

            layout_inc_type = new System.IO.MemoryStream();
            gridViewIncType.SaveLayoutToStream(layout_inc_type);
            layout_inc_type.Seek(0, System.IO.SeekOrigin.Begin);

            layout_exp_type = new System.IO.MemoryStream();
            gridViewExpType.SaveLayoutToStream(layout_exp_type);
            layout_exp_type.Seek(0, System.IO.SeekOrigin.Begin);

            layout_supplier = new System.IO.MemoryStream();
            gridViewSupplier.SaveLayoutToStream(layout_supplier);
            layout_supplier.Seek(0, System.IO.SeekOrigin.Begin);

            layout_main_db = new System.IO.MemoryStream();
            gridViewMainDB.SaveLayoutToStream(layout_main_db);
            layout_main_db.Seek(0, System.IO.SeekOrigin.Begin);
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm()
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }

                if (Visible)
                {
                    Select();
                }
                else
                {
                    checkRole();

                    InformationDataSet.Instance.Window.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    InformationDataSet.Instance.Role.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    InformationDataSet.Instance.IncType.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    InformationDataSet.Instance.IncType.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                    InformationDataSet.Instance.ExpType.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    InformationDataSet.Instance.ExpType.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                    UserDataSet.Instance.Supplier.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    UserDataSet.Instance.Supplier.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                    UserDataSet.Instance.LockMedicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    UserDataSet.Instance.LockMedicine.lockUserIDColumn.DefaultValue = UserUtil.getUserId();
                    reload();

                    gridLookUpEditAccount.EditValue = RoleUtil.getAccountUserID();
                    checkEditIsUseStoreOrder.Checked = RoleUtil.getIsUseStoreOrder();
                    checkEditIsUseStoreDrug.Checked = RoleUtil.getIsUseStoreDrug();
                    textEditBackUpPath.EditValue = RoleUtil.getBackUpPath();
                    spinEditLimitYear.EditValue = RoleUtil.getLimitYear();
                    checkedComboBoxEditRole.SetEditValue(RoleUtil.getConfirm());
                    gridLookUpEditExpType.EditValue = RoleUtil.getDrugExpType();
                    gridLookUpEditMedicineExpType.EditValue = RoleUtil.getMedicineExpType();
                    textEditMedicineAccount.EditValue = RoleUtil.getMedicineAccount();
                    gridLookUpEdit1.EditValue = RoleUtil.getLockUserID();

                    Show();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void checkRole()
        {
            bool isAccountUser = (UserUtil.getUserRoleId() == RoleUtil.ACCOUNTANT_ID);
            xtraTabPageRole.PageVisible = !isAccountUser;
            xtraTabPageWindow.PageVisible = !isAccountUser;
            xtraTabPageWindowRole.PageVisible = !isAccountUser;
            xtraTabPageMainConf.PageVisible = !isAccountUser;

            gridColumnIncTypeID.OptionsColumn.ReadOnly = isAccountUser;
            gridColumnIncTypeName.OptionsColumn.ReadOnly = isAccountUser;
            gridColumnIncDescription.OptionsColumn.ReadOnly = isAccountUser;
            gridColumnIncIsUse.OptionsColumn.ReadOnly = isAccountUser;

            gridColumnExpTypeID.OptionsColumn.ReadOnly = isAccountUser;
            gridColumnExpTypeName.OptionsColumn.ReadOnly = isAccountUser;
            gridColumnExpDescription.OptionsColumn.ReadOnly = isAccountUser;
            gridColumnExpIsUse.OptionsColumn.ReadOnly = isAccountUser;

            bool[] roles = UserUtil.getWindowRole(FORM_ID);

            simpleButtonReload.Visible = roles[0];
            gridControlRole.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
            gridControlRole.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
            gridViewRole.OptionsBehavior.Editable = !SystemUtil.SELECTED_DB_READONLY && roles[2];

            gridControlWindow.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
            gridControlWindow.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
            gridViewWindow.OptionsBehavior.Editable = !SystemUtil.SELECTED_DB_READONLY && roles[2];

            gridControlWindowRole.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];

            gridControlIncType.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
            gridControlIncType.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
            gridViewIncType.OptionsBehavior.Editable = !SystemUtil.SELECTED_DB_READONLY && roles[2];

            gridControlExpType.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
            gridControlExpType.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
            gridViewExpType.OptionsBehavior.Editable = !SystemUtil.SELECTED_DB_READONLY && roles[2];

            gridControlSupplier.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
            gridControlSupplier.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
            gridViewSupplier.OptionsBehavior.Editable = !SystemUtil.SELECTED_DB_READONLY && roles[2];

            gridControlMainDB.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];


            simpleButtonBrowse.Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
            simpleButtonSave.Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];

            simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;

            //Гүйлгээ түгжих админы эрх шалгах
            string lockUserID = "";
            SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
            myConn.Open();
            string query = "SELECT lockUserID FROM [MainConf]";
            SqlCommand oCmd = new SqlCommand(query, myConn);
            using (SqlDataReader oReader = oCmd.ExecuteReader())
            {
                while (oReader.Read())
                {
                    if (oReader.HasRows)
                    {
                        lockUserID = oReader["lockUserID"].ToString();
                    }
                }
            }
            bool isLockUser = (UserUtil.getUserId() == lockUserID);
            xtraTabPageLock.PageVisible = isLockUser;
            //lockButton.Visible = isLockUser;
            checkLayout();
        }

        private void checkLayout()
        {
            string layout = UserUtil.getLayout("role");
            if (layout != null)
            {
                gridViewRole.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewRole.RestoreLayoutFromStream(layout_role);
                layout_role.Seek(0, System.IO.SeekOrigin.Begin);
            }

            layout = UserUtil.getLayout("window");
            if (layout != null)
            {
                gridViewWindow.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewWindow.RestoreLayoutFromStream(layout_window);
                layout_window.Seek(0, System.IO.SeekOrigin.Begin);
            }

            layout = UserUtil.getLayout("windowRole");
            if (layout != null)
            {
                gridViewWindowRole.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewWindowRole.RestoreLayoutFromStream(layout_window_role);
                layout_window_role.Seek(0, System.IO.SeekOrigin.Begin);
            }

            layout = UserUtil.getLayout("incType");
            if (layout != null)
            {
                gridViewIncType.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewIncType.RestoreLayoutFromStream(layout_inc_type);
                layout_inc_type.Seek(0, System.IO.SeekOrigin.Begin);
            }

            layout = UserUtil.getLayout("expType");
            if (layout != null)
            {
                gridViewExpType.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewExpType.RestoreLayoutFromStream(layout_exp_type);
                layout_exp_type.Seek(0, System.IO.SeekOrigin.Begin);
            }

            layout = UserUtil.getLayout("supplier");
            if (layout != null)
            {
                gridViewSupplier.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewSupplier.RestoreLayoutFromStream(layout_supplier);
                layout_supplier.Seek(0, System.IO.SeekOrigin.Begin);
            }

            layout = UserUtil.getLayout("mainDB");
            if (layout != null)
            {
                gridViewMainDB.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewMainDB.RestoreLayoutFromStream(layout_main_db);
                layout_main_db.Seek(0, System.IO.SeekOrigin.Begin);
            }
        }

        private void WindowRoleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void reload()
        {
            ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
            InformationDataSet.RoleTableAdapter.Fill(InformationDataSet.Instance.Role);
            InformationDataSet.WindowTableAdapter.Fill(InformationDataSet.Instance.Window);
            InformationDataSet.WindowRoleTableAdapter.Fill(InformationDataSet.Instance.WindowRole, HospitalUtil.getHospitalId());
            InformationDataSet.IncTypeTableAdapter.Fill(InformationDataSet.Instance.IncType, HospitalUtil.getHospitalId());
            InformationDataSet.ExpTypeTableAdapter.Fill(InformationDataSet.Instance.ExpType, HospitalUtil.getHospitalId());
            UserDataSet.SupplierTableAdapter.Fill(UserDataSet.Instance.Supplier, HospitalUtil.getHospitalId());

            InformationDataSet.SystemUserOtherTableAdapter.Fill(InformationDataSet.Instance.SystemUserOther, HospitalUtil.getHospitalId(), SystemUtil.PHARMACIST);
            InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.ACCOUNT);
            JournalDataSet.SystemUserJournalTableAdapter.Fill(JournalDataSet.Instance.SystemUserJournal, HospitalUtil.getHospitalId(), SystemUtil.LOCK_ADMIN);
            UserDataSet.LockMedicineTableAdapter.Fill(UserDataSet.Instance.LockMedicine, HospitalUtil.getHospitalId());
            ProgramUtil.closeWaitDialog();
        }

        #endregion

        #region Форм event

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            try
            {
                reload();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void simpleButtonLayout_Click(object sender, EventArgs e)
        {
            try
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                System.IO.Stream str = new System.IO.MemoryStream();
                System.IO.StreamReader reader = null;
                if (xtraTabControl.SelectedTabPage == xtraTabPageRole)
                {
                    gridViewRole.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("role", reader.ReadToEnd());
                }
                else if (xtraTabControl.SelectedTabPage == xtraTabPageWindow)
                {
                    gridViewWindow.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("window", reader.ReadToEnd());
                }
                else if (xtraTabControl.SelectedTabPage == xtraTabPageWindowRole)
                {
                    gridViewWindowRole.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("windowRole", reader.ReadToEnd());
                }
                else if (xtraTabControl.SelectedTabPage == xtraTabPageIncType)
                {
                    gridViewIncType.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("incType", reader.ReadToEnd());
                    reader.Close();
                }
                else if (xtraTabControl.SelectedTabPage == xtraTabPageSupplier)
                {
                    gridViewSupplier.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("supplier", reader.ReadToEnd());
                    reader.Close();
                }
                reader.Close();
                str.Close();

                ProgramUtil.closeWaitDialog();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }


        private void gridControlRole_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    addRole();
                }
                else if (String.Equals(e.Button.Tag, "delete"))
                {
                    try
                    {
                        deleteRole();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }
        }

        private void gridViewRole_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            try
            {
                if (!gridViewRole.HasColumnErrors)
                    saveRole();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void gridViewRole_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (gridViewRole.GetFocusedRowCellValue(gridColumnRoleId).ToString().Trim().Equals(""))
            {
                e.Valid = false;
                gridViewRole.SetColumnError(gridColumnRoleId, "Дугаарыг оруулна уу.");
            }
            if (gridViewRole.GetFocusedRowCellValue(gridColumnRoleName).ToString().Trim().Equals(""))
            {
                e.Valid = false;
                gridViewRole.SetColumnError(gridColumnRoleName, "Нэрийг оруулна уу.");
            }
            if (e.Valid && e.RowHandle < 0)
            {
                DataRow[] rows = InformationDataSet.Instance.Role.Select("id = " + Convert.ToInt32(gridViewRole.GetFocusedRowCellValue(gridColumnRoleId)));
                if (rows.Length != 0)
                {
                    e.Valid = false;
                    gridViewRole.SetColumnError(gridColumnRoleId, "Дугаар давхацсан байна.");
                }
            }
        }

        private void gridViewRole_EditFormPrepared(object sender, DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs e)
        {
            gridColumnRoleId.OptionsColumn.ShowInCustomizationForm = e.RowHandle < 0;
        }

        private void gridViewRole_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
        }


        private void gridControlWindow_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    addWindow();
                }
                else if (String.Equals(e.Button.Tag, "delete"))
                {
                    try
                    {
                        deleteWindow();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }
        }

        private void gridViewWindow_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            try
            {
                if (!gridViewWindow.HasColumnErrors)
                    saveWindow();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void gridViewWindow_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (gridViewWindow.GetFocusedRowCellValue(gridColumnWindowId).ToString().Trim().Equals(""))
            {
                e.Valid = false;
                gridViewWindow.SetColumnError(gridColumnWindowId, "Дугаарыг оруулна уу.");
            }
            if (gridViewWindow.GetFocusedRowCellValue(gridColumnWindowName).ToString().Trim().Equals(""))
            {
                e.Valid = false;
                gridViewWindow.SetColumnError(gridColumnWindowName, "Нэрийг оруулна уу.");
            }
            if (gridViewWindow.GetFocusedRowCellValue(gridColumnWindowDesc).ToString().Trim().Equals(""))
            {
                e.Valid = false;
                gridViewWindow.SetColumnError(gridColumnWindowDesc, "Тайлбарыг оруулна уу.");
            }
            if (e.Valid && e.RowHandle < 0)
            {
                DataRow[] rows = InformationDataSet.Instance.Window.Select("id = " + Convert.ToInt32(gridViewWindow.GetFocusedRowCellValue(gridColumnWindowId)));
                if (rows.Length != 0)
                {
                    e.Valid = false;
                    gridViewWindow.SetColumnError(gridColumnWindowId, "Дугаар давхацсан байна.");
                }
            }
        }

        private void gridViewWindow_EditFormPrepared(object sender, DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs e)
        {
            gridColumnWindowId.OptionsColumn.ReadOnly = e.RowHandle >= 0;
            //gridColumnWindowId.OptionsColumn.ShowInCustomizationForm = e.RowHandle < 0;
        }

        private void gridViewWindow_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
        }


        private void gridControlWindowRole_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "edit"))
                {
                    editWindowRole();
                }
            }
        }

        private void repositoryItemCheckEdit1_CheckedChanged(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(repositoryItemCheckEdit1.ValueChecked) && (gridViewWindowRole.FocusedColumn == gridColumnAppend || gridViewWindowRole.FocusedColumn == gridColumnEdit || gridViewWindowRole.FocusedColumn == gridColumnRemove || gridViewWindowRole.FocusedColumn == gridColumnPrinting) &&
                !Convert.ToBoolean(gridViewWindowRole.GetRowCellValue(gridViewWindowRole.FocusedRowHandle, gridColumnShow)))
            {
                gridViewWindowRole.SetRowCellValue(gridViewWindowRole.FocusedRowHandle, gridColumnShow, true);
            }
        }

        private void gridViewWindowRole_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if ((Convert.ToBoolean(gridViewWindowRole.GetRowCellValue(e.RowHandle, gridColumnAppend)) || Convert.ToBoolean(gridViewWindowRole.GetRowCellValue(e.RowHandle, gridColumnEdit)) || Convert.ToBoolean(gridViewWindowRole.GetRowCellValue(e.RowHandle, gridColumnRemove)) || Convert.ToBoolean(gridViewWindowRole.GetRowCellValue(e.RowHandle, gridColumnPrinting))) && !Convert.ToBoolean(gridViewWindowRole.GetRowCellValue(e.RowHandle, gridColumnShow)))
            {
                e.Valid = false;
                gridViewWindowRole.SetColumnError(gridColumnShow, "Энэ мөрийг харах эрхтэй болгох ёстой.");
            }
        }

        private void gridViewWindowRole_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
        }


        private void xtraTabControl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            showCheckEdit(e.Page == xtraTabPageWindowRole);
        }

        private void checkEditAll_CheckedChanged(object sender, EventArgs e)
        {
            checkAll();
        }

        private void checkEditPrinting_CheckedChanged(object sender, EventArgs e)
        {
            checkColumn(checkEditPrinting.Checked, gridColumnPrinting);
        }

        private void checkEditRemove_CheckedChanged(object sender, EventArgs e)
        {
            checkColumn(checkEditRemove.Checked, gridColumnRemove);
        }

        private void checkEditEdit_CheckedChanged(object sender, EventArgs e)
        {
            checkColumn(checkEditEdit.Checked, gridColumnEdit);
        }

        private void checkEditAppend_CheckedChanged(object sender, EventArgs e)
        {
            checkColumn(checkEditAppend.Checked, gridColumnAppend);
        }

        private void checkEditShow_CheckedChanged(object sender, EventArgs e)
        {
            checkColumn(checkEditShow.Checked, gridColumnShow);
        }


        private void gridControlIncType_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    addIncType();
                }
                else if (String.Equals(e.Button.Tag, "delete"))
                {
                    try
                    {
                        deleteIncType();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }
        }

        private void gridViewIncType_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            try
            {
                if (!gridViewIncType.HasColumnErrors)
                    saveIncType();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void gridViewIncType_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (gridViewIncType.GetFocusedRowCellValue(gridColumnIncTypeID).ToString().Trim().Equals(""))
            {
                e.Valid = false;
                gridViewIncType.SetColumnError(gridColumnIncTypeID, "Дугаарыг оруулна уу.");
            }
            if (gridViewIncType.GetFocusedRowCellValue(gridColumnIncTypeName).ToString().Trim().Equals(""))
            {
                e.Valid = false;
                gridViewIncType.SetColumnError(gridColumnIncTypeName, "Нэрийг оруулна уу.");
            }
            if (e.Valid && e.RowHandle < 0)
            {
                DataRow[] rows = InformationDataSet.Instance.IncType.Select("id = " + Convert.ToInt32(gridViewIncType.GetFocusedRowCellValue(gridColumnIncTypeID)));
                if (rows.Length != 0)
                {
                    e.Valid = false;
                    gridViewIncType.SetColumnError(gridColumnIncTypeID, "Дугаар давхацсан байна.");
                }
            }
        }

        private void gridViewIncType_EditFormPrepared(object sender, DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs e)
        {
            gridColumnIncTypeID.OptionsColumn.ReadOnly = e.RowHandle >= 0;
            //gridColumnIncTypeID.OptionsColumn.ShowInCustomizationForm = e.RowHandle < 0;
        }

        private void gridViewIncType_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
        }


        private void gridControlExpType_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    addExpType();
                }
                else if (String.Equals(e.Button.Tag, "delete"))
                {
                    try
                    {
                        deleteExpType();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }
        }

        private void gridViewExpType_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            try
            {
                if (!gridViewExpType.HasColumnErrors)
                    saveExpType();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void gridViewExpType_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (gridViewExpType.GetFocusedRowCellValue(gridColumnIncTypeID).ToString().Trim().Equals(""))
            {
                e.Valid = false;
                gridViewExpType.SetColumnError(gridColumnExpTypeID, "Дугаарыг оруулна уу.");
            }
            if (gridViewExpType.GetFocusedRowCellValue(gridColumnExpTypeName).ToString().Trim().Equals(""))
            {
                e.Valid = false;
                gridViewExpType.SetColumnError(gridColumnExpTypeName, "Нэрийг оруулна уу.");
            }
            if (e.Valid && e.RowHandle < 0)
            {
                DataRow[] rows = InformationDataSet.Instance.ExpType.Select("id = " + Convert.ToInt32(gridViewExpType.GetFocusedRowCellValue(gridColumnExpTypeID)));
                if (rows.Length != 0)
                {
                    e.Valid = false;
                    gridViewExpType.SetColumnError(gridColumnExpTypeID, "Дугаар давхацсан байна.");
                }
            }
        }

        private void gridViewExpType_EditFormPrepared(object sender, DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs e)
        {
            gridColumnExpTypeID.OptionsColumn.ReadOnly = e.RowHandle >= 0;
            //gridColumnIncTypeID.OptionsColumn.ShowInCustomizationForm = e.RowHandle < 0;
        }

        private void gridViewExpType_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
        }


        private void simpleButtonSave_Click(object sender, EventArgs e)
        {
            saveMainConf();
        }

        private void gridControlProducer_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    addSupplier();
                }
                else if (String.Equals(e.Button.Tag, "delete"))
                {
                    try
                    {
                        deleteSupplier();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }
        }

        private void gridViewSupplier_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
        }

        private void gridViewSupplier_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            saveSupplier();
        }

        private void gridViewSupplier_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (gridViewSupplier.GetFocusedRowCellValue(gridColumnSupplier).ToString().Trim().Equals(""))
            {
                e.Valid = false;
                gridViewSupplier.SetColumnError(gridColumnSupplier, "Нэрийг оруулна уу.");
            }
            if (gridViewSupplier.GetFocusedRowCellValue(gridColumnConNo).ToString().Trim().Equals(""))
            {
                e.Valid = false;
                gridViewSupplier.SetColumnError(gridColumnConNo, "Дугаарыг оруулна уу.");
            }

            int exist = Convert.ToInt32(UserDataSet.QueriesTableAdapter.exist_supplier_name(gridViewSupplier.GetRowCellDisplayText(gridViewSupplier.FocusedRowHandle, gridColumnSupplier), Int32.Parse(gridViewSupplier.GetRowCellDisplayText(gridViewSupplier.FocusedRowHandle, gridColumnConNo))));
            if (exist == 2)
            {
                string text = "", errorText;
                errorText = "Бэлтгэн нийлүүлэгчийн нэр бүртгэгдсэн байна";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                e.Valid = false;
                gridViewSupplier.SetColumnError(gridColumnSupplier, errorText);
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
            }

        }


        private void gridControlMainDB_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "edit"))
                {
                    changeMainDB();
                }
            }
        }

        private void gridControlLock_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    LockAllForm.Instance.showForm(Instance);
                }
                else if (String.Equals(e.Button.Tag, "delete"))
                {
                    try
                    {
                        deleteLockMedicine();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }
        }

        #endregion


        #region Хэрэглэгчийн эрх нэмэх, засах, устгах функц

        private void addRole()
        {
            DataRowView view = (DataRowView)InformationDataSet.RoleBindingSource.AddNew();
            gridViewRole.ShowEditForm();
        }

        private void deleteRole()
        {
            DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Хэрэглэгчийн эрх устгах", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                gridViewRole.DeleteRow(gridViewRole.GetFocusedDataSourceRowIndex());
                saveRole();
            }
        }

        private void saveRole()
        {
            ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
            InformationDataSet.RoleBindingSource.EndEdit();
            InformationDataSet.RoleTableAdapter.Update(InformationDataSet.Instance.Role);
            ProgramUtil.closeWaitDialog();
        }

        #endregion

        #region Цонх нэмэх, засах, устгах функц

        private void addWindow()
        {
            DataRowView view = (DataRowView)InformationDataSet.WindowBindingSource.AddNew();
            gridViewWindow.ShowEditForm();
        }

        private void deleteWindow()
        {
            DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Цонх устгах", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                gridViewWindow.DeleteRow(gridViewWindow.GetFocusedDataSourceRowIndex());
                saveWindow();
            }
        }

        private void saveWindow()
        {
            ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
            InformationDataSet.WindowBindingSource.EndEdit();
            InformationDataSet.WindowTableAdapter.Update(InformationDataSet.Instance.Window);
            ProgramUtil.closeWaitDialog();
        }

        #endregion

        #region Цонхны эрх нэмэх, засах, устгах функц

        private void editWindowRole()
        {
            if (isEdit && !gridViewWindowRole.HasColumnErrors)
            {
                DialogResult dialogResult = XtraMessageBox.Show("Өөрчлөлтийг хадгалах уу", "Цонхны эрхийн тохиргоо", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    saveWindowRole();
                }
                else
                {
                    InformationDataSet.WindowRoleBindingSource.CancelEdit();
                    InformationDataSet.Instance.WindowRole.RejectChanges();
                }
                gridControlWindowRole.EmbeddedNavigator.Buttons.CustomButtons[0].ImageIndex = 15;
                gridViewWindowRole.OptionsBehavior.ReadOnly = true;
                isEdit = false;
            }
            else
            {
                gridControlWindowRole.EmbeddedNavigator.Buttons.CustomButtons[0].ImageIndex = 16;
                gridViewWindowRole.OptionsBehavior.ReadOnly = false;
                isEdit = true;
            }
        }

        private void saveWindowRole()
        {
            ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
            InformationDataSet.WindowRoleBindingSource.EndEdit();
            InformationDataSet.WindowRoleTableAdapter.Update(InformationDataSet.Instance.WindowRole);
            ProgramUtil.closeWaitDialog();
        }

        private void showCheckEdit(bool visible)
        {
            checkEditAll.Visible = visible;
            checkEditShow.Visible = visible;
            checkEditAppend.Visible = visible;
            checkEditEdit.Visible = visible;
            checkEditRemove.Visible = visible;
            checkEditPrinting.Visible = visible;
        }

        private void checkAll()
        {
            if (isEdit)
            {
                bool value = checkEditAll.Checked;
                checkEditShow.Checked = value;
                checkEditAppend.Checked = value;
                checkEditEdit.Checked = value;
                checkEditRemove.Checked = value;
                checkEditPrinting.Checked = value;
                //for (var i = 0; i < gridViewWindowRole.RowCount; i++)
                //{
                //    gridViewWindowRole.SetRowCellValue(i, gridColumnShow, value);
                //    gridViewWindowRole.SetRowCellValue(i, gridColumnAppend, value);
                //    gridViewWindowRole.SetRowCellValue(i, gridColumnEdit, value);
                //    gridViewWindowRole.SetRowCellValue(i, gridColumnRemove, value);
                //    gridViewWindowRole.SetRowCellValue(i, gridColumnPrinting, value);
                //}
            }
        }

        private void checkColumn(bool value, GridColumn column)
        {
            if (isEdit)
            {
                for (var i = 0; i < gridViewWindowRole.RowCount; i++)
                {
                    gridViewWindowRole.SetRowCellValue(i, column, value);
                    if (column != gridColumnShow && value && !Convert.ToBoolean(gridViewWindowRole.GetRowCellValue(i, gridColumnShow)))
                        gridViewWindowRole.SetRowCellValue(i, gridColumnShow, value);
                }
            }
        }

        #endregion

        #region Орлогын төрөл нэмэх, засах, устгах функц

        private void addIncType()
        {
            DataRowView view = (DataRowView)InformationDataSet.IncTypeBindingSource.AddNew();
            gridViewIncType.ShowEditForm();
        }

        private void deleteIncType()
        {
            if (Convert.ToBoolean(gridViewIncType.GetFocusedDataRow()["isDefault"]))
            {
                XtraMessageBox.Show("Энэ мөр үндсэн төрөл тул устгаж болохгүй");
                return;
            }
            object ret = InformationDataSet.QueriesTableAdapter.can_delete_inc_type(HospitalUtil.getHospitalId(), Convert.ToInt32(gridViewIncType.GetFocusedDataRow()["id"]));
            if (ret != DBNull.Value && ret != null)
            {
                string msg = ret.ToString();
                XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                return;
            }
            DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Орлогын төрөл устгах", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                gridViewIncType.DeleteSelectedRows();
                saveIncType();
            }
        }

        private void saveIncType()
        {
            ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
            InformationDataSet.IncTypeBindingSource.EndEdit();
            InformationDataSet.IncTypeTableAdapter.Update(InformationDataSet.Instance.IncType);
            ProgramUtil.closeWaitDialog();
        }

        #endregion

        #region Зарлагын төрөл нэмэх, засах, устгах функц

        private void addExpType()
        {
            DataRowView view = (DataRowView)InformationDataSet.ExpTypeBindingSource.AddNew();
            gridViewExpType.ShowEditForm();
        }

        private void deleteExpType()
        {
            if (Convert.ToBoolean(gridViewExpType.GetFocusedDataRow()["isDefault"]))
            {
                XtraMessageBox.Show("Энэ мөр үндсэн төрөл тул устгаж болохгүй");
                return;
            }
            object ret = InformationDataSet.QueriesTableAdapter.can_delete_exp_type(HospitalUtil.getHospitalId(), Convert.ToInt32(gridViewExpType.GetFocusedDataRow()["id"]));
            if (ret != DBNull.Value && ret != null)
            {
                string msg = ret.ToString();
                XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                return;
            }
            DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Зарлагын төрөл устгах", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                gridViewExpType.DeleteSelectedRows();
                saveExpType();
            }
        }

        private void saveExpType()
        {
            ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
            InformationDataSet.ExpTypeBindingSource.EndEdit();
            InformationDataSet.ExpTypeTableAdapter.Update(InformationDataSet.Instance.ExpType);
            ProgramUtil.closeWaitDialog();
        }

        #endregion

        #region Бэлтгэн нийлүүлэгч засах, нэмэх, устгах функцууд.

        private void addSupplier()
        {
            DataRow[] rows = UserDataSet.Instance.Supplier.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "'");
            if (rows.Length > 0)
            {
                DataRowView currentProducer = (DataRowView)UserDataSet.SupplierBindingSource.Current;
                if (currentProducer.IsNew)
                {
                    UserDataSet.SupplierBindingSource.CancelEdit();
                    UserDataSet.Instance.Supplier.RejectChanges();
                }
            }
            DataRowView view = (DataRowView)UserDataSet.SupplierBindingSource.AddNew();
            view["id"] = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            gridViewSupplier.ShowEditForm();
        }

        private void deleteSupplier()
        {
            object ret = UserDataSet.QueriesTableAdapter.can_delete_supplier(HospitalUtil.getHospitalId(), gridViewSupplier.GetFocusedDataRow()["id"].ToString());
            if (ret != DBNull.Value && ret != null)
            {
                string msg = ret.ToString();
                XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                return;
            }
            DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Бэлтгэн нийлүүлэгч усгах", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                gridViewSupplier.DeleteSelectedRows();
                saveSupplier();
            }
        }

        private void saveSupplier()
        {
            try
            {
                if (!gridViewSupplier.HasColumnErrors)
                    UserDataSet.SupplierBindingSource.EndEdit();
                UserDataSet.SupplierTableAdapter.Update(UserDataSet.Instance.Supplier);
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                ProgramUtil.closeWaitDialog();
            }
        }

        #endregion

        #region Ерөнхий тохиргоо хадгалах функц

        private void saveMainConf()
        {
            ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
            DataRow row = InformationDataSet.Instance.MainConf.Rows[0];
            row["accountUserID"] = gridLookUpEditAccount.EditValue;
            row["isUseStoreOrder"] = checkEditIsUseStoreOrder.Checked;
            row["isUseStoreDrug"] = checkEditIsUseStoreDrug.Checked;
            row["backUpPath"] = textEditBackUpPath.Text;
            row["limitYear"] = spinEditLimitYear.Value;
            row["configure"] = checkedComboBoxEditRole.EditValue;
            row["drugExpType"] = gridLookUpEditExpType.EditValue;
            row["medicineExpType"] = gridLookUpEditMedicineExpType.EditValue;
            row["medicineAccount"] = textEditMedicineAccount.Text;
            row["lockUserID"] = gridLookUpEdit1.EditValue;
            row.EndEdit();
            InformationDataSet.MainConfTableAdapter.Update(InformationDataSet.Instance.MainConf);
            RoleUtil.setMainConf(row);
            MainForm.Instance.setStoreMenu();
            WardRegisterForm.Instance.isUseStore();
            ProgramUtil.closeWaitDialog();
        }

        private void simpleButtonBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                textEditBackUpPath.Text = dlg.SelectedPath;
            }
        }

        #endregion

        #region Өгөгдлийн сангийн функцууд.

        private void changeMainDB()
        {
            try
            {
                if (gridViewMainDB.RowCount == 0 || gridViewMainDB.FocusedRowHandle < 0 || Convert.ToBoolean(gridViewMainDB.GetFocusedRowCellValue(gridColumnIsCurrDB)))
                    return;

                if (!checkDatabaseExists(gridViewMainDB.GetFocusedRowCellValue(gridColumnDBName).ToString()))
                {
                    XtraMessageBox.Show("' " + gridViewMainDB.GetFocusedRowCellValue(gridColumnDBName) + " ' " + "өгөгдлийн сан олдсонгүй!");
                    return;
                }

                DialogResult dialogResult = XtraMessageBox.Show("Өгөгдлийн санг " + gridViewMainDB.GetFocusedRowCellValue(gridColumnDBName) + " болгон өөрчлөх үү?", "Өгөгдлийн сан өөрчлөх", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    string oldName = "";
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    for (int i = 0; i < gridViewMainDB.RowCount; i++)
                    {
                        if (Convert.ToBoolean(gridViewMainDB.GetRowCellValue(i, gridColumnIsCurrDB)))
                        {
                            gridViewMainDB.SetRowCellValue(i, gridColumnIsCurrDB, false);
                            oldName = gridViewMainDB.GetRowCellValue(i, gridColumnDBName).ToString();
                            break;
                        }
                    }
                    gridViewMainDB.SetRowCellValue(gridViewMainDB.FocusedRowHandle, gridColumnIsCurrDB, true);

                    MainDBDataSet.MainDBBindingSource.EndEdit();
                    MainDBDataSet.MainDBTableAdapter.Update(MainDBDataSet.Instance.MainDB);

                    setReadOnly(gridViewMainDB.GetRowCellValue(gridViewMainDB.FocusedRowHandle, gridColumnDBName).ToString(), false);
                    setReadOnly(oldName, true);
                    ProgramUtil.closeWaitDialog();
                    XtraMessageBox.Show("Өгөгдлийн санг өөрчиллөө. Програм хаагдах болно.");
                    ProgramUtil.EXITFULLY();
                }
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                ProgramUtil.closeWaitDialog();
            }
        }

        private void setReadOnly(string database, bool isReadOnly)
        {
            try
            {
                var querySingle = "ALTER DATABASE " + database + " SET " + (isReadOnly ? "SINGLE_USER" : "MULTI_USER") + " WITH ROLLBACK IMMEDIATE";
                var connSingle = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                var commandSingle = new SqlCommand(querySingle, connSingle);
                {
                    connSingle.Open();
                    using (SqlDataReader oReader = commandSingle.ExecuteReader())
                    {
                        if (oReader.Read())
                        {
                            //XtraMessageBox.Show("Success.");
                        }
                        connSingle.Close();
                    }
                }

                var queryReadOnly = "ALTER  DATABASE " + database + " SET " + (isReadOnly ? "READ_ONLY" : "READ_WRITE") + " WITH NO_WAIT;";
                var connReadOnly = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                var commandReadOnly = new SqlCommand(queryReadOnly, connReadOnly);
                connReadOnly.Open();
                using (SqlDataReader oReader = commandReadOnly.ExecuteReader())
                {
                    if (oReader.Read())
                    {
                        //XtraMessageBox.Show("Success.");
                    }
                    connReadOnly.Close();
                }
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
            }
        }

        public bool checkDatabaseExists(string databaseName)
        {
            string conStr = Pharmacy2016.Properties.Settings.Default.conMedicine;
            using (var connection = new SqlConnection(conStr))
            {
                using (var command = new SqlCommand(string.Format("SELECT db_id('{0}')", databaseName), connection))
                {
                    connection.Open();
                    return (command.ExecuteScalar() != DBNull.Value);
                }
            }
        }

        #endregion

        #region Гүйлгээ түгжих функцууд.

        private void deleteLockMedicine()
        {

            DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Гүйлгээ түгжилтийг усгах", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                gridViewLock.DeleteSelectedRows();
                saveLock();
            }
        }

        private void saveLock()
        {
            try
            {
                if (!gridViewLock.HasColumnErrors)
                    UserDataSet.LockMedicineBindingSource.EndEdit();
                UserDataSet.LockMedicineTableAdapter.Update(UserDataSet.Instance.LockMedicine);
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                ProgramUtil.closeWaitDialog();
            }
        }


        #endregion

        #region Тайлан тохиргоо хадгалах функц

        private void saveReportSetting()
        {
            ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
            SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
            myConn.Open();
            string sqlmain = "UPDATE [ReportSetting] " +
                         "SET name = '"+ richTextBox1.Text +"' " +
                         "WHERE code = '0'";
            string sql1 = "UPDATE [ReportSetting] " +
                         "SET employ = '"+ textEmploy1.Text +"', name = '"+ textName1.Text +"' " +
                         "WHERE code = '1'";
            string sql2 = "UPDATE [ReportSetting] " +
                         "SET employ = '"+ textEmploy2.Text +"', name = '"+ textName2.Text +"' " +
                         "WHERE code = '2'";
            string sql3 = "UPDATE [ReportSetting] " +
                         "SET employ = '"+ textEmploy3.Text +"', name = '"+ textName3.Text +"' " +
                         "WHERE code = '3'";
            List<string> list = new List<string>();
            list.Add(sqlmain);
            list.Add(sql1);
            list.Add(sql2);
            list.Add(sql3);
            foreach (string sql in list) {
                SqlCommand oCmd2 = new SqlCommand(sql, myConn);
                using (SqlDataReader oReader1 = oCmd2.ExecuteReader())
                {
                    while (oReader1.Read())
                    {
                    }
                }
            }
            myConn.Close();
            ProgramUtil.closeWaitDialog();
        }

        #endregion

        private void gridViewWindowRole_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (isEdit)
            {
                if (e.Column == gridColumnShow)
                {
                    if (Convert.ToBoolean(e.Value) == false) gridViewWindowRole.SetFocusedRowCellValue(gridColumnShow, 0);
                    else gridViewWindowRole.SetFocusedRowCellValue(gridColumnShow, 1);
                }
                if (e.Column == gridColumnAppend)
                {
                    if (Convert.ToBoolean(e.Value) == false) gridViewWindowRole.SetFocusedRowCellValue(gridColumnAppend, 0);
                    else gridViewWindowRole.SetFocusedRowCellValue(gridColumnAppend, 1);
                }
                if (e.Column == gridColumnEdit)
                {
                    if (Convert.ToBoolean(e.Value) == false) gridViewWindowRole.SetFocusedRowCellValue(gridColumnEdit, 0);
                    else gridViewWindowRole.SetFocusedRowCellValue(gridColumnEdit, 1);
                }
                if (e.Column == gridColumnRemove)
                {
                    if (Convert.ToBoolean(e.Value) == false) gridViewWindowRole.SetFocusedRowCellValue(gridColumnRemove, 0);
                    else gridViewWindowRole.SetFocusedRowCellValue(gridColumnRemove, 1);
                }
                if (e.Column == gridColumnPrinting)
                {
                    if (Convert.ToBoolean(e.Value) == false) gridViewWindowRole.SetFocusedRowCellValue(gridColumnPrinting, 0);
                    else gridViewWindowRole.SetFocusedRowCellValue(gridColumnPrinting, 1);
                }
            }
        }

        private void lockButton_Click(object sender, EventArgs e)
        {
            //LockAllForm.Instance.showForm(Instance);
        }

        private void xtraTabControl_Click(object sender, EventArgs e)
        {

        }

        private void xtraTabPage1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void xtraTabPageLock_Paint(object sender, PaintEventArgs e)
        {

        }

        private void gridControlMainDB_Click(object sender, EventArgs e)
        {

        }

        private void gridControlLock_Click(object sender, EventArgs e)
        {

        }

        private void saveReport_Click(object sender, EventArgs e)
        {
            saveReportSetting();
        }

        private void richTextBox1_TextChanged_1(object sender, EventArgs e)
        {
        
        }


    }
}