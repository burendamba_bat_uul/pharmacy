﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.report.journal.exp;


namespace Pharmacy2016.gui.core.journal.exp
{
    public partial class LockAllForm : DevExpress.XtraEditors.XtraForm
    {
        private static LockAllForm INSTANCE = new LockAllForm();

        public static LockAllForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }
        public LockAllForm()
        {
        }
        private bool isInit = false;

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
        }    

        public void showForm(XtraForm form)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }
                clearData();
                ShowDialog(form);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
            }
        }

        #region Формын функц

        private void clearData()
        {
            dateEditEnd.EditValue = "";
            dateEditStart.EditValue = "";
            textEditPassword.Text = "";
        }

        private bool check()
        {
            bool isRight = true;
            return isRight;
        }

        private void save()
        {
            if (check())
            {
                ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                //string newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());

                UserDataSet.Instance.LockMedicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                UserDataSet.Instance.LockMedicine.lockUserIDColumn.DefaultValue = UserUtil.getUserId();
                //UserDataSet.Instance.LockMedicine.startDateColumn.DefaultValue = dateEditStart.Text;
                //UserDataSet.Instance.LockMedicine.endDateColumn.DefaultValue = dateEditEnd.Text;


                //UserDataSet.LockMedicineBindingSource.AddNew();
                DataRowView view = (DataRowView)UserDataSet.LockMedicineBindingSource.AddNew();
                view["startDate"] = dateEditStart.Text;
                view["endDate"] = dateEditEnd.Text;
                view.EndEdit();
                UserDataSet.LockMedicineTableAdapter.Update(UserDataSet.Instance.LockMedicine);                
                ProgramUtil.closeWaitDialog();
                Close();
            }
            else {
                XtraMessageBox.Show("Сонгосон хугацаанд гүйлгээ түгжсэн байна!");
            }
        }

        private void LockAllButton_Click(object sender, EventArgs e)
        {
            string password = "";
            SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
            myConn.Open();
            string query = "SELECT password FROM [SystemUser] WHERE userID = '" + UserUtil.getUserId() + "'";
            SqlCommand oCmd = new SqlCommand(query, myConn);
            using (SqlDataReader oReader = oCmd.ExecuteReader())
            {
                while (oReader.Read())
                {
                    if (oReader.HasRows)
                    {
                        password = oReader["password"].ToString();
                    }
                }
            }
            if (password != UserUtil.convertMD5Hash(textEditPassword.Text))
            {
                XtraMessageBox.Show("Хэрэглэгчийн нууц үг буруу байна!");
                textEditPassword.Text = "";
            }
            else
            {
                save();
                Close();
            }
        }

        #endregion         

        
        
    }
}