﻿namespace Pharmacy2016.gui.core.info.conf
{
    partial class WindowRoleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WindowRoleForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lockButton = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditShow = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditAppend = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditPrinting = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditEdit = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditRemove = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditAll = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageRole = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlRole = new DevExpress.XtraGrid.GridControl();
            this.gridViewRole = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnRoleId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnRoleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageWindow = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlWindow = new DevExpress.XtraGrid.GridControl();
            this.gridViewWindow = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnWindowId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnWindowName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnWindowDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageWindowRole = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlWindowRole = new DevExpress.XtraGrid.GridControl();
            this.gridViewWindowRole = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnShow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumnAppend = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnEdit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnRemove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPrinting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageIncType = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlIncType = new DevExpress.XtraGrid.GridControl();
            this.gridViewIncType = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnIncTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnIncTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnIncDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnIncIsUse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageExpType = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlExpType = new DevExpress.XtraGrid.GridControl();
            this.gridViewExpType = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnExpTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnExpTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnExpTypeAccount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnExpDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnExpIsUse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageMainConf = new DevExpress.XtraTab.XtraTabPage();
            this.gridLookUpEdit1 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.textEditMedicineAccount = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditMedicineExpType = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditExpType = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.checkedComboBoxEditRole = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditLimitYear = new DevExpress.XtraEditors.SpinEdit();
            this.simpleButtonBrowse = new DevExpress.XtraEditors.SimpleButton();
            this.textEditBackUpPath = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditIsUseStoreDrug = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditIsUseStoreOrder = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditAccount = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageSupplier = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlSupplier = new DevExpress.XtraGrid.GridControl();
            this.gridViewSupplier = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnSupplier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnConNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditCountry = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageMainDB = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlMainDB = new DevExpress.XtraGrid.GridControl();
            this.gridViewMainDB = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnDBName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnBegBalDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnIsCurrDB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageLock = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlLock = new DevExpress.XtraGrid.GridControl();
            this.gridViewLock = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabReport = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.textName3 = new DevExpress.XtraEditors.TextEdit();
            this.textName2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.textName1 = new DevExpress.XtraEditors.TextEdit();
            this.textEmploy3 = new DevExpress.XtraEditors.TextEdit();
            this.textEmploy2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEmploy1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.saveReport = new DevExpress.XtraEditors.SimpleButton();
            this.informationDataSet1 = new Pharmacy2016.gui.core.info.ds.InformationDataSet();
            this.informationDataSet2 = new Pharmacy2016.gui.core.info.ds.InformationDataSet();
            this.loginUserInfoTableAdapter1 = new Pharmacy2016.gui.core.info.ds.InformationDataSetTableAdapters.LoginUserInfoTableAdapter();
            this.loginUserInfoTableAdapter2 = new Pharmacy2016.gui.core.info.ds.InformationDataSetTableAdapters.LoginUserInfoTableAdapter();
            this.loginUserInfoTableAdapter3 = new Pharmacy2016.gui.core.info.ds.InformationDataSetTableAdapters.LoginUserInfoTableAdapter();
            this.expTypeOtherTableAdapter1 = new Pharmacy2016.gui.core.info.ds.InformationDataSetTableAdapters.ExpTypeOtherTableAdapter();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAppend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPrinting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditRemove.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageRole.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            this.xtraTabPageWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            this.xtraTabPageWindowRole.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWindowRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWindowRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.xtraTabPageIncType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlIncType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewIncType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            this.xtraTabPageExpType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExpType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExpType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).BeginInit();
            this.xtraTabPageMainConf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMedicineAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicineExpType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditExpType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEditRole.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditLimitYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBackUpPath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsUseStoreDrug.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsUseStoreOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.xtraTabPageSupplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.xtraTabPageMainDB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMainDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMainDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.xtraTabPageLock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            this.xtraTabReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textName3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textName2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textName1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEmploy3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEmploy2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEmploy1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.informationDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.informationDataSet2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lockButton);
            this.panelControl1.Controls.Add(this.simpleButtonLayout);
            this.panelControl1.Controls.Add(this.checkEditShow);
            this.panelControl1.Controls.Add(this.checkEditAppend);
            this.panelControl1.Controls.Add(this.checkEditPrinting);
            this.panelControl1.Controls.Add(this.checkEditEdit);
            this.panelControl1.Controls.Add(this.checkEditRemove);
            this.panelControl1.Controls.Add(this.checkEditAll);
            this.panelControl1.Controls.Add(this.simpleButtonReload);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1007, 49);
            this.panelControl1.TabIndex = 1;
            // 
            // lockButton
            // 
            this.lockButton.Image = global::Pharmacy2016.PrintRibbonControllerResources.RibbonPrintPreview_HandTool;
            this.lockButton.Location = new System.Drawing.Point(102, 12);
            this.lockButton.Name = "lockButton";
            this.lockButton.Size = new System.Drawing.Size(75, 23);
            this.lockButton.TabIndex = 8;
            this.lockButton.Text = "Түгжих";
            this.lockButton.ToolTip = "Гүйлгээ түгжих";
            this.lockButton.Click += new System.EventHandler(this.lockButton_Click);
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(972, 12);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 7;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // checkEditShow
            // 
            this.checkEditShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditShow.Location = new System.Drawing.Point(657, 14);
            this.checkEditShow.Name = "checkEditShow";
            this.checkEditShow.Properties.Caption = "Харах";
            this.checkEditShow.Size = new System.Drawing.Size(55, 19);
            this.checkEditShow.TabIndex = 6;
            this.checkEditShow.Visible = false;
            this.checkEditShow.CheckedChanged += new System.EventHandler(this.checkEditShow_CheckedChanged);
            // 
            // checkEditAppend
            // 
            this.checkEditAppend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditAppend.Location = new System.Drawing.Point(718, 14);
            this.checkEditAppend.Name = "checkEditAppend";
            this.checkEditAppend.Properties.Caption = "Нэмэх";
            this.checkEditAppend.Size = new System.Drawing.Size(56, 19);
            this.checkEditAppend.TabIndex = 5;
            this.checkEditAppend.Visible = false;
            this.checkEditAppend.CheckedChanged += new System.EventHandler(this.checkEditAppend_CheckedChanged);
            // 
            // checkEditPrinting
            // 
            this.checkEditPrinting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditPrinting.Location = new System.Drawing.Point(908, 14);
            this.checkEditPrinting.Name = "checkEditPrinting";
            this.checkEditPrinting.Properties.Caption = "Хэвлэх";
            this.checkEditPrinting.Size = new System.Drawing.Size(58, 19);
            this.checkEditPrinting.TabIndex = 4;
            this.checkEditPrinting.Visible = false;
            this.checkEditPrinting.CheckedChanged += new System.EventHandler(this.checkEditPrinting_CheckedChanged);
            // 
            // checkEditEdit
            // 
            this.checkEditEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditEdit.Location = new System.Drawing.Point(780, 14);
            this.checkEditEdit.Name = "checkEditEdit";
            this.checkEditEdit.Properties.Caption = "Засах";
            this.checkEditEdit.Size = new System.Drawing.Size(55, 19);
            this.checkEditEdit.TabIndex = 3;
            this.checkEditEdit.Visible = false;
            this.checkEditEdit.CheckedChanged += new System.EventHandler(this.checkEditEdit_CheckedChanged);
            // 
            // checkEditRemove
            // 
            this.checkEditRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditRemove.Location = new System.Drawing.Point(841, 14);
            this.checkEditRemove.Name = "checkEditRemove";
            this.checkEditRemove.Properties.Caption = "Устгах";
            this.checkEditRemove.Size = new System.Drawing.Size(61, 19);
            this.checkEditRemove.TabIndex = 2;
            this.checkEditRemove.Visible = false;
            this.checkEditRemove.CheckedChanged += new System.EventHandler(this.checkEditRemove_CheckedChanged);
            // 
            // checkEditAll
            // 
            this.checkEditAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditAll.Location = new System.Drawing.Point(549, 14);
            this.checkEditAll.Name = "checkEditAll";
            this.checkEditAll.Properties.Caption = "Бүгдийг сонгох";
            this.checkEditAll.Size = new System.Drawing.Size(102, 19);
            this.checkEditAll.TabIndex = 1;
            this.checkEditAll.Visible = false;
            this.checkEditAll.CheckedChanged += new System.EventHandler(this.checkEditAll_CheckedChanged);
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 12);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonReload.TabIndex = 0;
            this.simpleButtonReload.Text = "Сэргээх";
            this.simpleButtonReload.ToolTip = "Тохиргооны мэдээллийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl.Location = new System.Drawing.Point(0, 49);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageRole;
            this.xtraTabControl.Size = new System.Drawing.Size(1007, 412);
            this.xtraTabControl.TabIndex = 2;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageRole,
            this.xtraTabPageWindow,
            this.xtraTabPageWindowRole,
            this.xtraTabPageIncType,
            this.xtraTabPageExpType,
            this.xtraTabPageMainConf,
            this.xtraTabPageSupplier,
            this.xtraTabPageMainDB,
            this.xtraTabPageLock,
            this.xtraTabReport});
            this.xtraTabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl_SelectedPageChanged);
            this.xtraTabControl.Click += new System.EventHandler(this.xtraTabControl_Click);
            // 
            // xtraTabPageRole
            // 
            this.xtraTabPageRole.Controls.Add(this.gridControlRole);
            this.xtraTabPageRole.Name = "xtraTabPageRole";
            this.xtraTabPageRole.Size = new System.Drawing.Size(1001, 384);
            this.xtraTabPageRole.Text = "Хэрэглэгчийн эрх";
            // 
            // gridControlRole
            // 
            this.gridControlRole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRole.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlRole.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlRole.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlRole.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlRole.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlRole.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, false, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, false, false, "", "save")});
            this.gridControlRole.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlRole_EmbeddedNavigator_ButtonClick);
            this.gridControlRole.Location = new System.Drawing.Point(0, 0);
            this.gridControlRole.MainView = this.gridViewRole;
            this.gridControlRole.Name = "gridControlRole";
            this.gridControlRole.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1});
            this.gridControlRole.Size = new System.Drawing.Size(1001, 384);
            this.gridControlRole.TabIndex = 1;
            this.gridControlRole.UseEmbeddedNavigator = true;
            this.gridControlRole.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRole});
            // 
            // gridViewRole
            // 
            this.gridViewRole.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnRoleId,
            this.gridColumnRoleName});
            this.gridViewRole.GridControl = this.gridControlRole;
            this.gridViewRole.Name = "gridViewRole";
            this.gridViewRole.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewRole.OptionsBehavior.ReadOnly = true;
            this.gridViewRole.OptionsCustomization.AllowGroup = false;
            this.gridViewRole.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewRole.OptionsFind.AllowFindPanel = false;
            this.gridViewRole.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewRole.OptionsView.ShowAutoFilterRow = true;
            this.gridViewRole.OptionsView.ShowGroupPanel = false;
            this.gridViewRole.EditFormPrepared += new DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventHandler(this.gridViewRole_EditFormPrepared);
            this.gridViewRole.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewRole_InvalidRowException);
            this.gridViewRole.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewRole_ValidateRow);
            this.gridViewRole.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewRole_RowUpdated);
            // 
            // gridColumnRoleId
            // 
            this.gridColumnRoleId.Caption = "Дугаар";
            this.gridColumnRoleId.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnRoleId.CustomizationCaption = "Дугаар";
            this.gridColumnRoleId.FieldName = "id";
            this.gridColumnRoleId.Name = "gridColumnRoleId";
            this.gridColumnRoleId.Visible = true;
            this.gridColumnRoleId.VisibleIndex = 0;
            this.gridColumnRoleId.Width = 79;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnRoleName
            // 
            this.gridColumnRoleName.Caption = "Нэр";
            this.gridColumnRoleName.CustomizationCaption = "Нэр";
            this.gridColumnRoleName.FieldName = "name";
            this.gridColumnRoleName.Name = "gridColumnRoleName";
            this.gridColumnRoleName.Visible = true;
            this.gridColumnRoleName.VisibleIndex = 1;
            this.gridColumnRoleName.Width = 679;
            // 
            // xtraTabPageWindow
            // 
            this.xtraTabPageWindow.Controls.Add(this.gridControlWindow);
            this.xtraTabPageWindow.Name = "xtraTabPageWindow";
            this.xtraTabPageWindow.Size = new System.Drawing.Size(1001, 384);
            this.xtraTabPageWindow.Text = "Програмын цонх";
            // 
            // gridControlWindow
            // 
            this.gridControlWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlWindow.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlWindow.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlWindow.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlWindow.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlWindow.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlWindow.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, false, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, false, false, "", "save")});
            this.gridControlWindow.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlWindow_EmbeddedNavigator_ButtonClick);
            this.gridControlWindow.Location = new System.Drawing.Point(0, 0);
            this.gridControlWindow.MainView = this.gridViewWindow;
            this.gridControlWindow.Name = "gridControlWindow";
            this.gridControlWindow.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit2});
            this.gridControlWindow.Size = new System.Drawing.Size(1001, 384);
            this.gridControlWindow.TabIndex = 1;
            this.gridControlWindow.UseEmbeddedNavigator = true;
            this.gridControlWindow.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWindow});
            // 
            // gridViewWindow
            // 
            this.gridViewWindow.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnWindowId,
            this.gridColumnWindowName,
            this.gridColumnWindowDesc});
            this.gridViewWindow.GridControl = this.gridControlWindow;
            this.gridViewWindow.Name = "gridViewWindow";
            this.gridViewWindow.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm;
            this.gridViewWindow.OptionsBehavior.ReadOnly = true;
            this.gridViewWindow.OptionsCustomization.AllowGroup = false;
            this.gridViewWindow.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewWindow.OptionsFind.AllowFindPanel = false;
            this.gridViewWindow.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewWindow.OptionsView.ShowAutoFilterRow = true;
            this.gridViewWindow.OptionsView.ShowGroupPanel = false;
            this.gridViewWindow.EditFormPrepared += new DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventHandler(this.gridViewWindow_EditFormPrepared);
            this.gridViewWindow.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewWindow_InvalidRowException);
            this.gridViewWindow.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewWindow_ValidateRow);
            this.gridViewWindow.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewWindow_RowUpdated);
            // 
            // gridColumnWindowId
            // 
            this.gridColumnWindowId.Caption = "Дугаар";
            this.gridColumnWindowId.ColumnEdit = this.repositoryItemSpinEdit2;
            this.gridColumnWindowId.CustomizationCaption = "Дугаар";
            this.gridColumnWindowId.FieldName = "id";
            this.gridColumnWindowId.Name = "gridColumnWindowId";
            this.gridColumnWindowId.Visible = true;
            this.gridColumnWindowId.VisibleIndex = 0;
            this.gridColumnWindowId.Width = 78;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // gridColumnWindowName
            // 
            this.gridColumnWindowName.Caption = "Нэр";
            this.gridColumnWindowName.CustomizationCaption = "Нэр";
            this.gridColumnWindowName.FieldName = "name";
            this.gridColumnWindowName.Name = "gridColumnWindowName";
            this.gridColumnWindowName.Visible = true;
            this.gridColumnWindowName.VisibleIndex = 1;
            this.gridColumnWindowName.Width = 338;
            // 
            // gridColumnWindowDesc
            // 
            this.gridColumnWindowDesc.Caption = "Тайлбар";
            this.gridColumnWindowDesc.CustomizationCaption = "Тайлбар";
            this.gridColumnWindowDesc.FieldName = "description";
            this.gridColumnWindowDesc.Name = "gridColumnWindowDesc";
            this.gridColumnWindowDesc.Visible = true;
            this.gridColumnWindowDesc.VisibleIndex = 2;
            this.gridColumnWindowDesc.Width = 342;
            // 
            // xtraTabPageWindowRole
            // 
            this.xtraTabPageWindowRole.Controls.Add(this.gridControlWindowRole);
            this.xtraTabPageWindowRole.Name = "xtraTabPageWindowRole";
            this.xtraTabPageWindowRole.Size = new System.Drawing.Size(1001, 384);
            this.xtraTabPageWindowRole.Text = "Цонхны эрх";
            // 
            // gridControlWindowRole
            // 
            this.gridControlWindowRole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlWindowRole.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlWindowRole.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlWindowRole.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlWindowRole.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlWindowRole.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlWindowRole.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "", "edit")});
            this.gridControlWindowRole.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlWindowRole_EmbeddedNavigator_ButtonClick);
            this.gridControlWindowRole.Location = new System.Drawing.Point(0, 0);
            this.gridControlWindowRole.MainView = this.gridViewWindowRole;
            this.gridControlWindowRole.Name = "gridControlWindowRole";
            this.gridControlWindowRole.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControlWindowRole.Size = new System.Drawing.Size(1001, 384);
            this.gridControlWindowRole.TabIndex = 1;
            this.gridControlWindowRole.UseEmbeddedNavigator = true;
            this.gridControlWindowRole.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWindowRole});
            // 
            // gridViewWindowRole
            // 
            this.gridViewWindowRole.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumnShow,
            this.gridColumnAppend,
            this.gridColumnEdit,
            this.gridColumnRemove,
            this.gridColumnPrinting});
            this.gridViewWindowRole.GridControl = this.gridControlWindowRole;
            this.gridViewWindowRole.Name = "gridViewWindowRole";
            this.gridViewWindowRole.OptionsBehavior.ReadOnly = true;
            this.gridViewWindowRole.OptionsCustomization.AllowGroup = false;
            this.gridViewWindowRole.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewWindowRole.OptionsFind.AllowFindPanel = false;
            this.gridViewWindowRole.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewWindowRole.OptionsView.ShowAutoFilterRow = true;
            this.gridViewWindowRole.OptionsView.ShowGroupPanel = false;
            this.gridViewWindowRole.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewWindowRole_CellValueChanging);
            this.gridViewWindowRole.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewWindowRole_InvalidRowException);
            this.gridViewWindowRole.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewWindowRole_ValidateRow);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Цонх";
            this.gridColumn1.CustomizationCaption = "Цонх";
            this.gridColumn1.FieldName = "windowName";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Эрх";
            this.gridColumn2.CustomizationCaption = "Эрх";
            this.gridColumn2.FieldName = "roleName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumnShow
            // 
            this.gridColumnShow.Caption = "Харах";
            this.gridColumnShow.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumnShow.CustomizationCaption = "Харах";
            this.gridColumnShow.FieldName = "show";
            this.gridColumnShow.Name = "gridColumnShow";
            this.gridColumnShow.Visible = true;
            this.gridColumnShow.VisibleIndex = 2;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEdit1_CheckedChanged);
            // 
            // gridColumnAppend
            // 
            this.gridColumnAppend.Caption = "Нэмэх";
            this.gridColumnAppend.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumnAppend.CustomizationCaption = "Нэмэх";
            this.gridColumnAppend.FieldName = "append";
            this.gridColumnAppend.Name = "gridColumnAppend";
            this.gridColumnAppend.Visible = true;
            this.gridColumnAppend.VisibleIndex = 3;
            // 
            // gridColumnEdit
            // 
            this.gridColumnEdit.Caption = "Засах";
            this.gridColumnEdit.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumnEdit.CustomizationCaption = "Засах";
            this.gridColumnEdit.FieldName = "edit";
            this.gridColumnEdit.Name = "gridColumnEdit";
            this.gridColumnEdit.Visible = true;
            this.gridColumnEdit.VisibleIndex = 4;
            // 
            // gridColumnRemove
            // 
            this.gridColumnRemove.Caption = "Устгах";
            this.gridColumnRemove.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumnRemove.CustomizationCaption = "Устгах";
            this.gridColumnRemove.FieldName = "remove";
            this.gridColumnRemove.Name = "gridColumnRemove";
            this.gridColumnRemove.Visible = true;
            this.gridColumnRemove.VisibleIndex = 5;
            // 
            // gridColumnPrinting
            // 
            this.gridColumnPrinting.Caption = "Хэвлэх";
            this.gridColumnPrinting.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumnPrinting.CustomizationCaption = "Хэвлэх";
            this.gridColumnPrinting.FieldName = "printing";
            this.gridColumnPrinting.Name = "gridColumnPrinting";
            this.gridColumnPrinting.Visible = true;
            this.gridColumnPrinting.VisibleIndex = 6;
            // 
            // xtraTabPageIncType
            // 
            this.xtraTabPageIncType.Controls.Add(this.gridControlIncType);
            this.xtraTabPageIncType.Name = "xtraTabPageIncType";
            this.xtraTabPageIncType.Size = new System.Drawing.Size(1001, 384);
            this.xtraTabPageIncType.Text = "Орлогын төрөл";
            // 
            // gridControlIncType
            // 
            this.gridControlIncType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlIncType.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlIncType.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlIncType.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlIncType.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlIncType.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlIncType.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, false, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, false, false, "", "save")});
            this.gridControlIncType.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlIncType_EmbeddedNavigator_ButtonClick);
            this.gridControlIncType.Location = new System.Drawing.Point(0, 0);
            this.gridControlIncType.MainView = this.gridViewIncType;
            this.gridControlIncType.Name = "gridControlIncType";
            this.gridControlIncType.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit3});
            this.gridControlIncType.Size = new System.Drawing.Size(1001, 384);
            this.gridControlIncType.TabIndex = 2;
            this.gridControlIncType.UseEmbeddedNavigator = true;
            this.gridControlIncType.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewIncType});
            // 
            // gridViewIncType
            // 
            this.gridViewIncType.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnIncTypeID,
            this.gridColumnIncTypeName,
            this.gridColumn3,
            this.gridColumnIncDescription,
            this.gridColumnIncIsUse,
            this.gridColumn9});
            this.gridViewIncType.GridControl = this.gridControlIncType;
            this.gridViewIncType.Name = "gridViewIncType";
            this.gridViewIncType.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm;
            this.gridViewIncType.OptionsBehavior.ReadOnly = true;
            this.gridViewIncType.OptionsCustomization.AllowGroup = false;
            this.gridViewIncType.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewIncType.OptionsFind.AllowFindPanel = false;
            this.gridViewIncType.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewIncType.OptionsView.ShowAutoFilterRow = true;
            this.gridViewIncType.OptionsView.ShowGroupPanel = false;
            this.gridViewIncType.EditFormPrepared += new DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventHandler(this.gridViewIncType_EditFormPrepared);
            this.gridViewIncType.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewIncType_InvalidRowException);
            this.gridViewIncType.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewIncType_ValidateRow);
            this.gridViewIncType.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewIncType_RowUpdated);
            // 
            // gridColumnIncTypeID
            // 
            this.gridColumnIncTypeID.Caption = "Дугаар";
            this.gridColumnIncTypeID.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumnIncTypeID.CustomizationCaption = "Дугаар";
            this.gridColumnIncTypeID.FieldName = "id";
            this.gridColumnIncTypeID.Name = "gridColumnIncTypeID";
            this.gridColumnIncTypeID.Visible = true;
            this.gridColumnIncTypeID.VisibleIndex = 0;
            this.gridColumnIncTypeID.Width = 59;
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit3.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit3.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit3.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // gridColumnIncTypeName
            // 
            this.gridColumnIncTypeName.Caption = "Нэр";
            this.gridColumnIncTypeName.CustomizationCaption = "Нэр";
            this.gridColumnIncTypeName.FieldName = "name";
            this.gridColumnIncTypeName.Name = "gridColumnIncTypeName";
            this.gridColumnIncTypeName.Visible = true;
            this.gridColumnIncTypeName.VisibleIndex = 1;
            this.gridColumnIncTypeName.Width = 158;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Данс";
            this.gridColumn3.CustomizationCaption = "Данс";
            this.gridColumn3.FieldName = "account";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 131;
            // 
            // gridColumnIncDescription
            // 
            this.gridColumnIncDescription.Caption = "Тайлбар";
            this.gridColumnIncDescription.CustomizationCaption = "Тайлбар";
            this.gridColumnIncDescription.FieldName = "description";
            this.gridColumnIncDescription.Name = "gridColumnIncDescription";
            this.gridColumnIncDescription.Visible = true;
            this.gridColumnIncDescription.VisibleIndex = 3;
            this.gridColumnIncDescription.Width = 243;
            // 
            // gridColumnIncIsUse
            // 
            this.gridColumnIncIsUse.Caption = "Ашиглах эсэх";
            this.gridColumnIncIsUse.CustomizationCaption = "Ашиглах эсэх";
            this.gridColumnIncIsUse.FieldName = "isUse";
            this.gridColumnIncIsUse.Name = "gridColumnIncIsUse";
            this.gridColumnIncIsUse.Visible = true;
            this.gridColumnIncIsUse.VisibleIndex = 5;
            this.gridColumnIncIsUse.Width = 92;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Үндсэн төрөл";
            this.gridColumn9.CustomizationCaption = "Үндсэн төрөл";
            this.gridColumn9.FieldName = "isDefault";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 4;
            this.gridColumn9.Width = 77;
            // 
            // xtraTabPageExpType
            // 
            this.xtraTabPageExpType.Controls.Add(this.gridControlExpType);
            this.xtraTabPageExpType.Name = "xtraTabPageExpType";
            this.xtraTabPageExpType.Size = new System.Drawing.Size(1001, 384);
            this.xtraTabPageExpType.Text = "Зарлагын төрөл";
            // 
            // gridControlExpType
            // 
            this.gridControlExpType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlExpType.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlExpType.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlExpType.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlExpType.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlExpType.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlExpType.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, false, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, false, false, "", "save")});
            this.gridControlExpType.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlExpType_EmbeddedNavigator_ButtonClick);
            this.gridControlExpType.Location = new System.Drawing.Point(0, 0);
            this.gridControlExpType.MainView = this.gridViewExpType;
            this.gridControlExpType.Name = "gridControlExpType";
            this.gridControlExpType.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit4});
            this.gridControlExpType.Size = new System.Drawing.Size(1001, 384);
            this.gridControlExpType.TabIndex = 3;
            this.gridControlExpType.UseEmbeddedNavigator = true;
            this.gridControlExpType.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExpType});
            // 
            // gridViewExpType
            // 
            this.gridViewExpType.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnExpTypeID,
            this.gridColumnExpTypeName,
            this.gridColumnExpTypeAccount,
            this.gridColumnExpDescription,
            this.gridColumnExpIsUse,
            this.gridColumn12});
            this.gridViewExpType.GridControl = this.gridControlExpType;
            this.gridViewExpType.Name = "gridViewExpType";
            this.gridViewExpType.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm;
            this.gridViewExpType.OptionsBehavior.ReadOnly = true;
            this.gridViewExpType.OptionsCustomization.AllowGroup = false;
            this.gridViewExpType.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewExpType.OptionsFind.AllowFindPanel = false;
            this.gridViewExpType.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewExpType.OptionsView.ShowAutoFilterRow = true;
            this.gridViewExpType.OptionsView.ShowGroupPanel = false;
            this.gridViewExpType.EditFormPrepared += new DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventHandler(this.gridViewExpType_EditFormPrepared);
            this.gridViewExpType.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewExpType_InvalidRowException);
            this.gridViewExpType.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewExpType_ValidateRow);
            this.gridViewExpType.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewExpType_RowUpdated);
            // 
            // gridColumnExpTypeID
            // 
            this.gridColumnExpTypeID.Caption = "Дугаар";
            this.gridColumnExpTypeID.ColumnEdit = this.repositoryItemSpinEdit4;
            this.gridColumnExpTypeID.CustomizationCaption = "Дугаар";
            this.gridColumnExpTypeID.FieldName = "id";
            this.gridColumnExpTypeID.Name = "gridColumnExpTypeID";
            this.gridColumnExpTypeID.Visible = true;
            this.gridColumnExpTypeID.VisibleIndex = 0;
            this.gridColumnExpTypeID.Width = 73;
            // 
            // repositoryItemSpinEdit4
            // 
            this.repositoryItemSpinEdit4.AutoHeight = false;
            this.repositoryItemSpinEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit4.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit4.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit4.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit4.Name = "repositoryItemSpinEdit4";
            // 
            // gridColumnExpTypeName
            // 
            this.gridColumnExpTypeName.Caption = "Нэр";
            this.gridColumnExpTypeName.CustomizationCaption = "Нэр";
            this.gridColumnExpTypeName.FieldName = "name";
            this.gridColumnExpTypeName.Name = "gridColumnExpTypeName";
            this.gridColumnExpTypeName.Visible = true;
            this.gridColumnExpTypeName.VisibleIndex = 1;
            this.gridColumnExpTypeName.Width = 194;
            // 
            // gridColumnExpTypeAccount
            // 
            this.gridColumnExpTypeAccount.Caption = "Данс";
            this.gridColumnExpTypeAccount.CustomizationCaption = "Данс";
            this.gridColumnExpTypeAccount.FieldName = "account";
            this.gridColumnExpTypeAccount.Name = "gridColumnExpTypeAccount";
            this.gridColumnExpTypeAccount.Visible = true;
            this.gridColumnExpTypeAccount.VisibleIndex = 2;
            this.gridColumnExpTypeAccount.Width = 145;
            // 
            // gridColumnExpDescription
            // 
            this.gridColumnExpDescription.Caption = "Тайлбар";
            this.gridColumnExpDescription.CustomizationCaption = "Тайлбар";
            this.gridColumnExpDescription.FieldName = "description";
            this.gridColumnExpDescription.Name = "gridColumnExpDescription";
            this.gridColumnExpDescription.Visible = true;
            this.gridColumnExpDescription.VisibleIndex = 3;
            this.gridColumnExpDescription.Width = 317;
            // 
            // gridColumnExpIsUse
            // 
            this.gridColumnExpIsUse.Caption = "Ашиглах эсэх";
            this.gridColumnExpIsUse.CustomizationCaption = "Ашиглах эсэх";
            this.gridColumnExpIsUse.FieldName = "isUse";
            this.gridColumnExpIsUse.Name = "gridColumnExpIsUse";
            this.gridColumnExpIsUse.Visible = true;
            this.gridColumnExpIsUse.VisibleIndex = 5;
            this.gridColumnExpIsUse.Width = 107;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Үндсэн төрөл";
            this.gridColumn12.CustomizationCaption = "Үндсэн төрөл";
            this.gridColumn12.FieldName = "isDefault";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            this.gridColumn12.Width = 95;
            // 
            // xtraTabPageMainConf
            // 
            this.xtraTabPageMainConf.Controls.Add(this.gridLookUpEdit1);
            this.xtraTabPageMainConf.Controls.Add(this.labelControl9);
            this.xtraTabPageMainConf.Controls.Add(this.textEditMedicineAccount);
            this.xtraTabPageMainConf.Controls.Add(this.labelControl8);
            this.xtraTabPageMainConf.Controls.Add(this.gridLookUpEditMedicineExpType);
            this.xtraTabPageMainConf.Controls.Add(this.labelControl7);
            this.xtraTabPageMainConf.Controls.Add(this.gridLookUpEditExpType);
            this.xtraTabPageMainConf.Controls.Add(this.labelControl11);
            this.xtraTabPageMainConf.Controls.Add(this.labelControl6);
            this.xtraTabPageMainConf.Controls.Add(this.checkedComboBoxEditRole);
            this.xtraTabPageMainConf.Controls.Add(this.labelControl5);
            this.xtraTabPageMainConf.Controls.Add(this.spinEditLimitYear);
            this.xtraTabPageMainConf.Controls.Add(this.simpleButtonBrowse);
            this.xtraTabPageMainConf.Controls.Add(this.textEditBackUpPath);
            this.xtraTabPageMainConf.Controls.Add(this.labelControl4);
            this.xtraTabPageMainConf.Controls.Add(this.checkEditIsUseStoreDrug);
            this.xtraTabPageMainConf.Controls.Add(this.labelControl3);
            this.xtraTabPageMainConf.Controls.Add(this.checkEditIsUseStoreOrder);
            this.xtraTabPageMainConf.Controls.Add(this.labelControl2);
            this.xtraTabPageMainConf.Controls.Add(this.gridLookUpEditAccount);
            this.xtraTabPageMainConf.Controls.Add(this.labelControl1);
            this.xtraTabPageMainConf.Controls.Add(this.simpleButtonSave);
            this.xtraTabPageMainConf.Name = "xtraTabPageMainConf";
            this.xtraTabPageMainConf.Size = new System.Drawing.Size(1001, 384);
            this.xtraTabPageMainConf.Text = "Ерөнхий тохиргоо";
            // 
            // gridLookUpEdit1
            // 
            this.gridLookUpEdit1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEdit1.EditValue = "";
            this.gridLookUpEdit1.Location = new System.Drawing.Point(436, 270);
            this.gridLookUpEdit1.Name = "gridLookUpEdit1";
            this.gridLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEdit1.Properties.DisplayMember = "fullName";
            this.gridLookUpEdit1.Properties.NullText = "";
            this.gridLookUpEdit1.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEdit1.Properties.ValueMember = "id";
            this.gridLookUpEdit1.Properties.View = this.gridView6;
            this.gridLookUpEdit1.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEdit1.TabIndex = 57;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ShowAutoFilterRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Дугаар";
            this.gridColumn20.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn20.FieldName = "id";
            this.gridColumn20.MinWidth = 75;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Овог";
            this.gridColumn21.CustomizationCaption = "Овог";
            this.gridColumn21.FieldName = "lastName";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Нэр";
            this.gridColumn22.CustomizationCaption = "Нэр";
            this.gridColumn22.FieldName = "firstName";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Регистер";
            this.gridColumn23.CustomizationCaption = "Регистер";
            this.gridColumn23.FieldName = "register";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Эрх";
            this.gridColumn24.CustomizationCaption = "Эрх";
            this.gridColumn24.FieldName = "roleName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Тасаг";
            this.gridColumn25.CustomizationCaption = "Тасаг";
            this.gridColumn25.FieldName = "wardName";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Ажлын газар";
            this.gridColumn26.CustomizationCaption = "Ажлын газар";
            this.gridColumn26.FieldName = "organizationName";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Албан тушаал";
            this.gridColumn27.CustomizationCaption = "Албан тушаал";
            this.gridColumn27.FieldName = "positionName";
            this.gridColumn27.MinWidth = 75;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 7;
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl9.Location = new System.Drawing.Point(306, 277);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(118, 13);
            this.labelControl9.TabIndex = 56;
            this.labelControl9.Text = "Гүйлгээ түгжих админ :";
            // 
            // textEditMedicineAccount
            // 
            this.textEditMedicineAccount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditMedicineAccount.Location = new System.Drawing.Point(436, 246);
            this.textEditMedicineAccount.Name = "textEditMedicineAccount";
            this.textEditMedicineAccount.Properties.MaxLength = 200;
            this.textEditMedicineAccount.Size = new System.Drawing.Size(200, 20);
            this.textEditMedicineAccount.TabIndex = 55;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(295, 249);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(129, 13);
            this.labelControl8.TabIndex = 54;
            this.labelControl8.Text = "Бараа материалын данс :";
            // 
            // gridLookUpEditMedicineExpType
            // 
            this.gridLookUpEditMedicineExpType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditMedicineExpType.EditValue = "";
            this.gridLookUpEditMedicineExpType.Location = new System.Drawing.Point(436, 220);
            this.gridLookUpEditMedicineExpType.Name = "gridLookUpEditMedicineExpType";
            this.gridLookUpEditMedicineExpType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMedicineExpType.Properties.DisplayMember = "name";
            this.gridLookUpEditMedicineExpType.Properties.NullText = "";
            this.gridLookUpEditMedicineExpType.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditMedicineExpType.Properties.ValueMember = "id";
            this.gridLookUpEditMedicineExpType.Properties.View = this.gridView5;
            this.gridLookUpEditMedicineExpType.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditMedicineExpType.TabIndex = 51;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn19});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ShowAutoFilterRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Нэр";
            this.gridColumn13.CustomizationCaption = "Орлогын төрлийн нэр";
            this.gridColumn13.FieldName = "name";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 0;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Тайлбар";
            this.gridColumn19.CustomizationCaption = "Орлогын төрлийн тайлбар";
            this.gridColumn19.FieldName = "description";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl7.Location = new System.Drawing.Point(295, 223);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(135, 13);
            this.labelControl7.TabIndex = 52;
            this.labelControl7.Text = "Бараа материалын төрөл :";
            // 
            // gridLookUpEditExpType
            // 
            this.gridLookUpEditExpType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditExpType.EditValue = "";
            this.gridLookUpEditExpType.Location = new System.Drawing.Point(436, 194);
            this.gridLookUpEditExpType.Name = "gridLookUpEditExpType";
            this.gridLookUpEditExpType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditExpType.Properties.DisplayMember = "name";
            this.gridLookUpEditExpType.Properties.NullText = "";
            this.gridLookUpEditExpType.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditExpType.Properties.ValueMember = "id";
            this.gridLookUpEditExpType.Properties.View = this.gridView3;
            this.gridLookUpEditExpType.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditExpType.TabIndex = 49;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Нэр";
            this.gridColumn5.CustomizationCaption = "Орлогын төрлийн нэр";
            this.gridColumn5.FieldName = "name";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тайлбар";
            this.gridColumn6.CustomizationCaption = "Орлогын төрлийн тайлбар";
            this.gridColumn6.FieldName = "description";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl11.Location = new System.Drawing.Point(272, 197);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(158, 13);
            this.labelControl11.TabIndex = 50;
            this.labelControl11.Text = "Эмийн түүвэр зарлагын төрөл :";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl6.Location = new System.Drawing.Point(367, 171);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(63, 13);
            this.labelControl6.TabIndex = 30;
            this.labelControl6.Text = "Батлах эрх :";
            // 
            // checkedComboBoxEditRole
            // 
            this.checkedComboBoxEditRole.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkedComboBoxEditRole.Location = new System.Drawing.Point(436, 168);
            this.checkedComboBoxEditRole.Name = "checkedComboBoxEditRole";
            this.checkedComboBoxEditRole.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEditRole.Properties.DisplayMember = "name";
            this.checkedComboBoxEditRole.Properties.ValueMember = "id";
            this.checkedComboBoxEditRole.Size = new System.Drawing.Size(200, 20);
            this.checkedComboBoxEditRole.TabIndex = 29;
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(284, 145);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(146, 13);
            this.labelControl5.TabIndex = 28;
            this.labelControl5.Text = "Гүйлгээ хийх хязгаар /жил/ :";
            // 
            // spinEditLimitYear
            // 
            this.spinEditLimitYear.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditLimitYear.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditLimitYear.Location = new System.Drawing.Point(436, 142);
            this.spinEditLimitYear.Name = "spinEditLimitYear";
            this.spinEditLimitYear.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditLimitYear.Properties.DisplayFormat.FormatString = "n0";
            this.spinEditLimitYear.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditLimitYear.Properties.EditFormat.FormatString = "n0";
            this.spinEditLimitYear.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditLimitYear.Properties.Mask.EditMask = "n0";
            this.spinEditLimitYear.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditLimitYear.Size = new System.Drawing.Size(100, 20);
            this.spinEditLimitYear.TabIndex = 15;
            // 
            // simpleButtonBrowse
            // 
            this.simpleButtonBrowse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.simpleButtonBrowse.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonBrowse.Image")));
            this.simpleButtonBrowse.Location = new System.Drawing.Point(642, 114);
            this.simpleButtonBrowse.Name = "simpleButtonBrowse";
            this.simpleButtonBrowse.Size = new System.Drawing.Size(28, 23);
            this.simpleButtonBrowse.TabIndex = 22;
            this.simpleButtonBrowse.Click += new System.EventHandler(this.simpleButtonBrowse_Click);
            // 
            // textEditBackUpPath
            // 
            this.textEditBackUpPath.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditBackUpPath.Location = new System.Drawing.Point(436, 116);
            this.textEditBackUpPath.Name = "textEditBackUpPath";
            this.textEditBackUpPath.Properties.MaxLength = 200;
            this.textEditBackUpPath.Size = new System.Drawing.Size(200, 20);
            this.textEditBackUpPath.TabIndex = 14;
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(341, 119);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(89, 13);
            this.labelControl4.TabIndex = 24;
            this.labelControl4.Text = "Байршуулах зам :";
            // 
            // checkEditIsUseStoreDrug
            // 
            this.checkEditIsUseStoreDrug.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkEditIsUseStoreDrug.Location = new System.Drawing.Point(436, 91);
            this.checkEditIsUseStoreDrug.Name = "checkEditIsUseStoreDrug";
            this.checkEditIsUseStoreDrug.Properties.Caption = "";
            this.checkEditIsUseStoreDrug.Size = new System.Drawing.Size(19, 19);
            this.checkEditIsUseStoreDrug.TabIndex = 13;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(286, 94);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(144, 13);
            this.labelControl3.TabIndex = 23;
            this.labelControl3.Text = "Эмийн түүвэр ашиглах эсэх :";
            // 
            // checkEditIsUseStoreOrder
            // 
            this.checkEditIsUseStoreOrder.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkEditIsUseStoreOrder.Location = new System.Drawing.Point(436, 66);
            this.checkEditIsUseStoreOrder.Name = "checkEditIsUseStoreOrder";
            this.checkEditIsUseStoreOrder.Properties.Caption = "";
            this.checkEditIsUseStoreOrder.Size = new System.Drawing.Size(19, 19);
            this.checkEditIsUseStoreOrder.TabIndex = 12;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(216, 69);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(214, 13);
            this.labelControl2.TabIndex = 15;
            this.labelControl2.Text = "Дугаар олгох, өвчний түүх ашиглах эсэх :";
            // 
            // gridLookUpEditAccount
            // 
            this.gridLookUpEditAccount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditAccount.EditValue = "";
            this.gridLookUpEditAccount.Location = new System.Drawing.Point(436, 40);
            this.gridLookUpEditAccount.Name = "gridLookUpEditAccount";
            this.gridLookUpEditAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditAccount.Properties.DisplayMember = "fullName";
            this.gridLookUpEditAccount.Properties.NullText = "";
            this.gridLookUpEditAccount.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditAccount.Properties.ValueMember = "id";
            this.gridLookUpEditAccount.Properties.View = this.gridView2;
            this.gridLookUpEditAccount.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditAccount.TabIndex = 11;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn18,
            this.gridColumn4,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Дугаар";
            this.gridColumn18.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn18.FieldName = "id";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Овог";
            this.gridColumn4.CustomizationCaption = "Овог";
            this.gridColumn4.FieldName = "lastName";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Нэр";
            this.gridColumn10.CustomizationCaption = "Нэр";
            this.gridColumn10.FieldName = "firstName";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Регистер";
            this.gridColumn11.CustomizationCaption = "Регистер";
            this.gridColumn11.FieldName = "register";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Эрх";
            this.gridColumn14.CustomizationCaption = "Эрх";
            this.gridColumn14.FieldName = "roleName";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 4;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Тасаг";
            this.gridColumn15.CustomizationCaption = "Тасаг";
            this.gridColumn15.FieldName = "wardName";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 5;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Ажлын газар";
            this.gridColumn16.CustomizationCaption = "Ажлын газар";
            this.gridColumn16.FieldName = "organizationName";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 6;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Албан тушаал";
            this.gridColumn17.CustomizationCaption = "Албан тушаал";
            this.gridColumn17.FieldName = "positionName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(291, 43);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(139, 13);
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "Данс тохируулах нягтлан :";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.simpleButtonSave.Location = new System.Drawing.Point(561, 315);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 21;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // xtraTabPageSupplier
            // 
            this.xtraTabPageSupplier.Controls.Add(this.gridControlSupplier);
            this.xtraTabPageSupplier.Name = "xtraTabPageSupplier";
            this.xtraTabPageSupplier.Size = new System.Drawing.Size(1001, 384);
            this.xtraTabPageSupplier.Text = "Бэлтгэн нийлүүлэгч";
            // 
            // gridControlSupplier
            // 
            this.gridControlSupplier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSupplier.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlSupplier.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlSupplier.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlSupplier.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlSupplier.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlSupplier.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlSupplier.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlProducer_EmbeddedNavigator_ButtonClick);
            this.gridControlSupplier.Location = new System.Drawing.Point(0, 0);
            this.gridControlSupplier.MainView = this.gridViewSupplier;
            this.gridControlSupplier.Name = "gridControlSupplier";
            this.gridControlSupplier.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemGridLookUpEditCountry});
            this.gridControlSupplier.Size = new System.Drawing.Size(1001, 384);
            this.gridControlSupplier.TabIndex = 4;
            this.gridControlSupplier.UseEmbeddedNavigator = true;
            this.gridControlSupplier.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSupplier});
            // 
            // gridViewSupplier
            // 
            this.gridViewSupplier.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnSupplier,
            this.gridColumnConNo});
            this.gridViewSupplier.GridControl = this.gridControlSupplier;
            this.gridViewSupplier.Name = "gridViewSupplier";
            this.gridViewSupplier.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewSupplier.OptionsBehavior.ReadOnly = true;
            this.gridViewSupplier.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewSupplier.OptionsFind.AllowFindPanel = false;
            this.gridViewSupplier.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewSupplier.OptionsView.ShowAutoFilterRow = true;
            this.gridViewSupplier.OptionsView.ShowGroupPanel = false;
            this.gridViewSupplier.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewSupplier_InvalidRowException);
            this.gridViewSupplier.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewSupplier_ValidateRow);
            this.gridViewSupplier.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewSupplier_RowUpdated);
            // 
            // gridColumnSupplier
            // 
            this.gridColumnSupplier.Caption = "Бэлтгэн нийлүүлэгч";
            this.gridColumnSupplier.CustomizationCaption = "Бэлтгэн нийлүүлэгч";
            this.gridColumnSupplier.FieldName = "name";
            this.gridColumnSupplier.Name = "gridColumnSupplier";
            this.gridColumnSupplier.OptionsEditForm.Caption = "Нэр";
            this.gridColumnSupplier.Visible = true;
            this.gridColumnSupplier.VisibleIndex = 0;
            // 
            // gridColumnConNo
            // 
            this.gridColumnConNo.Caption = "Холбогдох дугаар";
            this.gridColumnConNo.CustomizationCaption = "Холбогдох дугаар";
            this.gridColumnConNo.FieldName = "conNo";
            this.gridColumnConNo.Name = "gridColumnConNo";
            this.gridColumnConNo.OptionsEditForm.Caption = "Дугаар";
            this.gridColumnConNo.Visible = true;
            this.gridColumnConNo.VisibleIndex = 1;
            // 
            // repositoryItemGridLookUpEditCountry
            // 
            this.repositoryItemGridLookUpEditCountry.AutoHeight = false;
            this.repositoryItemGridLookUpEditCountry.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditCountry.DisplayMember = "name";
            this.repositoryItemGridLookUpEditCountry.Name = "repositoryItemGridLookUpEditCountry";
            this.repositoryItemGridLookUpEditCountry.NullText = "";
            this.repositoryItemGridLookUpEditCountry.ValueMember = "id";
            this.repositoryItemGridLookUpEditCountry.View = this.gridView1;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Улсын жагсаалт";
            this.gridColumn7.FieldName = "name";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            // 
            // xtraTabPageMainDB
            // 
            this.xtraTabPageMainDB.Controls.Add(this.gridControlMainDB);
            this.xtraTabPageMainDB.Name = "xtraTabPageMainDB";
            this.xtraTabPageMainDB.Size = new System.Drawing.Size(1001, 384);
            this.xtraTabPageMainDB.Text = "Өгөгдлийн сан";
            // 
            // gridControlMainDB
            // 
            this.gridControlMainDB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMainDB.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlMainDB.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlMainDB.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlMainDB.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlMainDB.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlMainDB.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Одоогийн өгөгдлийн санг солих", "edit")});
            this.gridControlMainDB.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlMainDB_EmbeddedNavigator_ButtonClick);
            this.gridControlMainDB.Location = new System.Drawing.Point(0, 0);
            this.gridControlMainDB.MainView = this.gridViewMainDB;
            this.gridControlMainDB.Name = "gridControlMainDB";
            this.gridControlMainDB.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemGridLookUpEdit1});
            this.gridControlMainDB.Size = new System.Drawing.Size(1001, 384);
            this.gridControlMainDB.TabIndex = 5;
            this.gridControlMainDB.UseEmbeddedNavigator = true;
            this.gridControlMainDB.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMainDB});
            this.gridControlMainDB.Click += new System.EventHandler(this.gridControlMainDB_Click);
            // 
            // gridViewMainDB
            // 
            this.gridViewMainDB.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnDBName,
            this.gridColumnBegBalDate,
            this.gridColumnIsCurrDB});
            this.gridViewMainDB.GridControl = this.gridControlMainDB;
            this.gridViewMainDB.Name = "gridViewMainDB";
            this.gridViewMainDB.OptionsBehavior.ReadOnly = true;
            this.gridViewMainDB.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewMainDB.OptionsFind.AllowFindPanel = false;
            this.gridViewMainDB.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewMainDB.OptionsView.ShowAutoFilterRow = true;
            this.gridViewMainDB.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnDBName
            // 
            this.gridColumnDBName.Caption = "Бааз";
            this.gridColumnDBName.CustomizationCaption = "Бааз";
            this.gridColumnDBName.FieldName = "name";
            this.gridColumnDBName.Name = "gridColumnDBName";
            this.gridColumnDBName.OptionsEditForm.Caption = "Нэр";
            this.gridColumnDBName.Visible = true;
            this.gridColumnDBName.VisibleIndex = 0;
            this.gridColumnDBName.Width = 474;
            // 
            // gridColumnBegBalDate
            // 
            this.gridColumnBegBalDate.Caption = "Огноо";
            this.gridColumnBegBalDate.CustomizationCaption = "Огноо";
            this.gridColumnBegBalDate.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.gridColumnBegBalDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumnBegBalDate.FieldName = "begBalDate";
            this.gridColumnBegBalDate.Name = "gridColumnBegBalDate";
            this.gridColumnBegBalDate.OptionsEditForm.Caption = "Дугаар";
            this.gridColumnBegBalDate.Visible = true;
            this.gridColumnBegBalDate.VisibleIndex = 1;
            this.gridColumnBegBalDate.Width = 274;
            // 
            // gridColumnIsCurrDB
            // 
            this.gridColumnIsCurrDB.Caption = "Ашиглагдаж байгаа";
            this.gridColumnIsCurrDB.CustomizationCaption = "Одоо ашиглагдаж байгаа";
            this.gridColumnIsCurrDB.FieldName = "isCurrDB";
            this.gridColumnIsCurrDB.Name = "gridColumnIsCurrDB";
            this.gridColumnIsCurrDB.Visible = true;
            this.gridColumnIsCurrDB.VisibleIndex = 2;
            this.gridColumnIsCurrDB.Width = 183;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DisplayMember = "name";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.ValueMember = "id";
            this.repositoryItemGridLookUpEdit1.View = this.gridView4;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Улсын жагсаалт";
            this.gridColumn8.FieldName = "name";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            // 
            // xtraTabPageLock
            // 
            this.xtraTabPageLock.Controls.Add(this.gridControlLock);
            this.xtraTabPageLock.Name = "xtraTabPageLock";
            this.xtraTabPageLock.Size = new System.Drawing.Size(1001, 384);
            this.xtraTabPageLock.Text = "Гүйлгээ түгжих";
            this.xtraTabPageLock.Paint += new System.Windows.Forms.PaintEventHandler(this.xtraTabPageLock_Paint);
            // 
            // gridControlLock
            // 
            this.gridControlLock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLock.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlLock.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlLock.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlLock.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlLock.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlLock.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlLock.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlLock_EmbeddedNavigator_ButtonClick);
            this.gridControlLock.Location = new System.Drawing.Point(0, 0);
            this.gridControlLock.MainView = this.gridViewLock;
            this.gridControlLock.Name = "gridControlLock";
            this.gridControlLock.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemGridLookUpEdit2});
            this.gridControlLock.Size = new System.Drawing.Size(1001, 384);
            this.gridControlLock.TabIndex = 5;
            this.gridControlLock.UseEmbeddedNavigator = true;
            this.gridControlLock.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLock});
            this.gridControlLock.Click += new System.EventHandler(this.gridControlLock_Click);
            // 
            // gridViewLock
            // 
            this.gridViewLock.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn28,
            this.gridColumn29});
            this.gridViewLock.GridControl = this.gridControlLock;
            this.gridViewLock.Name = "gridViewLock";
            this.gridViewLock.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewLock.OptionsBehavior.ReadOnly = true;
            this.gridViewLock.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewLock.OptionsFind.AllowFindPanel = false;
            this.gridViewLock.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewLock.OptionsView.ShowAutoFilterRow = true;
            this.gridViewLock.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Эхлэх хугацаа";
            this.gridColumn28.CustomizationCaption = "Эхлэх хугацаа";
            this.gridColumn28.FieldName = "startDate";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsEditForm.Caption = "Нэр";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 0;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Дуусах хугацаа";
            this.gridColumn29.CustomizationCaption = "Дуусах хугацаа";
            this.gridColumn29.FieldName = "endDate";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsEditForm.Caption = "Дугаар";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 1;
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.DisplayMember = "name";
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.NullText = "";
            this.repositoryItemGridLookUpEdit2.ValueMember = "id";
            this.repositoryItemGridLookUpEdit2.View = this.gridView8;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn30});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ShowAutoFilterRow = true;
            this.gridView8.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Улсын жагсаалт";
            this.gridColumn30.FieldName = "name";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 0;
            // 
            // xtraTabReport
            // 
            this.xtraTabReport.Controls.Add(this.richTextBox1);
            this.xtraTabReport.Controls.Add(this.labelControl14);
            this.xtraTabReport.Controls.Add(this.labelControl12);
            this.xtraTabReport.Controls.Add(this.textName3);
            this.xtraTabReport.Controls.Add(this.textName2);
            this.xtraTabReport.Controls.Add(this.labelControl15);
            this.xtraTabReport.Controls.Add(this.labelControl16);
            this.xtraTabReport.Controls.Add(this.labelControl17);
            this.xtraTabReport.Controls.Add(this.textName1);
            this.xtraTabReport.Controls.Add(this.textEmploy3);
            this.xtraTabReport.Controls.Add(this.textEmploy2);
            this.xtraTabReport.Controls.Add(this.labelControl10);
            this.xtraTabReport.Controls.Add(this.textEmploy1);
            this.xtraTabReport.Controls.Add(this.labelControl13);
            this.xtraTabReport.Controls.Add(this.labelControl20);
            this.xtraTabReport.Controls.Add(this.saveReport);
            this.xtraTabReport.Name = "xtraTabReport";
            this.xtraTabReport.Size = new System.Drawing.Size(1001, 384);
            this.xtraTabReport.Text = "Тайлан тохиргоо";
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl14.Location = new System.Drawing.Point(192, 277);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(76, 13);
            this.labelControl14.TabIndex = 91;
            this.labelControl14.Text = "Албан тушаал:";
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl12.Location = new System.Drawing.Point(192, 241);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(76, 13);
            this.labelControl12.TabIndex = 90;
            this.labelControl12.Text = "Албан тушаал:";
            // 
            // textName3
            // 
            this.textName3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textName3.Location = new System.Drawing.Point(610, 274);
            this.textName3.Name = "textName3";
            this.textName3.Properties.MaxLength = 200;
            this.textName3.Size = new System.Drawing.Size(200, 20);
            this.textName3.TabIndex = 89;
            // 
            // textName2
            // 
            this.textName2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textName2.Location = new System.Drawing.Point(610, 238);
            this.textName2.Name = "textName2";
            this.textName2.Properties.MaxLength = 200;
            this.textName2.Size = new System.Drawing.Size(200, 20);
            this.textName2.TabIndex = 88;
            // 
            // labelControl15
            // 
            this.labelControl15.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl15.Location = new System.Drawing.Point(548, 277);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(22, 13);
            this.labelControl15.TabIndex = 87;
            this.labelControl15.Text = "Нэр:";
            // 
            // labelControl16
            // 
            this.labelControl16.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl16.Location = new System.Drawing.Point(548, 241);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(22, 13);
            this.labelControl16.TabIndex = 86;
            this.labelControl16.Text = "Нэр:";
            // 
            // labelControl17
            // 
            this.labelControl17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl17.Location = new System.Drawing.Point(548, 209);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(22, 13);
            this.labelControl17.TabIndex = 85;
            this.labelControl17.Text = "Нэр:";
            // 
            // textName1
            // 
            this.textName1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textName1.Location = new System.Drawing.Point(610, 206);
            this.textName1.Name = "textName1";
            this.textName1.Properties.MaxLength = 200;
            this.textName1.Size = new System.Drawing.Size(200, 20);
            this.textName1.TabIndex = 84;
            // 
            // textEmploy3
            // 
            this.textEmploy3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEmploy3.Location = new System.Drawing.Point(306, 274);
            this.textEmploy3.Name = "textEmploy3";
            this.textEmploy3.Properties.MaxLength = 200;
            this.textEmploy3.Size = new System.Drawing.Size(200, 20);
            this.textEmploy3.TabIndex = 83;
            // 
            // textEmploy2
            // 
            this.textEmploy2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEmploy2.Location = new System.Drawing.Point(306, 238);
            this.textEmploy2.Name = "textEmploy2";
            this.textEmploy2.Properties.MaxLength = 200;
            this.textEmploy2.Size = new System.Drawing.Size(200, 20);
            this.textEmploy2.TabIndex = 82;
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl10.Location = new System.Drawing.Point(192, 209);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(76, 13);
            this.labelControl10.TabIndex = 79;
            this.labelControl10.Text = "Албан тушаал:";
            // 
            // textEmploy1
            // 
            this.textEmploy1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEmploy1.Location = new System.Drawing.Point(306, 206);
            this.textEmploy1.Name = "textEmploy1";
            this.textEmploy1.Properties.MaxLength = 200;
            this.textEmploy1.Size = new System.Drawing.Size(200, 20);
            this.textEmploy1.TabIndex = 77;
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl13.Location = new System.Drawing.Point(192, 169);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(80, 13);
            this.labelControl13.TabIndex = 75;
            this.labelControl13.Text = "Тайлангийн хөл";
            // 
            // labelControl20
            // 
            this.labelControl20.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl20.Location = new System.Drawing.Point(192, 45);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(97, 13);
            this.labelControl20.TabIndex = 61;
            this.labelControl20.Text = "Тайлангийн толгой";
            // 
            // saveReport
            // 
            this.saveReport.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.saveReport.Location = new System.Drawing.Point(447, 326);
            this.saveReport.Name = "saveReport";
            this.saveReport.Size = new System.Drawing.Size(75, 23);
            this.saveReport.TabIndex = 65;
            this.saveReport.Text = "Хадгалах";
            this.saveReport.ToolTip = "Өгөгдлийг хадгалах";
            this.saveReport.Click += new System.EventHandler(this.saveReport_Click);
            // 
            // informationDataSet1
            // 
            this.informationDataSet1.DataSetName = "InformationDataSet";
            this.informationDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // informationDataSet2
            // 
            this.informationDataSet2.DataSetName = "InformationDataSet";
            this.informationDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // loginUserInfoTableAdapter1
            // 
            this.loginUserInfoTableAdapter1.ClearBeforeFill = true;
            // 
            // loginUserInfoTableAdapter2
            // 
            this.loginUserInfoTableAdapter2.ClearBeforeFill = true;
            // 
            // loginUserInfoTableAdapter3
            // 
            this.loginUserInfoTableAdapter3.ClearBeforeFill = true;
            // 
            // expTypeOtherTableAdapter1
            // 
            this.expTypeOtherTableAdapter1.ClearBeforeFill = true;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Тайлангийн төрөл";
            this.gridColumn34.CustomizationCaption = "Тайлангийн төрөл";
            this.gridColumn34.FieldName = "reportType";
            this.gridColumn34.Name = "gridColumn34";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(192, 75);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(618, 73);
            this.richTextBox1.TabIndex = 92;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged_1);
            // 
            // WindowRoleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 461);
            this.Controls.Add(this.xtraTabControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "WindowRoleForm";
            this.ShowInTaskbar = false;
            this.Text = "Програмын тохиргоо";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WindowRoleForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditShow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAppend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPrinting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditRemove.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageRole.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            this.xtraTabPageWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            this.xtraTabPageWindowRole.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWindowRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWindowRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.xtraTabPageIncType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlIncType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewIncType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            this.xtraTabPageExpType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExpType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExpType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).EndInit();
            this.xtraTabPageMainConf.ResumeLayout(false);
            this.xtraTabPageMainConf.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMedicineAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicineExpType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditExpType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEditRole.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditLimitYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBackUpPath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsUseStoreDrug.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsUseStoreOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.xtraTabPageSupplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.xtraTabPageMainDB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMainDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMainDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.xtraTabPageLock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            this.xtraTabReport.ResumeLayout(false);
            this.xtraTabReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textName3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textName2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textName1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEmploy3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEmploy2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEmploy1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.informationDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.informationDataSet2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageRole;
        private DevExpress.XtraGrid.GridControl gridControlRole;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRole;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRoleId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRoleName;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWindowRole;
        private DevExpress.XtraGrid.GridControl gridControlWindowRole;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWindowRole;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnShow;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnAppend;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRemove;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPrinting;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWindow;
        private DevExpress.XtraGrid.GridControl gridControlWindow;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWindow;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnWindowId;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnWindowName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnWindowDesc;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEditAll;
        private DevExpress.XtraEditors.CheckEdit checkEditShow;
        private DevExpress.XtraEditors.CheckEdit checkEditAppend;
        private DevExpress.XtraEditors.CheckEdit checkEditPrinting;
        private DevExpress.XtraEditors.CheckEdit checkEditEdit;
        private DevExpress.XtraEditors.CheckEdit checkEditRemove;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageIncType;
        private DevExpress.XtraGrid.GridControl gridControlIncType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewIncType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIncTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIncTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIncDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIncIsUse;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageExpType;
        private DevExpress.XtraGrid.GridControl gridControlExpType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewExpType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnExpTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnExpTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnExpTypeAccount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnExpDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnExpIsUse;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageMainConf;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditAccount;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.CheckEdit checkEditIsUseStoreDrug;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.CheckEdit checkEditIsUseStoreOrder;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageSupplier;
        private DevExpress.XtraGrid.GridControl gridControlSupplier;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSupplier;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSupplier;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConNo;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditCountry;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.TextEdit textEditBackUpPath;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButtonBrowse;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageMainDB;
        private DevExpress.XtraGrid.GridControl gridControlMainDB;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMainDB;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDBName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBegBalDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIsCurrDB;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SpinEdit spinEditLimitYear;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEditRole;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditExpType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMedicineExpType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEditMedicineAccount;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.SimpleButton lockButton;
        private ds.InformationDataSet informationDataSet1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private ds.InformationDataSet informationDataSet2;
        private ds.InformationDataSetTableAdapters.LoginUserInfoTableAdapter loginUserInfoTableAdapter1;
        private ds.InformationDataSetTableAdapters.LoginUserInfoTableAdapter loginUserInfoTableAdapter2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLock;
        private DevExpress.XtraGrid.GridControl gridControlLock;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLock;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private ds.InformationDataSetTableAdapters.LoginUserInfoTableAdapter loginUserInfoTableAdapter3;
        private ds.InformationDataSetTableAdapters.ExpTypeOtherTableAdapter expTypeOtherTableAdapter1;
        private DevExpress.XtraTab.XtraTabPage xtraTabReport;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit textName3;
        private DevExpress.XtraEditors.TextEdit textName2;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit textName1;
        private DevExpress.XtraEditors.TextEdit textEmploy3;
        private DevExpress.XtraEditors.TextEdit textEmploy2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit textEmploy1;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.SimpleButton saveReport;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private System.Windows.Forms.RichTextBox richTextBox1;

    }
}