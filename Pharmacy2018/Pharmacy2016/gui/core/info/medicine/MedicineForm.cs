﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.info.ds;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraBars.Docking;

namespace Pharmacy2016.gui.core.info.medicine
{
    /*
     * Эмийн ангилал, эмийн олон улсын ангилал, эмийн бүртгэлийг харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class MedicineForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх үүсгэх функц.

            private static MedicineForm INSTANCE = new MedicineForm();

            public static MedicineForm Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private readonly int FORM_ID = 3003;

            private bool isExpandTree = false;

            public DataRowView currentMedicineView;

            public DataRowView currentMedicineTypeView;

            public DataRowView currentMedicineTypeInterView;

            private System.IO.Stream layout_medicine = null;
            private System.IO.Stream layout_country = null;
            private System.IO.Stream layout_shape = null;
            private System.IO.Stream layout_unit = null;
            private System.IO.Stream layout_producer = null;
            private System.IO.Stream layout_quantity = null;

            private System.IO.Stream layout_package = null;
            private System.IO.Stream layout_packagedetial = null;
            private System.IO.Stream layout_packagemedicine = null;
            private MedicineForm()
            {
            
            }
            
            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initLayout();
                RoleUtil.addForm(FORM_ID, this);

                treeListMedicineType.DataSource = UserDataSet.MedicineTypeBindingSource;
                gridControlMedicine.DataSource = UserDataSet.MedicineBindingSource;
                treeListMedicineTypeInter.DataSource = UserDataSet.MedicineTypeInternationalBindingSource;

                //Эмийн лавлах
                gridControlCountry.DataSource = UserDataSet.ProducerCountryBindingSource;
                gridControlShape.DataSource = UserDataSet.MedicineShapeBindingSource;
                gridControlUnit.DataSource = UserDataSet.MedicineUnitBindingSource;
                gridControlQuantity.DataSource = UserDataSet.MedicineQuantityBindingSource;
                gridControlProducer.DataSource = UserDataSet.ProducerBindingSource;
                repositoryItemGridLookUpEditCountry.DataSource = UserDataSet.ProducerCountryBindingSource;

                //Багц
                gridControlPackage.DataSource = UserDataSet.PackageBindingSource;
                gridControlPackageMedicine.DataSource = UserDataSet.PackageMedicineBindingSource;
            }

            private void initLayout()
            {
                layout_medicine = new System.IO.MemoryStream();
                gridViewMedicine.SaveLayoutToStream(layout_medicine);
                layout_medicine.Seek(0, System.IO.SeekOrigin.Begin);

                layout_country = new System.IO.MemoryStream();
                gridViewCountry.SaveLayoutToStream(layout_country);
                layout_country.Seek(0, System.IO.SeekOrigin.Begin);

                layout_shape = new System.IO.MemoryStream();
                gridViewShape.SaveLayoutToStream(layout_shape);
                layout_shape.Seek(0, System.IO.SeekOrigin.Begin);

                layout_unit = new System.IO.MemoryStream();
                gridViewUnit.SaveLayoutToStream(layout_unit);
                layout_unit.Seek(0, System.IO.SeekOrigin.Begin);

                layout_quantity = new System.IO.MemoryStream();
                gridViewQuantity.SaveLayoutToStream(layout_quantity);
                layout_quantity.Seek(0, System.IO.SeekOrigin.Begin);

                layout_producer = new System.IO.MemoryStream();
                gridViewProducer.SaveLayoutToStream(layout_producer);
                layout_producer.Seek(0, System.IO.SeekOrigin.Begin);

                layout_package = new System.IO.MemoryStream();
                gridViewPackage.SaveLayoutToStream(layout_package);
                layout_package.Seek(0, System.IO.SeekOrigin.Begin);

                layout_packagedetial = new System.IO.MemoryStream();
                gridViewPackageDetial.SaveLayoutToStream(layout_packagedetial);
                layout_packagedetial.Seek(0, System.IO.SeekOrigin.Begin);

                layout_packagemedicine = new System.IO.MemoryStream();
                gridViewPackageMedicine.SaveLayoutToStream(layout_packagemedicine);
                layout_packagemedicine.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion

        #region Форм хаах, нээх функцууд.

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        checkRole();
                        setDefaultMed();
                        setDefaultType();
                        setDefaultInter();

                        //Эмийн мэдээлэл.
                        UserDataSet.Instance.Producer.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.Producer.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.ProducerCountry.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.MedicineShape.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.MedicineShape.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.MedicineUnit.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.MedicineUnit.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.MedicineQuantity.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.MedicineQuantity.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        //Эмийн багц
                        UserDataSet.Instance.Package.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.Package.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.PackageDetial.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.PackageDetial.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        UserDataSet.Instance.MedicineRelatives.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.MedicineRelatives.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        UserDataSet.Instance.PackageMedicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();                        
                        

                        reload();
                        reloadRef(false);
                        reloadPackage(false);
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх ачааллахад алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void MedicineForm_Shown(object sender, EventArgs e)
            {
                gridColumn16.VisibleIndex = 2;
                tabbedView1.ActivateDocument(dockPanelMedic.FloatForm);
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                gridControlMedicine.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlMedicine.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridControlMedicine.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                dropDownButtonPrint.Visible = !SystemUtil.SELECTED_DB_READONLY && roles[4];

                controlNavigatorMedicineType.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                controlNavigatorMedicineType.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                controlNavigatorMedicineType.Buttons.CustomButtons[3].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];

                controlNavigatorInternational.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                controlNavigatorInternational.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                controlNavigatorInternational.Buttons.CustomButtons[3].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];

                //Эмийн мэдээлэл.
                gridControlCountry.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlCountry.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridViewCountry.OptionsBehavior.ReadOnly = !SystemUtil.SELECTED_DB_READONLY && !roles[2];

                gridControlProducer.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlProducer.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridViewProducer.OptionsBehavior.ReadOnly = !SystemUtil.SELECTED_DB_READONLY && !roles[2];

                gridControlUnit.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlUnit.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridViewUnit.OptionsBehavior.ReadOnly = !SystemUtil.SELECTED_DB_READONLY && !roles[2];

                gridControlShape.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlShape.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridViewShape.OptionsBehavior.ReadOnly = !SystemUtil.SELECTED_DB_READONLY && !roles[2];

                gridControlQuantity.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlQuantity.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridViewQuantity.OptionsBehavior.ReadOnly = !SystemUtil.SELECTED_DB_READONLY && !roles[2];

                simpleButtonConvert.Visible = roles[4];
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;
                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("medicine");
                if (layout != null)
                {
                    gridViewMedicine.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewMedicine.RestoreLayoutFromStream(layout_medicine);
                    layout_medicine.Seek(0, System.IO.SeekOrigin.Begin);
                }

                layout = UserUtil.getLayout("producerCountry");
                if (layout != null)
                {
                    gridViewCountry.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewCountry.RestoreLayoutFromStream(layout_country);
                    layout_country.Seek(0, System.IO.SeekOrigin.Begin);
                }

                layout = UserUtil.getLayout("producer");
                if (layout != null)
                {
                    gridViewProducer.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewProducer.RestoreLayoutFromStream(layout_producer);
                    layout_producer.Seek(0, System.IO.SeekOrigin.Begin);
                }

                layout = UserUtil.getLayout("unit");
                if (layout != null)
                {
                    gridViewUnit.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewUnit.RestoreLayoutFromStream(layout_unit);
                    layout_unit.Seek(0, System.IO.SeekOrigin.Begin);
                }

                layout = UserUtil.getLayout("shape");
                if (layout != null)
                {
                    gridViewShape.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewShape.RestoreLayoutFromStream(layout_shape);
                    layout_shape.Seek(0, System.IO.SeekOrigin.Begin);
                }
                layout = UserUtil.getLayout("packagemedicine");
                if (layout != null)
                {
                    gridViewPackageMedicine.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewPackageMedicine.RestoreLayoutFromStream(layout_packagemedicine);
                    layout_packagemedicine.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }
            
            private void MedicineForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                UserDataSet.Instance.MedicineType.Clear();
                UserDataSet.Instance.MedicineTypeInternational.Clear();
                UserDataSet.Instance.Medicine.Clear();

                //Эмийн мэдээлэл.
                UserDataSet.Instance.ProducerCountry.Clear();
                UserDataSet.Instance.Producer.Clear();
                UserDataSet.Instance.MedicineShape.Clear();
                UserDataSet.Instance.MedicineUnit.Clear();
                UserDataSet.Instance.MedicineQuantity.Clear();

                //Багц
                UserDataSet.Instance.Package.Clear();
                UserDataSet.Instance.PackageMedicine.Clear();
            }

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                UserDataSet.MedicineTypeTableAdapter.Fill(UserDataSet.Instance.MedicineType, HospitalUtil.getHospitalId());
                UserDataSet.MedicineTypeInternationalTableAdapter.Fill(UserDataSet.Instance.MedicineTypeInternational, HospitalUtil.getHospitalId());
                UserDataSet.MedicineTableAdapter.Fill(UserDataSet.Instance.Medicine, HospitalUtil.getHospitalId());
                UserDataSet.MedicineRelativesTableAdapter.Fill(UserDataSet.Instance.MedicineRelatives, HospitalUtil.getHospitalId());
                UserDataSet.PackageMedicineTableAdapter.Fill(UserDataSet.Instance.PackageMedicine, HospitalUtil.getHospitalId());
                treeListMedicineType.ExpandAll();
                treeListMedicineTypeInter.ExpandAll();
                isExpandTree = true;
                gridViewMedicine.ActiveFilterString = "";
                ProgramUtil.closeWaitDialog();
            }

            private void reloadRef(bool reload)
            {
                if(reload)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    UserDataSet.ProducerCountryTableAdapter.Fill(UserDataSet.Instance.ProducerCountry);
                    UserDataSet.ProducerTableAdapter.Fill(UserDataSet.Instance.Producer, HospitalUtil.getHospitalId());
                    UserDataSet.MedicineShapeTableAdapter.Fill(UserDataSet.Instance.MedicineShape, HospitalUtil.getHospitalId());
                    UserDataSet.MedicineUnitTableAdapter.Fill(UserDataSet.Instance.MedicineUnit, HospitalUtil.getHospitalId());
                    UserDataSet.MedicineQuantityTableAdapter.Fill(UserDataSet.Instance.MedicineQuantity, HospitalUtil.getHospitalId());
                    gridViewProducer.ActiveFilterString = "";
                    ProgramUtil.closeWaitDialog();
                }
                else
                {
                    UserDataSet.ProducerCountryTableAdapter.Fill(UserDataSet.Instance.ProducerCountry);
                    UserDataSet.ProducerTableAdapter.Fill(UserDataSet.Instance.Producer, HospitalUtil.getHospitalId());
                    UserDataSet.MedicineShapeTableAdapter.Fill(UserDataSet.Instance.MedicineShape, HospitalUtil.getHospitalId());
                    UserDataSet.MedicineUnitTableAdapter.Fill(UserDataSet.Instance.MedicineUnit, HospitalUtil.getHospitalId());
                    UserDataSet.MedicineQuantityTableAdapter.Fill(UserDataSet.Instance.MedicineQuantity, HospitalUtil.getHospitalId());
                    gridViewProducer.ActiveFilterString = "";
                }

            }

        #endregion

        #region Формын event.

            private void simpleButtonReloadMedicine_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void gridControlMedicine_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addMedicine();
                    }
                    else if (String.Equals(e.Button.Tag, "edit") && gridViewMedicine.GetFocusedRow() != null)
                    {
                        editMedicine();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewMedicine.GetFocusedRow() != null)
                    {
                        try
                        {
                            deleteMedicine();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                }
            }

            private void controlNavigatorMedicineType_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addMedicineType();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && treeListMedicineType.FocusedNode != null)
                    {
                        try
                        {
                            deleteMedicineType();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "edit") && treeListMedicineType.FocusedNode != null)
                    {
                        editMedicineType();
                    }
                    else if (String.Equals(e.Button.Tag, "expand"))
                    {
                        expandTreeMedice();
                    }
                }
            }

            private void controlNavigatorInternational_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addMedicineTypeInter();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && treeListMedicineTypeInter.FocusedNode != null)
                    {
                        try
                        {
                            deleteMedicineTypeInter();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "edit") && treeListMedicineTypeInter.FocusedNode != null)
                    {
                        editMedicineTypeInter();
                    }
                    else if (String.Equals(e.Button.Tag, "expand"))
                    {
                        expandTreeMediceInter();
                    }
                }
            }

            private void treeListMedicineType_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
            {
                if(treeListMedicineType.FocusedNode != null)
                {
                    gridViewMedicine.ActiveFilterString = "medicineTypeName = '" + treeListMedicineType.FocusedNode.GetValue("name") + "'";
                }
            }

            private void treeListMedicineTypeInter_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
            {
                if (treeListMedicineTypeInter.FocusedNode != null)
                {
                    gridViewMedicine.ActiveFilterString = "medicineTypeInterID = '" + treeListMedicineTypeInter.FocusedNode.GetValue("id") + "'";
                }
            }

        /*    private void gridViewMedicine_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
            {
                if (e.Column == gridColumnValidDate && !e.DisplayText.Equals(""))
                {
                    if (Convert.ToDateTime(e.CellValue) < DateTime.Now)
                    {
                        e.Appearance.BackColor = Color.Red;
                    }
                    else if ((Convert.ToDateTime(e.CellValue) - DateTime.Now).TotalDays <= 7)
                    {
                        e.Appearance.BackColor = Color.Gold;
                    }
                }
            }
        */
            private void buttonSaveLayout_Click(object sender, EventArgs e)
            {
                saveLayout();
            }

            #region Эмийн мэдээлэл эвэнтүүд.

                private void reloadButtonRef_Click(object sender, EventArgs e)
                {
                    try
                    {
                        reloadRef(true);
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }

                private void simpleButtonConvert_Click(object sender, EventArgs e)
                {
                    if (xtraTabControlReference.SelectedTabPage == xtraTabPageCountry)
                    {
                        ProgramUtil.convertGrid(gridViewCountry, "Улсын бүртгэл");
                    }
                    else if (xtraTabControlReference.SelectedTabPage == xtraTabPageProducer)
                    {
                        ProgramUtil.convertGrid(gridViewProducer, "Үйлдвэрлэгчийн бүртгэл");
                    }
                    else if (xtraTabControlReference.SelectedTabPage == xtraTabPageShape)
                    {
                        ProgramUtil.convertGrid(gridViewShape, "Савлагааны бүртгэл");
                    }
                    else if (xtraTabControlReference.SelectedTabPage == xtraTabPageUnit)
                    {
                        ProgramUtil.convertGrid(gridViewUnit, "Тун хэмжээний бүртгэл");
                    }
                    else if (xtraTabControlReference.SelectedTabPage == xtraTabPageQuantity)
                    {
                        ProgramUtil.convertGrid(gridViewQuantity, "Хэмжих нэгж бүртгэл");
                    }
                }
        
                private void buttonLayout_Click(object sender, EventArgs e)
                {
                    try
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                        System.IO.Stream str = new System.IO.MemoryStream();
                        System.IO.StreamReader reader = null;
                        if (xtraTabControlReference.SelectedTabPage == xtraTabPageCountry)
                        {
                            gridViewCountry.SaveLayoutToStream(str);
                            str.Seek(0, System.IO.SeekOrigin.Begin);
                            reader = new System.IO.StreamReader(str);
                            string layout = reader.ReadToEnd();
                            UserUtil.saveUserLayout("producerCountry", layout);
                        }
                        else if (xtraTabControlReference.SelectedTabPage == xtraTabPageProducer)
                        {
                            gridViewProducer.SaveLayoutToStream(str);
                            str.Seek(0, System.IO.SeekOrigin.Begin);
                            reader = new System.IO.StreamReader(str);
                            string layout = reader.ReadToEnd();
                            UserUtil.saveUserLayout("producer", layout);
                        }
                        else if (xtraTabControlReference.SelectedTabPage == xtraTabPageShape)
                        {
                            gridViewShape.SaveLayoutToStream(str);
                            str.Seek(0, System.IO.SeekOrigin.Begin);
                            reader = new System.IO.StreamReader(str);
                            string layout = reader.ReadToEnd();
                            UserUtil.saveUserLayout("shape", layout);
                        }
                        else if (xtraTabControlReference.SelectedTabPage == xtraTabPageUnit)
                        {
                            gridViewUnit.SaveLayoutToStream(str);
                            str.Seek(0, System.IO.SeekOrigin.Begin);
                            reader = new System.IO.StreamReader(str);
                            string layout = reader.ReadToEnd();
                            UserUtil.saveUserLayout("unit", layout);
                        }
                        else if (xtraTabControlReference.SelectedTabPage == xtraTabPageQuantity)
                        {
                            gridViewQuantity.SaveLayoutToStream(str);
                            str.Seek(0, System.IO.SeekOrigin.Begin);
                            reader = new System.IO.StreamReader(str);
                            string layout = reader.ReadToEnd();
                            UserUtil.saveUserLayout("quantity", layout);
                        }
                        reader.Close();
                        str.Close();

                        ProgramUtil.closeWaitDialog();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }

                private void gridControlProducer_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
                {
                    if (e.Button.ButtonType == NavigatorButtonType.Custom)
                    {
                        if (String.Equals(e.Button.Tag, "add"))
                        {
                            addProducer();
                        }
                        else if (String.Equals(e.Button.Tag, "delete"))
                        {
                            deleteProducer();
                        }
                    }
                }

                private void gridViewProducer_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
                {
                    GridColumn country = gridViewProducer.Columns["countryID"];
                    if (gridViewProducer.GetRowCellValue(e.RowHandle, country).ToString().Trim().Equals(""))
                    {
                        e.Valid = false;
                        gridViewProducer.SetColumnError(country, "Үйлдвэрлэгч улсын нэрийг оруулна уу");
                    }
                    GridColumn col = gridViewProducer.Columns["name"];
                    if (gridViewProducer.GetRowCellValue(e.RowHandle, col).ToString().Trim().Equals(""))
                    {
                        e.Valid = false;
                        gridViewProducer.SetColumnError(col, "Үйлдвэрлэгчийн нэрийг оруулна уу");
                    }     
                }

                private void gridViewProducer_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
                {
                    e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
                }

                private void gridViewProducer_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
                {
                    saveProducer();
                }

                private void gridControlCountry_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
                {
                    if (e.Button.ButtonType == NavigatorButtonType.Custom)
                    {
                        if (String.Equals(e.Button.Tag, "add"))
                        {
                            addCountry();
                        }
                        else if (String.Equals(e.Button.Tag, "delete"))
                        {
                            deleteCountry();
                        }
                    }
                }

                private void gridViewCountry_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
                {
                    saveCountry();
                }

                private void gridViewCountry_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
                {
                    GridColumn col = gridViewCountry.Columns["name"];
                    if (gridViewCountry.GetRowCellValue(e.RowHandle, col).ToString().Trim().Equals(""))
                    {
                        e.Valid = false;
                        gridViewCountry.SetColumnError(col, "Улсын нэрийг оруулна уу");
                    }
                }

                private void gridViewCountry_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
                {
                    e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
                }

                private void gridControlUnit_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
                {
                    if (e.Button.ButtonType == NavigatorButtonType.Custom)
                    {
                        if (String.Equals(e.Button.Tag, "add"))
                        {
                            addUnit();
                        }
                        else if (String.Equals(e.Button.Tag, "delete"))
                        {

                            deleteUnit();
                        }
                    }
                }

                private void gridViewUnit_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
                {
                    saveUnit();
                }

                private void gridViewUnit_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
                {
                    GridColumn col = gridViewUnit.Columns["name"];
                    if (gridViewUnit.GetRowCellValue(e.RowHandle, col).ToString().Trim().Equals(""))
                    {
                        e.Valid = false;
                        gridViewUnit.SetColumnError(col, "Тун хэмжээ оруулна уу");
                    }
                    int exist = Convert.ToInt32(UserDataSet.QueriesTableAdapter.exist_valid_unit(gridViewUnit.GetRowCellDisplayText(gridViewUnit.FocusedRowHandle, gridColumnUnitName)));
                    
                    if (exist == 2)
                    {
                        string text = "", errorText;
                        errorText = "Тун хэмжээ бүртгэгдсэн байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        e.Valid = false;
                        gridViewUnit.SetColumnError(gridColumnUnitName, errorText);
                        ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                    }
                }

                private void gridViewUnit_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
                {
                    e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
                }

                private void gridControlShape_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
                {
                    if (e.Button.ButtonType == NavigatorButtonType.Custom)
                    {
                        if (String.Equals(e.Button.Tag, "add"))
                        {
                            addShape();
                        }
                        else if (String.Equals(e.Button.Tag, "delete"))
                        {
                            deleteShape();
                        }
                    }
                }

                private void gridViewShape_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
                {
                    saveShape();
                }

                private void gridViewShape_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
                {
                    GridColumn col = gridViewShape.Columns["name"];
                    if (gridViewShape.GetRowCellValue(e.RowHandle, col).ToString().Trim().Equals(""))
                    {
                        e.Valid = false;
                        gridViewShape.SetColumnError(col, "Хэлбэр оруулна уу");
                    }
                    int exist = Convert.ToInt32(UserDataSet.QueriesTableAdapter.exist_valid_shape(gridViewShape.GetRowCellDisplayText(gridViewShape.FocusedRowHandle, gridColumnShapeName)));
                    if (exist == 2)
                    {
                        string text = "", errorText;
                        errorText = "Хэлбэр бүртгэгдсэн байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        e.Valid = false;
                        gridViewShape.SetColumnError(gridColumnShapeName, errorText);
                        ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                    }
                }

                private void gridViewShape_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
                {
                    e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
                }

                private void gridControlQuantity_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
                {
                    if (e.Button.ButtonType == NavigatorButtonType.Custom)
                    {
                        if (String.Equals(e.Button.Tag, "add"))
                        {
                            addQuantity();
                        }
                        else if (String.Equals(e.Button.Tag, "delete"))
                        {
                            deleteQuantity();
                        }
                    }
                }

                private void gridViewQuantity_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
                {
                    saveQuantity();
                }

                private void gridViewQuantity_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
                {
                    e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
                }

                private void gridViewQuantity_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
                {
                    GridColumn col = gridViewQuantity.Columns["name"];
                    if (gridViewQuantity.GetRowCellValue(e.RowHandle, col).ToString().Trim().Equals(""))
                    {
                        e.Valid = false;
                        gridViewQuantity.SetColumnError(col, "Хэмжих нэгж оруулна уу");
                    }
                    int exist = Convert.ToInt32(UserDataSet.QueriesTableAdapter.exist_valid_quantity(gridViewQuantity.GetRowCellDisplayText(gridViewQuantity.FocusedRowHandle, gridColumnQuantityName)));
                    if (exist == 2)
                    {
                        string text = "", errorText;
                        errorText = "Хэмжих нэгж бүртгэгдсэн байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        e.Valid = false;
                        gridViewQuantity.SetColumnError(gridColumnQuantityName, errorText);
                        ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                    }
                }

            #endregion

        #endregion

        #region Эм нэмэх, засах, устгах функцууд.

            public void setDefaultMed()
            {
                UserDataSet.Instance.Medicine.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                UserDataSet.Instance.Medicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                UserDataSet.Instance.Medicine.idColumn.DefaultValue = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            }

            public void setDefaultPackageMed()
            {
                UserDataSet.Instance.PackageMedicine.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                UserDataSet.Instance.Package.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                UserDataSet.Instance.PackageMedicine.idColumn.DefaultValue = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            }
            
            private void addMedicine()
            {
                addMultipleMedicine();
                MedicineInUpForm.Instance.showForm(1, Instance);
            }

            public void addMultipleMedicine()
            {
                currentMedicineView = (DataRowView)UserDataSet.MedicineBindingSource.AddNew();
                if(treeListMedicineType.FocusedNode != null)
                    currentMedicineView["medicineTypeID"] = treeListMedicineType.FocusedNode.GetValue("id");
            }

            public void addMultiplePackageMedicine()
            {
                currentMedicineView = (DataRowView)UserDataSet.PackageMedicineBindingSource.AddNew();
                if (treeListMedicineType.FocusedNode != null)
                    currentMedicineView["medicineTypeID"] = treeListMedicineType.FocusedNode.GetValue("id");
            }    

            private void editMedicine()
            {
                currentMedicineView = (DataRowView)UserDataSet.MedicineBindingSource.Current;
                MedicineInUpForm.Instance.showForm(2, Instance);
            }

            private void deleteMedicine()
            {
                object ret = UserDataSet.QueriesTableAdapter.can_delete_medicine(HospitalUtil.getHospitalId(), gridViewMedicine.GetFocusedDataRow()["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }
                DialogResult dialogResult = XtraMessageBox.Show("Энэ эмийг устгах уу!", "Эм устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    gridViewMedicine.DeleteSelectedRows();
                    UserDataSet.MedicineBindingSource.EndEdit();
                    UserDataSet.MedicineTableAdapter.Update(UserDataSet.Instance.Medicine);
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void addPackageMedicine()
            {
                addMultiplePackageMedicine();
                PackageMedicine.Instance.showForm(1, Instance);
            }
            
            private void editPackageMedicine()
            {
                currentMedicineView = (DataRowView)UserDataSet.PackageMedicineBindingSource.Current;
                PackageMedicine.Instance.showForm(2, Instance);
            }

            private void deletePackageMedicine()
            {
                DataRow[] rows = UserDataSet.Instance.Medicine.Select("hospitalID = '" + HospitalUtil.getHospitalId() +                         
                        "' AND packageID = '" + gridViewPackageMedicine.GetFocusedDataRow()["id"].ToString() + "'");
                if (rows.Length > 0)
                {
                    XtraMessageBox.Show("Энэ эмийн багцыг эмийн бүртгэлд ашиглаж байгаа тул устгаж болохгүй");
                }
                else
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ эмийн багцыг устгах уу!", "Эмийн багц устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        gridViewPackageMedicine.DeleteSelectedRows();
                        UserDataSet.PackageMedicineBindingSource.EndEdit();
                        UserDataSet.PackageMedicineTableAdapter.Update(UserDataSet.Instance.PackageMedicine);
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

        #endregion

        #region Эмийн ангилал нэмэх, засах, устгах функцууд.

            public void setDefaultType()
            {
                UserDataSet.Instance.MedicineType.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                UserDataSet.Instance.MedicineType.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                UserDataSet.Instance.MedicineType.idColumn.DefaultValue = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            }    

            private void addMedicineType()
            {
                addDoubleType();
                MedicineTypeInUpForm.Instance.showForm(1);
            }

            public void addDoubleType()
            {
                if(treeListMedicineType.FocusedNode != null && treeListMedicineType.FocusedNode.Level == 0)
                {
                    UserDataSet.Instance.MedicineType.parentIdColumn.DefaultValue = treeListMedicineType.FocusedNode.GetValue("id");
                }
                currentMedicineTypeView = (DataRowView)UserDataSet.MedicineTypeBindingSource.AddNew();
            }

            private void deleteMedicineType()
            {
                TreeListNode node = treeListMedicineType.FocusedNode;
                if (node.HasChildren)
                {
                    XtraMessageBox.Show("Дэд ангилалтай тул устгаж болохгүй!", "Эмийн ангилал");
                    return;
                }
                object ret = UserDataSet.QueriesTableAdapter.can_delete_medicine_type(HospitalUtil.getHospitalId(), node.GetValue("id").ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                return;
                }
                    
                DialogResult dialogResult = XtraMessageBox.Show("Энэ ангилалыг устгах уу!", "Эмийн ангилал", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    treeListMedicineType.DeleteNode(treeListMedicineType.FocusedNode);
                    UserDataSet.MedicineTypeBindingSource.EndEdit();
                    UserDataSet.MedicineTypeTableAdapter.Update(UserDataSet.Instance.MedicineType);
                }
            }

            private void editMedicineType()
            {
                currentMedicineTypeView = (DataRowView)UserDataSet.MedicineTypeBindingSource.Current;
                MedicineTypeInUpForm.Instance.showForm(2);
            }

            private void expandTreeMedice()
            {
                if (isExpandTree)
                {
                    treeListMedicineType.CollapseAll();
                    controlNavigatorMedicineType.Buttons.CustomButtons[0].ImageIndex = 17;
                }
                else
                {
                    treeListMedicineType.ExpandAll();
                    controlNavigatorMedicineType.Buttons.CustomButtons[0].ImageIndex = 14;
                }
                isExpandTree = !isExpandTree;
            }
        
        #endregion

        #region Эмийн олон улсын ангилал нэмэх функцууд.

            public void setDefaultInter()
            {
                UserDataSet.Instance.MedicineTypeInternational.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                UserDataSet.Instance.MedicineTypeInternational.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                UserDataSet.Instance.MedicineTypeInternational.idColumn.DefaultValue = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            }
            
            private void addMedicineTypeInter()
            {
                addDoubleInter();
                MedicineTypeInterInUpForm.Instance.showForm(1);
            }

            public void addDoubleInter()
            {
                currentMedicineTypeInterView = (DataRowView)UserDataSet.MedicineTypeInternationalBindingSource.AddNew();
            }

            private void deleteMedicineTypeInter()
            {
                TreeListNode node = treeListMedicineTypeInter.FocusedNode;
                if (node.HasChildren)
                {
                    XtraMessageBox.Show("Холбоотой мэдээлэл байгаа тул устгаж болохгүй!", "Эмийн ОУ ангилал");
                    return;
                }

                object ret = UserDataSet.QueriesTableAdapter.can_delete_medicine_type_inter(HospitalUtil.getHospitalId(), node.GetValue("id").ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }

                DialogResult dialogResult = XtraMessageBox.Show("Энэ ангилалыг устгах уу!", "Эмийн ОУ ангилал", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    treeListMedicineTypeInter.DeleteNode(treeListMedicineTypeInter.FocusedNode);
                    UserDataSet.MedicineTypeInternationalBindingSource.EndEdit();
                    UserDataSet.MedicineTypeInternationalTableAdapter.Update(UserDataSet.Instance.MedicineTypeInternational);
                }
            }

            private void editMedicineTypeInter()
            {
                currentMedicineTypeInterView = (DataRowView)UserDataSet.MedicineTypeInternationalBindingSource.Current;
                MedicineTypeInterInUpForm.Instance.showForm(2);
            }

            private void expandTreeMediceInter()
            {
                if (isExpandTree)
                {
                    treeListMedicineTypeInter.CollapseAll();
                    controlNavigatorInternational.Buttons.CustomButtons[0].ImageIndex = 17;
                }
                else
                {
                    treeListMedicineTypeInter.ExpandAll();
                    controlNavigatorInternational.Buttons.CustomButtons[0].ImageIndex = 14;
                }
                isExpandTree = !isExpandTree;
            }

        #endregion
        
        #region Хэвлэх, хөрвүүлэх функц

            private void barButtonItemMedicineType_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemMedicineType.Caption;
                dropDownButtonPrint.Image = barButtonItemMedicineType.Glyph;

                ProgramUtil.convertGrid(treeListMedicineType, "Эмийн ангилал");
            }

            private void barButtonItemMedicineTypeInter_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemMedicineTypeInter.Caption;
                dropDownButtonPrint.Image = barButtonItemMedicineTypeInter.Glyph;

                ProgramUtil.convertGrid(treeListMedicineTypeInter, "Эмийн ОУ-ын ангилал");
            }

            private void barButtonItemEndBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemEndBal.Caption;
                dropDownButtonPrint.Image = barButtonItemEndBal.Glyph;
            }

            private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemConvert.Caption;
                dropDownButtonPrint.Image = barButtonItemConvert.Glyph;

                ProgramUtil.convertGrid(gridViewMedicine, "Эмийн бүртгэл");
            }

            private void saveLayout()
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    gridViewMedicine.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    System.IO.StreamReader reader = new System.IO.StreamReader(str);
                    string layout = reader.ReadToEnd();
                    UserUtil.saveUserLayout("medicine", layout);
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion                    
          
        #region Эмийн мэдээлэл.

            #region Эмийн тун хэмжээ усгах, нэмэх, засах функцууд.

                private void addUnit()
                {
                    DataRow[] rows = UserDataSet.Instance.MedicineUnit.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                    if (rows.Length > 1)
                    {
                        DataRowView view = (DataRowView)UserDataSet.MedicineUnitBindingSource.Current;
                        if (view.IsNew)
                        {
                            UserDataSet.MedicineUnitBindingSource.CancelEdit();
                            UserDataSet.Instance.MedicineUnit.RejectChanges();
                        }
                    }
                    DataRowView currUnit = (DataRowView)UserDataSet.MedicineUnitBindingSource.AddNew();
                    currUnit["id"] = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    gridViewUnit.ShowEditForm();


                }

                private void deleteUnit()
                {
                    object ret = UserDataSet.QueriesTableAdapter.can_delete_medicine_unit(HospitalUtil.getHospitalId(), gridViewUnit.GetFocusedDataRow()["id"].ToString());
                    if (ret != DBNull.Value && ret != null)
                    {
                        string msg = ret.ToString();
                        XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                        return;
                    }
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн тоо хэмжээ устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        gridViewUnit.DeleteRow(gridViewUnit.GetFocusedDataSourceRowIndex());
                        saveUnit();
                    }
                }

                private void saveUnit()
                {
                    try
                    {
                        UserDataSet.MedicineUnitBindingSource.EndEdit();
                        UserDataSet.MedicineUnitTableAdapter.Update(UserDataSet.Instance.MedicineUnit);
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                    }
                }

            #endregion

            #region Эмийн савлагаа устгах, засах, нэмэх функцууд.

                private void addShape()
                {
                    DataRow[] rows = UserDataSet.Instance.MedicineShape.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                    if (rows.Length > 1)
                    {
                        DataRowView currShapeView = (DataRowView)UserDataSet.MedicineShapeBindingSource.Current;
                        if (currShapeView.IsNew)
                        {
                            UserDataSet.MedicineShapeBindingSource.CancelEdit();
                            UserDataSet.Instance.MedicineShape.RejectChanges();
                        }
                    }
                    DataRowView view = (DataRowView)UserDataSet.MedicineShapeBindingSource.AddNew();
                    view["id"] = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    gridViewShape.ShowEditForm();
                }

                private void deleteShape()
                {
                    object ret = UserDataSet.QueriesTableAdapter.can_delete_medicine_shape(HospitalUtil.getHospitalId(), gridViewShape.GetFocusedDataRow()["id"].ToString());
                    if (ret != DBNull.Value && ret != null)
                    {
                        string msg = ret.ToString();
                        XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                        return;
                    }
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн хэлбэр устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        gridViewShape.DeleteRow(gridViewShape.GetFocusedDataSourceRowIndex());
                        saveShape();
                    }
                }

                private void saveShape()
                {
                    try
                    {
                        UserDataSet.MedicineShapeBindingSource.EndEdit();
                        UserDataSet.MedicineShapeTableAdapter.Update(UserDataSet.Instance.MedicineShape);
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                    }
                }

            #endregion

            #region Улс засах, нэмэх, устгах функцууд.

                private void addCountry()
                {
                    DataRowView currCountryView = (DataRowView)UserDataSet.ProducerCountryBindingSource.Current;
                    if (currCountryView.IsNew)
                    {
                        UserDataSet.ProducerCountryBindingSource.CancelEdit();
                        UserDataSet.Instance.ProducerCountry.RejectChanges();
                    }
                    DataRowView view = (DataRowView)UserDataSet.ProducerCountryBindingSource.AddNew();
                    gridViewCountry.ShowEditForm();
                }

                private void deleteCountry()
                {
                    object ret = UserDataSet.QueriesTableAdapter.can_delete_producer_country(HospitalUtil.getHospitalId(), Convert.ToInt32(gridViewCountry.GetFocusedDataRow()["id"]));
                    if (ret != DBNull.Value && ret != null)
                    {
                        string msg = ret.ToString();
                        XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                        return;
                    }
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Улс устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        gridViewCountry.DeleteSelectedRows();
                        saveCountry();
                    }
                }

                private void saveCountry()
                {
                    try
                    {
                        UserDataSet.ProducerCountryBindingSource.EndEdit();
                        UserDataSet.ProducerCountryTableAdapter.Update(UserDataSet.Instance.ProducerCountry);
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                    }
                }

            #endregion

            #region Үйлдвэрлэгч засах, нэмэх, устгах функцууд.

                private void addProducer()
                {
                    DataRow[] rows = UserDataSet.Instance.Producer.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                    if (rows.Length > 1)
                    {
                        DataRowView currentProducer = (DataRowView)UserDataSet.ProducerBindingSource.Current;
                        if (currentProducer.IsNew)
                        {
                            UserDataSet.ProducerBindingSource.CancelEdit();
                            UserDataSet.Instance.Producer.RejectChanges();
                        }
                    }
                    DataRowView view = (DataRowView)UserDataSet.ProducerBindingSource.AddNew();
                    view["id"] = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    gridViewProducer.ShowEditForm();
                }

                private void deleteProducer()
                {
                    object ret = UserDataSet.QueriesTableAdapter.can_delete_producer(HospitalUtil.getHospitalId(), gridViewProducer.GetFocusedDataRow()["id"].ToString());
                    if (ret != DBNull.Value && ret != null)
                    {
                        string msg = ret.ToString();
                        XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                        return;
                    }
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Үйлдвэрлэгч усгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        gridViewProducer.DeleteSelectedRows();
                        saveProducer();
                    }
                }

                private void saveProducer()
                {
                    try
                    {
                        if (!gridViewProducer.HasColumnErrors)
                            UserDataSet.ProducerBindingSource.EndEdit();
                            UserDataSet.ProducerTableAdapter.Update(UserDataSet.Instance.Producer);
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                    }
                }

            #endregion                                      

            #region Эмийн хэмжих нэгж устгах, засах, нэмэх функцууд.

                private void addQuantity()
                {
                    DataRow[] rows = UserDataSet.Instance.MedicineQuantity.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                    if (rows.Length > 1)
                    {
                        DataRowView currentQuantity = (DataRowView)UserDataSet.MedicineQuantityBindingSource.Current;
                        if (currentQuantity.IsNew)
                        {
                            UserDataSet.MedicineQuantityBindingSource.CancelEdit();
                            UserDataSet.Instance.MedicineQuantity.RejectChanges();
                        }
                    }
                    DataRowView quantity = (DataRowView)UserDataSet.MedicineQuantityBindingSource.AddNew();
                    quantity["id"] = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    gridViewQuantity.ShowEditForm();
                }

                private void deleteQuantity()
                {
                    object ret = UserDataSet.QueriesTableAdapter.can_delete_medicine_quantity(HospitalUtil.getHospitalId(), gridViewQuantity.GetFocusedDataRow()["id"].ToString());
                    if (ret != DBNull.Value && ret != null)
                    {
                        string msg = ret.ToString();
                        XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                        return;
                    }
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн хэмжих нэгж устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        gridViewQuantity.DeleteRow(gridViewQuantity.GetFocusedDataSourceRowIndex());
                        saveQuantity();
                    }
                }

                private void saveQuantity()
                {
                    try
                    {
                        UserDataSet.MedicineQuantityBindingSource.EndEdit();
                        UserDataSet.MedicineQuantityTableAdapter.Update(UserDataSet.Instance.MedicineQuantity);
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                    }
                }

            #endregion                                       

        #endregion

        #region Эмийн багц

            #region Формын event
            
                private void reloadPack_Click(object sender, EventArgs e)
                {
                    try
                    {
                        reloadPackage(true);
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }

                private void gridControlIPackage_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
                {
                    if (e.Button.ButtonType == NavigatorButtonType.Custom)
                    {
                        if (String.Equals(e.Button.Tag, "add"))
                        {
                            add();
                        }
                        else if (String.Equals(e.Button.Tag, "delete") && gridViewPackage.RowCount != 0 && gridViewPackage.GetFocusedDataRow() != null)
                        {
                            try
                            {
                                delete();
                            }
                            catch (System.Exception ex)
                            {
                                XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                            }
                            finally
                            {
                                ProgramUtil.closeWaitDialog();
                            }
                        }
                        else if (String.Equals(e.Button.Tag, "update") && gridViewPackage.RowCount != 0 && gridViewPackage.GetFocusedDataRow() != null)
                        {
                            update();
                        }
                    }
                }

            #endregion

            #region Формын функц

                private void reloadPackage(bool reload)
                {
                    if (reload)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        //Багц.
                        UserDataSet.PackageTableAdapter.Fill(UserDataSet.Instance.Package, HospitalUtil.getHospitalId());
                        UserDataSet.PackageDetialTableAdapter.Fill(UserDataSet.Instance.PackageDetial, HospitalUtil.getHospitalId());
                        ProgramUtil.closeWaitDialog();  
                    }
                    else
                    {
                        //Багц.
                        UserDataSet.PackageTableAdapter.Fill(UserDataSet.Instance.Package, HospitalUtil.getHospitalId());
                        UserDataSet.PackageDetialTableAdapter.Fill(UserDataSet.Instance.PackageDetial, HospitalUtil.getHospitalId());
                    }
                     
                }
            
                private void add()
                {
                    PackageInUpForm.Instance.showForm(1, Instance);
                }

                private void delete()
                {
                    object ret = UserDataSet.QueriesTableAdapter.can_delete_package(HospitalUtil.getHospitalId(), gridViewPackage.GetFocusedDataRow()["id"].ToString());
                    if (ret != DBNull.Value && ret != null)
                    {
                        string msg = ret.ToString();
                        XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                        return;
                    }

                    bool isDeleteDoc = false;
                    GridView detail = null;

                    if (gridViewPackage.IsFocusedView)
                    {
                        isDeleteDoc = true;
                    }
                    else
                    {
                        detail = gridViewPackage.GetDetailView(gridViewPackage.FocusedRowHandle, 0) as GridView;
                        if (detail.IsFocusedView)
                        {
                            isDeleteDoc = detail.RowCount == 1;
                        }
                    }
                    if (isDeleteDoc)
                    {
                        DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Багц устгах", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                            gridViewPackage.DeleteSelectedRows();
                            UserDataSet.PackageBindingSource.EndEdit();
                            UserDataSet.PackageTableAdapter.Update(UserDataSet.Instance.Package);
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else
                    {
                        DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Багцын эм устгах", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                            detail.DeleteSelectedRows();
                            UserDataSet.PackageDetialBindingSource.EndEdit();
                            UserDataSet.PackageDetialTableAdapter.Update(UserDataSet.Instance.PackageDetial);
                            ProgramUtil.closeWaitDialog();
                        }
                    }  
                }

                private void update()
                {
                    object ret = UserDataSet.QueriesTableAdapter.can_delete_package(HospitalUtil.getHospitalId(), gridViewPackage.GetFocusedDataRow()["id"].ToString());
                    if (ret != DBNull.Value && ret != null)
                    {
                        string msg = ret.ToString();
                        XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул өөрчилж болохгүй");
                        return;
                    }

                    if (!gridViewPackage.GetMasterRowExpanded(gridViewPackage.FocusedRowHandle))
                        gridViewPackage.SetMasterRowExpanded(gridViewPackage.FocusedRowHandle, true);
                    PackageInUpForm.Instance.showForm(2, Instance);
                }

                public DataRowView getSelectedRow()
                {
                    GridView detail = gridViewPackage.GetDetailView(gridViewPackage.FocusedRowHandle, 0) as GridView;
                    string id = detail.GetFocusedDataRow()["id"].ToString();
                    for (int i = 0; i < UserDataSet.PackageDetialBindingSource.Count; i++)
                    {
                        DataRowView view = (DataRowView)UserDataSet.PackageDetialBindingSource[i];
                        if (view["id"].ToString().Equals(id))
                            return view;
                    }
                    return null;
                }

            #endregion             

                private void gridControlPackageMedicine_Click(object sender, EventArgs e)
                {

                }

        #endregion

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
                 if (e.Button.ButtonType == NavigatorButtonType.Custom)
                 {
                     if (String.Equals(e.Button.Tag, "add"))
                     {
                         addPackageMedicine();
                     }
                     else if (String.Equals(e.Button.Tag, "edit") && gridViewPackageMedicine.GetFocusedRow() != null)
                     {
                         editPackageMedicine();
                     }
                     else if (String.Equals(e.Button.Tag, "delete") && gridViewPackageMedicine.GetFocusedRow() != null)
                     {
                         try
                         {
                             deletePackageMedicine();
                         }
                         catch (System.Exception ex)
                         {
                             XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                         }
                         finally
                         {
                             ProgramUtil.closeWaitDialog();
                         }
                     }
                 }
            
        }
    
    }
}