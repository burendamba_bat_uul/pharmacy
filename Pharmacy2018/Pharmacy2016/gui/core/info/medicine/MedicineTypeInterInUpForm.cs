﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.info.medicine
{
    /*
     * Эмийн олон улсын ангилал нэмэх, засах цонх.
     * 
     */

    public partial class MedicineTypeInterInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineTypeInterInUpForm INSTANCE = new MedicineTypeInterInUpForm();

            public static MedicineTypeInterInUpForm Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private MedicineTypeInterInUpForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                initBinding();
                reload();
            }

            private void reload()
            {
                if (UserDataSet.Instance.MedicineTypeInternational.Rows.Count == 0)
                    UserDataSet.MedicineTypeInternationalTableAdapter.Fill(UserDataSet.Instance.MedicineTypeInternational, HospitalUtil.getHospitalId());
            }

            private void initBinding()
            {
                textEditName.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineTypeInternationalBindingSource, "name", true));
                spinEditAccount.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineTypeInternationalBindingSource, "account", true));
                textEditCode.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineTypeInternationalBindingSource, "code", true));
                memoEditDesc.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineTypeInternationalBindingSource, "description", true));
                treeListLookUpEditMedicineTypeInter.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineTypeInternationalBindingSource, "parentId", true));
            }

            private void initDataSource()
            {
                treeListLookUpEditMedicineTypeInter.Properties.DataSource = UserDataSet.MedicineTypeInterOtherBindingSource;
            }

            private void initError()
            {
                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditAccount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    ShowDialog(MedicineForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                clearErrorText();
                UserDataSet.MedicineTypeInternationalBindingSource.CancelEdit();
                UserDataSet.Instance.MedicineTypeInternational.RejectChanges();
            }

            private void clearErrorText()
            {
                textEditName.ErrorText = "";
                spinEditAccount.ErrorText = "";
            }

            private void MedicineTypeInterInUpForm_Shown(object sender, EventArgs e)
            {
                treeListLookUpEditMedicineTypeInter.Focus();
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }

        #endregion

        #region Формын эвэнтүүд.

            private void treeListLookUpEditMedicineTypeInter_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditMedicineTypeInter.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        textEditName.Focus();
                    }
                }
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                saveMedicineTypeInter();
            }

        #endregion

        #region Формын функц.

            private bool checkMedicineTypeInter()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (textEditName.EditValue == DBNull.Value || textEditName.EditValue == null || textEditName.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийн ангилалыг нэрийг оруулна уу";
                    textEditName.ErrorText = errorText;
                    isRight = false;
                    textEditName.Focus();
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                }
                if (spinEditAccount.Value == 0)
                {
                    errorText = "Эмийн ангилалын дансны дугаар оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditAccount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditAccount.Focus();
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void saveMedicineTypeInter()
            {
                if (checkMedicineTypeInter())
                {              
             
                    UserDataSet.MedicineTypeInternationalBindingSource.EndEdit();
                    UserDataSet.MedicineTypeInternationalTableAdapter.Update(UserDataSet.Instance.MedicineTypeInternational);

                    clearData();
                    MedicineForm.Instance.setDefaultInter();
                    MedicineForm.Instance.addDoubleInter();    
                }
            }

        #endregion

    }
}