﻿using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.util;
using System;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.journal.bal;
using Pharmacy2016.gui.core.journal.inc;
using DevExpress.XtraEditors.Controls;
using Pharmacy2016.gui.core.store.drug;
using Pharmacy2016.gui.core.journal.order;

namespace Pharmacy2016.gui.core.info.medicine
{
    public partial class PackageMedicine : DevExpress.XtraEditors.XtraForm
    {
        #region Форм анх ачааллах функц

        private static PackageMedicine INSTANCE = new PackageMedicine();

        public static PackageMedicine Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;
        private int actionType;
        private XtraForm form;
        public DataRowView Detial;
        public DataRowView Relate;
        private DataRowView newView;
        // private List<string> deleteRows = new List<string>();

        private PackageMedicine()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            initError();
            initDataSource();
            reload();
        }

        private void initDataSource()
        {
            treeListLookUpEditType.Properties.DataSource = UserDataSet.MedicineTypeOtherBindingSource;
            treeListLookUpEditTypeInter.Properties.DataSource = UserDataSet.MedicineTypeInterOtherBindingSource;
            gridLookUpEditUnit.Properties.DataSource = UserDataSet.MedicineUnitBindingSource;
            gridLookUpEditShape.Properties.DataSource = UserDataSet.MedicineShapeBindingSource;
            gridLookUpEditQuantity.Properties.DataSource = UserDataSet.MedicineQuantityBindingSource;
        }

        private void initError()
        {
            textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditShape.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditUnit.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditQuantity.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
        }

        #endregion


        #region Форм нээх, хаах функц

        public void showForm(int type, XtraForm form)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }

                actionType = type;
                this.form = form;
                if (actionType == 1)
                {
                    insertPackageMedicine();
                }
                else if (actionType == 2)
                {
                    updatePackageMedicine();
                }
                else if (actionType == 3)
                {
                    insertOtherPackageMedicine();
                }

                ShowDialog(form);
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clearData();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void insertPackageMedicine()
        {
            initBinding();
        }

        // Өөр цонхноос эм нэмэх функц
        private void insertOtherPackageMedicine()
        {
            textEditName.EditValue = null;
            gridLookUpEditUnit.EditValue = null;
            gridLookUpEditShape.EditValue = null;
            treeListLookUpEditType.EditValue = null;
            treeListLookUpEditTypeInter.EditValue = null;
            gridLookUpEditQuantity.EditValue = null;
        }

        private void updatePackageMedicine()
        {
            initBinding();
        }

        private void initBinding()
        {
            textEditName.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageMedicineBindingSource, "name", true));
            gridLookUpEditUnit.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageMedicineBindingSource, "unitID", true));
            gridLookUpEditShape.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageMedicineBindingSource, "shapeID", true));
            gridLookUpEditQuantity.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageMedicineBindingSource, "quantityID", true));
            treeListLookUpEditType.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageMedicineBindingSource, "medicineTypeId", true));
            treeListLookUpEditTypeInter.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageMedicineBindingSource, "medicineTypeInterId", true));
        }


        private void clearData()
        {
            clearBinding();
            clearErrorText();

            UserDataSet.PackageMedicineBindingSource.CancelEdit();
            UserDataSet.Instance.PackageMedicine.RejectChanges();
        }

        private void clearBinding()
        {
            textEditName.DataBindings.Clear();
            gridLookUpEditUnit.DataBindings.Clear();
            gridLookUpEditShape.DataBindings.Clear();
            gridLookUpEditQuantity.DataBindings.Clear();
            treeListLookUpEditType.DataBindings.Clear();
            treeListLookUpEditTypeInter.DataBindings.Clear();
        }

        private void clearErrorText()
        {
            textEditName.ErrorText = "";
            treeListLookUpEditType.ErrorText = "";
            gridLookUpEditUnit.ErrorText = "";
            gridLookUpEditShape.ErrorText = "";
            gridLookUpEditQuantity.ErrorText = "";
        }



        #endregion


        #region Формын event

        private void treeListLookUpEditType_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditType.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    treeListLookUpEditTypeInter.Focus();
                }
            }
        }

        private void treeListLookUpEditTypeInter_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditTypeInter.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditShape.Focus();
                }
            }
        }

        private void gridLookUpEditShape_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditShape.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditUnit.Focus();
                }
            }
        }

        private void gridLookUpEditUnit_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditUnit.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditQuantity.Focus();
                }
            }
        }

        private void gridLookUpEditQuantity_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditQuantity.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    //                    dateEditValidDate.Focus();
                }
            }
        }

        #endregion



        private void gridLookUpEditUnit_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "add"))
            {
                MedicineConfInForm.Instance.showForm(2, Instance);
            }
        }

        private void gridLookUpEditShape_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "add"))
            {
                MedicineConfInForm.Instance.showForm(1, Instance);
            }
        }

        private void gridLookUpEditQuantity_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "add"))
            {
                MedicineConfInForm.Instance.showForm(3, Instance);
            }
        }


        #region Формын функц

        private void reload()
        {
            UserDataSet.MedicineShapeTableAdapter.Fill(UserDataSet.Instance.MedicineShape, HospitalUtil.getHospitalId());
            UserDataSet.MedicineUnitTableAdapter.Fill(UserDataSet.Instance.MedicineUnit, HospitalUtil.getHospitalId());
            UserDataSet.MedicineQuantityTableAdapter.Fill(UserDataSet.Instance.MedicineQuantity, HospitalUtil.getHospitalId());
            if (UserDataSet.Instance.MedicineType.Rows.Count == 0)
                UserDataSet.MedicineTypeTableAdapter.Fill(UserDataSet.Instance.MedicineType, HospitalUtil.getHospitalId());
            if (UserDataSet.Instance.MedicineTypeInternational.Rows.Count == 0)
                UserDataSet.MedicineTypeInternationalTableAdapter.Fill(UserDataSet.Instance.MedicineTypeInternational, HospitalUtil.getHospitalId());

        }

        private void PackageMedicine_Shown(object sender, System.EventArgs e)
        {
            textEditName.Focus();
            ProgramUtil.IsShowPopup = true;
            ProgramUtil.IsSaveEvent = false;
        }

        private bool checkMedicine()
        {
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();
            Instance.Validate();

            if (textEditName.EditValue == DBNull.Value || textEditName.EditValue == null || (textEditName.EditValue = textEditName.EditValue.ToString().Trim()).Equals(""))
            {
                errorText = "Эмийн багцын нэрийг оруулна уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                textEditName.ErrorText = errorText;
                isRight = false;
                textEditName.Focus();
            }

            if (textEditName.EditValue != DBNull.Value || textEditName.EditValue != null)
            {
                DataRow[] rows = UserDataSet.Instance.PackageMedicine.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' and name = '" + textEditName.EditValue + "' and unitID = '" + gridLookUpEditUnit.EditValue + "' and shapeID = '" + gridLookUpEditShape.EditValue + "' and quantityID = '" + gridLookUpEditQuantity.EditValue + "' and medicineTypeID = '" + treeListLookUpEditType.EditValue + "' and medicineTypeInterID = '" + treeListLookUpEditTypeInter.EditValue + "'  ");
                if (rows.Length > 0)
                {
                    errorText = "Эмийн багцын мэдээлэл давхардаж байна. Өөрчилнө үү";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    isRight = false;
                    textEditName.Focus();
                }
            }

            if (treeListLookUpEditType.EditValue == DBNull.Value || treeListLookUpEditType.EditValue == null || treeListLookUpEditType.Text.Equals(""))
            {
                errorText = "Эмийн ангилалыг оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditType.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditType.Focus();
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                }
            }
            if (gridLookUpEditShape.EditValue == DBNull.Value || gridLookUpEditShape.EditValue == null || gridLookUpEditShape.EditValue.ToString().Equals(""))
            {
                errorText = "Эмийн хэлбэрийг оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditShape.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditShape.Focus();
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                }
            }
            if (gridLookUpEditUnit.EditValue == DBNull.Value || gridLookUpEditUnit.EditValue == null || gridLookUpEditUnit.EditValue.ToString().Equals(""))
            {
                errorText = "Тун хэмжээг оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditUnit.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditUnit.Focus();
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                }
            }
            if (gridLookUpEditQuantity.EditValue == DBNull.Value || gridLookUpEditQuantity.EditValue == null || gridLookUpEditQuantity.EditValue.ToString().Equals(""))
            {
                errorText = "Хэмжих нэгжийг оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditQuantity.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditQuantity.Focus();
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                }
            }


            if (!isRight)
                ProgramUtil.showAlertDialog(this, Instance.Text, text);

            return isRight;
        }

        private void saveButtonRegisterMedicine_Click(object sender, EventArgs e)
        {
            savePackageMedicine();
        }

        private void savePackageMedicine()
        {
            if (checkMedicine())
            {
                try
                {
                    if (actionType == 1 || actionType == 2)
                    {
                        if (actionType == 1 && MedicineForm.Instance.currentMedicineView["id"] == DBNull.Value)
                        {
                            MedicineForm.Instance.currentMedicineView["actionUserID"] = UserUtil.getUserId();
                            MedicineForm.Instance.currentMedicineView["hospitalID"] = HospitalUtil.getHospitalId();
                            MedicineForm.Instance.currentMedicineView["id"] = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                        }
                        if (actionType == 2 && MedicineForm.Instance.currentMedicineView["actionUserID"] == DBNull.Value)
                        {
                            MedicineForm.Instance.currentMedicineView["actionUserID"] = UserUtil.getUserId();
                        }
                        MedicineForm.Instance.currentMedicineView["medicineTypeInterName"] = treeListLookUpEditTypeInter.Text;
                        MedicineForm.Instance.currentMedicineView["medicineTypeName"] = treeListLookUpEditType.Text;
                        MedicineForm.Instance.currentMedicineView["shapeName"] = gridLookUpEditShape.Text;
                        MedicineForm.Instance.currentMedicineView["unitName"] = gridLookUpEditUnit.Text;
                        MedicineForm.Instance.currentMedicineView["quantityName"] = gridLookUpEditQuantity.Text;
                        MedicineForm.Instance.currentMedicineView.EndEdit();
                        UserDataSet.PackageMedicineTableAdapter.Update(UserDataSet.Instance.PackageMedicine);

                        clearData();
                        MedicineForm.Instance.setDefaultPackageMed();
                        MedicineForm.Instance.addMultiplePackageMedicine();
                        initBinding();
                    }
                    else if (actionType == 3)
                    {
                        UserDataSet.Instance.PackageMedicine.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.PackageMedicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.PackageMedicine.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                        DataRowView view = (DataRowView)UserDataSet.PackageMedicineBindingSource.AddNew();
                        view["name"] = textEditName.EditValue;
                        view["unitID"] = gridLookUpEditUnit.EditValue;
                        view["shapeID"] = gridLookUpEditShape.EditValue;
                        view["quantityID"] = gridLookUpEditQuantity.EditValue;
                        view["medicineTypeId"] = treeListLookUpEditType.EditValue;
                        view["medicineTypeInterID"] = treeListLookUpEditTypeInter.EditValue;
                        view["medicineTypeName"] = treeListLookUpEditType.Text;
                        view["medicineTypeInterName"] = treeListLookUpEditTypeInter.Text;
                        view["shapeName"] = gridLookUpEditShape.Text;
                        view["unitName"] = gridLookUpEditUnit.Text;
                        view["quantityName"] = gridLookUpEditQuantity.Text;
                        view.EndEdit();
                        UserDataSet.PackageMedicineTableAdapter.Update(UserDataSet.Instance.PackageMedicine);
                        // Өөр цонхноос эм нэмсэн бол тухайн нэмсэн эмийг дамжуулж байна.
                        if (form == PackageInUpForm.Instance)
                        {
                            PackageInUpForm.Instance.addMedicine(view);
                        }
                        else if (form == MedicineOrderUpForm.Instance)
                        {
                            MedicineOrderUpForm.Instance.addMedicine(view);
                        }
                        else if (form == DoctorDetailUpForm.Instance)
                        {
                            DoctorDetailUpForm.Instance.addMedicine(view);
                        }
                        else if (form == MedicineInUpForm.Instance)
                        {
                            MedicineInUpForm.Instance.addMedicine(view);
                        }
                        Close();
                    }
                    else if (actionType == 4)
                    {
                        newView["name"] = textEditName.EditValue;
                        newView["unitID"] = gridLookUpEditUnit.EditValue;
                        newView["shapeID"] = gridLookUpEditShape.EditValue;
                        newView["quantityID"] = gridLookUpEditQuantity.EditValue;
                        newView["medicineTypeId"] = treeListLookUpEditType.EditValue;
                        newView["medicineTypeInterID"] = treeListLookUpEditTypeInter.EditValue;
                        newView["medicineTypeName"] = treeListLookUpEditType.Text;
                        newView["medicineTypeInterName"] = treeListLookUpEditTypeInter.Text;
                        newView["shapeName"] = gridLookUpEditShape.Text;
                        newView["unitName"] = gridLookUpEditUnit.Text;
                        newView["quantityName"] = gridLookUpEditQuantity.Text;
                        newView.EndEdit();
                        UserDataSet.PackageMedicineTableAdapter.Update(UserDataSet.Instance.PackageMedicine);

                        clearData();
                        UserDataSet.Instance.MedicRel.Rows.Clear();
                        MedicineForm.Instance.setDefaultMed();
                        MedicineForm.Instance.addMultiplePackageMedicine();
                        initBinding();
                    }
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                }
            }
        }

        //private void addNewRow(DataRow row)
        //{
        //    Relate = (DataRowView)UserDataSet.MedicineRelativeBindingSource.AddNew();

        //    Relate["id"] = row["id"];
        //    Relate["medicineID"] = MedicineForm.Instance.currentMedicineView["id"];
        //    Relate["conMedicineID"] = row["conMedicineID"];
        //    Relate.EndEdit();
        //}

        // Шинээр нэмсэн савлагааг авч байна

        public void addShape(string id)
        {
            gridLookUpEditShape.EditValue = id;
        }

        // Шинээр нэмсэн тун хэмжээг авч байна
        public void addUnit(string id)
        {
            gridLookUpEditUnit.EditValue = id;
        }

        // Шинээр нэмсэн хэмжих нэгжийг авч байна
        public void addQuantity(string id)
        {
            gridLookUpEditQuantity.EditValue = id;
        }

        #endregion

        private void closeButtonRegisterMedicine_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            reload();
        }
    }
}