﻿namespace Pharmacy2016.gui.core.info.medicine
{
    partial class MedicineForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicineForm));
            this.gridViewPackageDetial = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlPackage = new DevExpress.XtraGrid.GridControl();
            this.gridViewPackage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnCreatedDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bandedGridColumnName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnProducer = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnDesc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnIsUse = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridViewDetail = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.panelContainer1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanelMedicineType = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.treeListMedicineType = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.controlNavigatorMedicineType = new DevExpress.XtraEditors.ControlNavigator();
            this.dockPanelMedicineOtherType = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.treeListMedicineTypeInter = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.controlNavigatorInternational = new DevExpress.XtraEditors.ControlNavigator();
            this.dockPanelPackage = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel3_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButton2 = new DevExpress.XtraEditors.DropDownButton();
            this.reloadPack = new DevExpress.XtraEditors.SimpleButton();
            this.dockPanelMedicineConf = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel4_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControlReference = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageProducer = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlProducer = new DevExpress.XtraGrid.GridControl();
            this.gridViewProducer = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnCountryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditCountry = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageCountry = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlCountry = new DevExpress.XtraGrid.GridControl();
            this.gridViewCountry = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnCountry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageUnit = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlUnit = new DevExpress.XtraGrid.GridControl();
            this.gridViewUnit = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnUnitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageShape = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlShape = new DevExpress.XtraGrid.GridControl();
            this.gridViewShape = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnShapeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageQuantity = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlQuantity = new DevExpress.XtraGrid.GridControl();
            this.gridViewQuantity = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnQuantityName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonConvert = new DevExpress.XtraEditors.SimpleButton();
            this.reloadButtonRef = new DevExpress.XtraEditors.SimpleButton();
            this.dockPanelPackageMedicine = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer2 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.gridControlPackageMedicine = new DevExpress.XtraGrid.GridControl();
            this.gridViewPackageMedicine = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButton3 = new DevExpress.XtraEditors.DropDownButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.dockPanelMedic = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel5_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.gridControlMedicine = new DevExpress.XtraGrid.GridControl();
            this.gridViewMedicine = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.buttonSaveLayout = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemMedicineType = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemMedicineTypeInter = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEndBal = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.simpleButtonReloadMedicine = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButton1 = new DevExpress.XtraEditors.DropDownButton();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.documentManager1 = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.tabbedView1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.documentGroup1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.DocumentGroup(this.components);
            this.dockPanel5Document = new DevExpress.XtraBars.Docking2010.Views.Tabbed.Document(this.components);
            this.dockPanel4Document = new DevExpress.XtraBars.Docking2010.Views.Tabbed.Document(this.components);
            this.dockPanel3Document = new DevExpress.XtraBars.Docking2010.Views.Tabbed.Document(this.components);
            this.dockPanel1Document = new DevExpress.XtraBars.Docking2010.Views.Tabbed.Document(this.components);
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackageDetial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.panelContainer1.SuspendLayout();
            this.dockPanelMedicineType.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListMedicineType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.dockPanelMedicineOtherType.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListMedicineTypeInter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.dockPanelPackage.SuspendLayout();
            this.dockPanel3_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            this.dockPanelMedicineConf.SuspendLayout();
            this.dockPanel4_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlReference)).BeginInit();
            this.xtraTabControlReference.SuspendLayout();
            this.xtraTabPageProducer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProducer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProducer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.xtraTabPageCountry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCountry)).BeginInit();
            this.xtraTabPageUnit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUnit)).BeginInit();
            this.xtraTabPageShape.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlShape)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewShape)).BeginInit();
            this.xtraTabPageQuantity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            this.dockPanelPackageMedicine.SuspendLayout();
            this.controlContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPackageMedicine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackageMedicine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            this.dockPanelMedic.SuspendLayout();
            this.dockPanel5_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMedicine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMedicine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel5Document)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel4Document)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel3Document)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel1Document)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewPackageDetial
            // 
            this.gridViewPackageDetial.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn11,
            this.gridColumn9,
            this.gridColumn14,
            this.gridColumn27,
            this.gridColumn10});
            this.gridViewPackageDetial.GridControl = this.gridControlPackage;
            this.gridViewPackageDetial.LevelIndent = 1;
            this.gridViewPackageDetial.Name = "gridViewPackageDetial";
            this.gridViewPackageDetial.OptionsBehavior.ReadOnly = true;
            this.gridViewPackageDetial.OptionsPrint.ExpandAllDetails = true;
            this.gridViewPackageDetial.OptionsPrint.PrintDetails = true;
            this.gridViewPackageDetial.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewPackageDetial.OptionsView.ShowFooter = true;
            this.gridViewPackageDetial.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Эмийн олон улсын нэршил";
            this.gridColumn5.CustomizationCaption = "Ерөнхий нэршил";
            this.gridColumn5.FieldName = "soloName";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 183;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Эмийн хэлбэр";
            this.gridColumn11.CustomizationCaption = "Эмийн хэлбэр";
            this.gridColumn11.FieldName = "shape";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            this.gridColumn11.Width = 107;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Хэмжих нэгж";
            this.gridColumn9.FieldName = "quantity";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            this.gridColumn9.Width = 87;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Тоо";
            this.gridColumn14.ColumnEdit = this.repositoryItemSpinEdit2;
            this.gridColumn14.CustomizationCaption = "Тоо хэмжээ";
            this.gridColumn14.FieldName = "count";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 3;
            this.gridColumn14.Width = 87;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Үнэ";
            this.gridColumn27.ColumnEdit = this.repositoryItemSpinEdit2;
            this.gridColumn27.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumn27.FieldName = "price";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 4;
            this.gridColumn27.Width = 104;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Дүн";
            this.gridColumn10.ColumnEdit = this.repositoryItemSpinEdit2;
            this.gridColumn10.CustomizationCaption = "Нийт үнэ";
            this.gridColumn10.FieldName = "sumPrice";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 5;
            this.gridColumn10.Width = 126;
            // 
            // gridControlPackage
            // 
            this.gridControlPackage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPackage.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPackage.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPackage.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPackage.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPackage.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPackage.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "update")});
            this.gridControlPackage.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlIPackage_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.LevelTemplate = this.gridViewPackageDetial;
            gridLevelNode1.RelationName = "Package_PackageDetial";
            this.gridControlPackage.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlPackage.Location = new System.Drawing.Point(2, 52);
            this.gridControlPackage.MainView = this.gridViewPackage;
            this.gridControlPackage.Name = "gridControlPackage";
            this.gridControlPackage.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemSpinEdit2});
            this.gridControlPackage.Size = new System.Drawing.Size(574, 382);
            this.gridControlPackage.TabIndex = 9;
            this.gridControlPackage.UseEmbeddedNavigator = true;
            this.gridControlPackage.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPackage,
            this.gridViewDetail,
            this.gridViewPackageDetial});
            // 
            // gridViewPackage
            // 
            this.gridViewPackage.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.gridViewPackage.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumnName,
            this.bandedGridColumnProducer,
            this.bandedGridColumnCreatedDate,
            this.bandedGridColumnIsUse,
            this.bandedGridColumnDesc});
            this.gridViewPackage.GridControl = this.gridControlPackage;
            this.gridViewPackage.LevelIndent = 0;
            this.gridViewPackage.Name = "gridViewPackage";
            this.gridViewPackage.OptionsBehavior.ReadOnly = true;
            this.gridViewPackage.OptionsDetail.ShowDetailTabs = false;
            this.gridViewPackage.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewPackage.OptionsPrint.ExpandAllDetails = true;
            this.gridViewPackage.OptionsPrint.PrintDetails = true;
            this.gridViewPackage.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPackage.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Багцын ерөнхий мэдээлэл";
            this.gridBand1.Columns.Add(this.bandedGridColumnCreatedDate);
            this.gridBand1.Columns.Add(this.bandedGridColumnName);
            this.gridBand1.Columns.Add(this.bandedGridColumnProducer);
            this.gridBand1.Columns.Add(this.bandedGridColumnDesc);
            this.gridBand1.Columns.Add(this.bandedGridColumnIsUse);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 107;
            // 
            // bandedGridColumnCreatedDate
            // 
            this.bandedGridColumnCreatedDate.Caption = "Огноо";
            this.bandedGridColumnCreatedDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumnCreatedDate.CustomizationCaption = "Огноо";
            this.bandedGridColumnCreatedDate.FieldName = "createdDate";
            this.bandedGridColumnCreatedDate.Name = "bandedGridColumnCreatedDate";
            this.bandedGridColumnCreatedDate.Visible = true;
            this.bandedGridColumnCreatedDate.Width = 24;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // bandedGridColumnName
            // 
            this.bandedGridColumnName.Caption = "Багцын нэр";
            this.bandedGridColumnName.FieldName = "name";
            this.bandedGridColumnName.Name = "bandedGridColumnName";
            this.bandedGridColumnName.Visible = true;
            this.bandedGridColumnName.Width = 20;
            // 
            // bandedGridColumnProducer
            // 
            this.bandedGridColumnProducer.Caption = "Бэлтгэн нийлүүлэгч";
            this.bandedGridColumnProducer.FieldName = "supplierName";
            this.bandedGridColumnProducer.Name = "bandedGridColumnProducer";
            this.bandedGridColumnProducer.Width = 25;
            // 
            // bandedGridColumnDesc
            // 
            this.bandedGridColumnDesc.Caption = "Тайлбар";
            this.bandedGridColumnDesc.FieldName = "description";
            this.bandedGridColumnDesc.Name = "bandedGridColumnDesc";
            this.bandedGridColumnDesc.Visible = true;
            this.bandedGridColumnDesc.Width = 34;
            // 
            // bandedGridColumnIsUse
            // 
            this.bandedGridColumnIsUse.Caption = "Ашиглах эсэх";
            this.bandedGridColumnIsUse.FieldName = "isUse";
            this.bandedGridColumnIsUse.Name = "bandedGridColumnIsUse";
            this.bandedGridColumnIsUse.Visible = true;
            this.bandedGridColumnIsUse.Width = 29;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridViewDetail
            // 
            this.gridViewDetail.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand5,
            this.gridBand10,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8});
            this.gridViewDetail.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn14,
            this.bandedGridColumn6,
            this.bandedGridColumn24,
            this.bandedGridColumn7,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn25,
            this.bandedGridColumn26,
            this.bandedGridColumn12,
            this.bandedGridColumn13,
            this.bandedGridColumn11,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.bandedGridColumn19,
            this.bandedGridColumnCount,
            this.bandedGridColumn28,
            this.bandedGridColumnSumPrice,
            this.bandedGridColumn21,
            this.bandedGridColumn22,
            this.bandedGridColumn27});
            this.gridViewDetail.GridControl = this.gridControlPackage;
            this.gridViewDetail.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", this.bandedGridColumnCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", this.bandedGridColumnSumPrice, "{0:c2}")});
            this.gridViewDetail.LevelIndent = 0;
            this.gridViewDetail.Name = "gridViewDetail";
            this.gridViewDetail.OptionsBehavior.ReadOnly = true;
            this.gridViewDetail.OptionsDetail.ShowDetailTabs = false;
            this.gridViewDetail.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewDetail.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDetail.OptionsView.ShowFooter = true;
            this.gridViewDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Баримт";
            this.gridBand4.Columns.Add(this.bandedGridColumn14);
            this.gridBand4.Columns.Add(this.bandedGridColumn6);
            this.gridBand4.Columns.Add(this.bandedGridColumn24);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 259;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.Caption = "Дугаар";
            this.bandedGridColumn14.CustomizationCaption = "Дугаар";
            this.bandedGridColumn14.FieldName = "incJournalID";
            this.bandedGridColumn14.MinWidth = 75;
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn14.Visible = true;
            this.bandedGridColumn14.Width = 90;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "Огноо";
            this.bandedGridColumn6.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumn6.CustomizationCaption = "Огноо";
            this.bandedGridColumn6.FieldName = "date";
            this.bandedGridColumn6.MinWidth = 75;
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn6.Visible = true;
            this.bandedGridColumn6.Width = 94;
            // 
            // bandedGridColumn24
            // 
            this.bandedGridColumn24.Caption = "Орлогын төрөл";
            this.bandedGridColumn24.CustomizationCaption = "Орлогын төрөл";
            this.bandedGridColumn24.FieldName = "incTypeName";
            this.bandedGridColumn24.MinWidth = 75;
            this.bandedGridColumn24.Name = "bandedGridColumn24";
            this.bandedGridColumn24.Visible = true;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Нярав";
            this.gridBand5.Columns.Add(this.bandedGridColumn7);
            this.gridBand5.Columns.Add(this.bandedGridColumn9);
            this.gridBand5.Columns.Add(this.bandedGridColumn10);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 1;
            this.gridBand5.Width = 278;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "Дугаар";
            this.bandedGridColumn7.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.bandedGridColumn7.FieldName = "userID";
            this.bandedGridColumn7.MinWidth = 75;
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 91;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "Нэр";
            this.bandedGridColumn9.CustomizationCaption = "Хэрэглэгчийн нэр";
            this.bandedGridColumn9.FieldName = "userName";
            this.bandedGridColumn9.MinWidth = 75;
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.Visible = true;
            this.bandedGridColumn9.Width = 91;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.Caption = "Эрх";
            this.bandedGridColumn10.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.bandedGridColumn10.FieldName = "roleName";
            this.bandedGridColumn10.MinWidth = 75;
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn10.Visible = true;
            this.bandedGridColumn10.Width = 96;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand10.AppearanceHeader.Options.UseFont = true;
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "Агуулах";
            this.gridBand10.Columns.Add(this.bandedGridColumn25);
            this.gridBand10.Columns.Add(this.bandedGridColumn26);
            this.gridBand10.CustomizationCaption = "Агуулах";
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 2;
            this.gridBand10.Width = 150;
            // 
            // bandedGridColumn25
            // 
            this.bandedGridColumn25.Caption = "Нэр";
            this.bandedGridColumn25.CustomizationCaption = "Агуулахын нэр";
            this.bandedGridColumn25.FieldName = "warehouseName";
            this.bandedGridColumn25.MinWidth = 75;
            this.bandedGridColumn25.Name = "bandedGridColumn25";
            this.bandedGridColumn25.Visible = true;
            // 
            // bandedGridColumn26
            // 
            this.bandedGridColumn26.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn26.CustomizationCaption = "Агуулахын дэлгэрэнгүй";
            this.bandedGridColumn26.FieldName = "warehouseDesc";
            this.bandedGridColumn26.MinWidth = 75;
            this.bandedGridColumn26.Name = "bandedGridColumn26";
            this.bandedGridColumn26.Visible = true;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand6.AppearanceHeader.Options.UseFont = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Нэмэлт мэдээлэл";
            this.gridBand6.Columns.Add(this.bandedGridColumn12);
            this.gridBand6.Columns.Add(this.bandedGridColumn13);
            this.gridBand6.Columns.Add(this.bandedGridColumn11);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 3;
            this.gridBand6.Width = 281;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.Caption = "Бэлтгэн нийлүүлэгч";
            this.bandedGridColumn12.CustomizationCaption = "Бэлтгэн нийлүүлэгч";
            this.bandedGridColumn12.FieldName = "producerName";
            this.bandedGridColumn12.MinWidth = 75;
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.Visible = true;
            this.bandedGridColumn12.Width = 87;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.Caption = "Дагалдах баримт";
            this.bandedGridColumn13.CustomizationCaption = "Дагалдах баримт";
            this.bandedGridColumn13.FieldName = "docNumber";
            this.bandedGridColumn13.MinWidth = 75;
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.Visible = true;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.Caption = "Тайлбар";
            this.bandedGridColumn11.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn11.FieldName = "description";
            this.bandedGridColumn11.MinWidth = 75;
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn11.Visible = true;
            this.bandedGridColumn11.Width = 119;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand7.AppearanceHeader.Options.UseFont = true;
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "Эмийн мэдээлэл";
            this.gridBand7.Columns.Add(this.bandedGridColumn15);
            this.gridBand7.Columns.Add(this.bandedGridColumn16);
            this.gridBand7.Columns.Add(this.bandedGridColumn17);
            this.gridBand7.Columns.Add(this.bandedGridColumn22);
            this.gridBand7.Columns.Add(this.bandedGridColumn21);
            this.gridBand7.Columns.Add(this.bandedGridColumn19);
            this.gridBand7.Columns.Add(this.bandedGridColumn27);
            this.gridBand7.CustomizationCaption = "Эмийн мэдээлэл";
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 4;
            this.gridBand7.Width = 525;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.Caption = "Эмийн нэр";
            this.bandedGridColumn15.CustomizationCaption = "Худалдааны нэр";
            this.bandedGridColumn15.FieldName = "medicineName";
            this.bandedGridColumn15.MinWidth = 75;
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.Visible = true;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.Caption = "ОУ нэр";
            this.bandedGridColumn16.CustomizationCaption = "Олон улсын нэр";
            this.bandedGridColumn16.FieldName = "latinName";
            this.bandedGridColumn16.MinWidth = 75;
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.Visible = true;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.Caption = "Ангилал";
            this.bandedGridColumn17.CustomizationCaption = "Эмийн ангилал";
            this.bandedGridColumn17.FieldName = "medicineTypeName";
            this.bandedGridColumn17.MinWidth = 75;
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.Visible = true;
            // 
            // bandedGridColumn22
            // 
            this.bandedGridColumn22.Caption = "Хүчинтэй хугацаа";
            this.bandedGridColumn22.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumn22.CustomizationCaption = "Хүчинтэй хугацаа";
            this.bandedGridColumn22.FieldName = "validDate";
            this.bandedGridColumn22.MinWidth = 75;
            this.bandedGridColumn22.Name = "bandedGridColumn22";
            this.bandedGridColumn22.Visible = true;
            // 
            // bandedGridColumn21
            // 
            this.bandedGridColumn21.Caption = "Хэлбэр";
            this.bandedGridColumn21.CustomizationCaption = "Хэлбэр";
            this.bandedGridColumn21.FieldName = "shape";
            this.bandedGridColumn21.MinWidth = 75;
            this.bandedGridColumn21.Name = "bandedGridColumn21";
            this.bandedGridColumn21.Visible = true;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.Caption = "Тун хэмжээ";
            this.bandedGridColumn19.CustomizationCaption = "Тун хэмжээ";
            this.bandedGridColumn19.FieldName = "unitType";
            this.bandedGridColumn19.MinWidth = 75;
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.Visible = true;
            // 
            // bandedGridColumn27
            // 
            this.bandedGridColumn27.Caption = "Хэмжих нэгж";
            this.bandedGridColumn27.CustomizationCaption = "Хэмжих нэгж";
            this.bandedGridColumn27.FieldName = "quantity";
            this.bandedGridColumn27.MinWidth = 75;
            this.bandedGridColumn27.Name = "bandedGridColumn27";
            this.bandedGridColumn27.Visible = true;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "Дүн";
            this.gridBand8.Columns.Add(this.bandedGridColumnCount);
            this.gridBand8.Columns.Add(this.bandedGridColumn28);
            this.gridBand8.Columns.Add(this.bandedGridColumnSumPrice);
            this.gridBand8.CustomizationCaption = "Дүн";
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 5;
            this.gridBand8.Width = 225;
            // 
            // bandedGridColumnCount
            // 
            this.bandedGridColumnCount.Caption = "Тоо";
            this.bandedGridColumnCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnCount.CustomizationCaption = "Тоо";
            this.bandedGridColumnCount.FieldName = "count";
            this.bandedGridColumnCount.MinWidth = 75;
            this.bandedGridColumnCount.Name = "bandedGridColumnCount";
            this.bandedGridColumnCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.bandedGridColumnCount.Visible = true;
            // 
            // bandedGridColumn28
            // 
            this.bandedGridColumn28.Caption = "Нэгжийн үнэ";
            this.bandedGridColumn28.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn28.CustomizationCaption = "Нэгжийн үнэ";
            this.bandedGridColumn28.FieldName = "price";
            this.bandedGridColumn28.MinWidth = 75;
            this.bandedGridColumn28.Name = "bandedGridColumn28";
            this.bandedGridColumn28.Visible = true;
            // 
            // bandedGridColumnSumPrice
            // 
            this.bandedGridColumnSumPrice.Caption = "Дүн";
            this.bandedGridColumnSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnSumPrice.CustomizationCaption = "Дүн";
            this.bandedGridColumnSumPrice.FieldName = "sumPrice";
            this.bandedGridColumnSumPrice.MinWidth = 75;
            this.bandedGridColumnSumPrice.Name = "bandedGridColumnSumPrice";
            this.bandedGridColumnSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.bandedGridColumnSumPrice.Visible = true;
            // 
            // dockManager1
            // 
            this.dockManager1.DockingOptions.ShowCloseButton = false;
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.panelContainer1,
            this.dockPanelPackage,
            this.dockPanelMedicineConf,
            this.dockPanelPackageMedicine,
            this.dockPanelMedic});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // panelContainer1
            // 
            this.panelContainer1.ActiveChild = this.dockPanelMedicineType;
            this.panelContainer1.Controls.Add(this.dockPanelMedicineType);
            this.panelContainer1.Controls.Add(this.dockPanelMedicineOtherType);
            this.panelContainer1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.panelContainer1.ID = new System.Guid("a3b8ddc6-6de9-4ce5-8f8c-bd939063ac54");
            this.panelContainer1.Location = new System.Drawing.Point(0, 0);
            this.panelContainer1.Name = "panelContainer1";
            this.panelContainer1.OriginalSize = new System.Drawing.Size(200, 200);
            this.panelContainer1.Size = new System.Drawing.Size(200, 464);
            this.panelContainer1.Tabbed = true;
            this.panelContainer1.Text = "panelContainer1";
            // 
            // dockPanelMedicineType
            // 
            this.dockPanelMedicineType.Controls.Add(this.dockPanel1_Container);
            this.dockPanelMedicineType.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanelMedicineType.FloatVertical = true;
            this.dockPanelMedicineType.ID = new System.Guid("f1a3ceef-21b0-4d45-ae6a-dffbfbffbc26");
            this.dockPanelMedicineType.Location = new System.Drawing.Point(4, 23);
            this.dockPanelMedicineType.Name = "dockPanelMedicineType";
            this.dockPanelMedicineType.Options.ShowCloseButton = false;
            this.dockPanelMedicineType.OriginalSize = new System.Drawing.Size(192, 410);
            this.dockPanelMedicineType.Size = new System.Drawing.Size(192, 410);
            this.dockPanelMedicineType.Text = "Эмийн ангилал";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.treeListMedicineType);
            this.dockPanel1_Container.Controls.Add(this.panelControl3);
            this.dockPanel1_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(192, 410);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // treeListMedicineType
            // 
            this.treeListMedicineType.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeListMedicineType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListMedicineType.KeyFieldName = "id";
            this.treeListMedicineType.Location = new System.Drawing.Point(0, 0);
            this.treeListMedicineType.Name = "treeListMedicineType";
            this.treeListMedicineType.OptionsBehavior.Editable = false;
            this.treeListMedicineType.OptionsNavigation.AutoFocusNewNode = true;
            this.treeListMedicineType.ParentFieldName = "parentId";
            this.treeListMedicineType.Size = new System.Drawing.Size(192, 382);
            this.treeListMedicineType.TabIndex = 9;
            this.treeListMedicineType.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeListMedicineType_FocusedNodeChanged);
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Ангилал";
            this.treeListColumn2.FieldName = "name";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.OptionsColumn.AllowMove = false;
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.controlNavigatorMedicineType);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(0, 382);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(192, 28);
            this.panelControl3.TabIndex = 1;
            // 
            // controlNavigatorMedicineType
            // 
            this.controlNavigatorMedicineType.Buttons.Append.Visible = false;
            this.controlNavigatorMedicineType.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorMedicineType.Buttons.Edit.Visible = false;
            this.controlNavigatorMedicineType.Buttons.EndEdit.Visible = false;
            this.controlNavigatorMedicineType.Buttons.First.Visible = false;
            this.controlNavigatorMedicineType.Buttons.Last.Visible = false;
            this.controlNavigatorMedicineType.Buttons.Next.Visible = false;
            this.controlNavigatorMedicineType.Buttons.NextPage.Visible = false;
            this.controlNavigatorMedicineType.Buttons.Prev.Visible = false;
            this.controlNavigatorMedicineType.Buttons.PrevPage.Visible = false;
            this.controlNavigatorMedicineType.Buttons.Remove.Visible = false;
            this.controlNavigatorMedicineType.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 17, true, true, "Задлах", "expand"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "edit")});
            this.controlNavigatorMedicineType.Dock = System.Windows.Forms.DockStyle.Left;
            this.controlNavigatorMedicineType.Location = new System.Drawing.Point(2, 2);
            this.controlNavigatorMedicineType.Name = "controlNavigatorMedicineType";
            this.controlNavigatorMedicineType.ShowToolTips = true;
            this.controlNavigatorMedicineType.Size = new System.Drawing.Size(90, 24);
            this.controlNavigatorMedicineType.TabIndex = 2;
            this.controlNavigatorMedicineType.Text = "controlNavigator1";
            this.controlNavigatorMedicineType.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.controlNavigatorMedicineType_ButtonClick);
            // 
            // dockPanelMedicineOtherType
            // 
            this.dockPanelMedicineOtherType.Controls.Add(this.controlContainer1);
            this.dockPanelMedicineOtherType.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanelMedicineOtherType.ID = new System.Guid("7574af41-7e2b-427d-8be6-ebcd0cb4ee98");
            this.dockPanelMedicineOtherType.Location = new System.Drawing.Point(4, 23);
            this.dockPanelMedicineOtherType.Name = "dockPanelMedicineOtherType";
            this.dockPanelMedicineOtherType.Options.ShowCloseButton = false;
            this.dockPanelMedicineOtherType.Options.ShowMaximizeButton = false;
            this.dockPanelMedicineOtherType.OriginalSize = new System.Drawing.Size(192, 410);
            this.dockPanelMedicineOtherType.Size = new System.Drawing.Size(192, 410);
            this.dockPanelMedicineOtherType.Text = "ОУ ангилал";
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.treeListMedicineTypeInter);
            this.controlContainer1.Controls.Add(this.panelControl4);
            this.controlContainer1.Location = new System.Drawing.Point(0, 0);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(192, 410);
            this.controlContainer1.TabIndex = 0;
            // 
            // treeListMedicineTypeInter
            // 
            this.treeListMedicineTypeInter.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1,
            this.treeListColumnName});
            this.treeListMedicineTypeInter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListMedicineTypeInter.KeyFieldName = "id";
            this.treeListMedicineTypeInter.Location = new System.Drawing.Point(0, 0);
            this.treeListMedicineTypeInter.Name = "treeListMedicineTypeInter";
            this.treeListMedicineTypeInter.OptionsBehavior.Editable = false;
            this.treeListMedicineTypeInter.ParentFieldName = "parentId";
            this.treeListMedicineTypeInter.Size = new System.Drawing.Size(192, 382);
            this.treeListMedicineTypeInter.TabIndex = 9;
            this.treeListMedicineTypeInter.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeListMedicineTypeInter_FocusedNodeChanged);
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Эмийн код";
            this.treeListColumn1.FieldName = "code";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 25;
            // 
            // treeListColumnName
            // 
            this.treeListColumnName.Caption = "Ангилал";
            this.treeListColumnName.FieldName = "name";
            this.treeListColumnName.Name = "treeListColumnName";
            this.treeListColumnName.OptionsColumn.AllowMove = false;
            this.treeListColumnName.Visible = true;
            this.treeListColumnName.VisibleIndex = 1;
            this.treeListColumnName.Width = 149;
            // 
            // panelControl4
            // 
            this.panelControl4.Controls.Add(this.controlNavigatorInternational);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl4.Location = new System.Drawing.Point(0, 382);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(192, 28);
            this.panelControl4.TabIndex = 8;
            // 
            // controlNavigatorInternational
            // 
            this.controlNavigatorInternational.Buttons.Append.Visible = false;
            this.controlNavigatorInternational.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorInternational.Buttons.Edit.Visible = false;
            this.controlNavigatorInternational.Buttons.EndEdit.Visible = false;
            this.controlNavigatorInternational.Buttons.First.Visible = false;
            this.controlNavigatorInternational.Buttons.Last.Visible = false;
            this.controlNavigatorInternational.Buttons.Next.Visible = false;
            this.controlNavigatorInternational.Buttons.NextPage.Visible = false;
            this.controlNavigatorInternational.Buttons.Prev.Visible = false;
            this.controlNavigatorInternational.Buttons.PrevPage.Visible = false;
            this.controlNavigatorInternational.Buttons.Remove.Visible = false;
            this.controlNavigatorInternational.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 17, true, true, "Задлах", "expand"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "edit")});
            this.controlNavigatorInternational.Location = new System.Drawing.Point(0, 1);
            this.controlNavigatorInternational.Name = "controlNavigatorInternational";
            this.controlNavigatorInternational.ShowToolTips = true;
            this.controlNavigatorInternational.Size = new System.Drawing.Size(90, 24);
            this.controlNavigatorInternational.TabIndex = 8;
            this.controlNavigatorInternational.Text = "controlNavigator1";
            this.controlNavigatorInternational.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.controlNavigatorInternational_ButtonClick);
            // 
            // dockPanelPackage
            // 
            this.dockPanelPackage.Controls.Add(this.dockPanel3_Container);
            this.dockPanelPackage.DockedAsTabbedDocument = true;
            this.dockPanelPackage.FloatLocation = new System.Drawing.Point(357, 152);
            this.dockPanelPackage.ID = new System.Guid("67c0010f-47af-46c8-ae38-c53fc4c7bbc6");
            this.dockPanelPackage.Name = "dockPanelPackage";
            this.dockPanelPackage.Options.ShowCloseButton = false;
            this.dockPanelPackage.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelPackage.SavedIndex = 1;
            this.dockPanelPackage.SavedMdiDocument = true;
            this.dockPanelPackage.Text = "Тендерийн багц";
            // 
            // dockPanel3_Container
            // 
            this.dockPanel3_Container.Controls.Add(this.panelControl2);
            this.dockPanel3_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel3_Container.Name = "dockPanel3_Container";
            this.dockPanel3_Container.Size = new System.Drawing.Size(578, 436);
            this.dockPanel3_Container.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControlPackage);
            this.panelControl2.Controls.Add(this.panelControl7);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(578, 436);
            this.panelControl2.TabIndex = 9;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.simpleButton2);
            this.panelControl7.Controls.Add(this.dropDownButton2);
            this.panelControl7.Controls.Add(this.reloadPack);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(2, 2);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(574, 50);
            this.panelControl7.TabIndex = 8;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(541, 10);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(23, 23);
            this.simpleButton2.TabIndex = 8;
            this.simpleButton2.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            // 
            // dropDownButton2
            // 
            this.dropDownButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButton2.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButton2.Image")));
            this.dropDownButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButton2.Location = new System.Drawing.Point(405, 10);
            this.dropDownButton2.Name = "dropDownButton2";
            this.dropDownButton2.Size = new System.Drawing.Size(130, 23);
            this.dropDownButton2.TabIndex = 6;
            this.dropDownButton2.Text = "Хэвлэх";
            // 
            // reloadPack
            // 
            this.reloadPack.Image = ((System.Drawing.Image)(resources.GetObject("reloadPack.Image")));
            this.reloadPack.Location = new System.Drawing.Point(12, 10);
            this.reloadPack.Name = "reloadPack";
            this.reloadPack.Size = new System.Drawing.Size(74, 23);
            this.reloadPack.TabIndex = 4;
            this.reloadPack.Text = "Сэргээх";
            this.reloadPack.ToolTip = "Сэргээх";
            this.reloadPack.Click += new System.EventHandler(this.reloadPack_Click);
            // 
            // dockPanelMedicineConf
            // 
            this.dockPanelMedicineConf.Controls.Add(this.dockPanel4_Container);
            this.dockPanelMedicineConf.DockedAsTabbedDocument = true;
            this.dockPanelMedicineConf.FloatLocation = new System.Drawing.Point(402, 170);
            this.dockPanelMedicineConf.ID = new System.Guid("ab779e40-1095-4577-adb5-07772efc9c5d");
            this.dockPanelMedicineConf.Name = "dockPanelMedicineConf";
            this.dockPanelMedicineConf.Options.ShowCloseButton = false;
            this.dockPanelMedicineConf.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelMedicineConf.SavedIndex = 2;
            this.dockPanelMedicineConf.SavedMdiDocument = true;
            this.dockPanelMedicineConf.Text = "Эмийн мэдээлэл";
            // 
            // dockPanel4_Container
            // 
            this.dockPanel4_Container.Controls.Add(this.panelControl1);
            this.dockPanel4_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel4_Container.Name = "dockPanel4_Container";
            this.dockPanel4_Container.Size = new System.Drawing.Size(578, 436);
            this.dockPanel4_Container.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.xtraTabControlReference);
            this.panelControl1.Controls.Add(this.panelControl6);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(578, 436);
            this.panelControl1.TabIndex = 17;
            // 
            // xtraTabControlReference
            // 
            this.xtraTabControlReference.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlReference.Location = new System.Drawing.Point(2, 52);
            this.xtraTabControlReference.Name = "xtraTabControlReference";
            this.xtraTabControlReference.SelectedTabPage = this.xtraTabPageProducer;
            this.xtraTabControlReference.Size = new System.Drawing.Size(574, 382);
            this.xtraTabControlReference.TabIndex = 15;
            this.xtraTabControlReference.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageCountry,
            this.xtraTabPageProducer,
            this.xtraTabPageUnit,
            this.xtraTabPageShape,
            this.xtraTabPageQuantity});
            // 
            // xtraTabPageProducer
            // 
            this.xtraTabPageProducer.Controls.Add(this.gridControlProducer);
            this.xtraTabPageProducer.Name = "xtraTabPageProducer";
            this.xtraTabPageProducer.Size = new System.Drawing.Size(568, 354);
            this.xtraTabPageProducer.Text = "Үйлдвэрлэгч";
            // 
            // gridControlProducer
            // 
            this.gridControlProducer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlProducer.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlProducer.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlProducer.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlProducer.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlProducer.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlProducer.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlProducer.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlProducer_EmbeddedNavigator_ButtonClick);
            this.gridControlProducer.Location = new System.Drawing.Point(0, 0);
            this.gridControlProducer.MainView = this.gridViewProducer;
            this.gridControlProducer.Name = "gridControlProducer";
            this.gridControlProducer.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemGridLookUpEditCountry});
            this.gridControlProducer.Size = new System.Drawing.Size(568, 354);
            this.gridControlProducer.TabIndex = 3;
            this.gridControlProducer.UseEmbeddedNavigator = true;
            this.gridControlProducer.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProducer});
            // 
            // gridViewProducer
            // 
            this.gridViewProducer.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnCountryName,
            this.gridColumn4,
            this.gridColumn2});
            this.gridViewProducer.GridControl = this.gridControlProducer;
            this.gridViewProducer.Name = "gridViewProducer";
            this.gridViewProducer.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewProducer.OptionsBehavior.ReadOnly = true;
            this.gridViewProducer.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewProducer.OptionsFind.AllowFindPanel = false;
            this.gridViewProducer.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewProducer.OptionsView.ShowAutoFilterRow = true;
            this.gridViewProducer.OptionsView.ShowGroupPanel = false;
            this.gridViewProducer.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewProducer_InvalidRowException);
            this.gridViewProducer.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewProducer_ValidateRow);
            this.gridViewProducer.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewProducer_RowUpdated);
            // 
            // gridColumnCountryName
            // 
            this.gridColumnCountryName.Caption = "Улс";
            this.gridColumnCountryName.FieldName = "countryName";
            this.gridColumnCountryName.Name = "gridColumnCountryName";
            this.gridColumnCountryName.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Үйлдвэрлэгч";
            this.gridColumn4.FieldName = "name";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Улс";
            this.gridColumn2.ColumnEdit = this.repositoryItemGridLookUpEditCountry;
            this.gridColumn2.FieldName = "countryID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // repositoryItemGridLookUpEditCountry
            // 
            this.repositoryItemGridLookUpEditCountry.AutoHeight = false;
            this.repositoryItemGridLookUpEditCountry.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditCountry.DisplayMember = "name";
            this.repositoryItemGridLookUpEditCountry.Name = "repositoryItemGridLookUpEditCountry";
            this.repositoryItemGridLookUpEditCountry.NullText = "";
            this.repositoryItemGridLookUpEditCountry.ValueMember = "id";
            this.repositoryItemGridLookUpEditCountry.View = this.gridView2;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Улсын жагсаалт";
            this.gridColumn3.FieldName = "name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // xtraTabPageCountry
            // 
            this.xtraTabPageCountry.Controls.Add(this.gridControlCountry);
            this.xtraTabPageCountry.Name = "xtraTabPageCountry";
            this.xtraTabPageCountry.Size = new System.Drawing.Size(184, 93);
            this.xtraTabPageCountry.Text = "Улс";
            // 
            // gridControlCountry
            // 
            this.gridControlCountry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCountry.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlCountry.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlCountry.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlCountry.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlCountry.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlCountry.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlCountry.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlCountry_EmbeddedNavigator_ButtonClick);
            this.gridControlCountry.Location = new System.Drawing.Point(0, 0);
            this.gridControlCountry.MainView = this.gridViewCountry;
            this.gridControlCountry.Name = "gridControlCountry";
            this.gridControlCountry.Size = new System.Drawing.Size(184, 93);
            this.gridControlCountry.TabIndex = 0;
            this.gridControlCountry.UseEmbeddedNavigator = true;
            this.gridControlCountry.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCountry});
            // 
            // gridViewCountry
            // 
            this.gridViewCountry.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnCountry});
            this.gridViewCountry.GridControl = this.gridControlCountry;
            this.gridViewCountry.Name = "gridViewCountry";
            this.gridViewCountry.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewCountry.OptionsBehavior.ReadOnly = true;
            this.gridViewCountry.OptionsCustomization.AllowGroup = false;
            this.gridViewCountry.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewCountry.OptionsFind.AllowFindPanel = false;
            this.gridViewCountry.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewCountry.OptionsView.ShowAutoFilterRow = true;
            this.gridViewCountry.OptionsView.ShowGroupPanel = false;
            this.gridViewCountry.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewCountry_InvalidRowException);
            this.gridViewCountry.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewCountry_ValidateRow);
            this.gridViewCountry.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewCountry_RowUpdated);
            // 
            // gridColumnCountry
            // 
            this.gridColumnCountry.Caption = "Улсын нэрс";
            this.gridColumnCountry.FieldName = "name";
            this.gridColumnCountry.FieldNameSortGroup = "name";
            this.gridColumnCountry.Name = "gridColumnCountry";
            this.gridColumnCountry.OptionsColumn.AllowMove = false;
            this.gridColumnCountry.Visible = true;
            this.gridColumnCountry.VisibleIndex = 0;
            this.gridColumnCountry.Width = 627;
            // 
            // xtraTabPageUnit
            // 
            this.xtraTabPageUnit.Controls.Add(this.gridControlUnit);
            this.xtraTabPageUnit.Name = "xtraTabPageUnit";
            this.xtraTabPageUnit.Size = new System.Drawing.Size(184, 93);
            this.xtraTabPageUnit.Text = "Тун хэмжээ";
            // 
            // gridControlUnit
            // 
            this.gridControlUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlUnit.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlUnit.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlUnit.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlUnit.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlUnit.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlUnit.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlUnit.EmbeddedNavigator.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlUnit.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlUnit.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlUnit_EmbeddedNavigator_ButtonClick);
            this.gridControlUnit.Location = new System.Drawing.Point(0, 0);
            this.gridControlUnit.MainView = this.gridViewUnit;
            this.gridControlUnit.Name = "gridControlUnit";
            this.gridControlUnit.Size = new System.Drawing.Size(184, 93);
            this.gridControlUnit.TabIndex = 0;
            this.gridControlUnit.UseEmbeddedNavigator = true;
            this.gridControlUnit.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewUnit});
            // 
            // gridViewUnit
            // 
            this.gridViewUnit.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnUnitName});
            this.gridViewUnit.GridControl = this.gridControlUnit;
            this.gridViewUnit.Name = "gridViewUnit";
            this.gridViewUnit.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewUnit.OptionsBehavior.ReadOnly = true;
            this.gridViewUnit.OptionsCustomization.AllowGroup = false;
            this.gridViewUnit.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewUnit.OptionsFind.AllowFindPanel = false;
            this.gridViewUnit.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewUnit.OptionsView.ShowAutoFilterRow = true;
            this.gridViewUnit.OptionsView.ShowGroupPanel = false;
            this.gridViewUnit.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewUnit_InvalidRowException);
            this.gridViewUnit.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewUnit_ValidateRow);
            this.gridViewUnit.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewUnit_RowUpdated);
            // 
            // gridColumnUnitName
            // 
            this.gridColumnUnitName.Caption = "Тун хэмжээ";
            this.gridColumnUnitName.FieldName = "name";
            this.gridColumnUnitName.Name = "gridColumnUnitName";
            this.gridColumnUnitName.OptionsColumn.AllowMove = false;
            this.gridColumnUnitName.Visible = true;
            this.gridColumnUnitName.VisibleIndex = 0;
            // 
            // xtraTabPageShape
            // 
            this.xtraTabPageShape.Controls.Add(this.gridControlShape);
            this.xtraTabPageShape.Name = "xtraTabPageShape";
            this.xtraTabPageShape.Size = new System.Drawing.Size(184, 93);
            this.xtraTabPageShape.Text = "Хэлбэр";
            // 
            // gridControlShape
            // 
            this.gridControlShape.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlShape.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlShape.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlShape.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlShape.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlShape.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlShape.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlShape.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlShape_EmbeddedNavigator_ButtonClick);
            this.gridControlShape.Location = new System.Drawing.Point(0, 0);
            this.gridControlShape.MainView = this.gridViewShape;
            this.gridControlShape.Name = "gridControlShape";
            this.gridControlShape.Size = new System.Drawing.Size(184, 93);
            this.gridControlShape.TabIndex = 0;
            this.gridControlShape.UseEmbeddedNavigator = true;
            this.gridControlShape.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewShape});
            // 
            // gridViewShape
            // 
            this.gridViewShape.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnShapeName});
            this.gridViewShape.GridControl = this.gridControlShape;
            this.gridViewShape.Name = "gridViewShape";
            this.gridViewShape.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewShape.OptionsBehavior.ReadOnly = true;
            this.gridViewShape.OptionsCustomization.AllowGroup = false;
            this.gridViewShape.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewShape.OptionsFind.AllowFindPanel = false;
            this.gridViewShape.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewShape.OptionsView.ShowAutoFilterRow = true;
            this.gridViewShape.OptionsView.ShowGroupPanel = false;
            this.gridViewShape.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewShape_InvalidRowException);
            this.gridViewShape.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewShape_ValidateRow);
            this.gridViewShape.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewShape_RowUpdated);
            // 
            // gridColumnShapeName
            // 
            this.gridColumnShapeName.Caption = "Хэлбэр";
            this.gridColumnShapeName.FieldName = "name";
            this.gridColumnShapeName.Name = "gridColumnShapeName";
            this.gridColumnShapeName.OptionsColumn.AllowMove = false;
            this.gridColumnShapeName.Visible = true;
            this.gridColumnShapeName.VisibleIndex = 0;
            // 
            // xtraTabPageQuantity
            // 
            this.xtraTabPageQuantity.Controls.Add(this.gridControlQuantity);
            this.xtraTabPageQuantity.Name = "xtraTabPageQuantity";
            this.xtraTabPageQuantity.Size = new System.Drawing.Size(184, 93);
            this.xtraTabPageQuantity.Text = "Хэмжих нэгж";
            // 
            // gridControlQuantity
            // 
            this.gridControlQuantity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlQuantity.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlQuantity.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlQuantity.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlQuantity.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlQuantity.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlQuantity.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlQuantity.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlQuantity_EmbeddedNavigator_ButtonClick);
            this.gridControlQuantity.Location = new System.Drawing.Point(0, 0);
            this.gridControlQuantity.MainView = this.gridViewQuantity;
            this.gridControlQuantity.Name = "gridControlQuantity";
            this.gridControlQuantity.Size = new System.Drawing.Size(184, 93);
            this.gridControlQuantity.TabIndex = 1;
            this.gridControlQuantity.UseEmbeddedNavigator = true;
            this.gridControlQuantity.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewQuantity});
            // 
            // gridViewQuantity
            // 
            this.gridViewQuantity.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnQuantityName});
            this.gridViewQuantity.GridControl = this.gridControlQuantity;
            this.gridViewQuantity.Name = "gridViewQuantity";
            this.gridViewQuantity.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewQuantity.OptionsBehavior.ReadOnly = true;
            this.gridViewQuantity.OptionsCustomization.AllowGroup = false;
            this.gridViewQuantity.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewQuantity.OptionsFind.AllowFindPanel = false;
            this.gridViewQuantity.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewQuantity.OptionsView.ShowAutoFilterRow = true;
            this.gridViewQuantity.OptionsView.ShowGroupPanel = false;
            this.gridViewQuantity.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewQuantity_InvalidRowException);
            this.gridViewQuantity.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewQuantity_ValidateRow);
            this.gridViewQuantity.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewQuantity_RowUpdated);
            // 
            // gridColumnQuantityName
            // 
            this.gridColumnQuantityName.Caption = "Хэмжих нэгж";
            this.gridColumnQuantityName.FieldName = "name";
            this.gridColumnQuantityName.Name = "gridColumnQuantityName";
            this.gridColumnQuantityName.OptionsColumn.AllowMove = false;
            this.gridColumnQuantityName.Visible = true;
            this.gridColumnQuantityName.VisibleIndex = 0;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.simpleButtonLayout);
            this.panelControl6.Controls.Add(this.simpleButtonConvert);
            this.panelControl6.Controls.Add(this.reloadButtonRef);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl6.Location = new System.Drawing.Point(2, 2);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(574, 50);
            this.panelControl6.TabIndex = 16;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(539, 12);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 7;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.buttonLayout_Click);
            // 
            // simpleButtonConvert
            // 
            this.simpleButtonConvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonConvert.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonConvert.Image")));
            this.simpleButtonConvert.Location = new System.Drawing.Point(403, 12);
            this.simpleButtonConvert.Name = "simpleButtonConvert";
            this.simpleButtonConvert.Size = new System.Drawing.Size(130, 23);
            this.simpleButtonConvert.TabIndex = 5;
            this.simpleButtonConvert.Text = "Хөрвүүлэх";
            this.simpleButtonConvert.ToolTip = "Эмийн лавлах мэдээллийг хөрвүүлэх";
            this.simpleButtonConvert.Click += new System.EventHandler(this.simpleButtonConvert_Click);
            // 
            // reloadButtonRef
            // 
            this.reloadButtonRef.Image = ((System.Drawing.Image)(resources.GetObject("reloadButtonRef.Image")));
            this.reloadButtonRef.Location = new System.Drawing.Point(12, 12);
            this.reloadButtonRef.Name = "reloadButtonRef";
            this.reloadButtonRef.Size = new System.Drawing.Size(75, 23);
            this.reloadButtonRef.TabIndex = 0;
            this.reloadButtonRef.Text = "Сэргээх";
            this.reloadButtonRef.ToolTip = "Эмийн лавлах мэдээллийг сэргээх";
            this.reloadButtonRef.Click += new System.EventHandler(this.reloadButtonRef_Click);
            // 
            // dockPanelPackageMedicine
            // 
            this.dockPanelPackageMedicine.Controls.Add(this.controlContainer2);
            this.dockPanelPackageMedicine.DockedAsTabbedDocument = true;
            this.dockPanelPackageMedicine.FloatLocation = new System.Drawing.Point(309, 153);
            this.dockPanelPackageMedicine.ID = new System.Guid("9a7e9038-c36b-417c-9832-213fb6651809");
            this.dockPanelPackageMedicine.Name = "dockPanelPackageMedicine";
            this.dockPanelPackageMedicine.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelPackageMedicine.SavedIndex = 4;
            this.dockPanelPackageMedicine.SavedMdiDocument = true;
            this.dockPanelPackageMedicine.Text = "Эмийн багц";
            // 
            // controlContainer2
            // 
            this.controlContainer2.Controls.Add(this.gridControlPackageMedicine);
            this.controlContainer2.Controls.Add(this.panelControl10);
            this.controlContainer2.Location = new System.Drawing.Point(0, 0);
            this.controlContainer2.Name = "controlContainer2";
            this.controlContainer2.Size = new System.Drawing.Size(578, 436);
            this.controlContainer2.TabIndex = 0;
            // 
            // gridControlPackageMedicine
            // 
            this.gridControlPackageMedicine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPackageMedicine.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPackageMedicine.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPackageMedicine.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPackageMedicine.EmbeddedNavigator.Buttons.EnabledAutoRepeat = false;
            this.gridControlPackageMedicine.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPackageMedicine.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlPackageMedicine.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPackageMedicine.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "edit")});
            this.gridControlPackageMedicine.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControlPackageMedicine.Location = new System.Drawing.Point(0, 50);
            this.gridControlPackageMedicine.LookAndFeel.SkinName = "Office 2013";
            this.gridControlPackageMedicine.MainView = this.gridViewPackageMedicine;
            this.gridControlPackageMedicine.Name = "gridControlPackageMedicine";
            this.gridControlPackageMedicine.Size = new System.Drawing.Size(578, 386);
            this.gridControlPackageMedicine.TabIndex = 10;
            this.gridControlPackageMedicine.UseEmbeddedNavigator = true;
            this.gridControlPackageMedicine.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPackageMedicine,
            this.gridView3});
            this.gridControlPackageMedicine.Click += new System.EventHandler(this.gridControlPackageMedicine_Click);
            // 
            // gridViewPackageMedicine
            // 
            this.gridViewPackageMedicine.ActiveFilterEnabled = false;
            this.gridViewPackageMedicine.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn35,
            this.gridColumn12,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31});
            this.gridViewPackageMedicine.GridControl = this.gridControlPackageMedicine;
            this.gridViewPackageMedicine.Name = "gridViewPackageMedicine";
            this.gridViewPackageMedicine.OptionsBehavior.Editable = false;
            this.gridViewPackageMedicine.OptionsBehavior.ReadOnly = true;
            this.gridViewPackageMedicine.OptionsCustomization.AllowGroup = false;
            this.gridViewPackageMedicine.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewPackageMedicine.OptionsFind.AllowFindPanel = false;
            this.gridViewPackageMedicine.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewPackageMedicine.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPackageMedicine.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Нэр";
            this.gridColumn1.CustomizationCaption = "Эмийн нэр";
            this.gridColumn1.FieldName = "name";
            this.gridColumn1.FieldNameSortGroup = "name";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 118;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Эмийн ангилал";
            this.gridColumn35.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn35.FieldName = "medicineTypeName";
            this.gridColumn35.MinWidth = 75;
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 1;
            this.gridColumn35.Width = 114;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Олон улсын ангилал";
            this.gridColumn12.CustomizationCaption = "Олон улсын ангилал";
            this.gridColumn12.FieldName = "medicineTypeInterName";
            this.gridColumn12.MinWidth = 75;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 158;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Хэлбэр";
            this.gridColumn29.CustomizationCaption = "Хэлбэр";
            this.gridColumn29.FieldName = "shapeName";
            this.gridColumn29.MinWidth = 75;
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 3;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Тун хэмжээ";
            this.gridColumn30.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn30.FieldName = "unitName";
            this.gridColumn30.MinWidth = 75;
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 4;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Хэмжих нэгж";
            this.gridColumn31.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn31.FieldName = "quantityName";
            this.gridColumn31.MinWidth = 75;
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.FixedWidth = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 5;
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.gridControlPackageMedicine;
            this.gridView3.Name = "gridView3";
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.simpleButton3);
            this.panelControl10.Controls.Add(this.dropDownButton3);
            this.panelControl10.Controls.Add(this.simpleButton4);
            this.panelControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl10.Location = new System.Drawing.Point(0, 0);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(578, 50);
            this.panelControl10.TabIndex = 9;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton3.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(545, 10);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(23, 23);
            this.simpleButton3.TabIndex = 8;
            this.simpleButton3.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            // 
            // dropDownButton3
            // 
            this.dropDownButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButton3.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButton3.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButton3.Image")));
            this.dropDownButton3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButton3.Location = new System.Drawing.Point(409, 10);
            this.dropDownButton3.Name = "dropDownButton3";
            this.dropDownButton3.Size = new System.Drawing.Size(130, 23);
            this.dropDownButton3.TabIndex = 6;
            this.dropDownButton3.Text = "Хэвлэх";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(12, 10);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(74, 23);
            this.simpleButton4.TabIndex = 4;
            this.simpleButton4.Text = "Сэргээх";
            this.simpleButton4.ToolTip = "Сэргээх";
            // 
            // dockPanelMedic
            // 
            this.dockPanelMedic.Controls.Add(this.dockPanel5_Container);
            this.dockPanelMedic.DockedAsTabbedDocument = true;
            this.dockPanelMedic.FloatLocation = new System.Drawing.Point(255, 154);
            this.dockPanelMedic.ID = new System.Guid("9e434596-1c93-4bc9-ae47-bcd9e6cd30dd");
            this.dockPanelMedic.Name = "dockPanelMedic";
            this.dockPanelMedic.Options.ShowCloseButton = false;
            this.dockPanelMedic.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelMedic.Text = "Эмийн бүртгэл";
            // 
            // dockPanel5_Container
            // 
            this.dockPanel5_Container.Controls.Add(this.panelControl8);
            this.dockPanel5_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel5_Container.Name = "dockPanel5_Container";
            this.dockPanel5_Container.Size = new System.Drawing.Size(578, 436);
            this.dockPanel5_Container.TabIndex = 0;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.gridControlMedicine);
            this.panelControl8.Controls.Add(this.panelControl9);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(0, 0);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(578, 436);
            this.panelControl8.TabIndex = 0;
            // 
            // gridControlMedicine
            // 
            this.gridControlMedicine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMedicine.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlMedicine.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlMedicine.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlMedicine.EmbeddedNavigator.Buttons.EnabledAutoRepeat = false;
            this.gridControlMedicine.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlMedicine.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlMedicine.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlMedicine.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "edit")});
            this.gridControlMedicine.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlMedicine_EmbeddedNavigator_ButtonClick);
            this.gridControlMedicine.Location = new System.Drawing.Point(2, 52);
            this.gridControlMedicine.LookAndFeel.SkinName = "Office 2013";
            this.gridControlMedicine.MainView = this.gridViewMedicine;
            this.gridControlMedicine.Name = "gridControlMedicine";
            this.gridControlMedicine.Size = new System.Drawing.Size(574, 382);
            this.gridControlMedicine.TabIndex = 8;
            this.gridControlMedicine.UseEmbeddedNavigator = true;
            this.gridControlMedicine.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMedicine,
            this.gridView4});
            // 
            // gridViewMedicine
            // 
            this.gridViewMedicine.ActiveFilterEnabled = false;
            this.gridViewMedicine.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn16,
            this.gridColumn8,
            this.gridColumn13,
            this.gridColumn15,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn19});
            this.gridViewMedicine.GridControl = this.gridControlMedicine;
            this.gridViewMedicine.Name = "gridViewMedicine";
            this.gridViewMedicine.OptionsBehavior.Editable = false;
            this.gridViewMedicine.OptionsBehavior.ReadOnly = true;
            this.gridViewMedicine.OptionsCustomization.AllowGroup = false;
            this.gridViewMedicine.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewMedicine.OptionsFind.AllowFindPanel = false;
            this.gridViewMedicine.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewMedicine.OptionsView.ShowAutoFilterRow = true;
            this.gridViewMedicine.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Нэр";
            this.gridColumn6.CustomizationCaption = "Эмийн нэр";
            this.gridColumn6.FieldName = "name";
            this.gridColumn6.FieldNameSortGroup = "name";
            this.gridColumn6.MinWidth = 100;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 100;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Олон улсын нэр";
            this.gridColumn7.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn7.FieldName = "latinName";
            this.gridColumn7.FieldNameSortGroup = "latinName";
            this.gridColumn7.MinWidth = 100;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            this.gridColumn7.Width = 100;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Эмийн багц";
            this.gridColumn16.CustomizationCaption = "Эмийн багц";
            this.gridColumn16.FieldName = "PackageMedicineName";
            this.gridColumn16.MinWidth = 100;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 2;
            this.gridColumn16.Width = 100;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Хэлбэр";
            this.gridColumn8.CustomizationCaption = "Хэлбэр";
            this.gridColumn8.FieldName = "shapeName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Тун хэмжээ";
            this.gridColumn13.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn13.FieldName = "unitName";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 4;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Хэмжих нэгж";
            this.gridColumn15.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn15.FieldName = "quantityName";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.FixedWidth = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 5;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Хүчинтэй хугацаа";
            this.gridColumn17.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn17.FieldName = "validDate";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 6;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Сериал";
            this.gridColumn18.CustomizationCaption = "Сериал";
            this.gridColumn18.FieldName = "serial";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 7;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Бар код";
            this.gridColumn20.CustomizationCaption = "Бар код";
            this.gridColumn20.FieldName = "barcode";
            this.gridColumn20.MinWidth = 75;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 9;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Фармакопейн стандарт";
            this.gridColumn21.CustomizationCaption = "Фармакопейн стандарт";
            this.gridColumn21.FieldName = "parmaStandart";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 10;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Тайлбар";
            this.gridColumn22.CustomizationCaption = "Тайлбар";
            this.gridColumn22.FieldName = "description";
            this.gridColumn22.MinWidth = 100;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 11;
            this.gridColumn22.Width = 100;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Улсын нэр";
            this.gridColumn23.CustomizationCaption = "Улсын нэр";
            this.gridColumn23.FieldName = "producerCountryName";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 12;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Үйлдвэрлэгч";
            this.gridColumn24.CustomizationCaption = "Үйлдвэрлэгч";
            this.gridColumn24.FieldName = "producerName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 13;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Бүртгэлийн дугаар";
            this.gridColumn25.CustomizationCaption = "Бүртгэлийн дугаар";
            this.gridColumn25.FieldName = "registerID";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 14;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "БТКУС дугаар";
            this.gridColumn26.CustomizationCaption = "БТКУС дугаар";
            this.gridColumn26.FieldName = "btkusID";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 15;
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.gridControlMedicine;
            this.gridView4.Name = "gridView4";
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.buttonSaveLayout);
            this.panelControl9.Controls.Add(this.dropDownButtonPrint);
            this.panelControl9.Controls.Add(this.simpleButtonReloadMedicine);
            this.panelControl9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl9.Location = new System.Drawing.Point(2, 2);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(574, 50);
            this.panelControl9.TabIndex = 9;
            // 
            // buttonSaveLayout
            // 
            this.buttonSaveLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveLayout.Image = ((System.Drawing.Image)(resources.GetObject("buttonSaveLayout.Image")));
            this.buttonSaveLayout.Location = new System.Drawing.Point(541, 10);
            this.buttonSaveLayout.Name = "buttonSaveLayout";
            this.buttonSaveLayout.Size = new System.Drawing.Size(23, 23);
            this.buttonSaveLayout.TabIndex = 7;
            this.buttonSaveLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.buttonSaveLayout.Click += new System.EventHandler(this.buttonSaveLayout_Click);
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.DropDownControl = this.popupMenu;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(405, 10);
            this.dropDownButtonPrint.MenuManager = this.barManager;
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.barManager.SetPopupContextMenu(this.dropDownButtonPrint, this.popupMenu);
            this.dropDownButtonPrint.Size = new System.Drawing.Size(130, 23);
            this.dropDownButtonPrint.TabIndex = 5;
            this.dropDownButtonPrint.Text = "Хөрвүүлэх";
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemMedicineType),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemMedicineTypeInter),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemEndBal),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // barButtonItemMedicineType
            // 
            this.barButtonItemMedicineType.Caption = "Эмийн ангилал";
            this.barButtonItemMedicineType.Description = "Эмийн ангилал хөрвүүлэх";
            this.barButtonItemMedicineType.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemMedicineType.Glyph")));
            this.barButtonItemMedicineType.Id = 0;
            this.barButtonItemMedicineType.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemMedicineType.LargeGlyph")));
            this.barButtonItemMedicineType.Name = "barButtonItemMedicineType";
            this.barButtonItemMedicineType.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemMedicineType_ItemClick);
            // 
            // barButtonItemMedicineTypeInter
            // 
            this.barButtonItemMedicineTypeInter.Caption = "ОУ ангилал";
            this.barButtonItemMedicineTypeInter.Description = "Олон улсын ангилал хөрвүүлэх";
            this.barButtonItemMedicineTypeInter.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemMedicineTypeInter.Glyph")));
            this.barButtonItemMedicineTypeInter.Id = 1;
            this.barButtonItemMedicineTypeInter.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemMedicineTypeInter.LargeGlyph")));
            this.barButtonItemMedicineTypeInter.Name = "barButtonItemMedicineTypeInter";
            this.barButtonItemMedicineTypeInter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemMedicineTypeInter_ItemClick);
            // 
            // barButtonItemEndBal
            // 
            this.barButtonItemEndBal.Caption = "Эцсийн үлдэгдэл";
            this.barButtonItemEndBal.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.Glyph")));
            this.barButtonItemEndBal.Id = 2;
            this.barButtonItemEndBal.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.LargeGlyph")));
            this.barButtonItemEndBal.Name = "barButtonItemEndBal";
            this.barButtonItemEndBal.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemEndBal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEndBal_ItemClick);
            // 
            // barButtonItemConvert
            // 
            this.barButtonItemConvert.Caption = "Эмийн бүртгэл";
            this.barButtonItemConvert.Description = "Эмийн бүртгэл хөрвүүлэх";
            this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
            this.barButtonItemConvert.Id = 3;
            this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
            this.barButtonItemConvert.Name = "barButtonItemConvert";
            this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemMedicineType,
            this.barButtonItemMedicineTypeInter,
            this.barButtonItemEndBal,
            this.barButtonItemConvert});
            this.barManager.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 464);
            this.barDockControlBottom.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 464);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(784, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 464);
            // 
            // simpleButtonReloadMedicine
            // 
            this.simpleButtonReloadMedicine.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReloadMedicine.Image")));
            this.simpleButtonReloadMedicine.Location = new System.Drawing.Point(5, 10);
            this.simpleButtonReloadMedicine.Name = "simpleButtonReloadMedicine";
            this.simpleButtonReloadMedicine.Size = new System.Drawing.Size(80, 23);
            this.simpleButtonReloadMedicine.TabIndex = 4;
            this.simpleButtonReloadMedicine.Text = "Сэргээх";
            this.simpleButtonReloadMedicine.ToolTip = "Эмийн бүртгэлийн мэдээллийг сэргээх";
            this.simpleButtonReloadMedicine.Click += new System.EventHandler(this.simpleButtonReloadMedicine_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.simpleButton1);
            this.panelControl5.Controls.Add(this.dropDownButton1);
            this.panelControl5.Controls.Add(this.simpleButtonReload);
            this.panelControl5.Location = new System.Drawing.Point(202, 205);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(582, 50);
            this.panelControl5.TabIndex = 2;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(549, 10);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(23, 23);
            this.simpleButton1.TabIndex = 8;
            this.simpleButton1.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            // 
            // dropDownButton1
            // 
            this.dropDownButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButton1.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButton1.Image")));
            this.dropDownButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButton1.Location = new System.Drawing.Point(413, 10);
            this.dropDownButton1.Name = "dropDownButton1";
            this.dropDownButton1.Size = new System.Drawing.Size(130, 23);
            this.dropDownButton1.TabIndex = 6;
            this.dropDownButton1.Text = "Хэвлэх";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 10);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(74, 23);
            this.simpleButtonReload.TabIndex = 4;
            this.simpleButtonReload.Text = "Сэргээх";
            this.simpleButtonReload.ToolTip = "Сэргээх";
            // 
            // documentManager1
            // 
            this.documentManager1.ContainerControl = this;
            this.documentManager1.View = this.tabbedView1;
            this.documentManager1.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView1});
            // 
            // tabbedView1
            // 
            this.tabbedView1.DocumentGroups.AddRange(new DevExpress.XtraBars.Docking2010.Views.Tabbed.DocumentGroup[] {
            this.documentGroup1});
            this.tabbedView1.Documents.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseDocument[] {
            this.dockPanel1Document,
            this.dockPanel4Document,
            this.dockPanel5Document,
            this.dockPanel3Document});
            // 
            // documentGroup1
            // 
            this.documentGroup1.Items.AddRange(new DevExpress.XtraBars.Docking2010.Views.Tabbed.Document[] {
            this.dockPanel5Document,
            this.dockPanel4Document,
            this.dockPanel3Document,
            this.dockPanel1Document});
            // 
            // dockPanel5Document
            // 
            this.dockPanel5Document.Caption = "Эмийн бүртгэл";
            this.dockPanel5Document.ControlName = "dockPanelMedic";
            this.dockPanel5Document.FloatLocation = new System.Drawing.Point(255, 154);
            this.dockPanel5Document.FloatSize = new System.Drawing.Size(200, 200);
            this.dockPanel5Document.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.False;
            this.dockPanel5Document.Properties.AllowFloat = DevExpress.Utils.DefaultBoolean.True;
            this.dockPanel5Document.Properties.AllowFloatOnDoubleClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // dockPanel4Document
            // 
            this.dockPanel4Document.Caption = "Эмийн мэдээлэл";
            this.dockPanel4Document.ControlName = "dockPanelMedicineConf";
            this.dockPanel4Document.FloatLocation = new System.Drawing.Point(402, 170);
            this.dockPanel4Document.FloatSize = new System.Drawing.Size(200, 200);
            this.dockPanel4Document.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.False;
            this.dockPanel4Document.Properties.AllowFloat = DevExpress.Utils.DefaultBoolean.True;
            this.dockPanel4Document.Properties.AllowFloatOnDoubleClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // dockPanel3Document
            // 
            this.dockPanel3Document.Caption = "Тендерийн багц";
            this.dockPanel3Document.ControlName = "dockPanelPackage";
            this.dockPanel3Document.FloatLocation = new System.Drawing.Point(357, 152);
            this.dockPanel3Document.FloatSize = new System.Drawing.Size(200, 200);
            this.dockPanel3Document.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.False;
            this.dockPanel3Document.Properties.AllowFloat = DevExpress.Utils.DefaultBoolean.True;
            this.dockPanel3Document.Properties.AllowFloatOnDoubleClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // dockPanel1Document
            // 
            this.dockPanel1Document.Caption = "Эмийн багц";
            this.dockPanel1Document.ControlName = "dockPanelPackageMedicine";
            this.dockPanel1Document.FloatLocation = new System.Drawing.Point(309, 153);
            this.dockPanel1Document.FloatSize = new System.Drawing.Size(200, 200);
            this.dockPanel1Document.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.False;
            this.dockPanel1Document.Properties.AllowFloat = DevExpress.Utils.DefaultBoolean.True;
            this.dockPanel1Document.Properties.AllowFloatOnDoubleClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Үнэ";
            this.gridColumn19.CustomizationCaption = "Үнэ";
            this.gridColumn19.FieldName = "price";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 8;
            // 
            // MedicineForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 464);
            this.Controls.Add(this.panelContainer1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MedicineForm";
            this.ShowInTaskbar = false;
            this.Text = "Эмийн бүртгэл";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MedicineForm_FormClosing);
            this.Shown += new System.EventHandler(this.MedicineForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackageDetial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.panelContainer1.ResumeLayout(false);
            this.dockPanelMedicineType.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListMedicineType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.dockPanelMedicineOtherType.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListMedicineTypeInter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.dockPanelPackage.ResumeLayout(false);
            this.dockPanel3_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.dockPanelMedicineConf.ResumeLayout(false);
            this.dockPanel4_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlReference)).EndInit();
            this.xtraTabControlReference.ResumeLayout(false);
            this.xtraTabPageProducer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProducer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProducer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.xtraTabPageCountry.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCountry)).EndInit();
            this.xtraTabPageUnit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUnit)).EndInit();
            this.xtraTabPageShape.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlShape)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewShape)).EndInit();
            this.xtraTabPageQuantity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.dockPanelPackageMedicine.ResumeLayout(false);
            this.controlContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPackageMedicine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackageMedicine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.dockPanelMedic.ResumeLayout(false);
            this.dockPanel5_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMedicine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMedicine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel5Document)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel4Document)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel3Document)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockPanel1Document)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelMedicineType;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.ControlNavigator controlNavigatorMedicineType;
        private DevExpress.XtraBars.Docking.DockPanel panelContainer1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelMedicineOtherType;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.ControlNavigator controlNavigatorInternational;
        private DevExpress.XtraTreeList.TreeList treeListMedicineType;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraTreeList.TreeList treeListMedicineTypeInter;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnName;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemMedicineType;
        private DevExpress.XtraBars.BarButtonItem barButtonItemMedicineTypeInter;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEndBal;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlReference;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageProducer;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCountry;
        private DevExpress.XtraGrid.GridControl gridControlCountry;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCountry;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCountry;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageUnit;
        private DevExpress.XtraGrid.GridControl gridControlUnit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewUnit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnUnitName;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageShape;
        private DevExpress.XtraGrid.GridControl gridControlShape;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewShape;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnShapeName;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraEditors.SimpleButton simpleButtonConvert;
        private DevExpress.XtraEditors.SimpleButton reloadButtonRef;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageQuantity;
        private DevExpress.XtraGrid.GridControl gridControlQuantity;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnQuantityName;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.DropDownButton dropDownButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.DropDownButton dropDownButton2;
        private DevExpress.XtraEditors.SimpleButton reloadPack;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelPackage;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel3_Container;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        public DevExpress.XtraGrid.GridControl gridControlMedicine;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMedicine;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.SimpleButton buttonSaveLayout;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReloadMedicine;
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager1;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView1;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.DocumentGroup documentGroup1;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.Document dockPanel3Document;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelMedicineConf;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel4_Container;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.Document dockPanel4Document;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelMedic;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel5_Container;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.Document dockPanel5Document;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControlPackage;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPackageDetial;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewPackage;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCreatedDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnProducer;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnDesc;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnIsUse;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewDetail;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn24;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn26;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn27;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnSumPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraGrid.GridControl gridControlProducer;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProducer;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCountryName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditCountry;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelPackageMedicine;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer2;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.Document dockPanel1Document;
        public DevExpress.XtraGrid.GridControl gridControlPackageMedicine;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPackageMedicine;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.DropDownButton dropDownButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        public DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;

    }
}