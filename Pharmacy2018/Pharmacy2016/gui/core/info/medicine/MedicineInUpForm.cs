﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.journal.bal;
using Pharmacy2016.gui.core.journal.inc;
using DevExpress.XtraEditors.Controls;
using Pharmacy2016.gui.core.store.drug;
using Pharmacy2016.gui.core.journal.order;
using Pharmacy2016.gui.core.journal.exp;


namespace Pharmacy2016.gui.core.info.medicine
{
    /*
     * Эм нэмэх, засах цонх.
     * 
     */

    public partial class MedicineInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineInUpForm INSTANCE = new MedicineInUpForm();

            public static MedicineInUpForm Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private XtraForm form;

            public DataRowView Detial;

            public DataRowView Relate;

            private DataRowView newView;

            private List<string> deleteRows = new List<string>();

            private MedicineInUpForm()
            {
            
            }
            
            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                reload();      
            }

            private void initDataSource()
            {
                treeListLookUpEditPackage.Properties.DataSource = UserDataSet.PackageMedicineBindingSource;
                treeListLookUpEditType.Properties.DataSource = UserDataSet.MedicineTypeOtherBindingSource;
                treeListLookUpEditTypeInter.Properties.DataSource = UserDataSet.MedicineTypeInterOtherBindingSource;                
                gridLookUpEditUnit.Properties.DataSource = UserDataSet.MedicineUnitBindingSource;
                gridLookUpEditShape.Properties.DataSource = UserDataSet.MedicineShapeBindingSource;
                gridLookUpEditProducerCountry.Properties.DataSource = UserDataSet.ProducerCountryBindingSource;
                gridLookUpEditProducer.Properties.DataSource = UserDataSet.ProducerOtherBindingSource;
                gridLookUpEditQuantity.Properties.DataSource = UserDataSet.MedicineQuantityBindingSource;

                //Холбоотой эм
                //repositoryItemGridLookUpEditMedicine.DataSource = UserDataSet.MedicineBindingSource;
                //gridControlDetail.DataSource = UserDataSet.Instance.MedicRel;
            }

            private void initError()
            {
                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditLatinName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditPackage.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditTypeInter.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditShape.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditUnit.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditPackage.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditPrice.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditValidDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditQuantity.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditBarcode.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    this.form = form;                    
                    if (actionType == 1)
                    {                        
                        insertMedicine();
                    }
                    else if (actionType == 2)
                    {                     
                        updateMedicine();
                    }
                    else if (actionType == 3)
                    {
                        insertOtherMedicine();
                    }
                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insertMedicine()
            {
                initBinding();                
                spinEditPackage.Value = 0;
                spinEditPrice.Value = 0;
            }

            // Өөр цонхноос эм нэмэх функц
            private void insertOtherMedicine()
            {
                textEditName.EditValue = null;
                textEditLatinName.EditValue = null;
                textEditBarcode.EditValue = null;
                gridLookUpEditUnit.EditValue = null;
                gridLookUpEditShape.EditValue = null;
                textEditMedicineSeries.EditValue = null;
                memoEditDescription.EditValue = null;
                treeListLookUpEditPackage.EditValue = null;
                treeListLookUpEditType.EditValue = null;
                treeListLookUpEditTypeInter.EditValue = null;                
                gridLookUpEditProducerCountry.EditValue = null;
                gridLookUpEditProducer.EditValue = null;
                textEditRegisterID.EditValue = null;
                textEditBTKUSID.EditValue = null;
                gridLookUpEditQuantity.EditValue = null;
                spinEditPackage.Value = 0;
                spinEditPrice.Value = 0;
                dateEditValidDate.EditValue = null;
                textEditParmaStandart.EditValue = null;                
                textEditSerial.EditValue = null;
            }

            private void updateMedicine()
            {
                initBinding();                
            }

            private void initBinding()
            {
                textEditName.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "name", true));
                textEditLatinName.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "latinName", true));
                textEditBarcode.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "barcode", true));
                gridLookUpEditUnit.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "unitID", true));
                gridLookUpEditShape.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "shapeID", true));
                gridLookUpEditQuantity.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "quantityID", true));
                spinEditPackage.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "package", true));
                spinEditPrice.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "price", true));
                textEditMedicineSeries.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "medicineSeries", true));
                textEditSerial.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "serial", true));
                textEditParmaStandart.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "parmaStandart", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "description", true));
                treeListLookUpEditPackage.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "packageId", true));
                treeListLookUpEditType.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "medicineTypeId", true));
                treeListLookUpEditTypeInter.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "medicineTypeInterId", true)); 
                gridLookUpEditProducerCountry.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "producerCountryID", true));
                gridLookUpEditProducer.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "producerID", true));
                textEditRegisterID.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "registerID", true));
                textEditBTKUSID.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "btkusID"));
                dateEditValidDate.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineBindingSource, "validDate", true));
            }
            
            
            private void clearData()
            {
                clearBinding();
                clearErrorText();

                UserDataSet.MedicineBindingSource.CancelEdit();
                UserDataSet.Instance.Medicine.RejectChanges();
            }

            private void clearBinding()
            {
                textEditName.DataBindings.Clear();
                textEditLatinName.DataBindings.Clear();
                textEditBarcode.DataBindings.Clear();
                gridLookUpEditUnit.DataBindings.Clear();
                gridLookUpEditShape.DataBindings.Clear();
                gridLookUpEditQuantity.DataBindings.Clear();
                spinEditPackage.DataBindings.Clear();
                spinEditPrice.DataBindings.Clear();
                textEditMedicineSeries.DataBindings.Clear();
                textEditSerial.DataBindings.Clear();
                textEditParmaStandart.DataBindings.Clear();
                memoEditDescription.DataBindings.Clear();
                treeListLookUpEditPackage.DataBindings.Clear();
                treeListLookUpEditType.DataBindings.Clear();
                treeListLookUpEditTypeInter.DataBindings.Clear();                
                gridLookUpEditProducerCountry.DataBindings.Clear();
                gridLookUpEditProducer.DataBindings.Clear();
                textEditRegisterID.DataBindings.Clear();
                textEditBTKUSID.DataBindings.Clear();
                dateEditValidDate.DataBindings.Clear();
            }

            private void clearErrorText()
            {
                textEditName.ErrorText = "";
                textEditLatinName.ErrorText = "";
                treeListLookUpEditPackage.ErrorText = "";
                treeListLookUpEditType.ErrorText = "";
                treeListLookUpEditTypeInter.ErrorText = "";
                gridLookUpEditUnit.ErrorText = "";
                gridLookUpEditShape.ErrorText = "";
                spinEditPrice.ErrorText = "";
                spinEditPackage.ErrorText = "";
                dateEditValidDate.ErrorText = "";
                gridLookUpEditQuantity.ErrorText = "";
                textEditBarcode.ErrorText = "";
            }

            private void MedicineInUpForm_Shown(object sender, EventArgs e)
            {   
                textEditName.Focus();
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }            

        #endregion

        #region Формын event

            private void treeListLookUpEditType_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditType.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        treeListLookUpEditTypeInter.Focus();
                    }
                }
            }

            private void treeListLookUpEditTypeInter_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditTypeInter.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditShape.Focus();
                    }
                }
            }
        
            private void gridLookUpEditShape_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditShape.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditUnit.Focus();
                    }
                }
            }

            private void gridLookUpEditUnit_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditUnit.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditQuantity.Focus();
                    }
                }
            }

            private void gridLookUpEditQuantity_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditQuantity.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        saveButtonRegisterMedicine.Focus();
                    }
                }
            }

            private void dateEditValidDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditValidDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        textEditMedicineSeries.Focus();
                    }
                }
            }

            private void gridLookUpEditProducerCountry_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditProducerCountry.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditProducer.Focus();
                    }
                }
            }

            private void gridLookUpEditProducer_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditProducer.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        textEditRegisterID.Focus();
                    }
                }
            }

            private void gridLookUpEditShape_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    MedicineConfInForm.Instance.showForm(1, Instance);
                }
            }

            private void gridLookUpEditUnit_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    MedicineConfInForm.Instance.showForm(2, Instance);
                }
            }

            private void gridLookUpEditQuantity_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    MedicineConfInForm.Instance.showForm(3, Instance);
                }
            }

            private void gridLookUpEditProducer_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    ProducerInForm.Instance.showForm(2, Instance);
                }
            }

            private void gridLookUpEditProducerCountry_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditProducerCountry.EditValue != null && gridLookUpEditProducerCountry.EditValue != DBNull.Value)
                    UserDataSet.ProducerOtherBindingSource.Filter = "countryID = " + gridLookUpEditProducerCountry.EditValue ;
            }

            private void saveButtonRegisterMedicine_Click(object sender, EventArgs e)
            {
                saveMedicine();
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonCopy_Click(object sender, EventArgs e)
            {       
                copyMedicine();
            }

        #endregion

        #region Формын функц

            private void reload()
            {
                UserDataSet.MedicineShapeTableAdapter.Fill(UserDataSet.Instance.MedicineShape, HospitalUtil.getHospitalId());
                UserDataSet.MedicineUnitTableAdapter.Fill(UserDataSet.Instance.MedicineUnit, HospitalUtil.getHospitalId());
                UserDataSet.ProducerCountryTableAdapter.Fill(UserDataSet.Instance.ProducerCountry);
                UserDataSet.ProducerTableAdapter.Fill(UserDataSet.Instance.Producer, HospitalUtil.getHospitalId());
                UserDataSet.MedicineQuantityTableAdapter.Fill(UserDataSet.Instance.MedicineQuantity, HospitalUtil.getHospitalId());
                UserDataSet.PackageMedicineTableAdapter.Fill(UserDataSet.Instance.PackageMedicine, HospitalUtil.getHospitalId());
                if (UserDataSet.Instance.MedicineType.Rows.Count == 0)
                    UserDataSet.MedicineTypeTableAdapter.Fill(UserDataSet.Instance.MedicineType, HospitalUtil.getHospitalId());
                if (UserDataSet.Instance.MedicineTypeInternational.Rows.Count == 0)
                    UserDataSet.MedicineTypeInternationalTableAdapter.Fill(UserDataSet.Instance.MedicineTypeInternational, HospitalUtil.getHospitalId());
                //UserDataSet.MedicineRelativesTableAdapter.Fill(UserDataSet.Instance.MedicineRelatives, HospitalUtil.getHospitalId());
            }

            private bool checkMedicine()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (textEditName.EditValue == DBNull.Value || textEditName.EditValue == null || (textEditName.EditValue = textEditName.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Эмийн нэрийг оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    isRight = false;
                    textEditName.Focus();  
                }
                if (textEditLatinName.EditValue == DBNull.Value || textEditLatinName.EditValue == null || (textEditLatinName.EditValue = textEditLatinName.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Эмийн латин нэрийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditLatinName.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditLatinName.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (treeListLookUpEditPackage.EditValue == DBNull.Value || treeListLookUpEditPackage.EditValue == null || treeListLookUpEditPackage.Text.Equals(""))
                {
                    errorText = "Эмийн багцыг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditType.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditType.Focus();
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                
                if (gridLookUpEditShape.EditValue == DBNull.Value || gridLookUpEditShape.EditValue == null || gridLookUpEditShape.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийн хэлбэрийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditShape.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditShape.Focus();
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (gridLookUpEditUnit.EditValue == DBNull.Value || gridLookUpEditUnit.EditValue == null || gridLookUpEditUnit.EditValue.ToString().Equals(""))
                {
                    errorText = "Тун хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUnit.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditUnit.Focus();
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (gridLookUpEditQuantity.EditValue == DBNull.Value || gridLookUpEditQuantity.EditValue == null || gridLookUpEditQuantity.EditValue.ToString().Equals(""))
                {
                    errorText = "Хэмжих нэгжийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditQuantity.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditQuantity.Focus();
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                
                if (dateEditValidDate.EditValue == null || dateEditValidDate.EditValue == DBNull.Value || dateEditValidDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийн хүчинтэй хугацааг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditValidDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditValidDate.Focus();
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (textEditSerial.EditValue == DBNull.Value || textEditSerial.EditValue == null || (textEditSerial.EditValue = textEditSerial.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Эмийн сериалийг оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditSerial.ErrorText = errorText;
                    isRight = false;
                    textEditSerial.Focus();
                }
                if (spinEditPrice.Value == 0)
                {
                    errorText = "Үнийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditPrice.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditPrice.Focus();
                    }
                }
                if (isRight && MedicineForm.Instance.currentMedicineView != null)
                {
                    DataRow[] rows = UserDataSet.Instance.Medicine.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND id <> '" + MedicineForm.Instance.currentMedicineView["id"] + "'");
                    for (int i = 0; rows != null && i < rows.Length; i++)
                    {
                        if (textEditBarcode.EditValue != DBNull.Value && textEditBarcode.EditValue != null && !textEditBarcode.EditValue.ToString().Equals("") && textEditBarcode.Text != null && (textEditBarcode.EditValue.ToString().Equals(rows[i]["barcode"].ToString()) && rows[i] != null))
                        {
                            errorText = "Бар код давхардсан байна!";
                            text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            textEditBarcode.ErrorText = errorText;
                            if (isRight)
                            {
                                isRight = false;
                                textEditBarcode.Focus();
                                break; 
                            }
                        }
                    } 
                }

                if (isRight && MedicineForm.Instance.currentMedicineView != null)
                {
                    DataRow[] rows = UserDataSet.Instance.Medicine.Select("hospitalID = '" + HospitalUtil.getHospitalId() + 
                        "' AND name = '" + textEditName.EditValue + "' AND shapeID = '" + gridLookUpEditShape.EditValue +
                        "' AND unitID = '" + gridLookUpEditUnit.EditValue + "' AND quantityID = '" + gridLookUpEditQuantity.EditValue +
                        "' AND packageID = '" + treeListLookUpEditPackage.EditValue + "' AND price = '" + spinEditPrice.EditValue + "' AND serial = '" + textEditSerial.EditValue + "' AND validDate = '" + dateEditValidDate.EditValue + "'");
                    if (rows.Length > 0)
                    {
                        errorText = "Эмийн нэр, багцын нэр, хэлбэр, хэмжих нэгж, тунтай давхардсан бичлэг байна!";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        dateEditValidDate.ErrorText = errorText;
                        if (isRight)
                        {
                            isRight = false;
                            dateEditValidDate.Focus();
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        }
                    }                    
                }
               
                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void saveMedicine()
            {
                if (checkMedicine())
                {
                    try
                    {
                        if (actionType == 1 || actionType == 2)
                        {
                            MedicineForm.Instance.currentMedicineView["PackageMedicineName"] = treeListLookUpEditPackage.Text;
                            MedicineForm.Instance.currentMedicineView["packageID"] = treeListLookUpEditPackage.EditValue;
                            MedicineForm.Instance.currentMedicineView["medicineTypeInterName"] = treeListLookUpEditTypeInter.Text;
                            MedicineForm.Instance.currentMedicineView["medicineTypeInterID"] = treeListLookUpEditTypeInter.EditValue;
                            MedicineForm.Instance.currentMedicineView["medicineTypeName"] = treeListLookUpEditType.Text;
                            MedicineForm.Instance.currentMedicineView["medicineTypeID"] = treeListLookUpEditType.EditValue;
                            MedicineForm.Instance.currentMedicineView["shapeName"] = gridLookUpEditShape.Text;
                            MedicineForm.Instance.currentMedicineView["shapeID"] = gridLookUpEditShape.EditValue;
                            MedicineForm.Instance.currentMedicineView["unitName"] = gridLookUpEditUnit.Text;
                            MedicineForm.Instance.currentMedicineView["unitID"] = gridLookUpEditUnit.EditValue;
                            MedicineForm.Instance.currentMedicineView["producerCountryName"] = gridLookUpEditProducerCountry.Text;                                                        
                            MedicineForm.Instance.currentMedicineView["producerCountryID"] = gridLookUpEditProducerCountry.EditValue;
                            MedicineForm.Instance.currentMedicineView["producerName"] = gridLookUpEditProducer.Text;
                            MedicineForm.Instance.currentMedicineView["producerID"] = gridLookUpEditProducer.EditValue;
                            MedicineForm.Instance.currentMedicineView["quantityName"] = gridLookUpEditQuantity.Text;
                            MedicineForm.Instance.currentMedicineView["quantityID"] = gridLookUpEditQuantity.EditValue;

                            MedicineForm.Instance.currentMedicineView["serial"] = textEditSerial.EditValue;
                            MedicineForm.Instance.currentMedicineView["description"] = memoEditDescription.EditValue;
                            MedicineForm.Instance.currentMedicineView["medicineSeries"] = textEditMedicineSeries.EditValue;
                            MedicineForm.Instance.currentMedicineView["barcode"] = textEditBarcode.EditValue;
                            MedicineForm.Instance.currentMedicineView["parmaStandart"] = textEditParmaStandart.EditValue;
                            MedicineForm.Instance.currentMedicineView["registerID"] = textEditRegisterID.EditValue;
                            MedicineForm.Instance.currentMedicineView["btkusID"] = textEditBTKUSID.EditValue;
                            
                            MedicineForm.Instance.currentMedicineView.EndEdit();
                            UserDataSet.MedicineTableAdapter.Update(UserDataSet.Instance.Medicine);

                            clearData();
                            MedicineForm.Instance.setDefaultMed();
                            MedicineForm.Instance.addMultipleMedicine();                            
                            initBinding();                            
                        }
                        else if (actionType == 3)
                        {
                            UserDataSet.Instance.Medicine.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                            UserDataSet.Instance.Medicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                            UserDataSet.Instance.Medicine.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                            DataRowView view = (DataRowView)UserDataSet.MedicineBindingSource.AddNew();
                            view["name"] = textEditName.EditValue;
                            view["latinName"] = textEditLatinName.EditValue;
                            view["barcode"] = textEditBarcode.EditValue;
                            view["unitID"] = gridLookUpEditUnit.EditValue;
                            view["shapeID"] = gridLookUpEditShape.EditValue;
                            view["quantityID"] = gridLookUpEditQuantity.EditValue;
                            view["medicineSeries"] = textEditMedicineSeries.EditValue;
                            view["serial"] = textEditSerial.EditValue;
                            view["description"] = memoEditDescription.EditValue;
                            view["packageId"] = treeListLookUpEditPackage.EditValue;
                            view["medicineTypeId"] = treeListLookUpEditType.EditValue;
                            view["medicineTypeInterId"] = treeListLookUpEditTypeInter.EditValue;
                            view["producerCountryID"] = gridLookUpEditProducerCountry.EditValue;
                            view["producerID"] = gridLookUpEditProducer.EditValue;
                            view["registerID"] = textEditRegisterID.EditValue;
                            view["btkusID"] = textEditBTKUSID.EditValue;
                            view["validDate"] = dateEditValidDate.EditValue;
                            view["PackageMedicineName"] = treeListLookUpEditPackage.Text;
                            view["medicineTypeName"] = treeListLookUpEditType.Text;
                            view["medicineTypeInterName"] = treeListLookUpEditTypeInter.Text;  
                            view["shapeName"] = gridLookUpEditShape.Text;
                            view["unitName"] = gridLookUpEditUnit.Text;
                            view["producerCountryName"] = gridLookUpEditProducerCountry.Text;
                            view["producerName"] = gridLookUpEditProducer.Text;
                            view["quantityName"] = gridLookUpEditQuantity.Text;
                            view["parmaStandart"] = textEditParmaStandart.EditValue;
                            view["alterName"] = view["latinName"].ToString() + " " + view["unitName"].ToString();
                            view["soloName"] = view["name"].ToString() + " " + view["unitName"].ToString();
                            view.EndEdit();
                            UserDataSet.MedicineTableAdapter.Update(UserDataSet.Instance.Medicine);

                            // Өөр цонхноос эм нэмсэн бол тухайн нэмсэн эмийг дамжуулж байна.
                            if (form == MedicineBegBalUpForm.Instance)
                            {
                                MedicineBegBalUpForm.Instance.addMedicine(view);
                            }
                            else if (form == MedicineIncUpForm.Instance)
                            {
                                MedicineIncUpForm.Instance.addMedicine(view);
                            }
                            else if (form == PackageInUpForm.Instance)
                            {
                                PackageInUpForm.Instance.addMedicine(view);
                            }
                            else if (form == DoctorDetailUpForm.Instance)
                            {
                                DoctorDetailUpForm.Instance.addMedicine(view);
                            }
                            else if (form == MedicineOrderUpForm.Instance)
                            {
                                MedicineOrderUpForm.Instance.addMedicine(view);
                            }
                            else if (form == OrderDownForm.Instance)
                            {
                                OrderDownForm.Instance.addMedicine(view);
                            }
                            Close();
                        }
                        else if (actionType == 4)
                        {
                            newView["name"] = textEditName.EditValue;
                            newView["latinName"] = textEditLatinName.EditValue;
                            newView["barcode"] = textEditBarcode.EditValue;
                            newView["unitID"] = gridLookUpEditUnit.EditValue;
                            newView["shapeID"] = gridLookUpEditShape.EditValue;
                            newView["quantityID"] = gridLookUpEditQuantity.EditValue;
                            //newView["package"] = spinEditPackage.EditValue;
                            //newView["price"] = spinEditPrice.EditValue;
                            newView["medicineSeries"] = textEditMedicineSeries.EditValue;
                            newView["serial"] = textEditSerial.EditValue;
                            newView["description"] = memoEditDescription.EditValue;
                            newView["packageId"] = treeListLookUpEditPackage.EditValue;
                            newView["medicineTypeId"] = treeListLookUpEditType.EditValue;
                            newView["medicineTypeInterId"] = treeListLookUpEditTypeInter.EditValue;                            
                            newView["producerCountryID"] = gridLookUpEditProducerCountry.EditValue;
                            newView["producerID"] = gridLookUpEditProducer.EditValue;
                            newView["registerID"] = textEditRegisterID.EditValue;
                            newView["btkusID"] = textEditBTKUSID.EditValue;
                            newView["validDate"] = dateEditValidDate.EditValue;
                            newView["PackageMedicineName"] = treeListLookUpEditPackage.Text;
                            newView["medicineTypeName"] = treeListLookUpEditType.Text;
                            newView["medicineTypeInterName"] = treeListLookUpEditTypeInter.Text;  
                            newView["shapeName"] = gridLookUpEditShape.Text;
                            newView["unitName"] = gridLookUpEditUnit.Text;
                            newView["producerCountryName"] = gridLookUpEditProducerCountry.Text;
                            newView["producerName"] = gridLookUpEditProducer.Text;
                            newView["quantityName"] = gridLookUpEditQuantity.Text;
                            newView["parmaStandart"] = textEditParmaStandart.EditValue;
                            newView.EndEdit();
                            UserDataSet.MedicineTableAdapter.Update(UserDataSet.Instance.Medicine);

                            clearData();
                            UserDataSet.Instance.MedicRel.Rows.Clear();
                            MedicineForm.Instance.setDefaultMed();
                            MedicineForm.Instance.addMultipleMedicine();                            
                            initBinding();
                        }
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex);
                    }
                }
            }

            
            // Шинээр нэмсэн савлагааг авч байна

            public void addShape(string id)
            {
                gridLookUpEditShape.EditValue = id;
            }

            // Шинээр нэмсэн тун хэмжээг авч байна
            public void addUnit(string id)
            {
                gridLookUpEditUnit.EditValue = id;
            }

            // Шинээр нэмсэн хэмжих нэгжийг авч байна
            public void addQuantity(string id)
            {
                gridLookUpEditQuantity.EditValue = id;
            }

            // Шинээр нэмсэн бэлтгэн нийлүүлэгчийг авч байна
            public void addProducer(string id, int countryID)
            {
                gridLookUpEditProducerCountry.EditValue = countryID;
                gridLookUpEditProducer.EditValue = id;
            }

            public void copyMedicine()
            {
                if (MedicineForm.Instance.currentMedicineView != null && checkMedicine())
                {
                    if (actionType == 1)
                    {
                        MedicineForm.Instance.currentMedicineView["PackageMedicineName"] = treeListLookUpEditPackage.Text;
                        MedicineForm.Instance.currentMedicineView["medicineTypeInterName"] = treeListLookUpEditTypeInter.Text;
                        MedicineForm.Instance.currentMedicineView["medicineTypeName"] = treeListLookUpEditType.Text;
                        MedicineForm.Instance.currentMedicineView["shapeName"] = gridLookUpEditShape.Text;
                        MedicineForm.Instance.currentMedicineView["unitName"] = gridLookUpEditUnit.Text;
                        MedicineForm.Instance.currentMedicineView["producerCountryName"] = gridLookUpEditProducerCountry.Text;
                        MedicineForm.Instance.currentMedicineView["producerName"] = gridLookUpEditProducer.Text;
                        MedicineForm.Instance.currentMedicineView["producerCountryID"] = gridLookUpEditProducerCountry.EditValue;
                        MedicineForm.Instance.currentMedicineView["quantityName"] = gridLookUpEditQuantity.Text;
                        MedicineForm.Instance.currentMedicineView.EndEdit();
                        UserDataSet.MedicineTableAdapter.Update(UserDataSet.Instance.Medicine);
                    }

                    UserDataSet.Instance.Medicine.idColumn.DefaultValue = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    newView = (DataRowView)UserDataSet.MedicineBindingSource.AddNew();
                    newView["name"] = MedicineForm.Instance.currentMedicineView["name"];
                    newView["latinName"] = MedicineForm.Instance.currentMedicineView["latinName"];
                    newView["barcode"] = MedicineForm.Instance.currentMedicineView["barcode"];
                    newView["unitID"] = MedicineForm.Instance.currentMedicineView["unitID"];
                    newView["shapeID"] = MedicineForm.Instance.currentMedicineView["shapeID"];
                    newView["quantityID"] = MedicineForm.Instance.currentMedicineView["quantityID"];
                    newView["medicineSeries"] = MedicineForm.Instance.currentMedicineView["medicineSeries"];
                    newView["serial"] = MedicineForm.Instance.currentMedicineView["serial"];
                    newView["description"] = MedicineForm.Instance.currentMedicineView["description"];
                    newView["medicineTypeId"] = MedicineForm.Instance.currentMedicineView["medicineTypeId"];
                    newView["medicineTypeInterID"] = MedicineForm.Instance.currentMedicineView["medicineTypeInterID"];
                    newView["producerCountryID"] = MedicineForm.Instance.currentMedicineView["producerCountryID"];
                    newView["producerID"] = MedicineForm.Instance.currentMedicineView["producerID"];
                    newView["registerID"] = MedicineForm.Instance.currentMedicineView["registerID"];
                    newView["btkusID"] = MedicineForm.Instance.currentMedicineView["btkusID"];
                    newView["validDate"] = MedicineForm.Instance.currentMedicineView["validDate"];
                    newView["medicineTypeName"] = MedicineForm.Instance.currentMedicineView["medicineTypeName"];
                    newView["medicineTypeInterName"] = MedicineForm.Instance.currentMedicineView["medicineTypeInterName"];
                    newView["shapeName"] = MedicineForm.Instance.currentMedicineView["shapeName"];
                    newView["unitName"] = MedicineForm.Instance.currentMedicineView["unitName"];
                    newView["producerCountryName"] = MedicineForm.Instance.currentMedicineView["producerCountryName"];
                    newView["producerName"] = MedicineForm.Instance.currentMedicineView["producerName"];
                    newView["quantityName"] = MedicineForm.Instance.currentMedicineView["quantityName"];
                    newView["parmaStandart"] = MedicineForm.Instance.currentMedicineView["parmaStandart"];

                    textEditName.EditValue = newView["name"];
                    textEditLatinName.EditValue = newView["latinName"];
                    textEditBarcode.EditValue = newView["barcode"];
                    gridLookUpEditUnit.EditValue = newView["unitID"];
                    gridLookUpEditShape.EditValue = newView["shapeID"];
                    gridLookUpEditQuantity.EditValue = newView["quantityID"];
                    textEditMedicineSeries.EditValue = newView["medicineSeries"];
                    textEditSerial.EditValue = newView["serial"];
                    memoEditDescription.EditValue = newView["description"];
                    treeListLookUpEditPackage.EditValue = newView["packageId"];
                    treeListLookUpEditType.EditValue = newView["medicineTypeId"];
                    treeListLookUpEditTypeInter.EditValue = newView["medicineTypeInterId"];                    
                    gridLookUpEditProducerCountry.EditValue = newView["producerCountryID"];
                    gridLookUpEditProducer.EditValue = newView["producerID"];
                    textEditRegisterID.EditValue = newView["registerID"];
                    textEditBTKUSID.EditValue = newView["btkusID"];
                    dateEditValidDate.EditValue = newView["validDate"];
                    textEditParmaStandart.EditValue = newView["parmaStandart"];
                    actionType = 4;
                }      
            }

        #endregion                              

            

            
            private void treeListLookUpEditPackage_EditValueChanged(object sender, EventArgs e)
            {                
                if (treeListLookUpEditPackage.EditValue != DBNull.Value && treeListLookUpEditPackage.EditValue != null &&
                    !treeListLookUpEditPackage.Equals(""))
                {
                    DataRow[] rows = UserDataSet.Instance.PackageMedicine.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND id = '" + treeListLookUpEditPackage.EditValue + "'");
                    if (rows.Length > 0)
                    {
                        gridLookUpEditShape.EditValue = rows[0]["shapeID"];
                        gridLookUpEditUnit.EditValue = rows[0]["unitID"];
                        gridLookUpEditQuantity.EditValue = rows[0]["quantityID"];
                        treeListLookUpEditType.EditValue = rows[0]["medicineTypeID"];
                        treeListLookUpEditTypeInter.EditValue = rows[0]["medicineTypeInterID"];                    
                    }
                    else
                    {
                        gridLookUpEditShape.EditValue = null;
                        gridLookUpEditShape.Text = "";
                        gridLookUpEditUnit.EditValue = null;
                        gridLookUpEditUnit.Text = "";
                        gridLookUpEditQuantity.EditValue = null;
                        gridLookUpEditQuantity.Text = "";
                        treeListLookUpEditType.EditValue = null;
                        treeListLookUpEditType.Text = "";
                        treeListLookUpEditTypeInter.EditValue = null;
                        treeListLookUpEditTypeInter.Text = "";
                    }
                }
            }

            private void treeListLookUpEditPackage_ButtonClick(object sender, ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "addPack"))
                {
                    PackageMedicine.Instance.showForm(3, Instance);
                }
            }

            public void addMedicine(DataRowView view)
            {
                treeListLookUpEditPackage.EditValue = view["id"];
            }

            private void closeButtonRegisterMedicine_Click(object sender, EventArgs e)
            {
            }

            private void textEdit1_EditValueChanged(object sender, EventArgs e)
            {

            }

    }
}