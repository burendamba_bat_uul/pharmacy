﻿namespace Pharmacy2016.gui.core.info.medicine
{
    partial class MedicineTypeInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.textEditAccount = new DevExpress.XtraEditors.TextEdit();
            this.saveButtonRegisterMedicineType = new DevExpress.XtraEditors.SimpleButton();
            this.gridLookUpEditIsSpecial = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.closeButtonRegisterMedicineType = new DevExpress.XtraEditors.SimpleButton();
            this.treeListLookUpEditMedicineType = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.Нэр = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIsSpecial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditMedicineType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(434, 311);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.textEditAccount);
            this.panelControl2.Controls.Add(this.saveButtonRegisterMedicineType);
            this.panelControl2.Controls.Add(this.gridLookUpEditIsSpecial);
            this.panelControl2.Controls.Add(this.closeButtonRegisterMedicineType);
            this.panelControl2.Controls.Add(this.treeListLookUpEditMedicineType);
            this.panelControl2.Controls.Add(this.memoEditDescription);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.textEditName);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(430, 307);
            this.panelControl2.TabIndex = 0;
            // 
            // textEditAccount
            // 
            this.textEditAccount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditAccount.EnterMoveNextControl = true;
            this.textEditAccount.Location = new System.Drawing.Point(140, 82);
            this.textEditAccount.Name = "textEditAccount";
            this.textEditAccount.Properties.MaxLength = 50;
            this.textEditAccount.Size = new System.Drawing.Size(200, 20);
            this.textEditAccount.TabIndex = 3;
            // 
            // saveButtonRegisterMedicineType
            // 
            this.saveButtonRegisterMedicineType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveButtonRegisterMedicineType.Location = new System.Drawing.Point(264, 274);
            this.saveButtonRegisterMedicineType.Name = "saveButtonRegisterMedicineType";
            this.saveButtonRegisterMedicineType.Size = new System.Drawing.Size(75, 23);
            this.saveButtonRegisterMedicineType.TabIndex = 6;
            this.saveButtonRegisterMedicineType.Text = "Хадгалах";
            this.saveButtonRegisterMedicineType.ToolTip = "Өгөгдлийг хадгалах";
            this.saveButtonRegisterMedicineType.Click += new System.EventHandler(this.saveButtonRegisterMedicineType_Click);
            // 
            // gridLookUpEditIsSpecial
            // 
            this.gridLookUpEditIsSpecial.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditIsSpecial.EditValue = "id";
            this.gridLookUpEditIsSpecial.Location = new System.Drawing.Point(140, 108);
            this.gridLookUpEditIsSpecial.Name = "gridLookUpEditIsSpecial";
            this.gridLookUpEditIsSpecial.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditIsSpecial.Properties.DisplayMember = "name";
            this.gridLookUpEditIsSpecial.Properties.NullText = "";
            this.gridLookUpEditIsSpecial.Properties.ValueMember = "id";
            this.gridLookUpEditIsSpecial.Properties.View = this.gridLookUpEdit1View;
            this.gridLookUpEditIsSpecial.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditIsSpecial.TabIndex = 4;
            this.gridLookUpEditIsSpecial.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditIsSpecial_KeyUp);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnname});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoPopulateColumns = false;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnname
            // 
            this.gridColumnname.Caption = "Нэр";
            this.gridColumnname.FieldName = "name";
            this.gridColumnname.Name = "gridColumnname";
            this.gridColumnname.OptionsColumn.AllowMove = false;
            this.gridColumnname.Visible = true;
            this.gridColumnname.VisibleIndex = 0;
            // 
            // closeButtonRegisterMedicineType
            // 
            this.closeButtonRegisterMedicineType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButtonRegisterMedicineType.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButtonRegisterMedicineType.Location = new System.Drawing.Point(345, 274);
            this.closeButtonRegisterMedicineType.Name = "closeButtonRegisterMedicineType";
            this.closeButtonRegisterMedicineType.Size = new System.Drawing.Size(75, 23);
            this.closeButtonRegisterMedicineType.TabIndex = 7;
            this.closeButtonRegisterMedicineType.Text = "Гарах";
            this.closeButtonRegisterMedicineType.ToolTip = "Гарах";
            // 
            // treeListLookUpEditMedicineType
            // 
            this.treeListLookUpEditMedicineType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditMedicineType.EditValue = "";
            this.treeListLookUpEditMedicineType.Location = new System.Drawing.Point(140, 30);
            this.treeListLookUpEditMedicineType.Name = "treeListLookUpEditMedicineType";
            this.treeListLookUpEditMedicineType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditMedicineType.Properties.DisplayMember = "name";
            this.treeListLookUpEditMedicineType.Properties.NullText = "";
            this.treeListLookUpEditMedicineType.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEditMedicineType.Properties.ValueMember = "id";
            this.treeListLookUpEditMedicineType.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditMedicineType.TabIndex = 1;
            this.treeListLookUpEditMedicineType.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditMedicineType_KeyUp);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.Нэр});
            this.treeListLookUpEdit1TreeList.KeyFieldName = "id";
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.ParentFieldName = "parentId";
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // Нэр
            // 
            this.Нэр.Caption = "Эмийн төрөл";
            this.Нэр.FieldName = "name";
            this.Нэр.Name = "Нэр";
            this.Нэр.OptionsColumn.AllowMove = false;
            this.Нэр.Visible = true;
            this.Нэр.VisibleIndex = 0;
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditDescription.EditValue = "";
            this.memoEditDescription.EnterMoveNextControl = true;
            this.memoEditDescription.Location = new System.Drawing.Point(140, 134);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(280, 109);
            this.memoEditDescription.TabIndex = 5;
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(58, 111);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(76, 13);
            this.labelControl5.TabIndex = 10;
            this.labelControl5.Text = "Шинж чанар* :";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(96, 85);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(38, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Данс* :";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(88, 137);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(46, 13);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "Тайлбар:";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(106, 59);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(28, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Нэр*:";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(45, 33);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(89, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Үндсэн ангилал*:";
            // 
            // textEditName
            // 
            this.textEditName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditName.EnterMoveNextControl = true;
            this.textEditName.Location = new System.Drawing.Point(140, 56);
            this.textEditName.Name = "textEditName";
            this.textEditName.Properties.MaxLength = 100;
            this.textEditName.Size = new System.Drawing.Size(200, 20);
            this.textEditName.TabIndex = 2;
            // 
            // MedicineTypeInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButtonRegisterMedicineType;
            this.ClientSize = new System.Drawing.Size(434, 311);
            this.Controls.Add(this.panelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MedicineTypeInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Эмийн ангилал ";
            this.Shown += new System.EventHandler(this.MedicineTypeInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIsSpecial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditMedicineType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton saveButtonRegisterMedicineType;
        private DevExpress.XtraEditors.SimpleButton closeButtonRegisterMedicineType;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditMedicineType;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn Нэр;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditIsSpecial;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnname;
        private DevExpress.XtraEditors.TextEdit textEditAccount;
    }
}