﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.gui.core.journal.tender;
using Pharmacy2016.gui.core.info.conf;

namespace Pharmacy2016.gui.core.info.medicine
{
    public partial class PackageInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static PackageInUpForm INSTANCE = new PackageInUpForm();

            public static PackageInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            public DataRowView currentPackage;

            public DataRowView currentDetail;

            public DataRowView newRows;

            public DataRowView detial;

            private XtraForm form;

            private PackageInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                spinEditPrice.Properties.MaxValue = Decimal.MaxValue - 1;

                dateEditCreatedDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditCreatedDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                initError();
                initDataSource(); 
                reload();
            }

            private void initDataSource()
            {
                gridLookUpEditProducer.Properties.DataSource = UserDataSet.SupplierBindingSource;
                //gridLookUpEditMedicine.Properties.DataSource = UserDataSet.MedicinePackageBindingSource;
                gridLookUpEditMedicine.Properties.DataSource = UserDataSet.PackageMedicineBindingSource;
            }

            private void initError()
            {
                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditProducer.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;      
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditCreatedDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditPrice.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditSumPrice.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion    

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }
                    this.form = form;
                    //JournalDataSet.WarehouseOtherBindingSource.Filter = UserUtil.getFilterWarehouse();
                    actionType = type;
                    if (actionType == 1)
                    {
                        insert();
                    }
                    else if (actionType == 2)
                    {
                        update();
                    }
                    else if (actionType == 3)
                    {
                        insertOther();
                    }

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                    if(actionType == 3)
                    {
                        TenderUpForm.Instance.focusChanged();
                    }
                }
            }

            private void initBinding()
            {
                textEditName.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageBindingSource, "name", true));
                dateEditCreatedDate.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageBindingSource, "createdDate", true));
                //gridLookUpEditProducer.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageBindingSource, "supplierID", true));
                checkEditIsUse.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageBindingSource, "isUse", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageBindingSource, "description", true));

                //spinEditSumPrice.DataBindings.Add(new Binding("EditValue", UserDataSet.PackageBindingSource, "total", true));
            }

            private void insert()
            {
                this.Text = "Багц нэмэх цонх";
                initBinding();
                //gridLookUpEditProducer.Properties.ReadOnly = false;
                checkEditIsUse.Visible = true;
                labelControlIsUse.Visible = true;
                string id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                UserDataSet.Instance.Package.idColumn.DefaultValue = id;
                UserDataSet.Instance.PackageDetial.packageIDColumn.DefaultValue = id;

                currentPackage = (DataRowView)UserDataSet.PackageBindingSource.AddNew();
                DateTime now = DateTime.Now;
                currentPackage["id"] = id;
                currentPackage["createdDate"] = now;
                dateEditCreatedDate.DateTime = now;
                addRow();
            }

            private void update()
            {
                this.Text = "Багц засах цонх";
                initBinding();
                //gridLookUpEditProducer.Properties.ReadOnly = false;
                checkEditIsUse.Visible = true;
                labelControlIsUse.Visible = true;
                currentPackage = (DataRowView)UserDataSet.PackageBindingSource.Current;
                currentDetail = MedicineForm.Instance.getSelectedRow();
                UserDataSet.Instance.PackageDetial.packageIDColumn.DefaultValue = currentPackage["id"];

                gridLookUpEditMedicine.EditValue = currentDetail["medicineID"];
                spinEditPrice.EditValue = currentDetail["price"];
                spinEditCount.EditValue = currentDetail["count"];
                spinEditSumPrice.EditValue = currentDetail["sumPrice"];

                labelControlQuantity.Text = currentDetail["quantity"].ToString();
            }

            private void addRow()
            {
                clearErrorText();
                UserDataSet.Instance.PackageDetial.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                currentDetail = (DataRowView)UserDataSet.PackageDetialBindingSource.AddNew();
                clearMedicine();
            }

            private void insertOther()
            {             
                textEditName.EditValue = null;
                //gridLookUpEditProducer.EditValue = null;
                gridLookUpEditMedicine.EditValue = null;
                dateEditCreatedDate.EditValue = DateTime.Now;
                memoEditDescription.EditValue = null;
                checkEditIsUse.EditValue = true;
                checkEditIsUse.Visible = false;
                labelControlIsUse.Visible = false;
                spinEditPrice.EditValue = null;
                spinEditSumPrice.EditValue = null;
                spinEditCount.EditValue = null;
                spinEditPrice.Value = 0;
                spinEditSumPrice.Value = 0;
                spinEditCount.Value = 0;
                //gridLookUpEditProducer.EditValue = TenderUpForm.Instance.getEditValue();
                //gridLookUpEditProducer.Properties.ReadOnly = true;
                UserDataSet.Instance.Package.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                UserDataSet.Instance.Package.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                UserDataSet.Instance.Package.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                newRows = (DataRowView)UserDataSet.PackageBindingSource.AddNew();
            }

            private void clearData()
            {
                clearErrorText();
                clearMedicine();
                clearBinding();

                UserDataSet.PackageBindingSource.CancelEdit();
                UserDataSet.PackageDetialBindingSource.CancelEdit();
                UserDataSet.Instance.Package.RejectChanges();
                UserDataSet.Instance.PackageDetial.RejectChanges();
            }

            private void clearBinding()
            {
                textEditName.DataBindings.Clear();
                dateEditCreatedDate.DataBindings.Clear();
                //gridLookUpEditProducer.DataBindings.Clear();
                checkEditIsUse.DataBindings.Clear();
                memoEditDescription.DataBindings.Clear();
            }

            private void clearErrorText()
            {
                textEditName.ErrorText = "";
                //gridLookUpEditProducer.ErrorText = "";
                gridLookUpEditMedicine.ErrorText = "";
                dateEditCreatedDate.ErrorText = "";
                spinEditPrice.ErrorText = "";
                spinEditCount.ErrorText = "";
                spinEditSumPrice.ErrorText = "";
            }

            private void clearMedicine()
            {
                gridLookUpEditMedicine.EditValue = null;
                spinEditPrice.Value = 0;
                spinEditSumPrice.Value = 0;
                spinEditCount.Value = 0;
            }

            private void PackageInUpForm_Shown(object sender, EventArgs e)
            {
                textEditName.Focus();
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }

        #endregion

        #region Формын event

            private void dateEditCreatedDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditCreatedDate.ShowPopup();
                    }
                    else
                    {
                        if(checkEditIsUse.Visible == true)
                        {
                            ProgramUtil.IsShowPopup = true;
                            checkEditIsUse.Focus();
                        }
                        else
                        {
                            ProgramUtil.IsShowPopup = true;
                            memoEditDescription.Focus();
                        }
                    }
                }
            }

            private void gridLookUpEditProducer_KeyUp(object sender, KeyEventArgs e)
            {
                //if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                //{
                //    if (ProgramUtil.IsShowPopup)
                //    {
                //        ProgramUtil.IsShowPopup = false;
                //        gridLookUpEditProducer.ShowPopup();
                //    }
                //    else
                //    {
                //        if(checkEditIsUse.Visible == true)
                //        {
                //            ProgramUtil.IsShowPopup = true;
                //            checkEditIsUse.Focus();
                //        }
                //        else
                //        {
                //            ProgramUtil.IsShowPopup = true;
                //            memoEditDescription.Focus();
                //        }
                //    }
                //}
            }

            private void gridLookUpEditMedicine_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditMedicine.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        spinEditCount.Focus();
                    }
                }
            }

            private void gridLookUpEditProducer_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add") && actionType != 3)
                {
                    SupplierInForm.Instance.showForm(2, Instance);
                }
            }

            private void gridLookUpEditMedicine_EditValueChanged(object sender, EventArgs e)
             {
                DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                if (gridLookUpEditMedicine.EditValue != null && gridLookUpEditMedicine.EditValue.ToString() != "" && view != null)
                {
                    labelControlQuantity.Text = view["quantityName"].ToString();

                    spinEditPrice.Value = 0; //Convert.ToDecimal(view["price"]);
                }
            }

            private void gridLookUpEditMedicine_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                   PackageMedicine.Instance.showForm(3, Instance);
                }
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

            private void spinEditCount_EditValueChanged(object sender, EventArgs e)
            {
                calculatePrice();
            }

            private void spinEditPrice_EditValueChanged(object sender, EventArgs e)
            {
                calculatePrice();
            }

        #endregion      

        #region Формын функц

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                if (UserDataSet.Instance.PackageMedicine.Rows.Count == 0)
                    UserDataSet.PackageMedicineTableAdapter.Fill(UserDataSet.Instance.PackageMedicine, HospitalUtil.getHospitalId());
                if (UserDataSet.Instance.Supplier.Rows.Count == 0)
                    UserDataSet.SupplierTableAdapter.Fill(UserDataSet.Instance.Supplier, HospitalUtil.getHospitalId());
                ProgramUtil.closeWaitDialog();
            }

            private void calculatePrice()
            {
                spinEditSumPrice.EditValue = spinEditCount.Value * spinEditPrice.Value;
            }

            private bool check()
            {
                //Instance.Validate();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (textEditName.EditValue == DBNull.Value || textEditName.EditValue == null || textEditName.Text.Equals(""))
                {
                    errorText = "Багцын нэрийг оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    isRight = false;
                    textEditName.Focus();
                }
                if (dateEditCreatedDate.EditValue == DBNull.Value || dateEditCreatedDate.EditValue == null || dateEditCreatedDate.Text.Equals(""))
                {
                    errorText = "Огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditCreatedDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditCreatedDate.Focus();
                    }
                }
                //if (gridLookUpEditProducer.EditValue == DBNull.Value || gridLookUpEditProducer.EditValue == null || gridLookUpEditProducer.Text.Equals(""))
                //{
                //    errorText = "Бэлтгэн нийлүүлэгч сонгоно уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    gridLookUpEditProducer.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        isRight = false;
                //        gridLookUpEditProducer.Focus();
                //    }
                //}
                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.Text.Equals(""))
                {
                    errorText = "Эмийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                    }
                }
                if (spinEditCount.Value == 0)
                {
                    errorText = "Тоо хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditCount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditCount.Focus();
                    }
                }
                if (spinEditPrice.Value == 0)
                {
                    errorText = "Үнийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditPrice.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditPrice.Focus();
                    }
                }
                if (isRight && actionType != 3)
                {
                    DataRow[] rows = UserDataSet.Instance.PackageDetial.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND packageID = '" + currentPackage["id"] + "' AND id <> '" + currentDetail["id"] + "'");
                    for (int i = 0; rows != null && i < rows.Length; i++)
                    {
                        if (gridLookUpEditMedicine.EditValue.ToString().Equals(rows[i]["medicineID"].ToString()) && spinEditPrice.Value == Convert.ToDecimal(rows[i]["price"]))
                        {
                            errorText = "Эм, үнэ давхацсан байна";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            gridLookUpEditMedicine.ErrorText = errorText;
                            isRight = false;
                            gridLookUpEditMedicine.Focus();
                            break;
                        }
                    }
                }
                if (isRight && actionType == 3 && newRows != null && detial != null)
                {
                    DataRow[] rows = UserDataSet.Instance.PackageDetial.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND packageID = '" + newRows["id"] + "' AND id <> '" + detial["id"] + "'");
                    for (int i = 0; rows != null && i < rows.Length; i++)
                    {
                        if (gridLookUpEditMedicine.EditValue.ToString().Equals(rows[i]["medicineID"].ToString()))
                        {
                            errorText = "Эм, үнэ давхацсан байна";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            gridLookUpEditMedicine.ErrorText = errorText;
                            isRight = false;
                            gridLookUpEditMedicine.Focus();
                            break;
                        }
                    }
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                ProgramUtil.IsSaveEvent = true;
                ProgramUtil.IsShowPopup = true;
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    if (actionType == 1 || actionType == 2)
                    {
                        

                        DataRowView medicine = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();


                        currentDetail["medicineID"] = gridLookUpEditMedicine.EditValue;
                        currentDetail["medicineName"] = medicine["name"];
                        currentDetail["soloName"] = gridLookUpEditMedicine.Text;

                        currentDetail["unit"] = medicine["unitName"];
                        currentDetail["shape"] = medicine["shapeName"];
                        currentDetail["quantity"] = medicine["quantityName"];
                        currentDetail["validDate"] = DBNull.Value;//medicine["validDate"];
                        currentDetail["price"] = spinEditPrice.Value;
                        currentDetail["count"] = spinEditCount.Value;
                        currentDetail["sumPrice"] = spinEditSumPrice.Value;
                        currentDetail.EndEdit();

                        currentPackage["name"] = textEditName.EditValue;
                        //currentPackage["supplierID"] = gridLookUpEditProducer.EditValue;
                        //currentPackage["supplierName"] = gridLookUpEditProducer.Text;
                        currentPackage["createdDate"] = dateEditCreatedDate.EditValue;
                        currentPackage["isUse"] = checkEditIsUse.EditValue;
                        currentPackage["description"] = memoEditDescription.EditValue;
                        currentPackage.EndEdit();

                        UserDataSet.PackageTableAdapter.Update(UserDataSet.Instance.Package);
                        UserDataSet.PackageDetialTableAdapter.Update(UserDataSet.Instance.PackageDetial);

                        

                        addRow();
                        gridLookUpEditMedicine.Focus();
                    }
                    else if (actionType == 3)
                    {
                        newRows = (DataRowView)UserDataSet.PackageBindingSource.Current;
                        newRows["name"] = textEditName.EditValue;
                        //newRows["supplierID"] = gridLookUpEditProducer.EditValue;
                        //newRows["supplierName"] = gridLookUpEditProducer.Text;
                        newRows["createdDate"] = dateEditCreatedDate.EditValue;
                        newRows["isUse"] = checkEditIsUse.EditValue;
                        newRows["description"] = memoEditDescription.EditValue;
                        newRows.EndEdit();

                        UserDataSet.Instance.PackageDetial.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.PackageDetial.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.PackageDetial.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                        detial = (DataRowView)UserDataSet.PackageDetialBindingSource.AddNew();
                        DataRowView medicine = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                        detial["medicineID"] = gridLookUpEditMedicine.EditValue;
                        detial["medicineName"] = medicine["name"];
                        detial["soloName"] = gridLookUpEditMedicine.Text;
                        detial["unit"] = medicine["unitName"];
                        detial["shape"] = medicine["shapeName"];
                        detial["quantity"] = medicine["quantityName"];
                        detial["validDate"] = DBNull.Value;//medicine["validDate"];
                        detial["medicPrice"] = spinEditPrice.Value;
                        detial["price"] = spinEditPrice.Value;
                        detial["count"] = spinEditCount.Value;
                        detial["sumPrice"] = spinEditSumPrice.Value;
                        detial.EndEdit();
                        //MedicineForm.Instance.getSumPrice();
                        
                        UserDataSet.PackageTableAdapter.Update(UserDataSet.Instance.Package);
                        UserDataSet.PackageDetialTableAdapter.Update(UserDataSet.Instance.PackageDetial);            
                        if (form == TenderUpForm.Instance)
                        {
                            TenderUpForm.Instance.addPackage(newRows);
                        }       
                        clearMedicine();
                        gridLookUpEditMedicine.Focus();
                    }
                    ProgramUtil.closeWaitDialog();
                }
            }

            // Шинээр нэмсэн эмийг авч байна
            public void addMedicine(DataRowView view)
            {               
                gridLookUpEditMedicine.EditValue = view["id"];
            }

            // Шинээр нэмсэн бэлгэн нийлүүлэгчийг авч байна
            public void addProducer(string id)
            {
                gridLookUpEditProducer.EditValue = id;
            }

        #endregion          
    
    }
}