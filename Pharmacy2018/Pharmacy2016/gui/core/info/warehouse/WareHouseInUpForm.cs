﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.medicine;
using Pharmacy2016.gui.report.ds;
using Pharmacy2016.gui.core.info.hospital;

namespace Pharmacy2016.gui.core.info.warehouse
{
    public partial class WareHouseInUpForm : DevExpress.XtraEditors.XtraForm
    {
        
        #region Форм анх ачааллах функц

            private static WareHouseInUpForm INSTANCE = new WareHouseInUpForm();

            public static WareHouseInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private DataRow[] wardConns;

            private WareHouseInUpForm()
            {

            }

            
            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                initBinding();
                reload();
            }

            private void reload()
            {
                //if (UserDataSet.Instance.WareHouse.Rows.Count == 0)
                //    UserDataSet.WareHouseTableAdapter.Fill(UserDataSet.Instance.WareHouse, HospitalUtil.getHospitalId());
            }

            private void initBinding()
            {
                treeListLookUpEditWareHouses.DataBindings.Add(new Binding("EditValue", UserDataSet.WareHouseBindingSource, "pid", true));
                textEditName.DataBindings.Add(new Binding("EditValue", UserDataSet.WareHouseBindingSource, "name", true));
                spinEditNumber.DataBindings.Add(new Binding("EditValue", UserDataSet.WareHouseBindingSource, "number", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", UserDataSet.WareHouseBindingSource, "description", true));            
                checkEditIsUse.DataBindings.Add(new Binding("EditValue", UserDataSet.WareHouseBindingSource, "isUse", true));
            }

            private void initDataSource()
            {
                treeListLookUpEditWareHouses.Properties.DataSource = UserDataSet.WareHouseOtherBindingSource;
            }

            private void initError()
            {
                //treeListLookUpEditWareHouses.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditNumber.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insertWareHouse();
                    }
                    else
                    {
                        editWareHouse();
                    }
                    ShowDialog(WareHouseForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                clearErrorText();
                UserDataSet.WareHouseBindingSource.CancelEdit();
                textEditAcoulusID.EditValue = null;
                //UserDataSet.Instance.MedicineType.RejectChanges();
            }

            private void clearErrorText()
            {
                textEditName.ErrorText = "";
                //treeListLookUpEditWareHouses.ErrorText = "";
                spinEditNumber.ErrorText = "";
            }

            private void WareHouseInUpForm_Shown(object sender, EventArgs e)
            {
                treeListLookUpEditWareHouses.Focus();
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }

            private void insertWareHouse()
            {
                this.Text = "Агуулах нэмэх цонх";
            }

            private void editWareHouse()
            {
                wardConns = ReportDataSet.Instance.WardConn.Select("pharmacyID = '" + WareHouseForm.Instance.currentWareHouseView["id"] + "'");
                if (wardConns.Length > 0)
                    textEditAcoulusID.EditValue = wardConns[0]["acoulusID"];
                this.Text = "Агуулах засах цонх";
            }

        #endregion

        #region Формын event

            private void treeListLookUpEditWareHouses_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditWareHouses.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        textEditName.Focus();
                    }
                }
            }

            private void saveButton_Click(object sender, EventArgs e)
            {
                save();
            }

            private void treeListLookUpEditWareHouses_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "clear"))
                {
                    treeListLookUpEditWareHouses.Text = "0";
                }
            }

        #endregion

        #region Формын функц.

            private bool validation()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (textEditName.EditValue == null || textEditName.EditValue == DBNull.Value || (textEditName.EditValue = textEditName.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Агуулахын нэрийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditName.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (spinEditNumber.Value == 0)
                {
                    errorText = "Агуулахын дугаарыг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditNumber.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditNumber.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (validation())
                {
                    UserDataSet.WareHouseBindingSource.EndEdit();
                    UserDataSet.WareHouseTableAdapter.Update(UserDataSet.Instance.WareHouse);
                    if (textEditAcoulusID.EditValue != null && textEditAcoulusID.EditValue != DBNull.Value)
                    {
                        if (wardConns.Length == 0)
                        {
                            ReportDataSet.Instance.WardConn.idColumn.DefaultValue = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                            DataRowView wardConn = (DataRowView)ReportDataSet.WardConnBindingSource.AddNew();
                            wardConn["acoulusID"] = textEditAcoulusID.EditValue;
                            wardConn["pharmacyID"] = WareHouseForm.Instance.currentWareHouseView["id"];
                            wardConn.EndEdit();
                        }
                        else
                        {
                            wardConns[0]["acoulusID"] = textEditAcoulusID.EditValue;
                            wardConns[0].EndEdit();
                        }
                        ReportDataSet.WardConnTableAdapter.Update(ReportDataSet.Instance.WardConn);
                    }
                    clearData();
                    WareHouseForm.Instance.setDefaultHouse();
                    WareHouseForm.Instance.addDoubleHouse();                 
                }
            }

        #endregion            
 
    }
}