﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Controls;
using Pharmacy2016.gui.report.ds;

namespace Pharmacy2016.gui.core.info.warehouse
{
    public partial class WareHouseForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц.

            private static WareHouseForm INSTANCE = new WareHouseForm();

            public static WareHouseForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private readonly int FORM_ID = 3004;

            private bool isExpandTree = false;

            private bool isInsertPharmacist = false;

            public DataRowView currentHouseUserView;

            public DataRowView currentWareHouseView;

            private System.IO.Stream layout_wareHouse = null;
            private System.IO.Stream layout_houseUser = null;

            private WareHouseForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initLayout();
                RoleUtil.addForm(FORM_ID, this);
                initDataSource();
            }

            private void initDataSource()
            {
                treeListWareHouse.DataSource = UserDataSet.WareHouseBindingSource;
                gridControlHouseUser.DataSource = UserDataSet.HouseUserBindingSource;
                repositoryItemGridLookUpEditUser.DataSource = InformationDataSet.SystemUserBindingSource;
                repositoryItemTreeListLookUpEditWareHouse.DataSource = UserDataSet.WareHouseOtherBindingSource;
            }

            private void initLayout()
            {
                layout_wareHouse = new System.IO.MemoryStream();
                treeListWareHouse.SaveLayoutToStream(layout_wareHouse);
                layout_wareHouse.Seek(0, System.IO.SeekOrigin.Begin);

                layout_houseUser = new System.IO.MemoryStream();
                gridViewHouseUser.SaveLayoutToStream(layout_houseUser);
                layout_houseUser.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion

        #region Форм хаах, нээх функц.

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        checkRole();
                        setDefaultHouse();
                        
                        UserDataSet.Instance.HouseUser.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.HouseUser.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        reload();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                if (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID || UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                {
                    controlNavigatorWareHouse.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                    controlNavigatorWareHouse.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                    controlNavigatorWareHouse.Buttons.CustomButtons[3].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];

                    gridControlHouseUser.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                    gridControlHouseUser.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                    gridViewHouseUser.OptionsBehavior.ReadOnly = !SystemUtil.SELECTED_DB_READONLY && !roles[2];
                }
                else
                {
                    controlNavigatorWareHouse.Buttons.CustomButtons[1].Visible = false;
                    controlNavigatorWareHouse.Buttons.CustomButtons[2].Visible = false;
                    controlNavigatorWareHouse.Buttons.CustomButtons[3].Visible = false;

                    gridControlHouseUser.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlHouseUser.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridViewHouseUser.OptionsBehavior.ReadOnly = false;
                }
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;
                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("wareHouse");
                if (layout != null)
                {
                    treeListWareHouse.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    treeListWareHouse.RestoreLayoutFromStream(layout_wareHouse);
                    layout_wareHouse.Seek(0, System.IO.SeekOrigin.Begin);
                }

                layout = UserUtil.getLayout("houseUser");
                if (layout != null)
                {
                    gridViewHouseUser.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewHouseUser.RestoreLayoutFromStream(layout_houseUser);
                    layout_houseUser.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }

            private void WareHouseForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                isInsertPharmacist = false;
                Hide();
            }

            private void clearData()
            {
                UserDataSet.Instance.WareHouse.Clear();
                UserDataSet.Instance.HouseUser.Clear();
            }

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                UserDataSet.WareHouseTableAdapter.Fill(UserDataSet.Instance.WareHouse, HospitalUtil.getHospitalId());
                UserDataSet.HouseUserTableAdapter.Fill(UserDataSet.Instance.HouseUser, HospitalUtil.getHospitalId());
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                ReportDataSet.WardConnTableAdapter.Fill(ReportDataSet.Instance.WardConn);
                gridViewHouseUser.ActiveFilterString = "";
                treeListWareHouse.ExpandAll();
                isExpandTree = true;
                controlNavigatorWareHouse.Buttons.CustomButtons[0].ImageIndex = 14;
                ProgramUtil.closeWaitDialog();
            }

            private void gridViewHouseUser_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID)
                {
                    DataRowView User = (DataRowView)UserDataSet.HouseUserBindingSource.Current;
                    User["userID"] = UserUtil.getUserId();
                }
            }

        #endregion

        #region Формын эвэнтүүд.

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                //saveLayout();
            }

            private void controlNavigatorWareHouse_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addWareHouse();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && treeListWareHouse.FocusedNode != null)
                    {
                        deleteWareHouse();
                    }
                    else if (String.Equals(e.Button.Tag, "edit") && treeListWareHouse.FocusedNode != null)
                    {
                        editWareHouse();
                    }
                    else if (String.Equals(e.Button.Tag, "expand"))
                    {
                        expandTree();
                    }
                }
            }

            private void gridControlHouseUser_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addHouseUser();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewHouseUser.GetFocusedRow() != null)
                    {
                        deleteHouseUser();
                    }
                }
            }

            private void gridViewHouseUser_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
            {
                if (!gridViewHouseUser.HasColumnErrors)
                    saveHouseUser();
            }

            private void treeListWareHouse_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
            {
                if (treeListWareHouse.FocusedNode != null)
                {
                    gridViewHouseUser.ActiveFilterString = "wareHouseID = '" + treeListWareHouse.FocusedNode.GetValue("id") + "'";
                }
            }

            private void gridViewHouseUser_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
            {
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                object value = gridViewHouseUser.GetFocusedRowCellValue(gridColumnUserID);
                object ware = gridViewHouseUser.GetFocusedRowCellValue(gridColumnWarehouseID);
                if (ware == DBNull.Value || ware == null || ware.ToString().Trim().Equals(""))
                {
                    errorText = "Агуулах сонгоно уу!";
                    gridViewHouseUser.SetColumnError(gridColumnWarehouseID, errorText);
                    e.Valid = false;
                    e.ErrorText = errorText;
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);
                    return;
                }
                if (value == DBNull.Value || value == null || value.ToString().Trim().Equals(""))
                {
                    errorText = "Агуулах хариуцсан хэрэглэгчийг оруулна уу!";
                    gridViewHouseUser.SetColumnError(gridColumnUserID, errorText);
                    e.Valid = false;
                    e.ErrorText = errorText;
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);
                    return;
                }

                if (e.Valid)
                {
                    DataRow[] rows = UserDataSet.Instance.HouseUser.Select("userID = '" + gridViewHouseUser.GetFocusedRowCellValue(gridColumnUserID).ToString() + "' AND wareHouseID = '" + gridViewHouseUser.GetFocusedRowCellValue(gridColumnWarehouseID).ToString() + "'");
                    if (rows.Length != 0)
                    {
                        if (isInsertPharmacist)
                        {
                            e.Valid = false;
                            errorText = "Ажилтан, агуулах давхацсан байна.";
                            gridViewHouseUser.SetColumnError(gridColumnWarehouseID, errorText);
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            ProgramUtil.showAlertDialog(this, Instance.Text, text);
                        }
                        else
                        {
                            DataRow row = gridViewHouseUser.GetFocusedDataRow();
                            if (rows[0]["id"] != row["id"])
                            {
                                e.Valid = false;
                                errorText = "Ажилтан, агуулах давхацсан байна.";
                                gridViewHouseUser.SetColumnError(gridColumnWarehouseID, errorText);
                                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                                ProgramUtil.showAlertDialog(this, Instance.Text, text);
                            }
                        }
                    }
                }
                //for (var i = 0; i < UserDataSet.Instance.HouseUser.Rows.Count; i++)
                //{
                //    DataRow row = UserDataSet.Instance.HouseUser.Rows[i];
                //    Console.WriteLine(row["userID"] + " = " + value + " \t " + row["wareHouseID"] + " = " + gridViewHouseUser.GetFocusedRowCellValue(gridColumnWarehouseID));
                //    if (row["wareHouseID"].ToString().Equals(gridViewHouseUser.GetFocusedRowCellValue(gridColumnWarehouseID).ToString()) && !row["id"].ToString().Equals(gridViewHouseUser.GetFocusedDataRow()["id"].ToString()))
                //    {
                //        errorText = "Агуулах хариуцсан хэрэглэгчийг бүртгэсэн байна!";
                //        gridViewHouseUser.SetColumnError(gridColumnUserID, errorText);
                //        e.Valid = false;
                //        e.ErrorText = errorText;
                //        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //        ProgramUtil.showAlertDialog(this, Instance.Text, text);
                //        return;
                //    }
                //}
                
                //string text = "", errorText;
                //ProgramUtil.closeAlertDialog();
                //GridColumn colUserID = gridViewHouseUser.Columns["userID"];

                //String userID = Convert.ToString(gridViewHouseUser.GetRowCellValue(gridViewHouseUser.FocusedRowHandle, colUserID));
                ////double unitsOnOrder = Convert.ToDouble(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, colUnitsOnOrder));
                //if (userID == null || userID == "")
                //{
                //    errorText = "Агуулах хариуцсан хэрэглэгчийг оруулна уу!";
                //    gridViewHouseUser.SetColumnError(colUserID, errorText);
                //    e.Valid = false;
                //    e.ErrorText = errorText;
                //    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    ProgramUtil.showAlertDialog(this, Instance.Text, text);
                //}

                //DataTable nodes = UserDataSet.Instance.HouseUser;
                //string uID = textEditUs.EditValue.ToString();
                //string wareHouseID = gridViewHouseUser.GetFocusedDataRow()["wareHouseID"].ToString();
                //bool exist = false;
                //for (int i = 0; i < nodes.Rows.Count; i++)
                //{
                //    if (nodes.Rows[i]["userID"].ToString().Equals(uID) && nodes.Rows[i]["wareHouseID"].ToString().Equals(wareHouseID))
                //    {
                //        exist = true;
                //        break;
                //    }
                //}
                //if (exist)
                //{
                //    errorText = "Хэрэглэгч уг агуулахад аль хэдийн бүртгэгдсэн байна!";
                //    gridViewHouseUser.SetColumnError(colUserID, errorText);
                //    e.Valid = false;
                //    e.ErrorText = errorText;
                //    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    ProgramUtil.showAlertDialog(this, Instance.Text, text);
                //    return;
                //}
            }

            private void simpleButtonLayout_Click_1(object sender, EventArgs e)
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    gridViewHouseUser.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    System.IO.StreamReader reader = new System.IO.StreamReader(str);
                    string layout = reader.ReadToEnd();
                    UserUtil.saveUserLayout("houseUser", layout);
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }  
            }

            private void gridViewHouseUser_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                e.ExceptionMode = ExceptionMode.NoAction;
            }

        #endregion

        #region Агуулах нэмэх, засах, устгах функцууд.

            public void setDefaultHouse()
            {
                UserDataSet.Instance.WareHouse.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                UserDataSet.Instance.WareHouse.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                UserDataSet.Instance.WareHouse.idColumn.DefaultValue = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            }
            
            private void addWareHouse()
            {
                addDoubleHouse();
                WareHouseInUpForm.Instance.showForm(1);
            }

            public void addDoubleHouse()
            {
                if (treeListWareHouse.FocusedNode == null)
                    UserDataSet.Instance.WareHouse.pidColumn.DefaultValue = 0;
                else if (treeListWareHouse.FocusedNode != null && treeListWareHouse.FocusedNode.Level == 0)
                    UserDataSet.Instance.WareHouse.pidColumn.DefaultValue = treeListWareHouse.FocusedNode.GetValue("id");

                currentWareHouseView = (DataRowView)UserDataSet.WareHouseBindingSource.AddNew();
            }

            private void deleteWareHouse()
            {
                object ret = UserDataSet.QueriesTableAdapter.can_delete_ware_house(HospitalUtil.getHospitalId(), treeListWareHouse.FocusedNode.GetValue("id").ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }

                TreeListNode node = treeListWareHouse.FocusedNode;
                if (node.HasChildren)
                {
                    XtraMessageBox.Show("Дэд ангилалтай тул устгаж болохгүй!", "Агуулах");
                    return;
                }
                else
                {
                    DataTable nodes = UserDataSet.Instance.HouseUser;
                    string wareHouseID = treeListWareHouse.FocusedNode.GetValue("id").ToString();
                    bool exist = false;
                    for (int i = 0; i < nodes.Rows.Count; i++)
                    {
                        if (nodes.Rows[i]["wareHouseID"].ToString().Equals(wareHouseID))
                        {
                            exist = true;
                            break;
                        }
                    }
                    if (exist)
                    {
                        XtraMessageBox.Show("Холбоотой мэдээлэл байгаа тул устгаж болохгүй!", "Агуулах");
                        return;
                    }
                }


                DialogResult dialogResult = XtraMessageBox.Show("Энэ ангилалыг устгах уу!", "Агуулах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    treeListWareHouse.DeleteNode(treeListWareHouse.FocusedNode);
                    UserDataSet.WareHouseBindingSource.EndEdit();
                    UserDataSet.WareHouseTableAdapter.Update(UserDataSet.Instance.WareHouse);
                }
            }

            private void editWareHouse()
            {
                currentWareHouseView = (DataRowView)UserDataSet.WareHouseBindingSource.Current;
                WareHouseInUpForm.Instance.showForm(2);
            }

            private void expandTree()
            {
                if (isExpandTree)
                {
                    treeListWareHouse.CollapseAll();
                    controlNavigatorWareHouse.Buttons.CustomButtons[0].ImageIndex = 17;
                }
                else
                {
                    treeListWareHouse.ExpandAll();
                    controlNavigatorWareHouse.Buttons.CustomButtons[0].ImageIndex = 14;
                }
                isExpandTree = !isExpandTree;
            }

        #endregion     

        #region Агуулах хэрэглэгч засах, нэмэх, устгах функцууд.

            private void addHouseUser()
            {
                if (treeListWareHouse.FocusedNode != null)
                {
                    DataRow[] rows = UserDataSet.Instance.HouseUser.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND wareHouseID = '" + treeListWareHouse.FocusedNode.GetValue("id") + "'");
                    if (rows.Length > 1)
                    {
                        DataRowView view = (DataRowView)UserDataSet.HouseUserBindingSource.Current;
                        if (view.IsNew)
                        {
                            UserDataSet.HouseUserBindingSource.CancelEdit();
                            UserDataSet.Instance.HouseUser.RejectChanges();
                        }
                    }
                    isInsertPharmacist = true;
                    DataRowView HouseUser = (DataRowView)UserDataSet.HouseUserBindingSource.AddNew();
                    HouseUser["id"] = UserDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    HouseUser["wareHouseID"] = treeListWareHouse.FocusedNode.GetValue("id");
                    gridViewHouseUser.ShowEditForm();
                }
            }

            private void deleteHouseUser()
            {
                object ret = UserDataSet.QueriesTableAdapter.can_delete_ware_house_user(HospitalUtil.getHospitalId(), gridViewHouseUser.GetFocusedDataRow()["wareHouseID"].ToString(), gridViewHouseUser.GetFocusedRowCellValue("userID").ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Агуулах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    DataRowView view = (DataRowView)gridViewHouseUser.GetFocusedRow();
                    if (view != null)
                    {
                        UserUtil.removeUserWareHouse(view);
                    }  
                    gridViewHouseUser.DeleteSelectedRows();
                    saveHouseUser();
                }
            }

            private void saveHouseUser()
            {
                try
                {
                    UserDataSet.HouseUserBindingSource.EndEdit();
                    UserDataSet.HouseUserTableAdapter.Update(UserDataSet.Instance.HouseUser);
                    isInsertPharmacist = false;
                    gridViewHouseUser.ClearColumnErrors();
                    for (int i = 0; gridViewHouseUser != null && i < gridViewHouseUser.RowCount; i++)
                    {
                        DataRow row = gridViewHouseUser.GetDataRow(i);
                        if (row != null)
                        {
                            UserUtil.addUserWareHouse(row);
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                }
            }

        #endregion                               

            private void dropDownButtonPrint_Click(object sender, EventArgs e)
            {
                ProgramUtil.convertGrid(gridViewHouseUser, "Агуулахын бүртгэл");
            }
        
    }
}