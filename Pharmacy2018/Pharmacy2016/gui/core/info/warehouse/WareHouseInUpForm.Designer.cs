﻿namespace Pharmacy2016.gui.core.info.warehouse
{
    partial class WareHouseInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.saveButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.spinEditNumber = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditIsUse = new DevExpress.XtraEditors.CheckEdit();
            this.treeListLookUpEditWareHouses = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.textEditAcoulusID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsUse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWareHouses.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAcoulusID.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveButton.Location = new System.Drawing.Point(266, 276);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 6;
            this.saveButton.Text = "Хадгалах";
            this.saveButton.ToolTip = "Өгөгдлийг хадгалах";
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(347, 276);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Гарах";
            this.cancelButton.ToolTip = "Гарах";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditDescription.EditValue = "";
            this.memoEditDescription.EnterMoveNextControl = true;
            this.memoEditDescription.Location = new System.Drawing.Point(119, 104);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(280, 109);
            this.memoEditDescription.TabIndex = 4;
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(63, 81);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(50, 13);
            this.labelControl4.TabIndex = 20;
            this.labelControl4.Text = "Дугаар* :";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(67, 106);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(46, 13);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "Тайлбар:";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(27, 55);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(86, 13);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "Агуулахын нэр*:";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(24, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(89, 13);
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "Үндсэн ангилал*:";
            // 
            // textEditName
            // 
            this.textEditName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditName.EnterMoveNextControl = true;
            this.textEditName.Location = new System.Drawing.Point(119, 52);
            this.textEditName.Name = "textEditName";
            this.textEditName.Properties.MaxLength = 50;
            this.textEditName.Size = new System.Drawing.Size(200, 20);
            this.textEditName.TabIndex = 2;
            // 
            // spinEditNumber
            // 
            this.spinEditNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditNumber.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditNumber.EnterMoveNextControl = true;
            this.spinEditNumber.Location = new System.Drawing.Point(119, 78);
            this.spinEditNumber.Name = "spinEditNumber";
            this.spinEditNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditNumber.Properties.DisplayFormat.FormatString = "n0";
            this.spinEditNumber.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditNumber.Properties.EditFormat.FormatString = "n0";
            this.spinEditNumber.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditNumber.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditNumber.Properties.Mask.EditMask = "n0";
            this.spinEditNumber.Properties.MaxLength = 50;
            this.spinEditNumber.Properties.NullText = "0";
            this.spinEditNumber.Size = new System.Drawing.Size(200, 20);
            this.spinEditNumber.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(41, 220);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(72, 13);
            this.labelControl5.TabIndex = 22;
            this.labelControl5.Text = "Ашиглах эсэх:";
            // 
            // checkEditIsUse
            // 
            this.checkEditIsUse.EnterMoveNextControl = true;
            this.checkEditIsUse.Location = new System.Drawing.Point(119, 219);
            this.checkEditIsUse.Name = "checkEditIsUse";
            this.checkEditIsUse.Properties.Caption = "";
            this.checkEditIsUse.Size = new System.Drawing.Size(75, 19);
            this.checkEditIsUse.TabIndex = 5;
            // 
            // treeListLookUpEditWareHouses
            // 
            this.treeListLookUpEditWareHouses.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWareHouses.EditValue = "";
            this.treeListLookUpEditWareHouses.Location = new System.Drawing.Point(119, 26);
            this.treeListLookUpEditWareHouses.Name = "treeListLookUpEditWareHouses";
            this.treeListLookUpEditWareHouses.Properties.ActionButtonIndex = 1;
            this.treeListLookUpEditWareHouses.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Minus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "clear", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWareHouses.Properties.DisplayMember = "name";
            this.treeListLookUpEditWareHouses.Properties.NullText = "";
            this.treeListLookUpEditWareHouses.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditWareHouses.Properties.ValueMember = "id";
            this.treeListLookUpEditWareHouses.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.treeListLookUpEditWareHouses_Properties_ButtonClick);
            this.treeListLookUpEditWareHouses.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWareHouses.TabIndex = 1;
            this.treeListLookUpEditWareHouses.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWareHouses_KeyUp);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(17, 55);
            this.treeList1.Name = "treeList1";
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Эмийн төрөл";
            this.treeListColumn1.FieldName = "name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // textEditAcoulusID
            // 
            this.textEditAcoulusID.Location = new System.Drawing.Point(299, 219);
            this.textEditAcoulusID.Name = "textEditAcoulusID";
            this.textEditAcoulusID.Size = new System.Drawing.Size(100, 20);
            this.textEditAcoulusID.TabIndex = 23;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(193, 220);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(100, 13);
            this.labelControl6.TabIndex = 24;
            this.labelControl6.Text = "Холбогдох дугаар :";
            // 
            // WareHouseInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(434, 311);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.textEditAcoulusID);
            this.Controls.Add(this.treeListLookUpEditWareHouses);
            this.Controls.Add(this.checkEditIsUse);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.memoEditDescription);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.textEditName);
            this.Controls.Add(this.spinEditNumber);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WareHouseInUpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Агуулах";
            this.Shown += new System.EventHandler(this.WareHouseInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsUse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWareHouses.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAcoulusID.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton saveButton;
        private DevExpress.XtraEditors.SimpleButton cancelButton;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraEditors.SpinEdit spinEditNumber;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CheckEdit checkEditIsUse;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWareHouses;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraEditors.TextEdit textEditAcoulusID;
        private DevExpress.XtraEditors.LabelControl labelControl6;
    }
}