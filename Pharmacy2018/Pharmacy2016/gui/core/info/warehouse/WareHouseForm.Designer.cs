﻿namespace Pharmacy2016.gui.core.info.warehouse
{
    partial class WareHouseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WareHouseForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.dockManagerWare = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.treeListWareHouse = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnWareHouse = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnNumber = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnDesc = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnIsUse = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.textEditUs = new DevExpress.XtraEditors.TextEdit();
            this.controlNavigatorWareHouse = new DevExpress.XtraEditors.ControlNavigator();
            this.gridControlHouseUser = new DevExpress.XtraGrid.GridControl();
            this.gridViewHouseUser = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnWarehouseID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTreeListLookUpEditWareHouse = new DevExpress.XtraEditors.Repository.RepositoryItemTreeListLookUpEdit();
            this.repositoryItemTreeListLookUpEditWareHouseTreeList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridColumnUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditUser = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEditUserView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditCreatedDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.treeListColumnName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dockManagerWare)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListWareHouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHouseUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHouseUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTreeListLookUpEditWareHouse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTreeListLookUpEditWareHouseTreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUserView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditCreatedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditCreatedDate.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonLayout);
            this.panelControl1.Controls.Add(this.dropDownButtonPrint);
            this.panelControl1.Controls.Add(this.simpleButtonReload);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(200, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(584, 44);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(551, 11);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 14;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click_1);
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(415, 11);
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.dropDownButtonPrint.Size = new System.Drawing.Size(130, 23);
            this.dropDownButtonPrint.TabIndex = 13;
            this.dropDownButtonPrint.Text = "Хөрвүүлэх";
            this.dropDownButtonPrint.Click += new System.EventHandler(this.dropDownButtonPrint_Click);
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(6, 11);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonReload.TabIndex = 12;
            this.simpleButtonReload.Text = "Сэргээх";
            this.simpleButtonReload.ToolTip = "Тасаг, өрөө, орны мэдээллийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // dockManagerWare
            // 
            this.dockManagerWare.Form = this;
            this.dockManagerWare.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManagerWare.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("134531ba-ba06-4772-850a-d62ca26ba4e6");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanel1.Size = new System.Drawing.Size(200, 561);
            this.dockPanel1.Text = "Агуулах";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.treeListWareHouse);
            this.dockPanel1_Container.Controls.Add(this.panelControl2);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(192, 534);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // treeListWareHouse
            // 
            this.treeListWareHouse.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnWareHouse,
            this.treeListColumnNumber,
            this.treeListColumnDesc,
            this.treeListColumnIsUse});
            this.treeListWareHouse.CustomizationFormBounds = new System.Drawing.Rectangle(1064, 440, 216, 209);
            this.treeListWareHouse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListWareHouse.KeyFieldName = "id";
            this.treeListWareHouse.Location = new System.Drawing.Point(0, 0);
            this.treeListWareHouse.Name = "treeListWareHouse";
            this.treeListWareHouse.OptionsBehavior.Editable = false;
            this.treeListWareHouse.OptionsBehavior.EnableFiltering = true;
            this.treeListWareHouse.OptionsBehavior.PopulateServiceColumns = true;
            this.treeListWareHouse.ParentFieldName = "pid";
            this.treeListWareHouse.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.treeListWareHouse.Size = new System.Drawing.Size(192, 509);
            this.treeListWareHouse.TabIndex = 17;
            this.treeListWareHouse.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.treeListWareHouse_FocusedNodeChanged);
            // 
            // treeListColumnWareHouse
            // 
            this.treeListColumnWareHouse.Caption = "Агуулах";
            this.treeListColumnWareHouse.FieldName = "name";
            this.treeListColumnWareHouse.Name = "treeListColumnWareHouse";
            this.treeListColumnWareHouse.Visible = true;
            this.treeListColumnWareHouse.VisibleIndex = 0;
            this.treeListColumnWareHouse.Width = 385;
            // 
            // treeListColumnNumber
            // 
            this.treeListColumnNumber.Caption = "Дугаар";
            this.treeListColumnNumber.FieldName = "number";
            this.treeListColumnNumber.Name = "treeListColumnNumber";
            this.treeListColumnNumber.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumnNumber.Width = 139;
            // 
            // treeListColumnDesc
            // 
            this.treeListColumnDesc.Caption = "Тайлбар";
            this.treeListColumnDesc.FieldName = "description";
            this.treeListColumnDesc.Name = "treeListColumnDesc";
            this.treeListColumnDesc.Width = 160;
            // 
            // treeListColumnIsUse
            // 
            this.treeListColumnIsUse.Caption = "Ашиглах эсэх";
            this.treeListColumnIsUse.FieldName = "isUse";
            this.treeListColumnIsUse.Name = "treeListColumnIsUse";
            this.treeListColumnIsUse.Width = 283;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.textEditUs);
            this.panelControl2.Controls.Add(this.controlNavigatorWareHouse);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 509);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(192, 25);
            this.panelControl2.TabIndex = 17;
            // 
            // textEditUs
            // 
            this.textEditUs.Location = new System.Drawing.Point(387, 3);
            this.textEditUs.Name = "textEditUs";
            this.textEditUs.Size = new System.Drawing.Size(100, 20);
            this.textEditUs.TabIndex = 4;
            this.textEditUs.Visible = false;
            // 
            // controlNavigatorWareHouse
            // 
            this.controlNavigatorWareHouse.Buttons.Append.Visible = false;
            this.controlNavigatorWareHouse.Buttons.CancelEdit.Visible = false;
            this.controlNavigatorWareHouse.Buttons.Edit.Visible = false;
            this.controlNavigatorWareHouse.Buttons.EndEdit.Visible = false;
            this.controlNavigatorWareHouse.Buttons.First.Visible = false;
            this.controlNavigatorWareHouse.Buttons.Last.Visible = false;
            this.controlNavigatorWareHouse.Buttons.Next.Visible = false;
            this.controlNavigatorWareHouse.Buttons.NextPage.Visible = false;
            this.controlNavigatorWareHouse.Buttons.Prev.Visible = false;
            this.controlNavigatorWareHouse.Buttons.PrevPage.Visible = false;
            this.controlNavigatorWareHouse.Buttons.Remove.Visible = false;
            this.controlNavigatorWareHouse.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 17, true, true, "Задлах", "expand"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "edit")});
            this.controlNavigatorWareHouse.Dock = System.Windows.Forms.DockStyle.Left;
            this.controlNavigatorWareHouse.Location = new System.Drawing.Point(2, 2);
            this.controlNavigatorWareHouse.Name = "controlNavigatorWareHouse";
            this.controlNavigatorWareHouse.ShowToolTips = true;
            this.controlNavigatorWareHouse.Size = new System.Drawing.Size(90, 21);
            this.controlNavigatorWareHouse.TabIndex = 3;
            this.controlNavigatorWareHouse.Text = "controlNavigator1";
            this.controlNavigatorWareHouse.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.controlNavigatorWareHouse_ButtonClick);
            // 
            // gridControlHouseUser
            // 
            this.gridControlHouseUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlHouseUser.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlHouseUser.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlHouseUser.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlHouseUser.EmbeddedNavigator.Buttons.EnabledAutoRepeat = false;
            this.gridControlHouseUser.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlHouseUser.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlHouseUser.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlHouseUser.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlHouseUser.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlHouseUser_EmbeddedNavigator_ButtonClick);
            this.gridControlHouseUser.Location = new System.Drawing.Point(200, 44);
            this.gridControlHouseUser.LookAndFeel.SkinName = "Office 2013";
            this.gridControlHouseUser.MainView = this.gridViewHouseUser;
            this.gridControlHouseUser.Name = "gridControlHouseUser";
            this.gridControlHouseUser.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEditCreatedDate,
            this.repositoryItemGridLookUpEditUser,
            this.repositoryItemTreeListLookUpEditWareHouse});
            this.gridControlHouseUser.Size = new System.Drawing.Size(584, 517);
            this.gridControlHouseUser.TabIndex = 10;
            this.gridControlHouseUser.UseEmbeddedNavigator = true;
            this.gridControlHouseUser.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHouseUser});
            // 
            // gridViewHouseUser
            // 
            this.gridViewHouseUser.ActiveFilterEnabled = false;
            this.gridViewHouseUser.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnWarehouseID,
            this.gridColumnUserID});
            this.gridViewHouseUser.GridControl = this.gridControlHouseUser;
            this.gridViewHouseUser.Name = "gridViewHouseUser";
            this.gridViewHouseUser.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewHouseUser.OptionsCustomization.AllowGroup = false;
            this.gridViewHouseUser.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewHouseUser.OptionsFind.AllowFindPanel = false;
            this.gridViewHouseUser.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewHouseUser.OptionsView.ShowAutoFilterRow = true;
            this.gridViewHouseUser.OptionsView.ShowGroupPanel = false;
            this.gridViewHouseUser.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewHouseUser_InitNewRow);
            this.gridViewHouseUser.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewHouseUser_InvalidRowException);
            this.gridViewHouseUser.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewHouseUser_ValidateRow);
            this.gridViewHouseUser.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewHouseUser_RowUpdated);
            // 
            // gridColumnWarehouseID
            // 
            this.gridColumnWarehouseID.Caption = "Агуулах";
            this.gridColumnWarehouseID.ColumnEdit = this.repositoryItemTreeListLookUpEditWareHouse;
            this.gridColumnWarehouseID.FieldName = "wareHouseID";
            this.gridColumnWarehouseID.Name = "gridColumnWarehouseID";
            this.gridColumnWarehouseID.Visible = true;
            this.gridColumnWarehouseID.VisibleIndex = 0;
            this.gridColumnWarehouseID.Width = 347;
            // 
            // repositoryItemTreeListLookUpEditWareHouse
            // 
            this.repositoryItemTreeListLookUpEditWareHouse.AutoHeight = false;
            this.repositoryItemTreeListLookUpEditWareHouse.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTreeListLookUpEditWareHouse.DisplayMember = "name";
            this.repositoryItemTreeListLookUpEditWareHouse.Name = "repositoryItemTreeListLookUpEditWareHouse";
            this.repositoryItemTreeListLookUpEditWareHouse.TreeList = this.repositoryItemTreeListLookUpEditWareHouseTreeList;
            this.repositoryItemTreeListLookUpEditWareHouse.ValueMember = "id";
            // 
            // repositoryItemTreeListLookUpEditWareHouseTreeList
            // 
            this.repositoryItemTreeListLookUpEditWareHouseTreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.repositoryItemTreeListLookUpEditWareHouseTreeList.KeyFieldName = "id";
            this.repositoryItemTreeListLookUpEditWareHouseTreeList.Location = new System.Drawing.Point(0, 0);
            this.repositoryItemTreeListLookUpEditWareHouseTreeList.Name = "repositoryItemTreeListLookUpEditWareHouseTreeList";
            this.repositoryItemTreeListLookUpEditWareHouseTreeList.OptionsBehavior.EnableFiltering = true;
            this.repositoryItemTreeListLookUpEditWareHouseTreeList.ParentFieldName = "pid";
            this.repositoryItemTreeListLookUpEditWareHouseTreeList.Size = new System.Drawing.Size(400, 200);
            this.repositoryItemTreeListLookUpEditWareHouseTreeList.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Агуулах нэрс";
            this.treeListColumn1.FieldName = "name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // gridColumnUserID
            // 
            this.gridColumnUserID.Caption = "Агуулах хариуцсан ажилтан";
            this.gridColumnUserID.ColumnEdit = this.repositoryItemGridLookUpEditUser;
            this.gridColumnUserID.CustomizationCaption = "Агуулах хариуцсан ажилтан";
            this.gridColumnUserID.FieldName = "userID";
            this.gridColumnUserID.Name = "gridColumnUserID";
            this.gridColumnUserID.OptionsEditForm.Caption = "Ажилтан";
            this.gridColumnUserID.Visible = true;
            this.gridColumnUserID.VisibleIndex = 1;
            this.gridColumnUserID.Width = 345;
            // 
            // repositoryItemGridLookUpEditUser
            // 
            this.repositoryItemGridLookUpEditUser.AutoHeight = false;
            this.repositoryItemGridLookUpEditUser.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditUser.DisplayMember = "fullName";
            this.repositoryItemGridLookUpEditUser.Name = "repositoryItemGridLookUpEditUser";
            this.repositoryItemGridLookUpEditUser.NullText = "";
            this.repositoryItemGridLookUpEditUser.PopupFormSize = new System.Drawing.Size(500, 300);
            this.repositoryItemGridLookUpEditUser.ValueMember = "id";
            this.repositoryItemGridLookUpEditUser.View = this.repositoryItemGridLookUpEditUserView;
            // 
            // repositoryItemGridLookUpEditUserView
            // 
            this.repositoryItemGridLookUpEditUserView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            this.repositoryItemGridLookUpEditUserView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEditUserView.Name = "repositoryItemGridLookUpEditUserView";
            this.repositoryItemGridLookUpEditUserView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEditUserView.OptionsView.ShowAutoFilterRow = true;
            this.repositoryItemGridLookUpEditUserView.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Нэр";
            this.gridColumn3.FieldName = "firstName";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Овог";
            this.gridColumn2.FieldName = "lastName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Эрх";
            this.gridColumn4.FieldName = "roleName";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Регистер";
            this.gridColumn5.FieldName = "register";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тасаг";
            this.gridColumn6.FieldName = "wardName";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Дугаар";
            this.gridColumn7.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn7.FieldName = "id";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            // 
            // repositoryItemDateEditCreatedDate
            // 
            this.repositoryItemDateEditCreatedDate.AutoHeight = false;
            this.repositoryItemDateEditCreatedDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditCreatedDate.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditCreatedDate.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEditCreatedDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEditCreatedDate.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEditCreatedDate.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEditCreatedDate.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEditCreatedDate.Mask.IgnoreMaskBlank = false;
            this.repositoryItemDateEditCreatedDate.Name = "repositoryItemDateEditCreatedDate";
            // 
            // treeListColumnName
            // 
            this.treeListColumnName.Caption = "Агуулахын нэр";
            this.treeListColumnName.CustomizationCaption = "Өрөө";
            this.treeListColumnName.FieldName = "name";
            this.treeListColumnName.MinWidth = 70;
            this.treeListColumnName.Name = "treeListColumnName";
            this.treeListColumnName.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumnName.Visible = true;
            this.treeListColumnName.VisibleIndex = 0;
            this.treeListColumnName.Width = 360;
            // 
            // WareHouseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.gridControlHouseUser);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.dockPanel1);
            this.Name = "WareHouseForm";
            this.ShowInTaskbar = false;
            this.Text = "Агуулах бүртгэл";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WareHouseForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dockManagerWare)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListWareHouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditUs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHouseUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHouseUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTreeListLookUpEditWareHouse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTreeListLookUpEditWareHouseTreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUserView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditCreatedDate.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditCreatedDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraBars.Docking.DockManager dockManagerWare;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraTreeList.TreeList treeListWareHouse;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.ControlNavigator controlNavigatorWareHouse;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnNumber;
        public DevExpress.XtraGrid.GridControl gridControlHouseUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHouseUser;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditCreatedDate;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEditUserView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnUserID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnWarehouseID;
        private DevExpress.XtraEditors.Repository.RepositoryItemTreeListLookUpEdit repositoryItemTreeListLookUpEditWareHouse;
        private DevExpress.XtraTreeList.TreeList repositoryItemTreeListLookUpEditWareHouseTreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnWareHouse;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnDesc;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnIsUse;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnName;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditUser;
        private DevExpress.XtraEditors.TextEdit textEditUs;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
    }
}