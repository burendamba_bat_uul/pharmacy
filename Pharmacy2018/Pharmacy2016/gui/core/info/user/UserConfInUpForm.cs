﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.patient;

namespace Pharmacy2016.gui.core.info.user
{
    /*
     * Ажлын газар, албан тушаал, мэргэжил нэмэх, засах цонх.
     * 
     */

    public partial class UserConfInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static UserConfInUpForm INSTANCE = new UserConfInUpForm();

            public static UserConfInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private int formType;

            private UserConfInUpForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, int fType, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    formType = fType;
                    insertConf();

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                textEditName.ErrorText = "";
                textEditName.Text = "";
                memoEditDescription.Text = "";
            }

            private void insertConf()
            {
                if (actionType == 1)
                {
                    Text = "Ажлын газар нэмэх";
                }
                else if (actionType == 2)
                {
                    Text = "Албан тушаал нэмэх";
                }
                else if (actionType == 3)
                {
                    Text = "Мэргэжил нэмэх";
                }
            }
        
            private void UserConfInUpForm_Shown(object sender, EventArgs e)
            {
                textEditName.Focus();
            }

        #endregion

        #region Формын event

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if ((textEditName.EditValue = textEditName.Text.Trim()).Equals(""))
                {
                    errorText = "Нэрийг оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    isRight = false;
                    textEditName.Focus();
                }
                memoEditDescription.EditValue = memoEditDescription.Text.Trim();
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    string newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());

                    if (actionType == 1)
                    {
                        InformationDataSet.Instance.Organization.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        InformationDataSet.Instance.Organization.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        InformationDataSet.Instance.Organization.idColumn.DefaultValue = newID;
                        DataRowView view = (DataRowView)InformationDataSet.OrganizationBindingSource.AddNew();
                        view["name"] = textEditName.Text;
                        view["description"] = memoEditDescription.Text;
                        view.EndEdit();
                        InformationDataSet.OrganizationTableAdapter.Update(InformationDataSet.Instance.Organization);
                        if (formType == 1)
                        {
                            UserInUpForm.Instance.insertOrganization(newID);
                        }
                        else if (formType == 2)
                        {
                            PatientInUpForm.Instance.insertOrganization(newID);
                        }
                    }
                    else if (actionType == 2)
                    {
                        InformationDataSet.Instance.Position.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        InformationDataSet.Instance.Position.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        InformationDataSet.Instance.Position.idColumn.DefaultValue = newID;
                        DataRowView view = (DataRowView)InformationDataSet.PositionBindingSource.AddNew();
                        view["name"] = textEditName.Text;
                        view["description"] = memoEditDescription.Text;
                        view.EndEdit();
                        InformationDataSet.PositionTableAdapter.Update(InformationDataSet.Instance.Position);
                        if (formType == 1)
                        {
                            UserInUpForm.Instance.insertPosition(newID);
                        }
                        else if (formType == 2)
                        {
                            PatientInUpForm.Instance.insertPosition(newID);
                        }
                    }
                    else if (actionType == 3)
                    {
                        InformationDataSet.Instance.Specialty.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        InformationDataSet.Instance.Specialty.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        InformationDataSet.Instance.Specialty.idColumn.DefaultValue = newID;
                        DataRowView view = (DataRowView)InformationDataSet.SpecialtyBindingSource.AddNew();
                        view["name"] = textEditName.Text;
                        view["description"] = memoEditDescription.Text;
                        view.EndEdit();
                        InformationDataSet.SpecialtyTableAdapter.Update(InformationDataSet.Instance.Specialty);
                        if (formType == 1)
                        {
                            UserInUpForm.Instance.insertSpecialty(newID);
                        }
                        else if (formType == 2)
                        {
                            PatientInUpForm.Instance.insertSpecialty(newID);
                        }
                    }

                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }

        #endregion

    }
}