﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using Pharmacy2016.util;
using DevExpress.XtraGrid.Columns;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;

namespace Pharmacy2016.gui.core.info.user
{
    /*
     * Ажлын газар, албан тушаал, хот аймаг, мэргэжлийг харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class UserConfForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static UserConfForm INSTANCE = new UserConfForm();

            public static UserConfForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 3007;

            private bool isInit = false;

            private bool isEditTree = false;

            private bool isExpandTree = false;

            private System.IO.Stream layout_organization = null;
            private System.IO.Stream layout_position = null;
            private System.IO.Stream layout_specialty = null;
            private System.IO.Stream layout_province = null;

            private UserConfForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initLayout();
                RoleUtil.addForm(FORM_ID, this);

                gridControlOrganization.DataSource = InformationDataSet.OrganizationBindingSource;
                gridControlPosition.DataSource = InformationDataSet.PositionBindingSource;
                gridControlSpecialty.DataSource = InformationDataSet.SpecialtyBindingSource;
                treeListProvince.DataSource = InformationDataSet.ProvinceBindingSource;
                reload();
            }

            private void initLayout()
            {
                layout_organization = new System.IO.MemoryStream();
                gridViewOrganization.SaveLayoutToStream(layout_organization);
                layout_organization.Seek(0, System.IO.SeekOrigin.Begin);

                layout_position = new System.IO.MemoryStream();
                gridViewPosition.SaveLayoutToStream(layout_position);
                layout_position.Seek(0, System.IO.SeekOrigin.Begin);

                layout_specialty = new System.IO.MemoryStream();
                gridViewSpecialty.SaveLayoutToStream(layout_specialty);
                layout_specialty.Seek(0, System.IO.SeekOrigin.Begin);

                layout_province = new System.IO.MemoryStream();
                treeListProvince.SaveLayoutToStream(layout_province);
                layout_province.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        checkRole();

                        InformationDataSet.Instance.Organization.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        InformationDataSet.Instance.Organization.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        InformationDataSet.Instance.Position.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        InformationDataSet.Instance.Position.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        InformationDataSet.Instance.Province.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        
                        InformationDataSet.Instance.Specialty.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        InformationDataSet.Instance.Specialty.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                gridControlOrganization.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlOrganization.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridViewOrganization.OptionsBehavior.Editable = !SystemUtil.SELECTED_DB_READONLY && roles[2];

                gridControlPosition.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlPosition.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridViewPosition.OptionsBehavior.Editable = !SystemUtil.SELECTED_DB_READONLY && roles[2];

                gridControlSpecialty.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlSpecialty.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridViewSpecialty.OptionsBehavior.Editable = !SystemUtil.SELECTED_DB_READONLY && roles[2];

                controlNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                controlNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                controlNavigator.Buttons.CustomButtons[3].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];

                simpleButtonExport.Visible = roles[4];
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;
                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("organization");
                if (layout != null)
                {
                    gridViewOrganization.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewOrganization.RestoreLayoutFromStream(layout_organization);
                    layout_organization.Seek(0, System.IO.SeekOrigin.Begin);
                }

                layout = UserUtil.getLayout("position");
                if (layout != null)
                {
                    gridViewPosition.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewPosition.RestoreLayoutFromStream(layout_position);
                    layout_position.Seek(0, System.IO.SeekOrigin.Begin);
                }

                layout = UserUtil.getLayout("specialty");
                if (layout != null)
                {
                    gridViewSpecialty.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewSpecialty.RestoreLayoutFromStream(layout_specialty);
                    layout_specialty.Seek(0, System.IO.SeekOrigin.Begin);
                }

                layout = UserUtil.getLayout("province");
                if (layout != null)
                {
                    treeListProvince.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    treeListProvince.RestoreLayoutFromStream(layout_province);
                    layout_province.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }
            
            private void OrganizationForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                //InformationDataSet.Instance.Organization.Clear();
                //InformationDataSet.Instance.Position.Clear();
                //InformationDataSet.Instance.Province.Clear();
                //InformationDataSet.Instance.Specialty.Clear();
            }

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.OrganizationTableAdapter.Fill(InformationDataSet.Instance.Organization, HospitalUtil.getHospitalId());
                InformationDataSet.PositionTableAdapter.Fill(InformationDataSet.Instance.Position, HospitalUtil.getHospitalId());
                InformationDataSet.ProvinceTableAdapter.Fill(InformationDataSet.Instance.Province);
                InformationDataSet.SpecialtyTableAdapter.Fill(InformationDataSet.Instance.Specialty, HospitalUtil.getHospitalId());

                treeListProvince.ExpandAll();
                isExpandTree = true;
                controlNavigator.Buttons.CustomButtons[0].ImageIndex = 14;
                ProgramUtil.closeWaitDialog();
            }

        #endregion

        #region Форм event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void simpleButtonExport_Click(object sender, EventArgs e)
            {
                if (xtraTabControl.SelectedTabPage == xtraTabPageOrganization)
                {
                    ProgramUtil.convertGrid(gridViewOrganization, "Ажлын газрын бүртгэл");
                }
                else if (xtraTabControl.SelectedTabPage == xtraTabPagePosition)
                {
                    ProgramUtil.convertGrid(gridViewPosition, "Албан тушаалын бүртгэл");
                }
                else if (xtraTabControl.SelectedTabPage == xtraTabPageCountry)
                {
                    ProgramUtil.convertGrid(treeListProvince, "Хот, аймаг, сумын бүртгэл");
                }
                else if (xtraTabControl.SelectedTabPage == xtraTabPageSpecialty)
                {
                    ProgramUtil.convertGrid(gridViewSpecialty, "Мэргэжлийн бүртгэл");
                }
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    System.IO.StreamReader reader = null;
                    if (xtraTabControl.SelectedTabPage == xtraTabPageOrganization)
                    {
                        gridViewOrganization.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        reader = new System.IO.StreamReader(str);
                        UserUtil.saveUserLayout("organization", reader.ReadToEnd());
                    }
                    else if (xtraTabControl.SelectedTabPage == xtraTabPagePosition)
                    {
                        gridViewPosition.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        reader = new System.IO.StreamReader(str);
                        UserUtil.saveUserLayout("position", reader.ReadToEnd());
                    }
                    else if (xtraTabControl.SelectedTabPage == xtraTabPageCountry)
                    {
                        treeListProvince.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        reader = new System.IO.StreamReader(str);
                        UserUtil.saveUserLayout("province", reader.ReadToEnd());
                    }
                    else if (xtraTabControl.SelectedTabPage == xtraTabPageSpecialty)
                    {
                        gridViewSpecialty.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        reader = new System.IO.StreamReader(str);
                        UserUtil.saveUserLayout("specialty", reader.ReadToEnd());
                        reader.Close();
                    }
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }


            private void gridControlOrganization_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addOrganization();
                    }
                    else if (String.Equals(e.Button.Tag, "delete"))
                    {
                        try
                        {
                            if (gridViewOrganization.GetFocusedRow() != null)
                                deleteOrganization();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                }
            }

            private void gridViewOrganization_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
            {
                try
                {
                    if (!gridViewOrganization.HasColumnErrors)
                        saveOrganization();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void gridViewOrganization_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
            {
                GridColumn col = gridViewOrganization.Columns["name"];
                if (gridViewOrganization.GetRowCellValue(e.RowHandle, col).ToString().Trim().Equals(""))
                {
                    e.Valid = false;
                    gridViewOrganization.SetColumnError(col, "Ажлын газрын нэрийг оруулна уу");
                }
            }

            private void gridViewOrganization_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }


            private void gridControlPosition_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addPosition();
                    }
                    else if (String.Equals(e.Button.Tag, "delete"))
                    {
                        try
                        {
                            if (gridViewPosition.GetFocusedRow() != null)
                                deletePosition();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                }
            }

            private void gridViewPosition_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
            {
                try
                {
                    if (!gridViewPosition.HasColumnErrors)
                        savePosition();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void gridViewPosition_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
            {
                GridColumn col = gridViewPosition.Columns["name"];
                if (gridViewPosition.GetRowCellValue(e.RowHandle, col).ToString().Trim().Equals(""))
                {
                    e.Valid = false;
                    gridViewPosition.SetColumnError(col, "Мэргэжлийн нэрийг оруулна уу");
                }
            }

            private void gridViewPosition_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }


            private void controlNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addCountry();
                    }
                    else if (String.Equals(e.Button.Tag, "delete"))
                    {
                        try
                        {
                            if (treeListProvince.FocusedNode != null)
                                deleteCountry();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "edit"))
                    {
                        editCountry();
                    }
                    else if (String.Equals(e.Button.Tag, "expand"))
                    {
                        expandTree();
                    }
                }
            }


            private void gridControlSpecialty_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addSpecialty();
                    }
                    else if (String.Equals(e.Button.Tag, "delete"))
                    {
                        try
                        {
                            if (gridViewSpecialty.GetFocusedRow() != null)
                                deleteSpecialty();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                }
            }

            private void gridViewSpecialty_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
            {
                try
                {
                    if (!gridViewSpecialty.HasColumnErrors)
                        saveSpecialty();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void gridViewSpecialty_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridViewSpecialty_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
            {
                GridColumn col = gridViewSpecialty.Columns["name"];
                if (gridViewSpecialty.GetRowCellValue(e.RowHandle, col).ToString().Trim().Equals(""))
                {
                    e.Valid = false;
                    gridViewSpecialty.SetColumnError(col, "Мэргэжлийн нэрийг оруулна уу");
                }
            }

        #endregion


        #region Ажлын газар нэмэх, засах, устгах функц

            private void addOrganization()
            {
                DataRowView currOrgView = (DataRowView)InformationDataSet.OrganizationBindingSource.Current;
                if (currOrgView.IsNew)
                {
                    InformationDataSet.OrganizationBindingSource.CancelEdit();
                    InformationDataSet.Instance.Organization.RejectChanges();
                }
                InformationDataSet.Instance.Organization.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                DataRowView view = (DataRowView)InformationDataSet.OrganizationBindingSource.AddNew();
                //view["id"] = InformationDataSet.QueriesTableAdapter.get_new_id();
                gridViewOrganization.ShowEditForm();
            }

            private void deleteOrganization()
            {
                object ret = InformationDataSet.QueriesTableAdapter.can_delete_organization(HospitalUtil.getHospitalId(), gridViewOrganization.GetFocusedDataRow()["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Ажлын газар устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    gridViewOrganization.DeleteRow(gridViewOrganization.GetFocusedDataSourceRowIndex());
                    saveOrganization();
                }
            }

            private void saveOrganization()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                InformationDataSet.OrganizationBindingSource.EndEdit();
                InformationDataSet.OrganizationTableAdapter.Update(InformationDataSet.Instance.Organization);
                ProgramUtil.closeWaitDialog();
            }

        #endregion

        #region Албан тушаал нэмэх, засах, устгах функц

            private void addPosition()
            {
                DataRowView currPosView = (DataRowView)InformationDataSet.PositionBindingSource.Current;
                if (currPosView.IsNew)
                {
                    InformationDataSet.PositionBindingSource.CancelEdit();
                    InformationDataSet.Instance.Position.RejectChanges();
                }
                InformationDataSet.Instance.Position.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                DataRowView view = (DataRowView)InformationDataSet.PositionBindingSource.AddNew();
                //view["id"] = InformationDataSet.QueriesTableAdapter.get_new_id();
                gridViewPosition.ShowEditForm();
            }

            private void deletePosition()
            {
                object ret = InformationDataSet.QueriesTableAdapter.can_delete_position(HospitalUtil.getHospitalId(), gridViewPosition.GetFocusedDataRow()["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Ажлын газар устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    gridViewPosition.DeleteRow(gridViewPosition.GetFocusedDataSourceRowIndex());
                    savePosition();
                }
            }

            private void savePosition()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                InformationDataSet.PositionBindingSource.EndEdit();
                InformationDataSet.PositionTableAdapter.Update(InformationDataSet.Instance.Position);
                ProgramUtil.closeWaitDialog();
            }

        #endregion

        #region Хот-аймаг нэмэх, засах, устгах функц

            private void addCountry()
            {
                if (!isValidateTreeNodeEmptyText())
                    return;

                Instance.Validate();
                TreeListNode parentNode = treeListProvince.FocusedNode;
                if (Convert.ToInt32(parentNode.GetValue("id")) > 0)
                {
                    treeListProvince.SetFocusedNode(treeListProvince.AppendNode("", parentNode));
                    treeListProvince.FocusedNode.SetValue("isCity", parentNode.GetValue("isCity"));
                }
                else
                {
                    XtraMessageBox.Show("Шинээр нэмэгдсэн ангилал дотор дахин шинэ ангилал нэмж болохгүй");
                }
            }

            private void editCountry()
            {
                if (!isValidateTreeNodeEmptyText())
                    return;

                if (isEditTree)
                {
                    controlNavigator.Buttons.CustomButtons[3].ImageIndex = 15;
                    saveCountry();
                }
                else
                {
                    controlNavigator.Buttons.CustomButtons[3].ImageIndex = 16;
                }
                isEditTree = !isEditTree;
                controlNavigator.Buttons.CustomButtons[0].Enabled = !isEditTree;
                controlNavigator.Buttons.CustomButtons[1].Enabled = isEditTree;
                controlNavigator.Buttons.CustomButtons[2].Enabled = isEditTree;
                treeListProvince.OptionsBehavior.Editable = isEditTree;
            }

            private void deleteCountry()
            {
                if (!isValidateTreeNodeEmptyText())
                    return;

                TreeListNode node = treeListProvince.FocusedNode;
                if(node.HasChildren)
                {
                    XtraMessageBox.Show("Энэ мөр дэд мөчиртэй тул устгаж болохгүй");
                    return;
                }
                //XtraMessageBox.Show(node.GetValue(treeListColumnName) + " = " + node.Id);
                // id баганы утга буруу ирээд байгаа тул 1-ээр нэмэгдүүлж өгөв. Энэ алдаа зүгээр болвол устгана
                object ret = InformationDataSet.QueriesTableAdapter.can_delete_province(HospitalUtil.getHospitalId(), node.Id+1);
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Ажлын газар устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    treeListProvince.DeleteNode(treeListProvince.FocusedNode);
                }
            }

            private void saveCountry()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                InformationDataSet.ProvinceBindingSource.EndEdit();
                InformationDataSet.ProvinceTableAdapter.Update(InformationDataSet.Instance.Province);
                ProgramUtil.closeWaitDialog();
            }

            private bool isValidateTreeNodeEmptyText()
            {
                if (treeListProvince.FocusedNode != null && treeListProvince.FocusedNode.GetValue("name").ToString().Trim().Equals(""))
                {
                    treeListProvince.FocusedNode.SetValue("Name", "");
                    return false;
                }
                return true;
            }

            private void expandTree()
            {
                if (isExpandTree)
                {
                    treeListProvince.CollapseAll();
                    controlNavigator.Buttons.CustomButtons[0].ImageIndex = 17;
                }
                else
                {
                    treeListProvince.ExpandAll();
                    controlNavigator.Buttons.CustomButtons[0].ImageIndex = 14;
                }
                isExpandTree = !isExpandTree;
            }

        #endregion
        
        #region Мэргэжил тушаал нэмэх, засах, устгах функц

            private void addSpecialty()
            {
                DataRowView currSpecView = (DataRowView)InformationDataSet.SpecialtyBindingSource.Current;
                if (currSpecView.IsNew)
                {
                    InformationDataSet.SpecialtyBindingSource.CancelEdit();
                    InformationDataSet.Instance.Specialty.RejectChanges();
                }
                InformationDataSet.Instance.Specialty.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                DataRowView view = (DataRowView)InformationDataSet.SpecialtyBindingSource.AddNew();
                //view["id"] = InformationDataSet.QueriesTableAdapter.get_new_id();
                gridViewSpecialty.ShowEditForm();
            }

            private void deleteSpecialty()
            {
                object ret = InformationDataSet.QueriesTableAdapter.can_delete_specialty(HospitalUtil.getHospitalId(), gridViewSpecialty.GetFocusedDataRow()["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Мэргэжил устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    gridViewSpecialty.DeleteSelectedRows();
                    saveSpecialty();
                }
            }

            private void saveSpecialty()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                InformationDataSet.SpecialtyBindingSource.EndEdit();
                InformationDataSet.SpecialtyTableAdapter.Update(InformationDataSet.Instance.Specialty);
                ProgramUtil.closeWaitDialog();
            }

        #endregion

    }
}