﻿namespace Pharmacy2016.gui.core.info.user
{
    partial class UserInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserInUpForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textEditFirstName = new DevExpress.XtraEditors.TextEdit();
            this.textEditEMM = new DevExpress.XtraEditors.TextEdit();
            this.textEditRegister = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.textEditLastName = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit = new DevExpress.XtraEditors.PictureEdit();
            this.textEditSirName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditBirthday = new DevExpress.XtraEditors.DateEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageRole = new DevExpress.XtraTab.XtraTabPage();
            this.textEditUserName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlPasswordRepeat = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupIsUse = new DevExpress.XtraEditors.RadioGroup();
            this.simpleButtonCheck = new DevExpress.XtraEditors.SimpleButton();
            this.textEditPasswordRepeat = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.treeListLookUpEditWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridLookUpEditUserRole = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlPassword = new DevExpress.XtraEditors.LabelControl();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPageUser = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditSpecialty = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEditNDD = new DevExpress.XtraEditors.TextEdit();
            this.gridLookUpEditPosition = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditOrganization = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditEducation = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageAddress = new DevExpress.XtraTab.XtraTabPage();
            this.textEditEmail = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditAddress = new DevExpress.XtraEditors.MemoEdit();
            this.textEditPhone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.treeListLookUpEditCountry = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupGender = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsMarried = new DevExpress.XtraEditors.RadioGroup();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.textEditID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEMM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegister.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSirName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageRole.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsUse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPasswordRepeat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUserRole.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            this.xtraTabPageUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSpecialty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNDD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditOrganization.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditEducation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            this.xtraTabPageAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsMarried.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(169, 126);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(56, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Регистер*:";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(143, 18);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(69, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "ЭМД дугаар :";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(197, 100);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(28, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Нэр*:";
            // 
            // textEditFirstName
            // 
            this.textEditFirstName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditFirstName.EnterMoveNextControl = true;
            this.textEditFirstName.Location = new System.Drawing.Point(231, 97);
            this.textEditFirstName.Name = "textEditFirstName";
            this.textEditFirstName.Properties.MaxLength = 50;
            this.textEditFirstName.Size = new System.Drawing.Size(200, 20);
            this.textEditFirstName.TabIndex = 4;
            // 
            // textEditEMM
            // 
            this.textEditEMM.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditEMM.EnterMoveNextControl = true;
            this.textEditEMM.Location = new System.Drawing.Point(218, 15);
            this.textEditEMM.Name = "textEditEMM";
            this.textEditEMM.Properties.MaxLength = 10;
            this.textEditEMM.Size = new System.Drawing.Size(200, 20);
            this.textEditEMM.TabIndex = 21;
            // 
            // textEditRegister
            // 
            this.textEditRegister.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditRegister.EnterMoveNextControl = true;
            this.textEditRegister.Location = new System.Drawing.Point(231, 123);
            this.textEditRegister.Name = "textEditRegister";
            this.textEditRegister.Properties.MaxLength = 10;
            this.textEditRegister.Size = new System.Drawing.Size(200, 20);
            this.textEditRegister.TabIndex = 5;
            this.textEditRegister.EditValueChanged += new System.EventHandler(this.textEditRegister_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(148, 152);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(71, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Төрсөн огноо:";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl6.Location = new System.Drawing.Point(193, 180);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(32, 13);
            this.labelControl6.TabIndex = 8;
            this.labelControl6.Text = "Хүйс*:";
            // 
            // textEditLastName
            // 
            this.textEditLastName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditLastName.EnterMoveNextControl = true;
            this.textEditLastName.Location = new System.Drawing.Point(231, 71);
            this.textEditLastName.Name = "textEditLastName";
            this.textEditLastName.Properties.MaxLength = 50;
            this.textEditLastName.Size = new System.Drawing.Size(200, 20);
            this.textEditLastName.TabIndex = 3;
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(497, 526);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 52;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(416, 526);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 51;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl7.Location = new System.Drawing.Point(144, 44);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(69, 13);
            this.labelControl7.TabIndex = 14;
            this.labelControl7.Text = "НДД дугаар :";
            // 
            // pictureEdit
            // 
            this.pictureEdit.Location = new System.Drawing.Point(12, 12);
            this.pictureEdit.Name = "pictureEdit";
            this.pictureEdit.Properties.NullText = "Зураггүй";
            this.pictureEdit.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit.Size = new System.Drawing.Size(100, 125);
            this.pictureEdit.TabIndex = 54;
            this.pictureEdit.EditValueChanged += new System.EventHandler(this.pictureEdit_EditValueChanged);
            // 
            // textEditSirName
            // 
            this.textEditSirName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditSirName.EnterMoveNextControl = true;
            this.textEditSirName.Location = new System.Drawing.Point(231, 45);
            this.textEditSirName.Name = "textEditSirName";
            this.textEditSirName.Properties.MaxLength = 50;
            this.textEditSirName.Size = new System.Drawing.Size(200, 20);
            this.textEditSirName.TabIndex = 2;
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl9.Location = new System.Drawing.Point(153, 48);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(66, 13);
            this.labelControl9.TabIndex = 21;
            this.labelControl9.Text = "Ургийн овог:";
            // 
            // dateEditBirthday
            // 
            this.dateEditBirthday.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditBirthday.EditValue = null;
            this.dateEditBirthday.Location = new System.Drawing.Point(231, 149);
            this.dateEditBirthday.Name = "dateEditBirthday";
            this.dateEditBirthday.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBirthday.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBirthday.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBirthday.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBirthday.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBirthday.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBirthday.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditBirthday.Size = new System.Drawing.Size(100, 20);
            this.dateEditBirthday.TabIndex = 6;
            this.dateEditBirthday.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditBirthday_KeyUp);
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl10.Location = new System.Drawing.Point(148, 209);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(78, 13);
            this.labelControl10.TabIndex = 7;
            this.labelControl10.Text = "Гэрлэсэн эсэх*:";
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl.Location = new System.Drawing.Point(12, 233);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageRole;
            this.xtraTabControl.Size = new System.Drawing.Size(560, 287);
            this.xtraTabControl.TabIndex = 9;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageRole,
            this.xtraTabPageUser,
            this.xtraTabPageAddress});
            this.xtraTabControl.KeyUp += new System.Windows.Forms.KeyEventHandler(this.xtraTabControl_KeyUp);
            // 
            // xtraTabPageRole
            // 
            this.xtraTabPageRole.Controls.Add(this.textEditUserName);
            this.xtraTabPageRole.Controls.Add(this.labelControl22);
            this.xtraTabPageRole.Controls.Add(this.labelControlPasswordRepeat);
            this.xtraTabPageRole.Controls.Add(this.radioGroupIsUse);
            this.xtraTabPageRole.Controls.Add(this.simpleButtonCheck);
            this.xtraTabPageRole.Controls.Add(this.textEditPasswordRepeat);
            this.xtraTabPageRole.Controls.Add(this.labelControl21);
            this.xtraTabPageRole.Controls.Add(this.labelControl20);
            this.xtraTabPageRole.Controls.Add(this.treeListLookUpEditWard);
            this.xtraTabPageRole.Controls.Add(this.gridLookUpEditUserRole);
            this.xtraTabPageRole.Controls.Add(this.labelControl19);
            this.xtraTabPageRole.Controls.Add(this.labelControlPassword);
            this.xtraTabPageRole.Controls.Add(this.textEditPassword);
            this.xtraTabPageRole.Name = "xtraTabPageRole";
            this.xtraTabPageRole.Size = new System.Drawing.Size(554, 259);
            this.xtraTabPageRole.Text = "Хэрэглэгчийн эрх";
            // 
            // textEditUserName
            // 
            this.textEditUserName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditUserName.EnterMoveNextControl = true;
            this.textEditUserName.Location = new System.Drawing.Point(218, 96);
            this.textEditUserName.Name = "textEditUserName";
            this.textEditUserName.Properties.MaxLength = 30;
            this.textEditUserName.Size = new System.Drawing.Size(200, 20);
            this.textEditUserName.TabIndex = 14;
            // 
            // labelControl22
            // 
            this.labelControl22.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl22.Location = new System.Drawing.Point(141, 99);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(71, 13);
            this.labelControl22.TabIndex = 64;
            this.labelControl22.Text = "Нэвтрэх нэр*:";
            // 
            // labelControlPasswordRepeat
            // 
            this.labelControlPasswordRepeat.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlPasswordRepeat.Location = new System.Drawing.Point(124, 151);
            this.labelControlPasswordRepeat.Name = "labelControlPasswordRepeat";
            this.labelControlPasswordRepeat.Size = new System.Drawing.Size(88, 13);
            this.labelControlPasswordRepeat.TabIndex = 38;
            this.labelControlPasswordRepeat.Text = "Нууц үг давтах*:";
            // 
            // radioGroupIsUse
            // 
            this.radioGroupIsUse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupIsUse.EnterMoveNextControl = true;
            this.radioGroupIsUse.Location = new System.Drawing.Point(218, 67);
            this.radioGroupIsUse.Name = "radioGroupIsUse";
            this.radioGroupIsUse.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм", true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй", true, "Үгүй")});
            this.radioGroupIsUse.Size = new System.Drawing.Size(100, 23);
            this.radioGroupIsUse.TabIndex = 13;
            // 
            // simpleButtonCheck
            // 
            this.simpleButtonCheck.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonCheck.Image")));
            this.simpleButtonCheck.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonCheck.Location = new System.Drawing.Point(424, 94);
            this.simpleButtonCheck.Name = "simpleButtonCheck";
            this.simpleButtonCheck.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonCheck.TabIndex = 56;
            this.simpleButtonCheck.Text = "Хэрэглэгчийн регистрийн дугаар давхардсан эсэхийг шалгах";
            this.simpleButtonCheck.Click += new System.EventHandler(this.simpleButtonCheck_Click);
            // 
            // textEditPasswordRepeat
            // 
            this.textEditPasswordRepeat.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditPasswordRepeat.EnterMoveNextControl = true;
            this.textEditPasswordRepeat.Location = new System.Drawing.Point(218, 148);
            this.textEditPasswordRepeat.Name = "textEditPasswordRepeat";
            this.textEditPasswordRepeat.Properties.MaxLength = 32;
            this.textEditPasswordRepeat.Properties.PasswordChar = '*';
            this.textEditPasswordRepeat.Size = new System.Drawing.Size(200, 20);
            this.textEditPasswordRepeat.TabIndex = 16;
            this.textEditPasswordRepeat.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textEditPasswordRepeat_KeyUp);
            // 
            // labelControl21
            // 
            this.labelControl21.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl21.Location = new System.Drawing.Point(124, 72);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(88, 13);
            this.labelControl21.TabIndex = 33;
            this.labelControl21.Text = "Идэвхитэй эсэх*:";
            // 
            // labelControl20
            // 
            this.labelControl20.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl20.Location = new System.Drawing.Point(174, 44);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(38, 13);
            this.labelControl20.TabIndex = 36;
            this.labelControl20.Text = "Тасаг*:";
            // 
            // treeListLookUpEditWard
            // 
            this.treeListLookUpEditWard.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWard.Location = new System.Drawing.Point(218, 41);
            this.treeListLookUpEditWard.Name = "treeListLookUpEditWard";
            this.treeListLookUpEditWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditWard.Properties.NullText = "";
            this.treeListLookUpEditWard.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditWard.Properties.ValueMember = "id";
            this.treeListLookUpEditWard.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWard.TabIndex = 12;
            this.treeListLookUpEditWard.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWard_KeyUp);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(68, 38);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Нэр";
            this.treeListColumn2.FieldName = "name";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // gridLookUpEditUserRole
            // 
            this.gridLookUpEditUserRole.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditUserRole.EditValue = "na";
            this.gridLookUpEditUserRole.Location = new System.Drawing.Point(218, 15);
            this.gridLookUpEditUserRole.Name = "gridLookUpEditUserRole";
            this.gridLookUpEditUserRole.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUserRole.Properties.DisplayMember = "name";
            this.gridLookUpEditUserRole.Properties.NullText = " ";
            this.gridLookUpEditUserRole.Properties.ValueMember = "id";
            this.gridLookUpEditUserRole.Properties.View = this.gridView3;
            this.gridLookUpEditUserRole.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditUserRole.TabIndex = 11;
            this.gridLookUpEditUserRole.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditUserRole_KeyUp);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.ReadOnly = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Нэр";
            this.gridColumn4.CustomizationCaption = "Нэр";
            this.gridColumn4.FieldName = "name";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ShowCaption = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // labelControl19
            // 
            this.labelControl19.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl19.Location = new System.Drawing.Point(116, 18);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(97, 13);
            this.labelControl19.TabIndex = 33;
            this.labelControl19.Text = "Хэрэглэгчийн эрх*:";
            // 
            // labelControlPassword
            // 
            this.labelControlPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlPassword.Location = new System.Drawing.Point(164, 125);
            this.labelControlPassword.Name = "labelControlPassword";
            this.labelControlPassword.Size = new System.Drawing.Size(48, 13);
            this.labelControlPassword.TabIndex = 32;
            this.labelControlPassword.Text = "Нууц үг*:";
            // 
            // textEditPassword
            // 
            this.textEditPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditPassword.EnterMoveNextControl = true;
            this.textEditPassword.Location = new System.Drawing.Point(218, 122);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Properties.MaxLength = 32;
            this.textEditPassword.Properties.PasswordChar = '*';
            this.textEditPassword.Size = new System.Drawing.Size(200, 20);
            this.textEditPassword.TabIndex = 15;
            // 
            // xtraTabPageUser
            // 
            this.xtraTabPageUser.Controls.Add(this.labelControl17);
            this.xtraTabPageUser.Controls.Add(this.gridLookUpEditSpecialty);
            this.xtraTabPageUser.Controls.Add(this.textEditNDD);
            this.xtraTabPageUser.Controls.Add(this.gridLookUpEditPosition);
            this.xtraTabPageUser.Controls.Add(this.gridLookUpEditOrganization);
            this.xtraTabPageUser.Controls.Add(this.gridLookUpEditEducation);
            this.xtraTabPageUser.Controls.Add(this.labelControl13);
            this.xtraTabPageUser.Controls.Add(this.labelControl8);
            this.xtraTabPageUser.Controls.Add(this.labelControl12);
            this.xtraTabPageUser.Controls.Add(this.textEditEMM);
            this.xtraTabPageUser.Controls.Add(this.labelControl2);
            this.xtraTabPageUser.Controls.Add(this.labelControl7);
            this.xtraTabPageUser.Name = "xtraTabPageUser";
            this.xtraTabPageUser.Size = new System.Drawing.Size(554, 259);
            this.xtraTabPageUser.Text = "Даатгалын дугаар";
            // 
            // labelControl17
            // 
            this.labelControl17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl17.Location = new System.Drawing.Point(156, 96);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(56, 13);
            this.labelControl17.TabIndex = 25;
            this.labelControl17.Text = "Мэргэжил :";
            // 
            // gridLookUpEditSpecialty
            // 
            this.gridLookUpEditSpecialty.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditSpecialty.Location = new System.Drawing.Point(218, 93);
            this.gridLookUpEditSpecialty.Name = "gridLookUpEditSpecialty";
            this.gridLookUpEditSpecialty.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditSpecialty.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSpecialty.Properties.DisplayMember = "name";
            this.gridLookUpEditSpecialty.Properties.NullText = " ";
            this.gridLookUpEditSpecialty.Properties.ValueMember = "id";
            this.gridLookUpEditSpecialty.Properties.View = this.gridView4;
            this.gridLookUpEditSpecialty.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditSpecialty.TabIndex = 24;
            this.gridLookUpEditSpecialty.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditSpecialty_ButtonClick);
            this.gridLookUpEditSpecialty.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditSpecialty_KeyUp);
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn8});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.ReadOnly = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Нэр";
            this.gridColumn5.CustomizationCaption = "Нэр";
            this.gridColumn5.FieldName = "name";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ShowCaption = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Тайлбар";
            this.gridColumn8.CustomizationCaption = "Тайлбар";
            this.gridColumn8.FieldName = "description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 1;
            // 
            // textEditNDD
            // 
            this.textEditNDD.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditNDD.EnterMoveNextControl = true;
            this.textEditNDD.Location = new System.Drawing.Point(218, 41);
            this.textEditNDD.Name = "textEditNDD";
            this.textEditNDD.Properties.MaxLength = 10;
            this.textEditNDD.Size = new System.Drawing.Size(200, 20);
            this.textEditNDD.TabIndex = 22;
            // 
            // gridLookUpEditPosition
            // 
            this.gridLookUpEditPosition.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditPosition.Location = new System.Drawing.Point(218, 145);
            this.gridLookUpEditPosition.Name = "gridLookUpEditPosition";
            this.gridLookUpEditPosition.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditPosition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPosition.Properties.DisplayMember = "name";
            this.gridLookUpEditPosition.Properties.NullText = " ";
            this.gridLookUpEditPosition.Properties.ValueMember = "id";
            this.gridLookUpEditPosition.Properties.View = this.gridView2;
            this.gridLookUpEditPosition.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditPosition.TabIndex = 26;
            this.gridLookUpEditPosition.Visible = false;
            this.gridLookUpEditPosition.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditPosition_ButtonClick);
            this.gridLookUpEditPosition.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditPosition_KeyUp);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn7});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.ReadOnly = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Нэр";
            this.gridColumn3.CustomizationCaption = "Нэр";
            this.gridColumn3.FieldName = "name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ShowCaption = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Тайлбар";
            this.gridColumn7.CustomizationCaption = "Тайлбар";
            this.gridColumn7.FieldName = "description";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            // 
            // gridLookUpEditOrganization
            // 
            this.gridLookUpEditOrganization.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditOrganization.Location = new System.Drawing.Point(218, 119);
            this.gridLookUpEditOrganization.Name = "gridLookUpEditOrganization";
            this.gridLookUpEditOrganization.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditOrganization.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditOrganization.Properties.DisplayMember = "name";
            this.gridLookUpEditOrganization.Properties.NullText = " ";
            this.gridLookUpEditOrganization.Properties.ValueMember = "id";
            this.gridLookUpEditOrganization.Properties.View = this.gridView1;
            this.gridLookUpEditOrganization.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditOrganization.TabIndex = 25;
            this.gridLookUpEditOrganization.Visible = false;
            this.gridLookUpEditOrganization.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditOrganization_ButtonClick);
            this.gridLookUpEditOrganization.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditOrganization_KeyUp);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn6});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Нэр";
            this.gridColumn2.CustomizationCaption = "Нэр";
            this.gridColumn2.FieldName = "name";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тайлбар";
            this.gridColumn6.CustomizationCaption = "Тайлбар";
            this.gridColumn6.FieldName = "description";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            // 
            // gridLookUpEditEducation
            // 
            this.gridLookUpEditEducation.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditEducation.Location = new System.Drawing.Point(218, 67);
            this.gridLookUpEditEducation.Name = "gridLookUpEditEducation";
            this.gridLookUpEditEducation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditEducation.Properties.DisplayMember = "name";
            this.gridLookUpEditEducation.Properties.NullText = " ";
            this.gridLookUpEditEducation.Properties.ValueMember = "id";
            this.gridLookUpEditEducation.Properties.View = this.gridLookUpEdit1View;
            this.gridLookUpEditEducation.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditEducation.TabIndex = 23;
            this.gridLookUpEditEducation.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditEducation_KeyUp);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.ReadOnly = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Нэр";
            this.gridColumn1.FieldName = "name";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl13.Location = new System.Drawing.Point(133, 148);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(79, 13);
            this.labelControl13.TabIndex = 23;
            this.labelControl13.Text = "Албан тушаал :";
            this.labelControl13.Visible = false;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(139, 122);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(73, 13);
            this.labelControl8.TabIndex = 22;
            this.labelControl8.Text = "Ажлын газар :";
            this.labelControl8.Visible = false;
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl12.Location = new System.Drawing.Point(152, 70);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(60, 13);
            this.labelControl12.TabIndex = 21;
            this.labelControl12.Text = "Боловсрол :";
            // 
            // xtraTabPageAddress
            // 
            this.xtraTabPageAddress.Controls.Add(this.textEditEmail);
            this.xtraTabPageAddress.Controls.Add(this.labelControl16);
            this.xtraTabPageAddress.Controls.Add(this.memoEditAddress);
            this.xtraTabPageAddress.Controls.Add(this.textEditPhone);
            this.xtraTabPageAddress.Controls.Add(this.labelControl14);
            this.xtraTabPageAddress.Controls.Add(this.labelControl15);
            this.xtraTabPageAddress.Controls.Add(this.labelControl4);
            this.xtraTabPageAddress.Controls.Add(this.treeListLookUpEditCountry);
            this.xtraTabPageAddress.Name = "xtraTabPageAddress";
            this.xtraTabPageAddress.Size = new System.Drawing.Size(554, 259);
            this.xtraTabPageAddress.Text = "Холбоо барих";
            // 
            // textEditEmail
            // 
            this.textEditEmail.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditEmail.EnterMoveNextControl = true;
            this.textEditEmail.Location = new System.Drawing.Point(218, 41);
            this.textEditEmail.Name = "textEditEmail";
            this.textEditEmail.Properties.MaxLength = 50;
            this.textEditEmail.Size = new System.Drawing.Size(200, 20);
            this.textEditEmail.TabIndex = 32;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(144, 95);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(68, 13);
            this.labelControl16.TabIndex = 32;
            this.labelControl16.Text = "Гэрийн хаяг :";
            // 
            // memoEditAddress
            // 
            this.memoEditAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditAddress.EnterMoveNextControl = true;
            this.memoEditAddress.Location = new System.Drawing.Point(218, 93);
            this.memoEditAddress.Name = "memoEditAddress";
            this.memoEditAddress.Properties.MaxLength = 200;
            this.memoEditAddress.Size = new System.Drawing.Size(320, 153);
            this.memoEditAddress.TabIndex = 34;
            // 
            // textEditPhone
            // 
            this.textEditPhone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditPhone.EnterMoveNextControl = true;
            this.textEditPhone.Location = new System.Drawing.Point(218, 15);
            this.textEditPhone.Name = "textEditPhone";
            this.textEditPhone.Properties.MaxLength = 50;
            this.textEditPhone.Size = new System.Drawing.Size(200, 20);
            this.textEditPhone.TabIndex = 31;
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl14.Location = new System.Drawing.Point(129, 18);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(84, 13);
            this.labelControl14.TabIndex = 27;
            this.labelControl14.Text = "Утасны дугаар :";
            // 
            // labelControl15
            // 
            this.labelControl15.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl15.Location = new System.Drawing.Point(145, 44);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(67, 13);
            this.labelControl15.TabIndex = 29;
            this.labelControl15.Text = "И-мэйл хаяг :";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(161, 70);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(51, 13);
            this.labelControl4.TabIndex = 26;
            this.labelControl4.Text = "Байршил :";
            // 
            // treeListLookUpEditCountry
            // 
            this.treeListLookUpEditCountry.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditCountry.Location = new System.Drawing.Point(218, 67);
            this.treeListLookUpEditCountry.Name = "treeListLookUpEditCountry";
            this.treeListLookUpEditCountry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditCountry.Properties.DisplayMember = "name";
            this.treeListLookUpEditCountry.Properties.NullText = "";
            this.treeListLookUpEditCountry.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEditCountry.Properties.ValueMember = "id";
            this.treeListLookUpEditCountry.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditCountry.TabIndex = 33;
            this.treeListLookUpEditCountry.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditCountry_KeyUp);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.treeListLookUpEdit1TreeList.KeyFieldName = "id";
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.ParentFieldName = "pid";
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Нэр";
            this.treeListColumn1.FieldName = "name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl11.Location = new System.Drawing.Point(141, 74);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(85, 13);
            this.labelControl11.TabIndex = 28;
            this.labelControl11.Text = "Эцэг эхийн нэр*:";
            // 
            // radioGroupGender
            // 
            this.radioGroupGender.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupGender.EnterMoveNextControl = true;
            this.radioGroupGender.Location = new System.Drawing.Point(231, 175);
            this.radioGroupGender.Name = "radioGroupGender";
            this.radioGroupGender.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Эр", true, "Эр"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эм", true, "Эм")});
            this.radioGroupGender.Size = new System.Drawing.Size(100, 23);
            this.radioGroupGender.TabIndex = 7;
            // 
            // radioGroupIsMarried
            // 
            this.radioGroupIsMarried.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupIsMarried.EnterMoveNextControl = true;
            this.radioGroupIsMarried.Location = new System.Drawing.Point(231, 204);
            this.radioGroupIsMarried.Name = "radioGroupIsMarried";
            this.radioGroupIsMarried.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм", true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй", true, "Үгүй")});
            this.radioGroupIsMarried.Size = new System.Drawing.Size(100, 23);
            this.radioGroupIsMarried.TabIndex = 8;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 526);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 53;
            this.simpleButtonReload.Text = "Сонгох талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // textEditID
            // 
            this.textEditID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditID.EnterMoveNextControl = true;
            this.textEditID.Location = new System.Drawing.Point(231, 19);
            this.textEditID.Name = "textEditID";
            this.textEditID.Properties.MaxLength = 50;
            this.textEditID.Properties.ReadOnly = true;
            this.textEditID.Size = new System.Drawing.Size(200, 20);
            this.textEditID.TabIndex = 1;
            // 
            // labelControl18
            // 
            this.labelControl18.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl18.Location = new System.Drawing.Point(178, 22);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(47, 13);
            this.labelControl18.TabIndex = 62;
            this.labelControl18.Text = "Дугаар*:";
            // 
            // UserInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.textEditID);
            this.Controls.Add(this.labelControl18);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.radioGroupIsMarried);
            this.Controls.Add(this.radioGroupGender);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.xtraTabControl);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.dateEditBirthday);
            this.Controls.Add(this.textEditSirName);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.pictureEdit);
            this.Controls.Add(this.simpleButtonSave);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.textEditLastName);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.textEditRegister);
            this.Controls.Add(this.textEditFirstName);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Хэрэглэгч нэмэх";
            this.Shown += new System.EventHandler(this.UserInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEMM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegister.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSirName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageRole.ResumeLayout(false);
            this.xtraTabPageRole.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsUse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPasswordRepeat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUserRole.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            this.xtraTabPageUser.ResumeLayout(false);
            this.xtraTabPageUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSpecialty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNDD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditOrganization.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditEducation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            this.xtraTabPageAddress.ResumeLayout(false);
            this.xtraTabPageAddress.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsMarried.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit textEditFirstName;
        private DevExpress.XtraEditors.TextEdit textEditEMM;
        private DevExpress.XtraEditors.TextEdit textEditRegister;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit textEditLastName;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PictureEdit pictureEdit;
        private DevExpress.XtraEditors.TextEdit textEditSirName;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.DateEdit dateEditBirthday;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageUser;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAddress;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.MemoEdit memoEditAddress;
        private DevExpress.XtraEditors.TextEdit textEditPhone;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditCountry;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPosition;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditOrganization;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditEducation;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraEditors.TextEdit textEditNDD;
        private DevExpress.XtraEditors.TextEdit textEditEmail;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageRole;
        private DevExpress.XtraEditors.LabelControl labelControlPassword;
        private DevExpress.XtraEditors.TextEdit textEditPassword;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWard;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUserRole;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControlPasswordRepeat;
        private DevExpress.XtraEditors.TextEdit textEditPasswordRepeat;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSpecialty;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsUse;
        private DevExpress.XtraEditors.RadioGroup radioGroupGender;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsMarried;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.TextEdit textEditUserName;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCheck;
        private DevExpress.XtraEditors.TextEdit textEditID;
        private DevExpress.XtraEditors.LabelControl labelControl18;
    }
}