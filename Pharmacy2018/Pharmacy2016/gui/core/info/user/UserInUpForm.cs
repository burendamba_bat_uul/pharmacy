﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Pharmacy2016.util;
using System.Security.Cryptography;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.info.user
{
    /*
     * Хэрэглэгч нэмэх, засах цонх.
     * 
     */

    public partial class UserInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц
            
            private static UserInUpForm INSTANCE = new UserInUpForm();

            public static UserInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private bool isDownload = false;

            private UserInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                //Гэрлэсэн эсэх сонгох хэсгийг харуулахгүй болгов.
                radioGroupIsMarried.Visible = false;
                labelControl10.Visible = false;

                initError();
                initBinding();
                initDataSource();
                reload();
            }

            private void initError()
            {
                textEditFirstName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditLastName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditSirName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditRegister.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditUserName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditPassword.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditPasswordRepeat.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditBirthday.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditEducation.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditOrganization.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditPosition.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditUserRole.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditCountry.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                pictureEdit.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

            private void initDataSource()
            {
                gridLookUpEditOrganization.Properties.DataSource = InformationDataSet.OrganizationOtherBindingSource;
                gridLookUpEditPosition.Properties.DataSource = InformationDataSet.PositionOtherBindingSource;
                gridLookUpEditSpecialty.Properties.DataSource = InformationDataSet.SpecialtyOtherBindingSource;
                treeListLookUpEditCountry.Properties.DataSource = InformationDataSet.ProvinceOtherBindingSource;
                treeListLookUpEditWard.Properties.DataSource = UserDataSet.WardBindingSource;
                gridLookUpEditEducation.Properties.DataSource = InformationDataSet.Instance.Education;
                gridLookUpEditUserRole.Properties.DataSource = InformationDataSet.Instance.Role;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    
                    if (actionType == 1)
                    {
                        insertUser();
                    }
                    else
                    {
                        updateUser();
                    }

                    ShowDialog(UserForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void initBinding()
            {
                textEditID.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "id", true));
                textEditFirstName.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "firstName", true));
                textEditLastName.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "lastName", true));
                textEditSirName.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "sirName", true));
                textEditRegister.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "register", true));
                textEditEMM.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "emdNumber", true));
                textEditNDD.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "nddNumber", true));
                textEditPhone.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "phone", true));
                textEditEmail.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "email", true));
                textEditUserName.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "userName", true));
                //textEditPassword.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "password", true));
                //textEditPasswordRepeat.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "password", true));
                memoEditAddress.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "address", true));
                dateEditBirthday.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "birthday", true));
                gridLookUpEditEducation.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "education", true));
                //gridLookUpEditOrganization.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "organizationID", true));
                //gridLookUpEditPosition.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "positionID", true));
                gridLookUpEditSpecialty.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "specialtyID", true));
                gridLookUpEditUserRole.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "roleID", true));
                treeListLookUpEditCountry.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "provinceID", true));
                treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "wardID", true));
                radioGroupGender.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "gender", true));
                radioGroupIsMarried.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "isMarried", true));
                radioGroupIsUse.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "isUse", true));
                pictureEdit.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "picture", true));
            }

            private void insertUser()
            {
                textEditUserName.Properties.ReadOnly = false;
                textEditPassword.Visible = true;
                textEditPasswordRepeat.Visible = true;
                labelControlPassword.Visible = true;
                labelControlPasswordRepeat.Visible = true;
            }

            private void updateUser()
            {
                //textEditUserName.Properties.ReadOnly = true;
                textEditPassword.Visible = false;
                textEditPasswordRepeat.Visible = false;
                labelControlPassword.Visible = false;
                labelControlPasswordRepeat.Visible = false;
            }
            

            private void clearData()
            {
                clearErrorText();
                //clearBinding();

                textEditPassword.Text = "";
                textEditPasswordRepeat.Text = "";
                isDownload = false;
                
                InformationDataSet.UserBindingSource.CancelEdit();
                InformationDataSet.Instance.User.RejectChanges();
                InformationDataSet.Instance.UserInfo.Clear();
            }

            private void clearBinding()
            {
                textEditID.DataBindings.Clear();
                textEditFirstName.DataBindings.Clear();
                textEditLastName.DataBindings.Clear();
                textEditSirName.DataBindings.Clear();
                textEditRegister.DataBindings.Clear();
                textEditEMM.DataBindings.Clear();
                textEditNDD.DataBindings.Clear();
                textEditPhone.DataBindings.Clear();
                textEditEmail.DataBindings.Clear();
                textEditUserName.DataBindings.Clear();
                //textEditPassword.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "password", true));
                //textEditPasswordRepeat.DataBindings.Add(new Binding("EditValue", InformationDataSet.UserBindingSource, "password", true));
                memoEditAddress.DataBindings.Clear();
                dateEditBirthday.DataBindings.Clear();
                gridLookUpEditEducation.DataBindings.Clear();
                gridLookUpEditOrganization.DataBindings.Clear();
                gridLookUpEditPosition.DataBindings.Clear();
                gridLookUpEditSpecialty.DataBindings.Clear();
                gridLookUpEditUserRole.DataBindings.Clear();
                treeListLookUpEditCountry.DataBindings.Clear();
                treeListLookUpEditWard.DataBindings.Clear();
                radioGroupGender.DataBindings.Clear();
                radioGroupIsMarried.DataBindings.Clear();
                radioGroupIsUse.DataBindings.Clear();
                pictureEdit.DataBindings.Clear();
            }

            private void clearErrorText()
            {
                textEditFirstName.ErrorText = "";
                textEditLastName.ErrorText = "";
                textEditSirName.ErrorText = "";
                textEditRegister.ErrorText = "";
                textEditUserName.ErrorText = "";
                textEditPassword.ErrorText = "";
                textEditPasswordRepeat.ErrorText = "";
                dateEditBirthday.ErrorText = "";
                gridLookUpEditEducation.ErrorText = "";
                gridLookUpEditOrganization.ErrorText = "";
                gridLookUpEditPosition.ErrorText = "";
                gridLookUpEditUserRole.ErrorText = "";
                treeListLookUpEditCountry.ErrorText = "";
                treeListLookUpEditWard.ErrorText = "";
            }

            private void UserInUpForm_Shown(object sender, EventArgs e)
            {
                textEditSirName.Focus();
                xtraTabControl.SelectedTabPage = xtraTabPageAddress;
                xtraTabControl.SelectedTabPage = xtraTabPageUser;
                xtraTabControl.SelectedTabPage = xtraTabPageRole;
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }

        #endregion

        #region Формын event

            private void dateEditBirthday_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditBirthday.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        radioGroupGender.Focus();
                    }
                }
            }

            private void xtraTabControl_KeyUp(object sender, KeyEventArgs e)
            {
                if ((e.KeyData == Keys.Enter))
                {
                    SelectNextControl(ActiveControl, true, true, true, true);
                }
            }

            private void gridLookUpEditUserRole_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditUserRole.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        treeListLookUpEditWard.Focus();
                    }
                }
            }

            private void treeListLookUpEditWard_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditWard.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        radioGroupIsUse.Focus();
                    }
                }
            }

            private void gridLookUpEditEducation_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditEducation.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditSpecialty.Focus();
                    }
                }
            }

            private void gridLookUpEditSpecialty_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditSpecialty.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditOrganization.Focus();
                    }
                }
            }

            private void gridLookUpEditOrganization_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditOrganization.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditPosition.Focus();
                    }
                }
            }

            private void gridLookUpEditPosition_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditPosition.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        xtraTabControl.SelectedTabPage = xtraTabPageAddress;
                        textEditPhone.Focus();
                    }
                }
            }

            private void treeListLookUpEditCountry_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditCountry.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        memoEditAddress.Focus();
                    }
                }
            }

            private void textEditPasswordRepeat_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (textEditPasswordRepeat.Text != "")
                    {
                        xtraTabControl.SelectedTabPage = xtraTabPageUser;
                        textEditEMM.Focus();
                    }
                    else
                    {
                        textEditPasswordRepeat.Focus();
                    }
                    
                }
            }

            private void pictureEdit_EditValueChanged(object sender, EventArgs e)
            {
                if (pictureEdit.EditValue == DBNull.Value || pictureEdit.EditValue == null)
                {
                    return;
                }

                Image tmpImage = pictureEdit.Image;

                if (tmpImage.Width <= 300 && tmpImage.Height <= 400)
                {
                    return;
                }

                //Зургын хэмжээг багасгаж байна.
                if (tmpImage.Width / 3 > tmpImage.Height / 4)
                {
                    tmpImage = ProgramUtil.resizeImage(tmpImage, new Size(10 * Convert.ToInt32(Math.Round(40d * tmpImage.Width / tmpImage.Height)), 400));
                }
                else
                {
                    tmpImage = ProgramUtil.resizeImage(tmpImage, new Size(300, 10 * Convert.ToInt32(Math.Round(30d * tmpImage.Height / tmpImage.Width))));
                }

                tmpImage = ProgramUtil.cropImage(tmpImage, new Rectangle(new Point(tmpImage.Width / 2 - 150, tmpImage.Height / 2 - 200), new Size(300, 400)));

                UserForm.Instance.currView["picture"] = ProgramUtil.imageToByteArray(tmpImage, System.Drawing.Imaging.ImageFormat.Jpeg);
                UserForm.Instance.currView.EndEdit();
            }

            private void textEditRegister_EditValueChanged(object sender, EventArgs e)
            {
                if (isDownload)
                    isDownload = false;
            }

            private void gridLookUpEditOrganization_ButtonClick(object sender, ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    UserConfInUpForm.Instance.showForm(1, 1, Instance);
                }
            }

            private void gridLookUpEditPosition_ButtonClick(object sender, ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    UserConfInUpForm.Instance.showForm(2, 1, Instance);
                }
            }

            private void gridLookUpEditSpecialty_ButtonClick(object sender, ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    UserConfInUpForm.Instance.showForm(3, 1, Instance);
                }
            }


            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                saveUser();
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonCheck_Click(object sender, EventArgs e)
            {
                checkRegister();
            }

        #endregion

        #region Формын функц

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.ProvinceTableAdapter.Fill(InformationDataSet.Instance.Province);
                InformationDataSet.OrganizationTableAdapter.Fill(InformationDataSet.Instance.Organization, HospitalUtil.getHospitalId());
                InformationDataSet.PositionTableAdapter.Fill(InformationDataSet.Instance.Position, HospitalUtil.getHospitalId());
                InformationDataSet.SpecialtyTableAdapter.Fill(InformationDataSet.Instance.Specialty, HospitalUtil.getHospitalId());
                InformationDataSet.RoleTableAdapter.Fill(InformationDataSet.Instance.Role);
                UserDataSet.WardTableAdapter.Fill(UserDataSet.Instance.Ward, HospitalUtil.getHospitalId());
                ProgramUtil.closeWaitDialog();
            }
            
            private bool checkUser()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                //Instance.Validate();

                if (textEditFirstName.EditValue == DBNull.Value || textEditFirstName.EditValue == null || (textEditFirstName.EditValue = textEditFirstName.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Нэрийг оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditFirstName.ErrorText = errorText;
                    isRight = false;
                    textEditFirstName.Focus();
                }
                if (textEditLastName.EditValue == DBNull.Value || textEditLastName.EditValue == null || (textEditLastName.EditValue = textEditLastName.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Эцэг эхийн нэрийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditLastName.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditLastName.Focus();
                    }
                }
                //if (textEditSirName.EditValue == DBNull.Value || textEditSirName.EditValue == null || (textEditSirName.EditValue = textEditSirName.EditValue.ToString().Trim()).Equals(""))
                //{
                //    errorText = "Ургийн овгийг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    textEditSirName.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        isRight = false;
                //        textEditSirName.Focus();
                //    }
                //}
                if (textEditRegister.EditValue == DBNull.Value || textEditRegister.EditValue == null || (textEditRegister.EditValue = textEditRegister.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Регистерийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditRegister.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditRegister.Focus();
                    }
                }
                //if (dateEditBirthday.EditValue == DBNull.Value || dateEditBirthday.EditValue == null)
                //{
                //    errorText = "Төрсөн өдрийг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    dateEditBirthday.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        isRight = false;
                //        dateEditBirthday.Focus();
                //    }
                //}
                if (gridLookUpEditUserRole.EditValue == DBNull.Value || gridLookUpEditUserRole.EditValue == null || gridLookUpEditUserRole.Text.Equals(""))
                {
                    errorText = "Хэрэглэгчийн эрхийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUserRole.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        xtraTabControl.SelectedTabPage = xtraTabPageRole;
                        gridLookUpEditUserRole.Focus();
                    }
                }
                if (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals(""))
                {
                    errorText = "Тасгийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }
                if(actionType == 1)
                {
                    if (textEditUserName.EditValue == DBNull.Value || textEditUserName.EditValue == null || (textEditUserName.EditValue = textEditUserName.EditValue.ToString().Trim()).Equals(""))
                    {
                        errorText = "Нэвтрэх нэрийг оруулна уу";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        textEditUserName.ErrorText = errorText;
                        if (isRight)
                        {
                            isRight = false;
                            xtraTabControl.SelectedTabPage = xtraTabPageRole;
                            textEditUserName.Focus();
                        }
                    }
                    if (textEditPassword.EditValue == DBNull.Value || textEditPassword.EditValue == null || (textEditPassword.EditValue = textEditPassword.EditValue.ToString().Trim()).Equals(""))
                    {
                        errorText = "Нууц үгийг оруулна уу";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        textEditPassword.ErrorText = errorText;
                        if (isRight)
                        {
                            isRight = false;
                            xtraTabControl.SelectedTabPage = xtraTabPageRole;
                            textEditPassword.Focus();
                        }
                    }
                    if (textEditPasswordRepeat.EditValue == DBNull.Value || textEditPasswordRepeat.EditValue == null || (textEditPasswordRepeat.EditValue = textEditPasswordRepeat.EditValue.ToString().Trim()).Equals(""))
                    {
                        errorText = "Нууц үгийг давтан оруулна уу";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        textEditPasswordRepeat.ErrorText = errorText;
                        if (isRight)
                        {
                            isRight = false;
                            xtraTabControl.SelectedTabPage = xtraTabPageRole;
                            textEditPasswordRepeat.Focus();
                        }
                    }
                }
                //if (gridLookUpEditEducation.EditValue == DBNull.Value || gridLookUpEditEducation.EditValue == null || gridLookUpEditEducation.Text.Equals(""))
                //{
                //    errorText = "Боловсролыг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    gridLookUpEditEducation.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        isRight = false;
                //        xtraTabControl.SelectedTabPage = xtraTabPageUser;
                //        gridLookUpEditEducation.Focus();
                //    }
                //}
                //if (gridLookUpEditOrganization.EditValue == DBNull.Value || gridLookUpEditOrganization.EditValue == null || gridLookUpEditOrganization.Text.Equals(""))
                //{
                //    errorText = "Ажлын газрыг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    gridLookUpEditOrganization.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        isRight = false;
                //        xtraTabControl.SelectedTabPage = xtraTabPageUser;
                //        gridLookUpEditOrganization.Focus();
                //    }
                //}
                //if (gridLookUpEditPosition.EditValue == DBNull.Value || gridLookUpEditPosition.EditValue == null || gridLookUpEditPosition.Text.Equals(""))
                //{
                //    errorText = "Албан тушаалыг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    gridLookUpEditPosition.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        isRight = false;
                //        xtraTabControl.SelectedTabPage = xtraTabPageUser;
                //        gridLookUpEditPosition.Focus();
                //    }
                //}
                //if (treeListLookUpEditCountry.EditValue == DBNull.Value || treeListLookUpEditCountry.EditValue == null || treeListLookUpEditCountry.Text.Equals(""))
                //{
                //    errorText = "Байршлыг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    treeListLookUpEditCountry.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        isRight = false;
                //        xtraTabControl.SelectedTabPage = xtraTabPageAddress;
                //        treeListLookUpEditCountry.Focus();
                //    }
                //}
                if (isRight && actionType == 1)
                {
                    if (!isDownload)
                        isRight = checkRegister();

                    if (isRight && !textEditPassword.EditValue.ToString().Trim().Equals(textEditPasswordRepeat.EditValue.ToString().Trim()))
                    {
                        errorText = "Нууц үгийг буруу оруулсан байна";
                        text += ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        textEditPasswordRepeat.ErrorText = errorText;
                        isRight = false;
                        xtraTabControl.SelectedTabPage = xtraTabPageRole;
                        textEditPasswordRepeat.Focus();
                    }
                }
                else if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }
            
            private bool checkRegister()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                int exist = Convert.ToInt32(InformationDataSet.QueriesTableAdapter.exist_user_id(textEditUserName.EditValue.ToString()));
                if (exist == 2)
                {
                    errorText = "Энэ нэвтрэх нэртэй хэрэглэгч бүртгэсэн байна";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditUserName.ErrorText = errorText;
                    isRight = false;
                    textEditRegister.Focus();
                }
                //else if (exist == 3)
                //{
                //    errorText = "Энэ регистертэй хэрэглэгч бүртгэсэн бөгөөд идэвхигүй болгосон байна";
                //    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    textEditRegister.ErrorText = errorText;
                //    isRight = false;
                //    textEditRegister.Focus();
                //}
                //else if (exist == 4 || exist == 5)
                //{
                //    DialogResult dialogResult = XtraMessageBox.Show("Энэ регистертей эмчлүүлэгч бүртгэсэн байна. Эмчлүүлэгчийн мэдээллийг татах уу?", "Эмчлүүлэгчийн мэдээлэл татах", MessageBoxButtons.YesNo);
                //    if (dialogResult == DialogResult.Yes)
                //    {
                //        InformationDataSet.UserInfoTableAdapter.Fill(InformationDataSet.Instance.UserInfo, HospitalUtil.getHospitalId(), textEditRegister.Text);
                //        if (InformationDataSet.Instance.UserInfo.Rows.Count > 0)
                //        {
                //            DataRow row = InformationDataSet.Instance.UserInfo.Rows[0];
                //            UserForm.Instance.currView["firstName"] = row["firstName"];
                //            UserForm.Instance.currView["lastName"] = row["lastName"];
                //            UserForm.Instance.currView["sirName"] = row["sirName"];
                //            UserForm.Instance.currView["emdNumber"] = row["emdNumber"];
                //            UserForm.Instance.currView["nddNumber"] = row["nddNumber"];
                //            UserForm.Instance.currView["phone"] = row["phone"];
                //            UserForm.Instance.currView["email"] = row["email"];
                //            UserForm.Instance.currView["address"] = row["address"];
                //            UserForm.Instance.currView["birthday"] = row["birthday"];
                //            UserForm.Instance.currView["education"] = row["education"];
                //            UserForm.Instance.currView["organizationID"] = row["organizationID"];
                //            UserForm.Instance.currView["positionID"] = row["positionID"];
                //            UserForm.Instance.currView["specialtyID"] = row["specialtyID"];
                //            UserForm.Instance.currView["provinceID"] = row["provinceID"];
                //            UserForm.Instance.currView["gender"] = row["gender"];
                //            UserForm.Instance.currView["isMarried"] = row["isMarried"];
                //            UserForm.Instance.currView["picture"] = row["picture"];
                //            UserForm.Instance.currView.EndEdit();
                //            isDownload = true;
                //        }
                //        else
                //            isRight = false;
                //    }
                //    else
                //    {
                //        errorText = "Энэ регистертэй эмчлүүлэгч бүртгэсэн байна";
                //        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //        textEditRegister.ErrorText = errorText;
                //        textEditRegister.Focus();
                //        isRight = false;
                //    }
                //}
                //else if (exist == 6)
                //{
                //    DialogResult dialogResult = XtraMessageBox.Show("Энэ регистертей хүн бүртгэсэн байна. Хэрэглэгчийн мэдээллийг татах уу?", "Хэрэглэгчийн мэдээлэл татах", MessageBoxButtons.YesNo);
                //    if (dialogResult == DialogResult.Yes)
                //    {
                //        InformationDataSet.UserInfoTableAdapter.Fill(InformationDataSet.Instance.UserInfo, HospitalUtil.getHospitalId(), textEditRegister.Text);
                //        if (InformationDataSet.Instance.UserInfo.Rows.Count > 0)
                //        {
                //            DataRow row = InformationDataSet.Instance.UserInfo.Rows[0];
                //            UserForm.Instance.currView["firstName"] = row["firstName"];
                //            UserForm.Instance.currView["lastName"] = row["lastName"];
                //            UserForm.Instance.currView["sirName"] = row["sirName"];
                //            UserForm.Instance.currView["emdNumber"] = row["emdNumber"];
                //            UserForm.Instance.currView["nddNumber"] = row["nddNumber"];
                //            UserForm.Instance.currView["phone"] = row["phone"];
                //            UserForm.Instance.currView["email"] = row["email"];
                //            UserForm.Instance.currView["address"] = row["address"];
                //            UserForm.Instance.currView["birthday"] = row["birthday"];
                //            UserForm.Instance.currView["education"] = row["education"];
                //            UserForm.Instance.currView["organizationID"] = row["organizationID"];
                //            UserForm.Instance.currView["positionID"] = row["positionID"];
                //            UserForm.Instance.currView["specialtyID"] = row["specialtyID"];
                //            UserForm.Instance.currView["provinceID"] = row["provinceID"];
                //            UserForm.Instance.currView["gender"] = row["gender"];
                //            UserForm.Instance.currView["isMarried"] = row["isMarried"];
                //            UserForm.Instance.currView["picture"] = row["picture"];
                //            UserForm.Instance.currView.EndEdit();
                //            isDownload = true;
                //        }
                //        else
                //            isRight = false;
                //    }
                //    else
                //    {
                //        errorText = "Энэ регистертэй хүн бүртгэсэн байна";
                //        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //        textEditRegister.ErrorText = errorText;
                //        textEditRegister.Focus();
                //        isRight = false;
                //    }
                //}
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }
            
            private void saveUser()
            {
                if (checkUser())
                {
                    if (actionType == 1 || actionType == 2)
                    {
                        UserForm.Instance.currView["password"] = UserUtil.convertMD5Hash(textEditPassword.Text);
                        UserForm.Instance.currView["isNewUser"] = !isDownload;
                    }                 

                    //UserForm.Instance.currView["organizationName"] = gridLookUpEditOrganization.Text;
                    //UserForm.Instance.currView["positionName"] = gridLookUpEditPosition.Text;
                    UserForm.Instance.currView["specialtyName"] = gridLookUpEditSpecialty.Text;
                    UserForm.Instance.currView["wardName"] = treeListLookUpEditWard.Text;
                    UserForm.Instance.currView["provinceName"] = treeListLookUpEditCountry.Text;
                    UserForm.Instance.currView["roleName"] = gridLookUpEditUserRole.Text;

                    UserForm.Instance.currView.EndEdit();
                    InformationDataSet.UserTableAdapter.Update(InformationDataSet.Instance.User);
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                    if (ward != null && textEditID.EditValue != null)
                        UserUtil.addEachUserWard(ward, textEditID.EditValue);

                    if (actionType == 1)
                    {
                        textEditSirName.Focus();
                        xtraTabControl.SelectedTabPage = xtraTabPageAddress;
                        xtraTabControl.SelectedTabPage = xtraTabPageUser;
                        xtraTabControl.SelectedTabPage = xtraTabPageRole;
                        clearData();
                        InformationDataSet.Instance.User.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        InformationDataSet.Instance.User.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserForm.Instance.addMultiple();
                    }
                    else if (actionType == 2)
                    {
                        textEditSirName.Focus();
                        xtraTabControl.SelectedTabPage = xtraTabPageAddress;
                        xtraTabControl.SelectedTabPage = xtraTabPageUser;
                        xtraTabControl.SelectedTabPage = xtraTabPageRole;
                        clearData();
                        insertUser();
                        InformationDataSet.Instance.User.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        InformationDataSet.Instance.User.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserForm.Instance.addMultiple();
                    }
                }
            }
            

            // Шинээр нэмсэн мэргэжлийг авч байна
            public void insertSpecialty(string id)
            {
                gridLookUpEditSpecialty.EditValue = id;
            }

            // Шинээр нэмсэн албан тушаалыг авч байна
            public void insertPosition(string id)
            {
                gridLookUpEditPosition.EditValue = id;
            }

            // Шинээр нэмсэн ажлын газрыг авч байна
            public void insertOrganization(string id)
            {
                gridLookUpEditOrganization.EditValue = id;
            }

        #endregion                        

            
    }
}