﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.info.user
{
    /*
     * Хэрэглэгчийн бүртгэлийг харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class UserForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static UserForm INSTANCE = new UserForm();

            public static UserForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 3005;

            private bool isInit = false;

            public DataRowView currView;

            private System.IO.Stream layout_user = null;

            private UserForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initLayout();
                RoleUtil.addForm(FORM_ID, this);

                radioGroupIsUse.SelectedIndex = 0;
                listBoxControlRole.DataSource = InformationDataSet.Instance.RoleOther;
                gridControlUser.DataSource = InformationDataSet.UserBindingSource;
            }

            private void initLayout()
            {
                layout_user = new System.IO.MemoryStream();
                gridViewUser.SaveLayoutToStream(layout_user);
                layout_user.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion

        #region Форм нээх, хаах функц
        
            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        checkRole();

                        InformationDataSet.Instance.User.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        InformationDataSet.Instance.User.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        listBoxControlRole.SelectedIndex = 0;
                        reload();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                gridControlUser.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlUser.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridControlUser.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                gridControlUser.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];

                simpleButtonExport.Visible = roles[4];
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;
                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("user");
                if (layout != null)
                {
                    gridViewUser.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewUser.RestoreLayoutFromStream(layout_user);
                    layout_user.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }
            
            private void UserForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                InformationDataSet.Instance.User.Clear();
            }

        #endregion

        #region Формын event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void gridControlUser_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        add();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewUser.GetFocusedRow() != null)
                    {
                        try
                        {
                            delete();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "update") && gridViewUser.GetFocusedDataRow() != null)
                    {
                        update();
                    }
                    else if (String.Equals(e.Button.Tag, "password") && gridViewUser.GetFocusedDataRow() != null)
                    {
                        changePassword();
                    }
                }
            }

            private void gridViewUser_DoubleClick(object sender, EventArgs e)
            {
                if (gridViewUser.GetFocusedDataRow() != null)
                {
                    update();
                }
            }

            private void radioGroupIsUse_SelectedIndexChanged(object sender, EventArgs e)
            {
                showIsUse();
            }

            private void listBoxControlRole_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeRole();
            }

            private void simpleButtonExport_Click(object sender, EventArgs e)
            {
                ProgramUtil.convertGrid(gridViewUser, "Хэрэглэгчийн бүртгэл");
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                saveLayout();
            }

        #endregion

        #region Формын функц

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.UserTableAdapter.Fill(InformationDataSet.Instance.User, HospitalUtil.getHospitalId());
                showIsUse();
                ProgramUtil.setRoleOther();
                ProgramUtil.closeWaitDialog();
            }

            private void add()
            {               
                addMultiple();
                UserInUpForm.Instance.showForm(1);
            }

            public void addMultiple()
            {
                gridViewUser.ActiveFilterString = "";
                InformationDataSet.Instance.User.idColumn.DefaultValue = InformationDataSet.QueriesTableAdapter.get_user_id(HospitalUtil.getHospitalId(), 0);
                currView = (DataRowView)InformationDataSet.UserBindingSource.AddNew();
            }

            private void delete()
            {
                DataRow row = gridViewUser.GetFocusedDataRow();
                if (Convert.ToInt32(row["roleID"]) == RoleUtil.ADMIN_ID)
                {
                    bool isDelete = Convert.ToBoolean(InformationDataSet.QueriesTableAdapter.can_delete_admin(HospitalUtil.getHospitalId(), row["id"].ToString()));
                    if (isDelete)
                    {
                        XtraMessageBox.Show("Энэ хэрэглэгч цор ганц админ тул устгаж болохгүй");
                        return;
                    }
                }
                
                object ret = InformationDataSet.QueriesTableAdapter.can_delete_user(HospitalUtil.getHospitalId(), row["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }
                
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Хэрэглэгч устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    gridViewUser.DeleteSelectedRows();
                    InformationDataSet.UserBindingSource.EndEdit();
                    InformationDataSet.UserTableAdapter.Update(InformationDataSet.Instance.User);
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void update()
            {
                currView = (DataRowView)InformationDataSet.UserBindingSource.Current;
                UserInUpForm.Instance.showForm(2);
            }

            private void changePassword()
            {
                currView = (DataRowView)InformationDataSet.UserBindingSource.Current;
                ChangePassForm.Instance.showForm(currView["id"].ToString());
            }

            private void showIsUse()
            {
                if (radioGroupIsUse.SelectedIndex >= 0)
                    gridViewUser.ActiveFilterString = "isUseText = '" + radioGroupIsUse.Properties.Items[radioGroupIsUse.SelectedIndex].Description + "'";
            }

            private void changeRole()
            {
                if (listBoxControlRole.SelectedItem != null)
                {
                    DataRowView view = (DataRowView)listBoxControlRole.SelectedItem;
                    if (!view["id"].ToString().Equals("-1"))
                        gridViewUser.ActiveFilterString = "roleName = '" + view["name"] + "'";
                    else
                        gridViewUser.ActiveFilterString = "";
                }
            }

            private void saveLayout()
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    gridViewUser.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    System.IO.StreamReader reader = new System.IO.StreamReader(str);
                    string layout = reader.ReadToEnd();
                    UserUtil.saveUserLayout("user", layout);
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}