﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.info.user
{
    /*
     * Хэрэглэгчийн нууц үгийг өөрчлөх цонх.
     * 
     */

    public partial class ChangePassForm : DevExpress.XtraEditors.XtraForm
    {
        
        #region Форм анх ачааллах функц

            private static ChangePassForm INSTANCE = new ChangePassForm();

            public static ChangePassForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private string id;

            private ChangePassForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                textEditPassword.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditPasswordRepeat.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(string id)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    this.id = id;

                    ShowDialog(UserForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                textEditPassword.Text = "";
                textEditPasswordRepeat.Text = "";

                textEditPassword.ErrorText = "";
                textEditPasswordRepeat.ErrorText = "";
            }

            private void ChangePassForm_Shown(object sender, EventArgs e)
            {
                textEditPassword.Focus();
            }

        #endregion

        #region Формын event

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                changePassword();
            }

        #endregion

        #region Формын функц

            private bool checkUser()
            {
                Instance.Validate();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (textEditPassword.EditValue == DBNull.Value || (textEditPassword.EditValue = textEditPassword.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Нууц үгийг оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditPassword.ErrorText = errorText;
                    isRight = false;
                    textEditPassword.Focus();
                }
                if (textEditPasswordRepeat.EditValue == DBNull.Value || (textEditPasswordRepeat.EditValue = textEditPasswordRepeat.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Нууц үгийг давтан оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditPasswordRepeat.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditPasswordRepeat.Focus();
                    }
                }
                if (isRight && !textEditPassword.EditValue.ToString().Trim().Equals(textEditPasswordRepeat.EditValue.ToString().Trim()))
                {
                    errorText = "Нууц үгийг зөв оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditPasswordRepeat.ErrorText = errorText;
                    isRight = false;
                    textEditPasswordRepeat.Focus();
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void changePassword()
            {
                if (checkUser())
                {   
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    int result = InformationDataSet.QueriesTableAdapter.update_user_pass(id, HospitalUtil.getHospitalId(), UserUtil.convertMD5Hash(textEditPassword.Text), UserUtil.getUserId());
                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }

        #endregion

    }
}