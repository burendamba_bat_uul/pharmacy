﻿namespace Pharmacy2016.gui.core.info.user
{
    partial class ChangePassForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlPasswordRepeat = new DevExpress.XtraEditors.LabelControl();
            this.textEditPasswordRepeat = new DevExpress.XtraEditors.TextEdit();
            this.labelControlPassword = new DevExpress.XtraEditors.LabelControl();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPasswordRepeat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(297, 126);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 0;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(216, 126);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 1;
            this.simpleButtonSave.Text = "Солих";
            this.simpleButtonSave.ToolTip = "Нууц үг солих";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // labelControlPasswordRepeat
            // 
            this.labelControlPasswordRepeat.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlPasswordRepeat.Location = new System.Drawing.Point(52, 66);
            this.labelControlPasswordRepeat.Name = "labelControlPasswordRepeat";
            this.labelControlPasswordRepeat.Size = new System.Drawing.Size(88, 13);
            this.labelControlPasswordRepeat.TabIndex = 42;
            this.labelControlPasswordRepeat.Text = "Нууц үг давтах*:";
            // 
            // textEditPasswordRepeat
            // 
            this.textEditPasswordRepeat.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditPasswordRepeat.EnterMoveNextControl = true;
            this.textEditPasswordRepeat.Location = new System.Drawing.Point(143, 63);
            this.textEditPasswordRepeat.Name = "textEditPasswordRepeat";
            this.textEditPasswordRepeat.Properties.MaxLength = 32;
            this.textEditPasswordRepeat.Properties.PasswordChar = '*';
            this.textEditPasswordRepeat.Size = new System.Drawing.Size(200, 20);
            this.textEditPasswordRepeat.TabIndex = 41;
            // 
            // labelControlPassword
            // 
            this.labelControlPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlPassword.Location = new System.Drawing.Point(91, 40);
            this.labelControlPassword.Name = "labelControlPassword";
            this.labelControlPassword.Size = new System.Drawing.Size(48, 13);
            this.labelControlPassword.TabIndex = 40;
            this.labelControlPassword.Text = "Нууц үг*:";
            // 
            // textEditPassword
            // 
            this.textEditPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditPassword.EnterMoveNextControl = true;
            this.textEditPassword.Location = new System.Drawing.Point(143, 37);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Properties.MaxLength = 32;
            this.textEditPassword.Properties.PasswordChar = '*';
            this.textEditPassword.Size = new System.Drawing.Size(200, 20);
            this.textEditPassword.TabIndex = 39;
            // 
            // ChangePassForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(384, 161);
            this.Controls.Add(this.labelControlPasswordRepeat);
            this.Controls.Add(this.textEditPasswordRepeat);
            this.Controls.Add(this.labelControlPassword);
            this.Controls.Add(this.textEditPassword);
            this.Controls.Add(this.simpleButtonSave);
            this.Controls.Add(this.simpleButtonCancel);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(400, 200);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 200);
            this.Name = "ChangePassForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Нууц үг солих";
            this.Shown += new System.EventHandler(this.ChangePassForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.textEditPasswordRepeat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.LabelControl labelControlPasswordRepeat;
        private DevExpress.XtraEditors.TextEdit textEditPasswordRepeat;
        private DevExpress.XtraEditors.LabelControl labelControlPassword;
        private DevExpress.XtraEditors.TextEdit textEditPassword;
    }
}