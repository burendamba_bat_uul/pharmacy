﻿namespace Pharmacy2016.gui.core.info.user
{
    partial class UserConfForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserConfForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonExport = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageOrganization = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlOrganization = new DevExpress.XtraGrid.GridControl();
            this.gridViewOrganization = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnOrgName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOrgDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPagePosition = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlPosition = new DevExpress.XtraGrid.GridControl();
            this.gridViewPosition = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnPosName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPosDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageCountry = new DevExpress.XtraTab.XtraTabPage();
            this.treeListProvince = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnIsCity = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.controlNavigator = new DevExpress.XtraEditors.ControlNavigator();
            this.xtraTabPageSpecialty = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlSpecialty = new DevExpress.XtraGrid.GridControl();
            this.gridViewSpecialty = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageOrganization.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOrganization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOrganization)).BeginInit();
            this.xtraTabPagePosition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPosition)).BeginInit();
            this.xtraTabPageCountry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListProvince)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.xtraTabPageSpecialty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSpecialty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSpecialty)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonLayout);
            this.panelControl1.Controls.Add(this.simpleButtonExport);
            this.panelControl1.Controls.Add(this.simpleButtonReload);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 50);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(749, 13);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 7;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // simpleButtonExport
            // 
            this.simpleButtonExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonExport.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonExport.Image")));
            this.simpleButtonExport.Location = new System.Drawing.Point(653, 13);
            this.simpleButtonExport.Name = "simpleButtonExport";
            this.simpleButtonExport.Size = new System.Drawing.Size(90, 23);
            this.simpleButtonExport.TabIndex = 4;
            this.simpleButtonExport.Text = "Хөрвүүлэх";
            this.simpleButtonExport.ToolTip = "Хэрэглэгчийн лавлах мэдээллийг хөрвүүлэх";
            this.simpleButtonExport.Click += new System.EventHandler(this.simpleButtonExport_Click);
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 12);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(80, 25);
            this.simpleButtonReload.TabIndex = 0;
            this.simpleButtonReload.Text = "Сэргээх";
            this.simpleButtonReload.ToolTip = "Хэрэглэгчийн лавлах мэдээллийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl.Location = new System.Drawing.Point(0, 50);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageOrganization;
            this.xtraTabControl.Size = new System.Drawing.Size(784, 411);
            this.xtraTabControl.TabIndex = 1;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageOrganization,
            this.xtraTabPagePosition,
            this.xtraTabPageCountry,
            this.xtraTabPageSpecialty});
            // 
            // xtraTabPageOrganization
            // 
            this.xtraTabPageOrganization.Controls.Add(this.gridControlOrganization);
            this.xtraTabPageOrganization.Name = "xtraTabPageOrganization";
            this.xtraTabPageOrganization.Size = new System.Drawing.Size(778, 383);
            this.xtraTabPageOrganization.Text = "Ажлын газар";
            // 
            // gridControlOrganization
            // 
            this.gridControlOrganization.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlOrganization.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlOrganization.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlOrganization.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlOrganization.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlOrganization.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlOrganization.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlOrganization.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlOrganization_EmbeddedNavigator_ButtonClick);
            this.gridControlOrganization.Location = new System.Drawing.Point(0, 0);
            this.gridControlOrganization.MainView = this.gridViewOrganization;
            this.gridControlOrganization.Name = "gridControlOrganization";
            this.gridControlOrganization.Size = new System.Drawing.Size(778, 383);
            this.gridControlOrganization.TabIndex = 0;
            this.gridControlOrganization.UseEmbeddedNavigator = true;
            this.gridControlOrganization.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOrganization});
            // 
            // gridViewOrganization
            // 
            this.gridViewOrganization.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnOrgName,
            this.gridColumnOrgDesc});
            this.gridViewOrganization.GridControl = this.gridControlOrganization;
            this.gridViewOrganization.Name = "gridViewOrganization";
            this.gridViewOrganization.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewOrganization.OptionsBehavior.ReadOnly = true;
            this.gridViewOrganization.OptionsCustomization.AllowGroup = false;
            this.gridViewOrganization.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewOrganization.OptionsFind.AllowFindPanel = false;
            this.gridViewOrganization.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewOrganization.OptionsView.ShowAutoFilterRow = true;
            this.gridViewOrganization.OptionsView.ShowGroupPanel = false;
            this.gridViewOrganization.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewOrganization_InvalidRowException);
            this.gridViewOrganization.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewOrganization_ValidateRow);
            this.gridViewOrganization.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewOrganization_RowUpdated);
            // 
            // gridColumnOrgName
            // 
            this.gridColumnOrgName.Caption = "Ажлын газрын нэр";
            this.gridColumnOrgName.FieldName = "name";
            this.gridColumnOrgName.FieldNameSortGroup = "name";
            this.gridColumnOrgName.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumnOrgName.Name = "gridColumnOrgName";
            this.gridColumnOrgName.Visible = true;
            this.gridColumnOrgName.VisibleIndex = 0;
            // 
            // gridColumnOrgDesc
            // 
            this.gridColumnOrgDesc.Caption = "Тайлбар";
            this.gridColumnOrgDesc.CustomizationCaption = "Тайлбар";
            this.gridColumnOrgDesc.FieldName = "description";
            this.gridColumnOrgDesc.Name = "gridColumnOrgDesc";
            this.gridColumnOrgDesc.Visible = true;
            this.gridColumnOrgDesc.VisibleIndex = 1;
            // 
            // xtraTabPagePosition
            // 
            this.xtraTabPagePosition.Controls.Add(this.gridControlPosition);
            this.xtraTabPagePosition.Name = "xtraTabPagePosition";
            this.xtraTabPagePosition.Size = new System.Drawing.Size(778, 383);
            this.xtraTabPagePosition.Text = "Албан тушаал";
            // 
            // gridControlPosition
            // 
            this.gridControlPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPosition.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPosition.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPosition.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPosition.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPosition.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPosition.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlPosition.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlPosition_EmbeddedNavigator_ButtonClick);
            this.gridControlPosition.Location = new System.Drawing.Point(0, 0);
            this.gridControlPosition.MainView = this.gridViewPosition;
            this.gridControlPosition.Name = "gridControlPosition";
            this.gridControlPosition.Size = new System.Drawing.Size(778, 383);
            this.gridControlPosition.TabIndex = 1;
            this.gridControlPosition.UseEmbeddedNavigator = true;
            this.gridControlPosition.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPosition});
            // 
            // gridViewPosition
            // 
            this.gridViewPosition.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnPosName,
            this.gridColumnPosDesc});
            this.gridViewPosition.GridControl = this.gridControlPosition;
            this.gridViewPosition.Name = "gridViewPosition";
            this.gridViewPosition.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewPosition.OptionsBehavior.ReadOnly = true;
            this.gridViewPosition.OptionsCustomization.AllowGroup = false;
            this.gridViewPosition.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewPosition.OptionsFind.AllowFindPanel = false;
            this.gridViewPosition.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewPosition.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPosition.OptionsView.ShowGroupPanel = false;
            this.gridViewPosition.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewPosition_InvalidRowException);
            this.gridViewPosition.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewPosition_ValidateRow);
            this.gridViewPosition.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewPosition_RowUpdated);
            // 
            // gridColumnPosName
            // 
            this.gridColumnPosName.Caption = "Албан тушаалын нэр";
            this.gridColumnPosName.CustomizationCaption = "Албан тушаалын нэр";
            this.gridColumnPosName.FieldName = "name";
            this.gridColumnPosName.Name = "gridColumnPosName";
            this.gridColumnPosName.Visible = true;
            this.gridColumnPosName.VisibleIndex = 0;
            // 
            // gridColumnPosDesc
            // 
            this.gridColumnPosDesc.Caption = "Тайлбар";
            this.gridColumnPosDesc.CustomizationCaption = "Тайлбар";
            this.gridColumnPosDesc.FieldName = "description";
            this.gridColumnPosDesc.Name = "gridColumnPosDesc";
            this.gridColumnPosDesc.Visible = true;
            this.gridColumnPosDesc.VisibleIndex = 1;
            // 
            // xtraTabPageCountry
            // 
            this.xtraTabPageCountry.Controls.Add(this.treeListProvince);
            this.xtraTabPageCountry.Controls.Add(this.panelControl2);
            this.xtraTabPageCountry.Name = "xtraTabPageCountry";
            this.xtraTabPageCountry.Size = new System.Drawing.Size(778, 383);
            this.xtraTabPageCountry.Text = "Хот, аймаг";
            // 
            // treeListProvince
            // 
            this.treeListProvince.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnName,
            this.treeListColumnIsCity});
            this.treeListProvince.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListProvince.KeyFieldName = "id";
            this.treeListProvince.Location = new System.Drawing.Point(0, 0);
            this.treeListProvince.Name = "treeListProvince";
            this.treeListProvince.OptionsBehavior.Editable = false;
            this.treeListProvince.OptionsNavigation.AutoFocusNewNode = true;
            this.treeListProvince.ParentFieldName = "pid";
            this.treeListProvince.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.treeListProvince.Size = new System.Drawing.Size(778, 358);
            this.treeListProvince.TabIndex = 6;
            // 
            // treeListColumnName
            // 
            this.treeListColumnName.Caption = "Нэр";
            this.treeListColumnName.FieldName = "name";
            this.treeListColumnName.Name = "treeListColumnName";
            this.treeListColumnName.Visible = true;
            this.treeListColumnName.VisibleIndex = 0;
            this.treeListColumnName.Width = 683;
            // 
            // treeListColumnIsCity
            // 
            this.treeListColumnIsCity.Caption = "Хот эсэх";
            this.treeListColumnIsCity.ColumnEdit = this.repositoryItemCheckEdit1;
            this.treeListColumnIsCity.FieldName = "isCity";
            this.treeListColumnIsCity.Name = "treeListColumnIsCity";
            this.treeListColumnIsCity.Visible = true;
            this.treeListColumnIsCity.VisibleIndex = 1;
            this.treeListColumnIsCity.Width = 77;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.controlNavigator);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 358);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(778, 25);
            this.panelControl2.TabIndex = 5;
            // 
            // controlNavigator
            // 
            this.controlNavigator.Buttons.Append.Visible = false;
            this.controlNavigator.Buttons.CancelEdit.Visible = false;
            this.controlNavigator.Buttons.Edit.Visible = false;
            this.controlNavigator.Buttons.EndEdit.Visible = false;
            this.controlNavigator.Buttons.First.Visible = false;
            this.controlNavigator.Buttons.Last.Visible = false;
            this.controlNavigator.Buttons.Next.Visible = false;
            this.controlNavigator.Buttons.NextPage.Visible = false;
            this.controlNavigator.Buttons.Prev.Visible = false;
            this.controlNavigator.Buttons.PrevPage.Visible = false;
            this.controlNavigator.Buttons.Remove.Visible = false;
            this.controlNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 17, true, true, "Задлах", "expand"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, false, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, false, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "edit")});
            this.controlNavigator.Location = new System.Drawing.Point(0, 1);
            this.controlNavigator.Name = "controlNavigator";
            this.controlNavigator.ShowToolTips = true;
            this.controlNavigator.Size = new System.Drawing.Size(104, 24);
            this.controlNavigator.TabIndex = 3;
            this.controlNavigator.Text = "controlNavigator1";
            this.controlNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.controlNavigator_ButtonClick);
            // 
            // xtraTabPageSpecialty
            // 
            this.xtraTabPageSpecialty.Controls.Add(this.gridControlSpecialty);
            this.xtraTabPageSpecialty.Name = "xtraTabPageSpecialty";
            this.xtraTabPageSpecialty.Size = new System.Drawing.Size(778, 383);
            this.xtraTabPageSpecialty.Text = "Мэргэжил";
            // 
            // gridControlSpecialty
            // 
            this.gridControlSpecialty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSpecialty.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlSpecialty.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlSpecialty.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlSpecialty.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlSpecialty.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlSpecialty.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlSpecialty.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlSpecialty_EmbeddedNavigator_ButtonClick);
            this.gridControlSpecialty.Location = new System.Drawing.Point(0, 0);
            this.gridControlSpecialty.MainView = this.gridViewSpecialty;
            this.gridControlSpecialty.Name = "gridControlSpecialty";
            this.gridControlSpecialty.Size = new System.Drawing.Size(778, 383);
            this.gridControlSpecialty.TabIndex = 1;
            this.gridControlSpecialty.UseEmbeddedNavigator = true;
            this.gridControlSpecialty.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSpecialty});
            // 
            // gridViewSpecialty
            // 
            this.gridViewSpecialty.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.gridViewSpecialty.GridControl = this.gridControlSpecialty;
            this.gridViewSpecialty.Name = "gridViewSpecialty";
            this.gridViewSpecialty.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditFormInplace;
            this.gridViewSpecialty.OptionsBehavior.ReadOnly = true;
            this.gridViewSpecialty.OptionsCustomization.AllowGroup = false;
            this.gridViewSpecialty.OptionsEditForm.FormCaptionFormat = "{id}";
            this.gridViewSpecialty.OptionsFind.AllowFindPanel = false;
            this.gridViewSpecialty.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewSpecialty.OptionsView.ShowAutoFilterRow = true;
            this.gridViewSpecialty.OptionsView.ShowGroupPanel = false;
            this.gridViewSpecialty.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewSpecialty_InvalidRowException);
            this.gridViewSpecialty.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewSpecialty_ValidateRow);
            this.gridViewSpecialty.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewSpecialty_RowUpdated);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Мэргэжил";
            this.gridColumn1.CustomizationCaption = "Мэргэжил";
            this.gridColumn1.FieldName = "name";
            this.gridColumn1.FieldNameSortGroup = "name";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Тайлбар";
            this.gridColumn2.CustomizationCaption = "Тайлбар";
            this.gridColumn2.FieldName = "description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // UserConfForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.xtraTabControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "UserConfForm";
            this.ShowInTaskbar = false;
            this.Text = "Хэрэглэгчийн лавлах бүртгэл";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OrganizationForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageOrganization.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOrganization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOrganization)).EndInit();
            this.xtraTabPagePosition.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPosition)).EndInit();
            this.xtraTabPageCountry.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListProvince)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.xtraTabPageSpecialty.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSpecialty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSpecialty)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageOrganization;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePosition;
        private DevExpress.XtraGrid.GridControl gridControlOrganization;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewOrganization;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOrgName;
        private DevExpress.XtraGrid.GridControl gridControlPosition;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPosition;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPosDesc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPosName;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCountry;
        private DevExpress.XtraEditors.ControlNavigator controlNavigator;
        private DevExpress.XtraTreeList.TreeList treeListProvince;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnIsCity;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOrgDesc;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageSpecialty;
        private DevExpress.XtraGrid.GridControl gridControlSpecialty;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSpecialty;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonExport;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        
    }
}