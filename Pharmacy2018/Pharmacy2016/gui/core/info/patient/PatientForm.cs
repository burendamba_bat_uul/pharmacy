﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.info.patient
{
    /*
     * Эмчлүүлэгчийг харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class PatientForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static PatientForm INSTANCE = new PatientForm();

            public static PatientForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 3006;

            private bool isInit = false;

            public DataRowView currView;

            private System.IO.Stream layout_patient = null;

            private PatientForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                radioGroupIsUse.SelectedIndex = 0;
                initLayout();
                RoleUtil.addForm(FORM_ID, this);

                gridControlPatient.DataSource = InformationDataSet.PatientBindingSource;
            }

            private void initLayout()
            {
                layout_patient = new System.IO.MemoryStream();
                gridViewPatient.SaveLayoutToStream(layout_patient);
                layout_patient.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion

        #region Форм нээх, хаах функц
        
            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        checkRole();

                        InformationDataSet.Instance.Patient.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        InformationDataSet.Instance.Patient.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        reload();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                gridControlPatient.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlPatient.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridControlPatient.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];

                simpleButtonExport.Visible = roles[4];
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;
                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("patient");
                if (layout != null)
                {
                    gridViewPatient.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewPatient.RestoreLayoutFromStream(layout_patient);
                    layout_patient.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }
            
            private void PatientForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }
            
            private void clearData()
            {
                InformationDataSet.Instance.Patient.Clear();
            }

        #endregion

        #region Формын event

            private void gridControlPatient_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        add();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewPatient.GetFocusedDataRow() != null)
                    {
                        try
                        {
                            delete();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "update") && gridViewPatient.GetFocusedDataRow() != null)
                    {
                        update();
                    }
                }
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void radioGroupIsUse_SelectedIndexChanged(object sender, EventArgs e)
            {
                showIsUse();
            }

            private void simpleButtonExport_Click(object sender, EventArgs e)
            {
                ProgramUtil.convertGrid(gridViewPatient, "Эмчлүүлэгчийн бүртгэл");
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                saveLayout();
            }

        #endregion

        #region Формын функц

            public void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.PatientTableAdapter.Fill(InformationDataSet.Instance.Patient, HospitalUtil.getHospitalId());
                showIsUse();
                ProgramUtil.closeWaitDialog();
            }

            private void add()
            {
                addMultiple();    
                PatientInUpForm.Instance.showForm(1, Instance);
            }

            public void addMultiple()
            {
                gridViewPatient.ActiveFilterString = "";
                InformationDataSet.Instance.Patient.idColumn.DefaultValue = InformationDataSet.QueriesTableAdapter.get_user_id(HospitalUtil.getHospitalId(), 1);
                currView = (DataRowView)InformationDataSet.PatientBindingSource.AddNew();
            }

            private void delete()
            {  
                object ret = InformationDataSet.QueriesTableAdapter.can_delete_patient(HospitalUtil.getHospitalId(), gridViewPatient.GetFocusedDataRow()["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }

                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмчлүүлэгч устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    gridViewPatient.DeleteSelectedRows();
                    InformationDataSet.PatientBindingSource.EndEdit();
                    InformationDataSet.PatientTableAdapter.Update(InformationDataSet.Instance.Patient);
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void update()
            {   
                currView = (DataRowView)InformationDataSet.PatientBindingSource.Current;
                PatientInUpForm.Instance.showForm(2, Instance);
            }

            private void showIsUse()
            {
                if (radioGroupIsUse.SelectedIndex >= 0)
                    gridViewPatient.ActiveFilterString = "isUseText = '" + radioGroupIsUse.Properties.Items[radioGroupIsUse.SelectedIndex].Description + "'";
            }

            private void saveLayout()
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    gridViewPatient.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    System.IO.StreamReader reader = new System.IO.StreamReader(str);
                    string layout = reader.ReadToEnd();
                    UserUtil.saveUserLayout("patient", layout);
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}