﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.store.order;
using Pharmacy2016.gui.core.info.user;
using Pharmacy2016.gui.core.store.drug;



namespace Pharmacy2016.gui.core.info.patient
{
    /*
     * Эмчлүүлэгчийг нэмэх, засах цонх.
     * 
     */

    public partial class PatientInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц
            
            private static PatientInUpForm INSTANCE = new PatientInUpForm();

            public static PatientInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private bool isDownload = false;

            private PatientInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                //гэрлэсэн эсэх гэдэг талбарыг харагдуулахгүй болгов
                radioGroupIsMarried.Visible = false;
                labelControl10.Visible = false;

                initError();
                initDataSource();
                reload();
            }

            private void initDataSource()
            {
                gridLookUpEditOrganization.Properties.DataSource = InformationDataSet.OrganizationOtherBindingSource;
                gridLookUpEditPosition.Properties.DataSource = InformationDataSet.PositionOtherBindingSource;
                treeListLookUpEditCountry.Properties.DataSource = InformationDataSet.ProvinceOtherBindingSource;
                gridLookUpEditEducation.Properties.DataSource = InformationDataSet.Instance.Education;
                gridLookUpEditSpecialty.Properties.DataSource = InformationDataSet.SpecialtyOtherBindingSource;
            }

            private void initError()
            {
                textEditFirstName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditLastName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditSirName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditRegister.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditBirthday.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditEducation.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditOrganization.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditPosition.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditCountry.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insertPatient();
                    }
                    else if (actionType == 2)
                    {
                        updatePatient();
                    }
                    else if (actionType == 3)
                    {
                        insertPatientOther();
                    }
                    else if (actionType == 4)
                    {
                        insertPatientDrug();
                    }
                    
                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void initBinding()
            {
                textEditID.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "id", true));
                textEditFirstName.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "firstName", true));
                textEditLastName.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "lastName", true));
                textEditSirName.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "sirName", true));
                textEditRegister.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "register", true));
                textEditEMM.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "emdNumber", true));
                textEditNDD.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "nddNumber", true));
                textEditPhone.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "phone", true));
                textEditEmail.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "email", true));
                memoEditAddress.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "address", true));
                dateEditBirthday.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "birthday", true));
                gridLookUpEditEducation.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "education", true));
                gridLookUpEditOrganization.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "organizationID", true));
                gridLookUpEditPosition.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "positionID", true));
                gridLookUpEditSpecialty.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "specialtyID", true));
                treeListLookUpEditCountry.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "provinceID", true));
                radioGroupGender.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "gender", true));
                radioGroupIsMarried.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "isMarried", true));
                radioGroupIsUse.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "isUse", true));
                pictureEdit.DataBindings.Add(new Binding("EditValue", InformationDataSet.PatientBindingSource, "picture", true));
            }

            private void insertPatient()
            {
                initBinding();
            }

            private void updatePatient()
            {
                initBinding();
            }

            private void addRow()
            { 
                textEditSirName.Focus();
                xtraTabControl.SelectedTabPage = xtraTabPageAddress;
                xtraTabControl.SelectedTabPage = xtraTabPageUser;
                clearBinding();
                InformationDataSet.Instance.Patient.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                InformationDataSet.Instance.Patient.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                PatientForm.Instance.addMultiple();
                initBinding();
            }

            private void insertPatientOther()
            {
                textEditID.Text = InformationDataSet.QueriesTableAdapter.get_user_id(HospitalUtil.getHospitalId(), 1).ToString();
                pictureEdit.EditValue = null;
                textEditFirstName.EditValue = null;
                textEditLastName.EditValue = null;
                textEditSirName.EditValue = null;
                textEditRegister.EditValue = null;
                textEditEMM.EditValue = null;
                textEditNDD.EditValue = null;
                textEditPhone.EditValue = null;
                textEditEmail.EditValue = null;
                memoEditAddress.EditValue = null;
                dateEditBirthday.EditValue = null;
                gridLookUpEditEducation.EditValue = null;
                gridLookUpEditOrganization.EditValue = null;
                gridLookUpEditPosition.EditValue = null;
                gridLookUpEditSpecialty.EditValue = null;
                treeListLookUpEditCountry.EditValue = null;
                radioGroupGender.SelectedIndex = 0;
                radioGroupIsMarried.SelectedIndex = 1;
                radioGroupIsUse.SelectedIndex = 0;
            }

            private void insertPatientDrug()
            {
                textEditID.Text = InformationDataSet.QueriesTableAdapter.get_user_id(HospitalUtil.getHospitalId(), 1).ToString();
                pictureEdit.EditValue = null;
                textEditFirstName.EditValue = null;
                textEditLastName.EditValue = null;
                textEditSirName.EditValue = null;
                textEditRegister.EditValue = null;
                textEditEMM.EditValue = null;
                textEditNDD.EditValue = null;
                textEditPhone.EditValue = null;
                textEditEmail.EditValue = null;
                memoEditAddress.EditValue = null;
                dateEditBirthday.EditValue = null;
                gridLookUpEditEducation.EditValue = null;
                gridLookUpEditOrganization.EditValue = null;
                gridLookUpEditPosition.EditValue = null;
                gridLookUpEditSpecialty.EditValue = null;
                treeListLookUpEditCountry.EditValue = null;
                radioGroupGender.SelectedIndex = 0;
                radioGroupIsMarried.SelectedIndex = 1;
                radioGroupIsUse.SelectedIndex = 0;
            }

            private void clearData()
            {
                clearErrorText();
                clearBinding();
                isDownload = false;
                
                InformationDataSet.PatientBindingSource.CancelEdit();
                InformationDataSet.Instance.Patient.RejectChanges();
                InformationDataSet.Instance.UserInfo.Clear();
            }

            private void clearBinding()
            {
                textEditID.DataBindings.Clear();
                textEditFirstName.DataBindings.Clear();
                textEditLastName.DataBindings.Clear();
                textEditSirName.DataBindings.Clear();
                textEditRegister.DataBindings.Clear();
                textEditEMM.DataBindings.Clear();
                textEditNDD.DataBindings.Clear();
                textEditPhone.DataBindings.Clear();
                textEditEmail.DataBindings.Clear();
                memoEditAddress.DataBindings.Clear();
                dateEditBirthday.DataBindings.Clear();
                gridLookUpEditEducation.DataBindings.Clear();
                gridLookUpEditOrganization.DataBindings.Clear();
                gridLookUpEditPosition.DataBindings.Clear();
                gridLookUpEditSpecialty.DataBindings.Clear();
                treeListLookUpEditCountry.DataBindings.Clear();
                radioGroupGender.DataBindings.Clear();
                radioGroupIsMarried.DataBindings.Clear();
                radioGroupIsUse.DataBindings.Clear();
                pictureEdit.DataBindings.Clear();
            }

            private void clearErrorText()
            {
                textEditFirstName.ErrorText = "";
                textEditLastName.ErrorText = "";
                textEditSirName.ErrorText = "";
                textEditRegister.ErrorText = "";
                dateEditBirthday.ErrorText = "";
                gridLookUpEditEducation.ErrorText = "";
                gridLookUpEditOrganization.ErrorText = "";
                gridLookUpEditPosition.ErrorText = "";
                treeListLookUpEditCountry.ErrorText = "";
            }

            private void PatientInUpForm_Shown(object sender, EventArgs e)
            {
                textEditSirName.Focus();
                xtraTabControl.SelectedTabPage = xtraTabPageAddress;
                xtraTabControl.SelectedTabPage = xtraTabPageUser;
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }

        #endregion
        
        #region Формын event

            private void dateEditBirthday_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditBirthday.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        radioGroupGender.Focus();
                    }
                }
            }

            private void xtraTabControl_KeyUp(object sender, KeyEventArgs e)
            {
                if ((e.KeyData == Keys.Enter))
                {
                    SelectNextControl(ActiveControl, true, true, true, true);
                }
            }

            private void gridLookUpEditEducation_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditEducation.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditSpecialty.Focus();
                    }
                }
            }

            private void gridLookUpEditSpecialty_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditSpecialty.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditOrganization.Focus();
                    }
                }
            }

            private void gridLookUpEditOrganization_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditOrganization.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditPosition.Focus();
                    }
                }
            }

            private void gridLookUpEditPosition_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditPosition.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        radioGroupIsUse.Focus();
                    }
                }
            }

            private void treeListLookUpEditCountry_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditCountry.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        memoEditAddress.Focus();
                    }
                }
            }

            private void pictureEdit_EditValueChanged(object sender, EventArgs e)
            {
                if (pictureEdit.EditValue == DBNull.Value || pictureEdit.EditValue == null)
                {
                    return;
                }

                Image tmpImage = pictureEdit.Image;
                // Бага хэмжээтэй зураг бол буцааж байна
                if (tmpImage.Width <= 300 && tmpImage.Height <= 400)
                {
                    return;
                }

                // Зургийн хэмжээг багасгаж байна.
                if (tmpImage.Width / 3 > tmpImage.Height / 4)
                {
                    tmpImage = ProgramUtil.resizeImage(tmpImage, new Size(10 * Convert.ToInt32(Math.Round(40d * tmpImage.Width / tmpImage.Height)), 400));
                }
                else
                {
                    tmpImage = ProgramUtil.resizeImage(tmpImage, new Size(300, 10 * Convert.ToInt32(Math.Round(30d * tmpImage.Height / tmpImage.Width))));
                }

                tmpImage = ProgramUtil.cropImage(tmpImage, new Rectangle(new Point(tmpImage.Width / 2 - 150, tmpImage.Height / 2 - 200), new Size(300, 400)));

                PatientForm.Instance.currView["picture"] = ProgramUtil.imageToByteArray(tmpImage, System.Drawing.Imaging.ImageFormat.Jpeg);
                PatientForm.Instance.currView.EndEdit();
            }

            private void textEditRegister_EditValueChanged(object sender, EventArgs e)
            {
                if (isDownload)
                    isDownload = false;
            }

            private void gridLookUpEditOrganization_ButtonClick(object sender, ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    UserConfInUpForm.Instance.showForm(1, 2, Instance);
                    Activate();
                }
            }

            private void gridLookUpEditPosition_ButtonClick(object sender, ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    UserConfInUpForm.Instance.showForm(2, 2, Instance);
                    Activate();
                }
            }

            private void gridLookUpEditSpecialty_ButtonClick(object sender, ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    UserConfInUpForm.Instance.showForm(3, 2, Instance);
                    Activate();
                }
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                savePatient();
            }

            private void simpleButtonCheck_Click(object sender, EventArgs e)
            {
                checkRegister();
            }

        #endregion

        #region Формын функц

            private void reload()
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);       
                InformationDataSet.ProvinceTableAdapter.Fill(InformationDataSet.Instance.Province);
                InformationDataSet.OrganizationTableAdapter.Fill(InformationDataSet.Instance.Organization, HospitalUtil.getHospitalId());
                InformationDataSet.PositionTableAdapter.Fill(InformationDataSet.Instance.Position, HospitalUtil.getHospitalId());
                InformationDataSet.SpecialtyTableAdapter.Fill(InformationDataSet.Instance.Specialty, HospitalUtil.getHospitalId());
                //ProgramUtil.closeWaitDialog();
            }

            private bool checkPatient()
            {
                Instance.Validate();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (textEditFirstName.EditValue == DBNull.Value || textEditFirstName.EditValue == null || (textEditFirstName.EditValue = textEditFirstName.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Нэрийг оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditFirstName.ErrorText = errorText;
                    isRight = false;
                    textEditFirstName.Focus();
                }
                if (textEditLastName.EditValue == DBNull.Value || textEditLastName.EditValue == null || (textEditLastName.EditValue = textEditLastName.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Эцэг эхийн нэрийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditLastName.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditLastName.Focus();
                    }
                }
                //if (textEditSirName.EditValue == DBNull.Value || textEditSirName.EditValue == null || (textEditSirName.EditValue = textEditSirName.EditValue.ToString().Trim()).Equals(""))
                //{
                //    errorText = "Ургийн овгийг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    textEditSirName.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        isRight = false;
                //        textEditSirName.Focus();
                //    }
                //}
                if (textEditRegister.EditValue == DBNull.Value || textEditRegister.EditValue == null || (textEditRegister.EditValue = textEditRegister.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Регистерийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditRegister.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditRegister.Focus();
                    }
                }
                //if (dateEditBirthday.EditValue == DBNull.Value || dateEditBirthday.EditValue == null)
                //{
                //    errorText = "Төрсөн өдрийг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    dateEditBirthday.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        isRight = false;
                //        dateEditBirthday.Focus();
                //    }
                //}
                //if (gridLookUpEditEducation.EditValue == DBNull.Value || gridLookUpEditEducation.EditValue == null || gridLookUpEditEducation.EditValue.ToString().Equals(""))
                //{
                //    errorText = "Боловсролыг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    gridLookUpEditEducation.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        xtraTabControl.SelectedTabPage = xtraTabPageUser;
                //        isRight = false;
                //        gridLookUpEditEducation.Focus();
                //    }
                //}
                //if (gridLookUpEditOrganization.EditValue == DBNull.Value || gridLookUpEditOrganization.EditValue == null || gridLookUpEditOrganization.EditValue.ToString().Equals(""))
                //{
                //    errorText = "Ажлын газрыг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    gridLookUpEditOrganization.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        xtraTabControl.SelectedTabPage = xtraTabPageUser;
                //        isRight = false;
                //        gridLookUpEditOrganization.Focus();
                //    }
                //}
                //if (gridLookUpEditPosition.EditValue == DBNull.Value || gridLookUpEditPosition.EditValue == null || gridLookUpEditPosition.EditValue.ToString().Equals(""))
                //{
                //    errorText = "Албан тушаалыг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    gridLookUpEditPosition.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        xtraTabControl.SelectedTabPage = xtraTabPageUser;
                //        isRight = false;
                //        gridLookUpEditPosition.Focus();
                //    }
                //}
                //if (treeListLookUpEditCountry.EditValue == DBNull.Value || treeListLookUpEditCountry.EditValue == null || treeListLookUpEditCountry.EditValue.ToString().Equals(""))
                //{
                //    errorText = "Байршлыг оруулна уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    treeListLookUpEditCountry.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        xtraTabControl.SelectedTabPage = xtraTabPageAddress;
                //        isRight = false;
                //        treeListLookUpEditCountry.Focus();
                //    }
                //}

                //if (isRight && (actionType == 1 || actionType == 3) && !isDownload)
                //{
                //    isRight = checkRegister();
                //}
                //else 
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private bool checkRegister()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                int exist = Convert.ToInt32(InformationDataSet.QueriesTableAdapter.exist_user_id(textEditRegister.EditValue.ToString()));
                if (exist == 4)
                {
                    errorText = "Энэ регистертэй эмчлүүлэгч бүртгэсэн байна";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditRegister.ErrorText = errorText;
                    isRight = false;
                    textEditRegister.Focus();
                }
                else if (exist == 5)
                {
                    errorText = "Энэ регистертэй эмчлүүлэгч бүртгэсэн бөгөөд идэвхигүй болгосон байна";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditRegister.ErrorText = errorText;
                    isRight = false;
                    textEditRegister.Focus();
                }
                else if (exist == 2 || exist == 3)
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ регистертей хэрэглэгч бүртгэсэн байна. Хэрэглэгчийн мэдээллийг татах уу?", "Хэрэглэгчийн мэдээлэл татах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        InformationDataSet.UserInfoTableAdapter.Fill(InformationDataSet.Instance.UserInfo, HospitalUtil.getHospitalId(), textEditRegister.Text);
                        if (InformationDataSet.Instance.UserInfo.Rows.Count > 0)
                        {
                            DataRow row = InformationDataSet.Instance.UserInfo.Rows[0];
                            PatientForm.Instance.currView["firstName"] = row["firstName"];
                            PatientForm.Instance.currView["lastName"] = row["lastName"];
                            PatientForm.Instance.currView["sirName"] = row["sirName"];
                            PatientForm.Instance.currView["emdNumber"] = row["emdNumber"];
                            PatientForm.Instance.currView["nddNumber"] = row["nddNumber"];
                            PatientForm.Instance.currView["phone"] = row["phone"];
                            PatientForm.Instance.currView["email"] = row["email"];
                            PatientForm.Instance.currView["address"] = row["address"];
                            PatientForm.Instance.currView["birthday"] = row["birthday"];
                            PatientForm.Instance.currView["education"] = row["education"];
                            PatientForm.Instance.currView["organizationID"] = row["organizationID"];
                            PatientForm.Instance.currView["positionID"] = row["positionID"];
                            PatientForm.Instance.currView["specialtyID"] = row["specialtyID"];
                            PatientForm.Instance.currView["provinceID"] = row["provinceID"];
                            PatientForm.Instance.currView["gender"] = row["gender"];
                            PatientForm.Instance.currView["isMarried"] = row["isMarried"];
                            PatientForm.Instance.currView["picture"] = row["picture"];
                            PatientForm.Instance.currView.EndEdit();
                            isDownload = true;
                        }
                        else
                            isRight = false;
                    }
                    else
                    {
                        errorText = "Энэ регистертэй хэрэглэгч бүртгэсэн байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        textEditRegister.ErrorText = errorText;
                        textEditRegister.Focus();
                        isRight = false;
                    }
                }
                else if (exist == 6)
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ регистертей хүн бүртгэсэн байна. Хэрэглэгчийн мэдээллийг татах уу?", "Хэрэглэгчийн мэдээлэл татах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        InformationDataSet.UserInfoTableAdapter.Fill(InformationDataSet.Instance.UserInfo, HospitalUtil.getHospitalId(), textEditRegister.Text);
                        if (InformationDataSet.Instance.UserInfo.Rows.Count > 0)
                        {
                            DataRow row = InformationDataSet.Instance.UserInfo.Rows[0];
                            PatientForm.Instance.currView["firstName"] = row["firstName"];
                            PatientForm.Instance.currView["lastName"] = row["lastName"];
                            PatientForm.Instance.currView["sirName"] = row["sirName"];
                            PatientForm.Instance.currView["emdNumber"] = row["emdNumber"];
                            PatientForm.Instance.currView["nddNumber"] = row["nddNumber"];
                            PatientForm.Instance.currView["phone"] = row["phone"];
                            PatientForm.Instance.currView["email"] = row["email"];
                            PatientForm.Instance.currView["address"] = row["address"];
                            PatientForm.Instance.currView["birthday"] = row["birthday"];
                            PatientForm.Instance.currView["education"] = row["education"];
                            PatientForm.Instance.currView["organizationID"] = row["organizationID"];
                            PatientForm.Instance.currView["positionID"] = row["positionID"];
                            PatientForm.Instance.currView["specialtyID"] = row["specialtyID"];
                            PatientForm.Instance.currView["provinceID"] = row["provinceID"];
                            PatientForm.Instance.currView["gender"] = row["gender"];
                            PatientForm.Instance.currView["isMarried"] = row["isMarried"];
                            PatientForm.Instance.currView["picture"] = row["picture"];
                            PatientForm.Instance.currView.EndEdit();
                            isDownload = true;
                        }
                    }
                    else
                    {
                        errorText = "Энэ регистертэй хүн бүртгэсэн байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        textEditRegister.ErrorText = errorText;
                        textEditRegister.Focus();
                        isRight = false;
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void savePatient()
            {
                if (checkPatient())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    if (actionType == 1 || actionType == 2)
                    {
                        if (actionType == 1 || actionType == 2)
                        {
                            PatientForm.Instance.currView["isNewPatient"] = !isDownload;
                        }
                        PatientForm.Instance.currView["organizationName"] = gridLookUpEditOrganization.Text;
                        PatientForm.Instance.currView["positionName"] = gridLookUpEditPosition.Text;
                        PatientForm.Instance.currView["provinceName"] = treeListLookUpEditCountry.Text;
                        PatientForm.Instance.currView["specialtyName"] = gridLookUpEditSpecialty.Text;
                        PatientForm.Instance.currView.EndEdit();
                        InformationDataSet.PatientTableAdapter.Update(InformationDataSet.Instance.Patient);
                        addRow();         
                    }
                    else if (actionType == 3)
                    {
                        // Өөр цонхноос эмчүүлэгч бүртгэж байна.
                        InformationDataSet.Instance.Patient.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        InformationDataSet.Instance.Patient.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        DataRowView view = (DataRowView)InformationDataSet.PatientBindingSource.AddNew();
                        view["id"] = textEditID.EditValue;
                        view["firstName"] = textEditFirstName.EditValue;
                        view["lastName"] = textEditLastName.EditValue;
                        view["sirName"] = textEditSirName.EditValue;
                        view["register"] = textEditRegister.EditValue;
                        view["emdNumber"] = textEditEMM.EditValue;
                        view["nddNumber"] = textEditNDD.EditValue;
                        view["phone"] = textEditPhone.EditValue;
                        view["email"] = textEditEmail.EditValue;
                        view["address"] = memoEditAddress.EditValue;
                        view["birthday"] = dateEditBirthday.EditValue;
                        view["education"] = gridLookUpEditEducation.EditValue;
                        view["organizationID"] = gridLookUpEditOrganization.EditValue;
                        view["positionID"] = gridLookUpEditPosition.EditValue;
                        view["specialtyID"] = gridLookUpEditSpecialty.EditValue;
                        if (treeListLookUpEditCountry.EditValue != DBNull.Value && treeListLookUpEditCountry.EditValue != null)
                        {
                            view["provinceID"] = treeListLookUpEditCountry.EditValue;
                            view["provinceName"] = treeListLookUpEditCountry.Text;
                        }    
                        view["gender"] = radioGroupGender.EditValue;
                        view["isMarried"] = radioGroupIsMarried.EditValue;
                        view["isUse"] = radioGroupIsUse.EditValue;
                        view["picture"] = pictureEdit.EditValue;
                        view["organizationName"] = gridLookUpEditOrganization.Text;
                        view["positionName"] = gridLookUpEditPosition.Text;
                        view["specialtyName"] = gridLookUpEditSpecialty.Text;
                        view["isNewPatient"] = !isDownload;
                        view.EndEdit();
                        InformationDataSet.PatientTableAdapter.Update(InformationDataSet.Instance.Patient);

                        // Шинээр нэмсэн эмчүүлэгчийн мэдээллийг дамжуулж байна.
                        OrderInUpForm.Instance.addPatient(view);
                        Close();
                    }
                    else if (actionType == 4)
                    {
                        // Өөр цонхноос эмчүүлэгч бүртгэж байна.
                        InformationDataSet.Instance.Patient.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        InformationDataSet.Instance.Patient.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        DataRowView view = (DataRowView)InformationDataSet.PatientBindingSource.AddNew();
                        view["id"] = textEditID.EditValue;
                        view["firstName"] = textEditFirstName.EditValue;
                        view["lastName"] = textEditLastName.EditValue;
                        view["sirName"] = textEditSirName.EditValue;
                        view["register"] = textEditRegister.EditValue;
                        view["emdNumber"] = textEditEMM.EditValue;
                        view["nddNumber"] = textEditNDD.EditValue;
                        view["phone"] = textEditPhone.EditValue;
                        view["email"] = textEditEmail.EditValue;
                        view["address"] = memoEditAddress.EditValue;
                        view["birthday"] = dateEditBirthday.EditValue;
                        view["education"] = gridLookUpEditEducation.EditValue;
                        view["organizationID"] = gridLookUpEditOrganization.EditValue;
                        view["positionID"] = gridLookUpEditPosition.EditValue;
                        view["specialtyID"] = gridLookUpEditSpecialty.EditValue;
                        if (treeListLookUpEditCountry.EditValue != DBNull.Value && treeListLookUpEditCountry.EditValue != null)
                        {
                            view["provinceID"] = treeListLookUpEditCountry.EditValue;
                            view["provinceName"] = treeListLookUpEditCountry.Text;
                        }
                        view["gender"] = radioGroupGender.EditValue;
                        view["isMarried"] = radioGroupIsMarried.EditValue;
                        view["isUse"] = radioGroupIsUse.EditValue;
                        view["picture"] = pictureEdit.EditValue;
                        view["organizationName"] = gridLookUpEditOrganization.Text;
                        view["positionName"] = gridLookUpEditPosition.Text;                        
                        view["specialtyName"] = gridLookUpEditSpecialty.Text;
                        view["isNewPatient"] = !isDownload;
                        view.EndEdit();
                        InformationDataSet.PatientTableAdapter.Update(InformationDataSet.Instance.Patient);

                        // Шинээр нэмсэн эмчүүлэгчийн мэдээллийг дамжуулж байна.
                        DoctorUpForm.Instance.addPatient(view);
                        Close();
                    }
                    ProgramUtil.closeWaitDialog();
                }
            }


            // Шинээр нэмсэн мэргэжлийг авч байна
            public void insertSpecialty(string id)
            {
                gridLookUpEditSpecialty.EditValue = id;
            }

            // Шинээр нэмсэн албан тушаалыг авч байна
            public void insertPosition(string id)
            {
                gridLookUpEditPosition.EditValue = id;
            }

            // Шинээр нэмсэн ажлын газрыг авч байна
            public void insertOrganization(string id)
            {
                gridLookUpEditOrganization.EditValue = id;
            }

        #endregion                             

            
    }
}