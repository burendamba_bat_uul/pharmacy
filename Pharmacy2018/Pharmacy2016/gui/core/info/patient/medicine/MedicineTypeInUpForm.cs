﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.info.medicine
{
    /*
     * Эмийн ангилал нэмэх, засах цонх.
     * 
     */

    public partial class MedicineTypeInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineTypeInUpForm INSTANCE = new MedicineTypeInUpForm();

            public static MedicineTypeInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private MedicineTypeInUpForm()
            {
                
            }

            
            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                initBinding();
                reload();
            }

            private void reload()
            {
                if (UserDataSet.Instance.MedicineType.Rows.Count == 0)
                    UserDataSet.MedicineTypeTableAdapter.Fill(UserDataSet.Instance.MedicineType, HospitalUtil.getHospitalId());
            }

            private void initBinding()
            {
                textEditName.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineTypeBindingSource, "name", true));
                textEditAccount.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineTypeBindingSource, "account", true));
                gridLookUpEditIsSpecial.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineTypeBindingSource, "isSpecial", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineTypeBindingSource, "description", true));
                treeListLookUpEditMedicineType.DataBindings.Add(new Binding("EditValue", UserDataSet.MedicineTypeBindingSource, "parentId", true));
            }

            private void initDataSource()
            {
                treeListLookUpEditMedicineType.Properties.DataSource = UserDataSet.MedicineTypeBindingSource;
                gridLookUpEditIsSpecial.Properties.DataSource = UserDataSet.Instance.IsSpecial;
            }

            private void initError()
            {
                textEditAccount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditIsSpecial.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insertMedicineType();
                    }
                    else
                    {
                        editMedicineType();
                    }
                    ShowDialog(MedicineForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                clearErrorText();
                UserDataSet.MedicineTypeBindingSource.CancelEdit();
                UserDataSet.Instance.MedicineType.RejectChanges();
            }

            private void clearErrorText()
            {
                textEditName.ErrorText = "";
                textEditAccount.ErrorText = "";
                gridLookUpEditIsSpecial.ErrorText = "";
            }

            private void MedicineTypeInUpForm_Shown(object sender, EventArgs e)
            {
                treeListLookUpEditMedicineType.Focus();
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }

            private void insertMedicineType()
            {
                
            }

            private void editMedicineType()
            {

            }

        #endregion
        
        #region Формын event 
        
            private void treeListLookUpEditMedicineType_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditMedicineType.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        textEditName.Focus();
                    }
                }
            }

            private void gridLookUpEditIsSpecial_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditIsSpecial.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        memoEditDescription.Focus();
                    }
                }
            }

            private void saveButtonRegisterMedicineType_Click(object sender, EventArgs e)
            {
                saveMedicineType();
            }

        #endregion

        #region Формын функц.

            private bool checkMedicineType()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (textEditName.EditValue == null || textEditName.EditValue == DBNull.Value || textEditName.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийн ангилалын нэрийг оруулна уу";
                    textEditName.ErrorText = errorText;
                    isRight = false;
                    textEditName.Focus();
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                }
                if (textEditAccount.EditValue == null || textEditAccount.EditValue == DBNull.Value || textEditAccount.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийн ангилалын дансны дугаар оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditAccount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditAccount.Focus();
                    }
                }
                if (gridLookUpEditIsSpecial.EditValue == null || gridLookUpEditIsSpecial.EditValue == DBNull.Value || gridLookUpEditIsSpecial.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийн ангилалын шинж чанарыг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditIsSpecial.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditIsSpecial.Focus();
                    }
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);
                
               
                
                return isRight;
            }

            private void saveMedicineType() 
            {
                if (checkMedicineType())
                {
                    UserDataSet.MedicineTypeBindingSource.EndEdit();
                    UserDataSet.MedicineTypeTableAdapter.Update(UserDataSet.Instance.MedicineType);

                    clearData();
                    MedicineForm.Instance.setDefaultType();
                    MedicineForm.Instance.addDoubleType();
                }
            }

        #endregion       
    }
}