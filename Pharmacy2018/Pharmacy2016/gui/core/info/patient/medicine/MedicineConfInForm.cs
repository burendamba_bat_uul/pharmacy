﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.patient;
using Pharmacy2016.gui.core.journal.plan;

namespace Pharmacy2016.gui.core.info.medicine
{
    /*
     * Ажлын газар, албан тушаал, мэргэжил нэмэх, засах цонх.
     * 
     */

    public partial class MedicineConfInForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineConfInForm INSTANCE = new MedicineConfInForm();

            public static MedicineConfInForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private MedicineConfInForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    insertConf();

                    TopMost = true;
                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                textEditName.ErrorText = "";
                textEditName.Text = "";
            }

            private void insertConf()
            {
                if (actionType == 1)
                {
                    Text = "Савлагаа нэмэх";
                }
                else if (actionType == 2)
                {
                    Text = "Тун хэмжээ нэмэх";
                }
                else if (actionType == 3)
                {
                    Text = "Хэмжих нэгж нэмэх";
                }
            }
        
            private void UserConfInUpForm_Shown(object sender, EventArgs e)
            {
                textEditName.Focus();
            }

        #endregion

        #region Формын event

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if ((textEditName.EditValue = textEditName.Text.Trim()).Equals(""))
                {
                    errorText = "Нэрийг оруулна уу";
                    textEditName.ErrorText = errorText;
                    isRight = false;
                    textEditName.Focus();
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    string newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());

                    if (actionType == 1)
                    {
                        UserDataSet.Instance.MedicineShape.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.MedicineShape.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.MedicineShape.idColumn.DefaultValue = newID;
                        DataRowView view = (DataRowView)UserDataSet.MedicineShapeBindingSource.AddNew();
                        view["name"] = textEditName.Text;
                        view.EndEdit();
                        UserDataSet.MedicineShapeTableAdapter.Update(UserDataSet.Instance.MedicineShape);
                        if (actionType == 1)
                        {
                            MedicineInUpForm.Instance.addShape(newID);
                        }
                        else if (actionType == 2)
                        {
                            MedicineInUpForm.Instance.addUnit(newID);
                        }
                        else if (actionType == 3)
                        {
                            MedicineInUpForm.Instance.addQuantity(newID);
                        }
                    }
                    else if (actionType == 2)
                    {
                        UserDataSet.Instance.MedicineUnit.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.MedicineUnit.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.MedicineUnit.idColumn.DefaultValue = newID;
                        DataRowView view = (DataRowView)UserDataSet.MedicineUnitBindingSource.AddNew();
                        view["name"] = textEditName.Text;
                        view.EndEdit();
                        UserDataSet.MedicineUnitTableAdapter.Update(UserDataSet.Instance.MedicineUnit);
                        if (actionType == 1)
                        {
                            MedicineInUpForm.Instance.addShape(newID);
                        }
                        else if (actionType == 2)
                        {
                            MedicineInUpForm.Instance.addUnit(newID);
                            
                        }
                        else if (actionType == 3)
                        {
                            MedicineInUpForm.Instance.addQuantity(newID);
                        }
                    }
                    else if (actionType == 3)
                    {
                        UserDataSet.Instance.MedicineQuantity.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.MedicineQuantity.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        UserDataSet.Instance.MedicineQuantity.idColumn.DefaultValue = newID;
                        DataRowView view = (DataRowView)UserDataSet.MedicineQuantityBindingSource.AddNew();
                        view["name"] = textEditName.Text;
                        view.EndEdit();
                        UserDataSet.MedicineQuantityTableAdapter.Update(UserDataSet.Instance.MedicineQuantity);
                        if (actionType == 1)
                        {
                            MedicineInUpForm.Instance.addShape(newID);
                        }
                        else if (actionType == 2)
                        {
                            MedicineInUpForm.Instance.addUnit(newID);
                        }
                        else if (actionType == 3)
                        {
                            MedicineInUpForm.Instance.addQuantity(newID);
                        }
                    }
                    
                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }

        #endregion

    }
}