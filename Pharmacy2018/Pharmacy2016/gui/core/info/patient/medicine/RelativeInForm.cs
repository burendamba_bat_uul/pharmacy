﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;

namespace Pharmacy2016.gui.core.info.medicine
{
    public partial class RelativeInForm : DevExpress.XtraEditors.XtraForm
    {
   
        #region Форм анх ачааллах функц

            private static RelativeInForm INSTANCE = new RelativeInForm();

           public static RelativeInForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private DataRowView Relative;

            private bool isInit = false;

            private int actionType;

            private RelativeInForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;
             
                gridLookUpEditMedicine.Properties.DataSource = UserDataSet.MedicineBindingSource;
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                reload();
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    insertConf();

                    TopMost = true;
                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                gridLookUpEditMedicine.ErrorText = "";
                gridLookUpEditMedicine.EditValue= null;

                UserDataSet.MedicineRelativeBindingSource.CancelEdit();
                UserDataSet.Instance.MedicineRelatives.RejectChanges();
            }

            private void insertConf()
            {
                if (actionType == 1)
                {
                    insert();
                }
                else if (actionType == 2)
                {
                    update();
                }
            }
        
            private void RelativeInForm_Shown(object sender, EventArgs e)
            {
                gridLookUpEditMedicine.Focus();
            }

            private void insert()
            {
               
            }

            private void update()
            {
                //Relative = MedicineInUpForm.Instance.getSelectedRow();
                //if (Relative == null)
                //    return;
                //XtraMessageBox.Show(MedicineForm.Instance.currentMedicineView["id"] + "");
                //if (Relative["conMedicineID"] != MedicineForm.Instance.currentMedicineView["id"] && Relative["medicineID"] == MedicineForm.Instance.currentMedicineView["id"])
                 //   gridLookUpEditMedicine.EditValue = Relative["conMedicineID"];
                //else
                //    gridLookUpEditMedicine.EditValue = Relative["medcineID"];
            }

        #endregion

        #region Формын event

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }
        #endregion

        #region Формын функц

            private void reload()
            {
                if (UserDataSet.Instance.Medicine.Rows.Count == 0)
                    UserDataSet.MedicineTableAdapter.Fill(UserDataSet.Instance.Medicine, HospitalUtil.getHospitalId());
            }
            
            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals(""))
                {
                    errorText = "Нэрийг оруулна уу";
                    gridLookUpEditMedicine.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                }
                else if (gridLookUpEditMedicine.EditValue.Equals(MedicineForm.Instance.currentMedicineView["id"].ToString()))
                {
                    errorText = "Холбоотой эмийн нэр давхардсан байна";
                    gridLookUpEditMedicine.ErrorText = errorText;
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                    }
                }
                if (isRight && actionType != 2)
                {
                    Relative = (DataRowView)UserDataSet.MedicineRelativeBindingSource.Current;
                    DataRow[] row = UserDataSet.Instance.MedicineRelatives.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND medicineID = '" + MedicineForm.Instance.currentMedicineView["id"] + "'");
                    for (int i = 0; row != null && i < row.Length; i++)
                    {
                        if (gridLookUpEditMedicine.EditValue.ToString().Equals(row[i]["conMedicineID"].ToString()))
                        {
                            errorText = "Холбоотой эм бүртгэгдсэн байна";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            gridLookUpEditMedicine.ErrorText = errorText;
                            isRight = false;
                            gridLookUpEditMedicine.Focus();
                        }              
                    }
                    row = UserDataSet.Instance.MedicineRelatives.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND conMedicineID = '" + MedicineForm.Instance.currentMedicineView["id"] + "'");
                    for (int j = 0; row != null && j < row.Length; j++)
                    {
                        if (gridLookUpEditMedicine.EditValue.ToString().Equals(row[j]["medicineID"].ToString()))
                        {
                            errorText = "Холбоотой эм бүртгэгдсэн байна";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            gridLookUpEditMedicine.ErrorText = errorText;
                            isRight = false;
                            gridLookUpEditMedicine.Focus();
                            break;
                        }
                    }
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    try
                    {
                        if (actionType == 1)
                        {
                            DataRow[] Detial = UserDataSet.Instance.Medicine.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "'AND id = '" + gridLookUpEditMedicine.EditValue.ToString() + "'");
                            for (int i = 0; i < Detial.Length; i++)
                            {
                                DataRow row = UserDataSet.Instance.MedicRel.Rows.Add();
                                row["id"] = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                                row["conMedicineID"] = gridLookUpEditMedicine.EditValue;
                                row["name"] = Detial[i]["name"];
                                row["latinName"] = Detial[i]["latinName"];
                                row["typeName"] = Detial[i]["medicineTypeName"];
                                row["shape"] = Detial[i]["shapeName"];
                                row["unit"] = Detial[i]["unitName"];
                                row["quantity"] = Detial[i]["quantityName"];
                                row["validDate"] = Detial[i]["validDate"];
                                row["price"] = Detial[i]["price"];
                                row.EndEdit();
                            }
                        }
                        else if (actionType == 2)
                        {
                            DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                            DataRow[] edit = UserDataSet.Instance.MedicineRelatives.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "'AND id = '" + Relative["id"] + "' AND medicineID = '" + MedicineForm.Instance.currentMedicineView["id"] + "'");
                            for (int i = 0; i < edit.Length; i++)
                            {
                                DataRow row = UserDataSet.Instance.MedicRel.Rows.Add();
                                row["id"] = Relative["id"];
                                row["medicineID"] = MedicineForm.Instance.currentMedicineView["id"];
                                row["conMedicineID"] = gridLookUpEditMedicine.EditValue;
                                row["name"] = view["name"];
                                row["latinName"] = view["latinName"];
                                row["typeName"] = view["medicineTypeName"];
                                row["unit"] = view["unitName"];
                                row["shape"] = view["shapeName"];
                                row["quantity"] = view["quantityName"];
                                row["price"] = view["price"];
                                row["validDate"] = view["validDate"];
                                row.EndEdit();
                            }
                        }
                    } 
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                    }
                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }

        #endregion
   
    }
}