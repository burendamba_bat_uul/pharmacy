﻿namespace Pharmacy2016.gui.core.info.medicine
{
    partial class MedicineInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicineInUpForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonCopy = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.saveButtonRegisterMedicine = new DevExpress.XtraEditors.SimpleButton();
            this.closeButtonRegisterMedicine = new DevExpress.XtraEditors.SimpleButton();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.gridLookUpEditProducerCountry = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditProducer = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditUnit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.treeListLookUpEditType = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList2 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridLookUpEditShape = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEditParmaStandart = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditQuantity = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.textEditSerial = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.treeListLookUpEditTypeInter = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditPrice = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditPackage = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEditMedicineSeries = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.textEditRegisterID = new DevExpress.XtraEditors.TextEdit();
            this.textEditBTKUSID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.textEditBarcode = new DevExpress.XtraEditors.TextEdit();
            this.textEditLatinName = new DevExpress.XtraEditors.TextEdit();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditValidDate = new DevExpress.XtraEditors.DateEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemGridLookUpEditMedicine = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditProducerCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditProducer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditShape.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditParmaStandart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSerial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditTypeInter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPackage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMedicineSeries.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegisterID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBTKUSID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLatinName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditValidDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditValidDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditMedicine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonCopy);
            this.panelControl1.Controls.Add(this.simpleButtonReload);
            this.panelControl1.Controls.Add(this.saveButtonRegisterMedicine);
            this.panelControl1.Controls.Add(this.closeButtonRegisterMedicine);
            this.panelControl1.Controls.Add(this.xtraScrollableControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(514, 411);
            this.panelControl1.TabIndex = 2;
            // 
            // simpleButtonCopy
            // 
            this.simpleButtonCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCopy.Location = new System.Drawing.Point(265, 376);
            this.simpleButtonCopy.Name = "simpleButtonCopy";
            this.simpleButtonCopy.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCopy.TabIndex = 52;
            this.simpleButtonCopy.Text = "Хуулах";
            this.simpleButtonCopy.ToolTip = "Хуулах";
            this.simpleButtonCopy.Click += new System.EventHandler(this.simpleButtonCopy_Click);
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 376);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 23;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // saveButtonRegisterMedicine
            // 
            this.saveButtonRegisterMedicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveButtonRegisterMedicine.Location = new System.Drawing.Point(346, 376);
            this.saveButtonRegisterMedicine.Name = "saveButtonRegisterMedicine";
            this.saveButtonRegisterMedicine.Size = new System.Drawing.Size(75, 23);
            this.saveButtonRegisterMedicine.TabIndex = 21;
            this.saveButtonRegisterMedicine.Text = "Хадгалах";
            this.saveButtonRegisterMedicine.ToolTip = "Өгөгдлийг хадгалах";
            this.saveButtonRegisterMedicine.Click += new System.EventHandler(this.saveButtonRegisterMedicine_Click);
            // 
            // closeButtonRegisterMedicine
            // 
            this.closeButtonRegisterMedicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButtonRegisterMedicine.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButtonRegisterMedicine.Location = new System.Drawing.Point(427, 376);
            this.closeButtonRegisterMedicine.Name = "closeButtonRegisterMedicine";
            this.closeButtonRegisterMedicine.Size = new System.Drawing.Size(75, 23);
            this.closeButtonRegisterMedicine.TabIndex = 22;
            this.closeButtonRegisterMedicine.Text = "Гарах";
            this.closeButtonRegisterMedicine.ToolTip = "Гарах";
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.gridLookUpEditProducerCountry);
            this.xtraScrollableControl1.Controls.Add(this.gridLookUpEditProducer);
            this.xtraScrollableControl1.Controls.Add(this.gridLookUpEditUnit);
            this.xtraScrollableControl1.Controls.Add(this.treeListLookUpEditType);
            this.xtraScrollableControl1.Controls.Add(this.gridLookUpEditShape);
            this.xtraScrollableControl1.Controls.Add(this.textEditParmaStandart);
            this.xtraScrollableControl1.Controls.Add(this.labelControl19);
            this.xtraScrollableControl1.Controls.Add(this.gridLookUpEditQuantity);
            this.xtraScrollableControl1.Controls.Add(this.labelControl18);
            this.xtraScrollableControl1.Controls.Add(this.textEditSerial);
            this.xtraScrollableControl1.Controls.Add(this.labelControl17);
            this.xtraScrollableControl1.Controls.Add(this.treeListLookUpEditTypeInter);
            this.xtraScrollableControl1.Controls.Add(this.labelControl16);
            this.xtraScrollableControl1.Controls.Add(this.spinEditPrice);
            this.xtraScrollableControl1.Controls.Add(this.spinEditPackage);
            this.xtraScrollableControl1.Controls.Add(this.labelControl7);
            this.xtraScrollableControl1.Controls.Add(this.textEditMedicineSeries);
            this.xtraScrollableControl1.Controls.Add(this.labelControl15);
            this.xtraScrollableControl1.Controls.Add(this.labelControl14);
            this.xtraScrollableControl1.Controls.Add(this.labelControl12);
            this.xtraScrollableControl1.Controls.Add(this.textEditRegisterID);
            this.xtraScrollableControl1.Controls.Add(this.textEditBTKUSID);
            this.xtraScrollableControl1.Controls.Add(this.labelControl13);
            this.xtraScrollableControl1.Controls.Add(this.labelControl11);
            this.xtraScrollableControl1.Controls.Add(this.labelControl10);
            this.xtraScrollableControl1.Controls.Add(this.labelControl9);
            this.xtraScrollableControl1.Controls.Add(this.memoEditDescription);
            this.xtraScrollableControl1.Controls.Add(this.textEditBarcode);
            this.xtraScrollableControl1.Controls.Add(this.textEditLatinName);
            this.xtraScrollableControl1.Controls.Add(this.textEditName);
            this.xtraScrollableControl1.Controls.Add(this.labelControl8);
            this.xtraScrollableControl1.Controls.Add(this.labelControl6);
            this.xtraScrollableControl1.Controls.Add(this.labelControl5);
            this.xtraScrollableControl1.Controls.Add(this.labelControl4);
            this.xtraScrollableControl1.Controls.Add(this.labelControl3);
            this.xtraScrollableControl1.Controls.Add(this.labelControl2);
            this.xtraScrollableControl1.Controls.Add(this.labelControl1);
            this.xtraScrollableControl1.Controls.Add(this.dateEditValidDate);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(2, 2);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(510, 350);
            this.xtraScrollableControl1.TabIndex = 51;
            // 
            // gridLookUpEditProducerCountry
            // 
            this.gridLookUpEditProducerCountry.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditProducerCountry.EditValue = "";
            this.gridLookUpEditProducerCountry.Location = new System.Drawing.Point(183, 395);
            this.gridLookUpEditProducerCountry.Name = "gridLookUpEditProducerCountry";
            this.gridLookUpEditProducerCountry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditProducerCountry.Properties.DisplayMember = "name";
            this.gridLookUpEditProducerCountry.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.gridLookUpEditProducerCountry.Properties.NullText = "";
            this.gridLookUpEditProducerCountry.Properties.PopupSizeable = false;
            this.gridLookUpEditProducerCountry.Properties.ValueMember = "id";
            this.gridLookUpEditProducerCountry.Properties.View = this.gridView8;
            this.gridLookUpEditProducerCountry.Size = new System.Drawing.Size(222, 20);
            this.gridLookUpEditProducerCountry.TabIndex = 16;
            this.gridLookUpEditProducerCountry.EditValueChanged += new System.EventHandler(this.gridLookUpEditProducerCountry_EditValueChanged);
            this.gridLookUpEditProducerCountry.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditProducerCountry_KeyUp);
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn25});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ShowAutoFilterRow = true;
            this.gridView8.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Улсын нэр";
            this.gridColumn25.CustomizationCaption = "Улсын нэр";
            this.gridColumn25.FieldName = "name";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowMove = false;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 0;
            // 
            // gridLookUpEditProducer
            // 
            this.gridLookUpEditProducer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditProducer.EditValue = "";
            this.gridLookUpEditProducer.Location = new System.Drawing.Point(183, 421);
            this.gridLookUpEditProducer.Name = "gridLookUpEditProducer";
            this.gridLookUpEditProducer.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditProducer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditProducer.Properties.DisplayMember = "name";
            this.gridLookUpEditProducer.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.gridLookUpEditProducer.Properties.NullText = "";
            this.gridLookUpEditProducer.Properties.PopupSizeable = false;
            this.gridLookUpEditProducer.Properties.ValueMember = "id";
            this.gridLookUpEditProducer.Properties.View = this.gridView7;
            this.gridLookUpEditProducer.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditProducer_Properties_ButtonClick);
            this.gridLookUpEditProducer.Size = new System.Drawing.Size(222, 20);
            this.gridLookUpEditProducer.TabIndex = 17;
            this.gridLookUpEditProducer.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditProducer_KeyUp);
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn24});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.ShowAutoFilterRow = true;
            this.gridView7.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Үйлдвэрлэгч";
            this.gridColumn24.CustomizationCaption = "Үйлдвэрлэгчийн нэр";
            this.gridColumn24.FieldName = "name";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowMove = false;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 0;
            // 
            // gridLookUpEditUnit
            // 
            this.gridLookUpEditUnit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditUnit.EditValue = "";
            this.gridLookUpEditUnit.Location = new System.Drawing.Point(183, 157);
            this.gridLookUpEditUnit.Name = "gridLookUpEditUnit";
            this.gridLookUpEditUnit.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUnit.Properties.DisplayMember = "name";
            this.gridLookUpEditUnit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.gridLookUpEditUnit.Properties.NullText = "";
            this.gridLookUpEditUnit.Properties.PopupSizeable = false;
            this.gridLookUpEditUnit.Properties.ValueMember = "id";
            this.gridLookUpEditUnit.Properties.View = this.gridView6;
            this.gridLookUpEditUnit.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditUnit_Properties_ButtonClick);
            this.gridLookUpEditUnit.Size = new System.Drawing.Size(222, 20);
            this.gridLookUpEditUnit.TabIndex = 6;
            this.gridLookUpEditUnit.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditUnit_KeyUp);
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn23});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ShowAutoFilterRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Тун хэмжээ";
            this.gridColumn23.CustomizationCaption = "Тун хэмжээний нэр";
            this.gridColumn23.FieldName = "name";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowMove = false;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 0;
            // 
            // treeListLookUpEditType
            // 
            this.treeListLookUpEditType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditType.EditValue = "0";
            this.treeListLookUpEditType.Location = new System.Drawing.Point(183, 79);
            this.treeListLookUpEditType.Name = "treeListLookUpEditType";
            this.treeListLookUpEditType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditType.Properties.DisplayMember = "name";
            this.treeListLookUpEditType.Properties.NullText = "";
            this.treeListLookUpEditType.Properties.TreeList = this.treeList2;
            this.treeListLookUpEditType.Properties.ValueMember = "id";
            this.treeListLookUpEditType.Size = new System.Drawing.Size(222, 20);
            this.treeListLookUpEditType.TabIndex = 3;
            this.treeListLookUpEditType.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditType_KeyUp);
            // 
            // treeList2
            // 
            this.treeList2.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn3});
            this.treeList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList2.KeyFieldName = "id";
            this.treeList2.Location = new System.Drawing.Point(46, 174);
            this.treeList2.Name = "treeList2";
            this.treeList2.OptionsBehavior.EnableFiltering = true;
            this.treeList2.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList2.ParentFieldName = "parentId";
            this.treeList2.Size = new System.Drawing.Size(400, 200);
            this.treeList2.TabIndex = 0;
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "Эмийн  ангилал";
            this.treeListColumn3.CustomizationCaption = "Эмийн  ангилалын нэр";
            this.treeListColumn3.FieldName = "name";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.OptionsColumn.AllowMove = false;
            this.treeListColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraTreeList.FilterPopupMode.List;
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 0;
            // 
            // gridLookUpEditShape
            // 
            this.gridLookUpEditShape.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditShape.EditValue = "";
            this.gridLookUpEditShape.Location = new System.Drawing.Point(183, 131);
            this.gridLookUpEditShape.Name = "gridLookUpEditShape";
            this.gridLookUpEditShape.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditShape.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditShape.Properties.DisplayMember = "name";
            this.gridLookUpEditShape.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.gridLookUpEditShape.Properties.NullText = "";
            this.gridLookUpEditShape.Properties.PopupSizeable = false;
            this.gridLookUpEditShape.Properties.ValueMember = "id";
            this.gridLookUpEditShape.Properties.View = this.gridView5;
            this.gridLookUpEditShape.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditShape_Properties_ButtonClick);
            this.gridLookUpEditShape.Size = new System.Drawing.Size(222, 20);
            this.gridLookUpEditShape.TabIndex = 5;
            this.gridLookUpEditShape.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditShape_KeyUp);
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn22});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ShowAutoFilterRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Савлагаа";
            this.gridColumn22.CustomizationCaption = "Савлагааны нэр";
            this.gridColumn22.FieldName = "name";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowMove = false;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 0;
            // 
            // textEditParmaStandart
            // 
            this.textEditParmaStandart.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditParmaStandart.EnterMoveNextControl = true;
            this.textEditParmaStandart.Location = new System.Drawing.Point(183, 313);
            this.textEditParmaStandart.Name = "textEditParmaStandart";
            this.textEditParmaStandart.Properties.MaxLength = 50;
            this.textEditParmaStandart.Size = new System.Drawing.Size(222, 20);
            this.textEditParmaStandart.TabIndex = 13;
            // 
            // labelControl19
            // 
            this.labelControl19.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl19.Location = new System.Drawing.Point(51, 316);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(126, 13);
            this.labelControl19.TabIndex = 105;
            this.labelControl19.Text = "Фармакопейн стандарт :";
            // 
            // gridLookUpEditQuantity
            // 
            this.gridLookUpEditQuantity.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditQuantity.EditValue = "";
            this.gridLookUpEditQuantity.Location = new System.Drawing.Point(183, 183);
            this.gridLookUpEditQuantity.Name = "gridLookUpEditQuantity";
            this.gridLookUpEditQuantity.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditQuantity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditQuantity.Properties.DisplayMember = "name";
            this.gridLookUpEditQuantity.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.gridLookUpEditQuantity.Properties.NullText = "";
            this.gridLookUpEditQuantity.Properties.PopupSizeable = false;
            this.gridLookUpEditQuantity.Properties.ValueMember = "id";
            this.gridLookUpEditQuantity.Properties.View = this.gridView4;
            this.gridLookUpEditQuantity.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditQuantity_Properties_ButtonClick);
            this.gridLookUpEditQuantity.Size = new System.Drawing.Size(222, 20);
            this.gridLookUpEditQuantity.TabIndex = 7;
            this.gridLookUpEditQuantity.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditQuantity_KeyUp);
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Хэмжих нэгж";
            this.gridColumn5.FieldName = "name";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowMove = false;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            // 
            // labelControl18
            // 
            this.labelControl18.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl18.Location = new System.Drawing.Point(100, 186);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(77, 13);
            this.labelControl18.TabIndex = 104;
            this.labelControl18.Text = "Хэмжих нэгж* :";
            // 
            // textEditSerial
            // 
            this.textEditSerial.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditSerial.EnterMoveNextControl = true;
            this.textEditSerial.Location = new System.Drawing.Point(183, 235);
            this.textEditSerial.Name = "textEditSerial";
            this.textEditSerial.Properties.MaxLength = 50;
            this.textEditSerial.Size = new System.Drawing.Size(222, 20);
            this.textEditSerial.TabIndex = 10;
            // 
            // labelControl17
            // 
            this.labelControl17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl17.Location = new System.Drawing.Point(133, 238);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(44, 13);
            this.labelControl17.TabIndex = 103;
            this.labelControl17.Text = "Сериал :";
            // 
            // treeListLookUpEditTypeInter
            // 
            this.treeListLookUpEditTypeInter.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditTypeInter.EditValue = "0";
            this.treeListLookUpEditTypeInter.Location = new System.Drawing.Point(183, 105);
            this.treeListLookUpEditTypeInter.Name = "treeListLookUpEditTypeInter";
            this.treeListLookUpEditTypeInter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditTypeInter.Properties.DisplayMember = "name";
            this.treeListLookUpEditTypeInter.Properties.NullText = "";
            this.treeListLookUpEditTypeInter.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditTypeInter.Properties.ValueMember = "id";
            this.treeListLookUpEditTypeInter.Size = new System.Drawing.Size(222, 20);
            this.treeListLookUpEditTypeInter.TabIndex = 4;
            this.treeListLookUpEditTypeInter.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditTypeInter_KeyUp);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(66, 236);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.ParentFieldName = "parentId";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Эмийн  ангиллууд";
            this.treeListColumn2.FieldName = "name";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.OptionsColumn.AllowMove = false;
            this.treeListColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraTreeList.FilterPopupMode.List;
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // labelControl16
            // 
            this.labelControl16.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl16.Location = new System.Drawing.Point(66, 108);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(111, 13);
            this.labelControl16.TabIndex = 102;
            this.labelControl16.Text = "Олон улсын ангилал :";
            // 
            // spinEditPrice
            // 
            this.spinEditPrice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditPrice.EnterMoveNextControl = true;
            this.spinEditPrice.Location = new System.Drawing.Point(377, 209);
            this.spinEditPrice.Name = "spinEditPrice";
            this.spinEditPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditPrice.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.EditFormat.FormatString = "n2";
            this.spinEditPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.Mask.EditMask = "n2";
            this.spinEditPrice.Size = new System.Drawing.Size(100, 20);
            this.spinEditPrice.TabIndex = 50;
            this.spinEditPrice.Visible = false;
            // 
            // spinEditPackage
            // 
            this.spinEditPackage.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditPackage.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditPackage.EnterMoveNextControl = true;
            this.spinEditPackage.Location = new System.Drawing.Point(74, 369);
            this.spinEditPackage.Name = "spinEditPackage";
            this.spinEditPackage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditPackage.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditPackage.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPackage.Properties.EditFormat.FormatString = "n2";
            this.spinEditPackage.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPackage.Properties.Mask.EditMask = "n2";
            this.spinEditPackage.Size = new System.Drawing.Size(100, 20);
            this.spinEditPackage.TabIndex = 35;
            this.spinEditPackage.Visible = false;
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl7.Location = new System.Drawing.Point(5, 372);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(63, 13);
            this.labelControl7.TabIndex = 101;
            this.labelControl7.Text = "Тоо хэмжээ :";
            this.labelControl7.Visible = false;
            // 
            // textEditMedicineSeries
            // 
            this.textEditMedicineSeries.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditMedicineSeries.EnterMoveNextControl = true;
            this.textEditMedicineSeries.Location = new System.Drawing.Point(183, 261);
            this.textEditMedicineSeries.Name = "textEditMedicineSeries";
            this.textEditMedicineSeries.Properties.MaxLength = 50;
            this.textEditMedicineSeries.Size = new System.Drawing.Size(222, 20);
            this.textEditMedicineSeries.TabIndex = 11;
            // 
            // labelControl15
            // 
            this.labelControl15.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl15.Location = new System.Drawing.Point(100, 264);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(77, 13);
            this.labelControl15.TabIndex = 100;
            this.labelControl15.Text = "Эмийн цуврал :";
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl14.Location = new System.Drawing.Point(80, 212);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(97, 13);
            this.labelControl14.TabIndex = 99;
            this.labelControl14.Text = "Хүчинтэй хугацаа :";
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl12.Location = new System.Drawing.Point(296, 212);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(75, 13);
            this.labelControl12.TabIndex = 98;
            this.labelControl12.Text = " Нэгжийн үнэ*:";
            this.labelControl12.Visible = false;
            // 
            // textEditRegisterID
            // 
            this.textEditRegisterID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditRegisterID.EnterMoveNextControl = true;
            this.textEditRegisterID.Location = new System.Drawing.Point(183, 447);
            this.textEditRegisterID.Name = "textEditRegisterID";
            this.textEditRegisterID.Properties.MaxLength = 50;
            this.textEditRegisterID.Size = new System.Drawing.Size(222, 20);
            this.textEditRegisterID.TabIndex = 18;
            // 
            // textEditBTKUSID
            // 
            this.textEditBTKUSID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditBTKUSID.EnterMoveNextControl = true;
            this.textEditBTKUSID.Location = new System.Drawing.Point(183, 473);
            this.textEditBTKUSID.Name = "textEditBTKUSID";
            this.textEditBTKUSID.Properties.MaxLength = 50;
            this.textEditBTKUSID.Size = new System.Drawing.Size(222, 20);
            this.textEditBTKUSID.TabIndex = 19;
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl13.Location = new System.Drawing.Point(98, 476);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(79, 13);
            this.labelControl13.TabIndex = 95;
            this.labelControl13.Text = "БТКУС дугаар :";
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl11.Location = new System.Drawing.Point(74, 450);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(103, 13);
            this.labelControl11.TabIndex = 93;
            this.labelControl11.Text = "Бүртгэлийн дугаар :";
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl10.Location = new System.Drawing.Point(106, 424);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(71, 13);
            this.labelControl10.TabIndex = 89;
            this.labelControl10.Text = "Үйлдвэрлэгч :";
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl9.Location = new System.Drawing.Point(118, 398);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(59, 13);
            this.labelControl9.TabIndex = 88;
            this.labelControl9.Text = "Улсын нэр :";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.memoEditDescription.EnterMoveNextControl = true;
            this.memoEditDescription.Location = new System.Drawing.Point(183, 339);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(222, 50);
            this.memoEditDescription.TabIndex = 14;
            // 
            // textEditBarcode
            // 
            this.textEditBarcode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditBarcode.EnterMoveNextControl = true;
            this.textEditBarcode.Location = new System.Drawing.Point(183, 287);
            this.textEditBarcode.Name = "textEditBarcode";
            this.textEditBarcode.Properties.MaxLength = 50;
            this.textEditBarcode.Size = new System.Drawing.Size(222, 20);
            this.textEditBarcode.TabIndex = 12;
            // 
            // textEditLatinName
            // 
            this.textEditLatinName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditLatinName.EnterMoveNextControl = true;
            this.textEditLatinName.Location = new System.Drawing.Point(183, 53);
            this.textEditLatinName.Name = "textEditLatinName";
            this.textEditLatinName.Properties.MaxLength = 50;
            this.textEditLatinName.Size = new System.Drawing.Size(222, 20);
            this.textEditLatinName.TabIndex = 2;
            // 
            // textEditName
            // 
            this.textEditName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditName.EnterMoveNextControl = true;
            this.textEditName.Location = new System.Drawing.Point(183, 27);
            this.textEditName.Name = "textEditName";
            this.textEditName.Properties.MaxLength = 50;
            this.textEditName.Size = new System.Drawing.Size(222, 20);
            this.textEditName.TabIndex = 1;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(111, 160);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(66, 13);
            this.labelControl8.TabIndex = 78;
            this.labelControl8.Text = "Тун хэмжээ*:";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl6.Location = new System.Drawing.Point(119, 134);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(58, 13);
            this.labelControl6.TabIndex = 75;
            this.labelControl6.Text = "Савлагаа*:";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(130, 290);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(47, 13);
            this.labelControl5.TabIndex = 73;
            this.labelControl5.Text = "Бар код :";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(92, 82);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(85, 13);
            this.labelControl4.TabIndex = 71;
            this.labelControl4.Text = "Эмийн ангилал*:";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(128, 341);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(49, 13);
            this.labelControl3.TabIndex = 69;
            this.labelControl3.Text = "Тайлбар :";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(87, 56);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 13);
            this.labelControl2.TabIndex = 66;
            this.labelControl2.Text = "Олон улсын нэр*:";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(83, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(94, 13);
            this.labelControl1.TabIndex = 65;
            this.labelControl1.Text = "Худалдааны нэр*:";
            // 
            // dateEditValidDate
            // 
            this.dateEditValidDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditValidDate.EditValue = null;
            this.dateEditValidDate.Location = new System.Drawing.Point(183, 209);
            this.dateEditValidDate.Name = "dateEditValidDate";
            this.dateEditValidDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditValidDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditValidDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditValidDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditValidDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditValidDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditValidDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditValidDate.Properties.Mask.IgnoreMaskBlank = false;
            this.dateEditValidDate.Properties.MaxLength = 15;
            this.dateEditValidDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditValidDate.TabIndex = 9;
            this.dateEditValidDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditValidDate_KeyUp);
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // repositoryItemGridLookUpEditMedicine
            // 
            this.repositoryItemGridLookUpEditMedicine.AutoHeight = false;
            this.repositoryItemGridLookUpEditMedicine.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditMedicine.DisplayMember = "name";
            this.repositoryItemGridLookUpEditMedicine.Name = "repositoryItemGridLookUpEditMedicine";
            this.repositoryItemGridLookUpEditMedicine.PopupFormSize = new System.Drawing.Size(500, 300);
            this.repositoryItemGridLookUpEditMedicine.ValueMember = "id";
            this.repositoryItemGridLookUpEditMedicine.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Эмийн нэр";
            this.gridColumn13.CustomizationCaption = "Нэр";
            this.gridColumn13.FieldName = "name";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 0;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Латин нэр";
            this.gridColumn14.FieldName = "latinName";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Ангилал";
            this.gridColumn16.FieldName = "medicineTypeName";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 2;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Тун хэмжээ";
            this.gridColumn17.FieldName = "unitName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 3;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Хэлбэр";
            this.gridColumn18.FieldName = "shapeName";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 4;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Хэмжих  нэгж";
            this.gridColumn19.FieldName = "quantityName";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 5;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Нэгжийн үнэ";
            this.gridColumn20.FieldName = "price";
            this.gridColumn20.MinWidth = 75;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 6;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Хүчинтэй хугацаа";
            this.gridColumn21.FieldName = "validDate";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 7;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Тун хэмжээ";
            this.gridColumn3.FieldName = "name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowMove = false;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Улсын нэрс";
            this.gridColumn1.FieldName = "name";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowMove = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowAutoFilterRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListLookUpEdit1TreeList.KeyFieldName = "id";
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(1, 28);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsBehavior.PopulateServiceColumns = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.ParentFieldName = "parentId";
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Нэр";
            this.treeListColumn1.CustomizationCaption = "Эмийн нэр";
            this.treeListColumn1.FieldName = "name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraTreeList.FilterPopupMode.List;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 552;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Савлагаа";
            this.gridColumn2.FieldName = "name";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowMove = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Нэр";
            this.gridColumn6.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn6.FieldName = "name";
            this.gridColumn6.MinWidth = 100;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 100;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Тун";
            this.gridColumn11.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn11.FieldName = "unit";
            this.gridColumn11.MinWidth = 60;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 2;
            this.gridColumn11.Width = 66;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Үйлвэрлэгчдийн нэрс";
            this.gridColumn4.FieldName = "name";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowMove = false;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // MedicineInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 411);
            this.Controls.Add(this.panelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MedicineInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Эм бүртгэл";
            this.Shown += new System.EventHandler(this.MedicineInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.xtraScrollableControl1.ResumeLayout(false);
            this.xtraScrollableControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditProducerCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditProducer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditShape.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditParmaStandart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSerial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditTypeInter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPackage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMedicineSeries.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegisterID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBTKUSID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLatinName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditValidDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditValidDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditMedicine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.TextEdit textEditParmaStandart;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditQuantity;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit textEditSerial;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditTypeInter;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.SpinEdit spinEditPrice;
        private DevExpress.XtraEditors.SpinEdit spinEditPackage;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEditMedicineSeries;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit textEditRegisterID;
        private DevExpress.XtraEditors.TextEdit textEditBTKUSID;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.TextEdit textEditBarcode;
        private DevExpress.XtraEditors.TextEdit textEditLatinName;
        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditValidDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditMedicine;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.SimpleButton saveButtonRegisterMedicine;
        private DevExpress.XtraEditors.SimpleButton closeButtonRegisterMedicine;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditShape;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditType;
        private DevExpress.XtraTreeList.TreeList treeList2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUnit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditProducer;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditProducerCountry;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCopy;


    }
}