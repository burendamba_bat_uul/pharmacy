﻿namespace Pharmacy2016.gui.core.info.medicine
{
    partial class PackageInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PackageInUpForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.dateEditCreatedDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditProducer = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditIsUse = new DevExpress.XtraEditors.CheckEdit();
            this.labelControlIsUse = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControlQuantity = new DevExpress.XtraEditors.LabelControl();
            this.spinEditPrice = new DevExpress.XtraEditors.SpinEdit();
            this.gridLookUpEditMedicine = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPackage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditCount = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditSumPrice = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCreatedDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCreatedDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditProducer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsUse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSumPrice.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 326);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 17;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(297, 326);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 16;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(216, 326);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 15;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl7.Location = new System.Drawing.Point(67, 36);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(67, 13);
            this.labelControl7.TabIndex = 30;
            this.labelControl7.Text = "Багцын нэр*:";
            // 
            // textEditName
            // 
            this.textEditName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditName.EnterMoveNextControl = true;
            this.textEditName.Location = new System.Drawing.Point(140, 33);
            this.textEditName.Name = "textEditName";
            this.textEditName.Properties.MaxLength = 50;
            this.textEditName.Size = new System.Drawing.Size(200, 20);
            this.textEditName.TabIndex = 1;
            // 
            // dateEditCreatedDate
            // 
            this.dateEditCreatedDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditCreatedDate.EditValue = null;
            this.dateEditCreatedDate.Location = new System.Drawing.Point(140, 59);
            this.dateEditCreatedDate.Name = "dateEditCreatedDate";
            this.dateEditCreatedDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditCreatedDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditCreatedDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditCreatedDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditCreatedDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditCreatedDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditCreatedDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditCreatedDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditCreatedDate.TabIndex = 2;
            this.dateEditCreatedDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditCreatedDate_KeyUp);
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(93, 62);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(41, 13);
            this.labelControl3.TabIndex = 29;
            this.labelControl3.Text = "Огноо*:";
            // 
            // gridLookUpEditProducer
            // 
            this.gridLookUpEditProducer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditProducer.Location = new System.Drawing.Point(140, 295);
            this.gridLookUpEditProducer.Name = "gridLookUpEditProducer";
            this.gridLookUpEditProducer.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditProducer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditProducer.Properties.DisplayMember = "name";
            this.gridLookUpEditProducer.Properties.NullText = "";
            this.gridLookUpEditProducer.Properties.ValueMember = "id";
            this.gridLookUpEditProducer.Properties.View = this.gridView1;
            this.gridLookUpEditProducer.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditProducer_Properties_ButtonClick);
            this.gridLookUpEditProducer.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditProducer.TabIndex = 18;
            this.gridLookUpEditProducer.Visible = false;
            this.gridLookUpEditProducer.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditProducer_KeyUp);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn22,
            this.gridColumn23});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Нэр";
            this.gridColumn22.CustomizationCaption = "Бэлтгэн нийлүүлэгчийн нэр";
            this.gridColumn22.FieldName = "name";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 0;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Хөрвүүлэх код";
            this.gridColumn23.CustomizationCaption = "Хөрвүүлэх код";
            this.gridColumn23.FieldName = "conNo";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(29, 298);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(105, 13);
            this.labelControl2.TabIndex = 31;
            this.labelControl2.Text = "Бэлтгэн нийлүүлэгч :";
            this.labelControl2.Visible = false;
            // 
            // checkEditIsUse
            // 
            this.checkEditIsUse.EnterMoveNextControl = true;
            this.checkEditIsUse.Location = new System.Drawing.Point(140, 85);
            this.checkEditIsUse.Name = "checkEditIsUse";
            this.checkEditIsUse.Properties.Caption = "";
            this.checkEditIsUse.Size = new System.Drawing.Size(75, 19);
            this.checkEditIsUse.TabIndex = 4;
            // 
            // labelControlIsUse
            // 
            this.labelControlIsUse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlIsUse.Location = new System.Drawing.Point(59, 88);
            this.labelControlIsUse.Name = "labelControlIsUse";
            this.labelControlIsUse.Size = new System.Drawing.Size(75, 13);
            this.labelControlIsUse.TabIndex = 34;
            this.labelControlIsUse.Text = "Ашиглах эсэх :";
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(85, 112);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(49, 13);
            this.labelControl8.TabIndex = 36;
            this.labelControl8.Text = "Тайлбар :";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.memoEditDescription.EnterMoveNextControl = true;
            this.memoEditDescription.Location = new System.Drawing.Point(140, 110);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(200, 75);
            this.memoEditDescription.TabIndex = 5;
            // 
            // labelControlQuantity
            // 
            this.labelControlQuantity.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlQuantity.Location = new System.Drawing.Point(246, 220);
            this.labelControlQuantity.Name = "labelControlQuantity";
            this.labelControlQuantity.Size = new System.Drawing.Size(8, 13);
            this.labelControlQuantity.TabIndex = 47;
            this.labelControlQuantity.Text = "ш";
            // 
            // spinEditPrice
            // 
            this.spinEditPrice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditPrice.EnterMoveNextControl = true;
            this.spinEditPrice.Location = new System.Drawing.Point(140, 243);
            this.spinEditPrice.Name = "spinEditPrice";
            this.spinEditPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditPrice.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.EditFormat.FormatString = "n2";
            this.spinEditPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.Mask.EditMask = "n2";
            this.spinEditPrice.Size = new System.Drawing.Size(100, 20);
            this.spinEditPrice.TabIndex = 9;
            this.spinEditPrice.EditValueChanged += new System.EventHandler(this.spinEditPrice_EditValueChanged);
            // 
            // gridLookUpEditMedicine
            // 
            this.gridLookUpEditMedicine.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditMedicine.EditValue = "";
            this.gridLookUpEditMedicine.Location = new System.Drawing.Point(140, 191);
            this.gridLookUpEditMedicine.Name = "gridLookUpEditMedicine";
            this.gridLookUpEditMedicine.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditMedicine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMedicine.Properties.DisplayMember = "soloName";
            this.gridLookUpEditMedicine.Properties.NullText = "";
            this.gridLookUpEditMedicine.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditMedicine.Properties.ValueMember = "id";
            this.gridLookUpEditMedicine.Properties.View = this.gridView3;
            this.gridLookUpEditMedicine.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditMedicine.TabIndex = 6;
            this.gridLookUpEditMedicine.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditMedicine_ButtonClick);
            this.gridLookUpEditMedicine.EditValueChanged += new System.EventHandler(this.gridLookUpEditMedicine_EditValueChanged);
            this.gridLookUpEditMedicine.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditMedicine_KeyUp);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumnPackage,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn1,
            this.gridColumn21});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Нэр";
            this.gridColumn15.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn15.FieldName = "name";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "ОУ нэр /Тун хэмжээ/";
            this.gridColumn16.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn16.FieldName = "soloName";
            this.gridColumn16.MinWidth = 150;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 1;
            this.gridColumn16.Width = 150;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Ангилал";
            this.gridColumn17.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn17.FieldName = "medicineTypeName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 2;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Тун хэмжээ";
            this.gridColumn18.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn18.FieldName = "unitType";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 5;
            // 
            // gridColumnPackage
            // 
            this.gridColumnPackage.Caption = "Тоо хэмжээ";
            this.gridColumnPackage.FieldName = "package";
            this.gridColumnPackage.MinWidth = 70;
            this.gridColumnPackage.Name = "gridColumnPackage";
            this.gridColumnPackage.Visible = true;
            this.gridColumnPackage.VisibleIndex = 3;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Үнэ";
            this.gridColumn19.CustomizationCaption = "Үнэ";
            this.gridColumn19.DisplayFormat.FormatString = "n2";
            this.gridColumn19.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn19.FieldName = "price";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 6;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Бар код";
            this.gridColumn20.CustomizationCaption = "Бар код";
            this.gridColumn20.FieldName = "barcode";
            this.gridColumn20.MinWidth = 75;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 8;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Савлагаа";
            this.gridColumn1.CustomizationCaption = "Савлагаа";
            this.gridColumn1.FieldName = "shape";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 4;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Хүчинтэй хугацаа";
            this.gridColumn21.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn21.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.gridColumn21.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn21.FieldName = "validDate";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 7;
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl9.Location = new System.Drawing.Point(73, 194);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(61, 13);
            this.labelControl9.TabIndex = 45;
            this.labelControl9.Text = "Эмийн нэр*:";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(107, 246);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(27, 13);
            this.labelControl5.TabIndex = 40;
            this.labelControl5.Text = "Үнэ*:";
            // 
            // spinEditCount
            // 
            this.spinEditCount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditCount.EnterMoveNextControl = true;
            this.spinEditCount.Location = new System.Drawing.Point(140, 217);
            this.spinEditCount.Name = "spinEditCount";
            this.spinEditCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditCount.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditCount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.EditFormat.FormatString = "n2";
            this.spinEditCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.Mask.EditMask = "n2";
            this.spinEditCount.Size = new System.Drawing.Size(100, 20);
            this.spinEditCount.TabIndex = 8;
            this.spinEditCount.EditValueChanged += new System.EventHandler(this.spinEditCount_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(68, 220);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(66, 13);
            this.labelControl4.TabIndex = 51;
            this.labelControl4.Text = "Тоо хэмжээ*:";
            // 
            // spinEditSumPrice
            // 
            this.spinEditSumPrice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditSumPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditSumPrice.EnterMoveNextControl = true;
            this.spinEditSumPrice.Location = new System.Drawing.Point(140, 269);
            this.spinEditSumPrice.Name = "spinEditSumPrice";
            this.spinEditSumPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditSumPrice.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditSumPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditSumPrice.Properties.EditFormat.FormatString = "n2";
            this.spinEditSumPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditSumPrice.Properties.Mask.EditMask = "n2";
            this.spinEditSumPrice.Properties.ReadOnly = true;
            this.spinEditSumPrice.Size = new System.Drawing.Size(100, 20);
            this.spinEditSumPrice.TabIndex = 20;
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl6.Location = new System.Drawing.Point(105, 272);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(29, 13);
            this.labelControl6.TabIndex = 53;
            this.labelControl6.Text = "Дүн*:";
            // 
            // PackageInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.spinEditSumPrice);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.spinEditCount);
            this.Controls.Add(this.labelControlQuantity);
            this.Controls.Add(this.spinEditPrice);
            this.Controls.Add(this.gridLookUpEditMedicine);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.memoEditDescription);
            this.Controls.Add(this.labelControlIsUse);
            this.Controls.Add(this.checkEditIsUse);
            this.Controls.Add(this.gridLookUpEditProducer);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.textEditName);
            this.Controls.Add(this.dateEditCreatedDate);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonSave);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PackageInUpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Багц нэмэх";
            this.Shown += new System.EventHandler(this.PackageInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCreatedDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditCreatedDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditProducer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsUse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSumPrice.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraEditors.DateEdit dateEditCreatedDate;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditProducer;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit checkEditIsUse;
        private DevExpress.XtraEditors.LabelControl labelControlIsUse;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.LabelControl labelControlQuantity;
        private DevExpress.XtraEditors.SpinEdit spinEditPrice;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMedicine;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPackage;
        private DevExpress.XtraEditors.SpinEdit spinEditCount;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SpinEdit spinEditSumPrice;
        private DevExpress.XtraEditors.LabelControl labelControl6;
    }
}