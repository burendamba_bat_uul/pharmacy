﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.patient;
using Pharmacy2016.gui.core.journal.inc;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.gui.core.journal.tender;
using Pharmacy2016.gui.core.journal.order;

namespace Pharmacy2016.gui.core.info.medicine
{
    /*
     * Бэлтгэн нийлүүлэгч нэмэх цонх.
     * 
     */

    public partial class ProducerInForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static ProducerInForm INSTANCE = new ProducerInForm();

            public static ProducerInForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int formType;

            private ProducerInForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                gridLookUpEditProducerCountry.Properties.DataSource = UserDataSet.ProducerCountryBindingSource;
                gridLookUpEditProducerCountry.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                reload();
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int fType, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    formType = fType;
                    if (formType == 1)
                    {
                        Text = "Бэлтгэн нийлүүлэгч нэмэх";
                    }
                    else if( formType == 2)
                    {
                        Text = "Үйлдвэрлэгч нэмэх";
                    }
                    else if (formType == 3)
                    {
                        Text = "Бэлтгэн нийлүүлэгч";
                    }
                    else if (formType == 4)
                    {
                        Text = "Тендерт оролцогч";
                    }
                    else if (formType == 5)
                    {
                        Text = "Бэлтгэн нийлүүлэгч нэмэх";
                    }

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                textEditName.ErrorText = "";
                textEditName.Text = "";
                gridLookUpEditProducerCountry.ErrorText = "";
                gridLookUpEditProducerCountry.EditValue = null;
            }

            private void UserConfInUpForm_Shown(object sender, EventArgs e)
            {
                gridLookUpEditProducerCountry.Focus();
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }

        #endregion

        #region Формын event

            private void gridLookUpEditProducerCountry_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditProducerCountry.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        textEditName.Focus();
                    }
                }
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private void reload()
            {
                UserDataSet.ProducerCountryTableAdapter.Fill(UserDataSet.Instance.ProducerCountry);
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditProducerCountry.EditValue == DBNull.Value || gridLookUpEditProducerCountry.EditValue == null || gridLookUpEditProducerCountry.EditValue.ToString().Equals(""))
                {
                    errorText = "Улсыг сонгоно уу";
                    gridLookUpEditProducerCountry.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditProducerCountry.Focus();
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                }
                if ((textEditName.EditValue = textEditName.Text.Trim()).Equals(""))
                {
                    errorText = "Нэрийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditName.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    string newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());

                    UserDataSet.Instance.Producer.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    UserDataSet.Instance.Producer.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                    UserDataSet.Instance.Producer.idColumn.DefaultValue = newID;
                    DataRowView view = (DataRowView)UserDataSet.ProducerBindingSource.AddNew();
                    view["countryID"] = gridLookUpEditProducerCountry.EditValue;
                    view["name"] = textEditName.Text;
                    view["countryName"] = gridLookUpEditProducerCountry.Text;
                    view.EndEdit();
                    UserDataSet.ProducerTableAdapter.Update(UserDataSet.Instance.Producer);

                    if (formType == 1)
                    {
                        MedicineIncUpForm.Instance.addProducer(newID);               
                    }
                    else if (formType == 2)
                    {
                        MedicineInUpForm.Instance.addProducer(newID, Convert.ToInt32(gridLookUpEditProducerCountry.EditValue));
                    }
                    else if (formType == 3)
                    {
                        PackageInUpForm.Instance.addProducer(newID);
                    }
                    else if (formType == 4)
                    {
                        TenderUpForm.Instance.addProducer(newID);
                    }
                    else if (formType == 5)
                    {
                        MedicineOrderUpForm.Instance.addProducer(newID);
                    }

                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }

        #endregion         

    }
}