﻿namespace Pharmacy2016.gui.core.main
{
    partial class AmountBalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AmountBalForm));
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLeft = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.treeListWard = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnWard = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridControlBal = new DevExpress.XtraGrid.GridControl();
            this.gridViewBal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBandIncCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBandIncMotCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBandExpCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBandExpMotCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBandExpPatientCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnDocumentId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnOrgId = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnOrdId2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnDtAccount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnCrAccount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnDocumentComment = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnAmount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemWard = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCustomize = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            this.dockPanelLeft.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLeft});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanelLeft
            // 
            this.dockPanelLeft.Controls.Add(this.dockPanel1_Container);
            this.dockPanelLeft.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelLeft.ID = new System.Guid("32690437-4757-43e6-b1e2-053573c2cee2");
            this.dockPanelLeft.Location = new System.Drawing.Point(0, 0);
            this.dockPanelLeft.Name = "dockPanelLeft";
            this.dockPanelLeft.Options.ShowCloseButton = false;
            this.dockPanelLeft.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelLeft.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelLeft.SavedIndex = 0;
            this.dockPanelLeft.Size = new System.Drawing.Size(200, 461);
            this.dockPanelLeft.Text = "Тасаг";
            this.dockPanelLeft.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.treeListWard);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(192, 434);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // treeListWard
            // 
            this.treeListWard.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnWard});
            this.treeListWard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListWard.Location = new System.Drawing.Point(0, 0);
            this.treeListWard.Name = "treeListWard";
            this.treeListWard.BeginUnboundLoad();
            this.treeListWard.AppendNode(new object[] {
            "Хавдар судлалын үндэсний төв"}, -1);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 1"}, 0);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 2"}, 0);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 3"}, 0);
            this.treeListWard.EndUnboundLoad();
            this.treeListWard.OptionsBehavior.Editable = false;
            this.treeListWard.OptionsBehavior.ExpandNodeOnDrag = false;
            this.treeListWard.OptionsView.ShowColumns = false;
            this.treeListWard.OptionsView.ShowIndicator = false;
            this.treeListWard.Size = new System.Drawing.Size(192, 434);
            this.treeListWard.TabIndex = 0;
            // 
            // treeListColumnWard
            // 
            this.treeListColumnWard.Caption = "Тасаг";
            this.treeListColumnWard.FieldName = "ward";
            this.treeListColumnWard.MinWidth = 52;
            this.treeListColumnWard.Name = "treeListColumnWard";
            this.treeListColumnWard.OptionsColumn.ReadOnly = true;
            this.treeListColumnWard.Visible = true;
            this.treeListColumnWard.VisibleIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControlBal);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 461);
            this.panelControl1.TabIndex = 0;
            // 
            // gridControlBal
            // 
            this.gridControlBal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlBal.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlBal.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlBal.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlBal.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlBal.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlBal.Location = new System.Drawing.Point(2, 47);
            this.gridControlBal.MainView = this.gridViewBal;
            this.gridControlBal.Name = "gridControlBal";
            this.gridControlBal.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemDateEdit1});
            this.gridControlBal.Size = new System.Drawing.Size(780, 412);
            this.gridControlBal.TabIndex = 11;
            this.gridControlBal.UseEmbeddedNavigator = true;
            this.gridControlBal.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBal});
            // 
            // gridViewBal
            // 
            this.gridViewBal.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBandIncCount,
            this.gridBandIncMotCount,
            this.gridBandExpCount,
            this.gridBandExpMotCount,
            this.gridBandExpPatientCount,
            this.gridBand1});
            this.gridViewBal.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.gridColumnDate,
            this.bandedGridColumn1,
            this.gridColumnOrgId,
            this.gridColumnOrdId2,
            this.gridColumnDtAccount,
            this.bandedGridColumn9,
            this.gridColumnCrAccount,
            this.gridColumnAmount,
            this.gridColumnDocumentComment,
            this.gridColumnDocumentId});
            this.gridViewBal.GridControl = this.gridControlBal;
            this.gridViewBal.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "begBal", null, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "begSumPrice", null, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "endBal", null, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "endSumPrice", null, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incCount", null, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incSumPrice", null, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incMotCount", null, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incMotSumPrice", null, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalIncCount", null, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalIncSumPrice", null, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expCount", null, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expSumPrice", null, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expMotCount", null, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expMotSumPrice", null, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expPatientCount", null, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expPatientSumPrice", null, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalExpCount", null, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalExpSumPrice", null, "{0:c2}")});
            this.gridViewBal.Name = "gridViewBal";
            this.gridViewBal.OptionsBehavior.ReadOnly = true;
            this.gridViewBal.OptionsFind.AlwaysVisible = true;
            this.gridViewBal.OptionsFind.FindNullPrompt = "Хайх үг ээ оруулна уу";
            this.gridViewBal.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewBal.OptionsPrint.PrintFooter = false;
            this.gridViewBal.OptionsView.ShowAutoFilterRow = true;
            this.gridViewBal.OptionsView.ShowFooter = true;
            // 
            // gridBandIncCount
            // 
            this.gridBandIncCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandIncCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandIncCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandIncCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandIncCount.Caption = "Орлого";
            this.gridBandIncCount.CustomizationCaption = "Орлогын журнал";
            this.gridBandIncCount.Name = "gridBandIncCount";
            this.gridBandIncCount.Visible = false;
            this.gridBandIncCount.VisibleIndex = -1;
            this.gridBandIncCount.Width = 150;
            // 
            // gridBandIncMotCount
            // 
            this.gridBandIncMotCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandIncMotCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandIncMotCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandIncMotCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandIncMotCount.Caption = "Шилжүүлсэн орлого";
            this.gridBandIncMotCount.CustomizationCaption = "Шилжүүлсэн орлого";
            this.gridBandIncMotCount.Name = "gridBandIncMotCount";
            this.gridBandIncMotCount.Visible = false;
            this.gridBandIncMotCount.VisibleIndex = -1;
            this.gridBandIncMotCount.Width = 150;
            // 
            // gridBandExpCount
            // 
            this.gridBandExpCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandExpCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandExpCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandExpCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandExpCount.Caption = "Зарлага";
            this.gridBandExpCount.CustomizationCaption = "Зарлагын журнал";
            this.gridBandExpCount.Name = "gridBandExpCount";
            this.gridBandExpCount.Visible = false;
            this.gridBandExpCount.VisibleIndex = -1;
            this.gridBandExpCount.Width = 150;
            // 
            // gridBandExpMotCount
            // 
            this.gridBandExpMotCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandExpMotCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandExpMotCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandExpMotCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandExpMotCount.Caption = "Шилжүүлсэн зарлага";
            this.gridBandExpMotCount.CustomizationCaption = "Шилжүүлсэн зарлага";
            this.gridBandExpMotCount.Name = "gridBandExpMotCount";
            this.gridBandExpMotCount.Visible = false;
            this.gridBandExpMotCount.VisibleIndex = -1;
            this.gridBandExpMotCount.Width = 150;
            // 
            // gridBandExpPatientCount
            // 
            this.gridBandExpPatientCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandExpPatientCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandExpPatientCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandExpPatientCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandExpPatientCount.Caption = "Эмчлүүлэгчид олгосон";
            this.gridBandExpPatientCount.CustomizationCaption = "Эмчлүүлэгчид олгосон";
            this.gridBandExpPatientCount.Name = "gridBandExpPatientCount";
            this.gridBandExpPatientCount.Visible = false;
            this.gridBandExpPatientCount.VisibleIndex = -1;
            this.gridBandExpPatientCount.Width = 150;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Данс";
            this.gridBand1.Columns.Add(this.gridColumnDocumentId);
            this.gridBand1.Columns.Add(this.gridColumnDate);
            this.gridBand1.Columns.Add(this.bandedGridColumn1);
            this.gridBand1.Columns.Add(this.gridColumnOrgId);
            this.gridBand1.Columns.Add(this.gridColumnOrdId2);
            this.gridBand1.Columns.Add(this.gridColumnDtAccount);
            this.gridBand1.Columns.Add(this.gridColumnCrAccount);
            this.gridBand1.Columns.Add(this.gridColumnDocumentComment);
            this.gridBand1.Columns.Add(this.gridColumnAmount);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 691;
            // 
            // gridColumnDocumentId
            // 
            this.gridColumnDocumentId.Caption = "DOCUMENT_ID";
            this.gridColumnDocumentId.FieldName = "documentId";
            this.gridColumnDocumentId.Name = "gridColumnDocumentId";
            this.gridColumnDocumentId.Visible = true;
            this.gridColumnDocumentId.Width = 64;
            // 
            // gridColumnDate
            // 
            this.gridColumnDate.Caption = "Date_ID";
            this.gridColumnDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnDate.CustomizationCaption = "Гүйлгээний огноо";
            this.gridColumnDate.FieldName = "dateId";
            this.gridColumnDate.MinWidth = 75;
            this.gridColumnDate.Name = "gridColumnDate";
            this.gridColumnDate.Visible = true;
            this.gridColumnDate.Width = 81;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "Тасаг / Агуулах";
            this.bandedGridColumn1.CustomizationCaption = "Тасаг / Агуулах нэр";
            this.bandedGridColumn1.FieldName = "warehouseName";
            this.bandedGridColumn1.MinWidth = 75;
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.Width = 90;
            // 
            // gridColumnOrgId
            // 
            this.gridColumnOrgId.Caption = "ORG_ID";
            this.gridColumnOrgId.CustomizationCaption = "Тасаг / Агуулахаас холбогдох дугаар";
            this.gridColumnOrgId.FieldName = "orgId";
            this.gridColumnOrgId.MinWidth = 75;
            this.gridColumnOrgId.Name = "gridColumnOrgId";
            this.gridColumnOrgId.Visible = true;
            this.gridColumnOrgId.Width = 89;
            // 
            // gridColumnOrdId2
            // 
            this.gridColumnOrdId2.Caption = "ORG_ID2";
            this.gridColumnOrdId2.CustomizationCaption = "Бэлтгэн нийлүүлэгч (ORG_ID2)";
            this.gridColumnOrdId2.FieldName = "orgId2";
            this.gridColumnOrdId2.MinWidth = 75;
            this.gridColumnOrdId2.Name = "gridColumnOrdId2";
            this.gridColumnOrdId2.Visible = true;
            this.gridColumnOrdId2.Width = 85;
            // 
            // gridColumnDtAccount
            // 
            this.gridColumnDtAccount.Caption = "DT_ACCOUNT_CODE";
            this.gridColumnDtAccount.CustomizationCaption = "Орлогийн данс (DT_ACCOUNT_CODE)";
            this.gridColumnDtAccount.FieldName = "dtAccount";
            this.gridColumnDtAccount.MinWidth = 75;
            this.gridColumnDtAccount.Name = "gridColumnDtAccount";
            this.gridColumnDtAccount.Visible = true;
            this.gridColumnDtAccount.Width = 89;
            // 
            // gridColumnCrAccount
            // 
            this.gridColumnCrAccount.Caption = "CR_ACCOUNT_CODE";
            this.gridColumnCrAccount.CustomizationCaption = "Зарлагын данс";
            this.gridColumnCrAccount.FieldName = "crAccount";
            this.gridColumnCrAccount.MinWidth = 75;
            this.gridColumnCrAccount.Name = "gridColumnCrAccount";
            this.gridColumnCrAccount.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumnCrAccount.Visible = true;
            this.gridColumnCrAccount.Width = 88;
            // 
            // gridColumnDocumentComment
            // 
            this.gridColumnDocumentComment.Caption = "DOCUMENT_COMMENT";
            this.gridColumnDocumentComment.CustomizationCaption = "Баримтын тайлбар";
            this.gridColumnDocumentComment.FieldName = "documentComment";
            this.gridColumnDocumentComment.MinWidth = 75;
            this.gridColumnDocumentComment.Name = "gridColumnDocumentComment";
            this.gridColumnDocumentComment.Visible = true;
            this.gridColumnDocumentComment.Width = 88;
            // 
            // gridColumnAmount
            // 
            this.gridColumnAmount.Caption = "AMOUNT";
            this.gridColumnAmount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnAmount.CustomizationCaption = "Нийт дүн";
            this.gridColumnAmount.FieldName = "amount";
            this.gridColumnAmount.MinWidth = 75;
            this.gridColumnAmount.Name = "gridColumnAmount";
            this.gridColumnAmount.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumnAmount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "AMOUNT", "{0:c2}")});
            this.gridColumnAmount.Visible = true;
            this.gridColumnAmount.Width = 107;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "bandedGridColumn9";
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.Visible = true;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButtonLayout);
            this.panelControl2.Controls.Add(this.dateEditEnd);
            this.panelControl2.Controls.Add(this.dateEditStart);
            this.panelControl2.Controls.Add(this.dropDownButtonPrint);
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(780, 45);
            this.panelControl2.TabIndex = 1;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(747, 10);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 9;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(116, 13);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(100, 20);
            this.dateEditEnd.TabIndex = 2;
            this.dateEditEnd.ToolTip = "Дуусах огноо";
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(10, 13);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(100, 20);
            this.dateEditStart.TabIndex = 1;
            this.dateEditStart.ToolTip = "Эхлэх огноо";
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.DropDownControl = this.popupMenu;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(641, 11);
            this.dropDownButtonPrint.MenuManager = this.barManager;
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.barManager.SetPopupContextMenu(this.dropDownButtonPrint, this.popupMenu);
            this.dropDownButtonPrint.Size = new System.Drawing.Size(100, 23);
            this.dropDownButtonPrint.TabIndex = 7;
            this.dropDownButtonPrint.Text = "Хөрвүүлэх";
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemWard),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemCustomize),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "Хэвлэх";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 0;
            this.barButtonItemPrint.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.LargeGlyph")));
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemWard
            // 
            this.barButtonItemWard.Caption = "Тасгийн тайлан";
            this.barButtonItemWard.Description = "Тасгийн тайлан";
            this.barButtonItemWard.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemWard.Glyph")));
            this.barButtonItemWard.Id = 1;
            this.barButtonItemWard.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemWard.LargeGlyph")));
            this.barButtonItemWard.Name = "barButtonItemWard";
            this.barButtonItemWard.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemCustomize
            // 
            this.barButtonItemCustomize.Caption = "Тохиргоо";
            this.barButtonItemCustomize.Description = "Тохиргоо";
            this.barButtonItemCustomize.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCustomize.Glyph")));
            this.barButtonItemCustomize.Id = 2;
            this.barButtonItemCustomize.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCustomize.LargeGlyph")));
            this.barButtonItemCustomize.Name = "barButtonItemCustomize";
            this.barButtonItemCustomize.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemConvert
            // 
            this.barButtonItemConvert.Caption = "Хөрвүүлэх";
            this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
            this.barButtonItemConvert.Id = 3;
            this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
            this.barButtonItemConvert.Name = "barButtonItemConvert";
            this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemPrint,
            this.barButtonItemWard,
            this.barButtonItemCustomize,
            this.barButtonItemConvert});
            this.barManager.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 461);
            this.barDockControlBottom.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 461);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(784, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 461);
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(222, 11);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonReload.TabIndex = 5;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Сонгосон хэрэглэгчийн үлдэгдлийн мэдээллийг шүүж харах";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // AmountBalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "AmountBalForm";
            this.ShowInTaskbar = false;
            this.Text = "Өгөгдөл экспорт";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MedicineBalForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            this.dockPanelLeft.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLeft;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraTreeList.TreeList treeListWard;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnWard;
        private DevExpress.XtraGrid.GridControl gridControlBal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewBal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnOrgId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnCrAccount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnAmount;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnDtAccount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnDocumentComment;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemWard;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCustomize;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnOrdId2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandIncCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandIncMotCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandExpCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandExpMotCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandExpPatientCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnDocumentId;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
    }
}