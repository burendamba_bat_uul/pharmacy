﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Localization;
using DevExpress.XtraBars.Helpers;
using Pharmacy2016.util;
using Pharmacy2016.Properties;
using Pharmacy2016.gui.core.info.conf;
using Pharmacy2016.gui.core.store.order;
using Pharmacy2016.gui.core.journal.order;
using Pharmacy2016.gui.core.store.store;
using Pharmacy2016.gui.core.journal.inc;
using Pharmacy2016.gui.core.journal.exp;
using Pharmacy2016.gui.core.journal.mot;
using Pharmacy2016.gui.core.journal.patient;
using Pharmacy2016.gui.core.journal.bal;
using Pharmacy2016.gui.core.info.hospital;
using Pharmacy2016.gui.core.info.medicine;
using Pharmacy2016.gui.core.info.user;
using Pharmacy2016.gui.core.info.patient;
using Pharmacy2016.gui.core.info.warehouse;
using Pharmacy2016.gui.core.journal.census;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.gui.core.journal.tender;
using Pharmacy2016.gui.core.store.drug;
using System.IO;
using Pharmacy2016.gui.core.info.database;
using System.Data.SqlClient;
using Pharmacy2016.gui.core.info.ds;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using System.Reflection;
using AutoUpdater.Classes;
using System.Net;
using System.Diagnostics;

namespace Pharmacy2016.gui.core.main
{
    /*
     * Програм үндсэн цонх.
     * 
     */

    public partial class MainForm : DevExpress.XtraEditors.XtraForm, ISharpUpdatable
    {

        #region Програм анх ачааллах функц

            private static MainForm INSTANCE = new MainForm();

            public static MainForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private SharpUpdater updater;

            private bool isUpdated = false;

            public DevExpress.XtraEditors.ColorWheel.ColorWheelForm colorChooser;

            private MainForm()
            {

            }

            public void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                colorChooser = new DevExpress.XtraEditors.ColorWheel.ColorWheelForm();

                BarLocalizer.Active = new MyBarLocalizer();

                SkinHelper.InitSkinGallery(skinRibbonGalleryBarItem, true);
                SkinHelper.InitSkinPopupMenu(popupMenu);
                
                repositoryItemGridLookUpEdit1.DataSource = MainDBDataSet.MainDBBindingSource;
                if (SystemUtil.IsConnect)
                    barEditItemMainDB.EditValue = SystemUtil.CURR_DB;
                //if (Settings.Default["ApplicationSkinName"] != null && Settings.Default["ApplicationSkinName"] != DBNull.Value && !Settings.Default["ApplicationSkinName"].ToString().Equals(""))
                //    UserLookAndFeel.Default.SkinName = Settings.Default["ApplicationSkinName"].ToString();

                //if (Settings.Default["ApplicationColor"] != null && Settings.Default["ApplicationColor"] != DBNull.Value)
                //    UserLookAndFeel.Default.SkinMaskColor = (System.Drawing.Color)Settings.Default["ApplicationColor"];

                //if (Settings.Default["ApplicationColor2"] != null && Settings.Default["ApplicationColor2"] != DBNull.Value)
                //    UserLookAndFeel.Default.SkinMaskColor2 = (System.Drawing.Color)Settings.Default["ApplicationColor2"];
                hideMenu();
                RoleUtil.addMenu(ribbonControl);
            }

        #endregion

        #region Програм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!isInit)
                    {
                        initForm();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Үндсэн цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            public void showMenu()
            {
                //ribbonControl.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Show;
                //ribbonPageStore.Visible = true;
                barEditItemMainDB.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                ribbonPageJournal.Visible = true;
                ribbonPageRegister.Visible = true;
                //ribbonPageReport.Visible = true;
                ribbonPageConf.Visible = true;
                barButtonItemChangePass.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                barButtonItemLogOut.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                barButtonItemLogIn.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barButtonItemBackupDB.Visibility = (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never);
                barButtonItemNewDB.Visibility = (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never);
                barButtonItemLog.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                ribbonPageGroupAdmin.Visible = true;
                ribbonPageGroupView.Visible = true;
                setStoreMenu();
            }

            public void showLoginMenu()
            {
                //ribbonControl.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Show;
                ribbonPageStore.Visible = false;
                ribbonPageJournal.Visible = false;
                ribbonPageRegister.Visible = false;
                //ribbonPageReport.Visible = false;
                ribbonPageConf.Visible = true;
                barButtonItemChangePass.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barButtonItemLogOut.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barButtonItemLogIn.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                //barButtonItemLog.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                ribbonPageGroupAdmin.Visible = false;
                ribbonPageGroupView.Visible = true;
            }

            public void showLicenseMenu()
            {
                ribbonPageStore.Visible = false;
                ribbonPageJournal.Visible = false;
                ribbonPageRegister.Visible = false;
                //ribbonPageReport.Visible = false;
                ribbonPageConf.Visible = true;
                barButtonItemChangePass.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barButtonItemLogOut.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barButtonItemLogIn.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                //barButtonItemLog.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barButtonItemDBConf.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                ribbonPageGroupAdmin.Visible = false;
                ribbonPageGroupView.Visible = false;
            }

            public void showDbConfMenu()
            {
                //ribbonControl.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Show;
                ribbonPageStore.Visible = false;
                ribbonPageJournal.Visible = false;
                ribbonPageRegister.Visible = false;
                //ribbonPageReport.Visible = false;
                ribbonPageConf.Visible = true;
                barButtonItemChangePass.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barButtonItemLogOut.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barButtonItemLogIn.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                barButtonItemDBConf.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                ribbonPageGroupAdmin.Visible = false;
                //barButtonItemLog.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }

            public void hideMenu()
            {
                //ribbonControl.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Show;
                barEditItemMainDB.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                ribbonPageStore.Visible = false;
                ribbonPageJournal.Visible = false;
                ribbonPageRegister.Visible = false;
                //ribbonPageReport.Visible = false;
                ribbonPageConf.Visible = false;
            }

            public void setStoreMenu()
            {
                ribbonPageGroupStoreOrder.Visible = RoleUtil.getIsUseStoreOrder();
                ribbonPageGroupStoreDrug.Visible = RoleUtil.getIsUseStoreDrug();
                ribbonPageStore.Visible = RoleUtil.getIsUseStoreOrder() || RoleUtil.getIsUseStoreDrug();
            }

            private void MainForm_Shown(object sender, EventArgs e)
            {
                //SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(Pharmacy2016.Properties.Settings.Default.conMedicine);
                //string database = builder.InitialCatalog;
                //object ret = InformationDataSet.QueriesTableAdapter.is_read_only(database);
                //if (ret != DBNull.Value && ret != null)
                //{
                //    string msg = ret.ToString();
                //    XtraMessageBox.Show(msg.Substring(0, msg.Length - 2) + "");
                //    showDbConfMenu();
                //    ConfDBForm.Instance.showForm();
                //    return;
                //}

                /* UKA
                if (!SystemUtil.isLicense)
                {
                    LicenseForm.Instance.showForm();
                }
                else*/ 
                    if (SystemUtil.IsConnect)
                {
                    LoginForm.Instance.showForm();
                }
                else
                {
                    showDbConfMenu();
                    ConfDBForm.Instance.showForm();
                }
            }

            private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                if (isUpdated)
                {
                    /*FormCollection fc = Application.OpenForms;

                    foreach (Form frm in fc)
                    {
                        if (frm != null) frm.Close();
                    }*/
                    RoleUtil.closeForm();
                    UserUtil.clearUser();
                    e.Cancel = false;
                }
                else if (SystemUtil.IsConnect)
                {
                    DialogResult result = XtraMessageBox.Show("Та програмаас гарахыг зөвшөөрч байна уу?", "Програмыг хаах", MessageBoxButtons.OKCancel);
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        //Settings.Default["ApplicationSkinName"] = UserLookAndFeel.Default.SkinName;
                        //Settings.Default["ApplicationColor"] = UserLookAndFeel.Default.SkinMaskColor;
                        //Settings.Default["ApplicationColor2"] = UserLookAndFeel.Default.SkinMaskColor2;
                        //Settings.Default.Save();

                        ProgramUtil.EXIT(false);
                        e.Cancel = false;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    e.Cancel = false;
                }
            }

            public void setUserText()
            {
                barStaticItemUserName.Caption = UserUtil.getUserFullName();
                barStaticItemUserName.Description = UserUtil.getUserFullName();

                barStaticItemRole.Caption = UserUtil.getUserRoleName();
                barStaticItemRole.Description = UserUtil.getUserRoleName();

                barStaticItemWard.Caption = UserUtil.getUserWardName();
                barStaticItemWard.Description = UserUtil.getUserWardName();

                barStaticItemHospital.Caption = HospitalUtil.getHospitalName();
                barStaticItemHospital.Description = HospitalUtil.getHospitalName();

                barStaticItemValidDay.Caption = SystemUtil.LICENSE_VALID_DATE;
                barStaticItemValidDay.Description = SystemUtil.LICENSE_VALID_DATE;
            }

            public void clearUserText()
            {
                barStaticItemUserName.Caption = "Хэрэглэгч";
                barStaticItemUserName.Description = "Хэрэглэгч";

                barStaticItemRole.Caption = "Эрх";
                barStaticItemRole.Description = "Эрх";

                barStaticItemWard.Caption = "Тасаг";
                barStaticItemWard.Description = "Тасаг";

                barStaticItemHospital.Caption = "Эмнэлэг";
                barStaticItemHospital.Description = "Эмнэлэг";

                barStaticItemValidDay.Caption = "Хүчинтэй хугацаа";
                barStaticItemValidDay.Description = "Хүчинтэй хугацаа";
            }

            public void resetUserSkin()
            {
                UserLookAndFeel.Default.SkinName = "My Favorite Skin";
                colorChooser.ResetBackColor();
                colorChooser.ResetForeColor();
            }

        #endregion

        #region Програмын event

            private void barEditItemMainDB_EditValueChanged(object sender, EventArgs e)
            {
                SystemUtil.SELECTED_DB = barEditItemMainDB.EditValue.ToString();
                DataRow[] row = MainDBDataSet.Instance.MainDB.Select("name = '" + SystemUtil.SELECTED_DB + "'");
                SystemUtil.SELECTED_DB_READONLY = !Convert.ToBoolean(row[0]["isCurrDB"]);
                
                string[] lines = Pharmacy2016.Properties.Settings.Default.conMedicine.Split(';');
                Pharmacy2016.Properties.Settings.Default.conMedicine = lines[0] + ";" + "Initial Catalog=" + SystemUtil.SELECTED_DB + ";" + lines[2] + ";" + lines[3] + ";";
                Properties.Settings.Default.Save();
                ProgramUtil.changeConnection();

                for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
                {
                    if (Application.OpenForms[i] != SplashForm.Instance && Application.OpenForms[i] != MainForm.Instance)
                        Application.OpenForms[i].Close();
                }
            }

            private void repositoryItemGridLookUpEdit1View_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
            {
                //if (e.RowHandle >= 0)
                //    Console.WriteLine(e.CellValue + " = " + Convert.ToBoolean(repositoryItemGridLookUpEdit1.View.GetRowCellValue(e.RowHandle, "isCurrDB")));
                    //XtraMessageBox.Show(e.CellValue +" = "+Convert.ToBoolean(repositoryItemGridLookUpEdit1.View.GetRowCellValue(e.RowHandle, "isCurrDB")));
                //XtraMessageBox.Show(repositoryItemGridLookUpEdit1View.GetRowCellValue(e.RowHandle, "isCurrDB")+"");
                //if (Convert.ToBoolean(repositoryItemGridLookUpEdit1View.GetRowCellValue(e.RowHandle, "isCurrDB")))
                //    e.Appearance.BackColor = Color.Yellow;
            }

        #endregion
        
        #region Програм цэсэн дээр дарах функц

            #region Дугаар олгох цэс

                private void barButtonItemOrderNumber_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    OrderForm.Instance.showForm();
                }

                private void barButtonItemOrder_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    MedicineOrderForm.Instance.showForm();
                }

                private void barButtonItemPatientStore_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    StoreForm.Instance.showForm(0);
                }

                private void barButtonItemPatientWardRoom_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    RoomBedPatientForm.Instance.showForm();
                }

                private void barButtonItemDrug_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    DrugForm.Instance.showForm();
                }

                private void barButtonItemShareMed_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    DrugNurseForm.Instance.showForm();
                }

            #endregion

            #region Эмийн гүйлгээ цэс

                private void barButtonItemInc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    MedicineIncForm.Instance.showForm();
                }

                private void barButtonItemExp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    MedicineExpForm.Instance.showForm();
                }

                private void barButtonItemMot_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    MedicineMotForm.Instance.showForm();
                }

                private void barButtonItemMedicinePatient_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    MedicinePatientForm.Instance.showForm();
                }

                private void barButtonItemCensus_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    MedicineCensusForm.Instance.showForm();
                }

                private void barButtonItemBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    MedicineBalForm.Instance.showForm();
                }

                private void barButtonItemBegBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    MedicineBegBalForm.Instance.showForm();
                }

                private void barButtonItemPlan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    PlanForm.Instance.showForm();
                }

                private void barButtonItemTender_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    TenderForm.Instance.showForm();
                }

            #endregion

            #region Үндсэн бүртгэл цэс

                private void barButtonItemHospital_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    HospitalForm.Instance.showForm();
                }

                private void barButtonItemWardRoom_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    WardRegisterForm.Instance.showForm();
                }

                private void barButtonItemMedicine_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    MedicineForm.Instance.showForm();
                }

                private void barButtonItemWarehouse_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    WareHouseForm.Instance.showForm();
                }

                private void barButtonItemUser_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    UserForm.Instance.showForm();
                }

                private void barButtonItemPatient_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    PatientForm.Instance.showForm();
                }

                private void barButtonItemOrgPos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    UserConfForm.Instance.showForm();
                }

            #endregion

            #region Тайлан цэс

            #endregion

            #region Тохиргоо цэс

                private void barButtonItemChooseColor_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    colorChooser.ShowDialog(this);
                }

                private void barButtonItemDBConf_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    ConfDBForm.Instance.showForm();
                }

                private void barButtonItemLog_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    ProgramLogForm.Instance.showForm();
                }

                private void barButtonItemLicense_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    LicenseForm.Instance.showForm();
                }

                private void barButtonItemAmount_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    AmountBalForm.Instance.showForm();
                }

                private void barButtonItemBackupDB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Өгөгдлийн санг хадгалах уу", "Өгөгдлийн санг хадгалах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.BACKUP_DB_TITLE);

                        string conStr = Pharmacy2016.Properties.Settings.Default.conMedicine;
                        string conFolder = RoleUtil.getBackUpPath().ToString();
                        if (conFolder != null)
                        {
                            BackupService backup = new BackupService(conStr, conFolder);
                            IDbConnection connection = new System.Data.SqlClient.SqlConnection(conStr);
                            backup.backupDatabase(connection.Database);
                        }

                        ProgramUtil.closeWaitDialog();
                    }
                }

                private void barButtonItemNewDB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    //DialogResult dialogResult = XtraMessageBox.Show("Өгөгдлийн санг шинээр үүсгэх үү", "Өгөгдлийн санг үүсгэх", MessageBoxButtons.YesNo);
                    //if (dialogResult == DialogResult.Yes)
                    //{
                    //    ProgramUtil.showWaitDialog(ProgramUtil.CREATE_DB_TITLE);

                    //    string conStr = Pharmacy2016.Properties.Settings.Default.conMedicine;
                    //    CreateDB backup = new CreateDB(conStr);
                    //    backup.createDB();

                    //    ProgramUtil.closeWaitDialog();
                    //}
                    CreateDBForm.Instance.showForm(Instance);
                }

                private void barButtonItemLocation_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    WindowRoleForm.Instance.showForm();
                }

                private void barButtonItemChangePass_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    ChangePassForm.Instance.showForm(UserUtil.getUserId());
                }

                private void barButtonItemLogIn_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    LoginForm.Instance.showForm();
                }

                private void barButtonItemLogOut_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    ProgramUtil.LOGOUT();
                }

                private void barButtonItemExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
                {
                    MainForm.Instance.Close();
                }

            #endregion

        #endregion

        // Skin-г авах класс
        public class MyBarLocalizer : BarLocalizer
        {
            public override string GetLocalizedString(BarString id)
            {
                if (id == BarString.SkinCaptions)
                {
                    //Default value for BarString.SkinCaptions:
                    //"|DevExpress Style|Caramel|Money Twins|DevExpress Dark Style|iMaginary|Lilian|Black|Blue|Office 2010 Blue|Office 2010 Black|Office 2010 Silver|Office 2007 Blue|Office 2007 Black|Office 2007 Silver|Office 2007 Green|Office 2007 Pink|Seven|Seven Classic|Darkroom|McSkin|Sharp|Sharp Plus|Foggy|Dark Side|Xmas (Blue)|Springtime|Summer|Pumpkin|Valentine|Stardust|Coffee|Glass Oceans|High Contrast|Liquid Sky|London Liquid Sky|The Asphalt World|Blueprint|"
                    string defaultSkinCaptions = base.GetLocalizedString(id);
                    string newSkinCaptions = defaultSkinCaptions.Replace("|DevExpress Style|", "|My Favorite Skin|");
                    return newSkinCaptions;
                }
                return base.GetLocalizedString(id);
            }
        }

        private void barButtonItemUpdate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string new_ver = "";
            bool check_err = false;
            string check_updatefile = @"C:\Program Files (x86)\Siticom\PharmacyVer28\pharmacy.dat";
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    webClient.DownloadFile(new Uri("http://www.siticom.mn:8045/pharmacy/pharmacy.dat"), check_updatefile);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message.ToString());
                    check_err = true;
                }
            }
            if (!check_err)
            {
                new_ver = System.IO.File.ReadAllText(check_updatefile);
                System.IO.File.Delete(check_updatefile);
                if (Convert.ToInt32(new_ver) > Convert.ToInt32(Label_Version.Text))
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Програмын шинэчлэлтийг ажиллуулах уу? Хэрэв ажиллуулвал програм автоматаар хаагдах болохыг анхаарна уу!", "Програмын шинэчлэлт", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        Process.Start(@"C:\Program Files (x86)\Siticom\PharmacyVer28\Pharmacy_Updater.exe");
                       isUpdated = true;                        
                       MainForm.Instance.Close();                    
                    }
                }
                else
                {
                    XtraMessageBox.Show("Шинэ хувилбар гараагүй байна.");
                }
            }
                       
      
            /*try
            {
                updater = new SharpUpdater(this);
                updater.DoUpdate();
                isUpdated = true;
            }
            catch(Exception ex)
            {
                isUpdated = false;
                Console.WriteLine(ex.ToString());
            }
            */
        }

        #region Програмын шинэчлэл

        public string ApplicationName
        {
            get { return "Pharmacy2017"; }
        }

        public string ApplicationID
        {
            get { return "Pharmacy"; }
        }

        public Assembly ApplicationAssembly
        {
            get { return Assembly.GetExecutingAssembly(); }
        }

        public Icon ApplicationIcon
        {
            get { return this.Icon; }
        }

        public Uri UpdateXmlLocation
        {
            get { return new Uri("http://122.201.23.66:8081/Siticom/update.xml"); }
        }

        public Form Context
        {
            get { return this; }
        }

        #endregion 

        private void ribbonControl_Click(object sender, EventArgs e)
        {

        }        
    }
}