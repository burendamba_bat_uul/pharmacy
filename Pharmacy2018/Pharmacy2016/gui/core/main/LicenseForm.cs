﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using SKGL;
using SKM.V3;
using SKM.V3.Methods;
using SKM.V3.Models;

namespace Pharmacy2016.gui.core.main
{
    public partial class LicenseForm : DevExpress.XtraEditors.XtraForm
    {
        #region Програм анх ачааллах функц

            private static LicenseForm INSTANCE = new LicenseForm();

            public static LicenseForm Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private bool isLicense = false;

            private int loginCount = 0;

            private static readonly int MAX_LOGIN_COUNT = 3;

            private LicenseForm()
            {
            
            }

            private void initForm()
            {
                if (!this.isInit)
                {
                    InitializeComponent();
                    this.isInit = true;
                    Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                    textEditKey.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                }
            }

        #endregion

        #region Програм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    loginCount = 0;
                    isLicense = false;

                    ShowDialog(MainForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                textEditKey.Text = "";
                textEditKey.ErrorText = "";
                if (ProgramUtil.isLoginUser())
                {
                    MainForm.Instance.showMenu();
                }else
                {
                    if (!isLicense && !SystemUtil.isLicense)
                        MainForm.Instance.showLicenseMenu();
                    else if (!isLicense && SystemUtil.isLicense)
                        MainForm.Instance.showLoginMenu();
                    else
                    {
                        MainForm.Instance.showLoginMenu();
                        LoginForm.Instance.showForm();
                    }
                        
                }
            }

        #endregion

        #region Формын event

            private void btnSave_Click(object sender, EventArgs e)
            {
                validateLicense();
            }

            private void btnCheck_Click(object sender, EventArgs e)
            {
                isLicenseValid();
            }

        #endregion

        #region Формын функц

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                if ((textEditKey.Text = textEditKey.Text.Trim()).Equals(""))
                {
                    errorText = "Түлхүүр оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditKey.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditKey.Focus();
                    }
                }
                if(!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                
                return isRight;
            }

            private void validateLicense()
            {
                if (loginCount == MAX_LOGIN_COUNT)
                {
                    XtraMessageBox.Show("Олон удаа буруу лиценз оруулах гэж оролдсон тул програмыг хаалаа");
                    SystemUtil.IsConnect = false;
                    ProgramUtil.EXIT(true);
                }
                    /*
                else if (check())
                {
                    ProgramUtil.closeAlertDialog();
                    string Caption = "Анхааруулга";
                    loginCount++;
                    string token = SystemUtil.TOKEN;
                    int productID = SystemUtil.PRODUCT_ID;
                    string machineCode = SystemUtil.MACHINE_CODE;
                    string key = textEditKey.Text;
                    var license = new LicenseKey { ProductId = productID, Key = key };
                    string path = SystemUtil.LICENSE_PATH;
                    if (license.Refresh(token, true))
                    {
                        if (license.Created > DateTime.Now || license.Expires < DateTime.Now || !license.HasNotExpired().IsValid())
                        {
                            isLicense = false;
                            ProgramUtil.showAlertDialog(Instance, Caption, "Та системийн цагаа зөв тохируулаагүй эсвэл лицензийн хугацаа дууссан байна!");
                            return;
                        }
                        SystemUtil.LICENSE_VALID_DATE = "Хүчинтэй хугацаа: " + license.DaysLeft();
                        if (license.HasFeature(1)
                              .HasNotExpired()
                              .IsValid())
                        {
                            isLicense = true;
                        }
                        else if (license.HasNotExpired().IsValid())
                        {
                            isLicense = true;
                        }
                        else if (license.HasNotFeature(1).IsValid())
                        {
                            isLicense = true;
                        }
                        else if (!license.HasNotExpired().IsValid())
                        {
                            isLicense = false;
                            ProgramUtil.showAlertDialog(Instance, Caption, "Лицензийн хугацаа дууссан байна хэрэглэж болохгүй");
                            return;
                            //XtraMessageBox.Show("Лицензийн хугацаа дууссан байна хэрэглэж болохгүй");
                        }
                        var result = Key.Activate(token: token, parameters: new ActivateModel()
                        {
                            Key = key,
                            ProductId = productID,
                            Sign = true,
                            MachineCode = machineCode
                        });
                        if (result == null || result.Result == ResultType.Error)
                        {
                            //XtraMessageBox.Show("Лиценз аль хэдийн бүртгэгдсэн байна. Aлдаа: " + result.Message);
                            isLicense = false;
                            ProgramUtil.showAlertDialog(Instance, Caption, "Лиценз аль хэдийн бүртгэгдсэн байна. Aлдаа: " + result.Message);
                            return;
                        }
                        ProgramUtil.showAlertDialog(Instance, Caption, "Лиценз амжилттай сунгагдлаа" + " Pharmacy-ийн хугацаа дуусхад " + license.DaysLeft() + " өдөр үлдлээ.");
                        SystemUtil.isLicense = isLicense;
                        license.SaveToFile(path);
                        Close();
                    }
                    else
                    {
                        isLicense = false;
                        ProgramUtil.showAlertDialog(Instance, Caption, "Сервертэй холбогдож чадсангүй эсвэл оруулсан түлхүүр буруу байна!");
                        //XtraMessageBox.Show("сервертэй холбогдож чадсангүй эсвэл оруулсан түлхүүр буруу байна!");
                    }
                }
                */
            }

            private void isLicenseValid()
            {
                ProgramUtil.closeAlertDialog();
                string Caption = "";
                Caption = "Анхааруулга";
                string token = SystemUtil.TOKEN;
                int product = SystemUtil.PRODUCT_ID;
                string key = textEditKey.Text;
                if (check())
                {
                    var license = new LicenseKey { ProductId = product, Key = key };
                     if (license.Refresh(token, true))
                    {
                        if (license.HasFeature(1)
                              .HasNotExpired()
                              .IsValid())
                            //ProgramUtil.showAlertDialog(Instance, Caption, "Лиценз хүчинтэй байна." + " Pharmacy-ийн хугацаа дуусхад " + license.DaysLeft().ToString() + " өдөр үлдсэн байна.");
                            XtraMessageBox.Show("Лиценз хүчинтэй байна." + " Pharmacy-ийн хугацаа дуусхад " + license.DaysLeft().ToString() + " өдөр үлдсэн байна.");
                        else if (license.HasNotExpired().IsValid())
                            XtraMessageBox.Show("Лиценз хүчинтэй байна." + " Pharmacy-ийн хугацаа дуусхад " + license.DaysLeft().ToString() + " өдөр үлдсэн байна.");
                            //ProgramUtil.showAlertDialog(Instance, Caption, "Лиценз хүчинтэй байна.");
                        else
                            // XtraMessageBox.Show("Лицензийн хугацаа дууссан байна хэрэглэж болохгүй");
                            ProgramUtil.showAlertDialog(Instance, Caption, "Лицензийн хугацаа дууссан байна хэрэглэж болохгүй.");
                    }
                    else
                        ProgramUtil.showAlertDialog(Instance, Caption, "сервертэй холбогдож чадсангүй.");
                        //XtraMessageBox.Show("сервертэй холбогдож чадсангүй!");
                }
            }

        #endregion     
  
    }
}