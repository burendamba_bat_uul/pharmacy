﻿namespace Pharmacy2016.gui.core.main
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemChooseColor = new DevExpress.XtraBars.BarButtonItem();
            this.skinRibbonGalleryBarItem = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.barButtonItemOrder = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemMedicinePatient = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPatientStore = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemInc = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemExp = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemMot = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemBal = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemBegBal = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemMedicine = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHospital = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUser = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemWindowRole = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOrgPos = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemWardRoom = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPatient = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOrderNumber = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLogIn = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLogOut = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemExit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDBConf = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItemHospital = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemUserName = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemWard = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRole = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItemLog = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPatientWardRoom = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemChangePass = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemWarehouse = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCensus = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPlan = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTender = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDrug = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemBackupDB = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemNewDB = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemShareMed = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemMainDB = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnBegBalDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnIsCurrDB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItemLicense = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemValidDay = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItemUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemIntegration = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAmount = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageStore = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupStoreOrder = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupStoreDrug = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageJournal = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageRegister = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageReport = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageConf = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupView = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAdmin = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupLogin = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.xtraTabbedMdiManager = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.Label_Version = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl
            // 
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.barButtonItemChooseColor,
            this.skinRibbonGalleryBarItem,
            this.barButtonItemOrder,
            this.barButtonItemMedicinePatient,
            this.barButtonItemPatientStore,
            this.barButtonItemInc,
            this.barButtonItemExp,
            this.barButtonItemMot,
            this.barButtonItemBal,
            this.barButtonItemBegBal,
            this.barButtonItemMedicine,
            this.barButtonItemHospital,
            this.barButtonItemUser,
            this.barButtonItemWindowRole,
            this.barButtonItemOrgPos,
            this.barButtonItemWardRoom,
            this.barButtonItemPatient,
            this.barButtonItemOrderNumber,
            this.barButtonItemLogIn,
            this.barButtonItemLogOut,
            this.barButtonItemExit,
            this.barButtonItemDBConf,
            this.barStaticItemHospital,
            this.barStaticItemUserName,
            this.barStaticItemWard,
            this.barStaticItemRole,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barButtonItemLog,
            this.barButtonItemPatientWardRoom,
            this.barButtonItemChangePass,
            this.barButtonItemWarehouse,
            this.barButtonItemCensus,
            this.barButtonItemPlan,
            this.barButtonItemTender,
            this.barButtonItemDrug,
            this.barButtonItemBackupDB,
            this.barButtonItemNewDB,
            this.barButtonItemShareMed,
            this.barEditItemMainDB,
            this.barStaticItem4,
            this.barButtonItemLicense,
            this.barStaticItem5,
            this.barStaticItemValidDay,
            this.barButtonItemUpdate,
            this.barButtonItemIntegration,
            this.barButtonItemAmount});
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.MaxItemId = 31;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.PageAnimationLength = 1;
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageStore,
            this.ribbonPageJournal,
            this.ribbonPageRegister,
            this.ribbonPageReport,
            this.ribbonPageConf});
            this.ribbonControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit4,
            this.repositoryItemGridLookUpEdit1});
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl.Size = new System.Drawing.Size(784, 116);
            this.ribbonControl.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            this.ribbonControl.Click += new System.EventHandler(this.ribbonControl_Click);
            // 
            // barButtonItemChooseColor
            // 
            this.barButtonItemChooseColor.Caption = "Өнгө сонгох";
            this.barButtonItemChooseColor.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemChooseColor.Glyph")));
            this.barButtonItemChooseColor.Id = 1;
            this.barButtonItemChooseColor.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemChooseColor.LargeGlyph")));
            this.barButtonItemChooseColor.Name = "barButtonItemChooseColor";
            this.barButtonItemChooseColor.Tag = -1;
            this.barButtonItemChooseColor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemChooseColor_ItemClick);
            // 
            // skinRibbonGalleryBarItem
            // 
            this.skinRibbonGalleryBarItem.Caption = "Skin сонгох";
            this.skinRibbonGalleryBarItem.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.skinRibbonGalleryBarItem.Id = 3;
            this.skinRibbonGalleryBarItem.Name = "skinRibbonGalleryBarItem";
            this.skinRibbonGalleryBarItem.Tag = -1;
            // 
            // barButtonItemOrder
            // 
            this.barButtonItemOrder.Caption = "Захиалгын хуудас";
            this.barButtonItemOrder.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemOrder.Description = "Захиалгын хуудас";
            this.barButtonItemOrder.Glyph = global::Pharmacy2016.Properties.Resources.Medicine_order_64;
            this.barButtonItemOrder.Id = 2;
            this.barButtonItemOrder.LargeGlyph = global::Pharmacy2016.Properties.Resources.Medicine_order_64;
            this.barButtonItemOrder.Name = "barButtonItemOrder";
            this.barButtonItemOrder.Tag = 2007;
            this.barButtonItemOrder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOrder_ItemClick);
            // 
            // barButtonItemMedicinePatient
            // 
            this.barButtonItemMedicinePatient.Caption = "Эмчлүүлэгчид эм олгох";
            this.barButtonItemMedicinePatient.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemMedicinePatient.Description = "Эмчлүүлэгчид эм олгох";
            this.barButtonItemMedicinePatient.Glyph = global::Pharmacy2016.Properties.Resources.Patient_medicine_64;
            this.barButtonItemMedicinePatient.Id = 3;
            this.barButtonItemMedicinePatient.LargeGlyph = global::Pharmacy2016.Properties.Resources.Patient_medicine_64;
            this.barButtonItemMedicinePatient.Name = "barButtonItemMedicinePatient";
            this.barButtonItemMedicinePatient.Tag = 2004;
            this.barButtonItemMedicinePatient.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemMedicinePatient_ItemClick);
            // 
            // barButtonItemPatientStore
            // 
            this.barButtonItemPatientStore.Caption = "Өвчний түүх";
            this.barButtonItemPatientStore.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemPatientStore.Description = "Эмчлүүлэгчийн өвчний түүх";
            this.barButtonItemPatientStore.Glyph = global::Pharmacy2016.Properties.Resources.Store_64;
            this.barButtonItemPatientStore.Id = 4;
            this.barButtonItemPatientStore.LargeGlyph = global::Pharmacy2016.Properties.Resources.Store_64;
            this.barButtonItemPatientStore.Name = "barButtonItemPatientStore";
            this.barButtonItemPatientStore.Tag = 1002;
            this.barButtonItemPatientStore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPatientStore_ItemClick);
            // 
            // barButtonItemInc
            // 
            this.barButtonItemInc.Caption = "Орлого";
            this.barButtonItemInc.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemInc.Description = "Орлогын журнал";
            this.barButtonItemInc.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemInc.Glyph")));
            this.barButtonItemInc.Id = 5;
            this.barButtonItemInc.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemInc.LargeGlyph")));
            this.barButtonItemInc.Name = "barButtonItemInc";
            this.barButtonItemInc.Tag = 2001;
            this.barButtonItemInc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemInc_ItemClick);
            // 
            // barButtonItemExp
            // 
            this.barButtonItemExp.Caption = "Зарлага";
            this.barButtonItemExp.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemExp.Description = "Зарлагын журнал";
            this.barButtonItemExp.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemExp.Glyph")));
            this.barButtonItemExp.Id = 6;
            this.barButtonItemExp.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemExp.LargeGlyph")));
            this.barButtonItemExp.Name = "barButtonItemExp";
            this.barButtonItemExp.Tag = 2002;
            this.barButtonItemExp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemExp_ItemClick);
            // 
            // barButtonItemMot
            // 
            this.barButtonItemMot.Caption = "Шилжүүлэг";
            this.barButtonItemMot.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemMot.Description = "Шилжүүлэгийн журнал";
            this.barButtonItemMot.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemMot.Glyph")));
            this.barButtonItemMot.Id = 7;
            this.barButtonItemMot.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemMot.LargeGlyph")));
            this.barButtonItemMot.Name = "barButtonItemMot";
            this.barButtonItemMot.Tag = 2003;
            this.barButtonItemMot.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemMot.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemMot_ItemClick);
            // 
            // barButtonItemBal
            // 
            this.barButtonItemBal.Caption = "Журнал";
            this.barButtonItemBal.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemBal.Description = "Журнал";
            this.barButtonItemBal.Glyph = global::Pharmacy2016.Properties.Resources.Bal_64;
            this.barButtonItemBal.Id = 8;
            this.barButtonItemBal.LargeGlyph = global::Pharmacy2016.Properties.Resources.Bal_64;
            this.barButtonItemBal.Name = "barButtonItemBal";
            this.barButtonItemBal.Tag = 2005;
            this.barButtonItemBal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemBal_ItemClick);
            // 
            // barButtonItemBegBal
            // 
            this.barButtonItemBegBal.Caption = "Эхний үлдэгдэл";
            this.barButtonItemBegBal.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemBegBal.Description = "Эхний үлдэгдэл";
            this.barButtonItemBegBal.Glyph = global::Pharmacy2016.Properties.Resources.Beg_Bal_64;
            this.barButtonItemBegBal.Id = 9;
            this.barButtonItemBegBal.LargeGlyph = global::Pharmacy2016.Properties.Resources.Beg_Bal_64;
            this.barButtonItemBegBal.Name = "barButtonItemBegBal";
            this.barButtonItemBegBal.Tag = 2006;
            this.barButtonItemBegBal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemBegBal_ItemClick);
            // 
            // barButtonItemMedicine
            // 
            this.barButtonItemMedicine.Caption = "Эмийн бүртгэл";
            this.barButtonItemMedicine.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemMedicine.Description = "Эмийн бүртгэл";
            this.barButtonItemMedicine.Glyph = global::Pharmacy2016.Properties.Resources.Medicine_64;
            this.barButtonItemMedicine.Id = 10;
            this.barButtonItemMedicine.LargeGlyph = global::Pharmacy2016.Properties.Resources.Medicine_64;
            this.barButtonItemMedicine.Name = "barButtonItemMedicine";
            this.barButtonItemMedicine.Tag = 3003;
            this.barButtonItemMedicine.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemMedicine_ItemClick);
            // 
            // barButtonItemHospital
            // 
            this.barButtonItemHospital.Caption = "Байгууллагын бүртгэл";
            this.barButtonItemHospital.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemHospital.Description = "Байгууллагын бүртгэл";
            this.barButtonItemHospital.Glyph = global::Pharmacy2016.Properties.Resources.Hospital_64;
            this.barButtonItemHospital.Id = 11;
            this.barButtonItemHospital.LargeGlyph = global::Pharmacy2016.Properties.Resources.Hospital_64;
            this.barButtonItemHospital.Name = "barButtonItemHospital";
            this.barButtonItemHospital.Tag = 3001;
            this.barButtonItemHospital.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHospital_ItemClick);
            // 
            // barButtonItemUser
            // 
            this.barButtonItemUser.Caption = "Хэрэглэгчийн бүртгэл";
            this.barButtonItemUser.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemUser.Description = "Хэрэглэгчийн бүртгэл";
            this.barButtonItemUser.Glyph = global::Pharmacy2016.Properties.Resources.User_64;
            this.barButtonItemUser.Id = 12;
            this.barButtonItemUser.LargeGlyph = global::Pharmacy2016.Properties.Resources.User_64;
            this.barButtonItemUser.Name = "barButtonItemUser";
            this.barButtonItemUser.Tag = 3005;
            this.barButtonItemUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUser_ItemClick);
            // 
            // barButtonItemWindowRole
            // 
            this.barButtonItemWindowRole.Caption = "Програмын тохиргоо";
            this.barButtonItemWindowRole.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemWindowRole.Description = "Програмын тохиргоо";
            this.barButtonItemWindowRole.Glyph = global::Pharmacy2016.Properties.Resources.System_conf_64;
            this.barButtonItemWindowRole.Id = 13;
            this.barButtonItemWindowRole.LargeGlyph = global::Pharmacy2016.Properties.Resources.System_conf_64;
            this.barButtonItemWindowRole.Name = "barButtonItemWindowRole";
            this.barButtonItemWindowRole.Tag = 3008;
            this.barButtonItemWindowRole.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemLocation_ItemClick);
            // 
            // barButtonItemOrgPos
            // 
            this.barButtonItemOrgPos.Caption = "Эмчлүүлэгчийн лавлах";
            this.barButtonItemOrgPos.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemOrgPos.Description = "Эмчлүүлэгчийн лавлах";
            this.barButtonItemOrgPos.Glyph = global::Pharmacy2016.Properties.Resources.User_info_64;
            this.barButtonItemOrgPos.Id = 14;
            this.barButtonItemOrgPos.LargeGlyph = global::Pharmacy2016.Properties.Resources.User_info_64;
            this.barButtonItemOrgPos.Name = "barButtonItemOrgPos";
            this.barButtonItemOrgPos.Tag = 3007;
            this.barButtonItemOrgPos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOrgPos_ItemClick);
            // 
            // barButtonItemWardRoom
            // 
            this.barButtonItemWardRoom.Caption = "Тасаг, өрөө";
            this.barButtonItemWardRoom.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemWardRoom.Description = "Тасаг, өрөөний бүртгэл";
            this.barButtonItemWardRoom.Glyph = global::Pharmacy2016.Properties.Resources.Bed_64;
            this.barButtonItemWardRoom.Id = 15;
            this.barButtonItemWardRoom.LargeGlyph = global::Pharmacy2016.Properties.Resources.Bed_64;
            this.barButtonItemWardRoom.Name = "barButtonItemWardRoom";
            this.barButtonItemWardRoom.Tag = 3002;
            this.barButtonItemWardRoom.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemWardRoom_ItemClick);
            // 
            // barButtonItemPatient
            // 
            this.barButtonItemPatient.Caption = "Эмчлүүлэгчийн бүртгэл";
            this.barButtonItemPatient.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemPatient.Description = "Эмчлүүлэгчийн бүртгэл";
            this.barButtonItemPatient.Glyph = global::Pharmacy2016.Properties.Resources.Patient_64;
            this.barButtonItemPatient.Id = 16;
            this.barButtonItemPatient.LargeGlyph = global::Pharmacy2016.Properties.Resources.Patient_64;
            this.barButtonItemPatient.Name = "barButtonItemPatient";
            this.barButtonItemPatient.Tag = 3006;
            this.barButtonItemPatient.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPatient_ItemClick);
            // 
            // barButtonItemOrderNumber
            // 
            this.barButtonItemOrderNumber.Caption = "Хүлээлгийн жагсаалт";
            this.barButtonItemOrderNumber.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemOrderNumber.Description = "Хүлээлгийн жагсаалт";
            this.barButtonItemOrderNumber.Glyph = global::Pharmacy2016.Properties.Resources.receptionist_64;
            this.barButtonItemOrderNumber.Id = 17;
            this.barButtonItemOrderNumber.LargeGlyph = global::Pharmacy2016.Properties.Resources.receptionist_64;
            this.barButtonItemOrderNumber.Name = "barButtonItemOrderNumber";
            this.barButtonItemOrderNumber.Tag = 1001;
            this.barButtonItemOrderNumber.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOrderNumber_ItemClick);
            // 
            // barButtonItemLogIn
            // 
            this.barButtonItemLogIn.Caption = "Нэвтрэх";
            this.barButtonItemLogIn.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemLogIn.Description = "Нэвтрэх";
            this.barButtonItemLogIn.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLogIn.Glyph")));
            this.barButtonItemLogIn.Id = 19;
            this.barButtonItemLogIn.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLogIn.LargeGlyph")));
            this.barButtonItemLogIn.Name = "barButtonItemLogIn";
            this.barButtonItemLogIn.Tag = 5003;
            this.barButtonItemLogIn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemLogIn_ItemClick);
            // 
            // barButtonItemLogOut
            // 
            this.barButtonItemLogOut.Caption = "Хэрэглэгч солих";
            this.barButtonItemLogOut.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemLogOut.Description = "Хэрэглэгч солих";
            this.barButtonItemLogOut.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLogOut.Glyph")));
            this.barButtonItemLogOut.Id = 20;
            this.barButtonItemLogOut.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLogOut.LargeGlyph")));
            this.barButtonItemLogOut.Name = "barButtonItemLogOut";
            this.barButtonItemLogOut.Tag = 5004;
            this.barButtonItemLogOut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemLogOut_ItemClick);
            // 
            // barButtonItemExit
            // 
            this.barButtonItemExit.Caption = "Програмыг хаах";
            this.barButtonItemExit.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemExit.Description = "Програмыг хаах";
            this.barButtonItemExit.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemExit.Glyph")));
            this.barButtonItemExit.Id = 21;
            this.barButtonItemExit.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemExit.LargeGlyph")));
            this.barButtonItemExit.Name = "barButtonItemExit";
            this.barButtonItemExit.Tag = 5005;
            this.barButtonItemExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemExit_ItemClick);
            // 
            // barButtonItemDBConf
            // 
            this.barButtonItemDBConf.Caption = "Өгөгдлийн санг тохируулах";
            this.barButtonItemDBConf.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemDBConf.Description = "Өгөгдлийн санг тохируулах";
            this.barButtonItemDBConf.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDBConf.Glyph")));
            this.barButtonItemDBConf.Id = 22;
            this.barButtonItemDBConf.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDBConf.LargeGlyph")));
            this.barButtonItemDBConf.Name = "barButtonItemDBConf";
            this.barButtonItemDBConf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDBConf_ItemClick);
            // 
            // barStaticItemHospital
            // 
            this.barStaticItemHospital.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItemHospital.Caption = "Эмнэлэг";
            this.barStaticItemHospital.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barStaticItemHospital.Description = "Эмнэлэг";
            this.barStaticItemHospital.Id = 6;
            this.barStaticItemHospital.Name = "barStaticItemHospital";
            this.barStaticItemHospital.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemUserName
            // 
            this.barStaticItemUserName.Caption = "Хэрэглэгч";
            this.barStaticItemUserName.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barStaticItemUserName.Description = "Хэрэглэгч";
            this.barStaticItemUserName.Id = 7;
            this.barStaticItemUserName.Name = "barStaticItemUserName";
            this.barStaticItemUserName.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemWard
            // 
            this.barStaticItemWard.Caption = "Тасаг";
            this.barStaticItemWard.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barStaticItemWard.Description = "Тасаг";
            this.barStaticItemWard.Id = 8;
            this.barStaticItemWard.Name = "barStaticItemWard";
            this.barStaticItemWard.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRole
            // 
            this.barStaticItemRole.Caption = "Эрх";
            this.barStaticItemRole.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barStaticItemRole.Description = "Эрх";
            this.barStaticItemRole.Id = 9;
            this.barStaticItemRole.Name = "barStaticItemRole";
            this.barStaticItemRole.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "|";
            this.barStaticItem1.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barStaticItem1.Id = 10;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "|";
            this.barStaticItem2.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barStaticItem2.Id = 11;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barButtonItemLog
            // 
            this.barButtonItemLog.Caption = "Програмын хандалт";
            this.barButtonItemLog.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemLog.Description = "Програм, системийн хандалтын лог";
            this.barButtonItemLog.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLog.Glyph")));
            this.barButtonItemLog.Id = 12;
            this.barButtonItemLog.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLog.LargeGlyph")));
            this.barButtonItemLog.Name = "barButtonItemLog";
            this.barButtonItemLog.Tag = "5001";
            this.barButtonItemLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemLog_ItemClick);
            // 
            // barButtonItemPatientWardRoom
            // 
            this.barButtonItemPatientWardRoom.Caption = "Хэвтэн эмчлүүлэгч";
            this.barButtonItemPatientWardRoom.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonItemPatientWardRoom.Description = "Хэвтэн эмчлүүлэгчийг харах";
            this.barButtonItemPatientWardRoom.Glyph = global::Pharmacy2016.Properties.Resources.Patient_64;
            this.barButtonItemPatientWardRoom.Id = 13;
            this.barButtonItemPatientWardRoom.LargeGlyph = global::Pharmacy2016.Properties.Resources.Patient_64;
            this.barButtonItemPatientWardRoom.Name = "barButtonItemPatientWardRoom";
            this.barButtonItemPatientWardRoom.Tag = 1004;
            this.barButtonItemPatientWardRoom.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPatientWardRoom_ItemClick);
            // 
            // barButtonItemChangePass
            // 
            this.barButtonItemChangePass.Caption = "Нууц үг солих";
            this.barButtonItemChangePass.Description = "Хэрэглэгчийн нууц үг солих";
            this.barButtonItemChangePass.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemChangePass.Glyph")));
            this.barButtonItemChangePass.Id = 14;
            this.barButtonItemChangePass.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemChangePass.LargeGlyph")));
            this.barButtonItemChangePass.Name = "barButtonItemChangePass";
            this.barButtonItemChangePass.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemChangePass_ItemClick);
            // 
            // barButtonItemWarehouse
            // 
            this.barButtonItemWarehouse.Caption = "Агуулахын бүртгэл";
            this.barButtonItemWarehouse.Description = "Агуулахын бүртгэл";
            this.barButtonItemWarehouse.Glyph = global::Pharmacy2016.Properties.Resources.Warehouse_64;
            this.barButtonItemWarehouse.Id = 15;
            this.barButtonItemWarehouse.LargeGlyph = global::Pharmacy2016.Properties.Resources.Warehouse_64;
            this.barButtonItemWarehouse.Name = "barButtonItemWarehouse";
            this.barButtonItemWarehouse.Tag = 3004;
            this.barButtonItemWarehouse.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemWarehouse_ItemClick);
            // 
            // barButtonItemCensus
            // 
            this.barButtonItemCensus.Caption = "Тооллого";
            this.barButtonItemCensus.Description = "Тооллогын журнал";
            this.barButtonItemCensus.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCensus.Glyph")));
            this.barButtonItemCensus.Id = 16;
            this.barButtonItemCensus.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCensus.LargeGlyph")));
            this.barButtonItemCensus.Name = "barButtonItemCensus";
            this.barButtonItemCensus.Tag = 2008;
            this.barButtonItemCensus.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCensus_ItemClick);
            // 
            // barButtonItemPlan
            // 
            this.barButtonItemPlan.Caption = "Төлөвлөгөө";
            this.barButtonItemPlan.Description = "Төлөвлөгөө";
            this.barButtonItemPlan.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPlan.Glyph")));
            this.barButtonItemPlan.Id = 17;
            this.barButtonItemPlan.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPlan.LargeGlyph")));
            this.barButtonItemPlan.Name = "barButtonItemPlan";
            this.barButtonItemPlan.Tag = 2009;
            this.barButtonItemPlan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPlan_ItemClick);
            // 
            // barButtonItemTender
            // 
            this.barButtonItemTender.Caption = "Тендер";
            this.barButtonItemTender.Description = "Тендер";
            this.barButtonItemTender.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemTender.Glyph")));
            this.barButtonItemTender.Id = 18;
            this.barButtonItemTender.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemTender.LargeGlyph")));
            this.barButtonItemTender.Name = "barButtonItemTender";
            this.barButtonItemTender.Tag = 2010;
            this.barButtonItemTender.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTender_ItemClick);
            // 
            // barButtonItemDrug
            // 
            this.barButtonItemDrug.Caption = "Эмийн түүвэр";
            this.barButtonItemDrug.Description = "Эмийн түүвэр";
            this.barButtonItemDrug.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDrug.Glyph")));
            this.barButtonItemDrug.Id = 19;
            this.barButtonItemDrug.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDrug.LargeGlyph")));
            this.barButtonItemDrug.Name = "barButtonItemDrug";
            this.barButtonItemDrug.Tag = 1005;
            this.barButtonItemDrug.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDrug_ItemClick);
            // 
            // barButtonItemBackupDB
            // 
            this.barButtonItemBackupDB.Caption = "Бааз архивлах";
            this.barButtonItemBackupDB.Description = "Бааз архивлах";
            this.barButtonItemBackupDB.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemBackupDB.Glyph")));
            this.barButtonItemBackupDB.Id = 20;
            this.barButtonItemBackupDB.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemBackupDB.LargeGlyph")));
            this.barButtonItemBackupDB.Name = "barButtonItemBackupDB";
            this.barButtonItemBackupDB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemBackupDB_ItemClick);
            // 
            // barButtonItemNewDB
            // 
            this.barButtonItemNewDB.Caption = "Бааз үүсгэх";
            this.barButtonItemNewDB.Description = "Бааз үүсгэх";
            this.barButtonItemNewDB.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemNewDB.Glyph")));
            this.barButtonItemNewDB.Id = 21;
            this.barButtonItemNewDB.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemNewDB.LargeGlyph")));
            this.barButtonItemNewDB.Name = "barButtonItemNewDB";
            this.barButtonItemNewDB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemNewDB_ItemClick);
            // 
            // barButtonItemShareMed
            // 
            this.barButtonItemShareMed.Caption = "Эм олгох";
            this.barButtonItemShareMed.Description = "Эм олгох";
            this.barButtonItemShareMed.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemShareMed.Glyph")));
            this.barButtonItemShareMed.Id = 22;
            this.barButtonItemShareMed.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemShareMed.LargeGlyph")));
            this.barButtonItemShareMed.Name = "barButtonItemShareMed";
            this.barButtonItemShareMed.Tag = 1006;
            this.barButtonItemShareMed.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemShareMed_ItemClick);
            // 
            // barEditItemMainDB
            // 
            this.barEditItemMainDB.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barEditItemMainDB.Edit = this.repositoryItemGridLookUpEdit1;
            this.barEditItemMainDB.Id = 23;
            this.barEditItemMainDB.Name = "barEditItemMainDB";
            this.barEditItemMainDB.Width = 150;
            this.barEditItemMainDB.EditValueChanged += new System.EventHandler(this.barEditItemMainDB_EditValueChanged);
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DisplayMember = "name";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.ValueMember = "name";
            this.repositoryItemGridLookUpEdit1.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnName,
            this.gridColumnBegBalDate,
            this.gridColumnIsCurrDB});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.repositoryItemGridLookUpEdit1View_CustomDrawCell);
            // 
            // gridColumnName
            // 
            this.gridColumnName.Caption = "Нэр";
            this.gridColumnName.CustomizationCaption = "Өгөгдлийн сангийн нэр";
            this.gridColumnName.FieldName = "name";
            this.gridColumnName.Name = "gridColumnName";
            this.gridColumnName.Visible = true;
            this.gridColumnName.VisibleIndex = 0;
            this.gridColumnName.Width = 675;
            // 
            // gridColumnBegBalDate
            // 
            this.gridColumnBegBalDate.Caption = "Огноо";
            this.gridColumnBegBalDate.CustomizationCaption = "Өгөгдлийн сангийн огноо";
            this.gridColumnBegBalDate.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.gridColumnBegBalDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumnBegBalDate.FieldName = "begBalDate";
            this.gridColumnBegBalDate.Name = "gridColumnBegBalDate";
            this.gridColumnBegBalDate.Visible = true;
            this.gridColumnBegBalDate.VisibleIndex = 1;
            this.gridColumnBegBalDate.Width = 256;
            // 
            // gridColumnIsCurrDB
            // 
            this.gridColumnIsCurrDB.Caption = "Ашиглах эсэх";
            this.gridColumnIsCurrDB.CustomizationCaption = "Ашиглах эсэх";
            this.gridColumnIsCurrDB.FieldName = "isCurrDB";
            this.gridColumnIsCurrDB.Name = "gridColumnIsCurrDB";
            this.gridColumnIsCurrDB.Visible = true;
            this.gridColumnIsCurrDB.VisibleIndex = 2;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem4.Caption = "|";
            this.barStaticItem4.Id = 24;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barButtonItemLicense
            // 
            this.barButtonItemLicense.Caption = "Лиценз сунгах";
            this.barButtonItemLicense.Description = "Лиценз сунгах";
            this.barButtonItemLicense.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLicense.Glyph")));
            this.barButtonItemLicense.Id = 25;
            this.barButtonItemLicense.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLicense.LargeGlyph")));
            this.barButtonItemLicense.Name = "barButtonItemLicense";
            this.barButtonItemLicense.Tag = "5006";
            this.barButtonItemLicense.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemLicense_ItemClick);
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem5.Caption = "|";
            this.barStaticItem5.Id = 26;
            this.barStaticItem5.Name = "barStaticItem5";
            this.barStaticItem5.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemValidDay
            // 
            this.barStaticItemValidDay.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItemValidDay.Caption = "Хүчинтэй хугацаа";
            this.barStaticItemValidDay.Id = 27;
            this.barStaticItemValidDay.Name = "barStaticItemValidDay";
            this.barStaticItemValidDay.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barButtonItemUpdate
            // 
            this.barButtonItemUpdate.Caption = "Шинэчлэх";
            this.barButtonItemUpdate.Description = "Системийг шинэчлэх";
            this.barButtonItemUpdate.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdate.Glyph")));
            this.barButtonItemUpdate.Id = 28;
            this.barButtonItemUpdate.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdate.LargeGlyph")));
            this.barButtonItemUpdate.Name = "barButtonItemUpdate";
            this.barButtonItemUpdate.Tag = "5007";
            this.barButtonItemUpdate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUpdate_ItemClick);
            // 
            // barButtonItemIntegration
            // 
            this.barButtonItemIntegration.Caption = "Өгөгдөл экспорт";
            this.barButtonItemIntegration.Description = "Өгөгдөл Acoulus програмд экспорт хийх";
            this.barButtonItemIntegration.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemIntegration.Glyph")));
            this.barButtonItemIntegration.Id = 29;
            this.barButtonItemIntegration.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemIntegration.LargeGlyph")));
            this.barButtonItemIntegration.Name = "barButtonItemIntegration";
            this.barButtonItemIntegration.Tag = "5007";
            // 
            // barButtonItemAmount
            // 
            this.barButtonItemAmount.Caption = "Өгөгдөл экспорт";
            this.barButtonItemAmount.Description = "Өгөгдөл экспорт";
            this.barButtonItemAmount.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAmount.Glyph")));
            this.barButtonItemAmount.Id = 30;
            this.barButtonItemAmount.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAmount.LargeGlyph")));
            this.barButtonItemAmount.Name = "barButtonItemAmount";
            this.barButtonItemAmount.Tag = "5008";
            this.barButtonItemAmount.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAmount_ItemClick);
            // 
            // ribbonPageStore
            // 
            this.ribbonPageStore.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupStoreOrder,
            this.ribbonPageGroupStoreDrug});
            this.ribbonPageStore.Name = "ribbonPageStore";
            this.ribbonPageStore.Text = "Эмчилгээ";
            // 
            // ribbonPageGroupStoreOrder
            // 
            this.ribbonPageGroupStoreOrder.ItemLinks.Add(this.barButtonItemOrderNumber);
            this.ribbonPageGroupStoreOrder.ItemLinks.Add(this.barButtonItemPatientStore);
            this.ribbonPageGroupStoreOrder.ItemLinks.Add(this.barButtonItemPatientWardRoom);
            this.ribbonPageGroupStoreOrder.ItemLinks.Add(this.barButtonItemMedicinePatient);
            this.ribbonPageGroupStoreOrder.Name = "ribbonPageGroupStoreOrder";
            this.ribbonPageGroupStoreOrder.Text = "Дугаар олгох, өвчний түүх";
            // 
            // ribbonPageGroupStoreDrug
            // 
            this.ribbonPageGroupStoreDrug.ItemLinks.Add(this.barButtonItemDrug);
            this.ribbonPageGroupStoreDrug.ItemLinks.Add(this.barButtonItemShareMed);
            this.ribbonPageGroupStoreDrug.Name = "ribbonPageGroupStoreDrug";
            this.ribbonPageGroupStoreDrug.Text = "Эмийн түүвэр";
            // 
            // ribbonPageJournal
            // 
            this.ribbonPageJournal.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2});
            this.ribbonPageJournal.Name = "ribbonPageJournal";
            this.ribbonPageJournal.Text = "Эмийн гүйлгээ";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemInc);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemExp);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemMot);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemOrder);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemBal);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemTender);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemPlan);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemCensus);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemBegBal);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Эмийн гүйлгээ";
            // 
            // ribbonPageRegister
            // 
            this.ribbonPageRegister.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3});
            this.ribbonPageRegister.Name = "ribbonPageRegister";
            this.ribbonPageRegister.Text = "Үндсэн бүртгэл";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemHospital);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemWardRoom);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemWarehouse);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemMedicine);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemUser);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemPatient);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemOrgPos);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItemWindowRole);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Бүртгэл";
            // 
            // ribbonPageReport
            // 
            this.ribbonPageReport.Name = "ribbonPageReport";
            this.ribbonPageReport.Text = "Тайлан";
            this.ribbonPageReport.Visible = false;
            // 
            // ribbonPageConf
            // 
            this.ribbonPageConf.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupView,
            this.ribbonPageGroupAdmin,
            this.ribbonPageGroupLogin});
            this.ribbonPageConf.Name = "ribbonPageConf";
            this.ribbonPageConf.Text = "Хэрэглэгчийн тохиргоо";
            // 
            // ribbonPageGroupView
            // 
            this.ribbonPageGroupView.ItemLinks.Add(this.skinRibbonGalleryBarItem);
            this.ribbonPageGroupView.ItemLinks.Add(this.barButtonItemChooseColor);
            this.ribbonPageGroupView.Name = "ribbonPageGroupView";
            this.ribbonPageGroupView.Text = "Харагдах байдал";
            // 
            // ribbonPageGroupAdmin
            // 
            this.ribbonPageGroupAdmin.ItemLinks.Add(this.barButtonItemLog);
            this.ribbonPageGroupAdmin.ItemLinks.Add(this.barButtonItemBackupDB);
            this.ribbonPageGroupAdmin.ItemLinks.Add(this.barButtonItemNewDB);
            this.ribbonPageGroupAdmin.ItemLinks.Add(this.barButtonItemAmount);
            this.ribbonPageGroupAdmin.Name = "ribbonPageGroupAdmin";
            this.ribbonPageGroupAdmin.Text = "Өгөгдлийн сан";
            // 
            // ribbonPageGroupLogin
            // 
            this.ribbonPageGroupLogin.ItemLinks.Add(this.barButtonItemDBConf);
            this.ribbonPageGroupLogin.ItemLinks.Add(this.barButtonItemChangePass);
            this.ribbonPageGroupLogin.ItemLinks.Add(this.barButtonItemLogIn);
            this.ribbonPageGroupLogin.ItemLinks.Add(this.barButtonItemLogOut);
            this.ribbonPageGroupLogin.ItemLinks.Add(this.barButtonItemUpdate);
            this.ribbonPageGroupLogin.ItemLinks.Add(this.barButtonItemLicense);
            this.ribbonPageGroupLogin.ItemLinks.Add(this.barButtonItemExit);
            this.ribbonPageGroupLogin.Name = "ribbonPageGroupLogin";
            this.ribbonPageGroupLogin.Text = "Програм";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItemUserName);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem1);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItemRole);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem2);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItemWard);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItemValidDay);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem5);
            this.ribbonStatusBar1.ItemLinks.Add(this.barEditItemMainDB);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem4);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItemHospital);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 484);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(784, 27);
            // 
            // xtraTabbedMdiManager
            // 
            this.xtraTabbedMdiManager.Appearance.Image = global::Pharmacy2016.Properties.Resources.background;
            this.xtraTabbedMdiManager.Appearance.Options.UseImage = true;
            this.xtraTabbedMdiManager.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            this.xtraTabbedMdiManager.FloatOnDoubleClick = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabbedMdiManager.MdiParent = this;
            // 
            // popupMenu
            // 
            this.popupMenu.Name = "popupMenu";
            this.popupMenu.Ribbon = this.ribbonControl;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "|";
            this.barStaticItem3.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barStaticItem3.Id = 11;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // Label_Version
            // 
            this.Label_Version.AutoSize = true;
            this.Label_Version.Location = new System.Drawing.Point(609, 167);
            this.Label_Version.Name = "Label_Version";
            this.Label_Version.Size = new System.Drawing.Size(19, 13);
            this.Label_Version.TabIndex = 3;
            this.Label_Version.Text = "28";
            this.Label_Version.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 511);
            this.Controls.Add(this.Label_Version);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.ribbonControl);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.Text = "Эмийн бүртгэл";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageStore;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageJournal;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageRegister;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageReport;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageConf;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupStoreOrder;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager;
        private DevExpress.XtraBars.BarButtonItem barButtonItemChooseColor;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupView;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem;
        //private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOrder;
        private DevExpress.XtraBars.BarButtonItem barButtonItemMedicinePatient;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPatientStore;
        private DevExpress.XtraBars.BarButtonItem barButtonItemInc;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExp;
        private DevExpress.XtraBars.BarButtonItem barButtonItemMot;
        private DevExpress.XtraBars.BarButtonItem barButtonItemBal;
        private DevExpress.XtraBars.BarButtonItem barButtonItemBegBal;
        private DevExpress.XtraBars.BarButtonItem barButtonItemMedicine;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHospital;
        private DevExpress.XtraBars.BarButtonItem barButtonItemUser;
        private DevExpress.XtraBars.BarButtonItem barButtonItemWindowRole;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOrgPos;
        private DevExpress.XtraBars.BarButtonItem barButtonItemWardRoom;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPatient;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOrderNumber;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLogIn;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLogOut;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExit;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupLogin;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDBConf;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemHospital;
        private DevExpress.XtraBars.BarStaticItem barStaticItemUserName;
        private DevExpress.XtraBars.BarStaticItem barStaticItemWard;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRole;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLog;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPatientWardRoom;
        private DevExpress.XtraBars.BarButtonItem barButtonItemChangePass;
        private DevExpress.XtraBars.BarButtonItem barButtonItemWarehouse;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCensus;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPlan;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTender;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDrug;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupStoreDrug;
        private DevExpress.XtraBars.BarButtonItem barButtonItemBackupDB;
        private DevExpress.XtraBars.BarButtonItem barButtonItemNewDB;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAdmin;
        private DevExpress.XtraBars.BarButtonItem barButtonItemShareMed;
        private DevExpress.XtraBars.BarEditItem barEditItemMainDB;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBegBalDate;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIsCurrDB;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLicense;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.BarStaticItem barStaticItemValidDay;
        private DevExpress.XtraBars.BarButtonItem barButtonItemUpdate;
        private DevExpress.XtraBars.BarButtonItem barButtonItemIntegration;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAmount;
        private System.Windows.Forms.Label Label_Version;
    }
}