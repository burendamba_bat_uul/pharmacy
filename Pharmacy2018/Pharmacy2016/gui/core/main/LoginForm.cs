﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.main
{
    /*
     * Програм руу нэвтэрч орох цонх.
     * 
     */

    public partial class LoginForm : DevExpress.XtraEditors.XtraForm
    {

        #region Програм анх ачааллах функц

            private static LoginForm INSTANCE = new LoginForm();

            public static LoginForm Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private bool isLogin = false;

            private int loginCount = 0;

            private static readonly int MAX_LOGIN_COUNT = 3;

            private LoginForm()
            {
            
            }

            private void initForm()
            {
                if (!this.isInit)
                {
                    InitializeComponent();
                    this.isInit = true;
                    Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                    textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                    textEditPassword.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                }
            }

        #endregion

        #region Програм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    loginCount = 0;
                    isLogin = false;

                    ShowDialog(MainForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                textEditName.Text = "";
                textEditPassword.Text = "";

                textEditName.ErrorText = "";
                textEditPassword.ErrorText = "";

                if (isLogin)
                {
                    MainForm.Instance.showMenu();
                }
                else
                {
                    MainForm.Instance.showLoginMenu();
                }
            }

            private void LoginForm_Shown(object sender, EventArgs e)
            {
                textEditName.Focus();
            }

        #endregion

        #region Формын event

            private void textEditPassword_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    login();
                }   
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                login();
            }

        #endregion

        #region Формын функц

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if ((textEditName.Text = textEditName.Text.Trim()).Equals(""))
                {
                    isRight = false;
                    textEditName.Focus();
                    errorText = "Нэвтрэх нэрийг оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                }
                if ((textEditPassword.Text = textEditPassword.Text.Trim()).Equals(""))
                {
                    errorText = "Нууц үгийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditPassword.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditPassword.Focus();
                    }
                }
                if(!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                
                return isRight;
            }

            private void login()
            {
                if (loginCount == MAX_LOGIN_COUNT)
                {
                    XtraMessageBox.Show("Олон удаа буруу нэвтрэх гэж оролдсон тул програмыг хаалаа");
                    SystemUtil.IsConnect = false;
                    ProgramUtil.EXIT(true);
                }
                else if (check())
                {
                    loginCount++;
                    string errorText;
                    object result = InformationDataSet.QueriesTableAdapter.log_in(textEditName.Text, HospitalUtil.getHospitalId(), UserUtil.convertMD5Hash(textEditPassword.Text));
                    if (Convert.ToInt32(result) == -2)
                    {
                        errorText = "Энэ хэрэглэгчийн эрхийг идэвхигүй болгосон байна";                        
                        ProgramUtil.showAlertDialog(Instance, Instance.Text, ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX);
                        textEditName.ErrorText = errorText;
                        textEditName.Focus();
                    }
                    else if (Convert.ToInt32(result) == -3)
                    {
                        errorText = "Нэвтрэх нэр эсвэл нууц үг буруу байна";
                        ProgramUtil.showAlertDialog(Instance, Instance.Text, ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX);
                        textEditName.ErrorText = errorText;
                        textEditPassword.ErrorText = errorText;
                        textEditName.Focus();
                    }
                    else
                    {
                        isLogin = true;
                        ProgramUtil.LOGIN(result.ToString());
                        Close();
                    }
                }
            }

        #endregion

    }
}