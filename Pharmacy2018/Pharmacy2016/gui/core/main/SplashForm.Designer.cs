﻿namespace Pharmacy2016.gui.core.main
{
    partial class SplashForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashForm));
            this.progressBarControlInitializing = new DevExpress.XtraEditors.ProgressBarControl();
            this.labelControlProgramUpdateVersionAndDate = new DevExpress.XtraEditors.LabelControl();
            this.labelControlCopyright = new DevExpress.XtraEditors.LabelControl();
            this.pictureEditSplashClose = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControlInitializing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditSplashClose.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBarControlInitializing
            // 
            this.progressBarControlInitializing.Location = new System.Drawing.Point(10, 202);
            this.progressBarControlInitializing.Name = "progressBarControlInitializing";
            this.progressBarControlInitializing.Properties.DisplayFormat.FormatString = "Програм эхэлж байна... {0}%";
            this.progressBarControlInitializing.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.progressBarControlInitializing.Properties.ShowTitle = true;
            this.progressBarControlInitializing.Size = new System.Drawing.Size(380, 20);
            this.progressBarControlInitializing.TabIndex = 0;
            this.progressBarControlInitializing.TabStop = true;
            // 
            // labelControlProgramUpdateVersionAndDate
            // 
            this.labelControlProgramUpdateVersionAndDate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControlProgramUpdateVersionAndDate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlProgramUpdateVersionAndDate.Location = new System.Drawing.Point(190, 183);
            this.labelControlProgramUpdateVersionAndDate.Name = "labelControlProgramUpdateVersionAndDate";
            this.labelControlProgramUpdateVersionAndDate.Size = new System.Drawing.Size(194, 13);
            this.labelControlProgramUpdateVersionAndDate.TabIndex = 1;
            this.labelControlProgramUpdateVersionAndDate.Text = "0.0.0.0 / 0000-00-00";
            // 
            // labelControlCopyright
            // 
            this.labelControlCopyright.Appearance.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlCopyright.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControlCopyright.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControlCopyright.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlCopyright.Location = new System.Drawing.Point(95, 228);
            this.labelControlCopyright.Name = "labelControlCopyright";
            this.labelControlCopyright.Size = new System.Drawing.Size(295, 19);
            this.labelControlCopyright.TabIndex = 3;
            this.labelControlCopyright.Text = "Зохиогчийн эрх хуулиар хамгаалагдсан болно.  © 2016 он";
            // 
            // pictureEditSplashClose
            // 
            this.pictureEditSplashClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureEditSplashClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureEditSplashClose.EditValue = ((object)(resources.GetObject("pictureEditSplashClose.EditValue")));
            this.pictureEditSplashClose.Location = new System.Drawing.Point(378, 3);
            this.pictureEditSplashClose.Name = "pictureEditSplashClose";
            this.pictureEditSplashClose.Properties.AllowFocused = false;
            this.pictureEditSplashClose.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEditSplashClose.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEditSplashClose.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEditSplashClose.Properties.ReadOnly = true;
            this.pictureEditSplashClose.Properties.ShowMenu = false;
            this.pictureEditSplashClose.Size = new System.Drawing.Size(20, 20);
            this.pictureEditSplashClose.TabIndex = 244;
            this.pictureEditSplashClose.TabStop = true;
            this.pictureEditSplashClose.ToolTip = "Хаах";
            this.pictureEditSplashClose.Click += new System.EventHandler(this.pictureEditSplashClose_Click);
            // 
            // SplashForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Stretch;
            this.BackgroundImageStore = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImageStore")));
            this.ClientSize = new System.Drawing.Size(400, 250);
            this.Controls.Add(this.pictureEditSplashClose);
            this.Controls.Add(this.labelControlCopyright);
            this.Controls.Add(this.labelControlProgramUpdateVersionAndDate);
            this.Controls.Add(this.progressBarControlInitializing);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SplashForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ачаалах цонх";
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControlInitializing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditSplashClose.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ProgressBarControl progressBarControlInitializing;
        private DevExpress.XtraEditors.LabelControl labelControlProgramUpdateVersionAndDate;
        private DevExpress.XtraEditors.LabelControl labelControlCopyright;
        private DevExpress.XtraEditors.PictureEdit pictureEditSplashClose;
    }
}