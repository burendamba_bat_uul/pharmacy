﻿using System;
using System.Threading;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using DevExpress.UserSkins;
using Pharmacy2016.util;


namespace Pharmacy2016.gui.core.main
{
    public partial class SplashForm : XtraForm
    {

        #region Програм анх ачааллах функц

            // Классын цор ганц объект.
            private static readonly SplashForm INSTANCE = new SplashForm();

            public static SplashForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private SplashForm()
            {
                InitializeComponent();
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;
            }

        #endregion

        #region Програм нээх, хаах функц

            public static void showForm()
            {
                Instance.setProgramUpdatedVersionAndDate();

                Thread thread = new Thread(new ThreadStart(Instance.internalShowSplash));
                thread.Start();
            }

            // Програмын хувилбар, update хийсэн огноо
            private void setProgramUpdatedVersionAndDate()
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
                string version = fvi.FileVersion;
                labelControlProgramUpdateVersionAndDate.Text = version + " / " + ProgramUtil.ProgramUpdatedDate;
            }

            private void internalShowSplash()
            {
                ProgramUtil.initSkin();

                ShowDialog();
                internalSetInitializingPercent(0);
            }

            // Цонхыг хаах
            private void pictureEditSplashClose_Click(object sender, EventArgs e)
            {
                Environment.Exit(0);
            }

            public static void closeForm()
            {
                Instance.internalCloseSplash();
            }

            public delegate void delegateCloseSplashMethod();

            private void internalCloseSplash()
            {
                if (InvokeRequired)
                {
                    delegateCloseSplashMethod delegateMethodObj = new delegateCloseSplashMethod(internalCloseSplash);
                    Invoke(delegateMethodObj);
                }
                else
                {
                    internalSetInitializingPercent(100);
                    Close();
                    Dispose();
                }
            }

            // Ачааллаж байгаа хувийг тохируулах функц
            public static void setInitializingPercent(int percent)
            {
                Instance.internalSetInitializingPercent(percent);
            }

            private delegate void delegateSetInitializingPercentMethod(int percent);

            private void internalSetInitializingPercent(int percent)
            {
                if (progressBarControlInitializing.InvokeRequired)
                {
                    delegateSetInitializingPercentMethod delegateMethodObj = new delegateSetInitializingPercentMethod(internalSetInitializingPercent);
                    progressBarControlInitializing.Invoke(delegateMethodObj, new object[] { percent });
                }
                else
                {
                    try
                    {
                        progressBarControlInitializing.Position = percent;
                    }
                    catch (Exception ex)
                    {
                        System.Console.WriteLine("*************** EXCEPTION: progressBarControlInitializing.Position..." + ex.Message);
                        internalSetInitializingPercent(percent);
                    }
                }
            }

        #endregion

    }
}