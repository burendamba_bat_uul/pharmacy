﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using DevExpress.Utils;
using DevExpress.XtraPivotGrid;
using Pharmacy2016.report.journal.patient;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.journal.patient
{
    /*
     * Эмчлүүлэгчийн хэрэглэсэн эмийн бүртгэлийг харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class MedicinePatientForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicinePatientForm INSTANCE = new MedicinePatientForm();

            public static MedicinePatientForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private readonly int FORM_ID = 2004;

            private bool[] roles;

            public string wardID = "";

            private MedicinePatientForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                RoleUtil.addForm(FORM_ID, this);
                initError();

                //pivotGridFieldPatient.Width = 0;
                pivotGridControl.OptionsView.ShowDataHeaders = false;
                pivotGridControl.OptionsView.ShowFilterHeaders = false;
                
                comboBoxEditPatientType.SelectedIndex = 0;

                pivotGridControl.DataSource = JournalDataSet.PatientMedicineBindingSource;
                gridLookUpEditPatient.Properties.DataSource = JournalDataSet.PatientOtherBindingSource;
                
                DateTime date = DateTime.Now;
                dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1);
                dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            }

            private void initError()
            {
                gridLookUpEditPatient.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditEnd.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        pivotGridFieldIsGive.Visible = true;
                        pivotGridFieldCount.Visible = false;
                        pivotGridFieldSumPrice.Visible = false;

                        checkRole();

                        JournalDataSet.Instance.PatientMedicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        JournalDataSet.Instance.PatientMedicine.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        JournalDataSet.Instance.PatientMedicine.giveUserIDColumn.DefaultValue = UserUtil.getUserId();

                        reloadPatient();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                roles = UserUtil.getWindowRole(FORM_ID);

                controlNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                controlNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                controlNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                controlNavigator.Buttons.CustomButtons[3].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                dropDownButtonPrint.Visible = roles[4];

                if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                {
                    wardID = UserUtil.getUserWardID();
                    controlNavigator.Buttons.CustomButtons[1].Visible = false;
                    checkedComboBoxEdit.Visible = false;
                }
                else if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                {
                    checkedComboBoxEdit.Visible = true;
                    //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    //InformationDataSet.PharmacistWardTableAdapter.Fill(InformationDataSet.Instance.PharmacistWard, HospitalUtil.getHospitalId(), UserUtil.getUserId());
                    //for (int i = 0; i < InformationDataSet.Instance.PharmacistWard.Count; i++)
                    //    wardID += InformationDataSet.Instance.PharmacistWard[i]["wardID"].ToString() + "~";
                    //if (!wardID.Equals(""))
                    //    wardID = wardID.Substring(0, wardID.Length - 1);
                    ////textEdit.Text = wardID;
                    //ProgramUtil.closeWaitDialog();
                }
                else
                {
                    checkedComboBoxEdit.Visible = true;
                    wardID = "-1";
                }
            }

            private void MedicinePatient_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                wardID = "";
                InformationDataSet.Instance.Patient.Clear();
                JournalDataSet.Instance.PatientMedicine.Clear();
            }

        #endregion

        #region Формын event

            private void checkedComboBoxEdit_EditValueChanged(object sender, EventArgs e)
            {
                showDataField();
            }


            private void comboBoxEditPatientType_SelectedIndexChanged(object sender, EventArgs e)
            {
                reloadPatient();
            }

            private void checkEditAll_CheckedChanged(object sender, EventArgs e)
            {
                gridLookUpEditPatient.Enabled = !checkEditAll.Checked;
            }

            private void gridLookUpEditPatient_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "reload"))
                {
                    try
                    {
                        reloadPatient();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            private void gridLookUpEditPatient_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
            {
                e.DisplayText = "Эмчлүүлэгч сонгох";
            }


            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }


            private void controlNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addRow();
                    }
                    else if (String.Equals(e.Button.Tag, "delete"))
                    {
                        try
                        {
                            deleteRow();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "update"))
                    {
                        updateRow();
                    }
                    else if (String.Equals(e.Button.Tag, "download"))
                    {
                        download();
                    }
                }
            }
    

            private void pivotGridControl_CustomDrawCell(object sender, PivotCustomDrawCellEventArgs e)
            {
                // checkBox-г дахин утгыг харьцуулан зурж байна
                if (e.DataField == pivotGridFieldIsGive && e.Value != null)
                {
                    if (e.RowValueType == PivotGridValueType.Value)
                    {
                        int value;
                        if (Int32.TryParse(e.Value.ToString(), out value))
                        {
                            e.Appearance.ForeColor = Color.White;
                            // checkBox-г дахин утгыг харьцуулан зурж байна
                            DrawCheckBox(e.Graphics, e.Appearance, e.Bounds, value == 1);
                            e.Handled = true;
                        }
                    }
                    else
                    {
                        e.Appearance.ForeColor = Color.White;
                        e.Handled = true;
                    }
                }
            }

            protected void DrawCheckBox(Graphics g, DevExpress.Utils.AppearanceObject a, Rectangle r, bool Checked)
            {
                DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo info;
                DevExpress.XtraEditors.Drawing.CheckEditPainter painter;
                DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs args;
                info = repositoryItemCheckEdit1.CreateViewInfo() as DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo;
                painter = repositoryItemCheckEdit1.CreatePainter() as DevExpress.XtraEditors.Drawing.CheckEditPainter;

                info.EditValue = Checked;
                //DataRow[] rows = JournalDataSet.Instance.PatientMedicine.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND id = '" + id + "'");
                //if(rows != null && rows.Length > 0)
                //    info.EditValue = Convert.ToBoolean(rows[0]["isGive"]);
                r.Height -= 2;
                r.Width -= 2;

                info.Bounds = r;
                //Manually set the forecolor and font
                info.PaintAppearance.ForeColor = a.ForeColor;
                info.PaintAppearance.Font = a.Font;
                info.CalcViewInfo(g);
                args = new DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs(info, new DevExpress.Utils.Drawing.GraphicsCache(g), r);
                painter.Draw(args);
                args.Cache.Dispose();
            }

            private void pivotGridControl_CellDoubleClick(object sender, PivotCellEventArgs e)
            {
                if (roles[2] && e.ColumnValueType == PivotGridValueType.Value && e.RowValueType == PivotGridValueType.Value)
                {   
                    updateRow();
                }   
            }

            private void pivotGridControl_CustomCellValue(object sender, PivotCellValueEventArgs e)
            {
                if (e.DataField == pivotGridFieldCount)
                {
                    PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
                    double sum = 0;
                    for (int i = 0; i < ds.RowCount; i++)
                    {
                        PivotDrillDownDataRow row = ds[i];
                        if (Convert.ToBoolean(row["isGive"]))
                        {
                            sum += Convert.ToDouble(row["totalCount"]);
                        }
                    }
                    if (ds.RowCount > 0)
                    {
                        e.Value = sum;
                    }
                }
                else if (e.DataField == pivotGridFieldSumPrice)
                {
                    PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
                    double sum = 0;
                    for (int i = 0; i < ds.RowCount; i++)
                    {
                        PivotDrillDownDataRow row = ds[i];
                        if (Convert.ToBoolean(row["isGive"]))
                        {
                            sum += Convert.ToDouble(row["totalSumPrice"]);
                        }
                    }
                    if (ds.RowCount > 0)
                    {
                        e.Value = sum;
                    }
                }
            }

            private void pivotGridControl_CustomAppearance(object sender, PivotCustomAppearanceEventArgs e)
            {
                if (e.DataField == pivotGridFieldCount || e.DataField == pivotGridFieldSumPrice)
                {
                    PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
                    for (int i = 0; i < ds.RowCount; i++)
                    {
                        PivotDrillDownDataRow row = ds[i];
                        if (Convert.ToBoolean(row["isGive"]))
                        {
                            e.Appearance.BackColor =  Color.Yellow;
                        }
                    }
                }
            }

        #endregion

        #region Формын функц

            private void showDataField()
            {
                for(var i = 0; i < checkedComboBoxEdit.Properties.Items.Count; i++)
                {
                    if(checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("isGive"))
                    {
                        pivotGridFieldIsGive.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                    else if(checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("count"))
                    {
                        pivotGridFieldCount.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                    else if(checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("sumPrice"))
                    {
                        pivotGridFieldSumPrice.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                }
            }

            private bool checkDate()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private bool checkUser()
            {
                bool isRight = true;

                // (gridLookUpEditPatient.EditValue == DBNull.Value || gridLookUpEditPatient.EditValue == null || gridLookUpEditPatient.EditValue.ToString().Equals(""))
                if (!checkEditAll.Checked && gridLookUpEditPatient.Properties.View.GetSelectedRows().Length == 0)
                {
                    string errorText = "Эмчлүүлэгчийг сонгоно уу";
                    string text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditPatient.ErrorText = errorText;
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                    isRight = false;
                    gridLookUpEditPatient.Focus();
                }

                return isRight;
            }

            private void reload()
            {
                if (checkDate() && checkUser())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    var patientID = "";
                    if (checkEditAll.Checked)
                    {
                        pivotGridFieldPatient.Width = 200;
                        for (var i = 0; i < JournalDataSet.Instance.PatientOther.Rows.Count; i++)
                            patientID += JournalDataSet.Instance.PatientOther.Rows[i]["id"] + "~";
                    }
                    else
                    {
                        int[] rowID = gridLookUpEditPatient.Properties.View.GetSelectedRows();
                        for (int i = 0; i < rowID.Length; i++)
                            patientID += gridLookUpEditPatient.Properties.View.GetDataRow(rowID[i])["id"] + "~";
                    }
                    if (!patientID.Equals(""))
                        patientID = patientID.Substring(0, patientID.Length - 1);

                    JournalDataSet.PatientMedicineTableAdapter.Fill(JournalDataSet.Instance.PatientMedicine, patientID, HospitalUtil.getHospitalId(), (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID ? UserUtil.getUserId() : ""), dateEditStart.Text, dateEditEnd.Text);
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void reloadPatient()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                bool enable = comboBoxEditPatientType.SelectedIndex != 1;
                controlNavigator.Buttons.CustomButtons[0].Enabled = enable;
                controlNavigator.Buttons.CustomButtons[1].Enabled = enable;
                controlNavigator.Buttons.CustomButtons[2].Enabled = enable;
                controlNavigator.Buttons.CustomButtons[3].Enabled = enable;
                JournalDataSet.PatientOtherTableAdapter.Fill(JournalDataSet.Instance.PatientOther, HospitalUtil.getHospitalId(), wardID, comboBoxEditPatientType.SelectedIndex, dateEditStart.Text, dateEditEnd.Text);
                ProgramUtil.closeWaitDialog();
            }

            private void addRow()
            {
                MedicinePatientInUpForm.Instance.showForm(1);
            }

            private void updateRow()
            {
                if (JournalDataSet.Instance.PatientMedicine.Rows.Count > 0 && pivotGridControl.Cells.GetFocusedCellInfo() != null && pivotGridControl.Cells.GetFocusedCellInfo().Value != null)
                {   
                    MedicinePatientInUpForm.Instance.showForm(2);
                }
            }

            private void deleteRow()
            {
                if (JournalDataSet.Instance.PatientMedicine.Rows.Count > 0 && pivotGridControl.Cells.GetFocusedCellInfo() != null && pivotGridControl.Cells.GetFocusedCellInfo().Value != null)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    string[] values = getSelectedValues();
                    DialogResult dialogResult = XtraMessageBox.Show(values[3] + " эмчлүүлэгчийн " + values[0] + " өдрийн эмийг устгах уу", "Эмчлүүлэгчийн эм устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        DataRow[] rows = JournalDataSet.Instance.PatientMedicine.Select("patientID = '" + values[2] + "' AND date = '" + values[0] + "'");
                        for (var i = 0; i < rows.Length; i++)
                        {
                            rows[i].Delete();
                        }

                        JournalDataSet.PatientMedicineBindingSource.EndEdit();
                        JournalDataSet.PatientMedicineTableAdapter.Update(JournalDataSet.Instance.PatientMedicine);

                        refreshData();
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            private void download()
            {
                MedicineDownForm.Instance.showForm();
            }
            
            
            public void reloadPatient(DateTime start, DateTime end, string patientID)
            {
                dateEditStart.DateTime = start;
                dateEditEnd.DateTime = end;
                checkEditAll.Checked = false;
                int[] rowID = gridLookUpEditPatient.Properties.View.GetSelectedRows();
                for (int i = 0; i < rowID.Length; i++)
                    gridLookUpEditPatient.Properties.View.UnselectRow(rowID[i]);

                if (gridLookUpEditPatient.Properties.View.RowCount == 0)
                {
                    gridLookUpEditPatient.ShowPopup();
                    gridLookUpEditPatient.ClosePopup();
                }
                for(int i = 0; i < gridLookUpEditPatient.Properties.View.RowCount; i++){
                    if(gridLookUpEditPatient.Properties.View.GetDataRow(i)["id"].ToString().Equals(patientID)){
                        gridLookUpEditPatient.Properties.View.SelectRow(i);
                        break;
                    }
                }

                reload();
            }

            public string[] getSelectedValues()
            {
                string[] ret = { null, null, null, null, null };

                PivotCellEventArgs focus = pivotGridControl.Cells.GetFocusedCellInfo();
                PivotDrillDownDataSource source = focus.CreateDrillDownDataSource();

                ret[0] = source[0]["date"].ToString().Substring(0, 10);
                ret[1] = source[0]["userID"].ToString();
                ret[2] = source[0]["patientID"].ToString();
                ret[3] = source[0]["patientName"].ToString();
                ret[4] = comboBoxEditPatientType.SelectedIndex == 1 ? "true" : "false";

                //DataRow[] rows = JournalDataSet.Instance.PatientMedicine.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND id = '" + source[0]["id"] + "'");
                //if (rows.Length > 0)
                //{
                //    ret[0] = rows[0]["date"].ToString().Substring(0, 10);
                //    ret[1] = rows[0]["userID"].ToString();
                //    ret[2] = rows[0]["patientID"].ToString();
                //    ret[3] = rows[0]["patientName"].ToString();
                //    ret[4] = comboBoxEditPatientType.SelectedIndex == 1 ? "true" : "false";
                //}
                return ret;
            }

            public string getSelectedPatient()
            {
                if (gridLookUpEditPatient.EditValue != null)
                    return gridLookUpEditPatient.EditValue.ToString();

                return null;
            }

            public DateTime getStartDate()
            {
                return dateEditStart.DateTime;
            }

            public DateTime getEndDate()
            {
                return dateEditEnd.DateTime;
            }

            public void refreshData()
            {
                pivotGridControl.RefreshData();
            }

        #endregion
        
        #region Хэвлэх, хөрвүүлэх функц

            private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemPatient.Caption;
                dropDownButtonPrint.Image = barButtonItemPatient.Glyph;

                PatientChooseForm.Instance.showForm();
            }

            private void barButtonItemBegBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemAllPatient.Caption;
                dropDownButtonPrint.Image = barButtonItemAllPatient.Glyph;

                if (!checkDate())
                    return;

                PatientMedicineReport.Instance.initAndShow(dateEditStart.Text, dateEditEnd.Text);
            }

            private void barButtonItemEndBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemEndBal.Caption;
                dropDownButtonPrint.Image = barButtonItemEndBal.Glyph;
            }

            private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemConvert.Caption;
                dropDownButtonPrint.Image = barButtonItemConvert.Glyph;
                ProgramUtil.convertGrid(pivotGridControl, "Эмчлүүлэгчид эм олгох");
            }

        #endregion

    }
}