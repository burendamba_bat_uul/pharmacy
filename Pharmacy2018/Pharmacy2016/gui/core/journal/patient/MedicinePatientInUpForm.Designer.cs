﻿namespace Pharmacy2016.gui.core.journal.patient
{
    partial class MedicinePatientInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicinePatientInUpForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlDetail = new DevExpress.XtraGrid.GridControl();
            this.gridViewDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnIsGive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditPatient = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spinEditCount = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditPrice = new DevExpress.XtraEditors.SpinEdit();
            this.labelControlAll = new DevExpress.XtraEditors.LabelControl();
            this.checkEditIsGiven = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditTotalSumPrice = new DevExpress.XtraEditors.SpinEdit();
            this.dateEditDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditAll = new DevExpress.XtraEditors.CheckEdit();
            this.gridLookUpEditMedicine = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spinEditBal = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditPatientCount = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditTotalCount = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsGiven.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTotalSumPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPatientCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTotalCount.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Location = new System.Drawing.Point(415, 33);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(51, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Эм зүйч*: ";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(62, 33);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(70, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Эмчлүүлэгч*: ";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl5.Location = new System.Drawing.Point(436, 382);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(30, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Үнэ*: ";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(516, 446);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 31;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(597, 446);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 32;
            this.simpleButtonCancel.Text = "Болих";
            this.simpleButtonCancel.ToolTip = "Болих";
            // 
            // gridControlDetail
            // 
            this.gridControlDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlDetail.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlDetail.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlDetail_EmbeddedNavigator_ButtonClick);
            this.gridControlDetail.Location = new System.Drawing.Point(12, 82);
            this.gridControlDetail.MainView = this.gridViewDetail;
            this.gridControlDetail.Name = "gridControlDetail";
            this.gridControlDetail.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemCheckEdit1});
            this.gridControlDetail.Size = new System.Drawing.Size(660, 239);
            this.gridControlDetail.TabIndex = 10;
            this.gridControlDetail.UseEmbeddedNavigator = true;
            this.gridControlDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDetail});
            // 
            // gridViewDetail
            // 
            this.gridViewDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnIsGive,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn6,
            this.gridColumn9,
            this.gridColumn7,
            this.gridColumn5,
            this.gridColumn32,
            this.gridColumn34});
            this.gridViewDetail.GridControl = this.gridControlDetail;
            this.gridViewDetail.Name = "gridViewDetail";
            this.gridViewDetail.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewDetail.OptionsView.ShowFooter = true;
            this.gridViewDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewDetail.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewDetail_FocusedRowChanged);
            this.gridViewDetail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewDetail_CellValueChanged);
            this.gridViewDetail.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewDetail_InvalidRowException);
            this.gridViewDetail.BeforeLeaveRow += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.gridViewDetail_BeforeLeaveRow);
            // 
            // gridColumnIsGive
            // 
            this.gridColumnIsGive.Caption = "...";
            this.gridColumnIsGive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumnIsGive.CustomizationCaption = "Өгсөн эсэх";
            this.gridColumnIsGive.FieldName = "isGive";
            this.gridColumnIsGive.Name = "gridColumnIsGive";
            this.gridColumnIsGive.Visible = true;
            this.gridColumnIsGive.VisibleIndex = 0;
            this.gridColumnIsGive.Width = 36;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Эмийн нэр";
            this.gridColumn2.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn2.FieldName = "medicineName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 112;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ОУ нэр";
            this.gridColumn3.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn3.FieldName = "latinName";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 111;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Тун хэмжээ";
            this.gridColumn4.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn4.FieldName = "unitType";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Үнэ";
            this.gridColumn6.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn6.CustomizationCaption = "Үнэ";
            this.gridColumn6.FieldName = "price";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Тоо";
            this.gridColumn9.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn9.CustomizationCaption = "Тоо хэмжээ";
            this.gridColumn9.FieldName = "count";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Нийт үнэ";
            this.gridColumn7.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn7.FieldName = "totalSumPrice";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalSumPrice", "{0:c2}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Өгсөн хэрэглэгч";
            this.gridColumn5.CustomizationCaption = "Эм өгсөн хэрэглэгч";
            this.gridColumn5.FieldName = "giveUserName";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 8;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Орлогын төрөл";
            this.gridColumn32.CustomizationCaption = "Орлогын төрөл";
            this.gridColumn32.FieldName = "incTypeName";
            this.gridColumn32.MinWidth = 75;
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Width = 88;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Э/тоо";
            this.gridColumn34.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn34.CustomizationCaption = "Эмчлүүлэгчээс авсан тоо";
            this.gridColumn34.FieldName = "patientCount";
            this.gridColumn34.MinWidth = 75;
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "patientCount", "{0:n2}")});
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 6;
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl9.Location = new System.Drawing.Point(106, 330);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(26, 13);
            this.labelControl9.TabIndex = 21;
            this.labelControl9.Text = "Эм*: ";
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl10.Location = new System.Drawing.Point(101, 356);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(31, 13);
            this.labelControl10.TabIndex = 25;
            this.labelControl10.Text = "Тоо*: ";
            // 
            // gridLookUpEditPatient
            // 
            this.gridLookUpEditPatient.EditValue = "";
            this.gridLookUpEditPatient.Location = new System.Drawing.Point(138, 30);
            this.gridLookUpEditPatient.Name = "gridLookUpEditPatient";
            this.gridLookUpEditPatient.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPatient.Properties.DisplayMember = "fullName";
            this.gridLookUpEditPatient.Properties.NullText = "";
            this.gridLookUpEditPatient.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditPatient.Properties.ValueMember = "id";
            this.gridLookUpEditPatient.Properties.View = this.gridView1;
            this.gridLookUpEditPatient.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditPatient.TabIndex = 1;
            this.gridLookUpEditPatient.EditValueChanged += new System.EventHandler(this.gridLookUpEditPatient_EditValueChanged);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Овог";
            this.gridColumn10.CustomizationCaption = "Овог";
            this.gridColumn10.FieldName = "lastName";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Нэр";
            this.gridColumn11.CustomizationCaption = "Нэр";
            this.gridColumn11.FieldName = "firstName";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Регистер";
            this.gridColumn12.CustomizationCaption = "Регистер";
            this.gridColumn12.FieldName = "id";
            this.gridColumn12.MinWidth = 75;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Төрсөн өдөр";
            this.gridColumn13.CustomizationCaption = "Төрсөн өдөр";
            this.gridColumn13.FieldName = "birthday";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Утас";
            this.gridColumn14.CustomizationCaption = "Утас";
            this.gridColumn14.FieldName = "phone";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 4;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Тасаг";
            this.gridColumn15.CustomizationCaption = "Тасаг";
            this.gridColumn15.FieldName = "wardName";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 3;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Ажлын газар";
            this.gridColumn16.CustomizationCaption = "Ажлын газар";
            this.gridColumn16.FieldName = "organizationName";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 6;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Албан тушаал";
            this.gridColumn17.CustomizationCaption = "Албан тушаал";
            this.gridColumn17.FieldName = "positionName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 7;
            // 
            // gridLookUpEditUser
            // 
            this.gridLookUpEditUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditUser.EditValue = "";
            this.gridLookUpEditUser.Location = new System.Drawing.Point(472, 30);
            this.gridLookUpEditUser.Name = "gridLookUpEditUser";
            this.gridLookUpEditUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditUser.Properties.NullText = "";
            this.gridLookUpEditUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditUser.Properties.ValueMember = "id";
            this.gridLookUpEditUser.Properties.View = this.gridView3;
            this.gridLookUpEditUser.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditUser.TabIndex = 2;
            this.gridLookUpEditUser.EditValueChanged += new System.EventHandler(this.gridLookUpEditUser_EditValueChanged);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Овог";
            this.gridColumn18.CustomizationCaption = "Овог";
            this.gridColumn18.FieldName = "lastName";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 0;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Нэр";
            this.gridColumn19.CustomizationCaption = "Нэр";
            this.gridColumn19.FieldName = "firstName";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 1;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Регистер";
            this.gridColumn20.CustomizationCaption = "Регистер";
            this.gridColumn20.FieldName = "id";
            this.gridColumn20.MinWidth = 75;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 2;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Эрх";
            this.gridColumn21.CustomizationCaption = "Эрх";
            this.gridColumn21.FieldName = "roleName";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 3;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Тасаг";
            this.gridColumn22.CustomizationCaption = "Тасаг";
            this.gridColumn22.FieldName = "wardName";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 4;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Ажлын газар";
            this.gridColumn23.CustomizationCaption = "Ажлын газар";
            this.gridColumn23.FieldName = "organizationName";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 5;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Албан тушаал";
            this.gridColumn24.CustomizationCaption = "Албан тушаал";
            this.gridColumn24.FieldName = "positionName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 6;
            // 
            // spinEditCount
            // 
            this.spinEditCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spinEditCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditCount.Location = new System.Drawing.Point(138, 353);
            this.spinEditCount.Name = "spinEditCount";
            this.spinEditCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditCount.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditCount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.EditFormat.FormatString = "n2";
            this.spinEditCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.Mask.EditMask = "n2";
            this.spinEditCount.Size = new System.Drawing.Size(100, 20);
            this.spinEditCount.TabIndex = 22;
            this.spinEditCount.EditValueChanged += new System.EventHandler(this.spinEditCount_ValueChanged);
            this.spinEditCount.Leave += new System.EventHandler(this.spinEditCount_Leave);
            // 
            // spinEditPrice
            // 
            this.spinEditPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.spinEditPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditPrice.Location = new System.Drawing.Point(472, 379);
            this.spinEditPrice.Name = "spinEditPrice";
            this.spinEditPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditPrice.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.EditFormat.FormatString = "n2";
            this.spinEditPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.Mask.EditMask = "n2";
            this.spinEditPrice.Properties.ReadOnly = true;
            this.spinEditPrice.Size = new System.Drawing.Size(100, 20);
            this.spinEditPrice.TabIndex = 27;
            // 
            // labelControlAll
            // 
            this.labelControlAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlAll.Location = new System.Drawing.Point(56, 382);
            this.labelControlAll.Name = "labelControlAll";
            this.labelControlAll.Size = new System.Drawing.Size(73, 13);
            this.labelControlAll.TabIndex = 41;
            this.labelControlAll.Text = "Олгосон эсэх :";
            // 
            // checkEditIsGiven
            // 
            this.checkEditIsGiven.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEditIsGiven.Location = new System.Drawing.Point(138, 379);
            this.checkEditIsGiven.Name = "checkEditIsGiven";
            this.checkEditIsGiven.Properties.Caption = "";
            this.checkEditIsGiven.Size = new System.Drawing.Size(19, 19);
            this.checkEditIsGiven.TabIndex = 23;
            this.checkEditIsGiven.Leave += new System.EventHandler(this.spinEditCount_Leave);
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl6.Location = new System.Drawing.Point(409, 408);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(57, 13);
            this.labelControl6.TabIndex = 8;
            this.labelControl6.Text = "Нийт үнэ*: ";
            // 
            // spinEditTotalSumPrice
            // 
            this.spinEditTotalSumPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.spinEditTotalSumPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditTotalSumPrice.Location = new System.Drawing.Point(472, 405);
            this.spinEditTotalSumPrice.Name = "spinEditTotalSumPrice";
            this.spinEditTotalSumPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditTotalSumPrice.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditTotalSumPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditTotalSumPrice.Properties.EditFormat.FormatString = "n2";
            this.spinEditTotalSumPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditTotalSumPrice.Properties.Mask.EditMask = "n2";
            this.spinEditTotalSumPrice.Properties.ReadOnly = true;
            this.spinEditTotalSumPrice.Size = new System.Drawing.Size(100, 20);
            this.spinEditTotalSumPrice.TabIndex = 28;
            // 
            // dateEditDate
            // 
            this.dateEditDate.EditValue = null;
            this.dateEditDate.Location = new System.Drawing.Point(138, 56);
            this.dateEditDate.Name = "dateEditDate";
            this.dateEditDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditDate.TabIndex = 3;
            this.dateEditDate.EditValueChanged += new System.EventHandler(this.dateEditDate_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(92, 59);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(40, 13);
            this.labelControl4.TabIndex = 42;
            this.labelControl4.Text = "Өдөр*: ";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 446);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 33;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // checkEditAll
            // 
            this.checkEditAll.Location = new System.Drawing.Point(12, 56);
            this.checkEditAll.Name = "checkEditAll";
            this.checkEditAll.Properties.Caption = "Бүгд";
            this.checkEditAll.Size = new System.Drawing.Size(47, 19);
            this.checkEditAll.TabIndex = 4;
            this.checkEditAll.CheckedChanged += new System.EventHandler(this.checkEditAll_CheckedChanged);
            // 
            // gridLookUpEditMedicine
            // 
            this.gridLookUpEditMedicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gridLookUpEditMedicine.EditValue = "";
            this.gridLookUpEditMedicine.Location = new System.Drawing.Point(138, 327);
            this.gridLookUpEditMedicine.Name = "gridLookUpEditMedicine";
            this.gridLookUpEditMedicine.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditMedicine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMedicine.Properties.DisplayMember = "name";
            this.gridLookUpEditMedicine.Properties.NullText = "";
            this.gridLookUpEditMedicine.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditMedicine.Properties.ValueMember = "medicinePrice";
            this.gridLookUpEditMedicine.Properties.View = this.gridView2;
            this.gridLookUpEditMedicine.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditMedicine_Properties_ButtonClick);
            this.gridLookUpEditMedicine.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditMedicine.TabIndex = 21;
            this.gridLookUpEditMedicine.InvalidValue += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(this.gridLookUpEditMedicine_InvalidValue);
            this.gridLookUpEditMedicine.EditValueChanged += new System.EventHandler(this.gridLookUpEditMedicine_EditValueChanged);
            this.gridLookUpEditMedicine.Validating += new System.ComponentModel.CancelEventHandler(this.gridLookUpEditMedicine_Validating);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn8,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn33});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Нэр";
            this.gridColumn1.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn1.FieldName = "name";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "ОУ нэр";
            this.gridColumn8.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn8.FieldName = "latinName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 1;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Ангилал";
            this.gridColumn25.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn25.FieldName = "medicineTypeName";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 2;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Тун хэмжээ";
            this.gridColumn26.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn26.FieldName = "unitType";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 5;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Үнэ";
            this.gridColumn27.CustomizationCaption = "Үнэ";
            this.gridColumn27.DisplayFormat.FormatString = "n2";
            this.gridColumn27.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn27.FieldName = "price";
            this.gridColumn27.MinWidth = 75;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 6;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Үлдэгдэл";
            this.gridColumn28.CustomizationCaption = "Үлдэгдэл";
            this.gridColumn28.DisplayFormat.FormatString = "n2";
            this.gridColumn28.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn28.FieldName = "count";
            this.gridColumn28.MinWidth = 75;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 7;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Бар код";
            this.gridColumn29.CustomizationCaption = "Бар код";
            this.gridColumn29.FieldName = "barcode";
            this.gridColumn29.MinWidth = 75;
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 9;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Хэлбэр";
            this.gridColumn30.CustomizationCaption = "Хэлбэр";
            this.gridColumn30.FieldName = "shape";
            this.gridColumn30.MinWidth = 75;
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 4;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Хүчинтэй хугацаа";
            this.gridColumn31.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn31.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.gridColumn31.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn31.FieldName = "validDate";
            this.gridColumn31.MinWidth = 75;
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 8;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Орлогын төрөл";
            this.gridColumn33.CustomizationCaption = "Орлогын төрөл";
            this.gridColumn33.FieldName = "incTypeName";
            this.gridColumn33.MinWidth = 75;
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 3;
            // 
            // spinEditBal
            // 
            this.spinEditBal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.spinEditBal.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditBal.Location = new System.Drawing.Point(472, 327);
            this.spinEditBal.Name = "spinEditBal";
            this.spinEditBal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditBal.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditBal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditBal.Properties.EditFormat.FormatString = "n2";
            this.spinEditBal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditBal.Properties.Mask.EditMask = "n2";
            this.spinEditBal.Properties.ReadOnly = true;
            this.spinEditBal.Size = new System.Drawing.Size(100, 20);
            this.spinEditBal.TabIndex = 25;
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl12.Location = new System.Drawing.Point(406, 330);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(60, 13);
            this.labelControl12.TabIndex = 46;
            this.labelControl12.Text = "Үлдэгдэл*: ";
            // 
            // spinEditPatientCount
            // 
            this.spinEditPatientCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spinEditPatientCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditPatientCount.Location = new System.Drawing.Point(138, 405);
            this.spinEditPatientCount.Name = "spinEditPatientCount";
            this.spinEditPatientCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditPatientCount.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditPatientCount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPatientCount.Properties.EditFormat.FormatString = "n2";
            this.spinEditPatientCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPatientCount.Properties.Mask.EditMask = "n2";
            this.spinEditPatientCount.Size = new System.Drawing.Size(100, 20);
            this.spinEditPatientCount.TabIndex = 24;
            this.spinEditPatientCount.EditValueChanged += new System.EventHandler(this.spinEditCount_ValueChanged);
            this.spinEditPatientCount.Leave += new System.EventHandler(this.spinEditCount_Leave);
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl3.Location = new System.Drawing.Point(18, 408);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(114, 13);
            this.labelControl3.TabIndex = 48;
            this.labelControl3.Text = "Эмчлүүлэгчээс авсан : ";
            // 
            // spinEditTotalCount
            // 
            this.spinEditTotalCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.spinEditTotalCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditTotalCount.Location = new System.Drawing.Point(472, 353);
            this.spinEditTotalCount.Name = "spinEditTotalCount";
            this.spinEditTotalCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditTotalCount.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditTotalCount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditTotalCount.Properties.EditFormat.FormatString = "n2";
            this.spinEditTotalCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditTotalCount.Properties.Mask.EditMask = "n2";
            this.spinEditTotalCount.Properties.ReadOnly = true;
            this.spinEditTotalCount.Size = new System.Drawing.Size(100, 20);
            this.spinEditTotalCount.TabIndex = 26;
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl7.Location = new System.Drawing.Point(406, 357);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(59, 13);
            this.labelControl7.TabIndex = 51;
            this.labelControl7.Text = "Нийт тоо*: ";
            // 
            // MedicinePatientInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(684, 481);
            this.Controls.Add(this.spinEditTotalCount);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.spinEditPatientCount);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.spinEditBal);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.gridLookUpEditMedicine);
            this.Controls.Add(this.checkEditAll);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.dateEditDate);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControlAll);
            this.Controls.Add(this.checkEditIsGiven);
            this.Controls.Add(this.spinEditTotalSumPrice);
            this.Controls.Add(this.spinEditPrice);
            this.Controls.Add(this.spinEditCount);
            this.Controls.Add(this.gridLookUpEditUser);
            this.Controls.Add(this.gridLookUpEditPatient);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.gridControlDetail);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonSave);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MedicinePatientInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Эмчлүүлэгчид эм олгох";
            this.Shown += new System.EventHandler(this.MedicinePatientInForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsGiven.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTotalSumPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPatientCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTotalCount.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraGrid.GridControl gridControlDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDetail;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPatient;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraEditors.SpinEdit spinEditCount;
        private DevExpress.XtraEditors.SpinEdit spinEditPrice;
        private DevExpress.XtraEditors.LabelControl labelControlAll;
        private DevExpress.XtraEditors.CheckEdit checkEditIsGiven;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SpinEdit spinEditTotalSumPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIsGive;
        private DevExpress.XtraEditors.DateEdit dateEditDate;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEditAll;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMedicine;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraEditors.SpinEdit spinEditBal;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraEditors.SpinEdit spinEditPatientCount;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SpinEdit spinEditTotalCount;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
    }
}