﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.report.journal.patient;

namespace Pharmacy2016.gui.core.journal.patient
{
    /*
     * Эмчлүүлэгчийн хэрэглэх эмийг өвчний түүхээс нь татах цонх.
     * 
     */

    public partial class PatientChooseForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц
        
            private static PatientChooseForm INSTANCE = new PatientChooseForm();

            public static PatientChooseForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private PatientChooseForm()
            {

            }

            
            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                gridLookUpEditPatient.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditPatient.Properties.DataSource = JournalDataSet.PatientOther1BindingSource;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    ShowDialog(MedicinePatientForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                clearErrorText();
                gridLookUpEditPatient.EditValue = null;
            }

            private void clearErrorText()
            {
                gridLookUpEditPatient.ErrorText = "";
            }

            private void MedicineDownForm_Shown(object sender, EventArgs e)
            {
                gridLookUpEditPatient.Focus();
            }

        #endregion

        #region Формын event

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion  

        #region Формын функц

            private bool check()
            {
                clearErrorText();
                bool isRight = true;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditPatient.EditValue == DBNull.Value || gridLookUpEditPatient.EditValue == null || gridLookUpEditPatient.EditValue.ToString().Equals(""))
                {
                    string errorText = "Эмчлүүлэгчийг сонгоно уу";
                    string text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditPatient.ErrorText = errorText;
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                    isRight = false;
                    gridLookUpEditPatient.Focus();
                }
                
                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    PatientMedicineUserReport.Instance.initAndShow((DataRowView)gridLookUpEditPatient.GetSelectedDataRow(), radioGroupType.SelectedIndex);
                    //Close();
                }
            }

        #endregion

    }
}