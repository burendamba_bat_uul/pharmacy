﻿namespace Pharmacy2016.gui.core.journal.patient
{
    partial class MedicinePatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicinePatientForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridFieldDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPatient = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMedicine = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldIsGive = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPrice = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSumPrice = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldIncTypeName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.controlNavigator = new DevExpress.XtraEditors.ControlNavigator();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemPatient = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAllPatient = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEndBal = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.checkedComboBoxEdit = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.comboBoxEditPatientType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlAll = new DevExpress.XtraEditors.LabelControl();
            this.checkEditAll = new DevExpress.XtraEditors.CheckEdit();
            this.gridLookUpEditPatient = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPatientType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pivotGridControl);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 461);
            this.panelControl1.TabIndex = 0;
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldDate,
            this.pivotGridFieldPatient,
            this.pivotGridFieldMedicine,
            this.pivotGridFieldIsGive,
            this.pivotGridFieldPrice,
            this.pivotGridFieldCount,
            this.pivotGridFieldSumPrice,
            this.pivotGridFieldMonth,
            this.pivotGridFieldID,
            this.pivotGridFieldIncTypeName});
            this.pivotGridControl.Location = new System.Drawing.Point(2, 73);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsFilterPopup.IsRadioMode = true;
            this.pivotGridControl.OptionsSelection.CellSelection = false;
            this.pivotGridControl.OptionsSelection.MultiSelect = false;
            this.pivotGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemSpinEdit1});
            this.pivotGridControl.Size = new System.Drawing.Size(780, 360);
            this.pivotGridControl.TabIndex = 2;
            this.pivotGridControl.CustomCellValue += new System.EventHandler<DevExpress.XtraPivotGrid.PivotCellValueEventArgs>(this.pivotGridControl_CustomCellValue);
            this.pivotGridControl.CellDoubleClick += new DevExpress.XtraPivotGrid.PivotCellEventHandler(this.pivotGridControl_CellDoubleClick);
            this.pivotGridControl.CustomDrawCell += new DevExpress.XtraPivotGrid.PivotCustomDrawCellEventHandler(this.pivotGridControl_CustomDrawCell);
            this.pivotGridControl.CustomAppearance += new DevExpress.XtraPivotGrid.PivotCustomAppearanceEventHandler(this.pivotGridControl_CustomAppearance);
            // 
            // pivotGridFieldDate
            // 
            this.pivotGridFieldDate.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldDate.AreaIndex = 1;
            this.pivotGridFieldDate.Caption = "Өдөр";
            this.pivotGridFieldDate.FieldName = "date";
            this.pivotGridFieldDate.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateDay;
            this.pivotGridFieldDate.Name = "pivotGridFieldDate";
            this.pivotGridFieldDate.UnboundFieldName = "pivotGridFieldDate";
            this.pivotGridFieldDate.Width = 30;
            // 
            // pivotGridFieldPatient
            // 
            this.pivotGridFieldPatient.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldPatient.AreaIndex = 0;
            this.pivotGridFieldPatient.Caption = "Эмчлүүлэгч";
            this.pivotGridFieldPatient.FieldName = "patientName";
            this.pivotGridFieldPatient.MinWidth = 0;
            this.pivotGridFieldPatient.Name = "pivotGridFieldPatient";
            this.pivotGridFieldPatient.Width = 180;
            // 
            // pivotGridFieldMedicine
            // 
            this.pivotGridFieldMedicine.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldMedicine.AreaIndex = 1;
            this.pivotGridFieldMedicine.Caption = "Эм";
            this.pivotGridFieldMedicine.FieldName = "medicineName";
            this.pivotGridFieldMedicine.Name = "pivotGridFieldMedicine";
            this.pivotGridFieldMedicine.Width = 180;
            // 
            // pivotGridFieldIsGive
            // 
            this.pivotGridFieldIsGive.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldIsGive.AreaIndex = 0;
            this.pivotGridFieldIsGive.Caption = "Олгосон эсэх";
            this.pivotGridFieldIsGive.FieldName = "isGive";
            this.pivotGridFieldIsGive.Name = "pivotGridFieldIsGive";
            this.pivotGridFieldIsGive.Options.ReadOnly = true;
            this.pivotGridFieldIsGive.Options.ShowCustomTotals = false;
            this.pivotGridFieldIsGive.Options.ShowGrandTotal = false;
            this.pivotGridFieldIsGive.Options.ShowTotals = false;
            this.pivotGridFieldIsGive.UnboundFieldName = "id";
            // 
            // pivotGridFieldPrice
            // 
            this.pivotGridFieldPrice.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldPrice.AreaIndex = 3;
            this.pivotGridFieldPrice.Caption = "Үнэ";
            this.pivotGridFieldPrice.FieldName = "price";
            this.pivotGridFieldPrice.Name = "pivotGridFieldPrice";
            this.pivotGridFieldPrice.UnboundFieldName = "pivotGridFieldPrice";
            this.pivotGridFieldPrice.ValueFormat.FormatString = "n";
            this.pivotGridFieldPrice.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldPrice.Width = 70;
            // 
            // pivotGridFieldCount
            // 
            this.pivotGridFieldCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldCount.AreaIndex = 1;
            this.pivotGridFieldCount.Caption = "Тоо";
            this.pivotGridFieldCount.CellFormat.FormatString = "n";
            this.pivotGridFieldCount.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.FieldName = "totalCount";
            this.pivotGridFieldCount.GrandTotalCellFormat.FormatString = "n2";
            this.pivotGridFieldCount.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Name = "pivotGridFieldCount";
            this.pivotGridFieldCount.Options.ReadOnly = true;
            this.pivotGridFieldCount.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Value;
            this.pivotGridFieldCount.TotalValueFormat.FormatString = "n2";
            this.pivotGridFieldCount.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.ValueFormat.FormatString = "n";
            this.pivotGridFieldCount.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Width = 50;
            // 
            // pivotGridFieldSumPrice
            // 
            this.pivotGridFieldSumPrice.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldSumPrice.AreaIndex = 2;
            this.pivotGridFieldSumPrice.Caption = "Нийт үнэ";
            this.pivotGridFieldSumPrice.CellFormat.FormatString = "n0";
            this.pivotGridFieldSumPrice.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.FieldName = "totalSumPrice";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.Name = "pivotGridFieldSumPrice";
            this.pivotGridFieldSumPrice.Options.ReadOnly = true;
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.ValueFormat.FormatString = "n";
            this.pivotGridFieldSumPrice.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldMonth
            // 
            this.pivotGridFieldMonth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldMonth.AreaIndex = 0;
            this.pivotGridFieldMonth.Caption = "Сар";
            this.pivotGridFieldMonth.FieldName = "date";
            this.pivotGridFieldMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.pivotGridFieldMonth.Name = "pivotGridFieldMonth";
            this.pivotGridFieldMonth.UnboundFieldName = "pivotGridFieldMonth";
            // 
            // pivotGridFieldID
            // 
            this.pivotGridFieldID.AreaIndex = 0;
            this.pivotGridFieldID.Caption = "Дугаар";
            this.pivotGridFieldID.FieldName = "id";
            this.pivotGridFieldID.MinWidth = 0;
            this.pivotGridFieldID.Name = "pivotGridFieldID";
            this.pivotGridFieldID.Options.ReadOnly = true;
            // 
            // pivotGridFieldIncTypeName
            // 
            this.pivotGridFieldIncTypeName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldIncTypeName.AreaIndex = 2;
            this.pivotGridFieldIncTypeName.Caption = "Төрөл";
            this.pivotGridFieldIncTypeName.FieldName = "incTypeName";
            this.pivotGridFieldIncTypeName.Name = "pivotGridFieldIncTypeName";
            this.pivotGridFieldIncTypeName.Width = 80;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n0";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n0";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.controlNavigator);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 433);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(780, 26);
            this.panelControl3.TabIndex = 4;
            // 
            // controlNavigator
            // 
            this.controlNavigator.Buttons.Append.Visible = false;
            this.controlNavigator.Buttons.CancelEdit.Visible = false;
            this.controlNavigator.Buttons.Edit.Visible = false;
            this.controlNavigator.Buttons.EndEdit.Visible = false;
            this.controlNavigator.Buttons.First.Visible = false;
            this.controlNavigator.Buttons.Last.Visible = false;
            this.controlNavigator.Buttons.Next.Visible = false;
            this.controlNavigator.Buttons.NextPage.Visible = false;
            this.controlNavigator.Buttons.Prev.Visible = false;
            this.controlNavigator.Buttons.PrevPage.Visible = false;
            this.controlNavigator.Buttons.Remove.Visible = false;
            this.controlNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 11, true, true, "Эмчилгээ татах", "download"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "update")});
            this.controlNavigator.Dock = System.Windows.Forms.DockStyle.Left;
            this.controlNavigator.Location = new System.Drawing.Point(2, 2);
            this.controlNavigator.Name = "controlNavigator";
            this.controlNavigator.ShowToolTips = true;
            this.controlNavigator.Size = new System.Drawing.Size(90, 22);
            this.controlNavigator.TabIndex = 3;
            this.controlNavigator.Text = "controlNavigator1";
            this.controlNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.controlNavigator_ButtonClick);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.dropDownButtonPrint);
            this.panelControl2.Controls.Add(this.checkedComboBoxEdit);
            this.panelControl2.Controls.Add(this.comboBoxEditPatientType);
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Controls.Add(this.labelControlAll);
            this.panelControl2.Controls.Add(this.checkEditAll);
            this.panelControl2.Controls.Add(this.gridLookUpEditPatient);
            this.panelControl2.Controls.Add(this.dateEditEnd);
            this.panelControl2.Controls.Add(this.dateEditStart);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(780, 71);
            this.panelControl2.TabIndex = 1;
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.DropDownControl = this.popupMenu;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(640, 10);
            this.dropDownButtonPrint.MenuManager = this.barManager;
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.barManager.SetPopupContextMenu(this.dropDownButtonPrint, this.popupMenu);
            this.dropDownButtonPrint.Size = new System.Drawing.Size(130, 25);
            this.dropDownButtonPrint.TabIndex = 17;
            this.dropDownButtonPrint.Text = "Хэвлэх";
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPatient),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAllPatient),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemEndBal),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // barButtonItemPatient
            // 
            this.barButtonItemPatient.Caption = "Эмчлүүлэгч";
            this.barButtonItemPatient.Description = "Эмчлүүлэгчийн хэрэглэсэн эмийн тайлан";
            this.barButtonItemPatient.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPatient.Glyph")));
            this.barButtonItemPatient.Id = 0;
            this.barButtonItemPatient.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPatient.LargeGlyph")));
            this.barButtonItemPatient.Name = "barButtonItemPatient";
            this.barButtonItemPatient.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemAllPatient
            // 
            this.barButtonItemAllPatient.Caption = "Нийт эмчлүүлэгч";
            this.barButtonItemAllPatient.Description = "Нийт эмчлүүлэгч хэрэглэсэн эмийн тайлан";
            this.barButtonItemAllPatient.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAllPatient.Glyph")));
            this.barButtonItemAllPatient.Id = 1;
            this.barButtonItemAllPatient.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAllPatient.LargeGlyph")));
            this.barButtonItemAllPatient.Name = "barButtonItemAllPatient";
            this.barButtonItemAllPatient.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemBegBal_ItemClick);
            // 
            // barButtonItemEndBal
            // 
            this.barButtonItemEndBal.Caption = "Эцсийн үлдэгдэл";
            this.barButtonItemEndBal.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.Glyph")));
            this.barButtonItemEndBal.Id = 2;
            this.barButtonItemEndBal.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.LargeGlyph")));
            this.barButtonItemEndBal.Name = "barButtonItemEndBal";
            this.barButtonItemEndBal.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemEndBal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEndBal_ItemClick);
            // 
            // barButtonItemConvert
            // 
            this.barButtonItemConvert.Caption = "Хөрвүүлэх";
            this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
            this.barButtonItemConvert.Id = 3;
            this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
            this.barButtonItemConvert.Name = "barButtonItemConvert";
            this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemPatient,
            this.barButtonItemAllPatient,
            this.barButtonItemEndBal,
            this.barButtonItemConvert});
            this.barManager.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 461);
            this.barDockControlBottom.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 461);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(784, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 461);
            // 
            // checkedComboBoxEdit
            // 
            this.checkedComboBoxEdit.EditValue = "isGive";
            this.checkedComboBoxEdit.Location = new System.Drawing.Point(202, 13);
            this.checkedComboBoxEdit.Name = "checkedComboBoxEdit";
            this.checkedComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEdit.Properties.DropDownRows = 4;
            this.checkedComboBoxEdit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("isGive", "Олгосон эсэх", System.Windows.Forms.CheckState.Checked),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("count", "Тоо хэмжээ"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("sumPrice", "Нийт үнэ")});
            this.checkedComboBoxEdit.Size = new System.Drawing.Size(150, 20);
            this.checkedComboBoxEdit.TabIndex = 16;
            this.checkedComboBoxEdit.ToolTip = "Харагдах багана сонгох";
            this.checkedComboBoxEdit.EditValueChanged += new System.EventHandler(this.checkedComboBoxEdit_EditValueChanged);
            // 
            // comboBoxEditPatientType
            // 
            this.comboBoxEditPatientType.EditValue = "Эмчлүүлж байгаа";
            this.comboBoxEditPatientType.Location = new System.Drawing.Point(10, 39);
            this.comboBoxEditPatientType.Name = "comboBoxEditPatientType";
            this.comboBoxEditPatientType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditPatientType.Properties.Items.AddRange(new object[] {
            "Эмчлүүлж байгаа",
            "Эмнэлэгээс гарсан"});
            this.comboBoxEditPatientType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditPatientType.Size = new System.Drawing.Size(120, 20);
            this.comboBoxEditPatientType.TabIndex = 15;
            this.comboBoxEditPatientType.ToolTip = "Эмчлүүлэгчийн төрөл";
            this.comboBoxEditPatientType.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditPatientType_SelectedIndexChanged);
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(358, 37);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 25);
            this.simpleButtonReload.TabIndex = 14;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Эмчлүүлэгчийн эм сонгох";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // labelControlAll
            // 
            this.labelControlAll.Location = new System.Drawing.Point(141, 42);
            this.labelControlAll.Name = "labelControlAll";
            this.labelControlAll.Size = new System.Drawing.Size(30, 13);
            this.labelControlAll.TabIndex = 13;
            this.labelControlAll.Text = "Бүгд :";
            // 
            // checkEditAll
            // 
            this.checkEditAll.Location = new System.Drawing.Point(177, 40);
            this.checkEditAll.Name = "checkEditAll";
            this.checkEditAll.Properties.Caption = "";
            this.checkEditAll.Size = new System.Drawing.Size(19, 19);
            this.checkEditAll.TabIndex = 12;
            this.checkEditAll.ToolTip = "Бүгдийг сонгох";
            this.checkEditAll.CheckedChanged += new System.EventHandler(this.checkEditAll_CheckedChanged);
            // 
            // gridLookUpEditPatient
            // 
            this.gridLookUpEditPatient.EditValue = "Эмчлүүлэгч сонгох";
            this.gridLookUpEditPatient.Location = new System.Drawing.Point(202, 39);
            this.gridLookUpEditPatient.Name = "gridLookUpEditPatient";
            this.gridLookUpEditPatient.Properties.ActionButtonIndex = 1;
            serializableAppearanceObject1.Options.UseImage = true;
            this.gridLookUpEditPatient.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleRight, ((System.Drawing.Image)(resources.GetObject("gridLookUpEditPatient.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "reload", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPatient.Properties.DisplayMember = "fullName";
            this.gridLookUpEditPatient.Properties.NullText = "Эмчлүүлэгч сонгох";
            this.gridLookUpEditPatient.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditPatient.Properties.ValueMember = "id";
            this.gridLookUpEditPatient.Properties.View = this.gridView1;
            this.gridLookUpEditPatient.Size = new System.Drawing.Size(150, 22);
            this.gridLookUpEditPatient.TabIndex = 11;
            this.gridLookUpEditPatient.ToolTip = "Эмчлүүлэгч сонгох";
            this.gridLookUpEditPatient.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditPatient_ButtonClick);
            this.gridLookUpEditPatient.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.gridLookUpEditPatient_CustomDisplayText);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn3,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Нэр";
            this.gridColumn2.CustomizationCaption = "Нэр";
            this.gridColumn2.FieldName = "firstName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Регистер";
            this.gridColumn4.CustomizationCaption = "Регистер";
            this.gridColumn4.FieldName = "id";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Төрсөн өдөр";
            this.gridColumn5.CustomizationCaption = "Төрсөн өдөр";
            this.gridColumn5.FieldName = "birthday";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Утас";
            this.gridColumn3.CustomizationCaption = "Утас";
            this.gridColumn3.FieldName = "phone";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 5;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тасаг";
            this.gridColumn6.CustomizationCaption = "Тасаг";
            this.gridColumn6.FieldName = "wardName";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Ажлын газар";
            this.gridColumn7.CustomizationCaption = "Ажлын газар";
            this.gridColumn7.FieldName = "organizationName";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Албан тушаал";
            this.gridColumn8.CustomizationCaption = "Албан тушаал";
            this.gridColumn8.FieldName = "positionName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 8;
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(96, 13);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(80, 20);
            this.dateEditEnd.TabIndex = 10;
            this.dateEditEnd.ToolTip = "Дуусах огноо";
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(10, 13);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(80, 20);
            this.dateEditStart.TabIndex = 9;
            this.dateEditStart.ToolTip = "Эхлэх огноо";
            // 
            // MedicinePatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MedicinePatientForm";
            this.ShowInTaskbar = false;
            this.Text = "Эмчлүүлэгчид эм олгох журнал";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MedicinePatient_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditPatientType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDate;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPatient;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMedicine;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldIsGive;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCount;
        private DevExpress.XtraEditors.LabelControl labelControlAll;
        private DevExpress.XtraEditors.CheckEdit checkEditAll;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPatient;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.ControlNavigator controlNavigator;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSumPrice;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMonth;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldID;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditPatientType;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEdit;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPatient;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAllPatient;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEndBal;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldIncTypeName;
    }
}