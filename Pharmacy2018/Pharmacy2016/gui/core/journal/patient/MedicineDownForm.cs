﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;

namespace Pharmacy2016.gui.core.journal.patient
{
    /*
     * Эмчлүүлэгчийн хэрэглэх эмийг өвчний түүхээс нь татах цонх.
     * 
     */

    public partial class MedicineDownForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц
        
            private static MedicineDownForm INSTANCE = new MedicineDownForm();

            public static MedicineDownForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private MedicineDownForm()
            {

            }

            
            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                reload();
            }

            private void initDataSource()
            {
                //gridLookUpEditUser.Properties.DataSource = InformationDataSet.PharmacistOtherBindingSource;
                gridLookUpEditPatient.Properties.DataSource = JournalDataSet.PatientOther1BindingSource;
            }

            private void initError()
            {
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditPatient.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditEnd.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    JournalDataSet.PatientOther1BindingSource.Filter = "isClose = 0";
                    if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                    {
                        gridLookUpEditUser.Properties.ReadOnly = true;
                        gridLookUpEditUser.EditValue = UserUtil.getUserId();
                    }
                    else
                    {
                        gridLookUpEditUser.Properties.ReadOnly = false;
                        gridLookUpEditUser.EditValue = null;
                    }
                    //gridLookUpEditPatient.EditValue = MedicinePatientForm.Instance.getSelectedPatient();
                    //if (!gridLookUpEditUser.Properties.ReadOnly && gridLookUpEditPatient.EditValue != null && gridLookUpEditPatient.EditValue.ToString() != "")
                    //{
                    //    DataRowView view = (DataRowView)gridLookUpEditPatient.GetSelectedDataRow();
                    //    InformationDataSet.PharmacistOtherBindingSource.Filter = "wardID = '" + view["wardID"] + "'";
                    //}
                    dateEditStart.DateTime = MedicinePatientForm.Instance.getStartDate();
                    dateEditEnd.DateTime = MedicinePatientForm.Instance.getEndDate();

                    ShowDialog(MedicinePatientForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                clearErrorText();

                //InformationDataSet.PharmacistOtherBindingSource.Filter = "";
                JournalDataSet.PatientOther1BindingSource.Filter = "";
                //InformationDataSet.PharmacistOtherBindingSource.Filter = "";
                gridLookUpEditUser.EditValue = null;
                gridLookUpEditPatient.EditValue = null;
            }

            private void clearErrorText()
            {
                gridLookUpEditUser.ErrorText = "";
                gridLookUpEditPatient.ErrorText = "";
                dateEditStart.ErrorText = "";
                dateEditEnd.ErrorText = "";
            }

            private void MedicineDownForm_Shown(object sender, EventArgs e)
            {
                if (gridLookUpEditPatient.EditValue != DBNull.Value && gridLookUpEditPatient.EditValue != null && !gridLookUpEditPatient.Text.Equals(""))
                {
                    gridLookUpEditUser.Focus();
                }
                else 
                {
                    gridLookUpEditPatient.Focus();
                }
            }

        #endregion

        #region Формын event

            private void gridLookUpEditPatient_EditValueChanged(object sender, EventArgs e)
            {
                if (!gridLookUpEditUser.Properties.ReadOnly && gridLookUpEditPatient.EditValue != null && gridLookUpEditPatient.EditValue.ToString() != "")
                {
                    DataRowView view = (DataRowView)gridLookUpEditPatient.GetSelectedDataRow();
                    //InformationDataSet.PharmacistOtherBindingSource.Filter = "wardID = '" + view["wardID"] + "'";
                }
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion  

        #region Формын функц

            private void reload()
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                //InformationDataSet.PharmacistOtherTableAdapter.Fill(InformationDataSet.Instance.PharmacistOther, HospitalUtil.getHospitalId(), MedicinePatientForm.Instance.wardID);
                //ProgramUtil.closeWaitDialog();
            }

            private bool check()
            {
                clearErrorText();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditPatient.EditValue == DBNull.Value || gridLookUpEditPatient.EditValue == null || gridLookUpEditPatient.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмчлүүлэгчийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditPatient.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditPatient.Focus();
                }
                if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.EditValue.ToString().Equals(""))
                {
                    errorText = "Эм зүйчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditUser.Focus();
                    }
                }
                if (dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо дуусах огнооноос хойно байж болохгүй";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditStart.Focus();
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    JournalDataSet.QueriesTableAdapter.download_patient_medicine(gridLookUpEditPatient.EditValue.ToString(), gridLookUpEditUser.EditValue.ToString(), ((DataRowView)gridLookUpEditPatient.GetSelectedDataRow())["storeID"].ToString(), dateEditStart.Text, dateEditEnd.Text, HospitalUtil.getHospitalId(), UserUtil.getUserId());
                    MedicinePatientForm.Instance.reloadPatient(dateEditStart.DateTime, dateEditEnd.DateTime, gridLookUpEditPatient.EditValue.ToString());
                        
                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }

        #endregion

    }
}