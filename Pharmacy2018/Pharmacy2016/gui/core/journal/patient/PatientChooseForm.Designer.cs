﻿namespace Pharmacy2016.gui.core.journal.patient
{
    partial class PatientChooseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridLookUpEditPatient = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonPrint = new DevExpress.XtraEditors.SimpleButton();
            this.radioGroupType = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridLookUpEditPatient
            // 
            this.gridLookUpEditPatient.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditPatient.EditValue = "";
            this.gridLookUpEditPatient.Location = new System.Drawing.Point(122, 30);
            this.gridLookUpEditPatient.Name = "gridLookUpEditPatient";
            this.gridLookUpEditPatient.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPatient.Properties.DisplayMember = "fullName";
            this.gridLookUpEditPatient.Properties.NullText = "";
            this.gridLookUpEditPatient.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditPatient.Properties.ValueMember = "id";
            this.gridLookUpEditPatient.Properties.View = this.gridView1;
            this.gridLookUpEditPatient.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditPatient.TabIndex = 1;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Овог";
            this.gridColumn10.CustomizationCaption = "Овог";
            this.gridColumn10.FieldName = "lastName";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Нэр";
            this.gridColumn11.CustomizationCaption = "Нэр";
            this.gridColumn11.FieldName = "firstName";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Регистер";
            this.gridColumn12.CustomizationCaption = "Регистер";
            this.gridColumn12.FieldName = "id";
            this.gridColumn12.MinWidth = 75;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Төрсөн өдөр";
            this.gridColumn13.CustomizationCaption = "Төрсөн өдөр";
            this.gridColumn13.FieldName = "birthday";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Утас";
            this.gridColumn14.CustomizationCaption = "Утас";
            this.gridColumn14.FieldName = "phone";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 4;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Тасаг";
            this.gridColumn15.CustomizationCaption = "Тасаг";
            this.gridColumn15.FieldName = "wardName";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 3;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Ажлын газар";
            this.gridColumn16.CustomizationCaption = "Ажлын газар";
            this.gridColumn16.FieldName = "organizationName";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 6;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Албан тушаал";
            this.gridColumn17.CustomizationCaption = "Албан тушаал";
            this.gridColumn17.FieldName = "positionName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 7;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(46, 32);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(70, 13);
            this.labelControl2.TabIndex = 44;
            this.labelControl2.Text = "Эмчлүүлэгч*: ";
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(297, 156);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 12;
            this.simpleButtonCancel.Text = "Болих";
            this.simpleButtonCancel.ToolTip = "Болих";
            // 
            // simpleButtonPrint
            // 
            this.simpleButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonPrint.Location = new System.Drawing.Point(216, 156);
            this.simpleButtonPrint.Name = "simpleButtonPrint";
            this.simpleButtonPrint.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonPrint.TabIndex = 11;
            this.simpleButtonPrint.Text = "Хэвлэх";
            this.simpleButtonPrint.ToolTip = "Тайлан хэвлэх";
            this.simpleButtonPrint.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // radioGroupType
            // 
            this.radioGroupType.Location = new System.Drawing.Point(122, 56);
            this.radioGroupType.Name = "radioGroupType";
            this.radioGroupType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Эмнэлэгээс олгосон"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Эмчлүүлэгчээс олгосон"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Нийт олгосон")});
            this.radioGroupType.Size = new System.Drawing.Size(200, 69);
            this.radioGroupType.TabIndex = 45;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(73, 64);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(43, 13);
            this.labelControl1.TabIndex = 46;
            this.labelControl1.Text = "Төрөл*: ";
            // 
            // PatientChooseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(384, 191);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.radioGroupType);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonPrint);
            this.Controls.Add(this.gridLookUpEditPatient);
            this.Controls.Add(this.labelControl2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PatientChooseForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Эмчлүүлэгчийн тайлан";
            this.Shown += new System.EventHandler(this.MedicineDownForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPatient;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonPrint;
        private DevExpress.XtraEditors.RadioGroup radioGroupType;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}