﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.bal;
using Pharmacy2016.gui.core.journal.plan;

namespace Pharmacy2016.gui.core.journal.patient
{
    /*
     * Эмчлүүлэгчийн хэрэглэсэн эмийг нэмэх, засах, устгах цонх.
     * 
     */

    public partial class MedicinePatientInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicinePatientInUpForm INSTANCE = new MedicinePatientInUpForm();

            public static MedicinePatientInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private string beforeDate;

            private List<string> deleteRows = new List<string>();

            private MedicinePatientInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                dateEditDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                initError();
                initDataSource();
                reload();
            }

            private void initDataSource()
            {
                gridControlDetail.DataSource = JournalDataSet.Instance.PatientMedicineOther;
                gridLookUpEditUser.Properties.DataSource = PlanDataSet.SystemDoctorBindingSource;
                gridLookUpEditPatient.Properties.DataSource = JournalDataSet.PatientOther1BindingSource;
                gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineBalBindingSource;
            }

            private void initError()
            {
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditPatient.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                    {
                        gridLookUpEditUser.Properties.ReadOnly = true;
                        gridLookUpEditUser.EditValue = UserUtil.getUserId();
                        gridLookUpEditMedicine.Properties.ReadOnly = false;
                        spinEditCount.Properties.ReadOnly = false;
                    }
                    else if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                    {
                        gridLookUpEditUser.Properties.ReadOnly = false;
                        gridLookUpEditUser.EditValue = null;
                        gridLookUpEditMedicine.Properties.ReadOnly = true;
                        spinEditCount.Properties.ReadOnly = true;
                    }
                    else
                    {
                        gridLookUpEditUser.Properties.ReadOnly = false;
                        gridLookUpEditUser.EditValue = null;
                        gridLookUpEditMedicine.Properties.ReadOnly = false;
                        spinEditCount.Properties.ReadOnly = false;
                    }
                    if (actionType == 1)
                    {
                        insert();
                    }
                    else if (actionType == 2)
                    {
                        update();
                    }

                    ShowDialog(MedicinePatientForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insert()
            {
                JournalDataSet.PatientOther1BindingSource.Filter = "isClose = 0";
                dateEditDate.DateTime = DateTime.Now;
                gridLookUpEditPatient.EditValue = MedicinePatientForm.Instance.getSelectedPatient();
                if (!gridLookUpEditUser.Properties.ReadOnly && gridLookUpEditPatient.EditValue != null && gridLookUpEditPatient.EditValue != DBNull.Value && !gridLookUpEditPatient.Text.Equals(""))
                {
                    DataRowView view = (DataRowView)gridLookUpEditPatient.GetSelectedDataRow();
                    //InformationDataSet.PharmacistOtherBindingSource.Filter = "wardID = '" + view["wardID"] + "'";
                }

                dateEditDate.ReadOnly = false;
                gridLookUpEditPatient.Properties.ReadOnly = false;
                gridLookUpEditUser.Properties.ReadOnly = (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID);

                checkEditAll.Properties.ReadOnly = false;
                gridLookUpEditMedicine.Properties.ReadOnly = false;
                spinEditCount.Properties.ReadOnly = false;
                checkEditIsGiven.Properties.ReadOnly = false;
                gridColumnIsGive.OptionsColumn.ReadOnly = false;
                simpleButtonReload.Visible = true;
                simpleButtonSave.Visible = true;
            }

            private void update()
            {
                string[] values = MedicinePatientForm.Instance.getSelectedValues();

                beforeDate = values[0];
                dateEditDate.EditValue = values[0];
                gridLookUpEditUser.EditValue = values[1];
                gridLookUpEditPatient.EditValue = values[2];
                if (gridLookUpEditPatient.EditValue != null && gridLookUpEditPatient.EditValue != DBNull.Value && !gridLookUpEditPatient.Text.Equals(""))
                {
                    DataRowView view = (DataRowView)gridLookUpEditPatient.GetSelectedDataRow();
                    //InformationDataSet.PharmacistOtherBindingSource.Filter = "wardID = '" + view["wardID"] + "'";
                }
                
                dateEditDate.ReadOnly = true;
                gridLookUpEditUser.Properties.ReadOnly = true;
                gridLookUpEditPatient.Properties.ReadOnly = true;

                bool readOnly = Convert.ToBoolean(values[4]);
                checkEditAll.Properties.ReadOnly = readOnly;
                gridLookUpEditMedicine.Properties.ReadOnly = readOnly;
                spinEditCount.Properties.ReadOnly = readOnly;
                checkEditIsGiven.Properties.ReadOnly = readOnly;
                gridColumnIsGive.OptionsColumn.ReadOnly = readOnly;
                simpleButtonReload.Visible = !readOnly;
                simpleButtonSave.Visible = !readOnly;
                if (!readOnly)
                    JournalDataSet.PatientOther1BindingSource.Filter = "isClose = 0";
                
                DataRow[] rows = JournalDataSet.Instance.PatientMedicine.Select("patientID = '" + values[2] + "' AND date = '" + values[0] + "'");
                JournalDataSet.Instance.PatientMedicineOther.Rows.Clear();
                
                for (int i = 0; i < rows.Length; i++)
                {
                    DataRow row = JournalDataSet.Instance.PatientMedicineOther.Rows.Add();

                    row["id"] = rows[i]["id"];
                    row["userID"] = rows[i]["userID"];
                    row["patientID"] = rows[i]["patientID"];
                    row["userName"] = rows[i]["userName"];
                    row["patientName"] = rows[i]["patientName"];
                    row["date"] = rows[i]["date"];
                    row["medicineID"] = rows[i]["medicineID"];
                    row["medicineName"] = rows[i]["medicineName"];
                    row["latinName"] = rows[i]["latinName"];
                    row["unitType"] = rows[i]["unitType"];
                    row["medicineTypeName"] = rows[i]["medicineTypeName"];
                    row["shape"] = rows[i]["shape"];
                    row["validDate"] = rows[i]["validDate"];
                    row["price"] = rows[i]["price"];
                    row["count"] = rows[i]["count"];
                    row["isGive"] = rows[i]["isGive"];
                    row["isDownload"] = rows[i]["isDownload"];
                    row["giveUserID"] = rows[i]["giveUserID"];
                    row["giveUserName"] = rows[i]["giveUserName"];
                    row["incTypeID"] = rows[i]["incTypeID"];
                    row["incTypeName"] = rows[i]["incTypeName"];
                    row["patientCount"] = rows[i]["patientCount"];
                    //row["medicinePrice"] = rows[i]["medicinePrice"];

                    row.EndEdit();
                }
                gridViewDetail.FocusedRowHandle = 0;
            }


            private void clearData()
            {
                clearErrorText();

                beforeDate = null;
                deleteRows.Clear();

                JournalDataSet.PatientOther1BindingSource.Filter = "";
                //InformationDataSet.PharmacistOtherBindingSource.Filter = "";
                JournalDataSet.Instance.PatientMedicineOther.Clear();
                JournalDataSet.Instance.MedicineBal.Clear();

                checkEditAll.Checked = false;
                gridLookUpEditUser.EditValue = null;
                gridLookUpEditPatient.EditValue = null;
                clearMedicine();
            }

            private void clearMedicine()
            {
                gridLookUpEditMedicine.EditValue = null;
                spinEditCount.Value = 0;
                spinEditPrice.Value = 0;
                spinEditTotalSumPrice.Value = 0;
                spinEditBal.Value = 0;
                checkEditIsGiven.Checked = false;
            }

            private void clearErrorText()
            {
                gridLookUpEditUser.ErrorText = "";
                gridLookUpEditPatient.ErrorText = "";
                gridLookUpEditMedicine.ErrorText = "";
                spinEditCount.ErrorText = "";
                dateEditDate.ErrorText = "";
            }

            private void MedicinePatientInForm_Shown(object sender, EventArgs e)
            {
                if (actionType == 1)
                {
                    if (gridLookUpEditPatient.Text.Equals(""))
                        gridLookUpEditPatient.Focus();
                    else
                        gridLookUpEditUser.Focus();
                }
                else 
                {
                    setValueFromGrid();
                    gridLookUpEditMedicine.Focus();
                }
            }

        #endregion

        #region Формын event

            private void dateEditDate_EditValueChanged(object sender, EventArgs e)
            {
                reloadMedicine();
            }

            private void gridLookUpEditPatient_EditValueChanged(object sender, EventArgs e)
            {
                if (!gridLookUpEditUser.Properties.ReadOnly && gridLookUpEditPatient.EditValue != null && gridLookUpEditPatient.EditValue != DBNull.Value && !gridLookUpEditPatient.Text.Equals(""))
                {
                    DataRowView view = (DataRowView)gridLookUpEditPatient.GetSelectedDataRow();
                    //InformationDataSet.PharmacistOtherBindingSource.Filter = "wardID = '" + view["wardID"] + "'";
                }
            }

            private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditUser.EditValue != null && gridLookUpEditUser.EditValue.ToString() != "" && gridViewDetail.RowCount == 0)
                {
                    addRow();
                    gridLookUpEditMedicine.Focus();
                }
                reloadMedicine();
            }

            private void checkEditAll_CheckedChanged(object sender, EventArgs e)
            {
                checkAll();
            }


            private void gridControlDetail_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add") && checkMedicine())
                    {
                        addRow();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewDetail.GetFocusedDataRow() != null)
                    {
                        deleteRow();
                    }
                }
            }

            private void gridViewDetail_BeforeLeaveRow(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
            {
                if (this.Visible && gridViewDetail.GetDataRow(e.RowHandle) != null && 
                    (gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") == DBNull.Value
                    || Convert.ToDecimal(gridViewDetail.GetRowCellValue(e.RowHandle, "price")) == 0
                    || (Convert.ToDecimal(gridViewDetail.GetRowCellValue(e.RowHandle, "count")) == 0 && Convert.ToDecimal(gridViewDetail.GetRowCellValue(e.RowHandle, "patientCount")) == 0)))
                {
                    if (e.RowHandle >= 0)
                    {
                        e.Allow = false;
                    }
                    else
                    {
                        e.Allow = true;
                    }
                }
                else if (this.Visible && (gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != DBNull.Value
                    && Convert.ToDecimal(gridViewDetail.GetRowCellValue(e.RowHandle, "price")) != 0))
                {
                    bool isAllow = true;
                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        if (e.RowHandle != i && gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != null && gridViewDetail.GetRowCellValue(i, "medicineID") != null &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != DBNull.Value && gridViewDetail.GetRowCellValue(i, "medicineID") != DBNull.Value &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "incTypeID") != null && gridViewDetail.GetRowCellValue(i, "incTypeID") != null &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "incTypeID") != DBNull.Value && gridViewDetail.GetRowCellValue(i, "incTypeID") != DBNull.Value &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID").ToString().Equals(gridViewDetail.GetRowCellValue(i, "medicineID").ToString()) &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "incTypeID").ToString().Equals(gridViewDetail.GetRowCellValue(i, "incTypeID").ToString()) &&
                            Convert.ToDecimal(gridViewDetail.GetRowCellValue(e.RowHandle, "price")) == Convert.ToDecimal(gridViewDetail.GetRowCellValue(i, "price")))
                        {
                            gridLookUpEditMedicine.ErrorText = "Эм, үнэ, орлогын төрөл давхацсан байна";
                            gridLookUpEditMedicine.Focus();
                            isAllow = false;
                            break;
                        }
                    }
                    e.Allow = isAllow;
                }
            }

            private void gridViewDetail_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                gridLookUpEditMedicine.Focus();
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridViewDetail_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
            {
                if(Visible && e.FocusedRowHandle >= 0)
                {
                    setValueFromGrid();
                }
            }

            private void gridViewDetail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
            {
                if (e.Column == gridColumnIsGive)
                {
                    checkEditIsGiven.Checked = Convert.ToBoolean(e.Value);
                }
            }


            private void spinEditCount_ValueChanged(object sender, EventArgs e)
            {
                calcPrice();
            }

            private void spinEditCount_Leave(object sender, EventArgs e)
            {
                endEdit();
            }


            private void gridLookUpEditMedicine_EditValueChanged(object sender, EventArgs e)
            {
                DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                if (gridLookUpEditMedicine.EditValue != null && gridLookUpEditMedicine.EditValue.ToString() != "" && view != null)
                {
                    spinEditPrice.Value = Convert.ToDecimal(view["price"]);
                    spinEditBal.Value = Convert.ToDecimal(view["count"]);
                }
            }

            private void gridLookUpEditMedicine_Validating(object sender, CancelEventArgs e)
            {
                checkMedicine();
                if (!e.Cancel && gridLookUpEditMedicine.EditValue != DBNull.Value && gridLookUpEditMedicine.EditValue != null)
                {
                    endEdit();
                }
            }

            private void gridLookUpEditMedicine_InvalidValue(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridLookUpEditMedicine_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    MedicineBalExpForm.Instance.showForm(3, Instance);
                }   
            }


            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion
        
        #region Формын функц

            private void setValueFromGrid()
            {
                DataRowView row = (DataRowView)gridViewDetail.GetFocusedRow();
                if (row != null)
                {
                    gridLookUpEditMedicine.EditValue = row["medicinePrice"];
                    spinEditPrice.EditValue = row["price"];
                    spinEditCount.EditValue = row["count"];
                    spinEditPatientCount.EditValue = row["patientCount"];
                    spinEditTotalCount.EditValue = row["totalCount"];
                    spinEditTotalSumPrice.EditValue = row["totalSumPrice"];
                    checkEditIsGiven.Checked = Convert.ToBoolean(row["isGive"]);
                    DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                    if (view != null)
                    {
                        spinEditBal.EditValue = view["count"];
                    }
                    else
                    {
                        spinEditBal.Value = 0;
                    }
                }
            }

            private void endEdit()
            {
                DataRowView medicineRow = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                DataRowView currRow = (DataRowView)gridViewDetail.GetFocusedRow();
                if (medicineRow == null || currRow == null)
                    return;

                currRow["medicineID"] = medicineRow["medicineID"];
                currRow["medicineName"] = medicineRow["name"];
                currRow["latinName"] = medicineRow["latinName"];
                currRow["unitType"] = medicineRow["unitType"];
                currRow["medicineTypeName"] = medicineRow["medicineTypeName"];
                currRow["shape"] = medicineRow["shape"];
                currRow["validDate"] = medicineRow["validDate"];
                currRow["incTypeID"] = medicineRow["incTypeID"];
                currRow["incTypeName"] = medicineRow["incTypeName"];
                currRow["price"] = spinEditPrice.EditValue;
                currRow["count"] = spinEditCount.EditValue;
                currRow["patientCount"] = spinEditPatientCount.EditValue;
                currRow["isGive"] = checkEditIsGiven.Checked;
                currRow.EndEdit();
            }

            private void calcPrice()
            {
                spinEditTotalCount.Value = spinEditCount.Value + spinEditPatientCount.Value;
                spinEditTotalSumPrice.Value = spinEditTotalCount.Value * spinEditPrice.Value;
            }

            private void checkAll()
            {
                for (var i = 0; i < gridViewDetail.RowCount; i++)
                {
                    gridViewDetail.SetRowCellValue(i, "isGive", checkEditAll.Checked);
                }
            }


            private void reloadMedicine()
            {
                if (dateEditDate.EditValue != DBNull.Value && dateEditDate.EditValue != null && !dateEditDate.EditValue.ToString().Equals("") &&
                    gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null && !gridLookUpEditUser.EditValue.ToString().Equals(""))
                {
                    //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    //JournalDataSet.MedicineBalTableAdapter.Fill(JournalDataSet.Instance.MedicineBal, dateEditDate.Text, gridLookUpEditUser.EditValue.ToString(), HospitalUtil.getHospitalId());
                    //ProgramUtil.closeWaitDialog();
                }
            }

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                PlanDataSet.SystemDoctorTableAdapter.Fill(PlanDataSet.Instance.SystemDoctor, HospitalUtil.getHospitalId(), SystemUtil.PHARMACIST);
                //JournalDataSet.PatientOtherTableAdapter.Fill(JournalDataSet.Instance.PatientOther, HospitalUtil.getHospitalId(), )
                ProgramUtil.closeWaitDialog();
            }

            private void addRow()
            {
                JournalDataSet.Instance.PatientMedicineOther.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                JournalDataSet.Instance.PatientMedicineOther.Rows.Add();
                
                clearMedicine();
                gridLookUpEditMedicine.Focus();
            }

            private void deleteRow()
            {
                deleteRows.Add(gridViewDetail.GetFocusedDataRow()["id"].ToString());
                gridViewDetail.DeleteSelectedRows();
            }

            private bool check()
            {
                clearErrorText();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditPatient.EditValue == DBNull.Value || gridLookUpEditPatient.EditValue == null || gridLookUpEditPatient.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмчлүүлэгчийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditPatient.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditPatient.Focus();
                }
                if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.EditValue.ToString().Equals(""))
                {
                    errorText = "Эм зүйчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditUser.Focus();
                    }
                }
                if (isRight)
                    isRight = checkMedicine();

                if (isRight && (actionType == 1 || (actionType == 2 && !beforeDate.Equals(dateEditDate.Text))))
                {
                    if (Convert.ToBoolean(JournalDataSet.QueriesTableAdapter.exist_patient_medicine(HospitalUtil.getHospitalId(), gridLookUpEditPatient.EditValue.ToString(), dateEditDate.Text)))
                    {
                        errorText = "Энэ өдөр эмчлүүлэгч дээр эм олгосон тул өдрийг өөрчилнө үү";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        dateEditDate.ErrorText = errorText;
                        isRight = false;
                        dateEditDate.Focus();
                    }
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private bool checkMedicine()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
                if (spinEditCount.Value == 0 && spinEditPatientCount.Value == 0)
                {
                    errorText = "Тоо, хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditCount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditCount.Focus();
                    }
                }
                for (int i = 0; isRight && i < gridViewDetail.RowCount; i++)
                {
                    if (gridViewDetail.FocusedRowHandle != i && gridLookUpEditMedicine.EditValue != null
                        && gridLookUpEditMedicine.EditValue.ToString().Equals(gridViewDetail.GetRowCellValue(i, "medicineID").ToString() + gridViewDetail.GetRowCellValue(i, "incTypeID").ToString() + spinEditPrice.EditValue.ToString())
                        && spinEditPrice.Value == Convert.ToDecimal(gridViewDetail.GetRowCellValue(i, "price")))
                    {
                        errorText = "Эм давхацсан байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditMedicine.ErrorText = errorText;
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                        break;
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView patient = (DataRowView)gridLookUpEditPatient.GetSelectedDataRow();

                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        DataRow row = gridViewDetail.GetDataRow(i);
                        if (actionType == 1)
                        {
                            addNewRow(row, patient["storeID"].ToString(), patient["wardID"].ToString());
                        }
                        else if (actionType == 2)
                        {
                            //  "' AND medicineID = '" + row["medicineID"] + "' AND price = " + row["price"] + 
                            DataRow[] rows = JournalDataSet.Instance.PatientMedicine.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND id = '" + row["id"] + "' AND userID = '" + gridLookUpEditUser.EditValue.ToString() + "' AND patientID = '" + gridLookUpEditPatient.EditValue.ToString() + "' AND date = '" + dateEditDate.Text + "'");
                            if (rows.Length > 0)
                            {
                                rows[0]["medicineID"] = row["medicineID"];
                                rows[0]["medicineName"] = row["medicineName"];
                                rows[0]["latinName"] = row["latinName"];
                                rows[0]["unitType"] = row["unitType"];
                                rows[0]["medicineTypeName"] = row["medicineTypeName"];
                                rows[0]["shape"] = row["shape"];
                                rows[0]["validDate"] = row["validDate"];
                                rows[0]["incTypeID"] = row["incTypeID"];
                                rows[0]["incTypeName"] = row["incTypeName"];
                                rows[0]["price"] = row["price"];
                                rows[0]["count"] = row["count"];
                                rows[0]["patientCount"] = row["patientCount"];
                                rows[0]["isGive"] = row["isGive"];
                                if (Convert.ToBoolean(rows[0]["isGive", DataRowVersion.Original]) != Convert.ToBoolean(row["isGive"]) ||
                                    rows[0]["medicineID", DataRowVersion.Original].ToString().Equals(row["medicineID"]) || 
                                    Convert.ToDouble(rows[0]["price", DataRowVersion.Original]) != Convert.ToDouble(row["price"]) ||
                                    Convert.ToDouble(rows[0]["count", DataRowVersion.Original]) != Convert.ToDouble(row["count"]) ||
                                    Convert.ToDouble(rows[0]["patientCount", DataRowVersion.Original]) != Convert.ToDouble(row["patientCount"]) ||
                                    Convert.ToInt32(rows[0]["incTypeID", DataRowVersion.Original]) != Convert.ToInt32(row["incTypeID"]))
                                {   
                                    rows[0]["giveUserID"] = UserUtil.getUserId();
                                    rows[0]["giveUserName"] = UserUtil.getUserFullName();
                                }

                                rows[0].EndEdit();
                            }
                            else
                            {
                                addNewRow(row, patient["storeID"].ToString(), patient["wardID"].ToString());
                            }
                        }
                    }

                    foreach(string id in deleteRows)
                    {
                        DataRow[] rows = JournalDataSet.Instance.PatientMedicine.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND id = '" + id + "'");
                        if (rows.Length > 0)
                        {
                            rows[0].Delete();
                        }
                    }

                    JournalDataSet.PatientMedicineBindingSource.EndEdit();
                    JournalDataSet.PatientMedicineTableAdapter.Update(JournalDataSet.Instance.PatientMedicine);

                    MedicinePatientForm.Instance.refreshData();

                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }

            private void addNewRow(DataRow row, string storeID, string wardID)
            {
                DataRowView view = (DataRowView)JournalDataSet.PatientMedicineBindingSource.AddNew();
                
                view["id"] = row["id"];
                view["userID"] = gridLookUpEditUser.EditValue;
                view["patientID"] = gridLookUpEditPatient.EditValue;
                view["storeID"] = storeID;
                view["wardID"] = wardID;
                view["userName"] = gridLookUpEditUser.Text;
                view["patientName"] = gridLookUpEditPatient.Text;
                view["date"] = dateEditDate.Text;
                view["medicineID"] = row["medicineID"];
                view["medicineName"] = row["medicineName"];
                view["latinName"] = row["latinName"];
                view["unitType"] = row["unitType"];
                view["medicineTypeName"] = row["medicineTypeName"];
                view["shape"] = row["shape"];
                view["validDate"] = row["validDate"];
                view["price"] = row["price"];
                view["count"] = row["count"];
                view["patientCount"] = row["patientCount"];
                view["isGive"] = row["isGive"];
                view["incTypeID"] = row["incTypeID"];
                view["incTypeName"] = row["incTypeName"];
                view["giveUserName"] = UserUtil.getUserFullName();
                
                view.EndEdit();
            }



            public bool existMedicine(string medicineID)
            {
                for (int i = 0; i < gridViewDetail.RowCount; i++)
                {
                    if (medicineID.Equals(gridViewDetail.GetDataRow(i)["medicinePrice"].ToString()))
                    {
                        return true;
                    }
                }
                return false;
            }

            public void addMedicine(DataRowView view, object incTypeID, string incTypeText, decimal count, decimal price)
            {
                DataRowView row = (DataRowView)JournalDataSet.MedicineBalBindingSource.AddNew();
                row["medicineID"] = view["id"];
                row["name"] = view["name"];
                row["latinName"] = view["latinName"];
                row["medicineTypeName"] = view["medicineTypeName"];
                row["unitType"] = view["unitType"];
                row["barcode"] = view["barcode"];
                row["shape"] = view["shape"];
                row["validDate"] = view["validDate"];
                row["incTypeID"] = incTypeID;
                row["incTypeName"] = incTypeText;
                row["price"] = price;
                row["count"] = count;
                row.EndEdit();

                gridLookUpEditMedicine.EditValue = view["id"].ToString() + incTypeID.ToString() + price;

                spinEditCount.Value = count;
                spinEditPrice.Value = price;
                endEdit();
            }

        #endregion

    }
}