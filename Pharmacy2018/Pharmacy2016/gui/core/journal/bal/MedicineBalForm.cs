﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraTreeList.Nodes;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.report.journal.patient;
using Pharmacy2016.report.journal.bal;
using Pharmacy2016.gui.report.ds;



namespace Pharmacy2016.gui.core.journal.bal
{
    /*
     * Эмийн үлдэгдэл харах, хэвлэх цонх.
     * 
     */

    public partial class MedicineBalForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineBalForm INSTANCE = new MedicineBalForm();

            public static MedicineBalForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 2005;

            private bool isInit = false;

            public DataRowView currView;

            private System.IO.Stream layout_bal = null;

            private MedicineBalForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initLayout();
                initError();
                RoleUtil.addForm(FORM_ID, this);
                
                gridControlBal.DataSource = JournalDataSet.BalBindingSource;
                //gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUser1BindingSource;
                gridLookUpEditUser.Properties.DataSource = JournalDataSet.Instance.SystemUserJournal;

                dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                DateTime date = DateTime.Now;
                dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1);
                dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
                reloadUser(false);
            }

            private void initError()
            {
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditEnd.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

            private void initLayout()
            {
                layout_bal = new System.IO.MemoryStream();
                gridViewBal.SaveLayoutToStream(layout_bal);
                layout_bal.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        if ( UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                        {
                            gridLookUpEditUser.Visible = false;
                            labelControlAll.Visible = false;
                            checkEditAll.Visible = false;
                            gridBand4.Visible = false;
                            simpleButtonReload.Location = new Point(226, 11);
                            
                        }
                        else
                        {
                            gridLookUpEditUser.Visible = true;
                            labelControlAll.Visible = true;
                            checkEditAll.Visible = true;
                            gridBand4.Visible = true;
                            //labelControlAll.Location = new Point(226, 16);
                            //checkEditAll.Location = new Point(262, 13);
                            //gridLookUpEditUser.Location = new Point(287, 12);
                            simpleButtonReload.Location = new Point(493, 11);
                        }

                        checkRole();
                        Show();
                    }
                }
                catch(Exception ex)
                {
                    XtraMessageBox.Show("Цонх ачааллахад алдаа гарлаа "+ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {   
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                dropDownButtonPrint.Visible = roles[4];

                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("bal");
                if (layout != null)
                {
                    gridViewBal.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewBal.RestoreLayoutFromStream(layout_bal);
                    layout_bal.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }
            
            private void MedicineBalForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                JournalDataSet.WarehouseOther2BindingSource.Filter = "";
                JournalDataSet.Instance.Bal.Clear();
            }

        #endregion

        #region Формын event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                saveLayout();
            }

            private void checkEditAll_CheckedChanged(object sender, EventArgs e)
            {
                editUser();
            }

            private void gridLookUpEditUser_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals("reload", e.Button.Tag))
                {
                    try
                    {
                        reloadUser(true);
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            private void gridLookUpEditUser_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
            {
                e.DisplayText = "Хэрэглэгч сонгох";
            }

            private void checkedComboBoxEdit_EditValueChanged(object sender, EventArgs e)
            {
                showTotalBand();
            }

            private void gridViewBal_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
            {
                if (e.Column == bandedGridColumnBegBal || e.Column == bandedGridColumnTotalIncCount || e.Column == bandedGridColumnTotalExpCount || e.Column == bandedGridColumnEndBal)
                {
                    if (Convert.ToDouble(e.CellValue) < 0)
                    {
                        e.Appearance.BackColor = Color.Red;
                    }
                }
            }

            private void gridViewBal_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
            {
                GridGroupRowInfo info = e.Info as GridGroupRowInfo;
                // gridViewBal.GetChildRowCount(e.RowHandle),
                info.GroupText = string.Format("{0} (эцсийн үлдэгдэл = {1} эцсийн дүн = {2})", info.GroupValueText, gridViewBal.GetGroupSummaryDisplayText(e.RowHandle, gridViewBal.GroupSummary[2] as DevExpress.XtraGrid.GridGroupSummaryItem), gridViewBal.GetGroupSummaryDisplayText(e.RowHandle, gridViewBal.GroupSummary[3] as DevExpress.XtraGrid.GridGroupSummaryItem));
            }

        #endregion

        #region Формын функц

            private void showTotalBand()
            {
                for (var i = 0; i < checkedComboBoxEdit.Properties.Items.Count; i++)
                {
                    if (checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("incCount"))
                    {
                        gridBandIncCount.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                    else if (checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("incMotCount"))
                    {
                        gridBandIncMotCount.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                    else if (checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("totalIncCount"))
                    {
                        gridBandTotalIncCount.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                    else if (checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("expCount"))
                    {
                        gridBandExpCount.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                    else if (checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("expMotCount"))
                    {
                        gridBandExpMotCount.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                    else if (checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("expPatientCount"))
                    {
                        gridBandExpPatientCount.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                    else if (checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("totalExpCount"))
                    {
                        gridBandTotalExpCount.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                }
            }
            
            private void editUser()
            {
                gridLookUpEditUser.Enabled = !checkEditAll.Checked;
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if(dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if ((UserUtil.getUserRoleId() != RoleUtil.STOREMAN_ID && UserUtil.getUserRoleId() != RoleUtil.PHARMACIST_ID) &&
                    !checkEditAll.Checked && gridLookUpEditUser.Properties.View.GetSelectedRows().Length == 0)
                {
                    errorText = "Хэрэглэгчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditUser.Focus();
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void reloadUser(bool isReload)
            {   
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                if (isReload || JournalDataSet.Instance.SystemUserJournal.Rows.Count == 0)
                {
                    JournalDataSet.SystemUserJournalTableAdapter.Fill(JournalDataSet.Instance.SystemUserJournal, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);    
                }
                //ProgramUtil.closeWaitDialog();
            }

            private void reload()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                {
                    if (check())
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        JournalDataSet.BalTableAdapter.Fill(JournalDataSet.Instance.Bal, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                        ProgramUtil.closeWaitDialog();
                    }
                }
                else
                {
                    if (check())
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                        var userID = "";
                        if (checkEditAll.Checked)
                        {
                            for (var i = 0; i < JournalDataSet.Instance.SystemUserJournal.Rows.Count; i++)
                                userID += JournalDataSet.Instance.SystemUserJournal.Rows[i]["id"] + "~";
                        }
                        else
                        {
                            int[] rowID = gridLookUpEditUser.Properties.View.GetSelectedRows();
                            for (int i = 0; i < rowID.Length; i++)
                                userID += gridLookUpEditUser.Properties.View.GetDataRow(rowID[i])["id"] + "~";
                        }
                        if (!userID.Equals(""))
                            userID = userID.Substring(0, userID.Length - 1);

                        JournalDataSet.BalTableAdapter.Fill(JournalDataSet.Instance.Bal, HospitalUtil.getHospitalId(), userID, dateEditStart.Text, dateEditEnd.Text);

                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            private void saveLayout()
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    gridViewBal.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    System.IO.StreamReader reader = new System.IO.StreamReader(str);
                    string layout = reader.ReadToEnd();
                    UserUtil.saveUserLayout("bal", layout);
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

        #region Хэвлэх, хөрвүүлэх функц

            private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemPrint.Caption;
                dropDownButtonPrint.Image = barButtonItemPrint.Glyph;
                if (gridLookUpEditUser.Properties.View.SelectedRowsCount > 0 && gridViewBal.RowCount > 0)
                    MedicineBalReport.Instance.initAndShow(dateEditStart.Text, dateEditEnd.Text, gridViewBal.ActiveFilterString);
                else
                    XtraMessageBox.Show("Хэрэглэгч сонгон шүүнэ үү");
            }

            private void barButtonItemBegBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemWard.Caption;
                dropDownButtonPrint.Image = barButtonItemWard.Glyph;

                WardReportForm.Instance.showForm(this, dateEditStart.DateTime, dateEditEnd.DateTime);
            }

            private void barButtonItemCustomize_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                ProgramUtil.closeAlertDialog();
                if (gridViewBal.RowCount == 0 && gridViewBal.GetFocusedDataRow() == null)
                {
                    string errorText = "Хэвлэх мөр алга байна";
                    string text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                    return;
                }
                DataRow row = gridViewBal.GetFocusedDataRow();
                List<string[]> colList = new List<string[]>();
                foreach (DevExpress.XtraGrid.Columns.GridColumn col in gridViewBal.Columns.Where(x => (x.FieldName != "warehouseDesc" && x.FieldName != "userID" && x.FieldName != "latinName" && x.FieldName != "medicineTypeName" && x.FieldName != "begBal" && x.FieldName != "endBal" && x.FieldName != "price" && x.FieldName != "incCount" && x.FieldName != "expCount" && x.FieldName != "totalIncSumPrice" && x.FieldName != "totalExpSumPrice" && x.FieldName != "sumBegBal" && x.FieldName != "begSumPrice" && x.FieldName != "totalIncCount" && x.FieldName != "totalExpCount" && x.FieldName != "endSumPrice" && x.FieldName != "incSumPrice" && x.FieldName != "expSumPrice" && x.FieldName != "motSumPrice" && x.FieldName != "incMotCount" && x.FieldName != "incMotSumPrice" && x.FieldName != "expMotSumPrice" && x.FieldName != "expMotCount" && x.FieldName != "expPatientCount" && x.FieldName != "expPatientSumPrice" && x.FieldName != "incTypeName")))
                {
                    colList.Add(new string[]{ col.FieldName, col.CustomizationCaption});
                }

                ReportUpForm.Instance.showForm(colList, MedicineBalForm.Instance, row, dateEditStart.DateTime, dateEditEnd.DateTime, "");
                dropDownButtonPrint.Text = barButtonItemCustomize.Caption;
                dropDownButtonPrint.Image = barButtonItemCustomize.Glyph;
            }

            private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemConvert.Caption;
                dropDownButtonPrint.Image = barButtonItemConvert.Glyph;
                ProgramUtil.convertGrid(gridViewBal, "Эмийн журнал");
            }

        #endregion     

            private void gridControlBal_Click(object sender, EventArgs e)
            {

            }

            private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
            {

            }

    }
}