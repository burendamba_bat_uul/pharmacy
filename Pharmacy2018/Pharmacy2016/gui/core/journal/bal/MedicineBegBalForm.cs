﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.report.journal.bal;
using Pharmacy2016.gui.report.journal.bal;
using System.Data.SqlClient;



namespace Pharmacy2016.gui.core.journal.bal {
  /*
   * Эхний үлдэгдэл харах, нэмэх, засах, устгах цонх.
   * 
   */

  public partial class MedicineBegBalForm : DevExpress.XtraEditors.XtraForm {

    #region Форм анх ачааллах функц

    private static MedicineBegBalForm INSTANCE = new MedicineBegBalForm();

    public static MedicineBegBalForm Instance {
      get {
        return INSTANCE;
      }
    }

    private readonly int FORM_ID = 2006;

    private bool isInit = false;

    public DataRowView currView;

    private System.IO.Stream layout_beg_bal = null;

    private MedicineBegBalForm() {

    }

    private void initForm() {
      InitializeComponent();
      this.isInit = true;
      this.MdiParent = MainForm.Instance;
      Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

      initLayout();
      RoleUtil.addForm(FORM_ID, this);

      gridControlBegBal.DataSource = JournalDataSet.BegBalBindingSource;
    }

    private void initLayout() {
      layout_beg_bal = new System.IO.MemoryStream();
      gridViewBegBal.SaveLayoutToStream(layout_beg_bal);
      layout_beg_bal.Seek(0, System.IO.SeekOrigin.Begin);
    }

    #endregion

    #region Форм нээх, хаах функц

    public void showForm() {
      try {
        if (!this.isInit) {
          initForm();
        }

        if (Visible) {
          Select();
        } else {
          JournalDataSet.Instance.BegBal.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
          JournalDataSet.Instance.BegBal.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

          checkRole();
          reload();
          Show();
        }
      } catch (Exception ex) {
        XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
        clearData();
      } finally {
        ProgramUtil.closeWaitDialog();
      }
    }

    private void checkRole() {
      bool[] roles = UserUtil.getWindowRole(FORM_ID);

      gridControlBegBal.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
      gridControlBegBal.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
      gridControlBegBal.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
      dropDownButton1.Visible = roles[4];
      simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;
      //Эхний үлдэгдэл татах админы эрх шалгах
      string lockUserID = "";
      SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
      myConn.Open();
      string query = "SELECT lockUserID FROM [MainConf]";
      SqlCommand oCmd = new SqlCommand(query, myConn);
      using (SqlDataReader oReader = oCmd.ExecuteReader())
      {
          while (oReader.Read())
          {
              if (oReader.HasRows)
              {
                  lockUserID = oReader["lockUserID"].ToString();
              }
          }
      }
      bool isLockUser = (UserUtil.getUserId() == lockUserID);
      simpleButton1.Visible = isLockUser;
      checkLayout();
    }

    private void checkLayout() {
      string layout = UserUtil.getLayout("begBal");
      if (layout != null) {
        gridViewBegBal.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
      } else {
        gridViewBegBal.RestoreLayoutFromStream(layout_beg_bal);
        layout_beg_bal.Seek(0, System.IO.SeekOrigin.Begin);
      }
    }

    private void MedicineBegBalForm_FormClosing(object sender, FormClosingEventArgs e) {
      e.Cancel = true;
      clearData();
      Hide();
    }

    private void clearData() {
      JournalDataSet.Instance.BegBal.Clear();
    }

    #endregion

    #region Формын event

    private void simpleButtonReload_Click(object sender, EventArgs e) {
      try {
        reload();
      } catch (System.Exception ex) {
        XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
      } finally {
        ProgramUtil.closeWaitDialog();
      }
    }

    private void simpleButtonLayout_Click(object sender, EventArgs e) {
      saveLayout();
    }

    private void gridControlBegBal_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e) {
      if (e.Button.ButtonType == NavigatorButtonType.Custom) {
        if (String.Equals(e.Button.Tag, "add")) {
          add();
        } else if (String.Equals(e.Button.Tag, "delete") && gridViewBegBal.RowCount > 0 && gridViewBegBal.GetFocusedDataRow() != null) {
          try {
            delete();
          } catch (System.Exception ex) {
            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
          } finally {
            ProgramUtil.closeWaitDialog();
          }
        } else if (String.Equals(e.Button.Tag, "update") && gridViewBegBal.RowCount > 0 && gridViewBegBal.GetFocusedDataRow() != null) {
          update();
        }
      }
    }

    private void gridViewBegBal_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e) {
      GridGroupRowInfo info = e.Info as GridGroupRowInfo;
      info.GroupText = string.Format("{0} (Тоо хэмжээ = {1} Нийт үнэ = {2})", info.GroupValueText, gridViewBegBal.GetGroupSummaryDisplayText(e.RowHandle, gridViewBegBal.GroupSummary[0] as DevExpress.XtraGrid.GridGroupSummaryItem), gridViewBegBal.GetGroupSummaryDisplayText(e.RowHandle, gridViewBegBal.GroupSummary[1] as DevExpress.XtraGrid.GridGroupSummaryItem));
    }

    #endregion

    #region Формын функц

    private void reload() {
      if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID) {
        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
        JournalDataSet.BegBalTableAdapter.Fill(JournalDataSet.Instance.BegBal, HospitalUtil.getHospitalId(), UserUtil.getUserId());
        ProgramUtil.closeWaitDialog();
      } else {
        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
        JournalDataSet.BegBalTableAdapter.Fill(JournalDataSet.Instance.BegBal, HospitalUtil.getHospitalId(), "");
        ProgramUtil.closeWaitDialog();
      }
    }

    private void add() {
        if (ProgramUtil.checkLockMedicineBegBal())
        {
            MedicineBegBalUpForm.Instance.showForm(1, Instance);
        }
        else
        {
            XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
        }
    }

    private void delete() {
        if (ProgramUtil.checkLockMedicineBegBal())
        {
            DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эхний үлдэгдэл устгах", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                gridViewBegBal.DeleteSelectedRows();
                JournalDataSet.BegBalBindingSource.EndEdit();
                JournalDataSet.BegBalTableAdapter.Update(JournalDataSet.Instance.BegBal);
                ProgramUtil.closeWaitDialog();
            }
        }
        else
        {
            XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
        }
    }

    private void update() {
      //currView = (DataRowView)JournalDataSet.BegBalBindingSource.Current;
        if (ProgramUtil.checkLockMedicineBegBal()) {
            MedicineBegBalUpForm.Instance.showForm(2, Instance);
        }
        else
        {
            XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
        }
    }

    private void saveLayout() {
      try {
        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

        System.IO.Stream str = new System.IO.MemoryStream();
        gridViewBegBal.SaveLayoutToStream(str);
        str.Seek(0, System.IO.SeekOrigin.Begin);
        System.IO.StreamReader reader = new System.IO.StreamReader(str);
        string layout = reader.ReadToEnd();
        UserUtil.saveUserLayout("begBal", layout);
        reader.Close();
        str.Close();

        ProgramUtil.closeWaitDialog();
      } catch (System.Exception ex) {
        XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
      } finally {
        ProgramUtil.closeWaitDialog();
      }
    }

    #endregion

    #region Хэвлэх, хөрвүүлэх функц

    private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
      dropDownButtonPrint.Text = barButtonItem1.Caption;
      dropDownButtonPrint.Image = barButtonItem1.Glyph;

      BegBalReport.Instance.initAndShow(1, gridViewBegBal.ActiveFilterString);

    }

    private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
      dropDownButtonPrint.Text = barButtonItemPrint.Caption;
      dropDownButtonPrint.Image = barButtonItemPrint.Glyph;

      if (gridViewBegBal.RowCount == 0 || gridViewBegBal.GetFocusedRow() == null)
        return;

      string filter = "userID = '" + gridViewBegBal.GetFocusedDataRow()["userID"] + "'";

      MedicineBegBalReport.Instance.initAndShow(filter, gridViewBegBal.GetFocusedDataRow()["userName"].ToString(), gridViewBegBal.GetFocusedDataRow()["warehouseName"].ToString());
    }

    private void barButtonItemBegBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
      dropDownButtonPrint.Text = barButtonItemBegBal.Caption;
      dropDownButtonPrint.Image = barButtonItemBegBal.Glyph;
    }

    private void barButtonItemEndBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
      dropDownButtonPrint.Text = barButtonItemEndBal.Caption;
      dropDownButtonPrint.Image = barButtonItemEndBal.Glyph;
    }

    private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) {
      dropDownButtonPrint.Text = barButtonItemConvert.Caption;
      dropDownButtonPrint.Image = barButtonItemConvert.Glyph;
      ProgramUtil.convertGrid(gridViewBegBal, "Эхний үлдэгдэл");
    }

    #endregion

    #region Эхний үлдэгдэл татах
    private void simpleButton1_Click(object sender, EventArgs e) {
        if (ProgramUtil.checkLockMedicineBegBal())
        {
            importBegBal();
            reload();
        }
        else
        {
            XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
        }
    }

    private void importBegBal() {
      MedicineBegBalIncForm.Instance.showForm(2,Instance);
    }
    #endregion

    private void panelControl2_Paint(object sender, PaintEventArgs e) {

    }


    private void gridControlBegBal_Click(object sender, EventArgs e) {

    }



  }
}