﻿namespace Pharmacy2016.gui.core.journal.bal {
  partial class MedicineBegBalIncForm {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.btnOpen = new System.Windows.Forms.Button();
      this.treeList1 = new DevExpress.XtraTreeList.TreeList();
      this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
      this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
      this.buttonSave = new System.Windows.Forms.Button();
      this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
      ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
      this.SuspendLayout();
      // 
      // btnOpen
      // 
      this.btnOpen.Location = new System.Drawing.Point(60, 87);
      this.btnOpen.Name = "btnOpen";
      this.btnOpen.Size = new System.Drawing.Size(86, 23);
      this.btnOpen.TabIndex = 57;
      this.btnOpen.Text = "Сонгох";
      this.btnOpen.UseVisualStyleBackColor = true;
      this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
      // 
      // treeList1
      // 
      this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
      this.treeList1.KeyFieldName = "id";
      this.treeList1.Location = new System.Drawing.Point(-6, 77);
      this.treeList1.Name = "treeList1";
      this.treeList1.OptionsBehavior.EnableFiltering = true;
      this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
      this.treeList1.ParentFieldName = "pid";
      this.treeList1.Size = new System.Drawing.Size(400, 200);
      this.treeList1.TabIndex = 0;
      // 
      // treeListColumn2
      // 
      this.treeListColumn2.Caption = "Нэр";
      this.treeListColumn2.FieldName = "name";
      this.treeListColumn2.MinWidth = 32;
      this.treeListColumn2.Name = "treeListColumn2";
      this.treeListColumn2.Visible = true;
      this.treeListColumn2.VisibleIndex = 0;
      // 
      // labelControl2
      // 
      this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.labelControl2.Location = new System.Drawing.Point(60, 50);
      this.labelControl2.Name = "labelControl2";
      this.labelControl2.Size = new System.Drawing.Size(30, 13);
      this.labelControl2.TabIndex = 64;
      this.labelControl2.Text = "Файл:";
      this.labelControl2.Click += new System.EventHandler(this.labelControl2_Click);
      // 
      // buttonSave
      // 
      this.buttonSave.Location = new System.Drawing.Point(193, 87);
      this.buttonSave.Name = "buttonSave";
      this.buttonSave.Size = new System.Drawing.Size(85, 23);
      this.buttonSave.TabIndex = 67;
      this.buttonSave.Text = "Татах";
      this.buttonSave.UseVisualStyleBackColor = true;
      this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
      // 
      // labelControl1
      // 
      this.labelControl1.Location = new System.Drawing.Point(108, 50);
      this.labelControl1.Name = "labelControl1";
      this.labelControl1.Size = new System.Drawing.Size(0, 13);
      this.labelControl1.TabIndex = 68;
      // 
      // MedicineBegBalIncForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(352, 158);
      this.Controls.Add(this.labelControl1);
      this.Controls.Add(this.buttonSave);
      this.Controls.Add(this.labelControl2);
      this.Controls.Add(this.btnOpen);
      this.Name = "MedicineBegBalIncForm";
      this.Text = "Импорт хийх";
      ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnOpen;
    private DevExpress.XtraTreeList.TreeList treeList1;
    private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
    private DevExpress.XtraEditors.LabelControl labelControl2;
    private System.Windows.Forms.Button buttonSave;
    private DevExpress.XtraEditors.LabelControl labelControl1;
  }
}