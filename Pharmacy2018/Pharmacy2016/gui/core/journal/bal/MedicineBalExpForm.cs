﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.info.medicine;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.journal.exp;
using Pharmacy2016.gui.core.journal.mot;
using Pharmacy2016.gui.core.journal.patient;
using Pharmacy2016.gui.core.journal.census;



namespace Pharmacy2016.gui.core.journal.bal
{
    /*
     * Тухайн эм үлдэгдэлд байхгүй тохиолдолд нэмэх цонх.
     * 
     */

    public partial class MedicineBalExpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineBalExpForm INSTANCE = new MedicineBalExpForm();

            public static MedicineBalExpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int formType = 0;

            private MedicineBalExpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                reload();
            }

            private void initDataSource()
            {
                gridLookUpEditIncType.Properties.DataSource = InformationDataSet.IncTypeOtherBindingSource;
                gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineOtherBindingSource;
            }

            private void initError()
            {
                gridLookUpEditIncType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditPrice.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int fType, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    formType = fType;

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void MedicineBegBalInUpForm_Shown(object sender, EventArgs e)
            {
                ProgramUtil.IsShowPopup = true;
                gridLookUpEditIncType.Focus();
            }
        
            private void clearData()
            {
                clearErrorText();

                gridLookUpEditIncType.EditValue = null;
                gridLookUpEditMedicine.EditValue = null;
                spinEditCount.Value = 0;
                spinEditPrice.Value = 0;
                spinEditSumPrice.Value = 0;
            }

            private void clearErrorText()
            {
                gridLookUpEditIncType.ErrorText = "";
                gridLookUpEditMedicine.ErrorText = "";
                spinEditCount.ErrorText = "";
                spinEditPrice.ErrorText = "";
            }

        #endregion
        
        #region Формын event

            private void gridLookUpEditIncType_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditIncType.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditMedicine.Focus();
                    }
                }
            }

            private void gridLookUpEditMedicine_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditMedicine.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        spinEditCount.Focus();
                    }
                }
            }

            private void gridLookUpEditMedicine_EditValueChanged(object sender, EventArgs e)
            {
                DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                if (view != null)
                {
                    spinEditPrice.EditValue = view["price"];
                    labelControlQuantity.Text = view["quantity"].ToString();
                }
            }

            private void spinEditCount_ValueChanged(object sender, EventArgs e)
            {
                spinEditSumPrice.Value = spinEditCount.Value * spinEditPrice.Value;
            }


            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private void reload()
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.IncTypeOtherTableAdapter.Fill(InformationDataSet.Instance.IncTypeOther, HospitalUtil.getHospitalId());
                JournalDataSet.MedicineOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOther, HospitalUtil.getHospitalId(),"");
                //ProgramUtil.closeWaitDialog();
            }

            private bool check()
            {
                clearErrorText();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditIncType.EditValue == DBNull.Value || gridLookUpEditIncType.EditValue == null || gridLookUpEditIncType.EditValue.ToString().Equals(""))
                {
                    errorText = "Орлогын төрлийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditIncType.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditIncType.Focus();
                }
                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                    }
                }
                if (spinEditCount.Value == 0)
                {
                    errorText = "Тоо, хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditCount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditCount.Focus();
                    }
                }
                if (spinEditPrice.Value == 0)
                {
                    errorText = "Үнийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditPrice.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditPrice.Focus();
                    }
                }
                if (isRight && exist())
                {
                    errorText = "Орлогын төрөл, эм, үнэ давхацсан байна";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private bool exist()
            {
                bool exist = false;
                if (formType == 1)
                {
                    exist = MedicineExpUpForm.Instance.existMedicine(gridLookUpEditMedicine.EditValue.ToString() + gridLookUpEditIncType.EditValue.ToString() + spinEditPrice.Value);
                }
                else if (formType == 2)
                {
                    exist = MedicineCensusUpForm.Instance.existMedicine(gridLookUpEditMedicine.EditValue.ToString() + gridLookUpEditIncType.EditValue.ToString() + spinEditPrice.Value);
                }
                else if (formType == 3)
                {
                    exist = MedicinePatientInUpForm.Instance.existMedicine(gridLookUpEditMedicine.EditValue.ToString() + gridLookUpEditIncType.EditValue.ToString() + spinEditPrice.Value);
                }
                
                return exist;
            }

            private void save()
            {
                if (check())
                {
                    if (formType == 1)
                    {
                        MedicineExpUpForm.Instance.addMedicine((DataRowView) gridLookUpEditMedicine.GetSelectedDataRow(), gridLookUpEditIncType.EditValue, gridLookUpEditIncType.Text, spinEditCount.Value, spinEditPrice.Value);
                    }
                    else if (formType == 2)
                    {
                        MedicineCensusUpForm.Instance.addMedicine((DataRowView)gridLookUpEditMedicine.GetSelectedDataRow(), gridLookUpEditIncType.EditValue, gridLookUpEditIncType.Text, spinEditCount.Value, spinEditPrice.Value);
                    }
                    else if (formType == 3)
                    {
                        MedicinePatientInUpForm.Instance.addMedicine((DataRowView)gridLookUpEditMedicine.GetSelectedDataRow(), gridLookUpEditIncType.EditValue, gridLookUpEditIncType.Text, spinEditCount.Value, spinEditPrice.Value);
                    }
                    
                    Close();
                }
            }

        #endregion

    }
}