﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.info.medicine;



namespace Pharmacy2016.gui.core.journal.bal
{
    /*
     * Эхний үлдэгдэл нэмэх цонх.
     * 
     */

    public partial class MedicineBegBalInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineBegBalInUpForm INSTANCE = new MedicineBegBalInUpForm();

            public static MedicineBegBalInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType = 1;

            private List<string> deletedRow = new List<string>();

            private MedicineBegBalInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                reload(false);
            }

            private void initDataSource()
            {
                //gridControlBegBal.DataSource = JournalDataSet.Instance.BegBalOther;
                gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
                treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
                treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
                gridLookUpEditIncType.Properties.DataSource = InformationDataSet.IncTypeOtherBindingSource;
                gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineOtherBindingSource;
            }

            private void initError()
            {
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditIncType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditPrice.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int actionType, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    this.actionType = actionType;
                    if (actionType == 1)
                    {
                        insert();
                    }
                    else if (actionType == 2)
                    {
                        update();
                    }
                    //addRow();
                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insert()
            {
                radioGroupType.SelectedIndex = 0;
            }

            private void update()
            {
                gridLookUpEditUser.EditValue = MedicineBegBalForm.Instance.currView["userID"];
                gridLookUpEditIncType.EditValue = MedicineBegBalForm.Instance.currView["incTypeID"];
                radioGroupType.SelectedIndex = Convert.ToInt32(MedicineBegBalForm.Instance.currView["type"]);
                DataRow[] rows = null;
                if (radioGroupType.SelectedIndex == 0)
                {
                    treeListLookUpEditWarehouse.EditValue = MedicineBegBalForm.Instance.currView["warehouseID"];
                    rows = JournalDataSet.Instance.BegBal.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND userID = '" + gridLookUpEditUser.EditValue + "' AND type = " + radioGroupType.SelectedIndex + " AND warehouseID = '" + treeListLookUpEditWarehouse.EditValue + "'");
                }
                else if (radioGroupType.SelectedIndex == 1)
                {
                    treeListLookUpEditWard.EditValue = MedicineBegBalForm.Instance.currView["wardID"];
                    rows = JournalDataSet.Instance.BegBal.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND userID = '" + gridLookUpEditUser.EditValue + "' AND type = " + radioGroupType.SelectedIndex + " AND wardID = '" + treeListLookUpEditWard.EditValue + "'");
                }
                foreach (DataRow row in rows)
                {
                    //JournalDataSet.Instance.BegBalOther.idColumn.DefaultValue = row["id"];
                    //DataRow newRow = JournalDataSet.Instance.BegBalOther.Rows.Add();
                    ////newRow["id"] = row["id"];
                    //newRow["incTypeID"] = row["incTypeID"];
                    ////newRow["medicinePrice"] = row["medicinePrice"];
                    //newRow["medicineID"] = row["medicineID"];
                    //newRow["medicineName"] = row["medicineName"];
                    //newRow["latinName"] = row["latinName"];
                    //newRow["unitType"] = row["unitType"];
                    //newRow["medicineTypeName"] = row["medicineTypeName"];
                    //newRow["shape"] = row["shape"];
                    //newRow["validDate"] = row["validDate"];
                    //newRow["price"] = row["price"];
                    //newRow["count"] = row["count"];
                    //newRow["isNew"] = false;
                    //newRow.EndEdit();
                }
            }

            private void clearData()
            {
                clearErrorText();
                deletedRow.Clear();
                //JournalDataSet.Instance.BegBalOther.Clear();

                InformationDataSet.SystemUserBindingSource.Filter = "";
                JournalDataSet.WarehouseOtherBindingSource.Filter = "";
                JournalDataSet.WardOtherBindingSource.Filter = "";

                treeListLookUpEditWarehouse.EditValue = null;
                treeListLookUpEditWard.EditValue = null;
                gridLookUpEditIncType.EditValue = null;
                gridLookUpEditMedicine.EditValue = null;
                spinEditCount.Value = 0;
                spinEditPrice.Value = 0;
                spinEditSumPrice.Value = 0;
            }

            private void clearErrorText()
            {
                treeListLookUpEditWarehouse.ErrorText = "";
                gridLookUpEditIncType.ErrorText = "";
                gridLookUpEditMedicine.ErrorText = "";
                spinEditCount.ErrorText = "";
                spinEditPrice.ErrorText = "";
            }

            private void MedicineIncInForm_Shown(object sender, EventArgs e)
            {
                if (actionType == 2)
                {
                    if (UserUtil.getUserRoleId() != RoleUtil.STOREMAN_ID && UserUtil.getUserRoleId() != RoleUtil.PHARMACIST_ID)
                    {
                        gridLookUpEditUser.Focus();
                    }
                    else
                    {
                        InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                        gridLookUpEditUser.EditValue = UserUtil.getUserId();
                        if (radioGroupType.SelectedIndex == 0)
                            treeListLookUpEditWarehouse.Focus();
                        else if (radioGroupType.SelectedIndex == 0)
                            treeListLookUpEditWard.Focus();
                    }
                }
                else if (actionType == 2)
                {
                    setValueFromGrid();
                    gridLookUpEditMedicine.Focus();
                }
            }

        #endregion

        #region Формын event

            private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null)
                {
                    if (actionType == 1 && (gridLookUpEditIncType.EditValue != null && gridLookUpEditIncType.EditValue != DBNull.Value && gridLookUpEditIncType.EditValue.ToString().Equals("")) &&
                        (radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && treeListLookUpEditWarehouse.EditValue.ToString().Equals("")) &&
                        (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && treeListLookUpEditWard.EditValue.ToString().Equals("")) && gridViewBegBal.RowCount == 0)
                    {
                        addRow();
                        gridLookUpEditMedicine.Focus();
                    }
                }
            }

            private void gridLookUpEditIncType_EditValueChanged(object sender, EventArgs e)
            {
                if (actionType == 1 && (gridLookUpEditUser.EditValue != null && gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue.ToString().Equals("")) &&
                        (gridLookUpEditIncType.EditValue != null && gridLookUpEditIncType.EditValue != DBNull.Value && gridLookUpEditIncType.EditValue.ToString().Equals("")) &&
                        (radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && treeListLookUpEditWarehouse.EditValue.ToString().Equals("")) &&
                        (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && treeListLookUpEditWard.EditValue.ToString().Equals("")) && gridViewBegBal.RowCount == 0)
                {
                    addRow();
                    gridLookUpEditMedicine.Focus();
                }
            }

            private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeType();
            }


            private void gridControlDetail_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add") && (gridViewBegBal.RowCount == 0 || (gridLookUpEditMedicine.ErrorText == "" && gridLookUpEditMedicine.EditValue != null && gridLookUpEditMedicine.EditValue != DBNull.Value && checkMedicine())))
                    {
                        addRow();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewBegBal.GetFocusedDataRow() != null)
                    {
                        deleteRow();
                    }
                }
            }

            private void gridViewDetail_BeforeLeaveRow(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
            {
                if (this.Visible && (gridViewBegBal.GetRowCellValue(e.RowHandle, "medicineID") == DBNull.Value
                    || Convert.ToDecimal(gridViewBegBal.GetRowCellValue(e.RowHandle, "price")) == 0
                    || Convert.ToDecimal(gridViewBegBal.GetRowCellValue(e.RowHandle, "count")) == 0))
                {
                    if (e.RowHandle >= 0)
                    {
                        e.Allow = false;
                    }
                    else
                    {
                        e.Allow = true;
                    }
                }
                else if(this.Visible && (gridViewBegBal.GetRowCellValue(e.RowHandle, "medicineID") != DBNull.Value 
                    && Convert.ToDecimal(gridViewBegBal.GetRowCellValue(e.RowHandle, "price")) != 0))
                {
                    bool isAllow = true;
                    for (int i = 0; i < gridViewBegBal.RowCount; i++)
                    {
                        if (e.RowHandle != i && gridViewBegBal.GetRowCellValue(e.RowHandle, "medicineID") != null && gridViewBegBal.GetRowCellValue(i, "medicineID") != null &&
                            gridViewBegBal.GetRowCellValue(e.RowHandle, "medicineID").ToString().Equals(gridViewBegBal.GetRowCellValue(i, "medicineID").ToString()) &&
                            Convert.ToDecimal(gridViewBegBal.GetRowCellValue(e.RowHandle, "price")) == Convert.ToDecimal(gridViewBegBal.GetRowCellValue(i, "price")))
                        {
                            gridLookUpEditMedicine.ErrorText = "Эм, үнэ давхацсан байна";
                            spinEditPrice.ErrorText = "Эм, үнэ давхацсан байна";
                            spinEditPrice.Focus();
                            isAllow = false;
                            break;
                        }
                    }
                    e.Allow = isAllow;
                }
            }

            private void gridViewDetail_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                gridLookUpEditMedicine.Focus();
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridViewDetail_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
            {
                setValueFromGrid();
            }


            private void gridLookUpEditMedicine_EditValueChanged(object sender, EventArgs e)
            {
                DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                if (gridLookUpEditMedicine.EditValue != null && gridLookUpEditMedicine.EditValue.ToString() != "" && view != null)
                {
                    if(spinEditPrice.Value == 0)
                    {
                        spinEditPrice.Value = Convert.ToDecimal(view["price"]);
                    }
                }
            }

            private void gridLookUpEditMedicine_Validating(object sender, CancelEventArgs e)
            {
                //checkMedicine();
                if (!e.Cancel && gridLookUpEditMedicine.EditValue != DBNull.Value && gridLookUpEditMedicine.EditValue != null)
                {
                    endEdit();
                }
            }

            private void gridLookUpEditMedicine_InvalidValue(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridLookUpEditMedicine_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    MedicineInUpForm.Instance.showForm(3, Instance);
                    Activate();
                }   
            }


            private void spinEditCount_ValueChanged(object sender, EventArgs e)
            {
                calcPrice();
            }

            private void spinEditCount_Leave(object sender, EventArgs e)
            {
                endEdit();
            }


            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload(true);
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private void changeType()
            {
                if (radioGroupType.SelectedIndex == 0)
                {
                    labelControlWarehouse.Location = new Point(399, 59);
                    treeListLookUpEditWard.Location = new Point(457, 2);
                    treeListLookUpEditWarehouse.Location = new Point(457, 56);

                    treeListLookUpEditWard.Visible = false;
                    treeListLookUpEditWarehouse.Visible = true;
                    labelControlWarehouse.Text = "Агуулах*:";
                }
                else if (radioGroupType.SelectedIndex == 1)
                {
                    labelControlWarehouse.Location = new Point(413, 59);
                    treeListLookUpEditWard.Location = new Point(457, 56);
                    treeListLookUpEditWarehouse.Location = new Point(457, 2);

                    treeListLookUpEditWard.Visible = true;
                    treeListLookUpEditWarehouse.Visible = false;
                    labelControlWarehouse.Text = "Тасаг*:";
                }
            }

            private void setValueFromGrid()
            {
                DataRowView row = (DataRowView)gridViewBegBal.GetFocusedRow();
                if (row != null)
                {
                    gridLookUpEditMedicine.EditValue = row["medicineID"];
                    spinEditPrice.EditValue = row["price"];
                    spinEditCount.EditValue = row["count"];
                    spinEditSumPrice.EditValue = row["sumPrice"];
                }
            }

            private void endEdit()
            {
                DataRowView row = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                DataRowView view = (DataRowView)gridViewBegBal.GetFocusedRow();
                if (row == null || view == null)
                    return;

                view["medicineID"] = gridLookUpEditMedicine.EditValue;
                view["medicineName"] = row["name"];
                view["latinName"] = row["latinName"];
                view["unitType"] = row["unitType"];
                view["medicineTypeName"] = row["medicineTypeName"];
                view["shape"] = row["shape"];
                view["validDate"] = row["validDate"];
                calcPrice();
                view["price"] = spinEditPrice.EditValue;
                view["count"] = spinEditCount.EditValue;
                //view["sumPrice"] = spinEditSumPrice.EditValue;
                view.EndEdit();
            }

            private void calcPrice()
            {
                spinEditSumPrice.Value = spinEditCount.Value * spinEditPrice.Value;
            }


            private void reload(bool isReload)
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                    JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                    JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
                if (isReload || InformationDataSet.Instance.IncTypeOther.Rows.Count == 0)
                    InformationDataSet.IncTypeOtherTableAdapter.Fill(InformationDataSet.Instance.IncTypeOther, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.MedicineOther.Rows.Count == 0)
                    JournalDataSet.MedicineOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOther, HospitalUtil.getHospitalId(), "");
                //ProgramUtil.closeWaitDialog();
            }

            private void addRow()
            {
                //JournalDataSet.Instance.BegBalOther.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id();
                //JournalDataSet.Instance.BegBalOther.Rows.Add();

                gridLookUpEditMedicine.EditValue = null;
                spinEditPrice.Value = 0;
                spinEditCount.Value = 0;
                spinEditSumPrice.Value = 0;
                gridLookUpEditMedicine.Focus();
            }

            private void deleteRow()
            {
                if(actionType == 2)
                {
                    deletedRow.Add(gridViewBegBal.GetFocusedDataRow()["id"].ToString());
                }
                gridViewBegBal.DeleteSelectedRows();
                //JournalDataSet.IncDetailBindingSource.RemoveCurrent();
            }

            private bool check()
            {
                clearErrorText();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.EditValue.ToString().Equals(""))
                {
                    errorText = "Няравыг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
                if (gridLookUpEditIncType.EditValue == DBNull.Value || gridLookUpEditIncType.EditValue == null || gridLookUpEditIncType.EditValue.ToString().Equals(""))
                {
                    errorText = "Орлогын төрлийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditIncType.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditIncType.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.EditValue.ToString().Equals("")))
                {
                    errorText = "Агуулахыг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWarehouse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWarehouse.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.EditValue.ToString().Equals("")))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }
                if (isRight)
                    isRight = checkMedicine();
                if (isRight)
                {
                    for (int i = 0; i < gridViewBegBal.RowCount; i++)
                    {
                        bool exist = Convert.ToBoolean(JournalDataSet.QueriesTableAdapter.exist_beg_bal(HospitalUtil.getHospitalId(), gridViewBegBal.GetRowCellValue(i, "id").ToString(), gridLookUpEditUser.EditValue.ToString(), radioGroupType.SelectedIndex, 
                                                                            radioGroupType.SelectedIndex == 0 ? treeListLookUpEditWarehouse.EditValue.ToString() : null, radioGroupType.SelectedIndex == 1 ? treeListLookUpEditWard.EditValue.ToString() : null, 
                                                                            Convert.ToInt32(gridLookUpEditIncType.EditValue), gridViewBegBal.GetRowCellValue(i, "medicineID").ToString(), Convert.ToDouble(gridViewBegBal.GetRowCellValue(i, "price")), true));
                        if (exist)
                        {
                            gridViewBegBal.FocusedRowHandle = i;
                            errorText = "Хэрэглэгч, эмийн сан, орлогын төрөл, эм, үнэ дээр эхний үлдэгдэл оруулсан байна";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            treeListLookUpEditWarehouse.ErrorText = errorText;
                            gridLookUpEditIncType.ErrorText = errorText;
                            gridLookUpEditMedicine.ErrorText = errorText;
                            spinEditPrice.ErrorText = errorText;
                            gridLookUpEditMedicine.Focus();
                            isRight = false;
                            break;
                        }
                    }
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                
                return isRight;
            }

            private bool checkMedicine()
            {
                clearErrorText();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                    
                }
                if (spinEditPrice.Value == 0)
                {
                    spinEditPrice.ErrorText = "Үнийг оруулна уу";
                    if (isRight)
                    {
                        isRight = false;
                        spinEditPrice.Focus();
                    }
                }
                if (spinEditCount.Value == 0)
                {
                    spinEditCount.ErrorText = "Тоо, хэмжээг оруулна уу";
                    if (isRight)
                    {
                        isRight = false;
                        spinEditCount.Focus();
                    }
                }
                if (isRight)
                {
                    for (int i = 0; i < gridViewBegBal.RowCount; i++)
                    {
                        if (gridViewBegBal.FocusedRowHandle != i && gridLookUpEditMedicine.EditValue != null
                            && gridLookUpEditMedicine.EditValue.ToString().Equals(gridViewBegBal.GetRowCellValue(i, "medicineID").ToString())
                            && spinEditPrice.Value == Convert.ToDecimal(gridViewBegBal.GetRowCellValue(i, "price")))
                        {
                            isRight = false;
                            spinEditPrice.Focus();
                            errorText = "Эм, үнэ давхацсан байна";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            gridLookUpEditMedicine.ErrorText = errorText;
                            spinEditPrice.ErrorText = errorText;
                            break;
                        }
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {   
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();

                    for (int i = 0; i < gridViewBegBal.RowCount; i++)
                    {
                        DataRow row = gridViewBegBal.GetDataRow(i);
                        DataRow view = null;

                        if (actionType == 1 || Convert.ToBoolean(row["isNew"]))
                        {
                            view = ((DataRowView)JournalDataSet.BegBalBindingSource.AddNew()).Row;
                        }
                        else if (actionType == 2 && !Convert.ToBoolean(row["isNew"]))
                        {
                            view = null;
                            DataRow[] curr = JournalDataSet.Instance.BegBal.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND id = '" + row["id"] + "'");
                            if (curr != null && curr.Length > 0)
                                view = curr[0];

                            //for(int j = 0; j < JournalDataSet.BegBalBindingSource.Count; j++)
                            //{
                            //    DataRowView curr = (DataRowView)JournalDataSet.BegBalBindingSource[j];
                            //    if (curr["hospitalID"].ToString().Equals(HospitalUtil.getHospitalId()) && curr["id"].ToString().Equals(row["id"].ToString()))
                            //    {
                            //        view = curr;
                            //        break;
                            //    }
                            //}
                        }

                        if (view != null)
                        {
                            view["id"] = row["id"];
                            view["userID"] = gridLookUpEditUser.EditValue;
                            view["userName"] = user["fullName"];
                            view["roleName"] = user["roleName"];
                            view["incTypeID"] = gridLookUpEditIncType.EditValue;
                            view["incTypeName"] = gridLookUpEditIncType.Text;
                            view["type"] = radioGroupType.SelectedIndex;
                            if (radioGroupType.SelectedIndex == 0)
                            {
                                view["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                                view["wardID"] = null;
                                view["warehouseName"] = warehouse["name"];
                                view["warehouseDesc"] = warehouse["description"];
                            }
                            else if (radioGroupType.SelectedIndex == 1)
                            {
                                view["wardID"] = treeListLookUpEditWard.EditValue;
                                view["warehouseID"] = null;
                                view["warehouseName"] = ward["name"];
                                view["warehouseDesc"] = ward["description"];
                            }
                            view["medicineID"] = row["medicineID"];
                            view["medicineName"] = row["medicineName"];
                            view["latinName"] = row["latinName"];
                            view["unitType"] = row["unitType"];
                            view["medicineTypeName"] = row["medicineTypeName"];
                            view["shape"] = row["shape"];
                            view["validDate"] = row["validDate"];
                            view["price"] = row["price"];
                            view["count"] = row["count"];

                            view.EndEdit();
                        }
                    }
                    
                    if (actionType == 2)
                    {
                        foreach(string id in deletedRow)
                        {
                            DataRow[] row = JournalDataSet.Instance.BegBal.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND id = '" + id + "'");
                            if (row != null && row.Length > 0)
                                row[0].Delete();
                        }
                    }

                    JournalDataSet.BegBalBindingSource.EndEdit();
                    JournalDataSet.BegBalTableAdapter.Update(JournalDataSet.Instance.BegBal);

                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }

            
            // Шинээр нэмсэн эмийг авч байна
            public void addMedicine(DataRowView view)
            {
                DataRowView row = (DataRowView)JournalDataSet.MedicineOtherBindingSource.AddNew();
                row["id"] = view["id"];
                row["name"] = view["name"];
                row["latinName"] = view["latinName"];
                row["unitType"] = view["unitName"];
                row["price"] = view["price"];
                row["medicineTypeName"] = view["medicineTypeName"];
                row["barcode"] = view["barcode"];
                row["shape"] = view["shapeName"];
                row["validDate"] = view["validDate"];
                row.EndEdit();
                gridLookUpEditMedicine.EditValue = view["id"];
            }

        #endregion

    }
}