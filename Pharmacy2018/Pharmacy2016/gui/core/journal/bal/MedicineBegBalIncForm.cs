﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;
using ExcelDataReader;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;



namespace Pharmacy2016.gui.core.journal.bal
{
    public partial class MedicineBegBalIncForm : DevExpress.XtraEditors.XtraForm
    {



        #region Форм анх ачааллах функц

        private static MedicineBegBalIncForm INSTANCE = new MedicineBegBalIncForm();

        public static MedicineBegBalIncForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm(int type, XtraForm form)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }
                ShowDialog(form);

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clear();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        #endregion

        #region Формын event

        private void btnOpen_Click(object sender, EventArgs e)
        {
            open();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            save();



        }

        #endregion

        #region Формын функц

        private void clear()
        {
            labelControl1.Text = null;
        }

        DataSet result = null;

        private void open()
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Excel Files|*.xls;*.xlsx;*.xlsm", ValidateNames = true })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    FileStream fs = File.Open(ofd.FileName, FileMode.Open, FileAccess.Read);
                    IExcelDataReader reader = ExcelReaderFactory.CreateOpenXmlReader(fs);
                    result = reader.AsDataSet();
                    labelControl1.Text = ofd.FileName.Split('\\').Last();
                    ProgramUtil.closeWaitDialog();
                }
            }
        }

        private void save()
        {
            string success = "";
            int error = 0;
            SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
            myConn.Open();
            int i = 0;
            int ii = 0;
            try
            {
                //Эхний үлдэгдэл устгах table хоослох
                string sql = "DELETE FROM [BegBal] ";
                SqlCommand oCmd2 = new SqlCommand(sql, myConn);
                using (SqlDataReader oReader1 = oCmd2.ExecuteReader())
                {
                    while (oReader1.Read())
                    {
                    }
                }

                if (result != null)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    foreach (DataTable table in result.Tables)
                    {
                        foreach (DataRow dr in table.Rows)
                        {
                            ii = ii + 1;
                            if (!string.IsNullOrEmpty(dr[0].ToString()))
                            {
                                i = i + 1;
                                if (i > 1)
                                {
                                    string aa = dr[0].ToString();
                                    string bb = dr[4].ToString();

                                    //Эмийн багц 
                                    string typeID = "";
                                    string typeinterID = "";
                                    string shapeID = "";
                                    string unitID = "";
                                    string quantityID = "";

                                    if (!String.IsNullOrEmpty(dr[1].ToString()))
                                    {
                                        string query = "SELECT * FROM [MedicineType] WHERE REPLACE(REPLACE([name], ' ', ''),' ','') = REPLACE(REPLACE(N'" + dr[1] + "', ' ', ''),' ','')";
                                        SqlCommand oCmd = new SqlCommand(query, myConn);
                                        using (SqlDataReader oReader = oCmd.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    typeID = oReader["id"].ToString();
                                                }
                                            }
                                        }
                                    }

                                    if (!String.IsNullOrEmpty(dr[1].ToString()))
                                    {
                                        string query = "SELECT * FROM [MedicineTypeInternational] WHERE REPLACE(REPLACE([name], ' ', ''),' ','') = REPLACE(REPLACE(N'" + dr[2] + "', ' ', ''),' ','')";
                                        SqlCommand oCmd = new SqlCommand(query, myConn);
                                        using (SqlDataReader oReader = oCmd.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    typeinterID = oReader["id"].ToString();
                                                }
                                            }
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(dr[1].ToString()))
                                    {
                                        string query = "SELECT * FROM [MedicineShape] WHERE REPLACE(REPLACE([name], ' ', ''), ' ','') = REPLACE(REPLACE(N'" + dr[3] + "', ' ', ''), ' ','')";
                                        SqlCommand oCmd = new SqlCommand(query, myConn);
                                        using (SqlDataReader oReader = oCmd.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    shapeID = oReader["id"].ToString();
                                                }
                                            }
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(dr[1].ToString()))
                                    {
                                        string query = "SELECT * FROM [MedicineUnit] WHERE REPLACE(REPLACE([name], ' ', ''), ' ','') = REPLACE(REPLACE(N'" + dr[4] + "', ' ', ''), ' ','')";
                                        SqlCommand oCmd = new SqlCommand(query, myConn);
                                        using (SqlDataReader oReader = oCmd.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    unitID = oReader["id"].ToString();
                                                }
                                            }
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(dr[1].ToString()))
                                    {
                                        string query = "SELECT * FROM [MedicineQuantity] WHERE REPLACE(REPLACE([name], ' ', ''), ' ','') = REPLACE(REPLACE(N'" + dr[5] + "', ' ', ''), ' ','')";
                                        SqlCommand oCmd = new SqlCommand(query, myConn);
                                        using (SqlDataReader oReader = oCmd.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    quantityID = oReader["id"].ToString();
                                                }
                                            }
                                        }
                                    }


                                    if (typeID != "" && typeinterID != "" && shapeID != "" && unitID != "" && quantityID != "")
                                    {
                                        int add = 0;
                                        string query = "SELECT * FROM [PackageMedicine] WHERE hospitalID = '" + HospitalUtil.getHospitalId() + "' and name = N'" + dr[0] + "' and unitID = '" + unitID + "' and shapeID = '" + shapeID +
                                                    "' and quantityID = '" + quantityID + "' and medicineTypeID = '" + typeID + "' and medicinetypeinterID = '" + typeinterID + "'";
                                        SqlCommand oCmd = new SqlCommand(query, myConn);
                                        using (SqlDataReader oReader = oCmd.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    add = 1;
                                                }
                                            }
                                        }
                                        if (add == 0)
                                        {
                                            UserDataSet.Instance.PackageMedicine.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                                            UserDataSet.Instance.PackageMedicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                                            UserDataSet.Instance.PackageMedicine.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                                            DataRowView view = (DataRowView)UserDataSet.PackageMedicineBindingSource.AddNew();
                                            view["name"] = dr[0];
                                            view["unitID"] = unitID;
                                            view["shapeID"] = shapeID;
                                            view["quantityID"] = quantityID;
                                            view["medicineTypeId"] = typeID;
                                            view["medicineTypeInterID"] = typeinterID;
                                            view["medicineTypeName"] = dr[1];
                                            view["medicineTypeInterName"] = dr[2];
                                            view["shapeName"] = dr[3].ToString();
                                            view["unitName"] = dr[4];
                                            view["quantityName"] = dr[5];
                                            view.EndEdit();
                                            UserDataSet.PackageMedicineTableAdapter.Update(UserDataSet.Instance.PackageMedicine);
                                        }
                                    }
                                    else
                                    {
                                        success = ii + "-р мөрийн багцын бүртгэлийн мэдээлэл алдаатай байна!";
                                        error = 1;
                                        ProgramUtil.closeWaitDialog();
                                        XtraMessageBox.Show(success);
                                        Close();
                                    }


                                    //Эмийн бүртгэл
                                    string packageID = "";
                                    string producerID = "";
                                    string producerCountryID = "";
                                    string date = Regex.Replace(dr[8].ToString(), @"\t|\n|\r", "");
                                    decimal price = Convert.ToDecimal(dr[23]);
                                    if (!String.IsNullOrEmpty(dr[0].ToString()))
                                    {
                                        string queryp = "SELECT * FROM [PackageMedicine] WHERE hospitalID = '" + HospitalUtil.getHospitalId() + "' and name = N'" + dr[0] + "' and unitID = '" + unitID + "' and shapeID = '" + shapeID +
                                                     "' and quantityID = '" + quantityID + "' and medicineTypeID = '" + typeID + "' and medicinetypeinterID = '" + typeinterID + "'";
                                        SqlCommand oCmdp = new SqlCommand(queryp, myConn);
                                        using (SqlDataReader oReader = oCmdp.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    packageID = oReader["id"].ToString();
                                                }
                                            }
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(dr[14].ToString()))
                                    {
                                        string query = "SELECT * FROM [ProducerCountry] WHERE REPLACE(REPLACE([name], ' ', ''), ' ','') = REPLACE(REPLACE(N'" + dr[14] + "', ' ', ''), ' ','')";
                                        SqlCommand oCmd = new SqlCommand(query, myConn);
                                        using (SqlDataReader oReader = oCmd.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    producerCountryID = oReader["id"].ToString();
                                                }
                                            }
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(dr[15].ToString()))
                                    {
                                        string query = "SELECT * FROM [Producer] WHERE REPLACE(REPLACE([name], ' ', ''), ' ','') = REPLACE(REPLACE(N'" + dr[15] + "', ' ', ''), ' ','') AND " +
                                                       "REPLACE(REPLACE([countryID], ' ', ''), ' ','') = REPLACE(REPLACE(N'" + producerCountryID + "', ' ', ''), ' ','')";
                                        SqlCommand oCmd = new SqlCommand(query, myConn);
                                        using (SqlDataReader oReader = oCmd.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    producerID = oReader["id"].ToString();
                                                }
                                            }
                                        }
                                    }                                
                                    if (packageID != "")
                                    {
                                        int add = 0;
                                        string query = "SELECT * FROM [Medicine] WHERE hospitalID = '" + HospitalUtil.getHospitalId() + "' and name = N'" + dr[6] + "' and unitID = '" + unitID + "' and shapeID = '" + shapeID +
                                                    "' and quantityID = '" + quantityID + "' and medicineTypeID = '" + typeID + "' and medicinetypeinterID = '" + typeinterID + "' and latinName = N'" + dr[7] + "' and validDate = '" + date + "' and packageID = '" + packageID + "'" +
                                                    " and price = '" + price + "' and serial = '" + dr[9] + "'";
                                        SqlCommand oCmd = new SqlCommand(query, myConn);
                                        using (SqlDataReader oReader = oCmd.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    add = 1;
                                                }
                                            }
                                        }
                                        if (add == 0)
                                        {
                                            UserDataSet.Instance.Medicine.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                                            UserDataSet.Instance.Medicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                                            UserDataSet.Instance.Medicine.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                                            DataRowView view = (DataRowView)UserDataSet.MedicineBindingSource.AddNew();
                                            view["name"] = dr[6];
                                            view["latinName"] = dr[7];
                                            view["barcode"] = dr[11];
                                            view["unitID"] = unitID;
                                            view["shapeID"] = shapeID;
                                            view["quantityID"] = quantityID;
                                            view["medicineSeries"] = dr[10];
                                            view["serial"] = dr[9];
                                            view["description"] = dr[13];
                                            view["packageId"] = packageID;
                                            view["medicineTypeId"] = typeID;
                                            view["medicineTypeInterId"] = typeinterID;
                                            view["producerCountryID"] = producerCountryID;
                                            view["producerID"] = producerID;
                                            view["registerID"] = dr[16];
                                            view["btkusID"] = dr[17];
                                            view["validDate"] = date;
                                            view["price"] = price;

                                            view["PackageMedicineName"] = dr[0];
                                            view["medicineTypeName"] = dr[1];
                                            view["medicineTypeInterName"] = dr[2];
                                            view["shapeName"] = dr[3].ToString();
                                            view["unitName"] = dr[4];
                                            view["producerCountryName"] = dr[14];
                                            view["producerName"] = dr[15];
                                            view["quantityName"] = dr[5];
                                            view["parmaStandart"] = dr[12];
                                            view["alterName"] = view["latinName"].ToString() + " " + view["unitName"].ToString();
                                            view["soloName"] = view["name"].ToString() + " " + view["unitName"].ToString();
                                            view.EndEdit();
                                            UserDataSet.MedicineTableAdapter.Update(UserDataSet.Instance.Medicine);
                                        }
                                    }

                                    //Эхний үлдэгдэл
                                    string userID = "";
                                    string wardID = "";
                                    string warehouseID = "";
                                    int typebegID = 3;
                                    int incTypeID = 0;
                                    string medicineID = "";
                                    if (!String.IsNullOrEmpty(dr[18].ToString()))
                                    {
                                        string query1 = "SELECT * FROM [Users] as [u] inner join " +
                                                       "[SystemUser] as [s] on [s].[userID] = [u].[id] and ([s].[roleID] = '4' or [s].[roleID] = '5') " +
                                                       "WHERE REPLACE(REPLACE([u].[firstName] + [u].[lastName], ' ', ''), ' ','')  = REPLACE(REPLACE(N'" + dr[18] + "', ' ', ''), ' ','') " +
                                                       "AND [u].[hospitalID] = '" + HospitalUtil.getHospitalId() + "'";
                                        //string query1 = "SELECT * FROM [Users] WHERE REPLACE([userName], ' ', '') = REPLACE(N'" + dr[18] + "', ' ', '') AND [roleID] = '4' AND [hospitalID] = '" + HospitalUtil.getHospitalId() + "'";
                                        SqlCommand oCmd1 = new SqlCommand(query1, myConn);
                                        using (SqlDataReader oReader = oCmd1.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    userID = oReader["id"].ToString();
                                                }
                                            }
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(dr[19].ToString()))
                                    {
                                        if (dr[19].ToString().Replace(" ", String.Empty) == "Тасаг" || dr[19].ToString().Replace(" ", String.Empty) == "тасаг")
                                        {
                                            typebegID = 1;
                                            string query1 = "SELECT * FROM [Ward] WHERE REPLACE(REPLACE([name], ' ', ''), ' ','') = REPLACE(REPLACE(N'" + dr[20] + "', ' ', ''), ' ','') AND [hospitalID] = '" + HospitalUtil.getHospitalId() + "'";
                                            SqlCommand oCmd1 = new SqlCommand(query1, myConn);
                                            using (SqlDataReader oReader = oCmd1.ExecuteReader())
                                            {
                                                while (oReader.Read())
                                                {
                                                    if (oReader.HasRows)
                                                    {
                                                        wardID = oReader["id"].ToString();
                                                    }
                                                }
                                            }
                                        }
                                        else if (dr[19].ToString().Replace(" ", String.Empty) == "Эмийнсан" || dr[19].ToString().Replace(" ", String.Empty) == "эмийнсан")
                                        {
                                            typebegID = 0;
                                            string query1 = "SELECT * FROM [Warehouse] WHERE REPLACE(REPLACE([name], ' ', ''), ' ','') = REPLACE(REPLACE(N'" + dr[20] + "', ' ', ''), ' ','') AND " +
                                                            "[hospitalID] = '" + HospitalUtil.getHospitalId() + "'";
                                            SqlCommand oCmd1 = new SqlCommand(query1, myConn);
                                            using (SqlDataReader oReader = oCmd1.ExecuteReader())
                                            {
                                                while (oReader.Read())
                                                {
                                                    if (oReader.HasRows)
                                                    {
                                                        warehouseID = oReader["id"].ToString();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(dr[21].ToString()))
                                    {
                                        string query1 = "SELECT * FROM [IncType] WHERE REPLACE(REPLACE([name], ' ', ''), ' ','') = REPLACE(REPLACE(N'" + dr[21] + "', ' ', ''), ' ','') AND " +
                                                        "[hospitalID] = '" + HospitalUtil.getHospitalId() + "'";
                                        SqlCommand oCmd1 = new SqlCommand(query1, myConn);
                                        using (SqlDataReader oReader = oCmd1.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    incTypeID = Convert.ToInt32(oReader["id"].ToString());
                                                }
                                            }
                                        }
                                    }
                                    if (incTypeID > 0 && typebegID < 3 && !String.IsNullOrEmpty(userID))
                                    {
                                        string query1 = "SELECT * FROM [Medicine] WHERE hospitalID = '" + HospitalUtil.getHospitalId() + "' and name = N'" + dr[6] + "' and unitID = '" + unitID + "' and shapeID = '" + shapeID +
                                                   "' and quantityID = '" + quantityID + "' and medicineTypeID = '" + typeID + "' and medicinetypeinterID = '" + typeinterID + "' and latinName = N'" + dr[7] + "' and validDate = '" + date + "' and packageID = '" + packageID + "'";
                                        SqlCommand oCmd1 = new SqlCommand(query1, myConn);
                                        using (SqlDataReader oReader = oCmd1.ExecuteReader())
                                        {
                                            while (oReader.Read())
                                            {
                                                if (oReader.HasRows)
                                                {
                                                    medicineID = oReader["id"].ToString();
                                                }
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(medicineID))
                                        {
                                            JournalDataSet.Instance.BegBal.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                                            DataRowView currView = (DataRowView)JournalDataSet.BegBalBindingSource.AddNew();

                                            currView["hospitalID"] = HospitalUtil.getHospitalId();
                                            currView["userID"] = userID;
                                            currView["type"] = typebegID;
                                            currView["warehouseID"] = warehouseID;
                                            currView["wardID"] = wardID;
                                            currView["incTypeID"] = incTypeID;
                                            currView["medicineID"] = medicineID;

                                            currView["incTypeName"] = dr[21];
                                            currView["medicineName"] = dr[6];
                                            currView["latinName"] = dr[7];
                                            currView["medicineTypeName"] = dr[1];
                                            currView["unitType"] = dr[4];
                                            currView["shape"] = dr[3];
                                            currView["validDate"] = date;
                                            currView["quantity"] = dr[5];
                                            currView["serial"] = dr[9];
                                            currView["price"] = price;
                                            currView["count"] = dr[22];

                                            currView.EndEdit();
                                            JournalDataSet.BegBalTableAdapter.Update(JournalDataSet.Instance.BegBal);
                                            success = "Амжилттай хадгалагдлаа!";
                                        }
                                    }
                                    else
                                    {
                                        success = ii + "-р мөрийн эхний үлдэгдлийн бүртгэлийн мэдээлэл алдаатай байна!";
                                        error = 1;
                                        ProgramUtil.closeWaitDialog();
                                        XtraMessageBox.Show(success);
                                        Close();
                                    }
                                }
                            }
                        }
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                success = ii + "-р мөрөнд "+ex.Message +" алдаа гарлаа!";
                XtraMessageBox.Show(success);
                //XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                myConn.Close();
            }
            finally
            {
                if (error == 0)
                {
                    XtraMessageBox.Show(success);
                    // reload();
                }
                myConn.Close();
                Close();
            }
        }

        #endregion
        public MedicineBegBalIncForm()
        {
            InitializeComponent();
        }

        private void labelControl2_Click(object sender, EventArgs e)
        {

        }


    }

}
