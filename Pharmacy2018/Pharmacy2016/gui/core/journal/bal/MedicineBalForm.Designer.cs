﻿namespace Pharmacy2016.gui.core.journal.bal
{
    partial class MedicineBalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicineBalForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLeft = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.treeListWard = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnWard = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridControlBal = new DevExpress.XtraGrid.GridControl();
            this.gridViewBal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnUserName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnWardName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnUserRole = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnMedicineName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnLatinName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnMedicineTypeName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnSerial = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnBarcode = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnShape = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnUnit = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnQuantity = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnValidDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnIncTypeName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bandedGridColumnBegBal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnBegSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBandIncCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnIncCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnIncSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBandIncMotCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnIncMotCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnIncMotSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBandTotalIncCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnTotalIncCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnTotalIncSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBandExpCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnExpCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnExpSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBandExpMotCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnExpMotCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnExpMotSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBandExpPatientCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnExpPatientCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnExpPatientSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBandTotalExpCount = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnTotalExpCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnTotalExpSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnEndBal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnEndSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridLookUpEditUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.checkedComboBoxEdit = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.labelControlAll = new DevExpress.XtraEditors.LabelControl();
            this.checkEditAll = new DevExpress.XtraEditors.CheckEdit();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemWard = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCustomize = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            this.dockPanelLeft.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLeft});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanelLeft
            // 
            this.dockPanelLeft.Controls.Add(this.dockPanel1_Container);
            this.dockPanelLeft.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelLeft.ID = new System.Guid("32690437-4757-43e6-b1e2-053573c2cee2");
            this.dockPanelLeft.Location = new System.Drawing.Point(0, 0);
            this.dockPanelLeft.Name = "dockPanelLeft";
            this.dockPanelLeft.Options.ShowCloseButton = false;
            this.dockPanelLeft.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelLeft.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelLeft.SavedIndex = 0;
            this.dockPanelLeft.Size = new System.Drawing.Size(200, 461);
            this.dockPanelLeft.Text = "Тасаг";
            this.dockPanelLeft.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.treeListWard);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(192, 434);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // treeListWard
            // 
            this.treeListWard.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnWard});
            this.treeListWard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListWard.Location = new System.Drawing.Point(0, 0);
            this.treeListWard.Name = "treeListWard";
            this.treeListWard.BeginUnboundLoad();
            this.treeListWard.AppendNode(new object[] {
            "Хавдар судлалын үндэсний төв"}, -1);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 1"}, 0);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 2"}, 0);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 3"}, 0);
            this.treeListWard.EndUnboundLoad();
            this.treeListWard.OptionsBehavior.Editable = false;
            this.treeListWard.OptionsBehavior.ExpandNodeOnDrag = false;
            this.treeListWard.OptionsView.ShowColumns = false;
            this.treeListWard.OptionsView.ShowIndicator = false;
            this.treeListWard.Size = new System.Drawing.Size(192, 434);
            this.treeListWard.TabIndex = 0;
            // 
            // treeListColumnWard
            // 
            this.treeListColumnWard.Caption = "Тасаг";
            this.treeListColumnWard.FieldName = "ward";
            this.treeListColumnWard.MinWidth = 52;
            this.treeListColumnWard.Name = "treeListColumnWard";
            this.treeListColumnWard.OptionsColumn.ReadOnly = true;
            this.treeListColumnWard.Visible = true;
            this.treeListColumnWard.VisibleIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControlBal);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 461);
            this.panelControl1.TabIndex = 0;
            // 
            // gridControlBal
            // 
            this.gridControlBal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlBal.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlBal.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlBal.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlBal.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlBal.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlBal.Location = new System.Drawing.Point(2, 72);
            this.gridControlBal.MainView = this.gridViewBal;
            this.gridControlBal.Name = "gridControlBal";
            this.gridControlBal.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemDateEdit1});
            this.gridControlBal.Size = new System.Drawing.Size(780, 387);
            this.gridControlBal.TabIndex = 11;
            this.gridControlBal.UseEmbeddedNavigator = true;
            this.gridControlBal.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBal});
            this.gridControlBal.Click += new System.EventHandler(this.gridControlBal_Click);
            // 
            // gridViewBal
            // 
            this.gridViewBal.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBandIncCount,
            this.gridBandIncMotCount,
            this.gridBandTotalIncCount,
            this.gridBandExpCount,
            this.gridBandExpMotCount,
            this.gridBandExpPatientCount,
            this.gridBandTotalExpCount,
            this.gridBand6});
            this.gridViewBal.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.gridColumnUserName,
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumnWardName,
            this.gridColumnUserRole,
            this.bandedGridColumnMedicineName,
            this.bandedGridColumnLatinName,
            this.bandedGridColumnMedicineTypeName,
            this.bandedGridColumnBarcode,
            this.bandedGridColumnUnit,
            this.bandedGridColumnIncTypeName,
            this.bandedGridColumnPrice,
            this.bandedGridColumnBegBal,
            this.bandedGridColumnBegSumPrice,
            this.bandedGridColumnTotalIncCount,
            this.bandedGridColumnTotalIncSumPrice,
            this.bandedGridColumnTotalExpCount,
            this.bandedGridColumnTotalExpSumPrice,
            this.bandedGridColumnEndBal,
            this.bandedGridColumnEndSumPrice,
            this.bandedGridColumnShape,
            this.bandedGridColumnValidDate,
            this.bandedGridColumnIncCount,
            this.bandedGridColumnIncSumPrice,
            this.bandedGridColumnIncMotCount,
            this.bandedGridColumnIncMotSumPrice,
            this.bandedGridColumnExpCount,
            this.bandedGridColumnExpSumPrice,
            this.bandedGridColumnExpMotCount,
            this.bandedGridColumnExpMotSumPrice,
            this.bandedGridColumnExpPatientCount,
            this.bandedGridColumnExpPatientSumPrice,
            this.bandedGridColumnQuantity,
            this.bandedGridColumnSerial});
            this.gridViewBal.GridControl = this.gridControlBal;
            this.gridViewBal.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "begBal", this.bandedGridColumnBegBal, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "begSumPrice", this.bandedGridColumnBegSumPrice, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "endBal", this.bandedGridColumnEndBal, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "endSumPrice", this.bandedGridColumnEndSumPrice, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incCount", this.bandedGridColumnIncCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incSumPrice", this.bandedGridColumnIncSumPrice, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incMotCount", this.bandedGridColumnIncMotCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incMotSumPrice", this.bandedGridColumnIncMotSumPrice, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalIncCount", this.bandedGridColumnTotalIncCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalIncSumPrice", this.bandedGridColumnTotalIncSumPrice, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expCount", this.bandedGridColumnExpCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expSumPrice", this.bandedGridColumnExpSumPrice, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expMotCount", this.bandedGridColumnExpMotCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expMotSumPrice", this.bandedGridColumnExpMotSumPrice, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expPatientCount", this.bandedGridColumnExpPatientCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expPatientSumPrice", this.bandedGridColumnExpPatientSumPrice, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalExpCount", this.bandedGridColumnTotalExpCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalExpSumPrice", this.bandedGridColumnTotalExpSumPrice, "{0:c2}")});
            this.gridViewBal.Name = "gridViewBal";
            this.gridViewBal.OptionsBehavior.ReadOnly = true;
            this.gridViewBal.OptionsFind.AlwaysVisible = true;
            this.gridViewBal.OptionsFind.FindNullPrompt = "Хайх үг ээ оруулна уу";
            this.gridViewBal.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewBal.OptionsView.ShowAutoFilterRow = true;
            this.gridViewBal.OptionsView.ShowFooter = true;
            this.gridViewBal.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewBal_CustomDrawCell);
            this.gridViewBal.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridViewBal_CustomDrawGroupRow);
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Нярав";
            this.gridBand4.Columns.Add(this.gridColumnUserName);
            this.gridBand4.Columns.Add(this.bandedGridColumn1);
            this.gridBand4.Columns.Add(this.bandedGridColumn2);
            this.gridBand4.CustomizationCaption = "Нярав";
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 200;
            // 
            // gridColumnUserName
            // 
            this.gridColumnUserName.Caption = "Дугаар";
            this.gridColumnUserName.CustomizationCaption = "Агуулахын дугаар";
            this.gridColumnUserName.FieldName = "userID";
            this.gridColumnUserName.MinWidth = 75;
            this.gridColumnUserName.Name = "gridColumnUserName";
            this.gridColumnUserName.Visible = true;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "Нэр";
            this.bandedGridColumn1.CustomizationCaption = "Хэрэглэгчийн нэр";
            this.bandedGridColumn1.FieldName = "userName";
            this.bandedGridColumn1.MinWidth = 75;
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 125;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "Эрх";
            this.bandedGridColumn2.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.bandedGridColumn2.FieldName = "roleName";
            this.bandedGridColumn2.MinWidth = 75;
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.Width = 81;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Агуулах";
            this.gridBand1.Columns.Add(this.bandedGridColumnWardName);
            this.gridBand1.Columns.Add(this.gridColumnUserRole);
            this.gridBand1.CustomizationCaption = "Агуулах";
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 1;
            this.gridBand1.Width = 231;
            // 
            // bandedGridColumnWardName
            // 
            this.bandedGridColumnWardName.Caption = "Нэр";
            this.bandedGridColumnWardName.CustomizationCaption = "Агуулахын нэр";
            this.bandedGridColumnWardName.FieldName = "warehouseName";
            this.bandedGridColumnWardName.MinWidth = 75;
            this.bandedGridColumnWardName.Name = "bandedGridColumnWardName";
            this.bandedGridColumnWardName.Visible = true;
            this.bandedGridColumnWardName.Width = 128;
            // 
            // gridColumnUserRole
            // 
            this.gridColumnUserRole.Caption = "Дэлгэрэнгүй";
            this.gridColumnUserRole.CustomizationCaption = "Агуулахын дэлгэрэнгүй";
            this.gridColumnUserRole.FieldName = "warehouseDesc";
            this.gridColumnUserRole.MinWidth = 75;
            this.gridColumnUserRole.Name = "gridColumnUserRole";
            this.gridColumnUserRole.Visible = true;
            this.gridColumnUserRole.Width = 103;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Эм";
            this.gridBand2.Columns.Add(this.bandedGridColumnMedicineName);
            this.gridBand2.Columns.Add(this.bandedGridColumnLatinName);
            this.gridBand2.Columns.Add(this.bandedGridColumnMedicineTypeName);
            this.gridBand2.Columns.Add(this.bandedGridColumnSerial);
            this.gridBand2.Columns.Add(this.bandedGridColumnBarcode);
            this.gridBand2.Columns.Add(this.bandedGridColumnShape);
            this.gridBand2.Columns.Add(this.bandedGridColumnUnit);
            this.gridBand2.Columns.Add(this.bandedGridColumnQuantity);
            this.gridBand2.Columns.Add(this.bandedGridColumnValidDate);
            this.gridBand2.CustomizationCaption = "Эм";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 2;
            this.gridBand2.Width = 525;
            // 
            // bandedGridColumnMedicineName
            // 
            this.bandedGridColumnMedicineName.Caption = "Нэр";
            this.bandedGridColumnMedicineName.CustomizationCaption = "Худалдааны нэр";
            this.bandedGridColumnMedicineName.FieldName = "medicineName";
            this.bandedGridColumnMedicineName.MinWidth = 75;
            this.bandedGridColumnMedicineName.Name = "bandedGridColumnMedicineName";
            this.bandedGridColumnMedicineName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumnMedicineName.Visible = true;
            // 
            // bandedGridColumnLatinName
            // 
            this.bandedGridColumnLatinName.Caption = "ОУ нэр";
            this.bandedGridColumnLatinName.CustomizationCaption = "Олон улсын нэр";
            this.bandedGridColumnLatinName.FieldName = "latinName";
            this.bandedGridColumnLatinName.MinWidth = 75;
            this.bandedGridColumnLatinName.Name = "bandedGridColumnLatinName";
            this.bandedGridColumnLatinName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumnLatinName.Visible = true;
            // 
            // bandedGridColumnMedicineTypeName
            // 
            this.bandedGridColumnMedicineTypeName.Caption = "Ангилал";
            this.bandedGridColumnMedicineTypeName.CustomizationCaption = "Эмийн ангилал";
            this.bandedGridColumnMedicineTypeName.FieldName = "medicineTypeName";
            this.bandedGridColumnMedicineTypeName.MinWidth = 75;
            this.bandedGridColumnMedicineTypeName.Name = "bandedGridColumnMedicineTypeName";
            this.bandedGridColumnMedicineTypeName.Visible = true;
            // 
            // bandedGridColumnSerial
            // 
            this.bandedGridColumnSerial.Caption = "Сериал";
            this.bandedGridColumnSerial.CustomizationCaption = "Сериал";
            this.bandedGridColumnSerial.FieldName = "serial";
            this.bandedGridColumnSerial.MinWidth = 75;
            this.bandedGridColumnSerial.Name = "bandedGridColumnSerial";
            this.bandedGridColumnSerial.Visible = true;
            // 
            // bandedGridColumnBarcode
            // 
            this.bandedGridColumnBarcode.Caption = "Бар код";
            this.bandedGridColumnBarcode.CustomizationCaption = "Бар код";
            this.bandedGridColumnBarcode.FieldName = "barcode";
            this.bandedGridColumnBarcode.MinWidth = 75;
            this.bandedGridColumnBarcode.Name = "bandedGridColumnBarcode";
            this.bandedGridColumnBarcode.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            // 
            // bandedGridColumnShape
            // 
            this.bandedGridColumnShape.Caption = "Хэлбэр";
            this.bandedGridColumnShape.CustomizationCaption = "Хэлбэр";
            this.bandedGridColumnShape.FieldName = "shape";
            this.bandedGridColumnShape.MinWidth = 75;
            this.bandedGridColumnShape.Name = "bandedGridColumnShape";
            // 
            // bandedGridColumnUnit
            // 
            this.bandedGridColumnUnit.Caption = "Тун хэмжээ";
            this.bandedGridColumnUnit.CustomizationCaption = "Тун хэмжээ";
            this.bandedGridColumnUnit.FieldName = "unitType";
            this.bandedGridColumnUnit.MinWidth = 75;
            this.bandedGridColumnUnit.Name = "bandedGridColumnUnit";
            this.bandedGridColumnUnit.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumnUnit.Visible = true;
            // 
            // bandedGridColumnQuantity
            // 
            this.bandedGridColumnQuantity.Caption = "Хэмжих нэгж";
            this.bandedGridColumnQuantity.CustomizationCaption = "Хэмжих нэгж";
            this.bandedGridColumnQuantity.FieldName = "quantity";
            this.bandedGridColumnQuantity.MinWidth = 75;
            this.bandedGridColumnQuantity.Name = "bandedGridColumnQuantity";
            this.bandedGridColumnQuantity.Visible = true;
            // 
            // bandedGridColumnValidDate
            // 
            this.bandedGridColumnValidDate.Caption = "Хүчинтэй хугацаа";
            this.bandedGridColumnValidDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumnValidDate.CustomizationCaption = "Хүчинтэй хугацаа";
            this.bandedGridColumnValidDate.FieldName = "validDate";
            this.bandedGridColumnValidDate.MinWidth = 75;
            this.bandedGridColumnValidDate.Name = "bandedGridColumnValidDate";
            this.bandedGridColumnValidDate.Visible = true;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Эхний үлдэгдэл";
            this.gridBand3.Columns.Add(this.bandedGridColumnIncTypeName);
            this.gridBand3.Columns.Add(this.bandedGridColumnPrice);
            this.gridBand3.Columns.Add(this.bandedGridColumnBegBal);
            this.gridBand3.Columns.Add(this.bandedGridColumnBegSumPrice);
            this.gridBand3.CustomizationCaption = "Эхний үлдэгдэл";
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 3;
            this.gridBand3.Width = 225;
            // 
            // bandedGridColumnIncTypeName
            // 
            this.bandedGridColumnIncTypeName.Caption = "Орлогын төрөл";
            this.bandedGridColumnIncTypeName.CustomizationCaption = "Орлогын төрөл";
            this.bandedGridColumnIncTypeName.FieldName = "incTypeName";
            this.bandedGridColumnIncTypeName.MinWidth = 75;
            this.bandedGridColumnIncTypeName.Name = "bandedGridColumnIncTypeName";
            // 
            // bandedGridColumnPrice
            // 
            this.bandedGridColumnPrice.Caption = "Нэгжийн үнэ";
            this.bandedGridColumnPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnPrice.CustomizationCaption = "Эмийн үнэ";
            this.bandedGridColumnPrice.FieldName = "price";
            this.bandedGridColumnPrice.MinWidth = 75;
            this.bandedGridColumnPrice.Name = "bandedGridColumnPrice";
            this.bandedGridColumnPrice.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumnPrice.Visible = true;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // bandedGridColumnBegBal
            // 
            this.bandedGridColumnBegBal.Caption = "Тоо хэмжээ";
            this.bandedGridColumnBegBal.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnBegBal.CustomizationCaption = "Эхний үлдэгдэл тоо";
            this.bandedGridColumnBegBal.FieldName = "begBal";
            this.bandedGridColumnBegBal.MinWidth = 75;
            this.bandedGridColumnBegBal.Name = "bandedGridColumnBegBal";
            this.bandedGridColumnBegBal.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumnBegBal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "begBal", "{0:n2}")});
            this.bandedGridColumnBegBal.Visible = true;
            // 
            // bandedGridColumnBegSumPrice
            // 
            this.bandedGridColumnBegSumPrice.Caption = "Дүн";
            this.bandedGridColumnBegSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnBegSumPrice.CustomizationCaption = "Эхний үлдэгдлийн дүн";
            this.bandedGridColumnBegSumPrice.FieldName = "begSumPrice";
            this.bandedGridColumnBegSumPrice.MinWidth = 75;
            this.bandedGridColumnBegSumPrice.Name = "bandedGridColumnBegSumPrice";
            this.bandedGridColumnBegSumPrice.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumnBegSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "begSumPrice", "{0:c2}")});
            this.bandedGridColumnBegSumPrice.Visible = true;
            // 
            // gridBandIncCount
            // 
            this.gridBandIncCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandIncCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandIncCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandIncCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandIncCount.Caption = "Орлого";
            this.gridBandIncCount.Columns.Add(this.bandedGridColumnIncCount);
            this.gridBandIncCount.Columns.Add(this.bandedGridColumnIncSumPrice);
            this.gridBandIncCount.CustomizationCaption = "Орлогын журнал";
            this.gridBandIncCount.Name = "gridBandIncCount";
            this.gridBandIncCount.Visible = false;
            this.gridBandIncCount.VisibleIndex = -1;
            this.gridBandIncCount.Width = 150;
            // 
            // bandedGridColumnIncCount
            // 
            this.bandedGridColumnIncCount.Caption = "Тоо хэмжээ";
            this.bandedGridColumnIncCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnIncCount.CustomizationCaption = "Орлогын журналын нийт тоо";
            this.bandedGridColumnIncCount.FieldName = "incCount";
            this.bandedGridColumnIncCount.MinWidth = 75;
            this.bandedGridColumnIncCount.Name = "bandedGridColumnIncCount";
            this.bandedGridColumnIncCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incCount", "{0:n2}")});
            this.bandedGridColumnIncCount.Visible = true;
            // 
            // bandedGridColumnIncSumPrice
            // 
            this.bandedGridColumnIncSumPrice.Caption = "Дүн";
            this.bandedGridColumnIncSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnIncSumPrice.CustomizationCaption = "Орлогын журналын дүн";
            this.bandedGridColumnIncSumPrice.FieldName = "incSumPrice";
            this.bandedGridColumnIncSumPrice.MinWidth = 75;
            this.bandedGridColumnIncSumPrice.Name = "bandedGridColumnIncSumPrice";
            this.bandedGridColumnIncSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incSumPrice", "{0:c2}")});
            this.bandedGridColumnIncSumPrice.Visible = true;
            // 
            // gridBandIncMotCount
            // 
            this.gridBandIncMotCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandIncMotCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandIncMotCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandIncMotCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandIncMotCount.Caption = "Шилжүүлсэн орлого";
            this.gridBandIncMotCount.Columns.Add(this.bandedGridColumnIncMotCount);
            this.gridBandIncMotCount.Columns.Add(this.bandedGridColumnIncMotSumPrice);
            this.gridBandIncMotCount.CustomizationCaption = "Шилжүүлсэн орлого";
            this.gridBandIncMotCount.Name = "gridBandIncMotCount";
            this.gridBandIncMotCount.Visible = false;
            this.gridBandIncMotCount.VisibleIndex = -1;
            this.gridBandIncMotCount.Width = 150;
            // 
            // bandedGridColumnIncMotCount
            // 
            this.bandedGridColumnIncMotCount.Caption = "Тоо хэмжээ";
            this.bandedGridColumnIncMotCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnIncMotCount.CustomizationCaption = "Шилжүүлсэн орлогын нийт тоо";
            this.bandedGridColumnIncMotCount.FieldName = "incMotCount";
            this.bandedGridColumnIncMotCount.MinWidth = 75;
            this.bandedGridColumnIncMotCount.Name = "bandedGridColumnIncMotCount";
            this.bandedGridColumnIncMotCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incMotCount", "{0:n2}")});
            this.bandedGridColumnIncMotCount.Visible = true;
            // 
            // bandedGridColumnIncMotSumPrice
            // 
            this.bandedGridColumnIncMotSumPrice.Caption = "Дүн";
            this.bandedGridColumnIncMotSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnIncMotSumPrice.CustomizationCaption = "Шилжүүлсэн орлогын дүн";
            this.bandedGridColumnIncMotSumPrice.FieldName = "incMotSumPrice";
            this.bandedGridColumnIncMotSumPrice.MinWidth = 75;
            this.bandedGridColumnIncMotSumPrice.Name = "bandedGridColumnIncMotSumPrice";
            this.bandedGridColumnIncMotSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "incMotSumPrice", "{0:c2}")});
            this.bandedGridColumnIncMotSumPrice.Visible = true;
            // 
            // gridBandTotalIncCount
            // 
            this.gridBandTotalIncCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandTotalIncCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandTotalIncCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandTotalIncCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandTotalIncCount.Caption = "Орлого";
            this.gridBandTotalIncCount.Columns.Add(this.bandedGridColumnTotalIncCount);
            this.gridBandTotalIncCount.Columns.Add(this.bandedGridColumnTotalIncSumPrice);
            this.gridBandTotalIncCount.CustomizationCaption = "Нийт орлого";
            this.gridBandTotalIncCount.Name = "gridBandTotalIncCount";
            this.gridBandTotalIncCount.VisibleIndex = 4;
            this.gridBandTotalIncCount.Width = 150;
            // 
            // bandedGridColumnTotalIncCount
            // 
            this.bandedGridColumnTotalIncCount.Caption = "Тоо хэмжээ";
            this.bandedGridColumnTotalIncCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnTotalIncCount.CustomizationCaption = "Нийт орлогын тоо";
            this.bandedGridColumnTotalIncCount.FieldName = "totalIncCount";
            this.bandedGridColumnTotalIncCount.MinWidth = 75;
            this.bandedGridColumnTotalIncCount.Name = "bandedGridColumnTotalIncCount";
            this.bandedGridColumnTotalIncCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalIncCount", "{0:n2}")});
            this.bandedGridColumnTotalIncCount.Visible = true;
            // 
            // bandedGridColumnTotalIncSumPrice
            // 
            this.bandedGridColumnTotalIncSumPrice.Caption = "Дүн";
            this.bandedGridColumnTotalIncSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnTotalIncSumPrice.CustomizationCaption = "Орлогын дүн";
            this.bandedGridColumnTotalIncSumPrice.FieldName = "totalIncSumPrice";
            this.bandedGridColumnTotalIncSumPrice.MinWidth = 75;
            this.bandedGridColumnTotalIncSumPrice.Name = "bandedGridColumnTotalIncSumPrice";
            this.bandedGridColumnTotalIncSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalIncSumPrice", "{0:c2}")});
            this.bandedGridColumnTotalIncSumPrice.Visible = true;
            // 
            // gridBandExpCount
            // 
            this.gridBandExpCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandExpCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandExpCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandExpCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandExpCount.Caption = "Зарлага";
            this.gridBandExpCount.Columns.Add(this.bandedGridColumnExpCount);
            this.gridBandExpCount.Columns.Add(this.bandedGridColumnExpSumPrice);
            this.gridBandExpCount.CustomizationCaption = "Зарлагын журнал";
            this.gridBandExpCount.Name = "gridBandExpCount";
            this.gridBandExpCount.Visible = false;
            this.gridBandExpCount.VisibleIndex = -1;
            this.gridBandExpCount.Width = 150;
            // 
            // bandedGridColumnExpCount
            // 
            this.bandedGridColumnExpCount.Caption = "Тоо хэмжээ";
            this.bandedGridColumnExpCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnExpCount.CustomizationCaption = "Зарлагын журналын нийт тоо";
            this.bandedGridColumnExpCount.FieldName = "expCount";
            this.bandedGridColumnExpCount.MinWidth = 75;
            this.bandedGridColumnExpCount.Name = "bandedGridColumnExpCount";
            this.bandedGridColumnExpCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expCount", "{0:n2}")});
            this.bandedGridColumnExpCount.Visible = true;
            // 
            // bandedGridColumnExpSumPrice
            // 
            this.bandedGridColumnExpSumPrice.Caption = "Дүн";
            this.bandedGridColumnExpSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnExpSumPrice.CustomizationCaption = "Зарлагын журналын дүн";
            this.bandedGridColumnExpSumPrice.FieldName = "expSumPrice";
            this.bandedGridColumnExpSumPrice.MinWidth = 75;
            this.bandedGridColumnExpSumPrice.Name = "bandedGridColumnExpSumPrice";
            this.bandedGridColumnExpSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expSumPrice", "{0:c2}")});
            this.bandedGridColumnExpSumPrice.Visible = true;
            // 
            // gridBandExpMotCount
            // 
            this.gridBandExpMotCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandExpMotCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandExpMotCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandExpMotCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandExpMotCount.Caption = "Шилжүүлсэн зарлага";
            this.gridBandExpMotCount.Columns.Add(this.bandedGridColumnExpMotCount);
            this.gridBandExpMotCount.Columns.Add(this.bandedGridColumnExpMotSumPrice);
            this.gridBandExpMotCount.CustomizationCaption = "Шилжүүлсэн зарлага";
            this.gridBandExpMotCount.Name = "gridBandExpMotCount";
            this.gridBandExpMotCount.Visible = false;
            this.gridBandExpMotCount.VisibleIndex = -1;
            this.gridBandExpMotCount.Width = 150;
            // 
            // bandedGridColumnExpMotCount
            // 
            this.bandedGridColumnExpMotCount.Caption = "Тоо хэмжээ";
            this.bandedGridColumnExpMotCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnExpMotCount.CustomizationCaption = "Шилжүүлсэн зарлагын нийт тоо";
            this.bandedGridColumnExpMotCount.FieldName = "expMotCount";
            this.bandedGridColumnExpMotCount.MinWidth = 75;
            this.bandedGridColumnExpMotCount.Name = "bandedGridColumnExpMotCount";
            this.bandedGridColumnExpMotCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expMotCount", "{0:n2}")});
            this.bandedGridColumnExpMotCount.Visible = true;
            // 
            // bandedGridColumnExpMotSumPrice
            // 
            this.bandedGridColumnExpMotSumPrice.Caption = "Дүн";
            this.bandedGridColumnExpMotSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnExpMotSumPrice.CustomizationCaption = "Шилжүүлсэн зарлагын дүн";
            this.bandedGridColumnExpMotSumPrice.FieldName = "expMotSumPrice";
            this.bandedGridColumnExpMotSumPrice.MinWidth = 75;
            this.bandedGridColumnExpMotSumPrice.Name = "bandedGridColumnExpMotSumPrice";
            this.bandedGridColumnExpMotSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expMotSumPrice", "{0:c2}")});
            this.bandedGridColumnExpMotSumPrice.Visible = true;
            // 
            // gridBandExpPatientCount
            // 
            this.gridBandExpPatientCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandExpPatientCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandExpPatientCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandExpPatientCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandExpPatientCount.Caption = "Эмчлүүлэгчид олгосон";
            this.gridBandExpPatientCount.Columns.Add(this.bandedGridColumnExpPatientCount);
            this.gridBandExpPatientCount.Columns.Add(this.bandedGridColumnExpPatientSumPrice);
            this.gridBandExpPatientCount.CustomizationCaption = "Эмчлүүлэгчид олгосон";
            this.gridBandExpPatientCount.Name = "gridBandExpPatientCount";
            this.gridBandExpPatientCount.Visible = false;
            this.gridBandExpPatientCount.VisibleIndex = -1;
            this.gridBandExpPatientCount.Width = 150;
            // 
            // bandedGridColumnExpPatientCount
            // 
            this.bandedGridColumnExpPatientCount.Caption = "Тоо хэмжээ";
            this.bandedGridColumnExpPatientCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnExpPatientCount.CustomizationCaption = "Эмчлүүлэгчид олгосон нийт тоо";
            this.bandedGridColumnExpPatientCount.FieldName = "expPatientCount";
            this.bandedGridColumnExpPatientCount.MinWidth = 75;
            this.bandedGridColumnExpPatientCount.Name = "bandedGridColumnExpPatientCount";
            this.bandedGridColumnExpPatientCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expPatientCount", "{0:n2}")});
            this.bandedGridColumnExpPatientCount.Visible = true;
            // 
            // bandedGridColumnExpPatientSumPrice
            // 
            this.bandedGridColumnExpPatientSumPrice.Caption = "Дүн";
            this.bandedGridColumnExpPatientSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnExpPatientSumPrice.CustomizationCaption = "Эмчлүүлэгчид олгосон дүн";
            this.bandedGridColumnExpPatientSumPrice.FieldName = "expPatientSumPrice";
            this.bandedGridColumnExpPatientSumPrice.MinWidth = 75;
            this.bandedGridColumnExpPatientSumPrice.Name = "bandedGridColumnExpPatientSumPrice";
            this.bandedGridColumnExpPatientSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expPatientSumPrice", "{0:c2}")});
            this.bandedGridColumnExpPatientSumPrice.Visible = true;
            // 
            // gridBandTotalExpCount
            // 
            this.gridBandTotalExpCount.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBandTotalExpCount.AppearanceHeader.Options.UseFont = true;
            this.gridBandTotalExpCount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBandTotalExpCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBandTotalExpCount.Caption = "Зарлага";
            this.gridBandTotalExpCount.Columns.Add(this.bandedGridColumnTotalExpCount);
            this.gridBandTotalExpCount.Columns.Add(this.bandedGridColumnTotalExpSumPrice);
            this.gridBandTotalExpCount.CustomizationCaption = "Нийт зарлага";
            this.gridBandTotalExpCount.Name = "gridBandTotalExpCount";
            this.gridBandTotalExpCount.VisibleIndex = 5;
            this.gridBandTotalExpCount.Width = 150;
            // 
            // bandedGridColumnTotalExpCount
            // 
            this.bandedGridColumnTotalExpCount.Caption = "Тоо хэмжээ";
            this.bandedGridColumnTotalExpCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnTotalExpCount.CustomizationCaption = "Нийт зарлагын тоо";
            this.bandedGridColumnTotalExpCount.FieldName = "totalExpCount";
            this.bandedGridColumnTotalExpCount.MinWidth = 75;
            this.bandedGridColumnTotalExpCount.Name = "bandedGridColumnTotalExpCount";
            this.bandedGridColumnTotalExpCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalExpCount", "{0:n2}")});
            this.bandedGridColumnTotalExpCount.Visible = true;
            // 
            // bandedGridColumnTotalExpSumPrice
            // 
            this.bandedGridColumnTotalExpSumPrice.Caption = "Дүн";
            this.bandedGridColumnTotalExpSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnTotalExpSumPrice.CustomizationCaption = "Зарлагын дүн";
            this.bandedGridColumnTotalExpSumPrice.FieldName = "totalExpSumPrice";
            this.bandedGridColumnTotalExpSumPrice.MinWidth = 75;
            this.bandedGridColumnTotalExpSumPrice.Name = "bandedGridColumnTotalExpSumPrice";
            this.bandedGridColumnTotalExpSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalExpSumPrice", "{0:c2}")});
            this.bandedGridColumnTotalExpSumPrice.Visible = true;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand6.AppearanceHeader.Options.UseFont = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Эцсийн үлдэгдэл";
            this.gridBand6.Columns.Add(this.bandedGridColumnEndBal);
            this.gridBand6.Columns.Add(this.bandedGridColumnEndSumPrice);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 6;
            this.gridBand6.Width = 150;
            // 
            // bandedGridColumnEndBal
            // 
            this.bandedGridColumnEndBal.Caption = "Тоо хэмжээ";
            this.bandedGridColumnEndBal.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnEndBal.CustomizationCaption = "Эцсийн үлдэгдлийн тоо";
            this.bandedGridColumnEndBal.FieldName = "endBal";
            this.bandedGridColumnEndBal.MinWidth = 75;
            this.bandedGridColumnEndBal.Name = "bandedGridColumnEndBal";
            this.bandedGridColumnEndBal.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "endBal", "{0:n2}")});
            this.bandedGridColumnEndBal.Visible = true;
            // 
            // bandedGridColumnEndSumPrice
            // 
            this.bandedGridColumnEndSumPrice.Caption = "Дүн";
            this.bandedGridColumnEndSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnEndSumPrice.CustomizationCaption = "Эцсийн үлдэгдлийн дүн";
            this.bandedGridColumnEndSumPrice.FieldName = "endSumPrice";
            this.bandedGridColumnEndSumPrice.MinWidth = 75;
            this.bandedGridColumnEndSumPrice.Name = "bandedGridColumnEndSumPrice";
            this.bandedGridColumnEndSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "endSumPrice", "{0:c2}")});
            this.bandedGridColumnEndSumPrice.Visible = true;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridLookUpEditUser);
            this.panelControl2.Controls.Add(this.simpleButtonLayout);
            this.panelControl2.Controls.Add(this.checkedComboBoxEdit);
            this.panelControl2.Controls.Add(this.labelControlAll);
            this.panelControl2.Controls.Add(this.checkEditAll);
            this.panelControl2.Controls.Add(this.dateEditEnd);
            this.panelControl2.Controls.Add(this.dateEditStart);
            this.panelControl2.Controls.Add(this.dropDownButtonPrint);
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(780, 70);
            this.panelControl2.TabIndex = 1;
            // 
            // gridLookUpEditUser
            // 
            this.gridLookUpEditUser.EditValue = "";
            this.gridLookUpEditUser.Location = new System.Drawing.Point(287, 12);
            this.gridLookUpEditUser.Name = "gridLookUpEditUser";
            this.gridLookUpEditUser.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("gridLookUpEditUser.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "reload", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditUser.Properties.NullText = "Хэрэглэгч сонгох";
            this.gridLookUpEditUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditUser.Properties.ValueMember = "id";
            this.gridLookUpEditUser.Properties.View = this.gridView1;
            this.gridLookUpEditUser.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditUser_Properties_ButtonClick);
            this.gridLookUpEditUser.Size = new System.Drawing.Size(200, 22);
            this.gridLookUpEditUser.TabIndex = 10;
            this.gridLookUpEditUser.ToolTip = "Хэрэглэгч сонгох";
            this.gridLookUpEditUser.EditValueChanged += new System.EventHandler(this.gridLookUpEditUser_EditValueChanged);
            this.gridLookUpEditUser.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.gridLookUpEditUser_CustomDisplayText);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Дугаар";
            this.gridColumn4.CustomizationCaption = "Дугаар";
            this.gridColumn4.FieldName = "id";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 3;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Нэр";
            this.gridColumn2.CustomizationCaption = "Нэр";
            this.gridColumn2.FieldName = "firstName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Эрх";
            this.gridColumn5.CustomizationCaption = "Эрх";
            this.gridColumn5.FieldName = "roleName";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тасаг";
            this.gridColumn6.CustomizationCaption = "Тасаг";
            this.gridColumn6.FieldName = "wardName";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(747, 10);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 9;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // checkedComboBoxEdit
            // 
            this.checkedComboBoxEdit.EditValue = "totalIncCount, totalExpCount";
            this.checkedComboBoxEdit.Location = new System.Drawing.Point(10, 39);
            this.checkedComboBoxEdit.Name = "checkedComboBoxEdit";
            this.checkedComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEdit.Properties.DropDownRows = 8;
            this.checkedComboBoxEdit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("incCount", "Орлогын журнал"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("incMotCount", "Шилжүүлгийн орлого"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("totalIncCount", "Нийт Орлого", System.Windows.Forms.CheckState.Checked),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("expCount", "Зарлагын журнал"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("expMotCount", "Шилжүүлгийн зарлага"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("expPatientCount", "Эмчлүүлэгчид олгосон"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("totalExpCount", "Нийт зарлага", System.Windows.Forms.CheckState.Checked)});
            this.checkedComboBoxEdit.Size = new System.Drawing.Size(206, 20);
            this.checkedComboBoxEdit.TabIndex = 6;
            this.checkedComboBoxEdit.ToolTip = "Орлого, зарлагын багануудыг сонгох";
            this.checkedComboBoxEdit.Visible = false;
            this.checkedComboBoxEdit.EditValueChanged += new System.EventHandler(this.checkedComboBoxEdit_EditValueChanged);
            // 
            // labelControlAll
            // 
            this.labelControlAll.Location = new System.Drawing.Point(226, 16);
            this.labelControlAll.Name = "labelControlAll";
            this.labelControlAll.Size = new System.Drawing.Size(30, 13);
            this.labelControlAll.TabIndex = 8;
            this.labelControlAll.Text = "Бүгд :";
            // 
            // checkEditAll
            // 
            this.checkEditAll.Location = new System.Drawing.Point(262, 13);
            this.checkEditAll.Name = "checkEditAll";
            this.checkEditAll.Properties.Caption = "";
            this.checkEditAll.Size = new System.Drawing.Size(19, 19);
            this.checkEditAll.TabIndex = 3;
            this.checkEditAll.ToolTip = "Бүгдийг сонгох";
            this.checkEditAll.CheckedChanged += new System.EventHandler(this.checkEditAll_CheckedChanged);
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(116, 13);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(100, 20);
            this.dateEditEnd.TabIndex = 2;
            this.dateEditEnd.ToolTip = "Дуусах огноо";
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(10, 13);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(100, 20);
            this.dateEditStart.TabIndex = 1;
            this.dateEditStart.ToolTip = "Эхлэх огноо";
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.DropDownControl = this.popupMenu;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(641, 11);
            this.dropDownButtonPrint.MenuManager = this.barManager;
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.barManager.SetPopupContextMenu(this.dropDownButtonPrint, this.popupMenu);
            this.dropDownButtonPrint.Size = new System.Drawing.Size(100, 23);
            this.dropDownButtonPrint.TabIndex = 7;
            this.dropDownButtonPrint.Text = "Хэвлэх";
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemWard),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemCustomize),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "Хэвлэх";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 0;
            this.barButtonItemPrint.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.LargeGlyph")));
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemWard
            // 
            this.barButtonItemWard.Caption = "Тасгийн тайлан";
            this.barButtonItemWard.Description = "Тасгийн тайлан";
            this.barButtonItemWard.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemWard.Glyph")));
            this.barButtonItemWard.Id = 1;
            this.barButtonItemWard.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemWard.LargeGlyph")));
            this.barButtonItemWard.Name = "barButtonItemWard";
            this.barButtonItemWard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemBegBal_ItemClick);
            // 
            // barButtonItemCustomize
            // 
            this.barButtonItemCustomize.Caption = "Тохиргоо";
            this.barButtonItemCustomize.Description = "Тохиргоо";
            this.barButtonItemCustomize.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCustomize.Glyph")));
            this.barButtonItemCustomize.Id = 2;
            this.barButtonItemCustomize.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCustomize.LargeGlyph")));
            this.barButtonItemCustomize.Name = "barButtonItemCustomize";
            this.barButtonItemCustomize.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCustomize_ItemClick);
            // 
            // barButtonItemConvert
            // 
            this.barButtonItemConvert.Caption = "Хөрвүүлэх";
            this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
            this.barButtonItemConvert.Id = 3;
            this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
            this.barButtonItemConvert.Name = "barButtonItemConvert";
            this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemPrint,
            this.barButtonItemWard,
            this.barButtonItemCustomize,
            this.barButtonItemConvert});
            this.barManager.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 461);
            this.barDockControlBottom.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 461);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(784, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 461);
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(493, 11);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonReload.TabIndex = 5;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Сонгосон хэрэглэгчийн үлдэгдлийн мэдээллийг шүүж харах";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // MedicineBalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MedicineBalForm";
            this.ShowInTaskbar = false;
            this.Text = "Журнал";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MedicineBalForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            this.dockPanelLeft.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLeft;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraTreeList.TreeList treeListWard;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnWard;
        private DevExpress.XtraGrid.GridControl gridControlBal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewBal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnUserRole;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnUserName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnBarcode;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnMedicineName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnLatinName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnUnit;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnBegBal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnBegSumPrice;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnTotalIncCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnTotalIncSumPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnTotalExpCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnTotalExpSumPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnEndBal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnEndSumPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnWardName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnMedicineTypeName;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.LabelControl labelControlAll;
        private DevExpress.XtraEditors.CheckEdit checkEditAll;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnShape;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnValidDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemWard;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCustomize;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEdit;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnIncCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnIncSumPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnIncMotCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnIncMotSumPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnExpCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnExpSumPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnExpMotCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnExpMotSumPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnExpPatientCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnExpPatientSumPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnIncTypeName;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnQuantity;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnSerial;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandIncCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandIncMotCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandTotalIncCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandExpCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandExpMotCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandExpPatientCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBandTotalExpCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
    }
}