﻿namespace Pharmacy2016.gui.core.journal.bal
{
    partial class MedicineBegBalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicineBegBalForm));
      this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
      this.dockPanelLeft = new DevExpress.XtraBars.Docking.DockPanel();
      this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
      this.treeListWard = new DevExpress.XtraTreeList.TreeList();
      this.treeListColumnWard = new DevExpress.XtraTreeList.Columns.TreeListColumn();
      this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
      this.gridControlBegBal = new DevExpress.XtraGrid.GridControl();
      this.gridViewBegBal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
      this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
      this.gridColumnUserName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
      this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
      this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
      this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
      this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.bandedGridColumnCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
      this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.bandedGridColumnSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
      this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
      this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
      this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
      this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
      this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
      this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
      this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
      this.barButtonItemBegBal = new DevExpress.XtraBars.BarButtonItem();
      this.barButtonItemEndBal = new DevExpress.XtraBars.BarButtonItem();
      this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
      this.barManager = new DevExpress.XtraBars.BarManager(this.components);
      this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
      this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
      this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
      this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
      this.dropDownButton1 = new DevExpress.XtraEditors.DropDownButton();
      this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
      this.expTypeTableAdapter1 = new Pharmacy2016.gui.core.info.ds.InformationDataSetTableAdapters.ExpTypeTableAdapter();
      ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
      this.dockPanelLeft.SuspendLayout();
      this.dockPanel1_Container.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
      this.panelControl1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.gridControlBegBal)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gridViewBegBal)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
      this.panelControl2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
      this.SuspendLayout();
      // 
      // dockManager
      // 
      this.dockManager.Form = this;
      this.dockManager.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLeft});
      this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
      // 
      // dockPanelLeft
      // 
      this.dockPanelLeft.Controls.Add(this.dockPanel1_Container);
      this.dockPanelLeft.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
      this.dockPanelLeft.ID = new System.Guid("32690437-4757-43e6-b1e2-053573c2cee2");
      this.dockPanelLeft.Location = new System.Drawing.Point(0, 0);
      this.dockPanelLeft.Name = "dockPanelLeft";
      this.dockPanelLeft.Options.ShowCloseButton = false;
      this.dockPanelLeft.OriginalSize = new System.Drawing.Size(200, 200);
      this.dockPanelLeft.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
      this.dockPanelLeft.SavedIndex = 0;
      this.dockPanelLeft.Size = new System.Drawing.Size(200, 461);
      this.dockPanelLeft.Text = "Тасаг";
      this.dockPanelLeft.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
      // 
      // dockPanel1_Container
      // 
      this.dockPanel1_Container.Controls.Add(this.treeListWard);
      this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
      this.dockPanel1_Container.Name = "dockPanel1_Container";
      this.dockPanel1_Container.Size = new System.Drawing.Size(192, 434);
      this.dockPanel1_Container.TabIndex = 0;
      // 
      // treeListWard
      // 
      this.treeListWard.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnWard});
      this.treeListWard.Dock = System.Windows.Forms.DockStyle.Fill;
      this.treeListWard.Location = new System.Drawing.Point(0, 0);
      this.treeListWard.Name = "treeListWard";
      this.treeListWard.BeginUnboundLoad();
      this.treeListWard.AppendNode(new object[] {
            "Хавдар судлалын үндэсний төв"}, -1);
      this.treeListWard.AppendNode(new object[] {
            "Тасаг 1"}, 0);
      this.treeListWard.AppendNode(new object[] {
            "Тасаг 2"}, 0);
      this.treeListWard.AppendNode(new object[] {
            "Тасаг 3"}, 0);
      this.treeListWard.EndUnboundLoad();
      this.treeListWard.OptionsBehavior.Editable = false;
      this.treeListWard.OptionsBehavior.ExpandNodeOnDrag = false;
      this.treeListWard.OptionsView.ShowColumns = false;
      this.treeListWard.OptionsView.ShowIndicator = false;
      this.treeListWard.Size = new System.Drawing.Size(192, 434);
      this.treeListWard.TabIndex = 0;
      // 
      // treeListColumnWard
      // 
      this.treeListColumnWard.Caption = "Тасаг";
      this.treeListColumnWard.FieldName = "ward";
      this.treeListColumnWard.MinWidth = 52;
      this.treeListColumnWard.Name = "treeListColumnWard";
      this.treeListColumnWard.OptionsColumn.ReadOnly = true;
      this.treeListColumnWard.Visible = true;
      this.treeListColumnWard.VisibleIndex = 0;
      // 
      // panelControl1
      // 
      this.panelControl1.Controls.Add(this.gridControlBegBal);
      this.panelControl1.Controls.Add(this.panelControl2);
      this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panelControl1.Location = new System.Drawing.Point(0, 0);
      this.panelControl1.Name = "panelControl1";
      this.panelControl1.Size = new System.Drawing.Size(784, 461);
      this.panelControl1.TabIndex = 0;
      // 
      // gridControlBegBal
      // 
      this.gridControlBegBal.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gridControlBegBal.EmbeddedNavigator.Buttons.Append.Visible = false;
      this.gridControlBegBal.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
      this.gridControlBegBal.EmbeddedNavigator.Buttons.Edit.Visible = false;
      this.gridControlBegBal.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
      this.gridControlBegBal.EmbeddedNavigator.Buttons.Remove.Visible = false;
      this.gridControlBegBal.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "update")});
      this.gridControlBegBal.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlBegBal_EmbeddedNavigator_ButtonClick);
      this.gridControlBegBal.Location = new System.Drawing.Point(2, 52);
      this.gridControlBegBal.MainView = this.gridViewBegBal;
      this.gridControlBegBal.Name = "gridControlBegBal";
      this.gridControlBegBal.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemDateEdit1});
      this.gridControlBegBal.Size = new System.Drawing.Size(780, 407);
      this.gridControlBegBal.TabIndex = 0;
      this.gridControlBegBal.UseEmbeddedNavigator = true;
      this.gridControlBegBal.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBegBal});
      this.gridControlBegBal.Click += new System.EventHandler(this.gridControlBegBal_Click);
      // 
      // gridViewBegBal
      // 
      this.gridViewBegBal.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand1,
            this.gridBand2,
            this.gridBand3});
      this.gridViewBegBal.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.gridColumnUserName,
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn9,
            this.gridColumn3,
            this.bandedGridColumn5,
            this.bandedGridColumn6,
            this.bandedGridColumn4,
            this.bandedGridColumn7,
            this.bandedGridColumnCount,
            this.bandedGridColumn3,
            this.bandedGridColumnSumPrice,
            this.bandedGridColumn12,
            this.bandedGridColumn13,
            this.bandedGridColumn14,
            this.bandedGridColumn8,
            this.bandedGridColumn10});
      this.gridViewBegBal.GridControl = this.gridControlBegBal;
      this.gridViewBegBal.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", this.bandedGridColumnCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", this.bandedGridColumnSumPrice, "{0:c2}")});
      this.gridViewBegBal.Name = "gridViewBegBal";
      this.gridViewBegBal.OptionsBehavior.ReadOnly = true;
      this.gridViewBegBal.OptionsNavigation.AutoFocusNewRow = true;
      this.gridViewBegBal.OptionsView.ShowAutoFilterRow = true;
      this.gridViewBegBal.OptionsView.ShowFooter = true;
      this.gridViewBegBal.OptionsView.ShowGroupPanel = false;
      this.gridViewBegBal.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridViewBegBal_CustomDrawGroupRow);
      // 
      // gridBand4
      // 
      this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
      this.gridBand4.AppearanceHeader.Options.UseFont = true;
      this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
      this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      this.gridBand4.Caption = "Нярав";
      this.gridBand4.Columns.Add(this.gridColumnUserName);
      this.gridBand4.Columns.Add(this.bandedGridColumn1);
      this.gridBand4.Columns.Add(this.bandedGridColumn2);
      this.gridBand4.CustomizationCaption = "Нярав";
      this.gridBand4.Name = "gridBand4";
      this.gridBand4.VisibleIndex = 0;
      this.gridBand4.Width = 193;
      // 
      // gridColumnUserName
      // 
      this.gridColumnUserName.Caption = "Дугаар";
      this.gridColumnUserName.CustomizationCaption = "Хэрэглэгчийн дугаар";
      this.gridColumnUserName.FieldName = "userID";
      this.gridColumnUserName.MinWidth = 75;
      this.gridColumnUserName.Name = "gridColumnUserName";
      this.gridColumnUserName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
      this.gridColumnUserName.Visible = true;
      this.gridColumnUserName.Width = 96;
      // 
      // bandedGridColumn1
      // 
      this.bandedGridColumn1.Caption = "Нэр";
      this.bandedGridColumn1.CustomizationCaption = "Хэрэглэгчийн нэр";
      this.bandedGridColumn1.FieldName = "userName";
      this.bandedGridColumn1.MinWidth = 75;
      this.bandedGridColumn1.Name = "bandedGridColumn1";
      this.bandedGridColumn1.Visible = true;
      this.bandedGridColumn1.Width = 97;
      // 
      // bandedGridColumn2
      // 
      this.bandedGridColumn2.Caption = "Эрх";
      this.bandedGridColumn2.CustomizationCaption = "Хэрэглэгчийн эрх";
      this.bandedGridColumn2.FieldName = "roleName";
      this.bandedGridColumn2.MinWidth = 75;
      this.bandedGridColumn2.Name = "bandedGridColumn2";
      // 
      // gridBand1
      // 
      this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
      this.gridBand1.AppearanceHeader.Options.UseFont = true;
      this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
      this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      this.gridBand1.Caption = "Агуулах";
      this.gridBand1.Columns.Add(this.bandedGridColumn9);
      this.gridBand1.Columns.Add(this.gridColumn3);
      this.gridBand1.CustomizationCaption = "Агуулах";
      this.gridBand1.Name = "gridBand1";
      this.gridBand1.VisibleIndex = 1;
      this.gridBand1.Width = 150;
      // 
      // bandedGridColumn9
      // 
      this.bandedGridColumn9.Caption = "Нэр";
      this.bandedGridColumn9.CustomizationCaption = "Агуулахын нэр";
      this.bandedGridColumn9.FieldName = "warehouseName";
      this.bandedGridColumn9.MinWidth = 75;
      this.bandedGridColumn9.Name = "bandedGridColumn9";
      this.bandedGridColumn9.Visible = true;
      // 
      // gridColumn3
      // 
      this.gridColumn3.Caption = "Дэлгэрэнгүй";
      this.gridColumn3.CustomizationCaption = "Агуулахын дэлгэрэнгүй";
      this.gridColumn3.FieldName = "warehouseDesc";
      this.gridColumn3.MinWidth = 75;
      this.gridColumn3.Name = "gridColumn3";
      this.gridColumn3.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
      this.gridColumn3.Visible = true;
      // 
      // gridBand2
      // 
      this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
      this.gridBand2.AppearanceHeader.Options.UseFont = true;
      this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
      this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      this.gridBand2.Caption = "Эмийн мэдээлэл";
      this.gridBand2.Columns.Add(this.bandedGridColumn5);
      this.gridBand2.Columns.Add(this.bandedGridColumn6);
      this.gridBand2.Columns.Add(this.bandedGridColumn4);
      this.gridBand2.Columns.Add(this.bandedGridColumn10);
      this.gridBand2.Columns.Add(this.bandedGridColumn12);
      this.gridBand2.Columns.Add(this.bandedGridColumn7);
      this.gridBand2.Columns.Add(this.bandedGridColumn8);
      this.gridBand2.Columns.Add(this.bandedGridColumn13);
      this.gridBand2.CustomizationCaption = "Эмийн мэдээлэл";
      this.gridBand2.Name = "gridBand2";
      this.gridBand2.VisibleIndex = 2;
      this.gridBand2.Width = 600;
      // 
      // bandedGridColumn5
      // 
      this.bandedGridColumn5.Caption = "Нэр";
      this.bandedGridColumn5.CustomizationCaption = "Худалдааны нэр";
      this.bandedGridColumn5.FieldName = "medicineName";
      this.bandedGridColumn5.MinWidth = 75;
      this.bandedGridColumn5.Name = "bandedGridColumn5";
      this.bandedGridColumn5.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
      this.bandedGridColumn5.Visible = true;
      // 
      // bandedGridColumn6
      // 
      this.bandedGridColumn6.Caption = "ОУ нэр";
      this.bandedGridColumn6.CustomizationCaption = "Олон улсын нэр";
      this.bandedGridColumn6.FieldName = "latinName";
      this.bandedGridColumn6.MinWidth = 75;
      this.bandedGridColumn6.Name = "bandedGridColumn6";
      this.bandedGridColumn6.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
      this.bandedGridColumn6.Visible = true;
      // 
      // bandedGridColumn4
      // 
      this.bandedGridColumn4.Caption = "Ангилал";
      this.bandedGridColumn4.CustomizationCaption = "Эмийн ангилал";
      this.bandedGridColumn4.FieldName = "medicineTypeName";
      this.bandedGridColumn4.MinWidth = 75;
      this.bandedGridColumn4.Name = "bandedGridColumn4";
      this.bandedGridColumn4.Visible = true;
      // 
      // bandedGridColumn10
      // 
      this.bandedGridColumn10.Caption = "Сериал";
      this.bandedGridColumn10.CustomizationCaption = "Эмийн сериал дугаар";
      this.bandedGridColumn10.FieldName = "serial";
      this.bandedGridColumn10.MinWidth = 75;
      this.bandedGridColumn10.Name = "bandedGridColumn10";
      this.bandedGridColumn10.Visible = true;
      // 
      // bandedGridColumn12
      // 
      this.bandedGridColumn12.Caption = "Хэлбэр";
      this.bandedGridColumn12.CustomizationCaption = "Хэлбэр";
      this.bandedGridColumn12.FieldName = "shape";
      this.bandedGridColumn12.MinWidth = 75;
      this.bandedGridColumn12.Name = "bandedGridColumn12";
      this.bandedGridColumn12.Visible = true;
      // 
      // bandedGridColumn7
      // 
      this.bandedGridColumn7.Caption = "Тун хэмжээ";
      this.bandedGridColumn7.CustomizationCaption = "Тун хэмжээ";
      this.bandedGridColumn7.FieldName = "unitType";
      this.bandedGridColumn7.MinWidth = 75;
      this.bandedGridColumn7.Name = "bandedGridColumn7";
      this.bandedGridColumn7.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
      this.bandedGridColumn7.Visible = true;
      // 
      // bandedGridColumn8
      // 
      this.bandedGridColumn8.Caption = "Хэмжих нэгж";
      this.bandedGridColumn8.CustomizationCaption = "Хэмжих нэгж";
      this.bandedGridColumn8.FieldName = "quantity";
      this.bandedGridColumn8.MinWidth = 75;
      this.bandedGridColumn8.Name = "bandedGridColumn8";
      this.bandedGridColumn8.Visible = true;
      // 
      // bandedGridColumn13
      // 
      this.bandedGridColumn13.Caption = "Хүчинтэй хугацаа";
      this.bandedGridColumn13.ColumnEdit = this.repositoryItemDateEdit1;
      this.bandedGridColumn13.CustomizationCaption = "Хүчинтэй хугацаа";
      this.bandedGridColumn13.FieldName = "validDate";
      this.bandedGridColumn13.MinWidth = 75;
      this.bandedGridColumn13.Name = "bandedGridColumn13";
      this.bandedGridColumn13.Visible = true;
      // 
      // repositoryItemDateEdit1
      // 
      this.repositoryItemDateEdit1.AutoHeight = false;
      this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
      this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
      this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
      this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
      this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
      this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
      // 
      // gridBand3
      // 
      this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
      this.gridBand3.AppearanceHeader.Options.UseFont = true;
      this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
      this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
      this.gridBand3.Caption = "Дүн";
      this.gridBand3.Columns.Add(this.bandedGridColumn14);
      this.gridBand3.Columns.Add(this.bandedGridColumnCount);
      this.gridBand3.Columns.Add(this.bandedGridColumn3);
      this.gridBand3.Columns.Add(this.bandedGridColumnSumPrice);
      this.gridBand3.CustomizationCaption = "Дүн";
      this.gridBand3.Name = "gridBand3";
      this.gridBand3.VisibleIndex = 3;
      this.gridBand3.Width = 300;
      // 
      // bandedGridColumn14
      // 
      this.bandedGridColumn14.Caption = "Орлогын төрөл";
      this.bandedGridColumn14.CustomizationCaption = "Орлогын төрөл";
      this.bandedGridColumn14.FieldName = "incTypeName";
      this.bandedGridColumn14.MinWidth = 75;
      this.bandedGridColumn14.Name = "bandedGridColumn14";
      this.bandedGridColumn14.Visible = true;
      // 
      // bandedGridColumnCount
      // 
      this.bandedGridColumnCount.Caption = "Тоо хэмжээ";
      this.bandedGridColumnCount.ColumnEdit = this.repositoryItemSpinEdit1;
      this.bandedGridColumnCount.CustomizationCaption = "Тоо хэмжээ";
      this.bandedGridColumnCount.FieldName = "count";
      this.bandedGridColumnCount.MinWidth = 75;
      this.bandedGridColumnCount.Name = "bandedGridColumnCount";
      this.bandedGridColumnCount.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
      this.bandedGridColumnCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
      this.bandedGridColumnCount.Visible = true;
      // 
      // repositoryItemSpinEdit1
      // 
      this.repositoryItemSpinEdit1.AutoHeight = false;
      this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
      this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
      this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
      this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
      this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
      this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
      this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
      // 
      // bandedGridColumn3
      // 
      this.bandedGridColumn3.Caption = "Нэгжийн үнэ";
      this.bandedGridColumn3.ColumnEdit = this.repositoryItemSpinEdit1;
      this.bandedGridColumn3.CustomizationCaption = "Нэгжийн үнэ";
      this.bandedGridColumn3.DisplayFormat.FormatString = "0.000";
      this.bandedGridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
      this.bandedGridColumn3.FieldName = "price";
      this.bandedGridColumn3.MinWidth = 75;
      this.bandedGridColumn3.Name = "bandedGridColumn3";
      this.bandedGridColumn3.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
      this.bandedGridColumn3.Visible = true;
      // 
      // bandedGridColumnSumPrice
      // 
      this.bandedGridColumnSumPrice.Caption = "Дүн";
      this.bandedGridColumnSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
      this.bandedGridColumnSumPrice.CustomizationCaption = "Дүн";
      this.bandedGridColumnSumPrice.DisplayFormat.FormatString = "0.000";
      this.bandedGridColumnSumPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
      this.bandedGridColumnSumPrice.FieldName = "sumPrice";
      this.bandedGridColumnSumPrice.MinWidth = 75;
      this.bandedGridColumnSumPrice.Name = "bandedGridColumnSumPrice";
      this.bandedGridColumnSumPrice.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
      this.bandedGridColumnSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
      this.bandedGridColumnSumPrice.Visible = true;
      // 
      // panelControl2
      // 
      this.panelControl2.Controls.Add(this.simpleButton1);
      this.panelControl2.Controls.Add(this.simpleButtonLayout);
      this.panelControl2.Controls.Add(this.dropDownButtonPrint);
      this.panelControl2.Controls.Add(this.dropDownButton1);
      this.panelControl2.Controls.Add(this.simpleButtonReload);
      this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panelControl2.Location = new System.Drawing.Point(2, 2);
      this.panelControl2.Name = "panelControl2";
      this.panelControl2.Size = new System.Drawing.Size(780, 50);
      this.panelControl2.TabIndex = 1;
      this.panelControl2.Paint += new System.Windows.Forms.PaintEventHandler(this.panelControl2_Paint);
      // 
      // simpleButton1
      // 
      this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
      this.simpleButton1.Location = new System.Drawing.Point(101, 10);
      this.simpleButton1.Name = "simpleButton1";
      this.simpleButton1.Size = new System.Drawing.Size(75, 23);
      this.simpleButton1.TabIndex = 6;
      this.simpleButton1.Text = "Импорт";
      this.simpleButton1.ToolTip = "Импорт хийх";
      this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
      // 
      // simpleButtonLayout
      // 
      this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
      this.simpleButtonLayout.Location = new System.Drawing.Point(747, 10);
      this.simpleButtonLayout.Name = "simpleButtonLayout";
      this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
      this.simpleButtonLayout.TabIndex = 5;
      this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
      this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
      // 
      // dropDownButtonPrint
      // 
      this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
      this.dropDownButtonPrint.DropDownControl = this.popupMenu;
      this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
      this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
      this.dropDownButtonPrint.Location = new System.Drawing.Point(641, 10);
      this.dropDownButtonPrint.MenuManager = this.barManager;
      this.dropDownButtonPrint.Name = "dropDownButtonPrint";
      this.barManager.SetPopupContextMenu(this.dropDownButtonPrint, this.popupMenu);
      this.dropDownButtonPrint.Size = new System.Drawing.Size(100, 23);
      this.dropDownButtonPrint.TabIndex = 4;
      this.dropDownButtonPrint.Text = "Хэвлэх";
      // 
      // popupMenu
      // 
      this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemBegBal),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemEndBal),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
      this.popupMenu.Manager = this.barManager;
      this.popupMenu.Name = "popupMenu";
      // 
      // barButtonItem1
      // 
      this.barButtonItem1.Caption = "Хэвлэх /Бүгд/";
      this.barButtonItem1.Glyph = global::Pharmacy2016.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
      this.barButtonItem1.Id = 6;
      this.barButtonItem1.Name = "barButtonItem1";
      this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
      // 
      // barButtonItemPrint
      // 
      this.barButtonItemPrint.Caption = "Хэвлэх /Сонгосон/";
      this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
      this.barButtonItemPrint.Id = 0;
      this.barButtonItemPrint.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.LargeGlyph")));
      this.barButtonItemPrint.Name = "barButtonItemPrint";
      this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
      // 
      // barButtonItemBegBal
      // 
      this.barButtonItemBegBal.Caption = "Эхний үлдэгдэл";
      this.barButtonItemBegBal.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemBegBal.Glyph")));
      this.barButtonItemBegBal.Id = 1;
      this.barButtonItemBegBal.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemBegBal.LargeGlyph")));
      this.barButtonItemBegBal.Name = "barButtonItemBegBal";
      this.barButtonItemBegBal.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
      this.barButtonItemBegBal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemBegBal_ItemClick);
      // 
      // barButtonItemEndBal
      // 
      this.barButtonItemEndBal.Caption = "Эцсийн үлдэгдэл";
      this.barButtonItemEndBal.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.Glyph")));
      this.barButtonItemEndBal.Id = 2;
      this.barButtonItemEndBal.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.LargeGlyph")));
      this.barButtonItemEndBal.Name = "barButtonItemEndBal";
      this.barButtonItemEndBal.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
      this.barButtonItemEndBal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEndBal_ItemClick);
      // 
      // barButtonItemConvert
      // 
      this.barButtonItemConvert.Caption = "Хөрвүүлэх";
      this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
      this.barButtonItemConvert.Id = 3;
      this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
      this.barButtonItemConvert.Name = "barButtonItemConvert";
      this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
      // 
      // barManager
      // 
      this.barManager.DockControls.Add(this.barDockControlTop);
      this.barManager.DockControls.Add(this.barDockControlBottom);
      this.barManager.DockControls.Add(this.barDockControlLeft);
      this.barManager.DockControls.Add(this.barDockControlRight);
      this.barManager.Form = this;
      this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemPrint,
            this.barButtonItemBegBal,
            this.barButtonItemEndBal,
            this.barButtonItemConvert,
            this.barButtonItem1});
      this.barManager.MaxItemId = 7;
      // 
      // barDockControlTop
      // 
      this.barDockControlTop.CausesValidation = false;
      this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
      this.barDockControlTop.Size = new System.Drawing.Size(784, 0);
      // 
      // barDockControlBottom
      // 
      this.barDockControlBottom.CausesValidation = false;
      this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.barDockControlBottom.Location = new System.Drawing.Point(0, 461);
      this.barDockControlBottom.Size = new System.Drawing.Size(784, 0);
      // 
      // barDockControlLeft
      // 
      this.barDockControlLeft.CausesValidation = false;
      this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
      this.barDockControlLeft.Size = new System.Drawing.Size(0, 461);
      // 
      // barDockControlRight
      // 
      this.barDockControlRight.CausesValidation = false;
      this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
      this.barDockControlRight.Location = new System.Drawing.Point(784, 0);
      this.barDockControlRight.Size = new System.Drawing.Size(0, 461);
      // 
      // dropDownButton1
      // 
      this.dropDownButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.dropDownButton1.Location = new System.Drawing.Point(890, 10);
      this.dropDownButton1.Name = "dropDownButton1";
      this.dropDownButton1.Size = new System.Drawing.Size(80, 25);
      this.dropDownButton1.TabIndex = 3;
      this.dropDownButton1.Text = "Хэвлэх";
      // 
      // simpleButtonReload
      // 
      this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
      this.simpleButtonReload.Location = new System.Drawing.Point(10, 10);
      this.simpleButtonReload.Name = "simpleButtonReload";
      this.simpleButtonReload.Size = new System.Drawing.Size(75, 23);
      this.simpleButtonReload.TabIndex = 1;
      this.simpleButtonReload.Text = "Сэргээх";
      this.simpleButtonReload.ToolTip = "Эхний үлдэгдлийн мэдээллийг сэргээх";
      this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
      // 
      // expTypeTableAdapter1
      // 
      this.expTypeTableAdapter1.ClearBeforeFill = true;
      // 
      // MedicineBegBalForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(784, 461);
      this.Controls.Add(this.panelControl1);
      this.Controls.Add(this.barDockControlLeft);
      this.Controls.Add(this.barDockControlRight);
      this.Controls.Add(this.barDockControlBottom);
      this.Controls.Add(this.barDockControlTop);
      this.Name = "MedicineBegBalForm";
      this.ShowInTaskbar = false;
      this.Text = "Эхний үлдэгдэл";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MedicineBegBalForm_FormClosing);
      ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
      this.dockPanelLeft.ResumeLayout(false);
      this.dockPanel1_Container.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
      this.panelControl1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.gridControlBegBal)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gridViewBegBal)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
      this.panelControl2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLeft;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraTreeList.TreeList treeListWard;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnWard;
        private DevExpress.XtraGrid.GridControl gridControlBegBal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewBegBal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnUserName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnSumPrice;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.DropDownButton dropDownButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemBegBal;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEndBal;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private info.ds.InformationDataSetTableAdapters.ExpTypeTableAdapter expTypeTableAdapter1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}