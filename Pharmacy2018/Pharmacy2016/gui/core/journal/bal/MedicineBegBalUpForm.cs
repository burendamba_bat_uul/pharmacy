﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.medicine;
using System.Data.SqlClient;



namespace Pharmacy2016.gui.core.journal.bal
{
    /*
     * Эхний үлдэгдэл засах цонх.
     * 
     */

    public partial class MedicineBegBalUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineBegBalUpForm INSTANCE = new MedicineBegBalUpForm();

            public static MedicineBegBalUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType = 1;
        
            private DataRowView currView;

            private MedicineBegBalUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                initBinding();
                reload(false);
            }

            private void initDataSource()
            {
                //gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
                gridLookUpEditUser.Properties.DataSource = JournalDataSet.Instance.SystemUserBegBal;
                treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
                treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
                gridLookUpEditIncType.Properties.DataSource = InformationDataSet.IncTypeOtherBindingSource;
                gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineOtherBindingSource;
            }

            private void initBinding()
            {
                gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.BegBalBindingSource, "userID", true));
                radioGroupType.DataBindings.Add(new Binding("EditValue", JournalDataSet.BegBalBindingSource, "type", true));
                treeListLookUpEditWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.BegBalBindingSource, "warehouseID", true));
                treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.BegBalBindingSource, "wardID", true));
                gridLookUpEditIncType.DataBindings.Add(new Binding("EditValue", JournalDataSet.BegBalBindingSource, "incTypeID", true));
                gridLookUpEditMedicine.DataBindings.Add(new Binding("EditValue", JournalDataSet.BegBalBindingSource, "medicineID", true));
                //spinEditPrice.DataBindings.Add(new Binding("EditValue", JournalDataSet.BegBalBindingSource, "price", true));
                //spinEditCount.DataBindings.Add(new Binding("EditValue", JournalDataSet.BegBalBindingSource, "count", true));
                //spinEditSumPrice.DataBindings.Add(new Binding("EditValue", JournalDataSet.BegBalBindingSource, "sumPrice", true));
            }

            private void initError()
            {
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditIncType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditPrice.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insert(null, 0, null, null, 1);
                    }
                    else if (actionType == 2)
                    {
                        update();
                    }

                    changeType();
                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insert(object userID, int type, object warehouseID, object wardID, object incTypeID)
            {
                JournalDataSet.Instance.BegBal.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                currView = (DataRowView)JournalDataSet.BegBalBindingSource.AddNew();
                currView["userID"] = userID;
                currView["type"] = type;
                currView["incTypeID"] = incTypeID;

                gridLookUpEditUser.EditValue = userID;
                radioGroupType.SelectedIndex = type;
                changeType();
                treeListLookUpEditWarehouse.EditValue = warehouseID;
                treeListLookUpEditWard.EditValue = wardID;
                currView["warehouseID"] = warehouseID;
                currView["wardID"] = wardID;
                
                gridLookUpEditIncType.EditValue = incTypeID;
                clearMedicine();
            }

            private void update()
            {
                currView = (DataRowView)JournalDataSet.BegBalBindingSource.Current;
                spinEditPrice.EditValue = currView["price"];
                spinEditCount.EditValue = currView["count"];
            }

            private void MedicineBegBalInUpForm_Shown(object sender, EventArgs e)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                {
                    gridLookUpEditUser.EditValue = UserUtil.getUserId();
                    currView["userID"] = UserUtil.getUserId();
                    //currView.EndEdit();
                    gridLookUpEditUser.Properties.ReadOnly = true;
                    treeListLookUpEditWarehouse.Focus();
                }
                else
                {
                    gridLookUpEditUser.Properties.ReadOnly = false;
                    gridLookUpEditUser.Focus();
                }
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }
        
            private void clearData()
            {
                clearErrorText();
                clearMedicine();

                InformationDataSet.SystemUserBindingSource.Filter = "";
                JournalDataSet.WarehouseOtherBindingSource.Filter = "";
                JournalDataSet.WardOtherBindingSource.Filter = "";

                JournalDataSet.BegBalBindingSource.CancelEdit();
                JournalDataSet.Instance.BegBal.RejectChanges();
            }

            private void clearMedicine()
            {
                gridLookUpEditMedicine.EditValue = null;
                spinEditCount.Value = 0;
                spinEditPrice.Value = 0;
                spinEditSumPrice.Value = 0;
            }

            private void clearErrorText()
            {
                gridLookUpEditUser.ErrorText = "";
                treeListLookUpEditWarehouse.ErrorText = "";
                treeListLookUpEditWard.ErrorText = "";
                gridLookUpEditIncType.ErrorText = "";
                gridLookUpEditMedicine.ErrorText = "";
                spinEditCount.ErrorText = "";
                spinEditPrice.ErrorText = "";
            }

        #endregion
        
        #region Формын event

            private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
            {
                if (Visible && gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null)
                {
                    JournalDataSet.WarehouseOtherBindingSource.Filter = UserUtil.getUserWarehouse(gridLookUpEditUser.EditValue.ToString());
                    JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(gridLookUpEditUser.EditValue.ToString());
                }
            }

            private void gridLookUpEditUser_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditUser.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        if (radioGroupType.SelectedIndex == 0)
                            treeListLookUpEditWarehouse.Focus();
                        else
                            treeListLookUpEditWard.Focus();
                    }
                }
            }

            private void treeListLookUpEditWarehouse_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditWarehouse.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditIncType.Focus();
                    }
                }
            }

            private void treeListLookUpEditWard_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditWard.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditIncType.Focus();
                    }
                }
            }

            private void gridLookUpEditIncType_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditIncType.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditMedicine.Focus();
                    }
                }
            }

            private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeType();
            }


            private void gridLookUpEditMedicine_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {   
                    if (ProgramUtil.IsShowPopup)
                    {
                        if (!ProgramUtil.IsSaveEvent)
                        {
                            ProgramUtil.IsShowPopup = false;
                            gridLookUpEditMedicine.ShowPopup();
                        }
                        else
                        {
                            ProgramUtil.IsSaveEvent = false;
                        }   
                    }
                    else
                    {
                        spinEditCount.Focus();
                    }
                }
            }

            private void gridLookUpEditMedicine_EditValueChanged(object sender, EventArgs e)
            {
                DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                if (view != null)
                {
                    spinEditPrice.EditValue = view["price"];
                    labelControlQuantity.Text = view["quantity"].ToString();
                    serial.Text = view["serial"].ToString();
                    dateEditValidDate.Text = view["validDate"].ToString();
                }
            }

            private void gridLookUpEditMedicine_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    MedicineInUpForm.Instance.showForm(3, Instance);
                    Activate();
                }   
            }


            private void spinEditCount_ValueChanged(object sender, EventArgs e)
            {
                spinEditSumPrice.Value = spinEditCount.Value * spinEditPrice.Value;
            }


            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload(true);
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private void changeType()
            {
                if (radioGroupType.SelectedIndex == 0)
                {
                    labelControlWarehouse.Location = new Point(77, 94);
                    treeListLookUpEditWard.Location = new Point(144, 12);
                    treeListLookUpEditWarehouse.Location = new Point(144, 91);

                    treeListLookUpEditWard.Visible = false;
                    treeListLookUpEditWarehouse.Visible = true;
                    labelControlWarehouse.Text = "Эмийн сан*:";
                }
                else if (radioGroupType.SelectedIndex == 1)
                {
                    labelControlWarehouse.Location = new Point(100, 94);
                    treeListLookUpEditWard.Location = new Point(144, 91);
                    treeListLookUpEditWarehouse.Location = new Point(144, 12);

                    treeListLookUpEditWard.Visible = true;
                    treeListLookUpEditWarehouse.Visible = false;
                    labelControlWarehouse.Text = "Тасаг*:";
                }
            }

            private void reload(bool isReload)
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                //InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                if (isReload || JournalDataSet.Instance.SystemUserBegBal.Rows.Count == 0)
                    JournalDataSet.SystemUserBegBalTableAdapter.Fill(JournalDataSet.Instance.SystemUserBegBal, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                    JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                    JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
                if (isReload || InformationDataSet.Instance.IncTypeOther.Rows.Count == 0)
                    InformationDataSet.IncTypeOtherTableAdapter.Fill(InformationDataSet.Instance.IncTypeOther, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.MedicineOther.Rows.Count == 0)
                    JournalDataSet.MedicineOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOther, HospitalUtil.getHospitalId(), "");
                //ProgramUtil.closeWaitDialog();
            }

            private bool check()
            {
                //Instance.Validate();
                bool isRight = true;
                string text = "", errorText;
                clearErrorText();
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.Text.Equals(""))
                {
                    errorText = "Няравыг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
                if (gridLookUpEditIncType.EditValue == DBNull.Value || gridLookUpEditIncType.EditValue == null || gridLookUpEditIncType.Text.Equals(""))
                {
                    errorText = "Орлогын төрлийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditIncType.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditIncType.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.Text.Equals("")))
                {
                    errorText = "Агуулахыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWarehouse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWarehouse.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals("")))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }
                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.Text.Equals(""))
                {
                    errorText = "Эмийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                    }
                }
                if (serial.EditValue == DBNull.Value || serial.EditValue == null || serial.Text.Equals(""))
                {
                    errorText = "Эмийн сериалийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    serial.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                    }
                }
                if (dateEditValidDate.EditValue == DBNull.Value || dateEditValidDate.EditValue == null || dateEditValidDate.Text.Equals(""))
                {
                    errorText = "Хүчинтэй хугацааг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditValidDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                    }
                }
                if (spinEditPrice.Value == 0)
                {
                    errorText = "Үнийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditPrice.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditPrice.Focus();
                    }
                }
                if (spinEditCount.Value == 0)
                {
                    errorText = "Тоо, хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditCount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditCount.Focus();
                    }
                }
                if(isRight)
                {
                    bool exist = Convert.ToBoolean(JournalDataSet.QueriesTableAdapter.exist_beg_bal(HospitalUtil.getHospitalId(), currView["id"].ToString(), gridLookUpEditUser.EditValue.ToString(), radioGroupType.SelectedIndex,
                                                                            radioGroupType.SelectedIndex == 0 ? treeListLookUpEditWarehouse.EditValue.ToString() : null, radioGroupType.SelectedIndex == 1 ? treeListLookUpEditWard.EditValue.ToString() : null,
                                                                            Convert.ToInt32(gridLookUpEditIncType.EditValue), gridLookUpEditMedicine.EditValue.ToString(), Convert.ToDouble(spinEditPrice.Value), currView.IsNew));
                    if (exist)
                    {
                        errorText = "Хэрэглэгч, эмийн сан, орлогын төрөл, эм, үнэ дээр эхний үлдэгдэл оруулсан байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditMedicine.ErrorText = errorText;
                        spinEditPrice.ErrorText = errorText;
                        gridLookUpEditMedicine.Focus();
                        isRight = false;
                    }
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);

                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                    DataRowView medicine = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();

                    currView["userName"] = user["fullName"];
                    currView["roleName"] = user["roleName"];
                    if (radioGroupType.SelectedIndex == 0)
                    {
                        currView["wardID"] = null;
                        currView["warehouseName"] = warehouse["name"];
                        currView["warehouseDesc"] = warehouse["description"];
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {
                        currView["warehouseID"] = null;
                        currView["warehouseName"] = ward["name"];
                        currView["warehouseDesc"] = ward["description"];
                    }


                    //Эхний үлдэгдлээс эм бүртгэх
                    string newID = "";
                    if (!ProgramUtil.checkHaveMedicine(gridLookUpEditMedicine.EditValue.ToString(), spinEditPrice.EditValue.ToString(), dateEditValidDate.EditValue.ToString(), serial.Text))
                    {
                        SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                        myConn.Open();
                        string query = "SELECT * FROM [Medicine] WHERE [id] = '" + gridLookUpEditMedicine.EditValue.ToString() + "'";
                        SqlCommand oCmd = new SqlCommand(query, myConn);
                        using (SqlDataReader oReader = oCmd.ExecuteReader())
                        {
                            while (oReader.Read())
                            {
                                if (oReader.HasRows)
                                {
                                    UserDataSet.Instance.Medicine.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                                    UserDataSet.Instance.Medicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                                    newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                                    UserDataSet.Instance.Medicine.idColumn.DefaultValue = newID;
                                    DataRowView view = (DataRowView)UserDataSet.MedicineBindingSource.AddNew();
                                    view["name"] = oReader["name"].ToString();
                                    view["latinName"] = oReader["latinName"].ToString();
                                    view["barcode"] = "";
                                    view["unitID"] = oReader["unitID"].ToString();
                                    view["shapeID"] = oReader["shapeID"].ToString();
                                    view["quantityID"] = oReader["quantityID"].ToString();
                                    view["medicineSeries"] = "";
                                    view["serial"] = serial.EditValue;
                                    view["description"] = "";
                                    view["packageId"] = oReader["packageId"].ToString();
                                    view["medicineTypeId"] = oReader["medicineTypeId"].ToString();
                                    view["medicineTypeInterId"] = oReader["medicineTypeInterId"].ToString();
                                    view["producerCountryID"] = oReader["producerCountryID"].ToString();
                                    view["producerID"] = oReader["producerID"].ToString();
                                    view["registerID"] = oReader["registerID"].ToString();
                                    view["btkusID"] = oReader["btkusID"].ToString();
                                    view["validDate"] = dateEditValidDate.EditValue;
                                    view["price"] = spinEditPrice.EditValue;

                                    view["PackageMedicineName"] = "x";
                                    view["medicineTypeName"] = "x";
                                    view["medicineTypeInterName"] = "x";
                                    view["shapeName"] = "x";
                                    view["unitName"] = "x";
                                    view["producerCountryName"] = "x";
                                    view["producerName"] = "x";
                                    view["quantityName"] = "x";
                                    view["parmaStandart"] = "x";
                                    view["alterName"] = "x";
                                    view["soloName"] = "x";
                                    view.EndEdit();
                                    UserDataSet.MedicineTableAdapter.Update(UserDataSet.Instance.Medicine);
                                }
                            }
                        }
                        myConn.Close();
                        currView["medicineID"] = newID;
                    }
                    else
                    {
                        currView["medicineID"] = gridLookUpEditMedicine.EditValue;
                    }

                    currView["incTypeName"] = gridLookUpEditIncType.Text;
                    currView["medicineName"] = medicine["name"];
                    currView["latinName"] = medicine["latinName"];
                    currView["medicineTypeName"] = medicine["medicineTypeName"];
                    currView["unitType"] = medicine["unitType"];
                    currView["shape"] = medicine["shape"];
                    currView["validDate"] = medicine["validDate"];
                    currView["quantity"] = medicine["quantity"];
                    currView["serial"] = medicine["serial"];
                    currView["price"] = spinEditPrice.EditValue;
                    currView["count"] = spinEditCount.EditValue;

                    currView.EndEdit();
                    JournalDataSet.BegBalTableAdapter.Update(JournalDataSet.Instance.BegBal);

                    ProgramUtil.closeWaitDialog();

                    insert(currView["userID"], radioGroupType.SelectedIndex, currView["warehouseID"], currView["wardID"], currView["incTypeID"]);
                    ProgramUtil.IsSaveEvent = true;
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditMedicine.Focus();
                }
            }

            // Шинээр нэмсэн эмийг авч байна
            public void addMedicine(DataRowView view)
            {
                DataRowView row = (DataRowView)JournalDataSet.MedicineOtherBindingSource.AddNew();
                row["id"] = view["id"];
                row["name"] = view["name"];
                row["latinName"] = view["latinName"];
                row["unitType"] = view["unitName"];
                row["price"] = view["price"];
                row["medicineTypeName"] = view["medicineTypeName"];
                row["barcode"] = view["barcode"];
                row["shape"] = view["shapeName"];
                row["validDate"] = view["validDate"];
                row["serial"] = view["serial"];
                row["quantity"] = view["quantityName"];
                row.EndEdit();
                gridLookUpEditMedicine.EditValue = view["id"];
            }

        #endregion

            private void simpleButtonCancel_Click(object sender, EventArgs e) {

            }

    }
}