﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.patient;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList;
using Pharmacy2016.gui.report.journal.patient;
using Pharmacy2016.report.journal.bal;

namespace Pharmacy2016.gui.core.journal.bal
{
    public partial class WardReportForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static WardReportForm INSTANCE = new WardReportForm();

            public static WardReportForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private WardReportForm()
            {

            }

            private void initForm(DateTime start, DateTime end)
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                treeListLookUpEditWard.Properties.TreeList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(treeListWard_AfterCheckNode);

                dateEditStart.DateTime = start;
                dateEditEnd.DateTime = end;

                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.Properties.DataSource = UserDataSet.WardBindingSource;
                reload();
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(XtraForm form, DateTime start, DateTime end)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm(start, end);
                    }

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх ачааллахад гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                treeListLookUpEditWard.ErrorText = "";
            }

            private void UserConfInUpForm_Shown(object sender, EventArgs e)
            {
                radioGroupType.Focus();
            }

        #endregion

        #region Формын event

            private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeType();
            }

            private void treeListWard_AfterCheckNode(object sender, NodeEventArgs e)
            {
                if (e.Node.HasChildren)
                {   
                    foreach (TreeListNode node in e.Node.Nodes)
                        node.Checked = e.Node.Checked;
                }
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                print();
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

        #endregion

        #region Формын функц

            private void reload()
            {
                UserDataSet.WardTableAdapter.Fill(UserDataSet.Instance.Ward, HospitalUtil.getHospitalId());
            }

            private void changeType()
            {
                if (radioGroupType.SelectedIndex == 0)
                {
                    treeListLookUpEditWard.Properties.TreeList.OptionsView.ShowCheckBoxes = true;
                }
                else if (radioGroupType.SelectedIndex == 1)
                {
                    treeListLookUpEditWard.Properties.TreeList.OptionsView.ShowCheckBoxes = false;
                }
                else if (radioGroupType.SelectedIndex == 2)
                {
                    treeListLookUpEditWard.Properties.TreeList.OptionsView.ShowCheckBoxes = true;
                }
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditStart.Focus();
                    }
                }
                if ((radioGroupType.SelectedIndex == 0 || radioGroupType.SelectedIndex == 2) && treeListLookUpEditWard.Properties.TreeList.GetAllCheckedNodes().Count == 0)
                {
                    errorText = "Тасгийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.EditValue.ToString().Equals("")))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void print()
            {
                if (check())
                {
                    if (radioGroupType.SelectedIndex == 0)
                    {
                        string wardID = "";
                        foreach (TreeListNode node in treeListLookUpEditWard.Properties.TreeList.GetAllCheckedNodes())
                            wardID += node.GetValue("id") + "~";
                        if (!wardID.Equals(""))
                            wardID = wardID.Substring(0, wardID.Length - 1);

                        MedicineBalWardReport.Instance.initAndShow(1, dateEditStart.Text, dateEditEnd.Text, wardID, "");
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {
                        PatientWardReport.Instance.initAndShow(dateEditStart.Text, dateEditEnd.Text, treeListLookUpEditWard.EditValue.ToString());
                        //PatientWardForm.Instance.showForm(dateEditStart.Text, dateEditEnd.Text, treeListLookUpEditWard.EditValue.ToString());
                    }
                    else if (radioGroupType.SelectedIndex == 2)
                    {
                        string wardID = "";
                        foreach (TreeListNode node in treeListLookUpEditWard.Properties.TreeList.GetAllCheckedNodes())
                            wardID += node.GetValue("id") + "~";
                        if (!wardID.Equals(""))
                            wardID = wardID.Substring(0, wardID.Length - 1);

                        MedicineBalWardSumReport.Instance.initAndShow(dateEditStart.Text, dateEditEnd.Text, wardID);
                    }
                }
            }

        #endregion

    }
}