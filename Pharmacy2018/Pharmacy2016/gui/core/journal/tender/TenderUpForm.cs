﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using DevExpress.XtraGrid.Columns;
using Pharmacy2016.gui.core.info.medicine;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.gui.core.info.conf;

namespace Pharmacy2016.gui.core.journal.tender
{
    public partial class TenderUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static TenderUpForm INSTANCE = new TenderUpForm();

            public static TenderUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            public DataRowView PlanView;

            private DataRowView DetialView;

            private TenderUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                dateEditDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                initError();   
                initDataSource();
                initBinding();
                reload(true);  
            }

            private void initDataSource()
            {
                gridControlDetail.DataSource = PlanDataSet.Instance.TenderDetailOther;
                gridLookUpEditPackage.Properties.DataSource = UserDataSet.PackageBindingSource;
                gridLookUpEditProducer.Properties.DataSource = UserDataSet.SupplierBindingSource;
            }

            private void initError()
            {
                gridLookUpEditPackage.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditProducer.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditTokenNo.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditTotal.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

            private void initBinding()
            {
                textEditTokenNo.DataBindings.Add(new Binding("EditValue", PlanDataSet.TenderJournalBindingSource, "tenderNumber", true));
                dateEditDate.DataBindings.Add(new Binding("EditValue", PlanDataSet.TenderJournalBindingSource, "date", true));
                //gridLookUpEditPackage.DataBindings.Add(new Binding("EditValue", PlanDataSet.PlanJournalBindingSource, "packageID", true));
                gridLookUpEditProducer.DataBindings.Add(new Binding("EditValue", PlanDataSet.TenderJournalBindingSource, "supplierID", true));
                //checkEditDivide.DataBindings.Add(new Binding("EditValue", PlanDataSet.PlanJournalBindingSource, "isDivide", true));
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    
                    actionType = type;
                    if (actionType == 1)
                    {
                        insert();
                    }
                    else if (actionType == 2)
                    {
                        update();
                    }
                    else if (actionType == 3)
                    {
                        insertNewRow();
                    }

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insert()
            {
                gridLookUpEditProducer.Properties.ReadOnly = false;
                gridLookUpEditPackage.Properties.ReadOnly = false;
                clearAndNewRow();
                DateTime now = DateTime.Now;
                PlanDataSet.Instance.TenderJournal.dateColumn.DefaultValue = now;
                dateEditDate.DateTime = now;
                //gridLookUpEditPackage.Properties.ReadOnly = false;
                PlanDataSet.Instance.TenderJournal.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                PlanView = (DataRowView)PlanDataSet.TenderJournalBindingSource.AddNew();
            }

            private void update()
            {
                gridLookUpEditProducer.Properties.ReadOnly = true;
                gridLookUpEditPackage.Properties.ReadOnly = true;
                PlanView = (DataRowView)PlanDataSet.TenderJournalBindingSource.Current;
                DetialView = TenderForm.Instance.getSelectedRow();
                if (DetialView == null)
                    return;
                gridLookUpEditPackage.EditValue = DetialView["packageID"];
                spinEditTotal.EditValue = DetialView["packagePrice"];
                spinEditCount.EditValue = DetialView["packageCount"];

                //PlanDataSet.PlanDetialOtherBindingSource.Filter = "packageID = '" + DetialView["packageID"] + "'"; 
                DataRow[] rows = PlanDataSet.Instance.TenderDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND packageID = '" + DetialView["packageID"] + "'  AND tenderJournalID = '" + PlanView["id"] +"'");
                PlanDataSet.Instance.TenderDetailOther.Rows.Clear();
                for (int i = 0; i < rows.Length; i++)
                {
                    DataRow row = PlanDataSet.Instance.TenderDetailOther.Rows.Add();
                    row["medicineID"] = rows[i]["medicineID"];
                    row["medicineName"] = rows[i]["medicineName"];
                    row["latinName"] = rows[i]["latinName"];
                    row["unit"] = rows[i]["unit"];
                    row["shape"] = rows[i]["shape"];
                    row["quantity"] = rows[i]["quantity"];
                    row["validDate"] = rows[i]["validDate"];
                    row["medicinePrice"] = rows[i]["medicinePrice"];
                    row["medicineCount"] = rows[i]["medicineCount"];
                    row.EndEdit();
                }
                gridViewDetail.FocusedRowHandle = 0;
            }

            private void insertNewRow()
            {
                gridLookUpEditProducer.Properties.ReadOnly = true;
                gridLookUpEditPackage.Properties.ReadOnly = false;
                clearAndNewRow();
                PlanView = (DataRowView)PlanDataSet.TenderJournalBindingSource.Current;            
            }

            private void clearData()
            {
                clearErrorText();
                //clearAndNewRow();
                PlanDataSet.Instance.TenderDetailOther.Rows.Clear();
                PlanDataSet.TenderJournalBindingSource.CancelEdit();
                PlanDataSet.Instance.TenderJournal.RejectChanges();
                PlanDataSet.TenderDetialOtherBindingSource.CancelEdit();
                PlanDataSet.Instance.TenderDetail.RejectChanges();
            }

            private void clearErrorText()
            {
                textEditTokenNo.ErrorText = "";
                gridLookUpEditProducer.ErrorText = "";
                gridLookUpEditPackage.ErrorText = "";
                spinEditTotal.ErrorText = "";
                spinEditCount.ErrorText = "";
            }

            private void PlanUpForm_Shown(object sender, EventArgs e)
            {
                textEditTokenNo.Focus();
                //checkChanged();
            }

            private void clearAndNewRow() 
            {
                PlanDataSet.Instance.TenderDetailOther.Rows.Clear();
                gridLookUpEditPackage.Text = "";
                clearErrorText();
                spinEditTotal.Value = 0;
                spinEditCount.Value = 0;

            }

        #endregion

        #region Формын event

            private void dateEditDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                         ProgramUtil.IsShowPopup = false;
                        dateEditDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditProducer.Focus();
                    }
                }
            }

            private void gridLookUpEditProducer_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditProducer.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditPackage.Focus();
                    }
                }
            }

            private void gridLookUpEditPackage_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditPackage.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        spinEditCount.Focus();
                    }
                }
            }

            private void gridViewDetail_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridViewDetail_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
            {
                //if (gridViewDetail.Columns != null)
                //{
                //    spinEditTotal.EditValue = gridViewDetail.Columns["sumPrice"].SummaryItem.SummaryValue;
                //    spinEditCount.EditValue = gridViewDetail.Columns["count"].SummaryItem.SummaryValue;
                //}
            }

            private void gridLookUpEditPackage_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditPackage.EditValue != null || gridLookUpEditPackage.EditValue != DBNull.Value || !gridLookUpEditPackage.EditValue.ToString().Equals(""))
                    spinEditCount.Focus(); 
            }
          
            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload(true);
            }
            
            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

            private void gridControlDetail_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "delete"))
                    {
                        delete();
                    }
                }
            }

            

            private void gridLookUpEditProducer_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add") && actionType != 2 && actionType != 3)
                {
                    SupplierInForm.Instance.showForm(4, Instance);
                }
            }

            private void gridLookUpEditPackage_Validating(object sender, CancelEventArgs e)
            {
                if (Visible)
                    setPackDetial();
            }

            private void gridLookUpEditProducer_EditValueChanged(object sender, EventArgs e)
            {
                //if (gridLookUpEditProducer.EditValue != null && gridLookUpEditProducer.EditValue != DBNull.Value)
                //    gridLookUpEditPackage.Properties.View.ActiveFilterString = "supplierID = " + gridLookUpEditProducer.EditValue;
            }

        #endregion

        #region Формын функц

            private void reload(bool isReload)
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                if (isReload && UserDataSet.Instance.Package.Rows.Count == 0)
                    UserDataSet.PackageTableAdapter.Fill(UserDataSet.Instance.Package, HospitalUtil.getHospitalId());
                PlanDataSet.PackageOtherTableAdapter.Fill(PlanDataSet.Instance.PackageOther, HospitalUtil.getHospitalId());
                if (isReload && UserDataSet.Instance.PackageDetial.Rows.Count == 0)
                    UserDataSet.PackageDetialTableAdapter.Fill(UserDataSet.Instance.PackageDetial, HospitalUtil.getHospitalId());
                PlanDataSet.PackageDetialOtherTableAdapter.Fill(PlanDataSet.Instance.PackageDetialOther, HospitalUtil.getHospitalId());
                if (isReload && UserDataSet.Instance.Supplier.Rows.Count == 0)
                    UserDataSet.SupplierTableAdapter.Fill(UserDataSet.Instance.Supplier, HospitalUtil.getHospitalId());
                ProgramUtil.closeWaitDialog();
            }
         
            private void setPackDetial()
            {
                if (!gridLookUpEditPackage.Text.ToString().Equals("") && checkBeforeAdd())
                {
                    //PlanDataSet.Instance.TenderDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id();
                    //PlanDataSet.Instance.TenderDetail.tenderJournalIDColumn.DefaultValue = PlanView["id"];
                    //PlanDetial = (DataRowView)PlanDataSet.TenderDetialOtherBindingSource.AddNew();
                    //PlanDetial["packageID"] = gridLookUpEditPackage.EditValue;
                    //PlanDetial["packageName"] = gridLookUpEditPackage.Text;
                    //PlanDetial.EndEdit();
   
                    DataRow[] rows = UserDataSet.Instance.PackageDetial.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND packageID = '" + gridLookUpEditPackage.EditValue + "'");
                    PlanDataSet.Instance.TenderDetailOther.Rows.Clear();                  
                    for (int i = 0; i < rows.Length; i++)
                    {
                        DataRow row = PlanDataSet.Instance.TenderDetailOther.Rows.Add(); 
                        row["packageID"] = gridLookUpEditPackage.EditValue;
                        row["packageName"] = gridLookUpEditPackage.Text;
                        row["packageCount"] = spinEditCount.EditValue;
                        row["packagePrice"] = spinEditTotal.EditValue;
                        row["medicineID"] = rows[i]["medicineID"];
                        row["medicineName"] = rows[i]["medicineName"];
                        row["unit"] = rows[i]["unit"];
                        row["shape"] = rows[i]["shape"];
                        row["quantity"] = rows[i]["quantity"];               
                        row["validDate"] = rows[i]["validDate"];
                        row["medicinePrice"] = rows[i]["price"];
                        row["medicineCount"] = rows[i]["count"];
                        row.EndEdit();
                    }
                    spinEditTotal.EditValue = Convert.ToDouble(gridViewDetail.Columns["medicineSumPrice"].SummaryItem.SummaryValue)*Convert.ToDouble(spinEditCount.EditValue);
                    //spinEditCount.EditValue = gridViewDetail.Columns["medicineCount"].SummaryItem.SummaryValue;
                }         
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (textEditTokenNo.EditValue == DBNull.Value || textEditTokenNo.EditValue == null || textEditTokenNo.EditValue.ToString().Equals(""))
                {
                    errorText = "Гэрээний дугаар оруулна уу!";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditTokenNo.ErrorText = errorText;
                    isRight = false;
                    textEditTokenNo.Focus();
                }
                if (gridLookUpEditPackage.EditValue == DBNull.Value || gridLookUpEditPackage.EditValue == null || gridLookUpEditPackage.EditValue.ToString().Equals(""))
                {
                    errorText = "Багц сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditPackage.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditPackage.Focus();
                    }
                }
                if (gridLookUpEditProducer.EditValue == DBNull.Value || gridLookUpEditProducer.EditValue == null || gridLookUpEditProducer.EditValue.ToString().Equals(""))
                {
                    errorText = "Тендерт оролцогцчийн нэрийг оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditProducer.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditProducer.Focus();
                    }
                }
                if (spinEditTotal.Value == 0)
                {
                    errorText = "Та багцын нийт дүнг оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditTotal.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditTotal.Focus();
                    }
                }
                if (spinEditCount.Value == 0)
                {
                    errorText = "Та багцын тоог оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditCount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditCount.Focus();
                    }
                }
                if (isRight && PlanView != null && actionType != 2)
                {
                    List<string> medicine = gridRows();
                    List<DataRow> view = TenderForm.Instance.viewRows();
                    if (medicine.Count > 0)
                    {
                        for (int i = 0; view != null && i < view.Count; i++)
                        {
                            if (view[i] != null)
                            {
                                for (int j = 0; medicine != null && j < medicine.Count; j++)
                                {
                                    if (medicine[j].Equals(view[i]["medicineID"].ToString()) && view[i] != null && medicine[j] != null)
                                    {
                                        errorText = "Тендерийн эм давхацсан байна";
                                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                                        gridLookUpEditPackage.ErrorText = errorText;
                                        isRight = false;
                                        //gridLookUpEditPackage.Focus();
                                    }
                                }
                            }                    
                        }
                    }
                    
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private List<string> gridRows()
            {
                List<string> rows = new List<string>();
                for (int i = 0; i < gridViewDetail.RowCount; i++)
                {
                    rows.Add(gridViewDetail.GetDataRow(i)["medicineID"].ToString());
                }
                return rows;
            }

            private bool checkBeforeAdd()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (isRight && PlanView != null && actionType != 2 && gridLookUpEditPackage.EditValue != null)
                {
                    DataRow[] rows = PlanDataSet.Instance.TenderDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND tenderJournalID = '" + PlanView["id"] + " 'AND packageID = '" + gridLookUpEditPackage.EditValue + "'");
                    for (int i = 0; rows != null && i < rows.Length; i++)
                    {
                        if (gridLookUpEditPackage.EditValue.ToString().Equals(rows[i]["packageID"].ToString()) && rows[i]["packageID"] != null)
                        {
                            errorText = "Багц давхацсан байна";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            gridLookUpEditPackage.ErrorText = errorText;
                            isRight = false;
                            gridLookUpEditPackage.Focus();
                        }
                    }
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                try
                {
                    if (check())
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);                
                        for (int i = 0; i < gridViewDetail.RowCount; i++)
                        {
                            DataRow row = gridViewDetail.GetDataRow(i); 
                            if (actionType == 1 || actionType == 3)
                            {
                                addNewRow(row);
                            }
                            else if (actionType == 2)
                            {
                                DataRow[] rows = PlanDataSet.Instance.TenderDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND tenderJournalID = '" + PlanView["id"] + "' AND packageID = '" + gridLookUpEditPackage.EditValue + "'");
                                for (int j = 0; rows != null && j < rows.Length; j++)
                                {
                                    rows[j]["packageID"] = gridLookUpEditPackage.EditValue;                
                                    rows[j]["packageCount"] = spinEditCount.EditValue;
                                    rows[j]["packagePrice"] = spinEditTotal.EditValue;
                                    rows[j].EndEdit();
                                    //rows[0]["medicineID"] = row["medicineID"];
                                    //rows[0]["medicineName"] = row["medicineName"];
                                    //rows[0]["latinName"] = row["latinName"];
                                    //rows[0]["unit"] = row["unit"];
                                    //rows[0]["shape"] = row["shape"];
                                    //rows[0]["validDate"] = row["validDate"];
                                    //rows[0]["quantity"] = row["quantity"];
                                    //rows[0]["medicinePrice"] = row["medicinePrice"];
                                    //rows[0]["medicineCount"] = row["medicineCount"];
                                    //rows[0].EndEdit();
                                }
                            }         
                        }

                        PlanView["tenderNumber"] = textEditTokenNo.EditValue;
                        PlanView["date"] = dateEditDate.EditValue;
                        PlanView["supplierID"] = gridLookUpEditProducer.EditValue;
                        PlanView["supplierName"] = gridLookUpEditProducer.Text;
                        PlanView.EndEdit();

                        //PlanDataSet.PlanMonthlyTableAdapter.Update(PlanDataSet.Instance.PlanMonthly);


                        //PlanDataSet.PlanDetialBindingSource.EndEdit();
                        PlanDataSet.TenderDetailTableAdapter.Update(PlanDataSet.Instance.TenderDetail);

                        PlanDataSet.TenderJournalBindingSource.EndEdit();
                        PlanDataSet.TenderJournalTableAdapter.Update(PlanDataSet.Instance.TenderJournal);

                        

                        ProgramUtil.closeWaitDialog();
                        actionType = 3;
                        insertNewRow();
                    }
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                }
            }

            public void addProducer(string id)
            {
                gridLookUpEditProducer.EditValue = id;
            }

            public string getEditValue()
            {
                return gridLookUpEditProducer.EditValue.ToString();
            }

            public void focusChanged()
            {
                spinEditCount.Focus();
            }

            public void addPackage(DataRowView view)
            {               
                //Package["id"] = view["id"];
                //Package["name"] = view["name"];
                //Package["hospitalID"] = view["hospitalID"];
                //Package["supplierName"] = view["supplierName"];
                //Package["createdDate"] = view["createdDate"];
                //Package["isUse"] = view["isUse"];
                //Package["description"] = view["description"];
                //Package.EndEdit();
                gridLookUpEditPackage.EditValue = view["id"];      
            }

            private void addNewRow(DataRow row)
            {
                PlanDataSet.Instance.TenderDetail.idColumn.DefaultValue = getNotExistId();
                PlanDataSet.Instance.TenderDetail.tenderJournalIDColumn.DefaultValue = PlanView["id"];
                DataRowView view = (DataRowView)PlanDataSet.TenderDetialBindingSource.AddNew();
                view["packageID"] = row["packageID"];
                view["packageName"] = row["packageName"];
                view["packageCount"] = spinEditCount.EditValue;
                view["packagePrice"] = spinEditTotal.EditValue;
                view["medicineID"] = row["medicineID"];
                view["medicineCount"] = row["medicineCount"];
                view["medicinePrice"] = row["medicinePrice"];
                view["medicineName"] = row["medicineName"];
                view["latinName"] = row["latinName"];
                view["unit"] = row["unit"];
                view["shape"] = row["shape"];
                view["validDate"] = row["validDate"];
                view["medicineSumPrice"] = Convert.ToDouble(row["medicinePrice"]) * Convert.ToDouble(row["medicineCount"]); 
                view.EndEdit();
            }

            private string getNotExistId()
            {
                string newID = null;
                DataRow[] rows = null;
                do
                {
                    newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    rows = PlanDataSet.Instance.TenderDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND tenderJournalID = '" + PlanView["id"] + "' AND id = '" + newID + "'");
                } while (rows != null && rows.Length > 0);

                return newID;
            }

            private void delete()
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Тендерийн эм устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    gridViewDetail.DeleteSelectedRows();
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion          

            private void spinEditCount_EditValueChanged(object sender, EventArgs e)
            {
                spinEditTotal.EditValue = Convert.ToDouble(gridViewDetail.Columns["medicineSumPrice"].SummaryItem.SummaryValue) * Convert.ToDouble(spinEditCount.EditValue);
            }

            

    }
}