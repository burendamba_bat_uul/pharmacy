﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using DevExpress.XtraEditors.Controls;



namespace Pharmacy2016.gui.core.journal.tender
{
    public partial class TenderPlanForm : DevExpress.XtraEditors.XtraForm
    {
        
        #region Форм анх ачааллах функц

            private static TenderPlanForm INSTANCE = new TenderPlanForm();

            public static TenderPlanForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            public Int32 dividedNum;

            public DataRowView Months;

            public DataRowView Plan;


            private TenderPlanForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                repositoryItemSpinEditCount.MaxValue = Int32.MaxValue - 1;
                repositoryItemSpinEdit1.MaxValue = Int32.MaxValue - 1;

                dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                initError();   
                initDataSource();
                initBinding();
                reload();  
            }

            private void initDataSource()
            {
                gridControlPlan.DataSource = PlanDataSet.Instance.TenderPlanDetail;
                gridLookUpEditTender.Properties.DataSource = PlanDataSet.TenderJournalBindingSource;
                gridLookUpEditPackage.Properties.DataSource = PlanDataSet.Instance.TenderPackage;
                checkedComboBoxEdit.Properties.DataSource = PlanDataSet.GetBetweenDateBindingSource;
            }

            private void initError()
            {
                gridLookUpEditTender.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditPackage.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditEnd.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

            private void initBinding()
            {
                gridLookUpEditTender.DataBindings.Add(new Binding("EditValue", PlanDataSet.TenderPlanBindingSource, "tenderJournalID", true));
                gridLookUpEditPackage.DataBindings.Add(new Binding("EditValue", PlanDataSet.TenderPlanBindingSource, "packageID", true));
                dateEditStart.DataBindings.Add(new Binding("EditValue", PlanDataSet.TenderPlanBindingSource, "startDate", true));
                dateEditEnd.DataBindings.Add(new Binding("EditValue", PlanDataSet.TenderPlanBindingSource, "endDate", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", PlanDataSet.TenderPlanBindingSource, "description", true));
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    
                    actionType = type;
                    if (actionType == 1)
                    {
                        insert();
                    }
                    else if (actionType == 2)
                    {
                        update();
                    }

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insert()
            {
                DateTime date = DateTime.Now;

                //grid.Properties.ReadOnly = false;
                //dateEditEndDate.Properties.ReadOnly = false;
                //dateEditStartDate.Properties.ReadOnly = false;
                //memoEditDescription.Properties.ReadOnly = false;
                //checkedComboBoxEdit.Properties.ReadOnly = false;
                //gridViewPlan.OptionsBehavior.ReadOnly = false;
                //buttonCal.Visible = true;
                PlanDataSet.Instance.TenderPlan.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                Plan = (DataRowView)PlanDataSet.TenderPlanBindingSource.AddNew();
                Plan["startDate"] = date;
                Plan["endDate"] = date.AddMonths(11); 
                Plan.EndEdit();
            }

            private void update()
            {
                //labelControlConfirm.Visible = false;
                //checkEditIsConfirm.Visible = false;
                //textEditName.Properties.ReadOnly = false;
                //dateEditEndDate.Properties.ReadOnly = false;
                //dateEditStartDate.Properties.ReadOnly = false;
                //memoEditDescription.Properties.ReadOnly = false;
                //checkedComboBoxEdit.Properties.ReadOnly = false;
                //gridViewPlan.OptionsBehavior.ReadOnly = false;
                //buttonCal.Visible = true;
                Plan = (DataRowView)PlanDataSet.TenderPlanBindingSource.Current;
                //checkEditIsConfirm.EditValue = Plan["isConfirm"];
            }

            private void clearData()
            {
                clearErrorText();
                //ClearAndClose();
                checkedComboBoxEdit.SetEditValue(null);
                gridViewPlan.ClearSelection();
                PlanDataSet.Instance.GetbetweenDate.Rows.Clear();

                PlanDataSet.TenderPlanBindingSource.CancelEdit();
                PlanDataSet.Instance.TenderPlan.RejectChanges();
                PlanDataSet.TenderPlanDetailBindingSource.CancelEdit();
                PlanDataSet.Instance.TenderPlanDetail.RejectChanges();
            }

            private void clearErrorText()
            {
                dateEditStart.ErrorText = "";
                dateEditEnd.ErrorText = "";
                gridLookUpEditTender.ErrorText = "";
                gridLookUpEditPackage.ErrorText = "";
                gridViewPlan.ClearColumnErrors();
            }

            private void TenderPlanForm_Shown(object sender, EventArgs e)
            {
                ProgramUtil.IsShowPopup = true;
                gridLookUpEditTender.Focus();
                if (Plan != null)
                {
                    gridViewPlan.ActiveFilterString = "tenderPlanID = '" + Plan["id"] + "'";
                }
                setBetweenDate();
            }

        #endregion

        #region Формын event

            private void gridLookUpEditTender_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditTender.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditPackage.Focus();
                    }
                }
            }

            private void gridLookUpEditPackage_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditPackage.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        checkedComboBoxEdit.Focus();
                    }
                }
            }

            private void checkedComboBoxEdit_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        checkedComboBoxEdit.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        dateEditStart.Focus();
                    }
                }
            }

            private void dateEditDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditStart.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        memoEditDescription.Focus();
                    }
                }
            }

            private void gridLookUpEditTender_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditTender.EditValue != null && gridLookUpEditTender.EditValue != DBNull.Value)
                {
                    DataRowView view = (DataRowView)gridLookUpEditTender.GetSelectedDataRow();
                    if (view != null)
                        changeTender();
                }
            }

            private void gridLookUpEditPackage_Validating(object sender, CancelEventArgs e)
            {
                if (Visible)
                    setPlan();
            }

            private void gridLookUpEditPackage_EditValueChanged(object sender, EventArgs e)
            {
                //if (gridLookUpEditPackage.EditValue != null || gridLookUpEditPackage.EditValue != DBNull.Value || !gridLookUpEditPackage.EditValue.ToString().Equals(""))
                //    checkedComboBoxEdit.Focus();
                //if (gridLookUpEditPackage.EditValue == gridLookUpEditPackage.OldEditValue)
                //{
                //    PlanDataSet.TenderPlanDetailBindingSource.CancelEdit();
                //    PlanDataSet.Instance.TenderPlanDetail.RejectChanges();
                //}
            }

            private void gridViewPlan_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }
          
            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }
            
            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

            private void buttonCal_Click(object sender, EventArgs e)
            {
                if (checkedComboBoxEdit.EditValue != null || checkedComboBoxEdit.EditValue != DBNull.Value || !checkedComboBoxEdit.EditValue.ToString().Equals(""))
                    selectedRows();
            }

            private void dateEditEnd_EditValueChanged(object sender, EventArgs e)
            {
                setBetweenDate();
            }

            private void dateEditStart_EditValueChanged(object sender, EventArgs e)
            {
                dateEditEnd.Properties.MinValue = dateEditStart.DateTime.AddMonths(11);
                dateEditEnd.Properties.MaxValue = dateEditStart.DateTime.AddMonths(11);
                dateEditEnd.DateTime = dateEditStart.DateTime.AddMonths(11);
            }
        #endregion

        #region Формын функц

            private void setBetweenDate()
            {
                if (dateEditEnd.EditValue != null && dateEditEnd.EditValue != DBNull.Value && dateEditStart.EditValue != null && dateEditStart.EditValue != DBNull.Value)
                {
                    PlanDataSet.GetbetweenDateTableAdapter.Fill(PlanDataSet.Instance.GetbetweenDate, dateEditStart.DateTime, dateEditEnd.DateTime);
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("Jan", "1-р сар");
                    dict.Add("Feb", "2-р сар");
                    dict.Add("Mar", "3-р сар");
                    dict.Add("Apr", "4-р сар");
                    dict.Add("May", "5-р сар");
                    dict.Add("Jun", "6-р сар");
                    dict.Add("Jul", "7-р сар");
                    dict.Add("Aug", "8-р сар");
                    dict.Add("Sep", "9-р сар");
                    dict.Add("Oct", "10-р сар");
                    dict.Add("Nov", "11-р сар");
                    dict.Add("Dec", "12-р сар");
                    for (int i = 0; i < PlanDataSet.Instance.GetbetweenDate.Count; i++)
                    {
                        DataRow month = PlanDataSet.Instance.GetbetweenDate[i];
                        month["ColumnDate"] = dict[month["ColumnDate"].ToString().Split(' ')[0]] + " " + month["ColumnDate"].ToString().Split(' ')[1];
                    }
                }
                setColumnNames();
            }

            private void changeTender() 
            {
                PlanDataSet.Instance.TenderPackage.Clear();
                for (int i = 0; i < PlanDataSet.Instance.TenderDetail.Rows.Count; i++)
                {
                    DataRow row = PlanDataSet.Instance.TenderDetail.Rows[i];
                    if (PlanDataSet.Instance.TenderPackage.Select("tenderJournalID = '" + gridLookUpEditTender.EditValue + "' AND packageID = '" + row["packageID"] + "'").Length == 0 && gridLookUpEditTender.EditValue.ToString().Equals(row["tenderJournalID"].ToString()))
                    {
                        DataRow newRow = PlanDataSet.Instance.TenderPackage.NewRow();
                        newRow["tenderJournalID"] = row["tenderJournalID"];
                        newRow["packageID"] = row["packageID"];
                        newRow["packageName"] = row["packageName"];
                        newRow["packageCount"] = row["packageCount"];
                        newRow["packagePrice"] = row["packagePrice"];

                        DataRow[] journal = PlanDataSet.Instance.TenderJournal.Select("id = '" + row["tenderJournalID"] + "'");
                        newRow["supplierID"] = journal[0]["supplierID"];
                        newRow["supplierName"] = journal[0]["supplierName"];
                        newRow.EndEdit();
                        PlanDataSet.Instance.TenderPackage.Rows.Add(newRow);
                    }
                }
            }

            private void selectedRows()
            {
                for (int i = 0; i < gridViewPlan.SelectedRowsCount; i++)
                {
                    if (gridViewPlan.GetSelectedRows()[i] >= 0)
                    {
                        DataRow row = gridViewPlan.GetDataRow(gridViewPlan.GetSelectedRows()[i]);
                    
                        var ids = (from CheckedListBoxItem item in checkedComboBoxEdit.Properties.Items
                           where item.CheckState == CheckState.Checked
                           select Convert.ToString(item.Value)).ToArray();
                       

                        if (row["count"] != DBNull.Value && ids.Length > 0)
                        {
                            dividedNum = Convert.ToInt32(row["count"]) / ids.Length;                           
                        }  
                            if (checkedComboBoxEdit.Properties.Items[0].Value.ToString().Equals(gridViewPlan.Columns["jan"].Caption))
                            {
                                if (checkedComboBoxEdit.Properties.Items[0].CheckState == CheckState.Checked)
                                    row["jan"] = dividedNum;
                                else
                                    row["jan"] = 0;
                            }
                            if (checkedComboBoxEdit.Properties.Items[1].Value.ToString().Equals(gridViewPlan.Columns["feb"].Caption))
                            {
                                if (checkedComboBoxEdit.Properties.Items[1].CheckState == CheckState.Checked)
                                    row["feb"] = dividedNum;
                                else
                                    row["feb"] = 0;
                            }
                            if (checkedComboBoxEdit.Properties.Items[2].Value.ToString().Equals(gridViewPlan.Columns["mar"].Caption))
                            {
                                if (checkedComboBoxEdit.Properties.Items[2].CheckState == CheckState.Checked)
                                    row["mar"] = dividedNum;
                                else
                                    row["mar"] = 0;
                            }
                            if (checkedComboBoxEdit.Properties.Items[3].Value.ToString().Equals(gridViewPlan.Columns["apr"].Caption))
                            {
                                if (checkedComboBoxEdit.Properties.Items[3].CheckState == CheckState.Checked)
                                    row["apr"] = dividedNum;
                                else
                                    row["apr"] = 0;
                            }
                            if (checkedComboBoxEdit.Properties.Items[4].Value.ToString().Equals(gridViewPlan.Columns["may"].Caption))
                            {
                                if (checkedComboBoxEdit.Properties.Items[4].CheckState == CheckState.Checked)
                                    row["may"] = dividedNum;
                                else
                                    row["may"] = 0;
                            }
                            if (checkedComboBoxEdit.Properties.Items[5].Value.ToString().Equals(gridViewPlan.Columns["jun"].Caption))
                            {
                                if (checkedComboBoxEdit.Properties.Items[5].CheckState == CheckState.Checked)
                                    row["jun"] = dividedNum;
                                else
                                    row["jun"] = 0;
                            }
                            if (checkedComboBoxEdit.Properties.Items[6].Value.ToString().Equals(gridViewPlan.Columns["jul"].Caption))
                            {
                                if (checkedComboBoxEdit.Properties.Items[6].CheckState == CheckState.Checked)
                                    row["jul"] = dividedNum;
                                else
                                    row["jul"] = 0;
                            }
                            if (checkedComboBoxEdit.Properties.Items[7].Value.ToString().Equals(gridViewPlan.Columns["aug"].Caption))
                            {
                                if (checkedComboBoxEdit.Properties.Items[7].CheckState == CheckState.Checked)
                                    row["aug"] = dividedNum;
                                else
                                    row["aug"] = 0;
                            }
                            if (checkedComboBoxEdit.Properties.Items[8].Value.ToString().Equals(gridViewPlan.Columns["sep"].Caption))
                            {
                                if (checkedComboBoxEdit.Properties.Items[8].CheckState == CheckState.Checked)
                                    row["sep"] = dividedNum;
                                else
                                    row["sep"] = 0;
                            }
                            if (checkedComboBoxEdit.Properties.Items[9].Value.ToString().Equals(gridViewPlan.Columns["oct"].Caption))
                            {
                                if (checkedComboBoxEdit.Properties.Items[9].CheckState == CheckState.Checked)
                                    row["oct"] = dividedNum;
                                else
                                    row["oct"] = 0;
                            }
                            if (checkedComboBoxEdit.Properties.Items[10].Value.ToString().Equals(gridViewPlan.Columns["nov"].Caption))
                            {
                                if (checkedComboBoxEdit.Properties.Items[10].CheckState == CheckState.Checked)
                                    row["nov"] = dividedNum;
                                else
                                    row["nov"] = 0;
                            }
                            if (checkedComboBoxEdit.Properties.Items[11].Value.ToString().Equals(gridViewPlan.Columns["dec"].Caption))
                            {
                                row["dec"] = dividedNum;
                                if (checkedComboBoxEdit.Properties.Items[11].CheckState == CheckState.Checked)
                                {
                                    if (!Convert.ToInt32(row["count"]).Equals(Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(row["dec"])))
                                    {
                                        //XtraMessageBox.Show(Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(row["dec"]) + "");
                                        Int32 plusNum = Convert.ToInt32(row["count"]) - (Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(row["dec"]));
                                        row["dec"] = plusNum + dividedNum;
                                    }
                                    else
                                    {
                                        row["dec"] = dividedNum;
                                    }
                                }
                                else
                                {
                                    if (!Convert.ToInt32(row["count"]).Equals(Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(dividedNum)))
                                    {
                                        //XtraMessageBox.Show(Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(row["dec"]) + "");
                                        Int32 plusNum = Convert.ToInt32(row["count"]) - (Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(dividedNum));
                                        row["dec"] = dividedNum + plusNum;
                                    }
                                    else
                                    {
                                        row["dec"] = 0;
                                    }
                                }
                            }
                            row.EndEdit();       
                    }
                    else
                    {
                        DataRow row = gridViewPlan.GetDataRow(i);   
                        row["jan"] = 0;
                        row["feb"] = 0;
                        row["mar"] = 0;
                        row["apr"] = 0;
                        row["may"] = 0;
                        row["jun"] = 0;
                        row["jul"] = 0;
                        row["aug"] = 0;
                        row["sep"] = 0;
                        row["oct"] = 0;
                        row["nov"] = 0;
                        row["dec"] = 0;
                        row.EndEdit();
                    }
                }
            }

            private void setColumnNames()
            {
                if(PlanDataSet.Instance.GetbetweenDate.Count() > 0)
                {
                    gridViewPlan.Columns["jan"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[0]["ColumnDate"].ToString();
                    gridViewPlan.Columns["feb"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[1]["ColumnDate"].ToString();
                    gridViewPlan.Columns["mar"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[2]["ColumnDate"].ToString();
                    gridViewPlan.Columns["apr"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[3]["ColumnDate"].ToString();
                    gridViewPlan.Columns["may"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[4]["ColumnDate"].ToString();
                    gridViewPlan.Columns["jun"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[5]["ColumnDate"].ToString();
                    gridViewPlan.Columns["jul"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[6]["ColumnDate"].ToString();
                    gridViewPlan.Columns["aug"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[7]["ColumnDate"].ToString();
                    gridViewPlan.Columns["sep"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[8]["ColumnDate"].ToString();
                    gridViewPlan.Columns["oct"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[9]["ColumnDate"].ToString();
                    gridViewPlan.Columns["nov"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[10]["ColumnDate"].ToString();
                    gridViewPlan.Columns["dec"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[11]["ColumnDate"].ToString();
                }
            }

            private void setPlan()
            {
                if (gridLookUpEditPackage.EditValue != null && gridLookUpEditPackage.EditValue != DBNull.Value && !gridLookUpEditPackage.Text.ToString().Equals("") && checkBeforeAdd() && gridLookUpEditTender.EditValue != null && gridLookUpEditTender.EditValue != DBNull.Value)
                {  
                    //PlanDataSet.Instance.TenderPlanDetail.Rows.Clear();
                    if (Plan != null)
                    {
                        DataRow[] oldRows = PlanDataSet.Instance.TenderPlanDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND tenderPlanID = '" + Plan["id"] + "'");
                        foreach (DataRow row in oldRows)
                        {
                            row.Delete();
                        }
                    }
                    DataRow[] rows = PlanDataSet.Instance.TenderDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND packageID = '" + gridLookUpEditPackage.EditValue + "' AND tenderJournalID = '" + gridLookUpEditTender.EditValue + "'");
                    for (int i = 0; i < rows.Length; i++)
                    {  
                        DataRowView view = (DataRowView)PlanDataSet.TenderPlanDetailBindingSource.AddNew();
                        view["id"] = getNotExistId();
                        view["tenderPlanID"] = Plan["id"];
                        view["medicineID"] = rows[i]["medicineID"];
                        view["medicineName"] = rows[i]["medicineName"];
                        view["unit"] = rows[i]["unit"];
                        view["shape"] = rows[i]["shape"];
                        view["quantity"] = rows[i]["quantity"];
                        view["validDate"] = rows[i]["validDate"];
                        view["price"] = rows[i]["medicinePrice"];
                        view["count"] = rows[i]["medicineCount"];
                        view["jan"] = 0;
                        view["feb"] = 0;
                        view["mar"] = 0;
                        view["apr"] = 0;
                        view["may"] = 0;
                        view["jun"] = 0;
                        view["jul"] = 0;
                        view["aug"] = 0;
                        view["sep"] = 0;
                        view["oct"] = 0;
                        view["nov"] = 0;
                        view["dec"] = 0;
                        view.EndEdit();
                    }
                }
            }

            private bool checkBeforeAdd()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (isRight && Plan != null && gridLookUpEditTender.EditValue != null && gridLookUpEditPackage.EditValue != null)
                {
                    DataRow[] rows = PlanDataSet.Instance.TenderPlan.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND tenderJournalID = '" + gridLookUpEditTender.EditValue + " 'AND packageID = '" + gridLookUpEditPackage.EditValue + "'  AND id <> '" + Plan["id"] + "'");
                    for (int i = 0; rows != null && i < rows.Length; i++)
                    {
                        if (gridLookUpEditPackage.EditValue.ToString().Equals(rows[i]["packageID"].ToString()) && rows[i]["packageID"] != null)
                        {
                            errorText = "Тендер, багц давхацсан байна";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            gridLookUpEditPackage.ErrorText = errorText;
                            isRight = false;
                            gridLookUpEditPackage.Focus();
                        }
                    }
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private string getNotExistId()
            {
                string newID = null;
                DataRow[] rows = null;
                do
                {
                    newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    rows = PlanDataSet.Instance.TenderPlanDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND tenderPlanID = '" + Plan["id"] + "' AND id = '" + newID + "'");
                } while (rows != null && rows.Length > 0);

                return newID;
            }

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                //if (PlanDataSet.Instance.PlanMonthly.Rows.Count == 0)
                //    PlanDataSet.PlanMonthlyTableAdapter.Fill(PlanDataSet.Instance.PlanMonthly, HospitalUtil.getHospitalId());
                if (UserDataSet.Instance.Medicine.Rows.Count == 0)
                    UserDataSet.MedicineTableAdapter.Fill(UserDataSet.Instance.Medicine, HospitalUtil.getHospitalId());
                if (PlanDataSet.Instance.PackageOther.Rows.Count == 0)
                    PlanDataSet.PackageOtherTableAdapter.Fill(PlanDataSet.Instance.PackageOther, HospitalUtil.getHospitalId());

                ProgramUtil.closeWaitDialog();
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditTender.EditValue == null || gridLookUpEditTender.EditValue == DBNull.Value || gridLookUpEditTender.EditValue.ToString().Equals(""))
                {
                    errorText = "Тендер сонгоно уу!";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditTender.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditTender.Focus();
                }
                if (gridLookUpEditPackage.EditValue == DBNull.Value || gridLookUpEditPackage.EditValue == null || gridLookUpEditPackage.EditValue.ToString().Equals(""))
                {
                    errorText = "Багцаас сонгоно уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditPackage.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditPackage.Focus();
                    }
                }
                if (dateEditStart.EditValue == DBNull.Value || dateEditStart.EditValue == null || dateEditStart.EditValue.ToString().Equals(""))
                {
                    errorText = "Эхлэх огноог оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditStart.Focus();
                    }
                }
                if (dateEditEnd.EditValue == DBNull.Value || dateEditEnd.EditValue == null || dateEditEnd.EditValue.ToString().Equals(""))
                {
                    errorText = "Дуусах огноог оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                } 
                if (gridViewPlan.RowCount <= 0)
                {
                    errorText = "Төлөвлөгөөнд эм оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    //dateEditStartDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        //dateEditStartDate.Focus();
                    }
                }
                if (isRight)
                {
                    for (int j = 0; j < gridViewPlan.RowCount; j++)
                    {
                        if (gridViewPlan.GetRowCellValue(j, gridColumnCount) != null && gridViewPlan.GetRowCellValue(j, gridColumnCount) != DBNull.Value && Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnCount)) > 0 &&
                                !Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnCount)).Equals(Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnJan)) + Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnFeb)) + Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnMar)) + Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnApr)) + Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnMay)) + Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnJun)) + Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnJul)) + Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnAug)) + Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnSep)) + Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnOct)) + Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnNov)) + Convert.ToInt32(gridViewPlan.GetRowCellValue(j, gridColumnDec))))
                        {
                            errorText = "Жилийн тоо, саруудад хуваасан тоонууд тэнцэхгүй байна!";
                            gridViewPlan.SetColumnError(gridColumnCount, errorText);
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            isRight = false;
                            break;
                        }
                    }
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                try
                {
                    if (check())
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);

                        DataRowView tender = (DataRowView)gridLookUpEditTender.GetSelectedDataRow();

                        Plan["tenderNumber"] = tender["tenderNumber"];
                        Plan["packageName"] = gridLookUpEditPackage.Text;
                        Plan["supplierName"] = tender["supplierName"];
                        Plan["supplierID"] = tender["supplierID"];
                        Plan.EndEdit();

                        PlanDataSet.TenderPlanBindingSource.EndEdit();
                        PlanDataSet.TenderPlanTableAdapter.Update(PlanDataSet.Instance.TenderPlan);

                        PlanDataSet.TenderPlanDetailBindingSource.EndEdit();
                        PlanDataSet.TenderPlanDetailTableAdapter.Update(PlanDataSet.Instance.TenderPlanDetail);

                        ProgramUtil.closeWaitDialog();
                        Close();
                    }
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                }
            }        

            public List<string> gridRows()
            {
                List<string> rows = new List<string>();
                for (int i = 0; i < gridViewPlan.RowCount; i++)
                {
                    rows.Add(gridViewPlan.GetDataRow(i)["medicineID"].ToString());
                }
                return rows;            
            }

        #endregion                          
            
    }
}