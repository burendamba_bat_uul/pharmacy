﻿namespace Pharmacy2016.gui.core.journal.tender
{
    partial class TenderPlanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TenderPlanForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.buttonCal = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.checkedComboBoxEdit = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlPlan = new DevExpress.XtraGrid.GridControl();
            this.gridViewPlan = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnMedicineID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCount = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnJan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnFeb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnApr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnJun = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnJul = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnAug = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnNov = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.gridLookUpEditPackage = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlWarehouse = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditTender = new DevExpress.XtraEditors.GridLookUpEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPackage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCal
            // 
            this.buttonCal.Image = ((System.Drawing.Image)(resources.GetObject("buttonCal.Image")));
            this.buttonCal.Location = new System.Drawing.Point(597, 57);
            this.buttonCal.Name = "buttonCal";
            this.buttonCal.Size = new System.Drawing.Size(25, 20);
            this.buttonCal.TabIndex = 13;
            this.buttonCal.ToolTip = "Сонгосон саруудад тэгш хуваах";
            this.buttonCal.Click += new System.EventHandler(this.buttonCal_Click);
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(94, 85);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(49, 13);
            this.labelControl8.TabIndex = 109;
            this.labelControl8.Text = "Тайлбар :";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.memoEditDescription.EnterMoveNextControl = true;
            this.memoEditDescription.Location = new System.Drawing.Point(149, 83);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(442, 41);
            this.memoEditDescription.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(363, 60);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(72, 13);
            this.labelControl1.TabIndex = 108;
            this.labelControl1.Text = "Сард хуваах :";
            // 
            // checkedComboBoxEdit
            // 
            this.checkedComboBoxEdit.EditValue = "";
            this.checkedComboBoxEdit.Location = new System.Drawing.Point(441, 57);
            this.checkedComboBoxEdit.Name = "checkedComboBoxEdit";
            this.checkedComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEdit.Properties.DisplayMember = "ColumnDate";
            this.checkedComboBoxEdit.Properties.DropDownRows = 12;
            this.checkedComboBoxEdit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("jan", "1-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("feb", "2-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("mar", "3-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("apr", "4-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("may", "5-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("jun", "6-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("jul", "7-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("aug", "8-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("sep", "9-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("oct", "10-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("nov", "11-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("dec", "12-р сар")});
            this.checkedComboBoxEdit.Properties.ShowButtons = false;
            this.checkedComboBoxEdit.Properties.ValueMember = "ColumnDate";
            this.checkedComboBoxEdit.Size = new System.Drawing.Size(150, 20);
            this.checkedComboBoxEdit.TabIndex = 12;
            this.checkedComboBoxEdit.ToolTip = "Харагдах багана сонгох";
            this.checkedComboBoxEdit.KeyUp += new System.Windows.Forms.KeyEventHandler(this.checkedComboBoxEdit_KeyUp);
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(413, 31);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(100, 20);
            this.dateEditStart.TabIndex = 3;
            this.dateEditStart.EditValueChanged += new System.EventHandler(this.dateEditStart_EditValueChanged);
            this.dateEditStart.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditDate_KeyUp);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(94, 34);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(50, 13);
            this.labelControl6.TabIndex = 107;
            this.labelControl6.Text = "Тендер* :";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(361, 34);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(46, 13);
            this.labelControl4.TabIndex = 105;
            this.labelControl4.Text = "Эхлэх * :";
            // 
            // gridControlPlan
            // 
            this.gridControlPlan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlPlan.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, false, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, false, "Устгах", "delete")});
            this.gridControlPlan.Location = new System.Drawing.Point(12, 130);
            this.gridControlPlan.MainView = this.gridViewPlan;
            this.gridControlPlan.Name = "gridControlPlan";
            this.gridControlPlan.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemSpinEditCount});
            this.gridControlPlan.Size = new System.Drawing.Size(760, 287);
            this.gridControlPlan.TabIndex = 5;
            this.gridControlPlan.UseEmbeddedNavigator = true;
            this.gridControlPlan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPlan});
            // 
            // gridViewPlan
            // 
            this.gridViewPlan.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnMedicineID,
            this.gridColumnCount,
            this.gridColumnJan,
            this.gridColumnFeb,
            this.gridColumnMar,
            this.gridColumnApr,
            this.gridColumnMay,
            this.gridColumnJun,
            this.gridColumnJul,
            this.gridColumnAug,
            this.gridColumnSep,
            this.gridColumnOct,
            this.gridColumnNov,
            this.gridColumnDec,
            this.gridColumn6});
            this.gridViewPlan.GridControl = this.gridControlPlan;
            this.gridViewPlan.Name = "gridViewPlan";
            this.gridViewPlan.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewPlan.OptionsSelection.MultiSelect = true;
            this.gridViewPlan.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridViewPlan.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewPlan.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPlan.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewPlan.OptionsView.ShowFooter = true;
            this.gridViewPlan.OptionsView.ShowGroupPanel = false;
            this.gridViewPlan.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewPlan_InvalidRowException);
            // 
            // gridColumnMedicineID
            // 
            this.gridColumnMedicineID.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnMedicineID.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnMedicineID.Caption = "Нэр";
            this.gridColumnMedicineID.CustomizationCaption = "Худалдааны нэр";
            this.gridColumnMedicineID.FieldName = "medicineName";
            this.gridColumnMedicineID.MinWidth = 175;
            this.gridColumnMedicineID.Name = "gridColumnMedicineID";
            this.gridColumnMedicineID.Visible = true;
            this.gridColumnMedicineID.VisibleIndex = 1;
            this.gridColumnMedicineID.Width = 175;
            // 
            // gridColumnCount
            // 
            this.gridColumnCount.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnCount.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnCount.Caption = "Жилд авах багцын тоо";
            this.gridColumnCount.ColumnEdit = this.repositoryItemSpinEditCount;
            this.gridColumnCount.CustomizationCaption = "Жилдээ хэрэглэх тоо";
            this.gridColumnCount.DisplayFormat.FormatString = "n2";
            this.gridColumnCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnCount.FieldName = "count";
            this.gridColumnCount.MinWidth = 125;
            this.gridColumnCount.Name = "gridColumnCount";
            this.gridColumnCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumnCount.Visible = true;
            this.gridColumnCount.VisibleIndex = 3;
            this.gridColumnCount.Width = 125;
            // 
            // repositoryItemSpinEditCount
            // 
            this.repositoryItemSpinEditCount.AutoHeight = false;
            this.repositoryItemSpinEditCount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditCount.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEditCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditCount.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEditCount.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditCount.Mask.EditMask = "n2";
            this.repositoryItemSpinEditCount.Name = "repositoryItemSpinEditCount";
            // 
            // gridColumnJan
            // 
            this.gridColumnJan.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnJan.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnJan.Caption = "1-р сар";
            this.gridColumnJan.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnJan.FieldName = "jan";
            this.gridColumnJan.MinWidth = 75;
            this.gridColumnJan.Name = "gridColumnJan";
            this.gridColumnJan.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jan", "{0:n2}")});
            this.gridColumnJan.Visible = true;
            this.gridColumnJan.VisibleIndex = 4;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnFeb
            // 
            this.gridColumnFeb.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnFeb.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnFeb.Caption = "2-р сар";
            this.gridColumnFeb.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnFeb.FieldName = "feb";
            this.gridColumnFeb.MinWidth = 75;
            this.gridColumnFeb.Name = "gridColumnFeb";
            this.gridColumnFeb.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "feb", "{0:n2}")});
            this.gridColumnFeb.Visible = true;
            this.gridColumnFeb.VisibleIndex = 5;
            // 
            // gridColumnMar
            // 
            this.gridColumnMar.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnMar.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnMar.Caption = "3-р сар";
            this.gridColumnMar.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnMar.FieldName = "mar";
            this.gridColumnMar.MinWidth = 75;
            this.gridColumnMar.Name = "gridColumnMar";
            this.gridColumnMar.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "mar", "{0:n2}")});
            this.gridColumnMar.Visible = true;
            this.gridColumnMar.VisibleIndex = 6;
            // 
            // gridColumnApr
            // 
            this.gridColumnApr.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnApr.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnApr.Caption = "4-р сар";
            this.gridColumnApr.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnApr.FieldName = "apr";
            this.gridColumnApr.MinWidth = 75;
            this.gridColumnApr.Name = "gridColumnApr";
            this.gridColumnApr.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "apr", "{0:n2}")});
            this.gridColumnApr.Visible = true;
            this.gridColumnApr.VisibleIndex = 7;
            // 
            // gridColumnMay
            // 
            this.gridColumnMay.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnMay.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnMay.Caption = "5-р сар";
            this.gridColumnMay.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnMay.FieldName = "may";
            this.gridColumnMay.MinWidth = 75;
            this.gridColumnMay.Name = "gridColumnMay";
            this.gridColumnMay.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "may", "{0:n2}")});
            this.gridColumnMay.Visible = true;
            this.gridColumnMay.VisibleIndex = 8;
            // 
            // gridColumnJun
            // 
            this.gridColumnJun.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnJun.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnJun.Caption = "6-р сар";
            this.gridColumnJun.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnJun.FieldName = "jun";
            this.gridColumnJun.MinWidth = 75;
            this.gridColumnJun.Name = "gridColumnJun";
            this.gridColumnJun.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jun", "{0:n2}")});
            this.gridColumnJun.Visible = true;
            this.gridColumnJun.VisibleIndex = 9;
            // 
            // gridColumnJul
            // 
            this.gridColumnJul.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnJul.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnJul.Caption = "7-р сар";
            this.gridColumnJul.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnJul.FieldName = "jul";
            this.gridColumnJul.MinWidth = 75;
            this.gridColumnJul.Name = "gridColumnJul";
            this.gridColumnJul.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jul", "{0:n2}")});
            this.gridColumnJul.Visible = true;
            this.gridColumnJul.VisibleIndex = 10;
            // 
            // gridColumnAug
            // 
            this.gridColumnAug.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnAug.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnAug.Caption = "8-р сар";
            this.gridColumnAug.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnAug.FieldName = "aug";
            this.gridColumnAug.MinWidth = 75;
            this.gridColumnAug.Name = "gridColumnAug";
            this.gridColumnAug.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "aug", "{0:n2}")});
            this.gridColumnAug.Visible = true;
            this.gridColumnAug.VisibleIndex = 11;
            // 
            // gridColumnSep
            // 
            this.gridColumnSep.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnSep.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnSep.Caption = "9-р сар";
            this.gridColumnSep.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnSep.FieldName = "sep";
            this.gridColumnSep.MinWidth = 75;
            this.gridColumnSep.Name = "gridColumnSep";
            this.gridColumnSep.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sep", "{0:n2}")});
            this.gridColumnSep.Visible = true;
            this.gridColumnSep.VisibleIndex = 12;
            // 
            // gridColumnOct
            // 
            this.gridColumnOct.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnOct.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnOct.Caption = "10-р сар";
            this.gridColumnOct.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnOct.FieldName = "oct";
            this.gridColumnOct.MinWidth = 75;
            this.gridColumnOct.Name = "gridColumnOct";
            this.gridColumnOct.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "oct", "{0:n2}")});
            this.gridColumnOct.Visible = true;
            this.gridColumnOct.VisibleIndex = 13;
            // 
            // gridColumnNov
            // 
            this.gridColumnNov.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnNov.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnNov.Caption = "11-р сар";
            this.gridColumnNov.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnNov.FieldName = "nov";
            this.gridColumnNov.MinWidth = 75;
            this.gridColumnNov.Name = "gridColumnNov";
            this.gridColumnNov.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "nov", "{0:n2}")});
            this.gridColumnNov.Visible = true;
            this.gridColumnNov.VisibleIndex = 14;
            // 
            // gridColumnDec
            // 
            this.gridColumnDec.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnDec.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnDec.Caption = "12-р сар";
            this.gridColumnDec.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnDec.FieldName = "dec";
            this.gridColumnDec.MinWidth = 75;
            this.gridColumnDec.Name = "gridColumnDec";
            this.gridColumnDec.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "dec", "{0:n2}")});
            this.gridColumnDec.Visible = true;
            this.gridColumnDec.VisibleIndex = 15;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumn6.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn6.Caption = "Үнэ";
            this.gridColumn6.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn6.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumn6.FieldName = "price";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 423);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 10;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(697, 423);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 9;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(616, 423);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 8;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // gridLookUpEditPackage
            // 
            this.gridLookUpEditPackage.EditValue = "";
            this.gridLookUpEditPackage.Location = new System.Drawing.Point(149, 57);
            this.gridLookUpEditPackage.Name = "gridLookUpEditPackage";
            this.gridLookUpEditPackage.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditPackage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPackage.Properties.DisplayMember = "packageName";
            this.gridLookUpEditPackage.Properties.NullText = "";
            this.gridLookUpEditPackage.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditPackage.Properties.ValueMember = "packageID";
            this.gridLookUpEditPackage.Properties.View = this.gridView2;
            this.gridLookUpEditPackage.Size = new System.Drawing.Size(199, 20);
            this.gridLookUpEditPackage.TabIndex = 2;
            this.gridLookUpEditPackage.EditValueChanged += new System.EventHandler(this.gridLookUpEditPackage_EditValueChanged);
            this.gridLookUpEditPackage.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditPackage_KeyUp);
            this.gridLookUpEditPackage.Validating += new System.ComponentModel.CancelEventHandler(this.gridLookUpEditPackage_Validating);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn2,
            this.gridColumn9,
            this.gridColumn7});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsMenu.ShowAutoFilterRowItem = false;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Багцын нэр";
            this.gridColumn8.CustomizationCaption = "Багцын нэр";
            this.gridColumn8.FieldName = "packageName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Бэлтгэн нийлүүлэгч";
            this.gridColumn2.CustomizationCaption = "Бэлтгэн нийлүүлэгч";
            this.gridColumn2.FieldName = "supplierName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Тоо хэмжээ";
            this.gridColumn9.CustomizationCaption = "Тоо хэмжээ";
            this.gridColumn9.FieldName = "packageCount";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 2;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Үнэ";
            this.gridColumn7.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumn7.FieldName = "packagePrice";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            // 
            // labelControlWarehouse
            // 
            this.labelControlWarehouse.Location = new System.Drawing.Point(107, 60);
            this.labelControlWarehouse.Name = "labelControlWarehouse";
            this.labelControlWarehouse.Size = new System.Drawing.Size(36, 13);
            this.labelControlWarehouse.TabIndex = 112;
            this.labelControlWarehouse.Text = "Багц* :";
            // 
            // gridLookUpEditTender
            // 
            this.gridLookUpEditTender.EditValue = "";
            this.gridLookUpEditTender.Location = new System.Drawing.Point(149, 31);
            this.gridLookUpEditTender.Name = "gridLookUpEditTender";
            this.gridLookUpEditTender.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditTender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditTender.Properties.DisplayMember = "tenderNumber";
            this.gridLookUpEditTender.Properties.NullText = "";
            this.gridLookUpEditTender.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditTender.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridLookUpEditTender.Properties.ValueMember = "id";
            this.gridLookUpEditTender.Properties.View = this.gridView1;
            this.gridLookUpEditTender.Size = new System.Drawing.Size(199, 20);
            this.gridLookUpEditTender.TabIndex = 1;
            this.gridLookUpEditTender.EditValueChanged += new System.EventHandler(this.gridLookUpEditTender_EditValueChanged);
            this.gridLookUpEditTender.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditTender_KeyUp);
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsMenu.ShowAutoFilterRowItem = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Тендерийн дугаар";
            this.gridColumn3.CustomizationCaption = "Тендерийн дугаар";
            this.gridColumn3.FieldName = "tenderNumber";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Бэлтгэн нийлүүлэгч";
            this.gridColumn4.CustomizationCaption = "Бэлтгэн нийлүүлэгч";
            this.gridColumn4.FieldName = "supplierName";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Огноо";
            this.gridColumn5.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn5.CustomizationCaption = "Огноо";
            this.gridColumn5.FieldName = "date";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(575, 31);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(100, 20);
            this.dateEditEnd.TabIndex = 113;
            this.dateEditEnd.EditValueChanged += new System.EventHandler(this.dateEditEnd_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(519, 34);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(50, 13);
            this.labelControl2.TabIndex = 114;
            this.labelControl2.Text = "Дуусах* :";
            // 
            // TenderPlanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.dateEditEnd);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.gridLookUpEditTender);
            this.Controls.Add(this.gridLookUpEditPackage);
            this.Controls.Add(this.labelControlWarehouse);
            this.Controls.Add(this.buttonCal);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.memoEditDescription);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.checkedComboBoxEdit);
            this.Controls.Add(this.dateEditStart);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.gridControlPlan);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonSave);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TenderPlanForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Тендерийн төлөвлөгөө";
            this.Shown += new System.EventHandler(this.TenderPlanForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPackage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonCal;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEdit;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGrid.GridControl gridControlPlan;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPlan;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMedicineID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnJan;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnFeb;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMar;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnApr;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMay;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnJun;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnJul;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnAug;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSep;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOct;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnNov;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDec;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPackage;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.LabelControl labelControlWarehouse;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditTender;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.LabelControl labelControl2;

    }
}