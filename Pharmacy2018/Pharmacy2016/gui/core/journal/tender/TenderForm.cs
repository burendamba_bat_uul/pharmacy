﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.util;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPivotGrid;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.gui.core.info.ds;
using Excel = Microsoft.Office.Interop.Excel;



namespace Pharmacy2016.gui.core.journal.tender
{
    public partial class TenderForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static TenderForm INSTANCE = new TenderForm();

            public static TenderForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 2010;

            private bool isInit = false;

            public DataRowView currView;

            private GridView detail;

            private TenderForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                RoleUtil.addForm(FORM_ID, this);

                //gridControlMonthly.DataSource = PlanDataSet.PlanMonthlyBindingSource;
                gridControlPlan.DataSource = PlanDataSet.TenderJournalBindingSource;
                gridControlTenderPlan.DataSource = PlanDataSet.TenderPlanBindingSource;
                gridControlTenderInc.DataSource = PlanDataSet.TenderIncBindingSource;
                gridControlTenderIncMonth.DataSource = PlanDataSet.Instance.TenderIncMonth;
                gridLookUpEditTender.Properties.DataSource = PlanDataSet.TenderPlanBindingSource;

                dateEditPlanStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditPlanStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                DateTime date = DateTime.Now;
                dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1);
                dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            }

        #endregion
        
        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        checkRole();

                        PlanDataSet.Instance.TenderJournal.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        PlanDataSet.Instance.TenderJournal.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        PlanDataSet.Instance.TenderDetail.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        PlanDataSet.Instance.TenderDetail.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        PlanDataSet.Instance.TenderPlan.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        PlanDataSet.Instance.TenderPlan.actionUserIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        PlanDataSet.Instance.TenderPlanDetail.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        PlanDataSet.Instance.TenderPlanDetail.actionUserIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        reload();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                gridControlPlan.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlPlan.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridControlPlan.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                gridControlTenderPlan.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlTenderPlan.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridControlTenderPlan.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                dropDownButtonPrint.Visible = roles[4];
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;
            }

            private void PlanForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                PlanDataSet.Instance.TenderJournal.Clear();
                PlanDataSet.Instance.TenderDetail.Clear();

                PlanDataSet.Instance.TenderPlan.Clear();
                PlanDataSet.Instance.TenderPlanDetail.Clear();

                PlanDataSet.Instance.TenderInc.Clear();
            }

        #endregion

        #region Формын event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                //saveLayout();
            }

            private void radioGroupJournal_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeJournal();
            }

            private void gridControlInc_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        add();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewPlan.GetFocusedRow() != null)
                    {
                        try
                        {
                            delete();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "update") && gridViewPlan.GetFocusedRow() != null)
                    {
                        update();
                    }
                }
            }
            
            private void gridViewPlan_MasterRowExpanded(object sender, CustomMasterRowEventArgs e)
            {
                for (int rowHandle = 0; rowHandle < gridViewPlan.RowCount; rowHandle++)
                {
                    if (rowHandle != gridViewPlan.FocusedRowHandle)
                        gridViewPlan.CollapseMasterRow(rowHandle);
                }
                if (!gridViewPlan.GetMasterRowExpanded(e.RowHandle))
                    gridViewPlan.SetMasterRowExpanded(e.RowHandle, true);
                detail = gridViewPlan.GetDetailView(e.RowHandle, e.RelationIndex) as GridView;
            }

            private void gridViewPlanDetail_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
            {
                //DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo info = e.Info as DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo;
                //info.GroupText = string.Format("{0} (Багцын тоо = {1} Дүн = {2})", info.GroupValueText, gridViewPlanDetail.GetGroupSummaryDisplayText(e.RowHandle, gridViewPlanDetail.GroupSummary[0] as DevExpress.XtraGrid.GridGroupSummaryItem), gridViewPlanDetail.GetGroupSummaryValue(e.RowHandle, gridViewPlanDetail.GroupSummary[1] as DevExpress.XtraGrid.GridGroupSummaryItem));

                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo info = e.Info as DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo;
                info.GroupText = string.Format("{0} (Багцын тоо = {1} Дүн = {2})", info.GroupValueText, detail.GetGroupSummaryDisplayText(e.RowHandle, detail.GroupSummary[0] as DevExpress.XtraGrid.GridGroupSummaryItem), detail.GetGroupSummaryDisplayText(e.RowHandle, detail.GroupSummary[1] as DevExpress.XtraGrid.GridGroupSummaryItem));
            }

            private void gridControlTenderPlan_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addPlan();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && bandedGridViewTenderPlan.GetFocusedRow() != null)
                    {
                        try
                        {
                            deletePlan();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "update") && bandedGridViewTenderPlan.GetFocusedRow() != null)
                    {
                        updatePlan();
                    }
                }
            }

            private void gridLookUpEditTender_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditTender.EditValue != DBNull.Value && gridLookUpEditTender.EditValue != null)
                {
                    changeTender();
                }
            }

            private void comboBoxEditMonth_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeTenderMonth();
            }

            private void gridControlTenderPlan_ViewRegistered(object sender, DevExpress.XtraGrid.ViewOperationEventArgs e)
            {
                setBetweenDate();
                if (PlanDataSet.Instance.GetbetweenDate.Count() > 0)
                {
                    if (Convert.ToDouble((e.View as GridView).Columns["jan"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["jan"].Visible = true;
                        (e.View as GridView).Columns["jan"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[0]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["jan"].Visible = false;
                    if (Convert.ToDouble((e.View as GridView).Columns["feb"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["feb"].Visible = true;
                        (e.View as GridView).Columns["feb"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[1]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["feb"].Visible = false;
                    if (Convert.ToDouble((e.View as GridView).Columns["mar"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["mar"].Visible = true;
                        (e.View as GridView).Columns["mar"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[2]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["mar"].Visible = false;
                    if (Convert.ToDouble((e.View as GridView).Columns["apr"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["apr"].Visible = true;
                        (e.View as GridView).Columns["apr"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[3]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["apr"].Visible = false;
                    if (Convert.ToDouble((e.View as GridView).Columns["may"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["may"].Visible = true;
                        (e.View as GridView).Columns["may"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[4]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["may"].Visible = false;
                    if (Convert.ToDouble((e.View as GridView).Columns["jun"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["jun"].Visible = true;
                        (e.View as GridView).Columns["jun"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[5]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["jun"].Visible = false;
                    if (Convert.ToDouble((e.View as GridView).Columns["jul"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["jul"].Visible = true;
                        (e.View as GridView).Columns["jul"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[6]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["jul"].Visible = false;
                    if (Convert.ToDouble((e.View as GridView).Columns["aug"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["aug"].Visible = true;
                        (e.View as GridView).Columns["aug"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[7]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["aug"].Visible = false;
                    if (Convert.ToDouble((e.View as GridView).Columns["sep"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["sep"].Visible = true;
                        (e.View as GridView).Columns["sep"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[8]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["sep"].Visible = false;
                    if (Convert.ToDouble((e.View as GridView).Columns["oct"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["oct"].Visible = true;
                        (e.View as GridView).Columns["oct"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[9]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["oct"].Visible = false;
                    if (Convert.ToDouble((e.View as GridView).Columns["nov"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["nov"].Visible = true;
                        (e.View as GridView).Columns["nov"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[10]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["nov"].Visible = false;
                    if (Convert.ToDouble((e.View as GridView).Columns["dec"].SummaryItem.SummaryValue) > 0)
                    {
                        (e.View as GridView).Columns["dec"].Visible = true;
                        (e.View as GridView).Columns["dec"].Caption = PlanDataSet.Instance.GetbetweenDate.Rows[11]["ColumnDate"].ToString();
                    }
                    else (e.View as GridView).Columns["dec"].Visible = false;                    
                    
                }
            }

        #endregion

        #region Формын функц

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void reload()
            {
                if(check())
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        PlanDataSet.TenderJournalTableAdapter.Fill(PlanDataSet.Instance.TenderJournal, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        PlanDataSet.TenderDetailTableAdapter.Fill(PlanDataSet.Instance.TenderDetail, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        //PlanDataSet.PlanMonthlyTableAdapter.Fill(PlanDataSet.Instance.PlanMonthly, HospitalUtil.getHospitalId());
                        PlanDataSet.PackageDetialOtherTableAdapter.Fill(PlanDataSet.Instance.PackageDetialOther, HospitalUtil.getHospitalId());
                        PlanDataSet.TenderPlanTableAdapter.Fill(PlanDataSet.Instance.TenderPlan, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        PlanDataSet.TenderPlanDetailTableAdapter.Fill(PlanDataSet.Instance.TenderPlanDetail, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        ProgramUtil.closeWaitDialog();
                    }
                    else
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        PlanDataSet.TenderJournalTableAdapter.Fill(PlanDataSet.Instance.TenderJournal, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        PlanDataSet.TenderDetailTableAdapter.Fill(PlanDataSet.Instance.TenderDetail, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        //PlanDataSet.PlanMonthlyTableAdapter.Fill(PlanDataSet.Instance.PlanMonthly, HospitalUtil.getHospitalId());
                        PlanDataSet.TenderPlanTableAdapter.Fill(PlanDataSet.Instance.TenderPlan, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        PlanDataSet.TenderPlanDetailTableAdapter.Fill(PlanDataSet.Instance.TenderPlanDetail, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        PlanDataSet.PackageDetialOtherTableAdapter.Fill(PlanDataSet.Instance.PackageDetialOther, HospitalUtil.getHospitalId());
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            private void changeJournal()
            {
                if (radioGroupJournal.SelectedIndex == 0)
                {
                    labelControlMonth.Visible = true;
                    comboBoxEditMonth.Visible = true;
                    gridControlTenderIncMonth.Visible = true;
                    gridControlTenderInc.Visible = false;
                }
                else if (radioGroupJournal.SelectedIndex == 1)
                {
                    labelControlMonth.Visible = false;
                    comboBoxEditMonth.Visible = false;
                    gridControlTenderIncMonth.Visible = false;
                    gridControlTenderInc.Visible = true;
                }
            }

            private void add()
            {
                gridViewPlan.CollapseAllDetails();
                if (gridViewPlan.GetMasterRowExpanded(gridViewPlan.FocusedRowHandle))
                    gridViewPlan.SetMasterRowExpanded(gridViewPlan.FocusedRowHandle, false);
                    TenderUpForm.Instance.showForm(1, Instance);               
            }

            private void delete()
            {
                object ret = UserDataSet.QueriesTableAdapter.can_delete_tender(HospitalUtil.getHospitalId(), gridViewPlan.GetFocusedDataRow()["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }

                bool isDeleteDoc = false;
                GridView detail = null;

                if (gridViewPlan.IsFocusedView)
                {
                    isDeleteDoc = true;
                }
                else
                {
                    if (!gridViewPlan.GetMasterRowExpanded(gridViewPlan.FocusedRowHandle))
                        gridViewPlan.SetMasterRowExpanded(gridViewPlan.FocusedRowHandle, true);
                    detail = gridViewPlan.GetDetailView(gridViewPlan.FocusedRowHandle, 0) as GridView;
                    if (detail.IsFocusedView)
                    {
                        isDeleteDoc = detail.RowCount == 1;
                    }
                }
                if (isDeleteDoc)
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Тендер устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        gridViewPlan.DeleteSelectedRows();
                        PlanDataSet.TenderJournalBindingSource.EndEdit();
                        PlanDataSet.TenderJournalTableAdapter.Update(PlanDataSet.Instance.TenderJournal);
                        ProgramUtil.closeWaitDialog();
                    }
                }
                else
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Багц устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        detail.DeleteSelectedRows();
                        PlanDataSet.TenderDetialBindingSource.EndEdit();
                        PlanDataSet.TenderDetailTableAdapter.Update(PlanDataSet.Instance.TenderDetail);
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            private void update()
            {
                if (PlanDataSet.Instance.TenderJournal.Rows.Count > 0)
                {
                    if (!gridViewPlan.GetMasterRowExpanded(gridViewPlan.FocusedRowHandle))
                        gridViewPlan.SetMasterRowExpanded(gridViewPlan.FocusedRowHandle, true);

                    TenderUpForm.Instance.showForm(2, Instance);
                }
            }

            public DataRowView getSelectedRow()
            {
                gridViewPlan.SetMasterRowExpanded(gridViewPlan.FocusedRowHandle, true);
                GridView detail = gridViewPlan.GetDetailView(gridViewPlan.FocusedRowHandle, 0) as GridView;
                string id = detail.GetFocusedDataRow()["id"].ToString();
                for (int i = 0; i < PlanDataSet.TenderDetialBindingSource.Count; i++)
                {
                    DataRowView view = (DataRowView)PlanDataSet.TenderDetialBindingSource[i];
                    if (view["id"].ToString().Equals(id))
                        return view;
                }
                return null;
            }

            public DateTime getStartDate()
            {
                return dateEditStart.DateTime;
            }

            public DateTime getEndDate()
            {
                return dateEditEnd.DateTime;
            }

            public List<DataRow> viewRows()
            {
                List<DataRow> rows = new List<DataRow>();
                if (!gridViewPlan.GetMasterRowExpanded(gridViewPlan.FocusedRowHandle))
                    gridViewPlan.SetMasterRowExpanded(gridViewPlan.FocusedRowHandle, true);
                gridViewPlan.ExpandAllGroups();
                gridViewPlan.ExpandGroupRow(gridViewPlan.FocusedRowHandle);

                GridView detail = gridViewPlan.GetDetailView(gridViewPlan.FocusedRowHandle, 0) as GridView;
                for (int i = 0; detail != null && i < detail.RowCount; i++)
                {
                    rows.Add(detail.GetDataRow(i));
                }
                return rows;
            }

            private void changeTender()
            {
                DataRowView view = (DataRowView)gridLookUpEditTender.GetSelectedDataRow();
                dateEditPlanStart.EditValue = view["startDate"];
                textEditPackage.EditValue = view["packageName"];

                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                PlanDataSet.TenderIncTableAdapter.Fill(PlanDataSet.Instance.TenderInc, HospitalUtil.getHospitalId(), view["tenderJournalID"].ToString(), view["packageID"].ToString());
                ProgramUtil.closeWaitDialog();
                if (radioGroupJournal.SelectedIndex == 0)
                    changeTenderMonth();
            }

            private void changeTenderMonth()
            {
                if (radioGroupJournal.SelectedIndex == 0)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    
                    PlanDataSet.Instance.TenderIncMonth.Clear();
                    for (int i = 0; i < PlanDataSet.Instance.TenderInc.Rows.Count; i++)
                    {
                        DataRow row = PlanDataSet.Instance.TenderIncMonth.NewRow();
                        row["medicineID"] = PlanDataSet.Instance.TenderInc.Rows[i]["medicineID"];
                        row["medicineName"] = PlanDataSet.Instance.TenderInc.Rows[i]["medicineName"];
                        row["latinName"] = PlanDataSet.Instance.TenderInc.Rows[i]["latinName"];
                        row["medicineTypeName"] = PlanDataSet.Instance.TenderInc.Rows[i]["medicineTypeName"];
                        row["unitType"] = PlanDataSet.Instance.TenderInc.Rows[i]["unitType"];
                        row["shape"] = PlanDataSet.Instance.TenderInc.Rows[i]["shape"];
                        row["quantity"] = PlanDataSet.Instance.TenderInc.Rows[i]["quantity"];
                        //row["price"] = PlanDataSet.Instance.TenderInc.Rows[i]["price"];

                        if (comboBoxEditMonth.SelectedIndex == 0)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["jan"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["janInc"];
                            row["countInc"] = PlanDataSet.Instance.TenderInc.Rows[i]["janInc"];
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 1)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["feb"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["febInc"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["janInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["febInc"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 2)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["mar"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["marInc"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["janInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["febInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["marInc"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 3)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["apr"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["aprInc"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["janInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["febInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["marInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["aprInc"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 4)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["may"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["mayInc"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["janInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["febInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["marInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["aprInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["mayInc"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 5)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["jun"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["junInc"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["janInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["febInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["marInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["aprInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["mayInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["junInc"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 6)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["jul"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["julInc"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["janInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["febInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["marInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["aprInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["mayInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["junInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["julInc"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 7)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["aug"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["augInc"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["janInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["febInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["marInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["aprInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["mayInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["junInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["julInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["augInc"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 8)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["sep"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["sepInc"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["janInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["febInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["marInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["aprInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["mayInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["junInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["julInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["augInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["sepInc"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 9)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["oct"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["octInc"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["janInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["febInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["marInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["aprInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["mayInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["junInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["julInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["augInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["sepInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["octInc"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 10)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["nov"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["novInc"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["janInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["febInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["marInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["aprInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["mayInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["junInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["julInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["augInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["sepInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["octInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["novInc"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 11)
                        {
                            row["countPlan"] = PlanDataSet.Instance.TenderInc.Rows[i]["dec"];
                            row["count"] = PlanDataSet.Instance.TenderInc.Rows[i]["decInc"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["janInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["febInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["marInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["aprInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["mayInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["junInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["julInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["augInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["sepInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["octInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["novInc"]) + Convert.ToDouble(PlanDataSet.Instance.TenderInc.Rows[i]["decInc"]);
                        }
                        
                        row["countDis"] = Convert.ToDouble(row["countPlan"]) - Convert.ToDouble(row["count"]);
                        row.EndEdit();
                        PlanDataSet.Instance.TenderIncMonth.Rows.Add(row);
                    }

                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion              

        #region Тендерийн төлөвлөгөө

            private void addPlan()
            {
                TenderPlanForm.Instance.showForm(1, Instance);
            }

            private void deletePlan()
            {
                //object ret = UserDataSet.QueriesTableAdapter.can_delete_tender(HospitalUtil.getHospitalId(), gridViewPlan.GetFocusedDataRow()["id"].ToString());
                //if (ret != DBNull.Value && ret != null)
                //{
                //    string msg = ret.ToString();
                //    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                //    return;
                //}

                bool isDeleteDoc = false;
                GridView detail = null;

                if (bandedGridViewTenderPlan.IsFocusedView)
                {
                    isDeleteDoc = true;
                }
                else
                {
                    if (!bandedGridViewTenderPlan.GetMasterRowExpanded(bandedGridViewTenderPlan.FocusedRowHandle))
                        bandedGridViewTenderPlan.SetMasterRowExpanded(bandedGridViewTenderPlan.FocusedRowHandle, true);
                    detail = bandedGridViewTenderPlan.GetDetailView(bandedGridViewTenderPlan.FocusedRowHandle, 0) as GridView;
                    if (detail.IsFocusedView)
                    {
                        isDeleteDoc = detail.RowCount == 1;
                    }
                }
                if (isDeleteDoc)
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Тендерийн төлөвлөгөө устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        bandedGridViewTenderPlan.DeleteSelectedRows();
                        PlanDataSet.TenderPlanBindingSource.EndEdit();
                        PlanDataSet.TenderPlanTableAdapter.Update(PlanDataSet.Instance.TenderPlan);
                        ProgramUtil.closeWaitDialog();
                    }
                }
                else
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Тендерийн төлөвлөгөөний эм устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        detail.DeleteSelectedRows();
                        PlanDataSet.TenderPlanDetailBindingSource.EndEdit();
                        PlanDataSet.TenderPlanDetailTableAdapter.Update(PlanDataSet.Instance.TenderPlanDetail);
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            private void updatePlan()
            {
                if (PlanDataSet.Instance.TenderPlan.Rows.Count > 0)
                {
                    if (!bandedGridViewTenderPlan.GetMasterRowExpanded(bandedGridViewTenderPlan.FocusedRowHandle))
                        bandedGridViewTenderPlan.SetMasterRowExpanded(bandedGridViewTenderPlan.FocusedRowHandle, true);

                    TenderPlanForm.Instance.showForm(2, Instance);
                }
            }

            private void setBetweenDate()
            {
                currView = (DataRowView)bandedGridViewTenderPlan.GetFocusedRow();
                if (currView != null)
                {
                    PlanDataSet.GetbetweenDateTableAdapter.Fill(PlanDataSet.Instance.GetbetweenDate, Convert.ToDateTime(currView["startDate"]), Convert.ToDateTime(currView["endDate"]));
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    dict.Add("Jan", "1-р сар");
                    dict.Add("Feb", "2-р сар");
                    dict.Add("Mar", "3-р сар");
                    dict.Add("Apr", "4-р сар");
                    dict.Add("May", "5-р сар");
                    dict.Add("Jun", "6-р сар");
                    dict.Add("Jul", "7-р сар");
                    dict.Add("Aug", "8-р сар");
                    dict.Add("Sep", "9-р сар");
                    dict.Add("Oct", "10-р сар");
                    dict.Add("Nov", "11-р сар");
                    dict.Add("Dec", "12-р сар");
                    for (int i = 0; i < PlanDataSet.Instance.GetbetweenDate.Count; i++)
                    {
                        DataRow month = PlanDataSet.Instance.GetbetweenDate[i];
                        month["ColumnDate"] = dict[month["ColumnDate"].ToString().Split(' ')[0]] + " " + month["ColumnDate"].ToString().Split(' ')[1];
                    }
                }
            }
        #endregion                

            private void button1_Click(object sender, EventArgs e)
            {
                string filename = "E:\\Batsukh\\test.xlsx";
                try
                {
                    var excelApp = new Excel.Application().Workbooks.Open(filename);
                    var worksheet = excelApp.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;
                    MessageBox.Show(Convert.ToString(worksheet.get_Range("A2", "A2").Value2));
                    
                    //excelApp.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }                


                
                //var worksheet = excelApp.workbook.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;
                //Excel._Worksheet workSheet = (Excel.Worksheet)excelApp.ActiveSheet;
                //MessageBox.Show(Convert.ToString(workSheet.Cells[1, "A"]));
                //workSheet.Cells[1, "A"] = "ID Number";
                //workSheet.Cells[1, "B"] = "Current Balance";*/
            }
    }
}