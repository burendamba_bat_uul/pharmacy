﻿namespace Pharmacy2016.gui.core.journal.tender
{
    partial class TenderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TenderForm));
            this.gridViewPlanDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlPlan = new DevExpress.XtraGrid.GridControl();
            this.gridViewPlan = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumn88 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridViewPackage = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewDetail = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridViewTenderPlanDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlTenderPlan = new DevExpress.XtraGrid.GridControl();
            this.bandedGridViewTenderPlan = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bandedGridColumn46 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn47 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn77 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bandedGridView2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn34 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn35 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn36 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn37 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn38 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn39 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn40 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn41 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn42 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn43 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn44 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageTender = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPagePlan = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageTenderInc = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlTenderIncMonth = new DevExpress.XtraGrid.GridControl();
            this.gridViewTenderIncMonth = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand29 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn69 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn70 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn71 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn72 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn73 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn74 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand31 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn76 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bandedGridColumn78 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bandedGridColumn82 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn80 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn102 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn75 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridControlTenderInc = new DevExpress.XtraGrid.GridControl();
            this.gridViewTenderInc = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn45 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn43 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn42 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn48 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn49 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn50 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn51 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn52 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn53 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn54 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn55 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn56 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn57 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn58 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn59 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn60 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn61 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn62 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand24 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn63 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn64 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand25 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn37 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn65 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand26 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn38 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn66 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand27 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn39 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn67 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand28 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn40 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn68 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControlMonth = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditMonth = new DevExpress.XtraEditors.ComboBoxEdit();
            this.radioGroupJournal = new DevExpress.XtraEditors.RadioGroup();
            this.textEditPackage = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditPlanStart = new DevExpress.XtraEditors.DateEdit();
            this.gridLookUpEditTender = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlanDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTenderPlanDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTenderPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridViewTenderPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageTender.SuspendLayout();
            this.xtraTabPagePlan.SuspendLayout();
            this.xtraTabPageTenderInc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTenderIncMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTenderIncMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTenderInc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTenderInc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMonth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupJournal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPackage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewPlanDetail
            // 
            this.gridViewPlanDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumnCount,
            this.gridColumnPrice,
            this.gridColumn5,
            this.gridColumnMedCount,
            this.gridColumn12,
            this.gridColumnSum});
            this.gridViewPlanDetail.GridControl = this.gridControlPlan;
            this.gridViewPlanDetail.GroupCount = 1;
            this.gridViewPlanDetail.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Min, "packageCount", this.gridColumnCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Min, "packagePrice", this.gridColumnPrice, "{0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "medicineCount", this.gridColumnMedCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "medicineSumPrice", this.gridColumnSum, "{0:c2}")});
            this.gridViewPlanDetail.LevelIndent = 1;
            this.gridViewPlanDetail.Name = "gridViewPlanDetail";
            this.gridViewPlanDetail.OptionsBehavior.ReadOnly = true;
            this.gridViewPlanDetail.OptionsDetail.ShowDetailTabs = false;
            this.gridViewPlanDetail.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewPlanDetail.OptionsView.ShowGroupedColumns = true;
            this.gridViewPlanDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewPlanDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewPlanDetail.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridViewPlanDetail_CustomDrawGroupRow);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Багц";
            this.gridColumn2.CustomizationCaption = "Багцын нэр";
            this.gridColumn2.FieldName = "packageName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 114;
            // 
            // gridColumnCount
            // 
            this.gridColumnCount.Caption = "Багцын тоо";
            this.gridColumnCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnCount.CustomizationCaption = "Нийт тоо ширхэг";
            this.gridColumnCount.FieldName = "packageCount";
            this.gridColumnCount.Name = "gridColumnCount";
            this.gridColumnCount.Width = 127;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnPrice
            // 
            this.gridColumnPrice.Caption = "Багцын дүн";
            this.gridColumnPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnPrice.CustomizationCaption = "Багцын дүн";
            this.gridColumnPrice.FieldName = "packagePrice";
            this.gridColumnPrice.MinWidth = 75;
            this.gridColumnPrice.Name = "gridColumnPrice";
            this.gridColumnPrice.Width = 126;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Эмийн нэр";
            this.gridColumn5.CustomizationCaption = "Эмийн нэр";
            this.gridColumn5.FieldName = "medicineName";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            this.gridColumn5.Width = 158;
            // 
            // gridColumnMedCount
            // 
            this.gridColumnMedCount.Caption = "Эмийн Тоо";
            this.gridColumnMedCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnMedCount.CustomizationCaption = "Тоо ширхэг";
            this.gridColumnMedCount.DisplayFormat.FormatString = "n2";
            this.gridColumnMedCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnMedCount.FieldName = "medicineCount";
            this.gridColumnMedCount.Name = "gridColumnMedCount";
            this.gridColumnMedCount.Visible = true;
            this.gridColumnMedCount.VisibleIndex = 2;
            this.gridColumnMedCount.Width = 149;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Эмийн үнэ";
            this.gridColumn12.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn12.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumn12.DisplayFormat.FormatString = "c2";
            this.gridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn12.FieldName = "medicinePrice";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 3;
            this.gridColumn12.Width = 131;
            // 
            // gridColumnSum
            // 
            this.gridColumnSum.Caption = "Дүн";
            this.gridColumnSum.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnSum.CustomizationCaption = "Эмийн дүн";
            this.gridColumnSum.DisplayFormat.FormatString = "c2";
            this.gridColumnSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnSum.FieldName = "medicineSumPrice";
            this.gridColumnSum.Name = "gridColumnSum";
            this.gridColumnSum.Visible = true;
            this.gridColumnSum.VisibleIndex = 4;
            this.gridColumnSum.Width = 134;
            // 
            // gridControlPlan
            // 
            this.gridControlPlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPlan.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "update")});
            this.gridControlPlan.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInc_EmbeddedNavigator_ButtonClick);
            gridLevelNode3.LevelTemplate = this.gridViewPlanDetail;
            gridLevelNode3.RelationName = "TenderJournal_TenderDetail";
            this.gridControlPlan.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.gridControlPlan.Location = new System.Drawing.Point(0, 0);
            this.gridControlPlan.MainView = this.gridViewPlan;
            this.gridControlPlan.Name = "gridControlPlan";
            this.gridControlPlan.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemSpinEdit2});
            this.gridControlPlan.Size = new System.Drawing.Size(778, 387);
            this.gridControlPlan.TabIndex = 8;
            this.gridControlPlan.UseEmbeddedNavigator = true;
            this.gridControlPlan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPlan,
            this.gridViewPackage,
            this.gridViewDetail,
            this.gridViewPlanDetail});
            // 
            // gridViewPlan
            // 
            this.gridViewPlan.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2});
            this.gridViewPlan.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn5,
            this.gridColumn3,
            this.gridColumn88});
            this.gridViewPlan.GridControl = this.gridControlPlan;
            this.gridViewPlan.LevelIndent = 0;
            this.gridViewPlan.Name = "gridViewPlan";
            this.gridViewPlan.OptionsBehavior.ReadOnly = true;
            this.gridViewPlan.OptionsDetail.ShowDetailTabs = false;
            this.gridViewPlan.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewPlan.OptionsPrint.ExpandAllDetails = true;
            this.gridViewPlan.OptionsPrint.PrintDetails = true;
            this.gridViewPlan.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPlan.OptionsView.ShowGroupPanel = false;
            this.gridViewPlan.MasterRowExpanded += new DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventHandler(this.gridViewPlan_MasterRowExpanded);
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Тендер";
            this.gridBand2.Columns.Add(this.bandedGridColumn5);
            this.gridBand2.Columns.Add(this.gridColumn3);
            this.gridBand2.Columns.Add(this.gridColumn88);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 696;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.Caption = "Гэрээний дугаар";
            this.bandedGridColumn5.CustomizationCaption = "Гэрээний дугаар";
            this.bandedGridColumn5.FieldName = "tenderNumber";
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn5.Visible = true;
            this.bandedGridColumn5.Width = 240;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Огноо";
            this.gridColumn3.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn3.CustomizationCaption = "Огноо";
            this.gridColumn3.FieldName = "date";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.Width = 254;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Бэлтгэн нийлүүлэгч";
            this.gridColumn88.CustomizationCaption = "Tендерт оролцогч";
            this.gridColumn88.FieldName = "supplierName";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.Visible = true;
            this.gridColumn88.Width = 202;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.DisplayFormat.FormatString = "c2";
            this.repositoryItemSpinEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.EditFormat.FormatString = "c2";
            this.repositoryItemSpinEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.Mask.EditMask = "c2";
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // gridViewPackage
            // 
            this.gridViewPackage.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn10,
            this.gridColumn20,
            this.gridColumn9,
            this.gridColumn19});
            this.gridViewPackage.GridControl = this.gridControlPlan;
            this.gridViewPackage.Name = "gridViewPackage";
            this.gridViewPackage.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Эмийн нэр";
            this.gridColumn1.FieldName = "medicineName";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тун хэмжээ";
            this.gridColumn6.FieldName = "unit";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Хэлбэр";
            this.gridColumn7.FieldName = "shape";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Хэмжих нэгж";
            this.gridColumn8.FieldName = "quantity";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Хүчинтэй хугацаа";
            this.gridColumn10.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn10.FieldName = "validDate";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 4;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Тоо";
            this.gridColumn20.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn20.CustomizationCaption = "Тоо хэмжээ";
            this.gridColumn20.DisplayFormat.FormatString = "n2";
            this.gridColumn20.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn20.FieldName = "count";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 5;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Үнэ";
            this.gridColumn9.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn9.CustomizationCaption = "Нэгжийг үнэ";
            this.gridColumn9.DisplayFormat.FormatString = "n2";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn9.FieldName = "price";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 6;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Дүн";
            this.gridColumn19.CustomizationCaption = "Нийт дүн";
            this.gridColumn19.DisplayFormat.FormatString = "n2";
            this.gridColumn19.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn19.FieldName = "sumPrice";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 7;
            // 
            // gridViewDetail
            // 
            this.gridViewDetail.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand5,
            this.gridBand10,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8});
            this.gridViewDetail.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn14,
            this.bandedGridColumn6,
            this.bandedGridColumn24,
            this.bandedGridColumn7,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn25,
            this.bandedGridColumn26,
            this.bandedGridColumn12,
            this.bandedGridColumn13,
            this.bandedGridColumn11,
            this.bandedGridColumn3,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.bandedGridColumnCount,
            this.bandedGridColumn19,
            this.bandedGridColumnSumPrice,
            this.bandedGridColumn21,
            this.bandedGridColumn22,
            this.bandedGridColumn27});
            this.gridViewDetail.GridControl = this.gridControlPlan;
            this.gridViewDetail.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", this.bandedGridColumnCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", this.bandedGridColumnSumPrice, "{0:c2}")});
            this.gridViewDetail.LevelIndent = 0;
            this.gridViewDetail.Name = "gridViewDetail";
            this.gridViewDetail.OptionsBehavior.ReadOnly = true;
            this.gridViewDetail.OptionsDetail.ShowDetailTabs = false;
            this.gridViewDetail.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewDetail.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDetail.OptionsView.ShowFooter = true;
            this.gridViewDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Баримт";
            this.gridBand4.Columns.Add(this.bandedGridColumn14);
            this.gridBand4.Columns.Add(this.bandedGridColumn6);
            this.gridBand4.Columns.Add(this.bandedGridColumn24);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 259;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.Caption = "Дугаар";
            this.bandedGridColumn14.CustomizationCaption = "Дугаар";
            this.bandedGridColumn14.FieldName = "incJournalID";
            this.bandedGridColumn14.MinWidth = 75;
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn14.Visible = true;
            this.bandedGridColumn14.Width = 90;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "Огноо";
            this.bandedGridColumn6.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumn6.CustomizationCaption = "Огноо";
            this.bandedGridColumn6.FieldName = "date";
            this.bandedGridColumn6.MinWidth = 75;
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn6.Visible = true;
            this.bandedGridColumn6.Width = 94;
            // 
            // bandedGridColumn24
            // 
            this.bandedGridColumn24.Caption = "Орлогын төрөл";
            this.bandedGridColumn24.CustomizationCaption = "Орлогын төрөл";
            this.bandedGridColumn24.FieldName = "incTypeName";
            this.bandedGridColumn24.MinWidth = 75;
            this.bandedGridColumn24.Name = "bandedGridColumn24";
            this.bandedGridColumn24.Visible = true;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Нярав";
            this.gridBand5.Columns.Add(this.bandedGridColumn7);
            this.gridBand5.Columns.Add(this.bandedGridColumn9);
            this.gridBand5.Columns.Add(this.bandedGridColumn10);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 1;
            this.gridBand5.Width = 278;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "Дугаар";
            this.bandedGridColumn7.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.bandedGridColumn7.FieldName = "userID";
            this.bandedGridColumn7.MinWidth = 75;
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 91;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "Нэр";
            this.bandedGridColumn9.CustomizationCaption = "Хэрэглэгчийн нэр";
            this.bandedGridColumn9.FieldName = "userName";
            this.bandedGridColumn9.MinWidth = 75;
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.Visible = true;
            this.bandedGridColumn9.Width = 91;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.Caption = "Эрх";
            this.bandedGridColumn10.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.bandedGridColumn10.FieldName = "roleName";
            this.bandedGridColumn10.MinWidth = 75;
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn10.Visible = true;
            this.bandedGridColumn10.Width = 96;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand10.AppearanceHeader.Options.UseFont = true;
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "Агуулах";
            this.gridBand10.Columns.Add(this.bandedGridColumn25);
            this.gridBand10.Columns.Add(this.bandedGridColumn26);
            this.gridBand10.CustomizationCaption = "Агуулах";
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 2;
            this.gridBand10.Width = 150;
            // 
            // bandedGridColumn25
            // 
            this.bandedGridColumn25.Caption = "Нэр";
            this.bandedGridColumn25.CustomizationCaption = "Агуулахын нэр";
            this.bandedGridColumn25.FieldName = "warehouseName";
            this.bandedGridColumn25.MinWidth = 75;
            this.bandedGridColumn25.Name = "bandedGridColumn25";
            this.bandedGridColumn25.Visible = true;
            // 
            // bandedGridColumn26
            // 
            this.bandedGridColumn26.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn26.CustomizationCaption = "Агуулахын дэлгэрэнгүй";
            this.bandedGridColumn26.FieldName = "warehouseDesc";
            this.bandedGridColumn26.MinWidth = 75;
            this.bandedGridColumn26.Name = "bandedGridColumn26";
            this.bandedGridColumn26.Visible = true;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand6.AppearanceHeader.Options.UseFont = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Нэмэлт мэдээлэл";
            this.gridBand6.Columns.Add(this.bandedGridColumn12);
            this.gridBand6.Columns.Add(this.bandedGridColumn13);
            this.gridBand6.Columns.Add(this.bandedGridColumn11);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 3;
            this.gridBand6.Width = 281;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.Caption = "Бэлтгэн нийлүүлэгч";
            this.bandedGridColumn12.CustomizationCaption = "Бэлтгэн нийлүүлэгч";
            this.bandedGridColumn12.FieldName = "producerName";
            this.bandedGridColumn12.MinWidth = 75;
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.Visible = true;
            this.bandedGridColumn12.Width = 87;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.Caption = "Дагалдах баримт";
            this.bandedGridColumn13.CustomizationCaption = "Дагалдах баримт";
            this.bandedGridColumn13.FieldName = "docNumber";
            this.bandedGridColumn13.MinWidth = 75;
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.Visible = true;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.Caption = "Тайлбар";
            this.bandedGridColumn11.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn11.FieldName = "description";
            this.bandedGridColumn11.MinWidth = 75;
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn11.Visible = true;
            this.bandedGridColumn11.Width = 119;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand7.AppearanceHeader.Options.UseFont = true;
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "Эмийн мэдээлэл";
            this.gridBand7.Columns.Add(this.bandedGridColumn3);
            this.gridBand7.Columns.Add(this.bandedGridColumn15);
            this.gridBand7.Columns.Add(this.bandedGridColumn16);
            this.gridBand7.Columns.Add(this.bandedGridColumn22);
            this.gridBand7.Columns.Add(this.bandedGridColumn21);
            this.gridBand7.Columns.Add(this.bandedGridColumn17);
            this.gridBand7.Columns.Add(this.bandedGridColumn27);
            this.gridBand7.CustomizationCaption = "Эмийн мэдээлэл";
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 4;
            this.gridBand7.Width = 525;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.Caption = "Эмийн нэр";
            this.bandedGridColumn3.CustomizationCaption = "Худалдааны нэр";
            this.bandedGridColumn3.FieldName = "medicineName";
            this.bandedGridColumn3.MinWidth = 75;
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.Visible = true;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.Caption = "ОУ нэр";
            this.bandedGridColumn15.CustomizationCaption = "Олон улсын нэр";
            this.bandedGridColumn15.FieldName = "latinName";
            this.bandedGridColumn15.MinWidth = 75;
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.Visible = true;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.Caption = "Ангилал";
            this.bandedGridColumn16.CustomizationCaption = "Эмийн ангилал";
            this.bandedGridColumn16.FieldName = "medicineTypeName";
            this.bandedGridColumn16.MinWidth = 75;
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.Visible = true;
            // 
            // bandedGridColumn22
            // 
            this.bandedGridColumn22.Caption = "Хүчинтэй хугацаа";
            this.bandedGridColumn22.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumn22.CustomizationCaption = "Хүчинтэй хугацаа";
            this.bandedGridColumn22.FieldName = "validDate";
            this.bandedGridColumn22.MinWidth = 75;
            this.bandedGridColumn22.Name = "bandedGridColumn22";
            this.bandedGridColumn22.Visible = true;
            // 
            // bandedGridColumn21
            // 
            this.bandedGridColumn21.Caption = "Хэлбэр";
            this.bandedGridColumn21.CustomizationCaption = "Хэлбэр";
            this.bandedGridColumn21.FieldName = "shape";
            this.bandedGridColumn21.MinWidth = 75;
            this.bandedGridColumn21.Name = "bandedGridColumn21";
            this.bandedGridColumn21.Visible = true;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.Caption = "Тун хэмжээ";
            this.bandedGridColumn17.CustomizationCaption = "Тун хэмжээ";
            this.bandedGridColumn17.FieldName = "unitType";
            this.bandedGridColumn17.MinWidth = 75;
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.Visible = true;
            // 
            // bandedGridColumn27
            // 
            this.bandedGridColumn27.Caption = "Хэмжих нэгж";
            this.bandedGridColumn27.CustomizationCaption = "Хэмжих нэгж";
            this.bandedGridColumn27.FieldName = "quantity";
            this.bandedGridColumn27.MinWidth = 75;
            this.bandedGridColumn27.Name = "bandedGridColumn27";
            this.bandedGridColumn27.Visible = true;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "Дүн";
            this.gridBand8.Columns.Add(this.bandedGridColumnCount);
            this.gridBand8.Columns.Add(this.bandedGridColumn19);
            this.gridBand8.Columns.Add(this.bandedGridColumnSumPrice);
            this.gridBand8.CustomizationCaption = "Дүн";
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 5;
            this.gridBand8.Width = 225;
            // 
            // bandedGridColumnCount
            // 
            this.bandedGridColumnCount.Caption = "Тоо";
            this.bandedGridColumnCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnCount.CustomizationCaption = "Тоо";
            this.bandedGridColumnCount.FieldName = "count";
            this.bandedGridColumnCount.MinWidth = 75;
            this.bandedGridColumnCount.Name = "bandedGridColumnCount";
            this.bandedGridColumnCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.bandedGridColumnCount.Visible = true;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.Caption = "Нэгжийн үнэ";
            this.bandedGridColumn19.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn19.CustomizationCaption = "Нэгжийн үнэ";
            this.bandedGridColumn19.FieldName = "price";
            this.bandedGridColumn19.MinWidth = 75;
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.Visible = true;
            // 
            // bandedGridColumnSumPrice
            // 
            this.bandedGridColumnSumPrice.Caption = "Дүн";
            this.bandedGridColumnSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnSumPrice.CustomizationCaption = "Дүн";
            this.bandedGridColumnSumPrice.FieldName = "sumPrice";
            this.bandedGridColumnSumPrice.MinWidth = 75;
            this.bandedGridColumnSumPrice.Name = "bandedGridColumnSumPrice";
            this.bandedGridColumnSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.bandedGridColumnSumPrice.Visible = true;
            // 
            // gridViewTenderPlanDetail
            // 
            this.gridViewTenderPlanDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn11,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn36,
            this.gridColumn35,
            this.gridColumn18});
            this.gridViewTenderPlanDetail.GridControl = this.gridControlTenderPlan;
            this.gridViewTenderPlanDetail.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "medicineCount", this.gridColumn16, "{0:n2}")});
            this.gridViewTenderPlanDetail.LevelIndent = 1;
            this.gridViewTenderPlanDetail.Name = "gridViewTenderPlanDetail";
            this.gridViewTenderPlanDetail.OptionsBehavior.ReadOnly = true;
            this.gridViewTenderPlanDetail.OptionsDetail.ShowDetailTabs = false;
            this.gridViewTenderPlanDetail.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewTenderPlanDetail.OptionsView.ShowFooter = true;
            this.gridViewTenderPlanDetail.OptionsView.ShowGroupedColumns = true;
            this.gridViewTenderPlanDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Эмийн нэр";
            this.gridColumn15.CustomizationCaption = "Эмийн нэр";
            this.gridColumn15.FieldName = "medicineName";
            this.gridColumn15.MinWidth = 100;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 127;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Жилд авах багцын тоо";
            this.gridColumn16.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn16.CustomizationCaption = "Жилийн тоо";
            this.gridColumn16.DisplayFormat.FormatString = "n2";
            this.gridColumn16.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn16.FieldName = "count";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 1;
            this.gridColumn16.Width = 127;
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit3.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit3.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit3.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit3.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "1-р сар";
            this.gridColumn17.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn17.CustomizationCaption = "1-р сар";
            this.gridColumn17.FieldName = "jan";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jan", "{0:n2}")});
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 2;
            this.gridColumn17.Width = 130;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "2-р сар";
            this.gridColumn11.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn11.CustomizationCaption = "2-р сар";
            this.gridColumn11.FieldName = "feb";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "feb", "{0:n2}")});
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "3-р сар";
            this.gridColumn13.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn13.CustomizationCaption = "3-р сар";
            this.gridColumn13.FieldName = "mar";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "mar", "{0:n2}")});
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 4;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "4-р сар";
            this.gridColumn14.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn14.CustomizationCaption = "4-р сар";
            this.gridColumn14.FieldName = "apr";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "apr", "{0:n2}")});
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 5;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "5-р сар";
            this.gridColumn29.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn29.CustomizationCaption = "5-р сар";
            this.gridColumn29.FieldName = "may";
            this.gridColumn29.MinWidth = 75;
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "may", "{0:n2}")});
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 6;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "6-р сар";
            this.gridColumn30.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn30.CustomizationCaption = "6-р сар";
            this.gridColumn30.FieldName = "jun";
            this.gridColumn30.MinWidth = 75;
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jun", "{0:n2}")});
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 7;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "7-р сар";
            this.gridColumn31.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn31.CustomizationCaption = "7-р сар";
            this.gridColumn31.FieldName = "jul";
            this.gridColumn31.MinWidth = 75;
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jul", "{0:n2}")});
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 8;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "8-р сар";
            this.gridColumn32.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn32.CustomizationCaption = "8-р сар";
            this.gridColumn32.FieldName = "aug";
            this.gridColumn32.MinWidth = 75;
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "aug", "{0:n2}")});
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 9;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "9-р сар";
            this.gridColumn33.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn33.CustomizationCaption = "9-р сар";
            this.gridColumn33.FieldName = "sep";
            this.gridColumn33.MinWidth = 75;
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sep", "{0:n2}")});
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 10;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "10-р сар";
            this.gridColumn34.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn34.CustomizationCaption = "10-р сар";
            this.gridColumn34.FieldName = "oct";
            this.gridColumn34.MinWidth = 75;
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "oct", "{0:n2}")});
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 11;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "11-р сар";
            this.gridColumn36.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn36.CustomizationCaption = "11-р сар";
            this.gridColumn36.FieldName = "nov";
            this.gridColumn36.MinWidth = 75;
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "nov", "{0:n2}")});
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 12;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "12-р сар";
            this.gridColumn35.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn35.CustomizationCaption = "12-р сар";
            this.gridColumn35.FieldName = "dec";
            this.gridColumn35.MinWidth = 75;
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "dec", "{0:n2}")});
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 13;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Дүн";
            this.gridColumn18.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn18.CustomizationCaption = "Эмийн дүн";
            this.gridColumn18.FieldName = "medicineSumPrice";
            this.gridColumn18.MinWidth = 100;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Width = 100;
            // 
            // gridControlTenderPlan
            // 
            this.gridControlTenderPlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTenderPlan.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlTenderPlan.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlTenderPlan.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlTenderPlan.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlTenderPlan.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlTenderPlan.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "update")});
            this.gridControlTenderPlan.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlTenderPlan_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.LevelTemplate = this.gridViewTenderPlanDetail;
            gridLevelNode1.RelationName = "TenderPlan_TenderPlanDetail";
            this.gridControlTenderPlan.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlTenderPlan.Location = new System.Drawing.Point(0, 0);
            this.gridControlTenderPlan.MainView = this.bandedGridViewTenderPlan;
            this.gridControlTenderPlan.Name = "gridControlTenderPlan";
            this.gridControlTenderPlan.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit2,
            this.repositoryItemSpinEdit3,
            this.repositoryItemSpinEdit4});
            this.gridControlTenderPlan.Size = new System.Drawing.Size(778, 387);
            this.gridControlTenderPlan.TabIndex = 9;
            this.gridControlTenderPlan.UseEmbeddedNavigator = true;
            this.gridControlTenderPlan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridViewTenderPlan,
            this.gridView2,
            this.bandedGridView2,
            this.gridViewTenderPlanDetail});
            this.gridControlTenderPlan.ViewRegistered += new DevExpress.XtraGrid.ViewOperationEventHandler(this.gridControlTenderPlan_ViewRegistered);
            // 
            // bandedGridViewTenderPlan
            // 
            this.bandedGridViewTenderPlan.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.bandedGridViewTenderPlan.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn1,
            this.bandedGridColumn2,
            this.bandedGridColumn77,
            this.bandedGridColumn4,
            this.bandedGridColumn46,
            this.bandedGridColumn47});
            this.bandedGridViewTenderPlan.GridControl = this.gridControlTenderPlan;
            this.bandedGridViewTenderPlan.LevelIndent = 0;
            this.bandedGridViewTenderPlan.Name = "bandedGridViewTenderPlan";
            this.bandedGridViewTenderPlan.OptionsBehavior.ReadOnly = true;
            this.bandedGridViewTenderPlan.OptionsDetail.ShowDetailTabs = false;
            this.bandedGridViewTenderPlan.OptionsNavigation.AutoFocusNewRow = true;
            this.bandedGridViewTenderPlan.OptionsPrint.ExpandAllDetails = true;
            this.bandedGridViewTenderPlan.OptionsPrint.PrintDetails = true;
            this.bandedGridViewTenderPlan.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridViewTenderPlan.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Тендер төлөвлөгөө";
            this.gridBand1.Columns.Add(this.bandedGridColumn1);
            this.gridBand1.Columns.Add(this.bandedGridColumn4);
            this.gridBand1.Columns.Add(this.bandedGridColumn2);
            this.gridBand1.Columns.Add(this.bandedGridColumn46);
            this.gridBand1.Columns.Add(this.bandedGridColumn47);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 601;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "Тендер";
            this.bandedGridColumn1.CustomizationCaption = "Тендер";
            this.bandedGridColumn1.FieldName = "tenderNumber";
            this.bandedGridColumn1.MinWidth = 75;
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 143;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "Бэлтгэн нийлүүлэгч";
            this.bandedGridColumn4.CustomizationCaption = "Бэлтгэн нийлүүлэгч";
            this.bandedGridColumn4.FieldName = "supplierName";
            this.bandedGridColumn4.MinWidth = 75;
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 128;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "Эхлэх огноо";
            this.bandedGridColumn2.ColumnEdit = this.repositoryItemDateEdit2;
            this.bandedGridColumn2.CustomizationCaption = "Эхлэх огноо";
            this.bandedGridColumn2.FieldName = "startDate";
            this.bandedGridColumn2.MinWidth = 75;
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn2.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 89;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // bandedGridColumn46
            // 
            this.bandedGridColumn46.Caption = "Багцын нэр";
            this.bandedGridColumn46.CustomizationCaption = "Багцын нэр";
            this.bandedGridColumn46.FieldName = "packageName";
            this.bandedGridColumn46.MinWidth = 75;
            this.bandedGridColumn46.Name = "bandedGridColumn46";
            this.bandedGridColumn46.Visible = true;
            this.bandedGridColumn46.Width = 107;
            // 
            // bandedGridColumn47
            // 
            this.bandedGridColumn47.Caption = "Тайлбар";
            this.bandedGridColumn47.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn47.FieldName = "description";
            this.bandedGridColumn47.MinWidth = 75;
            this.bandedGridColumn47.Name = "bandedGridColumn47";
            this.bandedGridColumn47.Visible = true;
            this.bandedGridColumn47.Width = 134;
            // 
            // bandedGridColumn77
            // 
            this.bandedGridColumn77.Caption = "Дуусах огноо";
            this.bandedGridColumn77.ColumnEdit = this.repositoryItemDateEdit2;
            this.bandedGridColumn77.CustomizationCaption = "Дуусах огноо";
            this.bandedGridColumn77.FieldName = "endDate";
            this.bandedGridColumn77.Name = "bandedGridColumn77";
            this.bandedGridColumn77.Visible = true;
            // 
            // repositoryItemSpinEdit4
            // 
            this.repositoryItemSpinEdit4.AutoHeight = false;
            this.repositoryItemSpinEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit4.DisplayFormat.FormatString = "c2";
            this.repositoryItemSpinEdit4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit4.EditFormat.FormatString = "c2";
            this.repositoryItemSpinEdit4.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit4.Mask.EditMask = "c2";
            this.repositoryItemSpinEdit4.Name = "repositoryItemSpinEdit4";
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28});
            this.gridView2.GridControl = this.gridControlTenderPlan;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Эмийн нэр";
            this.gridColumn21.FieldName = "medicineName";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 0;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Тун хэмжээ";
            this.gridColumn22.FieldName = "unit";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 1;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Хэлбэр";
            this.gridColumn23.FieldName = "shape";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 2;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Хэмжих нэгж";
            this.gridColumn24.FieldName = "quantity";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 3;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Хүчинтэй хугацаа";
            this.gridColumn25.ColumnEdit = this.repositoryItemDateEdit2;
            this.gridColumn25.FieldName = "validDate";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 4;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Тоо";
            this.gridColumn26.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn26.CustomizationCaption = "Тоо хэмжээ";
            this.gridColumn26.DisplayFormat.FormatString = "n2";
            this.gridColumn26.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn26.FieldName = "count";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 5;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Үнэ";
            this.gridColumn27.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn27.CustomizationCaption = "Нэгжийг үнэ";
            this.gridColumn27.DisplayFormat.FormatString = "n2";
            this.gridColumn27.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn27.FieldName = "price";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 6;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Дүн";
            this.gridColumn28.CustomizationCaption = "Нийт дүн";
            this.gridColumn28.DisplayFormat.FormatString = "n2";
            this.gridColumn28.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn28.FieldName = "sumPrice";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 7;
            // 
            // bandedGridView2
            // 
            this.bandedGridView2.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3,
            this.gridBand9,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13,
            this.gridBand14});
            this.bandedGridView2.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn8,
            this.bandedGridColumn18,
            this.bandedGridColumn20,
            this.bandedGridColumn23,
            this.bandedGridColumn28,
            this.bandedGridColumn29,
            this.bandedGridColumn30,
            this.bandedGridColumn31,
            this.bandedGridColumn32,
            this.bandedGridColumn33,
            this.bandedGridColumn34,
            this.bandedGridColumn35,
            this.bandedGridColumn36,
            this.bandedGridColumn37,
            this.bandedGridColumn40,
            this.bandedGridColumn42,
            this.bandedGridColumn43,
            this.bandedGridColumn44,
            this.bandedGridColumn39,
            this.bandedGridColumn38,
            this.bandedGridColumn41});
            this.bandedGridView2.GridControl = this.gridControlTenderPlan;
            this.bandedGridView2.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", this.bandedGridColumn42, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", this.bandedGridColumn44, "{0:c2}")});
            this.bandedGridView2.LevelIndent = 0;
            this.bandedGridView2.Name = "bandedGridView2";
            this.bandedGridView2.OptionsBehavior.ReadOnly = true;
            this.bandedGridView2.OptionsDetail.ShowDetailTabs = false;
            this.bandedGridView2.OptionsNavigation.AutoFocusNewRow = true;
            this.bandedGridView2.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridView2.OptionsView.ShowFooter = true;
            this.bandedGridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Баримт";
            this.gridBand3.Columns.Add(this.bandedGridColumn8);
            this.gridBand3.Columns.Add(this.bandedGridColumn18);
            this.gridBand3.Columns.Add(this.bandedGridColumn20);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 0;
            this.gridBand3.Width = 259;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.Caption = "Дугаар";
            this.bandedGridColumn8.CustomizationCaption = "Дугаар";
            this.bandedGridColumn8.FieldName = "incJournalID";
            this.bandedGridColumn8.MinWidth = 75;
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn8.Visible = true;
            this.bandedGridColumn8.Width = 90;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.Caption = "Огноо";
            this.bandedGridColumn18.ColumnEdit = this.repositoryItemDateEdit2;
            this.bandedGridColumn18.CustomizationCaption = "Огноо";
            this.bandedGridColumn18.FieldName = "date";
            this.bandedGridColumn18.MinWidth = 75;
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            this.bandedGridColumn18.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn18.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn18.Visible = true;
            this.bandedGridColumn18.Width = 94;
            // 
            // bandedGridColumn20
            // 
            this.bandedGridColumn20.Caption = "Орлогын төрөл";
            this.bandedGridColumn20.CustomizationCaption = "Орлогын төрөл";
            this.bandedGridColumn20.FieldName = "incTypeName";
            this.bandedGridColumn20.MinWidth = 75;
            this.bandedGridColumn20.Name = "bandedGridColumn20";
            this.bandedGridColumn20.Visible = true;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand9.AppearanceHeader.Options.UseFont = true;
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "Нярав";
            this.gridBand9.Columns.Add(this.bandedGridColumn23);
            this.gridBand9.Columns.Add(this.bandedGridColumn28);
            this.gridBand9.Columns.Add(this.bandedGridColumn29);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 1;
            this.gridBand9.Width = 278;
            // 
            // bandedGridColumn23
            // 
            this.bandedGridColumn23.Caption = "Дугаар";
            this.bandedGridColumn23.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.bandedGridColumn23.FieldName = "userID";
            this.bandedGridColumn23.MinWidth = 75;
            this.bandedGridColumn23.Name = "bandedGridColumn23";
            this.bandedGridColumn23.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn23.Visible = true;
            this.bandedGridColumn23.Width = 91;
            // 
            // bandedGridColumn28
            // 
            this.bandedGridColumn28.Caption = "Нэр";
            this.bandedGridColumn28.CustomizationCaption = "Хэрэглэгчийн нэр";
            this.bandedGridColumn28.FieldName = "userName";
            this.bandedGridColumn28.MinWidth = 75;
            this.bandedGridColumn28.Name = "bandedGridColumn28";
            this.bandedGridColumn28.Visible = true;
            this.bandedGridColumn28.Width = 91;
            // 
            // bandedGridColumn29
            // 
            this.bandedGridColumn29.Caption = "Эрх";
            this.bandedGridColumn29.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.bandedGridColumn29.FieldName = "roleName";
            this.bandedGridColumn29.MinWidth = 75;
            this.bandedGridColumn29.Name = "bandedGridColumn29";
            this.bandedGridColumn29.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn29.Visible = true;
            this.bandedGridColumn29.Width = 96;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand11.AppearanceHeader.Options.UseFont = true;
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "Агуулах";
            this.gridBand11.Columns.Add(this.bandedGridColumn30);
            this.gridBand11.Columns.Add(this.bandedGridColumn31);
            this.gridBand11.CustomizationCaption = "Агуулах";
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 2;
            this.gridBand11.Width = 150;
            // 
            // bandedGridColumn30
            // 
            this.bandedGridColumn30.Caption = "Нэр";
            this.bandedGridColumn30.CustomizationCaption = "Агуулахын нэр";
            this.bandedGridColumn30.FieldName = "warehouseName";
            this.bandedGridColumn30.MinWidth = 75;
            this.bandedGridColumn30.Name = "bandedGridColumn30";
            this.bandedGridColumn30.Visible = true;
            // 
            // bandedGridColumn31
            // 
            this.bandedGridColumn31.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn31.CustomizationCaption = "Агуулахын дэлгэрэнгүй";
            this.bandedGridColumn31.FieldName = "warehouseDesc";
            this.bandedGridColumn31.MinWidth = 75;
            this.bandedGridColumn31.Name = "bandedGridColumn31";
            this.bandedGridColumn31.Visible = true;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand12.AppearanceHeader.Options.UseFont = true;
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "Нэмэлт мэдээлэл";
            this.gridBand12.Columns.Add(this.bandedGridColumn32);
            this.gridBand12.Columns.Add(this.bandedGridColumn33);
            this.gridBand12.Columns.Add(this.bandedGridColumn34);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 3;
            this.gridBand12.Width = 281;
            // 
            // bandedGridColumn32
            // 
            this.bandedGridColumn32.Caption = "Бэлтгэн нийлүүлэгч";
            this.bandedGridColumn32.CustomizationCaption = "Бэлтгэн нийлүүлэгч";
            this.bandedGridColumn32.FieldName = "producerName";
            this.bandedGridColumn32.MinWidth = 75;
            this.bandedGridColumn32.Name = "bandedGridColumn32";
            this.bandedGridColumn32.Visible = true;
            this.bandedGridColumn32.Width = 87;
            // 
            // bandedGridColumn33
            // 
            this.bandedGridColumn33.Caption = "Дагалдах баримт";
            this.bandedGridColumn33.CustomizationCaption = "Дагалдах баримт";
            this.bandedGridColumn33.FieldName = "docNumber";
            this.bandedGridColumn33.MinWidth = 75;
            this.bandedGridColumn33.Name = "bandedGridColumn33";
            this.bandedGridColumn33.Visible = true;
            // 
            // bandedGridColumn34
            // 
            this.bandedGridColumn34.Caption = "Тайлбар";
            this.bandedGridColumn34.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn34.FieldName = "description";
            this.bandedGridColumn34.MinWidth = 75;
            this.bandedGridColumn34.Name = "bandedGridColumn34";
            this.bandedGridColumn34.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn34.Visible = true;
            this.bandedGridColumn34.Width = 119;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand13.AppearanceHeader.Options.UseFont = true;
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "Эмийн мэдээлэл";
            this.gridBand13.Columns.Add(this.bandedGridColumn35);
            this.gridBand13.Columns.Add(this.bandedGridColumn36);
            this.gridBand13.Columns.Add(this.bandedGridColumn37);
            this.gridBand13.Columns.Add(this.bandedGridColumn38);
            this.gridBand13.Columns.Add(this.bandedGridColumn39);
            this.gridBand13.Columns.Add(this.bandedGridColumn40);
            this.gridBand13.Columns.Add(this.bandedGridColumn41);
            this.gridBand13.CustomizationCaption = "Эмийн мэдээлэл";
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 4;
            this.gridBand13.Width = 525;
            // 
            // bandedGridColumn35
            // 
            this.bandedGridColumn35.Caption = "Эмийн нэр";
            this.bandedGridColumn35.CustomizationCaption = "Худалдааны нэр";
            this.bandedGridColumn35.FieldName = "medicineName";
            this.bandedGridColumn35.MinWidth = 75;
            this.bandedGridColumn35.Name = "bandedGridColumn35";
            this.bandedGridColumn35.Visible = true;
            // 
            // bandedGridColumn36
            // 
            this.bandedGridColumn36.Caption = "ОУ нэр";
            this.bandedGridColumn36.CustomizationCaption = "Олон улсын нэр";
            this.bandedGridColumn36.FieldName = "latinName";
            this.bandedGridColumn36.MinWidth = 75;
            this.bandedGridColumn36.Name = "bandedGridColumn36";
            this.bandedGridColumn36.Visible = true;
            // 
            // bandedGridColumn37
            // 
            this.bandedGridColumn37.Caption = "Ангилал";
            this.bandedGridColumn37.CustomizationCaption = "Эмийн ангилал";
            this.bandedGridColumn37.FieldName = "medicineTypeName";
            this.bandedGridColumn37.MinWidth = 75;
            this.bandedGridColumn37.Name = "bandedGridColumn37";
            this.bandedGridColumn37.Visible = true;
            // 
            // bandedGridColumn38
            // 
            this.bandedGridColumn38.Caption = "Хүчинтэй хугацаа";
            this.bandedGridColumn38.ColumnEdit = this.repositoryItemDateEdit2;
            this.bandedGridColumn38.CustomizationCaption = "Хүчинтэй хугацаа";
            this.bandedGridColumn38.FieldName = "validDate";
            this.bandedGridColumn38.MinWidth = 75;
            this.bandedGridColumn38.Name = "bandedGridColumn38";
            this.bandedGridColumn38.Visible = true;
            // 
            // bandedGridColumn39
            // 
            this.bandedGridColumn39.Caption = "Хэлбэр";
            this.bandedGridColumn39.CustomizationCaption = "Хэлбэр";
            this.bandedGridColumn39.FieldName = "shape";
            this.bandedGridColumn39.MinWidth = 75;
            this.bandedGridColumn39.Name = "bandedGridColumn39";
            this.bandedGridColumn39.Visible = true;
            // 
            // bandedGridColumn40
            // 
            this.bandedGridColumn40.Caption = "Тун хэмжээ";
            this.bandedGridColumn40.CustomizationCaption = "Тун хэмжээ";
            this.bandedGridColumn40.FieldName = "unitType";
            this.bandedGridColumn40.MinWidth = 75;
            this.bandedGridColumn40.Name = "bandedGridColumn40";
            this.bandedGridColumn40.Visible = true;
            // 
            // bandedGridColumn41
            // 
            this.bandedGridColumn41.Caption = "Хэмжих нэгж";
            this.bandedGridColumn41.CustomizationCaption = "Хэмжих нэгж";
            this.bandedGridColumn41.FieldName = "quantity";
            this.bandedGridColumn41.MinWidth = 75;
            this.bandedGridColumn41.Name = "bandedGridColumn41";
            this.bandedGridColumn41.Visible = true;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand14.AppearanceHeader.Options.UseFont = true;
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "Дүн";
            this.gridBand14.Columns.Add(this.bandedGridColumn42);
            this.gridBand14.Columns.Add(this.bandedGridColumn43);
            this.gridBand14.Columns.Add(this.bandedGridColumn44);
            this.gridBand14.CustomizationCaption = "Дүн";
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 5;
            this.gridBand14.Width = 225;
            // 
            // bandedGridColumn42
            // 
            this.bandedGridColumn42.Caption = "Тоо";
            this.bandedGridColumn42.ColumnEdit = this.repositoryItemSpinEdit3;
            this.bandedGridColumn42.CustomizationCaption = "Тоо";
            this.bandedGridColumn42.FieldName = "count";
            this.bandedGridColumn42.MinWidth = 75;
            this.bandedGridColumn42.Name = "bandedGridColumn42";
            this.bandedGridColumn42.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.bandedGridColumn42.Visible = true;
            // 
            // bandedGridColumn43
            // 
            this.bandedGridColumn43.Caption = "Нэгжийн үнэ";
            this.bandedGridColumn43.ColumnEdit = this.repositoryItemSpinEdit3;
            this.bandedGridColumn43.CustomizationCaption = "Нэгжийн үнэ";
            this.bandedGridColumn43.FieldName = "price";
            this.bandedGridColumn43.MinWidth = 75;
            this.bandedGridColumn43.Name = "bandedGridColumn43";
            this.bandedGridColumn43.Visible = true;
            // 
            // bandedGridColumn44
            // 
            this.bandedGridColumn44.Caption = "Дүн";
            this.bandedGridColumn44.ColumnEdit = this.repositoryItemSpinEdit3;
            this.bandedGridColumn44.CustomizationCaption = "Дүн";
            this.bandedGridColumn44.FieldName = "sumPrice";
            this.bandedGridColumn44.MinWidth = 75;
            this.bandedGridColumn44.Name = "bandedGridColumn44";
            this.bandedGridColumn44.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.bandedGridColumn44.Visible = true;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.button1);
            this.panelControl2.Controls.Add(this.simpleButtonLayout);
            this.panelControl2.Controls.Add(this.dropDownButtonPrint);
            this.panelControl2.Controls.Add(this.dateEditEnd);
            this.panelControl2.Controls.Add(this.dateEditStart);
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(784, 46);
            this.panelControl2.TabIndex = 2;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(751, 10);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 8;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(615, 10);
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.dropDownButtonPrint.Size = new System.Drawing.Size(130, 23);
            this.dropDownButtonPrint.TabIndex = 6;
            this.dropDownButtonPrint.Text = "Хэвлэх";
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(116, 12);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(100, 20);
            this.dateEditEnd.TabIndex = 3;
            this.dateEditEnd.ToolTip = "Дуусах огноо";
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(10, 12);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(100, 20);
            this.dateEditStart.TabIndex = 2;
            this.dateEditStart.ToolTip = "Эхлэх огноо";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(222, 10);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonReload.TabIndex = 4;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Орлогын журналыг шүүх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 46);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageTender;
            this.xtraTabControl1.Size = new System.Drawing.Size(784, 415);
            this.xtraTabControl1.TabIndex = 9;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageTender,
            this.xtraTabPagePlan,
            this.xtraTabPageTenderInc});
            // 
            // xtraTabPageTender
            // 
            this.xtraTabPageTender.Controls.Add(this.gridControlPlan);
            this.xtraTabPageTender.Name = "xtraTabPageTender";
            this.xtraTabPageTender.Size = new System.Drawing.Size(778, 387);
            this.xtraTabPageTender.Text = "Тендер";
            // 
            // xtraTabPagePlan
            // 
            this.xtraTabPagePlan.Controls.Add(this.gridControlTenderPlan);
            this.xtraTabPagePlan.Name = "xtraTabPagePlan";
            this.xtraTabPagePlan.Size = new System.Drawing.Size(778, 387);
            this.xtraTabPagePlan.Text = "Тендер төлөвлөгөө";
            // 
            // xtraTabPageTenderInc
            // 
            this.xtraTabPageTenderInc.Controls.Add(this.gridControlTenderIncMonth);
            this.xtraTabPageTenderInc.Controls.Add(this.gridControlTenderInc);
            this.xtraTabPageTenderInc.Controls.Add(this.panelControl1);
            this.xtraTabPageTenderInc.Name = "xtraTabPageTenderInc";
            this.xtraTabPageTenderInc.Size = new System.Drawing.Size(778, 387);
            this.xtraTabPageTenderInc.Text = "Төлөвлөгөөний гүйцэтгэл";
            // 
            // gridControlTenderIncMonth
            // 
            this.gridControlTenderIncMonth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTenderIncMonth.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlTenderIncMonth.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlTenderIncMonth.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlTenderIncMonth.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlTenderIncMonth.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlTenderIncMonth.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInc_EmbeddedNavigator_ButtonClick);
            this.gridControlTenderIncMonth.Location = new System.Drawing.Point(0, 72);
            this.gridControlTenderIncMonth.MainView = this.gridViewTenderIncMonth;
            this.gridControlTenderIncMonth.Name = "gridControlTenderIncMonth";
            this.gridControlTenderIncMonth.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit7,
            this.repositoryItemSpinEdit8});
            this.gridControlTenderIncMonth.Size = new System.Drawing.Size(778, 315);
            this.gridControlTenderIncMonth.TabIndex = 10;
            this.gridControlTenderIncMonth.UseEmbeddedNavigator = true;
            this.gridControlTenderIncMonth.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTenderIncMonth});
            // 
            // gridViewTenderIncMonth
            // 
            this.gridViewTenderIncMonth.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand29,
            this.gridBand31});
            this.gridViewTenderIncMonth.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn69,
            this.bandedGridColumn102,
            this.bandedGridColumn75,
            this.bandedGridColumn70,
            this.bandedGridColumn71,
            this.bandedGridColumn72,
            this.bandedGridColumn73,
            this.bandedGridColumn74,
            this.bandedGridColumn76,
            this.bandedGridColumn78,
            this.bandedGridColumn80,
            this.bandedGridColumn82});
            this.gridViewTenderIncMonth.GridControl = this.gridControlTenderIncMonth;
            this.gridViewTenderIncMonth.LevelIndent = 1;
            this.gridViewTenderIncMonth.Name = "gridViewTenderIncMonth";
            this.gridViewTenderIncMonth.OptionsBehavior.ReadOnly = true;
            this.gridViewTenderIncMonth.OptionsDetail.ShowDetailTabs = false;
            this.gridViewTenderIncMonth.OptionsPrint.ExpandAllDetails = true;
            this.gridViewTenderIncMonth.OptionsPrint.PrintDetails = true;
            this.gridViewTenderIncMonth.OptionsView.ShowAutoFilterRow = true;
            this.gridViewTenderIncMonth.OptionsView.ShowFooter = true;
            this.gridViewTenderIncMonth.OptionsView.ShowGroupPanel = false;
            this.gridViewTenderIncMonth.PreviewFieldName = "preRow";
            // 
            // gridBand29
            // 
            this.gridBand29.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand29.AppearanceHeader.Options.UseFont = true;
            this.gridBand29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand29.Caption = "Эм";
            this.gridBand29.Columns.Add(this.bandedGridColumn69);
            this.gridBand29.Columns.Add(this.bandedGridColumn70);
            this.gridBand29.Columns.Add(this.bandedGridColumn71);
            this.gridBand29.Columns.Add(this.bandedGridColumn72);
            this.gridBand29.Columns.Add(this.bandedGridColumn73);
            this.gridBand29.Columns.Add(this.bandedGridColumn74);
            this.gridBand29.CustomizationCaption = "Эмийн бүртгэл";
            this.gridBand29.Name = "gridBand29";
            this.gridBand29.VisibleIndex = 0;
            this.gridBand29.Width = 400;
            // 
            // bandedGridColumn69
            // 
            this.bandedGridColumn69.Caption = "Нэр";
            this.bandedGridColumn69.CustomizationCaption = "Эмийн нэр";
            this.bandedGridColumn69.FieldName = "medicineName";
            this.bandedGridColumn69.MinWidth = 100;
            this.bandedGridColumn69.Name = "bandedGridColumn69";
            this.bandedGridColumn69.Visible = true;
            this.bandedGridColumn69.Width = 100;
            // 
            // bandedGridColumn70
            // 
            this.bandedGridColumn70.Caption = "Ангилал";
            this.bandedGridColumn70.CustomizationCaption = "Эмийн ангилал";
            this.bandedGridColumn70.FieldName = "medicineTypeName";
            this.bandedGridColumn70.MinWidth = 75;
            this.bandedGridColumn70.Name = "bandedGridColumn70";
            this.bandedGridColumn70.Visible = true;
            // 
            // bandedGridColumn71
            // 
            this.bandedGridColumn71.Caption = "ОУ нэр";
            this.bandedGridColumn71.CustomizationCaption = "Олон улсын нэр";
            this.bandedGridColumn71.FieldName = "latinName";
            this.bandedGridColumn71.MinWidth = 75;
            this.bandedGridColumn71.Name = "bandedGridColumn71";
            // 
            // bandedGridColumn72
            // 
            this.bandedGridColumn72.Caption = "Тун хэмжээ";
            this.bandedGridColumn72.CustomizationCaption = "Тун хэмжээ";
            this.bandedGridColumn72.FieldName = "unitType";
            this.bandedGridColumn72.MinWidth = 75;
            this.bandedGridColumn72.Name = "bandedGridColumn72";
            this.bandedGridColumn72.Visible = true;
            // 
            // bandedGridColumn73
            // 
            this.bandedGridColumn73.Caption = "Хэлбэр";
            this.bandedGridColumn73.CustomizationCaption = "Хэлбэр";
            this.bandedGridColumn73.FieldName = "shape";
            this.bandedGridColumn73.MinWidth = 75;
            this.bandedGridColumn73.Name = "bandedGridColumn73";
            this.bandedGridColumn73.Visible = true;
            // 
            // bandedGridColumn74
            // 
            this.bandedGridColumn74.Caption = "Хэмжих нэгж";
            this.bandedGridColumn74.CustomizationCaption = "Хэмжих нэгж";
            this.bandedGridColumn74.FieldName = "quantity";
            this.bandedGridColumn74.MinWidth = 75;
            this.bandedGridColumn74.Name = "bandedGridColumn74";
            this.bandedGridColumn74.Visible = true;
            // 
            // gridBand31
            // 
            this.gridBand31.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand31.AppearanceHeader.Options.UseFont = true;
            this.gridBand31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand31.Caption = "Төлөвлөгөө";
            this.gridBand31.Columns.Add(this.bandedGridColumn76);
            this.gridBand31.Columns.Add(this.bandedGridColumn78);
            this.gridBand31.Columns.Add(this.bandedGridColumn82);
            this.gridBand31.Columns.Add(this.bandedGridColumn80);
            this.gridBand31.CustomizationCaption = "Төлөвлөгөө";
            this.gridBand31.Name = "gridBand31";
            this.gridBand31.VisibleIndex = 1;
            this.gridBand31.Width = 300;
            // 
            // bandedGridColumn76
            // 
            this.bandedGridColumn76.Caption = "Төлөвлөгөө";
            this.bandedGridColumn76.ColumnEdit = this.repositoryItemSpinEdit7;
            this.bandedGridColumn76.CustomizationCaption = "Сарын төлөвлөгөө";
            this.bandedGridColumn76.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn76.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn76.FieldName = "countPlan";
            this.bandedGridColumn76.MinWidth = 75;
            this.bandedGridColumn76.Name = "bandedGridColumn76";
            this.bandedGridColumn76.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "countPlan", "{0:n2}")});
            this.bandedGridColumn76.Visible = true;
            // 
            // repositoryItemSpinEdit7
            // 
            this.repositoryItemSpinEdit7.AutoHeight = false;
            this.repositoryItemSpinEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit7.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit7.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit7.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit7.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit7.Name = "repositoryItemSpinEdit7";
            // 
            // bandedGridColumn78
            // 
            this.bandedGridColumn78.Caption = "Гүйцэтгэл";
            this.bandedGridColumn78.ColumnEdit = this.repositoryItemSpinEdit8;
            this.bandedGridColumn78.CustomizationCaption = "Сарын гүйцэтгэл";
            this.bandedGridColumn78.FieldName = "count";
            this.bandedGridColumn78.MinWidth = 75;
            this.bandedGridColumn78.Name = "bandedGridColumn78";
            this.bandedGridColumn78.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.bandedGridColumn78.Visible = true;
            // 
            // repositoryItemSpinEdit8
            // 
            this.repositoryItemSpinEdit8.AutoHeight = false;
            this.repositoryItemSpinEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit8.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit8.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit8.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit8.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit8.Name = "repositoryItemSpinEdit8";
            // 
            // bandedGridColumn82
            // 
            this.bandedGridColumn82.Caption = "Өссөн дүн";
            this.bandedGridColumn82.ColumnEdit = this.repositoryItemSpinEdit8;
            this.bandedGridColumn82.CustomizationCaption = "Өссөн дүн";
            this.bandedGridColumn82.FieldName = "countInc";
            this.bandedGridColumn82.MinWidth = 75;
            this.bandedGridColumn82.Name = "bandedGridColumn82";
            this.bandedGridColumn82.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "countInc", "{0:n2}")});
            this.bandedGridColumn82.Visible = true;
            // 
            // bandedGridColumn80
            // 
            this.bandedGridColumn80.Caption = "Зөрүү";
            this.bandedGridColumn80.ColumnEdit = this.repositoryItemSpinEdit8;
            this.bandedGridColumn80.CustomizationCaption = "Зөрүү";
            this.bandedGridColumn80.FieldName = "countDis";
            this.bandedGridColumn80.MinWidth = 75;
            this.bandedGridColumn80.Name = "bandedGridColumn80";
            this.bandedGridColumn80.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "countDis", "{0:n2}")});
            this.bandedGridColumn80.Visible = true;
            // 
            // bandedGridColumn102
            // 
            this.bandedGridColumn102.Caption = "Дүн";
            this.bandedGridColumn102.ColumnEdit = this.repositoryItemSpinEdit7;
            this.bandedGridColumn102.CustomizationCaption = "Нийт дүн";
            this.bandedGridColumn102.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn102.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn102.FieldName = "sumPrice";
            this.bandedGridColumn102.MinWidth = 100;
            this.bandedGridColumn102.Name = "bandedGridColumn102";
            this.bandedGridColumn102.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.bandedGridColumn102.Width = 100;
            // 
            // bandedGridColumn75
            // 
            this.bandedGridColumn75.Caption = "Үнэ";
            this.bandedGridColumn75.ColumnEdit = this.repositoryItemSpinEdit7;
            this.bandedGridColumn75.CustomizationCaption = "Нэгжийн үнэ";
            this.bandedGridColumn75.FieldName = "price";
            this.bandedGridColumn75.MinWidth = 75;
            this.bandedGridColumn75.Name = "bandedGridColumn75";
            // 
            // gridControlTenderInc
            // 
            this.gridControlTenderInc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTenderInc.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlTenderInc.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlTenderInc.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlTenderInc.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlTenderInc.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlTenderInc.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInc_EmbeddedNavigator_ButtonClick);
            this.gridControlTenderInc.Location = new System.Drawing.Point(0, 72);
            this.gridControlTenderInc.MainView = this.gridViewTenderInc;
            this.gridControlTenderInc.Name = "gridControlTenderInc";
            this.gridControlTenderInc.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit5,
            this.repositoryItemSpinEdit6});
            this.gridControlTenderInc.Size = new System.Drawing.Size(778, 315);
            this.gridControlTenderInc.TabIndex = 8;
            this.gridControlTenderInc.UseEmbeddedNavigator = true;
            this.gridControlTenderInc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTenderInc});
            // 
            // gridViewTenderInc
            // 
            this.gridViewTenderInc.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand15,
            this.gridBand21,
            this.gridBand16,
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gridBand20,
            this.gridBand22,
            this.gridBand23,
            this.gridBand24,
            this.gridBand25,
            this.gridBand26,
            this.gridBand27,
            this.gridBand28});
            this.gridViewTenderInc.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn45,
            this.bandedGridColumn49,
            this.bandedGridColumn51,
            this.bandedGridColumn53,
            this.bandedGridColumn55,
            this.bandedGridColumn57,
            this.bandedGridColumn59,
            this.bandedGridColumn61,
            this.bandedGridColumn63,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.bandedGridColumn50,
            this.bandedGridColumn52,
            this.bandedGridColumn54,
            this.bandedGridColumn56,
            this.bandedGridColumn58,
            this.bandedGridColumn60,
            this.bandedGridColumn62,
            this.bandedGridColumn64,
            this.bandedGridColumn65,
            this.bandedGridColumn66,
            this.bandedGridColumn67,
            this.bandedGridColumn68,
            this.bandedGridColumn48});
            this.gridViewTenderInc.GridControl = this.gridControlTenderInc;
            this.gridViewTenderInc.LevelIndent = 1;
            this.gridViewTenderInc.Name = "gridViewTenderInc";
            this.gridViewTenderInc.OptionsBehavior.ReadOnly = true;
            this.gridViewTenderInc.OptionsDetail.ShowDetailTabs = false;
            this.gridViewTenderInc.OptionsPrint.ExpandAllDetails = true;
            this.gridViewTenderInc.OptionsPrint.PrintDetails = true;
            this.gridViewTenderInc.OptionsView.ShowAutoFilterRow = true;
            this.gridViewTenderInc.OptionsView.ShowFooter = true;
            this.gridViewTenderInc.OptionsView.ShowGroupPanel = false;
            this.gridViewTenderInc.PreviewFieldName = "preRow";
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand15.AppearanceHeader.Options.UseFont = true;
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "Эм";
            this.gridBand15.Columns.Add(this.bandedGridColumn45);
            this.gridBand15.Columns.Add(this.gridColumn44);
            this.gridBand15.Columns.Add(this.gridColumn45);
            this.gridBand15.Columns.Add(this.gridColumn46);
            this.gridBand15.Columns.Add(this.gridColumn47);
            this.gridBand15.Columns.Add(this.gridColumn48);
            this.gridBand15.CustomizationCaption = "Эмийн бүртгэл";
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 0;
            this.gridBand15.Width = 100;
            // 
            // bandedGridColumn45
            // 
            this.bandedGridColumn45.Caption = "Нэр";
            this.bandedGridColumn45.CustomizationCaption = "Эмийн нэр";
            this.bandedGridColumn45.FieldName = "medicineName";
            this.bandedGridColumn45.MinWidth = 100;
            this.bandedGridColumn45.Name = "bandedGridColumn45";
            this.bandedGridColumn45.Visible = true;
            this.bandedGridColumn45.Width = 100;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Ангилал";
            this.gridColumn44.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn44.FieldName = "medicineTypeName";
            this.gridColumn44.MinWidth = 75;
            this.gridColumn44.Name = "gridColumn44";
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "ОУ нэр";
            this.gridColumn45.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn45.FieldName = "latinName";
            this.gridColumn45.MinWidth = 75;
            this.gridColumn45.Name = "gridColumn45";
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Тун хэмжээ";
            this.gridColumn46.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn46.FieldName = "unitType";
            this.gridColumn46.MinWidth = 75;
            this.gridColumn46.Name = "gridColumn46";
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Хэлбэр";
            this.gridColumn47.CustomizationCaption = "Хэлбэр";
            this.gridColumn47.FieldName = "shape";
            this.gridColumn47.MinWidth = 75;
            this.gridColumn47.Name = "gridColumn47";
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Хэмжих нэгж";
            this.gridColumn48.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn48.FieldName = "quantity";
            this.gridColumn48.MinWidth = 75;
            this.gridColumn48.Name = "gridColumn48";
            // 
            // gridBand21
            // 
            this.gridBand21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand21.AppearanceHeader.Options.UseFont = true;
            this.gridBand21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand21.Caption = "Жилийн тоо";
            this.gridBand21.Columns.Add(this.gridColumn43);
            this.gridBand21.Columns.Add(this.gridColumn42);
            this.gridBand21.Columns.Add(this.bandedGridColumn48);
            this.gridBand21.CustomizationCaption = "Жилийн тоо";
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.VisibleIndex = 1;
            this.gridBand21.Width = 150;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Үнэ";
            this.gridColumn43.ColumnEdit = this.repositoryItemSpinEdit5;
            this.gridColumn43.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumn43.FieldName = "price";
            this.gridColumn43.MinWidth = 75;
            this.gridColumn43.Name = "gridColumn43";
            // 
            // repositoryItemSpinEdit5
            // 
            this.repositoryItemSpinEdit5.AutoHeight = false;
            this.repositoryItemSpinEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit5.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit5.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit5.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit5.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit5.Name = "repositoryItemSpinEdit5";
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Төлөвлөгөө";
            this.gridColumn42.ColumnEdit = this.repositoryItemSpinEdit5;
            this.gridColumn42.CustomizationCaption = "Жилийн төлөвлөгөөний тоо";
            this.gridColumn42.DisplayFormat.FormatString = "n2";
            this.gridColumn42.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn42.FieldName = "count";
            this.gridColumn42.MinWidth = 75;
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumn42.Visible = true;
            // 
            // bandedGridColumn48
            // 
            this.bandedGridColumn48.Caption = "Гүйцэтгэл";
            this.bandedGridColumn48.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn48.CustomizationCaption = "Жилийн гүйцэтгэлийн тоо";
            this.bandedGridColumn48.FieldName = "countInc";
            this.bandedGridColumn48.MinWidth = 75;
            this.bandedGridColumn48.Name = "bandedGridColumn48";
            this.bandedGridColumn48.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "countInc", "{0:n2}")});
            this.bandedGridColumn48.Visible = true;
            // 
            // repositoryItemSpinEdit6
            // 
            this.repositoryItemSpinEdit6.AutoHeight = false;
            this.repositoryItemSpinEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit6.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit6.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit6.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit6.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit6.Name = "repositoryItemSpinEdit6";
            // 
            // gridBand16
            // 
            this.gridBand16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand16.AppearanceHeader.Options.UseFont = true;
            this.gridBand16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand16.Caption = "1-р сар";
            this.gridBand16.Columns.Add(this.bandedGridColumn49);
            this.gridBand16.Columns.Add(this.bandedGridColumn50);
            this.gridBand16.CustomizationCaption = "1-р сар";
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 2;
            this.gridBand16.Width = 150;
            // 
            // bandedGridColumn49
            // 
            this.bandedGridColumn49.Caption = "Төлөвлөгөө";
            this.bandedGridColumn49.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn49.CustomizationCaption = "1-р сар төлөвлөгөө";
            this.bandedGridColumn49.FieldName = "jan";
            this.bandedGridColumn49.MinWidth = 75;
            this.bandedGridColumn49.Name = "bandedGridColumn49";
            this.bandedGridColumn49.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jan", "{0:n2}")});
            this.bandedGridColumn49.Visible = true;
            // 
            // bandedGridColumn50
            // 
            this.bandedGridColumn50.Caption = "Гүйцэтгэл";
            this.bandedGridColumn50.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn50.CustomizationCaption = "1-р сар гүйцэтгэл тоо";
            this.bandedGridColumn50.FieldName = "janInc";
            this.bandedGridColumn50.MinWidth = 75;
            this.bandedGridColumn50.Name = "bandedGridColumn50";
            this.bandedGridColumn50.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "janInc", "{0:n2}")});
            this.bandedGridColumn50.Visible = true;
            // 
            // gridBand17
            // 
            this.gridBand17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand17.AppearanceHeader.Options.UseFont = true;
            this.gridBand17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand17.Caption = "2-р сар";
            this.gridBand17.Columns.Add(this.bandedGridColumn51);
            this.gridBand17.Columns.Add(this.bandedGridColumn52);
            this.gridBand17.CustomizationCaption = "2-р сар";
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 3;
            this.gridBand17.Width = 150;
            // 
            // bandedGridColumn51
            // 
            this.bandedGridColumn51.Caption = "Төлөвлөгөө";
            this.bandedGridColumn51.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn51.CustomizationCaption = "2-р сар төлөвлөгөө";
            this.bandedGridColumn51.FieldName = "feb";
            this.bandedGridColumn51.MinWidth = 75;
            this.bandedGridColumn51.Name = "bandedGridColumn51";
            this.bandedGridColumn51.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "feb", "{0:n2}")});
            this.bandedGridColumn51.Visible = true;
            // 
            // bandedGridColumn52
            // 
            this.bandedGridColumn52.Caption = "Гүйцэтгэл";
            this.bandedGridColumn52.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn52.CustomizationCaption = "2-р сар гүйцэтгэл тоо";
            this.bandedGridColumn52.FieldName = "febInc";
            this.bandedGridColumn52.MinWidth = 75;
            this.bandedGridColumn52.Name = "bandedGridColumn52";
            this.bandedGridColumn52.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "febInc", "{0:n2}")});
            this.bandedGridColumn52.Visible = true;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand18.AppearanceHeader.Options.UseFont = true;
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.Caption = "3-р сар";
            this.gridBand18.Columns.Add(this.bandedGridColumn53);
            this.gridBand18.Columns.Add(this.bandedGridColumn54);
            this.gridBand18.CustomizationCaption = "3-р сар";
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = 4;
            this.gridBand18.Width = 150;
            // 
            // bandedGridColumn53
            // 
            this.bandedGridColumn53.Caption = "Төлөвлөгөө";
            this.bandedGridColumn53.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn53.CustomizationCaption = "3-р сар төлөвлөгөө";
            this.bandedGridColumn53.FieldName = "mar";
            this.bandedGridColumn53.MinWidth = 75;
            this.bandedGridColumn53.Name = "bandedGridColumn53";
            this.bandedGridColumn53.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "mar", "{0:n2}")});
            this.bandedGridColumn53.Visible = true;
            // 
            // bandedGridColumn54
            // 
            this.bandedGridColumn54.Caption = "Гүйцэтгэл";
            this.bandedGridColumn54.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn54.CustomizationCaption = "3-р сар гүйцэтгэл тоо";
            this.bandedGridColumn54.FieldName = "marInc";
            this.bandedGridColumn54.MinWidth = 75;
            this.bandedGridColumn54.Name = "bandedGridColumn54";
            this.bandedGridColumn54.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "marInc", "{0:n2}")});
            this.bandedGridColumn54.Visible = true;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand19.AppearanceHeader.Options.UseFont = true;
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.Caption = "4-р сар";
            this.gridBand19.Columns.Add(this.bandedGridColumn55);
            this.gridBand19.Columns.Add(this.bandedGridColumn56);
            this.gridBand19.CustomizationCaption = "4-р сар";
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 5;
            this.gridBand19.Width = 150;
            // 
            // bandedGridColumn55
            // 
            this.bandedGridColumn55.Caption = "Төлөвлөгөө";
            this.bandedGridColumn55.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn55.CustomizationCaption = "4-р сар төлөвлөгөө";
            this.bandedGridColumn55.FieldName = "apr";
            this.bandedGridColumn55.MinWidth = 75;
            this.bandedGridColumn55.Name = "bandedGridColumn55";
            this.bandedGridColumn55.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "apr", "{0:n2}")});
            this.bandedGridColumn55.Visible = true;
            // 
            // bandedGridColumn56
            // 
            this.bandedGridColumn56.Caption = "Гүйцэтгэл";
            this.bandedGridColumn56.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn56.CustomizationCaption = "4-р сар гүйцэтгэл тоо";
            this.bandedGridColumn56.FieldName = "augInc";
            this.bandedGridColumn56.MinWidth = 75;
            this.bandedGridColumn56.Name = "bandedGridColumn56";
            this.bandedGridColumn56.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "augInc", "{0:n2}")});
            this.bandedGridColumn56.Visible = true;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand20.AppearanceHeader.Options.UseFont = true;
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.Caption = "5-р сар";
            this.gridBand20.Columns.Add(this.bandedGridColumn57);
            this.gridBand20.Columns.Add(this.bandedGridColumn58);
            this.gridBand20.CustomizationCaption = "5-р сар";
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 6;
            this.gridBand20.Width = 150;
            // 
            // bandedGridColumn57
            // 
            this.bandedGridColumn57.Caption = "Төлөвлөгөө";
            this.bandedGridColumn57.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn57.CustomizationCaption = "5-р сар төлөвлөгөө";
            this.bandedGridColumn57.FieldName = "may";
            this.bandedGridColumn57.MinWidth = 75;
            this.bandedGridColumn57.Name = "bandedGridColumn57";
            this.bandedGridColumn57.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "may", "{0:n2}")});
            this.bandedGridColumn57.Visible = true;
            // 
            // bandedGridColumn58
            // 
            this.bandedGridColumn58.Caption = "Гүйцэтгэл";
            this.bandedGridColumn58.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn58.CustomizationCaption = "5-р сар гүйцэтгэл тоо";
            this.bandedGridColumn58.FieldName = "mayInc";
            this.bandedGridColumn58.MinWidth = 75;
            this.bandedGridColumn58.Name = "bandedGridColumn58";
            this.bandedGridColumn58.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "mayInc", "{0:n2}")});
            this.bandedGridColumn58.Visible = true;
            // 
            // gridBand22
            // 
            this.gridBand22.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand22.AppearanceHeader.Options.UseFont = true;
            this.gridBand22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand22.Caption = "6-р сар";
            this.gridBand22.Columns.Add(this.bandedGridColumn59);
            this.gridBand22.Columns.Add(this.bandedGridColumn60);
            this.gridBand22.CustomizationCaption = "6-р сар";
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.VisibleIndex = 7;
            this.gridBand22.Width = 150;
            // 
            // bandedGridColumn59
            // 
            this.bandedGridColumn59.Caption = "Төлөвлөгөө";
            this.bandedGridColumn59.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn59.CustomizationCaption = "6-р сар төлөвлөгөө";
            this.bandedGridColumn59.FieldName = "jun";
            this.bandedGridColumn59.MinWidth = 75;
            this.bandedGridColumn59.Name = "bandedGridColumn59";
            this.bandedGridColumn59.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jun", "{0:n2}")});
            this.bandedGridColumn59.Visible = true;
            // 
            // bandedGridColumn60
            // 
            this.bandedGridColumn60.Caption = "Гүйцэтгэл";
            this.bandedGridColumn60.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn60.CustomizationCaption = "6-р сар гүйцэтгэл тоо";
            this.bandedGridColumn60.FieldName = "junInc";
            this.bandedGridColumn60.MinWidth = 75;
            this.bandedGridColumn60.Name = "bandedGridColumn60";
            this.bandedGridColumn60.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "junInc", "{0:n2}")});
            this.bandedGridColumn60.Visible = true;
            // 
            // gridBand23
            // 
            this.gridBand23.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand23.AppearanceHeader.Options.UseFont = true;
            this.gridBand23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand23.Caption = "7-р сар";
            this.gridBand23.Columns.Add(this.bandedGridColumn61);
            this.gridBand23.Columns.Add(this.bandedGridColumn62);
            this.gridBand23.CustomizationCaption = "7-р сар";
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.VisibleIndex = 8;
            this.gridBand23.Width = 150;
            // 
            // bandedGridColumn61
            // 
            this.bandedGridColumn61.Caption = "Төлөвлөгөө";
            this.bandedGridColumn61.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn61.CustomizationCaption = "7-р сар төлөвлөгөө";
            this.bandedGridColumn61.FieldName = "jul";
            this.bandedGridColumn61.MinWidth = 75;
            this.bandedGridColumn61.Name = "bandedGridColumn61";
            this.bandedGridColumn61.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jul", "{0:n2}")});
            this.bandedGridColumn61.Visible = true;
            // 
            // bandedGridColumn62
            // 
            this.bandedGridColumn62.Caption = "Гүйцэтгэл";
            this.bandedGridColumn62.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn62.CustomizationCaption = "7-р сар гүйцэтгэл тоо";
            this.bandedGridColumn62.FieldName = "julInc";
            this.bandedGridColumn62.MinWidth = 75;
            this.bandedGridColumn62.Name = "bandedGridColumn62";
            this.bandedGridColumn62.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "julInc", "{0:n2}")});
            this.bandedGridColumn62.Visible = true;
            // 
            // gridBand24
            // 
            this.gridBand24.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand24.AppearanceHeader.Options.UseFont = true;
            this.gridBand24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand24.Caption = "8-р сар";
            this.gridBand24.Columns.Add(this.bandedGridColumn63);
            this.gridBand24.Columns.Add(this.bandedGridColumn64);
            this.gridBand24.CustomizationCaption = "8-р сар";
            this.gridBand24.Name = "gridBand24";
            this.gridBand24.VisibleIndex = 9;
            this.gridBand24.Width = 150;
            // 
            // bandedGridColumn63
            // 
            this.bandedGridColumn63.Caption = "Төлөвлөгөө";
            this.bandedGridColumn63.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn63.CustomizationCaption = "8-р сар төлөвлөгөө";
            this.bandedGridColumn63.FieldName = "aug";
            this.bandedGridColumn63.MinWidth = 75;
            this.bandedGridColumn63.Name = "bandedGridColumn63";
            this.bandedGridColumn63.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "aug", "{0:n2}")});
            this.bandedGridColumn63.Visible = true;
            // 
            // bandedGridColumn64
            // 
            this.bandedGridColumn64.Caption = "Гүйцэтгэл";
            this.bandedGridColumn64.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn64.CustomizationCaption = "8-р сар гүйцэтгэл тоо";
            this.bandedGridColumn64.FieldName = "augInc";
            this.bandedGridColumn64.MinWidth = 75;
            this.bandedGridColumn64.Name = "bandedGridColumn64";
            this.bandedGridColumn64.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "augInc", "{0:n2}")});
            this.bandedGridColumn64.Visible = true;
            // 
            // gridBand25
            // 
            this.gridBand25.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand25.AppearanceHeader.Options.UseFont = true;
            this.gridBand25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand25.Caption = "9-р сар";
            this.gridBand25.Columns.Add(this.gridColumn37);
            this.gridBand25.Columns.Add(this.bandedGridColumn65);
            this.gridBand25.CustomizationCaption = "9-р сар";
            this.gridBand25.Name = "gridBand25";
            this.gridBand25.VisibleIndex = 10;
            this.gridBand25.Width = 150;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Төлөвлөгөө";
            this.gridColumn37.ColumnEdit = this.repositoryItemSpinEdit6;
            this.gridColumn37.CustomizationCaption = "9-р сар төлөвлөгөө";
            this.gridColumn37.FieldName = "sep";
            this.gridColumn37.MinWidth = 75;
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sep", "{0:n2}")});
            this.gridColumn37.Visible = true;
            // 
            // bandedGridColumn65
            // 
            this.bandedGridColumn65.Caption = "Гүйцэтгэл";
            this.bandedGridColumn65.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn65.CustomizationCaption = "9-р сар гүйцэтгэл тоо";
            this.bandedGridColumn65.FieldName = "sepInc";
            this.bandedGridColumn65.MinWidth = 75;
            this.bandedGridColumn65.Name = "bandedGridColumn65";
            this.bandedGridColumn65.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sepInc", "{0:n2}")});
            this.bandedGridColumn65.Visible = true;
            // 
            // gridBand26
            // 
            this.gridBand26.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand26.AppearanceHeader.Options.UseFont = true;
            this.gridBand26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand26.Caption = "10-р сар";
            this.gridBand26.Columns.Add(this.gridColumn38);
            this.gridBand26.Columns.Add(this.bandedGridColumn66);
            this.gridBand26.CustomizationCaption = "10-р сар";
            this.gridBand26.Name = "gridBand26";
            this.gridBand26.VisibleIndex = 11;
            this.gridBand26.Width = 150;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Төлөвлөгөө";
            this.gridColumn38.ColumnEdit = this.repositoryItemSpinEdit6;
            this.gridColumn38.CustomizationCaption = "10-р сар төлөвлөгөө";
            this.gridColumn38.FieldName = "oct";
            this.gridColumn38.MinWidth = 75;
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "oct", "{0:n2}")});
            this.gridColumn38.Visible = true;
            // 
            // bandedGridColumn66
            // 
            this.bandedGridColumn66.Caption = "Гүйцэтгэл";
            this.bandedGridColumn66.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn66.CustomizationCaption = "10-р сар гүйцэтгэл тоо";
            this.bandedGridColumn66.FieldName = "octInc";
            this.bandedGridColumn66.MinWidth = 75;
            this.bandedGridColumn66.Name = "bandedGridColumn66";
            this.bandedGridColumn66.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "octInc", "{0:n2}")});
            this.bandedGridColumn66.Visible = true;
            // 
            // gridBand27
            // 
            this.gridBand27.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand27.AppearanceHeader.Options.UseFont = true;
            this.gridBand27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand27.Caption = "11-р сар";
            this.gridBand27.Columns.Add(this.gridColumn39);
            this.gridBand27.Columns.Add(this.bandedGridColumn67);
            this.gridBand27.CustomizationCaption = "11-р сар";
            this.gridBand27.Name = "gridBand27";
            this.gridBand27.VisibleIndex = 12;
            this.gridBand27.Width = 150;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Төлөвлөгөө";
            this.gridColumn39.ColumnEdit = this.repositoryItemSpinEdit6;
            this.gridColumn39.CustomizationCaption = "11-р сар төлөвлөгөө";
            this.gridColumn39.FieldName = "nov";
            this.gridColumn39.MinWidth = 75;
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "nov", "{0:n2}")});
            this.gridColumn39.Visible = true;
            // 
            // bandedGridColumn67
            // 
            this.bandedGridColumn67.Caption = "Гүйцэтгэл";
            this.bandedGridColumn67.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn67.CustomizationCaption = "11-р сар гүйцэтгэл тоо";
            this.bandedGridColumn67.FieldName = "novInc";
            this.bandedGridColumn67.MinWidth = 75;
            this.bandedGridColumn67.Name = "bandedGridColumn67";
            this.bandedGridColumn67.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "novInc", "{0:n2}")});
            this.bandedGridColumn67.Visible = true;
            // 
            // gridBand28
            // 
            this.gridBand28.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand28.AppearanceHeader.Options.UseFont = true;
            this.gridBand28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand28.Caption = "12-р сар";
            this.gridBand28.Columns.Add(this.gridColumn40);
            this.gridBand28.Columns.Add(this.bandedGridColumn68);
            this.gridBand28.CustomizationCaption = "12-р сар";
            this.gridBand28.Name = "gridBand28";
            this.gridBand28.VisibleIndex = 13;
            this.gridBand28.Width = 150;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Төлөвлөгөө";
            this.gridColumn40.ColumnEdit = this.repositoryItemSpinEdit6;
            this.gridColumn40.CustomizationCaption = "12-р сар төлөвлөгөө";
            this.gridColumn40.FieldName = "dec";
            this.gridColumn40.MinWidth = 75;
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "dec", "{0:n2}")});
            this.gridColumn40.Visible = true;
            // 
            // bandedGridColumn68
            // 
            this.bandedGridColumn68.Caption = "Гүйцэтгэл";
            this.bandedGridColumn68.ColumnEdit = this.repositoryItemSpinEdit6;
            this.bandedGridColumn68.CustomizationCaption = "12-р сар гүйцэтгэл тоо";
            this.bandedGridColumn68.FieldName = "decInc";
            this.bandedGridColumn68.MinWidth = 75;
            this.bandedGridColumn68.Name = "bandedGridColumn68";
            this.bandedGridColumn68.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "decInc", "{0:n2}")});
            this.bandedGridColumn68.Visible = true;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Дүн";
            this.gridColumn41.ColumnEdit = this.repositoryItemSpinEdit5;
            this.gridColumn41.CustomizationCaption = "Нийт дүн";
            this.gridColumn41.DisplayFormat.FormatString = "n2";
            this.gridColumn41.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn41.FieldName = "sumPrice";
            this.gridColumn41.MinWidth = 100;
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.gridColumn41.Width = 100;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControlMonth);
            this.panelControl1.Controls.Add(this.comboBoxEditMonth);
            this.panelControl1.Controls.Add(this.radioGroupJournal);
            this.panelControl1.Controls.Add(this.textEditPackage);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.dateEditPlanStart);
            this.panelControl1.Controls.Add(this.gridLookUpEditTender);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(778, 72);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControlMonth
            // 
            this.labelControlMonth.Location = new System.Drawing.Point(290, 17);
            this.labelControlMonth.Name = "labelControlMonth";
            this.labelControlMonth.Size = new System.Drawing.Size(26, 13);
            this.labelControlMonth.TabIndex = 22;
            this.labelControlMonth.Text = "Сар :";
            // 
            // comboBoxEditMonth
            // 
            this.comboBoxEditMonth.Location = new System.Drawing.Point(322, 14);
            this.comboBoxEditMonth.Name = "comboBoxEditMonth";
            this.comboBoxEditMonth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditMonth.Properties.Items.AddRange(new object[] {
            "1-р сар",
            "2-р сар",
            "3-р сар",
            "4-р сар",
            "5-р сар",
            "6-р сар",
            "7-р сар",
            "8-р сар",
            "9-р сар",
            "10-р сар",
            "11-р сар",
            "12-р сар"});
            this.comboBoxEditMonth.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditMonth.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEditMonth.TabIndex = 21;
            this.comboBoxEditMonth.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditMonth_SelectedIndexChanged);
            // 
            // radioGroupJournal
            // 
            this.radioGroupJournal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupJournal.Location = new System.Drawing.Point(631, 12);
            this.radioGroupJournal.Name = "radioGroupJournal";
            this.radioGroupJournal.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Сар", true, "Journal"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Жил", true, "Detail")});
            this.radioGroupJournal.Size = new System.Drawing.Size(140, 23);
            this.radioGroupJournal.TabIndex = 20;
            this.radioGroupJournal.SelectedIndexChanged += new System.EventHandler(this.radioGroupJournal_SelectedIndexChanged);
            // 
            // textEditPackage
            // 
            this.textEditPackage.Location = new System.Drawing.Point(115, 40);
            this.textEditPackage.Name = "textEditPackage";
            this.textEditPackage.Properties.ReadOnly = true;
            this.textEditPackage.Size = new System.Drawing.Size(150, 20);
            this.textEditPackage.TabIndex = 19;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(79, 43);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(30, 13);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "Багц :";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(278, 43);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(38, 13);
            this.labelControl2.TabIndex = 17;
            this.labelControl2.Text = "Огноо :";
            // 
            // dateEditPlanStart
            // 
            this.dateEditPlanStart.EditValue = null;
            this.dateEditPlanStart.Location = new System.Drawing.Point(322, 40);
            this.dateEditPlanStart.Name = "dateEditPlanStart";
            this.dateEditPlanStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditPlanStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditPlanStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditPlanStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditPlanStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditPlanStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditPlanStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditPlanStart.Properties.ReadOnly = true;
            this.dateEditPlanStart.Size = new System.Drawing.Size(100, 20);
            this.dateEditPlanStart.TabIndex = 16;
            this.dateEditPlanStart.ToolTip = "Эхлэх огноо";
            // 
            // gridLookUpEditTender
            // 
            this.gridLookUpEditTender.EditValue = "";
            this.gridLookUpEditTender.Location = new System.Drawing.Point(115, 14);
            this.gridLookUpEditTender.Name = "gridLookUpEditTender";
            this.gridLookUpEditTender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditTender.Properties.DisplayMember = "tenderNumber";
            this.gridLookUpEditTender.Properties.NullText = "";
            this.gridLookUpEditTender.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditTender.Properties.ValueMember = "id";
            this.gridLookUpEditTender.Properties.View = this.gridView1;
            this.gridLookUpEditTender.Size = new System.Drawing.Size(150, 20);
            this.gridLookUpEditTender.TabIndex = 15;
            this.gridLookUpEditTender.EditValueChanged += new System.EventHandler(this.gridLookUpEditTender_EditValueChanged);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Тендер";
            this.gridColumn49.CustomizationCaption = "Тендер";
            this.gridColumn49.FieldName = "tenderNumber";
            this.gridColumn49.MinWidth = 75;
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 0;
            this.gridColumn49.Width = 171;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Бэлтгэн нийлүүлэгч";
            this.gridColumn50.CustomizationCaption = "Бэлтгэн нийлүүлэгч";
            this.gridColumn50.FieldName = "supplierName";
            this.gridColumn50.MinWidth = 75;
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 2;
            this.gridColumn50.Width = 138;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Багц";
            this.gridColumn51.CustomizationCaption = "Багц";
            this.gridColumn51.FieldName = "packageName";
            this.gridColumn51.MinWidth = 75;
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 1;
            this.gridColumn51.Width = 166;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Тайлбар";
            this.gridColumn52.CustomizationCaption = "Тайлбар";
            this.gridColumn52.FieldName = "description";
            this.gridColumn52.MinWidth = 75;
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 4;
            this.gridColumn52.Width = 334;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Огноо";
            this.gridColumn53.CustomizationCaption = "Огноо";
            this.gridColumn53.FieldName = "date";
            this.gridColumn53.MinWidth = 75;
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 3;
            this.gridColumn53.Width = 122;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(65, 17);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(44, 13);
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "Тендер :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(341, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TenderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl2);
            this.Name = "TenderForm";
            this.Text = "Тендер";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlanForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlanDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTenderPlanDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTenderPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridViewTenderPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageTender.ResumeLayout(false);
            this.xtraTabPagePlan.ResumeLayout(false);
            this.xtraTabPageTenderInc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTenderIncMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTenderIncMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTenderInc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTenderInc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMonth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupJournal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPackage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraGrid.GridControl gridControlPlan;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPlanDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPackage;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewPlan;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn88;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewDetail;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn24;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn26;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn27;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnSumPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMedCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSum;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageTender;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePlan;
        private DevExpress.XtraGrid.GridControl gridControlTenderPlan;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTenderPlanDetail;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridViewTenderPlan;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn20;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn30;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn32;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn33;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn34;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn35;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn36;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn37;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn38;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn39;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn40;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn41;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn42;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn43;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn44;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn46;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageTenderInc;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControlTenderInc;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewTenderInc;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn45;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn44;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn45;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn46;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn47;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn48;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn43;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn42;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn48;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn49;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn50;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn51;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn52;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn53;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn54;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn55;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn56;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn57;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn58;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn59;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn60;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn61;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn62;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand24;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn63;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn64;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn37;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn65;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn38;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn66;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand27;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn39;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn67;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn40;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn68;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn41;
        private DevExpress.XtraEditors.TextEdit textEditPackage;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit dateEditPlanStart;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditTender;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.RadioGroup radioGroupJournal;
        private DevExpress.XtraEditors.LabelControl labelControlMonth;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditMonth;
        private DevExpress.XtraGrid.GridControl gridControlTenderIncMonth;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewTenderIncMonth;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn69;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn70;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn71;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn72;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn73;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn74;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand31;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn76;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn78;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn82;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn80;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn102;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn75;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn77;
        private System.Windows.Forms.Button button1;
    }
}