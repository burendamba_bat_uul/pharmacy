﻿namespace Pharmacy2016.gui.core.journal.plan
{
    partial class PlanMonthlyUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlanMonthlyUpForm));
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlDivide = new DevExpress.XtraGrid.GridControl();
            this.gridViewDivide = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnMedicineID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditMed = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCount = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnJan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnFeb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnApr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnJun = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnJul = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnAug = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnNov = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDec = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.dateEditStartDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditEndDate = new DevExpress.XtraEditors.DateEdit();
            this.checkedComboBoxEdit = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlConfirm = new DevExpress.XtraEditors.LabelControl();
            this.checkEditIsConfirm = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.buttonCal = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDivide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDivide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditMed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsConfirm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 426);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 8;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(697, 426);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 7;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(616, 426);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 6;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // gridControlDivide
            // 
            this.gridControlDivide.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlDivide.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlDivide.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlDivide.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlDivide.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlDivide.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlDivide.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlDivide.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlDivide_EmbeddedNavigator_ButtonClick);
            this.gridControlDivide.Location = new System.Drawing.Point(12, 117);
            this.gridControlDivide.MainView = this.gridViewDivide;
            this.gridControlDivide.Name = "gridControlDivide";
            this.gridControlDivide.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemGridLookUpEditMed,
            this.repositoryItemSpinEditCount});
            this.gridControlDivide.Size = new System.Drawing.Size(760, 303);
            this.gridControlDivide.TabIndex = 5;
            this.gridControlDivide.UseEmbeddedNavigator = true;
            this.gridControlDivide.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDivide});
            // 
            // gridViewDivide
            // 
            this.gridViewDivide.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnMedicineID,
            this.gridColumnCount,
            this.gridColumnJan,
            this.gridColumnFeb,
            this.gridColumnMar,
            this.gridColumnApr,
            this.gridColumnMay,
            this.gridColumnJun,
            this.gridColumnJul,
            this.gridColumnAug,
            this.gridColumnSep,
            this.gridColumnOct,
            this.gridColumnNov,
            this.gridColumnDec,
            this.gridColumn2});
            this.gridViewDivide.GridControl = this.gridControlDivide;
            this.gridViewDivide.Name = "gridViewDivide";
            this.gridViewDivide.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewDivide.OptionsSelection.MultiSelect = true;
            this.gridViewDivide.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridViewDivide.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewDivide.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDivide.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewDivide.OptionsView.ShowFooter = true;
            this.gridViewDivide.OptionsView.ShowGroupPanel = false;
            this.gridViewDivide.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewDivide_SelectionChanged);
            this.gridViewDivide.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewDivide_FocusedRowChanged);
            this.gridViewDivide.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewDivide_InvalidRowException);
            // 
            // gridColumnMedicineID
            // 
            this.gridColumnMedicineID.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnMedicineID.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnMedicineID.Caption = "Нэр";
            this.gridColumnMedicineID.ColumnEdit = this.repositoryItemGridLookUpEditMed;
            this.gridColumnMedicineID.CustomizationCaption = "Худалдааны нэр";
            this.gridColumnMedicineID.FieldName = "medicineID";
            this.gridColumnMedicineID.MinWidth = 175;
            this.gridColumnMedicineID.Name = "gridColumnMedicineID";
            this.gridColumnMedicineID.Visible = true;
            this.gridColumnMedicineID.VisibleIndex = 1;
            this.gridColumnMedicineID.Width = 175;
            // 
            // repositoryItemGridLookUpEditMed
            // 
            this.repositoryItemGridLookUpEditMed.AutoHeight = false;
            this.repositoryItemGridLookUpEditMed.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditMed.DisplayMember = "name";
            this.repositoryItemGridLookUpEditMed.Name = "repositoryItemGridLookUpEditMed";
            this.repositoryItemGridLookUpEditMed.PopupFormSize = new System.Drawing.Size(500, 300);
            this.repositoryItemGridLookUpEditMed.ValueMember = "id";
            this.repositoryItemGridLookUpEditMed.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn17,
            this.gridColumn19,
            this.gridColumn18,
            this.gridColumn16});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Нэр";
            this.gridColumn15.CustomizationCaption = "Эмийн нэр";
            this.gridColumn15.FieldName = "name";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Тун";
            this.gridColumn17.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn17.FieldName = "unitName";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 1;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Хэлбэр";
            this.gridColumn19.CustomizationCaption = "Эмийн хэлбэр";
            this.gridColumn19.FieldName = "shapeName";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 2;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Хэмжих нэгж";
            this.gridColumn18.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn18.FieldName = "quantityName";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 3;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Ангилал";
            this.gridColumn16.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn16.FieldName = "medicineTypeName";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 4;
            // 
            // gridColumnCount
            // 
            this.gridColumnCount.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnCount.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnCount.Caption = "Жилд авах багцын тоо";
            this.gridColumnCount.ColumnEdit = this.repositoryItemSpinEditCount;
            this.gridColumnCount.CustomizationCaption = "Жилд авах багцын тоо";
            this.gridColumnCount.DisplayFormat.FormatString = "n2";
            this.gridColumnCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnCount.FieldName = "count";
            this.gridColumnCount.MinWidth = 125;
            this.gridColumnCount.Name = "gridColumnCount";
            this.gridColumnCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumnCount.Visible = true;
            this.gridColumnCount.VisibleIndex = 3;
            this.gridColumnCount.Width = 125;
            // 
            // repositoryItemSpinEditCount
            // 
            this.repositoryItemSpinEditCount.AutoHeight = false;
            this.repositoryItemSpinEditCount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditCount.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEditCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditCount.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEditCount.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditCount.Mask.EditMask = "n2";
            this.repositoryItemSpinEditCount.Name = "repositoryItemSpinEditCount";
            // 
            // gridColumnJan
            // 
            this.gridColumnJan.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnJan.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnJan.Caption = "1-р сар";
            this.gridColumnJan.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnJan.FieldName = "jan";
            this.gridColumnJan.MinWidth = 50;
            this.gridColumnJan.Name = "gridColumnJan";
            this.gridColumnJan.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jan", "{0:n2}")});
            this.gridColumnJan.Visible = true;
            this.gridColumnJan.VisibleIndex = 4;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnFeb
            // 
            this.gridColumnFeb.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnFeb.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnFeb.Caption = "2-р сар";
            this.gridColumnFeb.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnFeb.FieldName = "feb";
            this.gridColumnFeb.MinWidth = 50;
            this.gridColumnFeb.Name = "gridColumnFeb";
            this.gridColumnFeb.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "feb", "{0:n2}")});
            this.gridColumnFeb.Visible = true;
            this.gridColumnFeb.VisibleIndex = 5;
            // 
            // gridColumnMar
            // 
            this.gridColumnMar.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnMar.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnMar.Caption = "3-р сар";
            this.gridColumnMar.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnMar.FieldName = "mar";
            this.gridColumnMar.MinWidth = 50;
            this.gridColumnMar.Name = "gridColumnMar";
            this.gridColumnMar.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "mar", "{0:n2}")});
            this.gridColumnMar.Visible = true;
            this.gridColumnMar.VisibleIndex = 6;
            // 
            // gridColumnApr
            // 
            this.gridColumnApr.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnApr.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnApr.Caption = "4-р сар";
            this.gridColumnApr.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnApr.FieldName = "apr";
            this.gridColumnApr.MinWidth = 50;
            this.gridColumnApr.Name = "gridColumnApr";
            this.gridColumnApr.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "apr", "{0:n2}")});
            this.gridColumnApr.Visible = true;
            this.gridColumnApr.VisibleIndex = 7;
            // 
            // gridColumnMay
            // 
            this.gridColumnMay.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnMay.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnMay.Caption = "5-р сар";
            this.gridColumnMay.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnMay.FieldName = "may";
            this.gridColumnMay.MinWidth = 50;
            this.gridColumnMay.Name = "gridColumnMay";
            this.gridColumnMay.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "may", "{0:n2}")});
            this.gridColumnMay.Visible = true;
            this.gridColumnMay.VisibleIndex = 8;
            // 
            // gridColumnJun
            // 
            this.gridColumnJun.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnJun.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnJun.Caption = "6-р сар";
            this.gridColumnJun.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnJun.FieldName = "jun";
            this.gridColumnJun.MinWidth = 50;
            this.gridColumnJun.Name = "gridColumnJun";
            this.gridColumnJun.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jun", "{0:n2}")});
            this.gridColumnJun.Visible = true;
            this.gridColumnJun.VisibleIndex = 9;
            // 
            // gridColumnJul
            // 
            this.gridColumnJul.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnJul.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnJul.Caption = "7-р сар";
            this.gridColumnJul.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnJul.FieldName = "jul";
            this.gridColumnJul.MinWidth = 50;
            this.gridColumnJul.Name = "gridColumnJul";
            this.gridColumnJul.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jul", "{0:n2}")});
            this.gridColumnJul.Visible = true;
            this.gridColumnJul.VisibleIndex = 10;
            // 
            // gridColumnAug
            // 
            this.gridColumnAug.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnAug.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnAug.Caption = "8-р сар";
            this.gridColumnAug.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnAug.FieldName = "aug";
            this.gridColumnAug.MinWidth = 50;
            this.gridColumnAug.Name = "gridColumnAug";
            this.gridColumnAug.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "aug", "{0:n2}")});
            this.gridColumnAug.Visible = true;
            this.gridColumnAug.VisibleIndex = 11;
            // 
            // gridColumnSep
            // 
            this.gridColumnSep.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnSep.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnSep.Caption = "9-р сар";
            this.gridColumnSep.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnSep.FieldName = "sep";
            this.gridColumnSep.MinWidth = 50;
            this.gridColumnSep.Name = "gridColumnSep";
            this.gridColumnSep.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sep", "{0:n2}")});
            this.gridColumnSep.Visible = true;
            this.gridColumnSep.VisibleIndex = 12;
            // 
            // gridColumnOct
            // 
            this.gridColumnOct.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnOct.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnOct.Caption = "10-р сар";
            this.gridColumnOct.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnOct.FieldName = "oct";
            this.gridColumnOct.MinWidth = 50;
            this.gridColumnOct.Name = "gridColumnOct";
            this.gridColumnOct.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "oct", "{0:n2}")});
            this.gridColumnOct.Visible = true;
            this.gridColumnOct.VisibleIndex = 13;
            // 
            // gridColumnNov
            // 
            this.gridColumnNov.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnNov.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnNov.Caption = "11-р сар";
            this.gridColumnNov.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnNov.FieldName = "nov";
            this.gridColumnNov.MinWidth = 50;
            this.gridColumnNov.Name = "gridColumnNov";
            this.gridColumnNov.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "nov", "{0:n2}")});
            this.gridColumnNov.Visible = true;
            this.gridColumnNov.VisibleIndex = 14;
            // 
            // gridColumnDec
            // 
            this.gridColumnDec.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnDec.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnDec.Caption = "12-р сар";
            this.gridColumnDec.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnDec.FieldName = "dec";
            this.gridColumnDec.MinWidth = 50;
            this.gridColumnDec.Name = "gridColumnDec";
            this.gridColumnDec.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "dec", "{0:n2}")});
            this.gridColumnDec.Visible = true;
            this.gridColumnDec.VisibleIndex = 15;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumn2.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn2.Caption = "Үнэ";
            this.gridColumn2.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn2.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumn2.FieldName = "price";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(68, 47);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(75, 13);
            this.labelControl4.TabIndex = 81;
            this.labelControl4.Text = "Эхлэх огноо*: ";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(356, 47);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(79, 13);
            this.labelControl5.TabIndex = 84;
            this.labelControl5.Text = "Дуусах огноо : ";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(39, 21);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(110, 13);
            this.labelControl6.TabIndex = 85;
            this.labelControl6.Text = "Төлөвлөгөөний нэр*: ";
            // 
            // textEditName
            // 
            this.textEditName.EnterMoveNextControl = true;
            this.textEditName.Location = new System.Drawing.Point(149, 18);
            this.textEditName.Name = "textEditName";
            this.textEditName.Properties.MaxLength = 50;
            this.textEditName.Size = new System.Drawing.Size(200, 20);
            this.textEditName.TabIndex = 1;
            // 
            // dateEditStartDate
            // 
            this.dateEditStartDate.EditValue = null;
            this.dateEditStartDate.Location = new System.Drawing.Point(149, 44);
            this.dateEditStartDate.Name = "dateEditStartDate";
            this.dateEditStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStartDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStartDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStartDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStartDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStartDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStartDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditStartDate.TabIndex = 2;
            this.dateEditStartDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditStartDate_KeyUp);
            // 
            // dateEditEndDate
            // 
            this.dateEditEndDate.EditValue = null;
            this.dateEditEndDate.Location = new System.Drawing.Point(441, 44);
            this.dateEditEndDate.Name = "dateEditEndDate";
            this.dateEditEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEndDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEndDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEndDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEndDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEndDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEndDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditEndDate.TabIndex = 3;
            this.dateEditEndDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditEndDate_KeyUp);
            // 
            // checkedComboBoxEdit
            // 
            this.checkedComboBoxEdit.EditValue = "";
            this.checkedComboBoxEdit.Location = new System.Drawing.Point(441, 18);
            this.checkedComboBoxEdit.Name = "checkedComboBoxEdit";
            this.checkedComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEdit.Properties.DropDownRows = 12;
            this.checkedComboBoxEdit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("jan", "1-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("feb", "2-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("mar", "3-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("apr", "4-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("may", "5-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("jun", "6-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("jul", "7-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("aug", "8-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("sep", "9-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("oct", "10-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("nov", "11-р сар"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("dec", "12-р сар")});
            this.checkedComboBoxEdit.Properties.ShowButtons = false;
            this.checkedComboBoxEdit.Size = new System.Drawing.Size(150, 20);
            this.checkedComboBoxEdit.TabIndex = 20;
            this.checkedComboBoxEdit.ToolTip = "Харагдах багана сонгох";
            this.checkedComboBoxEdit.EditValueChanged += new System.EventHandler(this.checkedComboBoxEdit_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(360, 21);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(75, 13);
            this.labelControl1.TabIndex = 90;
            this.labelControl1.Text = "Сард хуваах : ";
            // 
            // labelControlConfirm
            // 
            this.labelControlConfirm.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlConfirm.Location = new System.Drawing.Point(651, 95);
            this.labelControlConfirm.Name = "labelControlConfirm";
            this.labelControlConfirm.Size = new System.Drawing.Size(95, 13);
            this.labelControlConfirm.TabIndex = 10;
            this.labelControlConfirm.Text = "Баталгаажуулах*:";
            // 
            // checkEditIsConfirm
            // 
            this.checkEditIsConfirm.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkEditIsConfirm.Location = new System.Drawing.Point(752, 92);
            this.checkEditIsConfirm.Name = "checkEditIsConfirm";
            this.checkEditIsConfirm.Properties.Caption = "";
            this.checkEditIsConfirm.Size = new System.Drawing.Size(20, 19);
            this.checkEditIsConfirm.TabIndex = 93;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(94, 72);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(49, 13);
            this.labelControl8.TabIndex = 92;
            this.labelControl8.Text = "Тайлбар :";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.memoEditDescription.EnterMoveNextControl = true;
            this.memoEditDescription.Location = new System.Drawing.Point(149, 70);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(442, 41);
            this.memoEditDescription.TabIndex = 4;
            // 
            // buttonCal
            // 
            this.buttonCal.Image = ((System.Drawing.Image)(resources.GetObject("buttonCal.Image")));
            this.buttonCal.Location = new System.Drawing.Point(597, 18);
            this.buttonCal.Name = "buttonCal";
            this.buttonCal.Size = new System.Drawing.Size(25, 20);
            this.buttonCal.TabIndex = 94;
            this.buttonCal.ToolTip = "Сонгосон саруудад тэгш хуваах";
            this.buttonCal.Click += new System.EventHandler(this.buttonCal_Click);
            // 
            // PlanMonthlyUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.buttonCal);
            this.Controls.Add(this.labelControlConfirm);
            this.Controls.Add(this.checkEditIsConfirm);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.memoEditDescription);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.checkedComboBoxEdit);
            this.Controls.Add(this.dateEditEndDate);
            this.Controls.Add(this.dateEditStartDate);
            this.Controls.Add(this.textEditName);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.gridControlDivide);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonSave);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PlanMonthlyUpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Төлөвлөгөө";
            this.Shown += new System.EventHandler(this.PlanMonthlyUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDivide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDivide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditMed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsConfirm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraGrid.GridControl gridControlDivide;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDivide;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMedicineID;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnJan;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnFeb;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMar;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnApr;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMay;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnJun;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnJul;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnAug;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSep;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOct;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnNov;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDec;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCount;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraEditors.DateEdit dateEditStartDate;
        private DevExpress.XtraEditors.DateEdit dateEditEndDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditMed;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEdit;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCount;
        private DevExpress.XtraEditors.LabelControl labelControlConfirm;
        private DevExpress.XtraEditors.CheckEdit checkEditIsConfirm;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.SimpleButton buttonCal;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
    }
}