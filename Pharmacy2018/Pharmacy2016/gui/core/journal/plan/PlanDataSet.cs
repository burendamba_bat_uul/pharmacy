﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
namespace Pharmacy2016.gui.core.journal.plan {
    
    
    public partial class PlanDataSet {

        #region DataSet-н обьект

            private static PlanDataSet INSTANCE = new PlanDataSet();

            public static PlanDataSet Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            public static readonly string[] orderType = {
                                   "Үндсэн захиалга",
                                   "Яаралтай захиалга",
                                   "Нэмэлт захиалга"
                                  };

        #endregion

        #region TableAdapter, BindingSource-ууд

            // QueriesTableAdapter
            //private static readonly UserDataSetTableAdapters.QueriesTableAdapter INSTANCE_QUERY_TABLE_ADAPTER = new UserDataSetTableAdapters.QueriesTableAdapter();
            //public static UserDataSetTableAdapters.QueriesTableAdapter QueriesTableAdapter
            //{
            //    get
            //    {
            //        return INSTANCE_QUERY_TABLE_ADAPTER;
            //    }
            //}

            //Эм
            private static readonly PlanDataSetTableAdapters.MedicineOtherTableAdapter INSTANCE_MEDICINE_OTHER_TABLE_ADAPTER = new PlanDataSetTableAdapters.MedicineOtherTableAdapter();
            public static PlanDataSetTableAdapters.MedicineOtherTableAdapter MedicineOtherTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINE_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINE_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineOtherBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINE_OTHER_BINDING_SOURCE;
                }
            }

            // Эмчилгээ өөр
            private static readonly PlanDataSetTableAdapters.DrugDoctorOtherTableAdapter INSTANCE_DRUG_DOCTOR_OTHER_TABLE_ADAPTER = new PlanDataSetTableAdapters.DrugDoctorOtherTableAdapter();
            public static PlanDataSetTableAdapters.DrugDoctorOtherTableAdapter DrugDoctorOtherTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_DOCTOR_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_DRUG_DOCTOR_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugDoctorOtherBindingSource
            {
                get
                {
                    return INSTANCE_DRUG_DOCTOR_OTHER_BINDING_SOURCE;
                }
            }

            // Эмчилгээ детиал өөр
            private static readonly PlanDataSetTableAdapters.DrugDoctorDetailOtherTableAdapter INSTANCE_DRUG_DOCTOR_DETAIL_OTHER_TABLE_ADAPTER = new PlanDataSetTableAdapters.DrugDoctorDetailOtherTableAdapter();
            public static PlanDataSetTableAdapters.DrugDoctorDetailOtherTableAdapter DrugDoctorDetailOtherTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_DOCTOR_DETAIL_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_DRUG_DOCTOR_DETAIL_ANOTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugDoctorDetailAnotherBindingSource
            {
                get
                {
                    return INSTANCE_DRUG_DOCTOR_DETAIL_ANOTHER_BINDING_SOURCE;
                }
            }

            // Эмчилгээ
            private static readonly PlanDataSetTableAdapters.DrugDoctorTableAdapter INSTANCE_DRUG_DOCTOR_TABLE_ADAPTER = new PlanDataSetTableAdapters.DrugDoctorTableAdapter();
            public static PlanDataSetTableAdapters.DrugDoctorTableAdapter DrugDoctorTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_DOCTOR_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_DRUG_DOCTOR_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugDoctorBindingSource
            {
                get
                {
                    return INSTANCE_DRUG_DOCTOR_BINDING_SOURCE;
                }
            }

            // Эмчилгээ детиал
            private static readonly PlanDataSetTableAdapters.DrugDoctorDetailTableAdapter INSTANCE_DRUG_DOCTOR_DETAIL_TABLE_ADAPTER = new PlanDataSetTableAdapters.DrugDoctorDetailTableAdapter();
            public static PlanDataSetTableAdapters.DrugDoctorDetailTableAdapter DrugDoctorDetailTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_DOCTOR_DETAIL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_DRUG_DOCTOR_DETAIL_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugDoctorDetailBindingSource
            {
                get
                {
                    return INSTANCE_DRUG_DOCTOR_DETAIL_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_DRUG_DOCTOR_DETAIL_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugDoctorDetailOtherBindingSource
            {
                get
                {
                    return INSTANCE_DRUG_DOCTOR_DETAIL_OTHER_BINDING_SOURCE;
                }
            }


            // Төлөвлөгөө
            private static readonly PlanDataSetTableAdapters.TenderJournalTableAdapter INSTANCE_TENDER_JOURNAL_TABLE_ADAPTER = new PlanDataSetTableAdapters.TenderJournalTableAdapter();
            public static PlanDataSetTableAdapters.TenderJournalTableAdapter TenderJournalTableAdapter
            {
                get
                {
                    return INSTANCE_TENDER_JOURNAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_TENDER_JOURNAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource TenderJournalBindingSource
            {
                get
                {
                    return INSTANCE_TENDER_JOURNAL_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_TENDER_JOURNAL_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource TenderJournalOtherBindingSource
            {
                get
                {
                    return INSTANCE_TENDER_JOURNAL_OTHER_BINDING_SOURCE;
                }
            }


            // Төлөвлөгөө эм
            private static readonly PlanDataSetTableAdapters.TenderDetailTableAdapter INSTANCE_TENDER_DETIAL_TABLE_ADAPTER = new PlanDataSetTableAdapters.TenderDetailTableAdapter();
            public static PlanDataSetTableAdapters.TenderDetailTableAdapter TenderDetailTableAdapter
            {
                get
                {
                    return INSTANCE_TENDER_DETIAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_TENDER_DETIAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource TenderDetialBindingSource
            {
                get
                {
                    return INSTANCE_TENDER_DETIAL_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_TENDER_DETIAL_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource TenderDetialOtherBindingSource
            {
                get
                {
                    return INSTANCE_TENDER_DETIAL_OTHER_BINDING_SOURCE;
                }
            }


            // Төлөвлөлт
            private static readonly PlanDataSetTableAdapters.PlanMonthlyTableAdapter INSTANCE_PLAN_MONTHLY_TABLE_ADAPTER = new PlanDataSetTableAdapters.PlanMonthlyTableAdapter();
            public static PlanDataSetTableAdapters.PlanMonthlyTableAdapter PlanMonthlyTableAdapter
            {
                get
                {
                    return INSTANCE_PLAN_MONTHLY_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PLAN_MONTHLY_BINDING_SOURCE = new BindingSource();
            public static BindingSource PlanMonthlyBindingSource
            {
                get
                {
                    return INSTANCE_PLAN_MONTHLY_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_PLAN_MONTHLY_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource PlanMonthlyOtherBindingSource
            {
                get
                {
                    return INSTANCE_PLAN_MONTHLY_OTHER_BINDING_SOURCE;
                }
            }


            // Багц буцааж байна
            private static readonly PlanDataSetTableAdapters.PackageDetialOtherTableAdapter INSTANCE_PLAN_PACKAGE_DETIAL_OTHER_TABLE_ADAPTER = new PlanDataSetTableAdapters.PackageDetialOtherTableAdapter();
            public static PlanDataSetTableAdapters.PackageDetialOtherTableAdapter PackageDetialOtherTableAdapter
            {
                get
                {
                    return INSTANCE_PLAN_PACKAGE_DETIAL_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PACKAGE_DETIAL_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource PackageDetialOtherBindingSource
            {
                get
                {
                    return INSTANCE_PACKAGE_DETIAL_OTHER_BINDING_SOURCE;
                }
            }
            
        
            // Багц буцааж байна
            private static readonly PlanDataSetTableAdapters.PackageOtherTableAdapter INSTANCE_PLAN_PACKAGE_OTHER_TABLE_ADAPTER = new PlanDataSetTableAdapters.PackageOtherTableAdapter();
            public static PlanDataSetTableAdapters.PackageOtherTableAdapter PackageOtherTableAdapter
            {
                get
                {
                    return INSTANCE_PLAN_PACKAGE_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PACKAGE_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource PackageOtherBindingSource
            {
                get
                {
                    return INSTANCE_PACKAGE_OTHER_BINDING_SOURCE;
                }
            }


            // Төлөвлөгөө.
            private static readonly PlanDataSetTableAdapters.PlanTableAdapter INSTANCE_PLAN_TABLE_ADAPTER = new PlanDataSetTableAdapters.PlanTableAdapter();
            public static PlanDataSetTableAdapters.PlanTableAdapter PlanTableAdapter
            {
                get
                {
                    return INSTANCE_PLAN_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PLAN_BINDING_SOURCE = new BindingSource();
            public static BindingSource PlanBindingSource
            {
                get
                {
                    return INSTANCE_PLAN_BINDING_SOURCE;
                }
            }


            // Эмийн түүвэр
            private static readonly PlanDataSetTableAdapters.DrugJournalTableAdapter INSTANCE_DRUG_JOURNAL_TABLE_ADAPTER = new PlanDataSetTableAdapters.DrugJournalTableAdapter();
            public static PlanDataSetTableAdapters.DrugJournalTableAdapter DrugJournalTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_JOURNAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_DRUG_JOURNAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugJournalBindingSource
            {
                get
                {
                    return INSTANCE_DRUG_JOURNAL_BINDING_SOURCE;
                }
            }


            // Эмийн түүврийн эм
            private static readonly PlanDataSetTableAdapters.DrugDetailTableAdapter INSTANCE_DRUG_DETAIL_TABLE_ADAPTER = new PlanDataSetTableAdapters.DrugDetailTableAdapter();
            public static PlanDataSetTableAdapters.DrugDetailTableAdapter DrugDetailTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_DETAIL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_DRUG_DETAIL_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugDetailBindingSource
            {
                get
                {
                    return INSTANCE_DRUG_DETAIL_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_DRUG_DETAIL_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugDetailOtherBindingSource
            {
                get
                {
                    return INSTANCE_DRUG_DETAIL_OTHER_BINDING_SOURCE;
                }
            }

                    
            // Эмчлүүлэгч
            private static readonly PlanDataSetTableAdapters.PatientDrugTableAdapter INSTANCE_PATIENT_DRUG_TABLE_ADAPTER = new PlanDataSetTableAdapters.PatientDrugTableAdapter();
            public static PlanDataSetTableAdapters.PatientDrugTableAdapter PatientDrugTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_DRUG_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_DRUG_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientDrugBindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_DRUG_BINDING_SOURCE;
                }
            }


            // Тендерийн төлөвлөгөө
            private static readonly PlanDataSetTableAdapters.TenderPlanTableAdapter INSTANCE_TENDER_PLAN_TABLE_ADAPTER = new PlanDataSetTableAdapters.TenderPlanTableAdapter();
            public static PlanDataSetTableAdapters.TenderPlanTableAdapter TenderPlanTableAdapter
            {
                get
                {
                    return INSTANCE_TENDER_PLAN_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_TENDER_PLAN_BINDING_SOURCE = new BindingSource();
            public static BindingSource TenderPlanBindingSource
            {
                get
                {
                    return INSTANCE_TENDER_PLAN_BINDING_SOURCE;
                }
            }


            // Тендерийн төлөвлөгөөний эм
            private static readonly PlanDataSetTableAdapters.TenderPlanDetailTableAdapter INSTANCE_TENDER_PLAN_DETAIL_TABLE_ADAPTER = new PlanDataSetTableAdapters.TenderPlanDetailTableAdapter();
            public static PlanDataSetTableAdapters.TenderPlanDetailTableAdapter TenderPlanDetailTableAdapter
            {
                get
                {
                    return INSTANCE_TENDER_PLAN_DETAIL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_TENDER_PLAN_DETAIL_BINDING_SOURCE = new BindingSource();
            public static BindingSource TenderPlanDetailBindingSource
            {
                get
                {
                    return INSTANCE_TENDER_PLAN_DETAIL_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_TENDER_PLAN_DETAIL_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource TenderPlanDetailOtherBindingSource
            {
                get
                {
                    return INSTANCE_TENDER_PLAN_DETAIL_OTHER_BINDING_SOURCE;
                }
            }


            // Төлөвлөгөөний эмийн хэрэгжилт
            private static readonly PlanDataSetTableAdapters.PlanExpTableAdapter INSTANCE_PLAN_EXP_TABLE_ADAPTER = new PlanDataSetTableAdapters.PlanExpTableAdapter();
            public static PlanDataSetTableAdapters.PlanExpTableAdapter PlanExpTableAdapter
            {
                get
                {
                    return INSTANCE_PLAN_EXP_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PLAN_EXP_BINDING_SOURCE = new BindingSource();
            public static BindingSource PlanExpBindingSource
            {
                get
                {
                    return INSTANCE_PLAN_EXP_BINDING_SOURCE;
                }
            }


            // Тендерийн төлөвлөгөөний хэрэгжилт
            private static readonly PlanDataSetTableAdapters.TenderIncTableAdapter INSTANCE_TENDER_INC_TABLE_ADAPTER = new PlanDataSetTableAdapters.TenderIncTableAdapter();
            public static PlanDataSetTableAdapters.TenderIncTableAdapter TenderIncTableAdapter
            {
                get
                {
                    return INSTANCE_TENDER_INC_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_TENDER_INC_BINDING_SOURCE = new BindingSource();
            public static BindingSource TenderIncBindingSource
            {
                get
                {
                    return INSTANCE_TENDER_INC_BINDING_SOURCE;
                }
            }

            // Систэмийн хэрэглэгч үзэх
            private static readonly PlanDataSetTableAdapters.SystemUserTableAdapter INSTANCE_SYSTEM_USER_TABLE_ADAPTER = new PlanDataSetTableAdapters.SystemUserTableAdapter();
            public static PlanDataSetTableAdapters.SystemUserTableAdapter SystemUserTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_USER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUserBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_BINDING_SOURCE;
                }
            }

            // Систэмийн хэрэглэгч үзэх
            private static readonly PlanDataSetTableAdapters.SystemOtherUserTableAdapter INSTANCE_SYSTEM_OTHER_USER_TABLE_ADAPTER = new PlanDataSetTableAdapters.SystemOtherUserTableAdapter();
            public static PlanDataSetTableAdapters.SystemOtherUserTableAdapter SystemOtherUserTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_OTHER_USER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_USER_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemOtherUserBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_OTHER_BINDING_SOURCE;
                }
            }

            //Систем сувилагч
            private static readonly PlanDataSetTableAdapters.SystemNurseTableAdapter INSTANCE_SYSTEM_NURSE_TABLE_ADAPTER = new PlanDataSetTableAdapters.SystemNurseTableAdapter();
            public static PlanDataSetTableAdapters.SystemNurseTableAdapter SystemNurseTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_NURSE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_NURSE_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemNurseBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_NURSE_BINDING_SOURCE;
                }
            }
            //Систем сувилагч №2
            private static readonly PlanDataSetTableAdapters.SystemNurse2TableAdapter INSTANCE_SYSTEM_NURSE_2_TABLE_ADAPTER = new PlanDataSetTableAdapters.SystemNurse2TableAdapter();
            public static PlanDataSetTableAdapters.SystemNurse2TableAdapter SystemNurse2TableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_NURSE_2_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_NURSE_2_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemNurse2BindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_NURSE_2_BINDING_SOURCE;
                }
            }
            //Систем эмч №1
            private static readonly PlanDataSetTableAdapters.SystemDoctor1TableAdapter INSTANCE_SYSTEM_DOCTOR_1_TABLE_ADAPTER = new PlanDataSetTableAdapters.SystemDoctor1TableAdapter();
            public static PlanDataSetTableAdapters.SystemDoctor1TableAdapter SystemDoctor1TableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_DOCTOR_1_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_DOCTOR_1_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemDoctor1BindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_DOCTOR_1_BINDING_SOURCE;
                }
            }

            //Эмийн түүвэр
            private static readonly PlanDataSetTableAdapters.DrugTableAdapter INSTANCE_DRUG_TABLE_ADAPTER = new PlanDataSetTableAdapters.DrugTableAdapter();
            public static PlanDataSetTableAdapters.DrugTableAdapter DrugTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_DRUG_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugBindingSource
            {
                get
                {
                    return INSTANCE_DRUG_BINDING_SOURCE;
                }
            }

            // Эмийн түүвэр сувилагч
            private static readonly PlanDataSetTableAdapters.DrugNurseTableAdapter INSTANCE_DRUG_NURSE_TABLE_ADAPTER = new PlanDataSetTableAdapters.DrugNurseTableAdapter();
            public static PlanDataSetTableAdapters.DrugNurseTableAdapter DrugNurseTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_NURSE_TABLE_ADAPTER;
                }
            }
         
            private static readonly BindingSource INSTANCE_SYSTEM_DRUG_NURSE_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugNurseBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_DRUG_NURSE_BINDING_SOURCE;
                }
            }

            // Эмийн түүвэр сувилагч
            private static readonly PlanDataSetTableAdapters.DrugNurseDetailTableAdapter INSTANCE_DRUG_NURSE_DETAIL_TABLE_ADAPTER = new PlanDataSetTableAdapters.DrugNurseDetailTableAdapter();
            public static PlanDataSetTableAdapters.DrugNurseDetailTableAdapter DrugNurseDetailTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_NURSE_DETAIL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_DRUG_NURSE_DETAIL_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugNurseDetailBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_DRUG_NURSE_DETAIL_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_DRUG_NURSE_DETAIL_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugNurseDetailOtherBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_DRUG_NURSE_DETAIL_OTHER_BINDING_SOURCE;
                }
            }

            // Эмийн түүвэр 
            private static readonly PlanDataSetTableAdapters.DrugDetailOtherTableAdapter INSTANCE_DRUG_DETAIL_OTHER_TABLE_ADAPTER = new PlanDataSetTableAdapters.DrugDetailOtherTableAdapter();
            public static PlanDataSetTableAdapters.DrugDetailOtherTableAdapter DrugDetailOtherTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_DETAIL_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_DRUG_DETAIL_ANOTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource DrugDetailAnotherBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_DRUG_DETAIL_ANOTHER_BINDING_SOURCE;
                }
            }

            //Систем хэрэглэгч №1
            private static readonly PlanDataSetTableAdapters.SystemDoctorTableAdapter INSTANCE_SYSTEM_DOCTOR_TABLE_ADAPTER = new PlanDataSetTableAdapters.SystemDoctorTableAdapter();
            public static PlanDataSetTableAdapters.SystemDoctorTableAdapter SystemDoctorTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_DOCTOR_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_DOCTOR_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemDoctorBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_DOCTOR_BINDING_SOURCE;
                }
            }

            //Систем хэрэглэгч №2
            private static readonly PlanDataSetTableAdapters.SystemNurse1TableAdapter INSTANCE_SYSTEM_NURSE_1_TABLE_ADAPTER = new PlanDataSetTableAdapters.SystemNurse1TableAdapter();
            public static PlanDataSetTableAdapters.SystemNurse1TableAdapter SystemNurse1TableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_NURSE_1_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_SYSTEM_NURSE_1_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemNurse1BindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_NURSE_1_BINDING_SOURCE;
                }
            }

            //Эмчлүүлэгч №1
            private static readonly PlanDataSetTableAdapters.PatientDoctorTableAdapter INSTANCE_PATIENT_DOCTOR_TABLE_ADAPTER = new PlanDataSetTableAdapters.PatientDoctorTableAdapter();
            public static PlanDataSetTableAdapters.PatientDoctorTableAdapter PatientDoctorTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_DOCTOR_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_DOCTOR_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientDoctorBindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_DOCTOR_BINDING_SOURCE;
                }
            }

            //Эмчлүүлэгч №1
            private static readonly PlanDataSetTableAdapters.GetbetweenDateTableAdapter INSTANCE_GET_BETWEEN_DATE_TABLE_ADAPTER = new PlanDataSetTableAdapters.GetbetweenDateTableAdapter();
            public static PlanDataSetTableAdapters.GetbetweenDateTableAdapter GetbetweenDateTableAdapter
            {
                get
                {
                    return INSTANCE_GET_BETWEEN_DATE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_BETWEEN_DATE_BINDING_SOURCE = new BindingSource();
            public static BindingSource GetBetweenDateBindingSource
            {
                get
                {
                    return INSTANCE_BETWEEN_DATE_BINDING_SOURCE;
                }
            }

        #endregion

        public static void initialize()
        {
            ((ISupportInitialize)(Instance)).BeginInit();

            Instance.DataSetName = "PlanDataSet";
            Instance.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;


                // Хэрвээ доорх нэг мөр кодчлол нь алдаатай болсон тохиолдолд тухайн
                // DataSet-ийн designer-ийн код руу нь орж InitCommandCollection() функцийн 
                // 'private' -> 'public' болгон өөрчилнө.
            //QueriesTableAdapter.InitCommandCollection();

            SystemDoctorTableAdapter.Initialize();
            ((ISupportInitialize)(SystemDoctorBindingSource)).BeginInit();
            SystemDoctorBindingSource.DataMember = "SystemDoctor";
            SystemDoctorBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemDoctorBindingSource)).EndInit();

            SystemDoctor1TableAdapter.Initialize();
            ((ISupportInitialize)(SystemDoctor1BindingSource)).BeginInit();
            SystemDoctor1BindingSource.DataMember = "SystemDoctor1";
            SystemDoctor1BindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemDoctor1BindingSource)).EndInit();

            SystemNurse1TableAdapter.Initialize();
            ((ISupportInitialize)(SystemNurse1BindingSource)).BeginInit();
            SystemNurse1BindingSource.DataMember = "SystemNurse1";
            SystemNurse1BindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemNurse1BindingSource)).EndInit();

            SystemNurse2TableAdapter.Initialize();
            ((ISupportInitialize)(SystemNurse2BindingSource)).BeginInit();
            SystemNurse2BindingSource.DataMember = "SystemNurse2";
            SystemNurse2BindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemNurse2BindingSource)).EndInit();

            PatientDoctorTableAdapter.Initialize();
            ((ISupportInitialize)(PatientDoctorBindingSource)).BeginInit();
            PatientDoctorBindingSource.DataMember = "PatientDoctor";
            PatientDoctorBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientDoctorBindingSource)).EndInit();

            MedicineOtherTableAdapter.Initialize();
            ((ISupportInitialize)(MedicineOtherBindingSource)).BeginInit();
            MedicineOtherBindingSource.DataMember = "MedicineOther";
            MedicineOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineOtherBindingSource)).EndInit();

            DrugDoctorOtherTableAdapter.Initialize();
            ((ISupportInitialize)(DrugDoctorOtherBindingSource)).BeginInit();
            DrugDoctorOtherBindingSource.DataMember = "DrugDoctorOther";
            DrugDoctorOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(DrugDoctorOtherBindingSource)).EndInit();

            DrugDoctorDetailOtherTableAdapter.Initialize();
            ((ISupportInitialize)(DrugDoctorDetailAnotherBindingSource)).BeginInit();
            DrugDoctorDetailAnotherBindingSource.DataMember = "DrugDoctorDetailOther";
            DrugDoctorDetailAnotherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(DrugDoctorDetailAnotherBindingSource)).EndInit();

            DrugDoctorTableAdapter.Initialize();
            ((ISupportInitialize)(DrugDoctorBindingSource)).BeginInit();
            DrugDoctorBindingSource.DataMember = "DrugDoctor";
            DrugDoctorBindingSource.DataSource = Instance;
            ((ISupportInitialize)(DrugDoctorBindingSource)).EndInit();

            DrugDoctorDetailTableAdapter.Initialize();
            ((ISupportInitialize)(DrugDoctorDetailBindingSource)).BeginInit();
            DrugDoctorDetailBindingSource.DataMember = "DrugDoctorDetail";
            DrugDoctorDetailBindingSource.DataSource = Instance;
            ((ISupportInitialize)(DrugDoctorDetailBindingSource)).EndInit();

            ((ISupportInitialize)(DrugDoctorDetailOtherBindingSource)).BeginInit();
            DrugDoctorDetailOtherBindingSource.DataMember = "DrugDoctor_DrugDoctorDetail";
            DrugDoctorDetailOtherBindingSource.DataSource = DrugDoctorBindingSource;
            ((ISupportInitialize)(DrugDoctorDetailOtherBindingSource)).EndInit();

            // Төлөвлөгөө
            TenderJournalTableAdapter.Initialize();
            ((ISupportInitialize)(TenderJournalBindingSource)).BeginInit();
            TenderJournalBindingSource.DataMember = "TenderJournal";
            TenderJournalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(TenderJournalBindingSource)).EndInit();

            ((ISupportInitialize)(TenderJournalOtherBindingSource)).BeginInit();
            TenderJournalOtherBindingSource.DataMember = "TenderJournal";
            TenderJournalOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(TenderJournalOtherBindingSource)).EndInit();

            TenderDetailTableAdapter.Initialize();
            ((ISupportInitialize)(TenderDetialBindingSource)).BeginInit();
            TenderDetialBindingSource.DataMember = "TenderDetail";
            TenderDetialBindingSource.DataSource = Instance;
            ((ISupportInitialize)(TenderDetialBindingSource)).EndInit();

            ((ISupportInitialize)(TenderDetialOtherBindingSource)).BeginInit();
            TenderDetialOtherBindingSource.DataMember = "TenderJournal_TenderDetail";
            TenderDetialOtherBindingSource.DataSource = TenderJournalBindingSource;
            ((ISupportInitialize)(TenderDetialOtherBindingSource)).EndInit();

            PlanMonthlyTableAdapter.Initialize();
            ((ISupportInitialize)(PlanMonthlyBindingSource)).BeginInit();
            PlanMonthlyBindingSource.DataMember = "PlanMonthly";
            PlanMonthlyBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PlanMonthlyBindingSource)).EndInit();

            //((ISupportInitialize)(PlanMonthlyOtherBindingSource)).BeginInit();
            //PlanMonthlyOtherBindingSource.DataMember = "Plan_PlanMonthly";
            //PlanMonthlyOtherBindingSource.DataSource = PlanBindingSource;
            //((ISupportInitialize)(PlanMonthlyOtherBindingSource)).EndInit();

            PlanTableAdapter.Initialize();
            ((ISupportInitialize)(PlanBindingSource)).BeginInit();
            PlanBindingSource.DataMember = "Plan";
            PlanBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PlanBindingSource)).EndInit();

            PackageDetialOtherTableAdapter.Initialize();
            ((ISupportInitialize)(PackageDetialOtherBindingSource)).BeginInit();
            PackageDetialOtherBindingSource.DataMember = "PackageDetialOther";
            PackageDetialOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PackageDetialOtherBindingSource)).EndInit();

            PackageOtherTableAdapter.Initialize();
            ((ISupportInitialize)(PackageOtherBindingSource)).BeginInit();
            PackageOtherBindingSource.DataMember = "PackageOther";
            PackageOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PackageOtherBindingSource)).EndInit();

            DrugJournalTableAdapter.Initialize();
            ((ISupportInitialize)(DrugJournalBindingSource)).BeginInit();
            DrugJournalBindingSource.DataMember = "DrugJournal";
            DrugJournalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(DrugJournalBindingSource)).EndInit();

            DrugDetailTableAdapter.Initialize();
            ((ISupportInitialize)(DrugDetailBindingSource)).BeginInit();
            DrugDetailBindingSource.DataMember = "DrugDetail";
            DrugDetailBindingSource.DataSource = Instance;
            ((ISupportInitialize)(DrugDetailBindingSource)).EndInit();

            ((ISupportInitialize)(DrugDetailOtherBindingSource)).BeginInit();
            DrugDetailOtherBindingSource.DataMember = "DrugJournal_DrugDetail";
            DrugDetailOtherBindingSource.DataSource = DrugJournalBindingSource;
            ((ISupportInitialize)(DrugDetailOtherBindingSource)).EndInit();

            PatientDrugTableAdapter.Initialize();
            ((ISupportInitialize)(PatientDrugBindingSource)).BeginInit();
            PatientDrugBindingSource.DataMember = "PatientDrug";
            PatientDrugBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientDrugBindingSource)).EndInit();

            TenderPlanTableAdapter.Initialize();
            ((ISupportInitialize)(TenderPlanBindingSource)).BeginInit();
            TenderPlanBindingSource.DataMember = "TenderPlan";
            TenderPlanBindingSource.DataSource = Instance;
            ((ISupportInitialize)(TenderPlanBindingSource)).EndInit();

            TenderPlanDetailTableAdapter.Initialize();
            ((ISupportInitialize)(TenderPlanDetailBindingSource)).BeginInit();
            TenderPlanDetailBindingSource.DataMember = "TenderPlanDetail";
            TenderPlanDetailBindingSource.DataSource = Instance;
            ((ISupportInitialize)(TenderPlanDetailBindingSource)).EndInit();

            ((ISupportInitialize)(TenderPlanDetailOtherBindingSource)).BeginInit();
            TenderPlanDetailOtherBindingSource.DataMember = "TenderPlan_TenderPlanDetail";
            TenderPlanDetailOtherBindingSource.DataSource = TenderPlanBindingSource;
            ((ISupportInitialize)(TenderPlanDetailOtherBindingSource)).EndInit();

            PlanExpTableAdapter.Initialize();
            ((ISupportInitialize)(PlanExpBindingSource)).BeginInit();
            PlanExpBindingSource.DataMember = "PlanExp";
            PlanExpBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PlanExpBindingSource)).EndInit();

            TenderIncTableAdapter.Initialize();
            ((ISupportInitialize)(TenderIncBindingSource)).BeginInit();
            TenderIncBindingSource.DataMember = "TenderInc";
            TenderIncBindingSource.DataSource = Instance;
            ((ISupportInitialize)(TenderIncBindingSource)).EndInit();

            SystemUserTableAdapter.Initialize();
            ((ISupportInitialize)(SystemUserBindingSource)).BeginInit();
            SystemUserBindingSource.DataMember = "SystemUser";
            SystemUserBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUserBindingSource)).EndInit();

            SystemOtherUserTableAdapter.Initialize();
            ((ISupportInitialize)(SystemOtherUserBindingSource)).BeginInit();
            SystemOtherUserBindingSource.DataMember = "SystemOtherUser";
            SystemOtherUserBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemOtherUserBindingSource)).EndInit();

            DrugNurseTableAdapter.Initialize();
            ((ISupportInitialize)(DrugNurseBindingSource)).BeginInit();
            DrugNurseBindingSource.DataMember = "DrugNurse";
            DrugNurseBindingSource.DataSource = Instance;
            ((ISupportInitialize)(DrugNurseBindingSource)).EndInit();

            DrugNurseDetailTableAdapter.Initialize();
            ((ISupportInitialize)(DrugNurseDetailBindingSource)).BeginInit();
            DrugNurseDetailBindingSource.DataMember = "DrugNurseDetail";
            DrugNurseDetailBindingSource.DataSource = Instance;
            ((ISupportInitialize)(DrugNurseDetailBindingSource)).EndInit();

            ((ISupportInitialize)(DrugNurseDetailOtherBindingSource)).BeginInit();
            DrugNurseDetailOtherBindingSource.DataMember = "DrugNurse_DrugNurseDetail";
            DrugNurseDetailOtherBindingSource.DataSource = DrugNurseBindingSource;
            ((ISupportInitialize)(DrugNurseDetailOtherBindingSource)).EndInit();

            DrugTableAdapter.Initialize();
            ((ISupportInitialize)(DrugBindingSource)).BeginInit();
            DrugBindingSource.DataMember = "Drug";
            DrugBindingSource.DataSource = Instance;
            ((ISupportInitialize)(DrugBindingSource)).EndInit();

            SystemNurseTableAdapter.Initialize();
            ((ISupportInitialize)(SystemNurseBindingSource)).BeginInit();
            SystemNurseBindingSource.DataMember = "SystemNurse";
            SystemNurseBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemNurseBindingSource)).EndInit();

            DrugDetailOtherTableAdapter.Initialize();
            ((ISupportInitialize)(DrugDetailAnotherBindingSource)).BeginInit();
            DrugDetailAnotherBindingSource.DataMember = "DrugDetailOther";
            DrugDetailAnotherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(DrugDetailAnotherBindingSource)).EndInit();

            GetbetweenDateTableAdapter.Initialize();
            ((ISupportInitialize)(GetBetweenDateBindingSource)).BeginInit();
            GetBetweenDateBindingSource.DataMember = "GetbetweenDate";
            GetBetweenDateBindingSource.DataSource = Instance;
            ((ISupportInitialize)(GetBetweenDateBindingSource)).EndInit();

            if (Instance.OrderState.Rows.Count == 0)
            {
                for (int i = 0; i < orderType.Length; i++)
                {
                    DataRow row = Instance.OrderState.NewRow();
                    row["id"] = i + 1;
                    row["name"] = orderType[i];
                    row.EndEdit();
                    Instance.OrderState.Rows.Add(row);
                }
            }

            ((ISupportInitialize)(Instance)).EndInit();
        }

        public static void changeConnectionString()
        {
            TenderJournalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            TenderDetailTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PlanMonthlyTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PlanTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PackageDetialOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PackageOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugJournalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugDetailTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PatientDrugTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PlanExpTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            TenderIncTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemOtherUserTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugNurseTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugNurseDetailTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemNurseTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugDetailOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugDoctorTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugDoctorDetailTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MedicineOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugDoctorDetailOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugDoctorOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PatientDoctorTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemNurse1TableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemDoctorTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemNurse2TableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemDoctor1TableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            GetbetweenDateTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
        }
    }
}
