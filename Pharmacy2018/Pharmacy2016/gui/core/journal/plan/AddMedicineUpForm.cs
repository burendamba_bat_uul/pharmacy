﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;




namespace Pharmacy2016.gui.core.journal.plan
{
    public partial class AddMedicineUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static AddMedicineUpForm INSTANCE = new AddMedicineUpForm();

            public static AddMedicineUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private AddMedicineUpForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                initError();   
                initDataSource();
                reload();  
            }

            private void initDataSource()
            {
                //gridControlMedicine.DataSource = JournalDataSet.MedicineOtherBindingSource;
                gridControlMedicine.DataSource = UserDataSet.PackageMedicineBindingSource;
            }

            private void initError()
            {
                //gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                //spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                //spinEditSumCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                //dateEditDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    
                    actionType = type;
                    if (actionType == 1)
                    {
                        insert();
                    }

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insert()
            {
                //PlanDataSet.Instance.Plan.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id();
                //PlanDataSet.Instance.Plan.startDateColumn.DefaultValue = DateTime.Now;
                //dateEditDate.EditValue = DateTime.Now;
                //Plan = (DataRowView)PlanDataSet.PlanBindingSource.AddNew();
            }

            private void clearData()
            {
                gridViewMedicine.ClearSelection();
            }

            private void addMedicineUpForm_Shown(object sender, EventArgs e)
            {
                gridViewMedicine.Focus();
            }

        #endregion

        #region Формын event

            private void gridViewMedicine_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }
          
            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }
            
            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

            private void gridViewMedicine_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
            {
                //if (Visible)
                //    selectedRows();
            }

            private void gridViewMedicine_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
            {
                //if (Visible)
                //    selectedRows();
            }

        #endregion

        #region Формын функц

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                if (UserDataSet.Instance.PackageMedicine.Rows.Count == 0)
                    UserDataSet.PackageMedicineTableAdapter.Fill(UserDataSet.Instance.PackageMedicine, HospitalUtil.getHospitalId());
                if (PlanDataSet.Instance.PlanMonthly.Rows.Count == 0)
                    PlanDataSet.PlanMonthlyTableAdapter.Fill(PlanDataSet.Instance.PlanMonthly, HospitalUtil.getHospitalId(), PlanForm.Instance.getStartDate().ToString(), PlanForm.Instance.getEndDate().ToString());
                ProgramUtil.closeWaitDialog();
            }

            private void save()
            {
                List<string> gridRow = PlanMonthlyUpForm.Instance.gridRows();
                if (gridRow.Count > 0)
                {
                    for (int i = 0; i < gridViewMedicine.SelectedRowsCount; i++)
                    {
                        DataRow medicine = gridViewMedicine.GetDataRow(gridViewMedicine.GetSelectedRows()[i]);
                        if (gridRow.IndexOf(medicine["id"].ToString()) < 0)
                        {
                            PlanMonthlyUpForm.Instance.addNewRow(medicine);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < gridViewMedicine.SelectedRowsCount; i++)
                    {   
                        PlanMonthlyUpForm.Instance.addNewRow(gridViewMedicine.GetDataRow(gridViewMedicine.GetSelectedRows()[i]));
                    }
                }
                Close();                
            }

        #endregion                                           
    
    }
}