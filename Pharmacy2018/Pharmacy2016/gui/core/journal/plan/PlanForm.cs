﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.util;
using DevExpress.XtraGrid.Views.Grid;



namespace Pharmacy2016.gui.core.journal.plan
{
    public partial class PlanForm : DevExpress.XtraEditors.XtraForm
    {
       
        #region Форм анх ачааллах функц

           private static PlanForm INSTANCE = new PlanForm();

           public static PlanForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 2009;

            private bool isInit = false;

            public DataRowView currView;

            private PlanForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                RoleUtil.addForm(FORM_ID, this);

                gridControlPlan.DataSource = PlanDataSet.PlanBindingSource;
                gridControlPlanExp.DataSource = PlanDataSet.PlanExpBindingSource;
                gridLookUpEditPlan.Properties.DataSource = PlanDataSet.PlanBindingSource;
                
                gridControlPlanExpMonth.DataSource = PlanDataSet.Instance.PlanExpMonth;

                dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditPlanStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditPlanStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditPlanEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditPlanEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                DateTime date = DateTime.Now;
                dateEditStart.DateTime = new DateTime(date.Year, 1, 1);
                dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            }

        #endregion
        
        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        checkRole();

                        PlanDataSet.Instance.Plan.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        PlanDataSet.Instance.Plan.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        PlanDataSet.Instance.PlanMonthly.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        PlanDataSet.Instance.PlanMonthly.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        reload();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                gridControlPlan.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlPlan.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridControlPlan.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                dropDownButtonPrint.Visible = roles[4];
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;

                if (UserUtil.getUserRoleId() == RoleUtil.TREATMENT_DIRECTOR_ID || UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                    simpleButtonConfirm.Visible = !SystemUtil.SELECTED_DB_READONLY;
                    //gridControlPlan.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = true;
                else
                    simpleButtonConfirm.Visible = false;
                    //gridControlPlan.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = false;

            }

            private void PlanForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                PlanDataSet.Instance.Plan.Clear();
                PlanDataSet.Instance.PlanMonthly.Clear();
            }

        #endregion

        #region Формын event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void simpleButtonConfirm_Click(object sender, EventArgs e)
            {
                if (bandedGridViewPlan.RowCount > 0)
                    confirm();
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                //saveLayout();
            }

            private void gridControlInc_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        add();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && bandedGridViewPlan.GetFocusedRow() != null)
                    {
                        try
                        {
                            delete();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "update") && bandedGridViewPlan.GetFocusedRow() != null)
                    {
                        update();
                    }
                    //else if (String.Equals(e.Button.Tag, "confirm") && bandedGridViewPlan.GetFocusedRow() != null)
                    //{
                    //    confirm();
                    //}
                }
            }

            private void checkEditChange_CheckedChanged(object sender, EventArgs e)
            {
                if (checkEditChange.Checked)
                {
                    gridViewPlanMonthly.OptionsView.ShowPreview = true;
                }
                else
                {
                    gridViewPlanMonthly.OptionsView.ShowPreview = false;
                }
            }

            private void gridLookUpEditTender_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditPlan.EditValue != DBNull.Value && gridLookUpEditPlan.EditValue != null)
                {
                    changePlan();
                }
            }

            private void comboBoxEditMonth_SelectedIndexChanged(object sender, EventArgs e)
            {
                changePlanMonth();
            }

            private void radioGroupJournal_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeJournal();
            }

            private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
            {
                if (xtraTabControl1.SelectedTabPage == xtraTabPagePlan)
                {
                    simpleButtonConfirm.Visible = true;
                }
                else if (xtraTabControl1.SelectedTabPage == xtraTabPagePlanExp)
                {
                    simpleButtonConfirm.Visible = false;
                }
            }

        #endregion

        #region Формын функц

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void reload()
            {
                if (check())
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        PlanDataSet.PlanTableAdapter.Fill(PlanDataSet.Instance.Plan, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        PlanDataSet.PlanMonthlyTableAdapter.Fill(PlanDataSet.Instance.PlanMonthly, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        //PlanDataSet.PlanMonthlyTableAdapter.Fill(PlanDataSet.Instance.PlanMonthly, HospitalUtil.getHospitalId());
                        //PlanDataSet.PackageDetialOtherTableAdapter.Fill(PlanDataSet.Instance.PackageDetialOther, HospitalUtil.getHospitalId());
                        ProgramUtil.closeWaitDialog();
                    }
                    else
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        PlanDataSet.PlanTableAdapter.Fill(PlanDataSet.Instance.Plan, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        PlanDataSet.PlanMonthlyTableAdapter.Fill(PlanDataSet.Instance.PlanMonthly, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);
                        //PlanDataSet.PackageDetialOtherTableAdapter.Fill(PlanDataSet.Instance.PackageDetialOther, HospitalUtil.getHospitalId());
                        ProgramUtil.closeWaitDialog();
                    }
                }
                
            }

            private void add()
            {     
                PlanMonthlyUpForm.Instance.showForm(1, Instance);               
            }

            private void delete()
            {
                if (Convert.ToBoolean(bandedGridViewPlan.GetFocusedRowCellValue("isConfirm")))
                {
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, "Төлөвлөгөөг баталгаажуулсан тул өөрчилж болохгүй");
                    return;
                }

                bool isDeleteDoc = false;
                GridView detail = null;

                if (bandedGridViewPlan.IsFocusedView)
                {
                    isDeleteDoc = true;
                }
                else
                {
                    if (!bandedGridViewPlan.GetMasterRowExpanded(bandedGridViewPlan.FocusedRowHandle))
                        bandedGridViewPlan.SetMasterRowExpanded(bandedGridViewPlan.FocusedRowHandle, true);
                    detail = bandedGridViewPlan.GetDetailView(bandedGridViewPlan.FocusedRowHandle, 0) as GridView;
                    if (detail.IsFocusedView)
                    {
                        isDeleteDoc = detail.RowCount == 1;
                    }
                }
                if (isDeleteDoc)
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Төлөвлөгөө устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        bandedGridViewPlan.DeleteSelectedRows();
                        PlanDataSet.PlanBindingSource.EndEdit();
                        PlanDataSet.PlanTableAdapter.Update(PlanDataSet.Instance.Plan);
                        ProgramUtil.closeWaitDialog();
                    }
                }
                else
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Багц устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        detail.DeleteSelectedRows();
                        PlanDataSet.PlanMonthlyBindingSource.EndEdit();
                        PlanDataSet.PlanMonthlyTableAdapter.Update(PlanDataSet.Instance.PlanMonthly);
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            private void update()
            {
                if (Convert.ToBoolean(bandedGridViewPlan.GetFocusedRowCellValue("isConfirm")))
                {
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, "Төлөвлөгөөг баталгаажуулсан тул өөрчилж болохгүй");
                    return;
                }

                if (PlanDataSet.Instance.Plan.Rows.Count > 0)
                {
                    if (!bandedGridViewPlan.GetMasterRowExpanded(bandedGridViewPlan.FocusedRowHandle))
                        bandedGridViewPlan.SetMasterRowExpanded(bandedGridViewPlan.FocusedRowHandle, true);

                    PlanMonthlyUpForm.Instance.showForm(2, Instance);
                }
            }

            private void confirm()
            {
                PlanMonthlyUpForm.Instance.showForm(3, Instance);
            }

            public DataRowView getSelectedRow()
            {
                bandedGridViewPlan.SetMasterRowExpanded(bandedGridViewPlan.FocusedRowHandle, true);
                GridView detail = bandedGridViewPlan.GetDetailView(bandedGridViewPlan.FocusedRowHandle, 0) as GridView;
                string id = detail.GetFocusedDataRow()["id"].ToString();
                for (int i = 0; i < PlanDataSet.PlanBindingSource.Count; i++)
                {
                    DataRowView view = (DataRowView)PlanDataSet.PlanBindingSource[i];
                    if (view["id"].ToString().Equals(id))
                        return view;
                }
                return null;
            }

            public DateTime getStartDate()
            {
                return dateEditStart.DateTime;
            }

            public DateTime getEndDate()
            {
                return dateEditEnd.DateTime;
            }


            private void changeJournal()
            {
                if (radioGroupJournal.SelectedIndex == 0)
                {
                    labelControlMonth.Visible = true;
                    comboBoxEditMonth.Visible = true;
                    gridControlPlanExpMonth.Visible = true;
                    gridControlPlanExp.Visible = false;
                }
                else if (radioGroupJournal.SelectedIndex == 1)
                {
                    labelControlMonth.Visible = false;
                    comboBoxEditMonth.Visible = false;
                    gridControlPlanExpMonth.Visible = false;
                    gridControlPlanExp.Visible = true;
                }
            }

            private void changePlan()
            {
                DataRowView view = (DataRowView)gridLookUpEditPlan.GetSelectedDataRow();
                dateEditPlanStart.EditValue = view["startDate"];
                dateEditPlanEnd.EditValue = view["endDate"];

                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                PlanDataSet.PlanExpTableAdapter.Fill(PlanDataSet.Instance.PlanExp, HospitalUtil.getHospitalId(), gridLookUpEditPlan.EditValue.ToString());
                ProgramUtil.closeWaitDialog();
                if (radioGroupJournal.SelectedIndex == 0)
                    changePlanMonth();
            }

            private void changePlanMonth()
            {
                if (radioGroupJournal.SelectedIndex == 0)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    
                    PlanDataSet.Instance.PlanExpMonth.Clear();
                    for (int i = 0; i < PlanDataSet.Instance.PlanExp.Rows.Count; i++)
                    {
                        DataRow row = PlanDataSet.Instance.PlanExpMonth.NewRow();
                        row["medicineID"] = PlanDataSet.Instance.PlanExp.Rows[i]["medicineID"];
                        row["medicineName"] = PlanDataSet.Instance.PlanExp.Rows[i]["medicineName"];
                        row["latinName"] = PlanDataSet.Instance.PlanExp.Rows[i]["latinName"];
                        row["medicineTypeName"] = PlanDataSet.Instance.PlanExp.Rows[i]["medicineTypeName"];
                        row["unitType"] = PlanDataSet.Instance.PlanExp.Rows[i]["unitType"];
                        row["shape"] = PlanDataSet.Instance.PlanExp.Rows[i]["shape"];
                        row["quantity"] = PlanDataSet.Instance.PlanExp.Rows[i]["quantity"];
                        row["price"] = PlanDataSet.Instance.PlanExp.Rows[i]["price"];

                        if (comboBoxEditMonth.SelectedIndex == 0)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["jan"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["janExp"];
                            row["countInc"] = PlanDataSet.Instance.PlanExp.Rows[i]["janExp"];
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 1)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["feb"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["febExp"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["janExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["febExp"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 2)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["mar"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["marExp"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["janExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["febExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["marExp"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 3)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["apr"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["aprExp"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["janExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["febExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["marExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["aprExp"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 4)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["may"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["mayExp"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["janExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["febExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["marExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["aprExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["mayExp"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 5)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["jun"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["junExp"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["janExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["febExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["marExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["aprExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["mayExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["junExp"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 6)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["jul"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["julExp"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["janExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["febExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["marExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["aprExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["mayExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["junExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["julExp"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 7)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["aug"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["augExp"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["janExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["febExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["marExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["aprExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["mayExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["junExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["julExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["augExp"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 8)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["sep"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["sepExp"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["janExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["febExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["marExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["aprExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["mayExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["junExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["julExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["augExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["sepExp"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 9)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["oct"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["octExp"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["janExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["febExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["marExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["aprExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["mayExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["junExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["julExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["augExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["sepExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["octExp"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 10)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["nov"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["novExp"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["janExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["febExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["marExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["aprExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["mayExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["junExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["julExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["augExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["sepExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["octExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["novExp"]);
                        }
                        else if (comboBoxEditMonth.SelectedIndex == 11)
                        {
                            row["countPlan"] = PlanDataSet.Instance.PlanExp.Rows[i]["dec"];
                            row["count"] = PlanDataSet.Instance.PlanExp.Rows[i]["decExp"];
                            row["countInc"] = Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["janExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["febExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["marExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["aprExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["mayExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["junExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["julExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["augExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["sepExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["octExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["novExp"]) + Convert.ToDouble(PlanDataSet.Instance.PlanExp.Rows[i]["decExp"]);
                        }

                        row["countDis"] = Convert.ToDouble(row["countPlan"]) - Convert.ToDouble(row["count"]);
                        row.EndEdit();
                        PlanDataSet.Instance.PlanExpMonth.Rows.Add(row);
                    }

                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion              

            private void gridControlPlan_ViewRegistered(object sender, DevExpress.XtraGrid.ViewOperationEventArgs e)
            {               
                if (Convert.ToDouble((e.View as GridView).Columns["jan"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["jan"].Visible = true;                                    
                else (e.View as GridView).Columns["jan"].Visible = false;
                if (Convert.ToDouble((e.View as GridView).Columns["feb"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["feb"].Visible = true;                    
                else (e.View as GridView).Columns["feb"].Visible = false;
                if (Convert.ToDouble((e.View as GridView).Columns["mar"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["mar"].Visible = true;                    
                else (e.View as GridView).Columns["mar"].Visible = false;
                if (Convert.ToDouble((e.View as GridView).Columns["apr"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["apr"].Visible = true;                    
                else (e.View as GridView).Columns["apr"].Visible = false;
                if (Convert.ToDouble((e.View as GridView).Columns["may"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["may"].Visible = true;                    
                else (e.View as GridView).Columns["may"].Visible = false;
                if (Convert.ToDouble((e.View as GridView).Columns["jun"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["jun"].Visible = true;                
                else (e.View as GridView).Columns["jun"].Visible = false;
                if (Convert.ToDouble((e.View as GridView).Columns["jul"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["jul"].Visible = true;
                else (e.View as GridView).Columns["jul"].Visible = false;
                if (Convert.ToDouble((e.View as GridView).Columns["aug"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["aug"].Visible = true;
                else (e.View as GridView).Columns["aug"].Visible = false;
                if (Convert.ToDouble((e.View as GridView).Columns["sep"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["sep"].Visible = true;
                else (e.View as GridView).Columns["sep"].Visible = false;
                if (Convert.ToDouble((e.View as GridView).Columns["oct"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["oct"].Visible = true;
                else (e.View as GridView).Columns["oct"].Visible = false;
                if (Convert.ToDouble((e.View as GridView).Columns["nov"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["nov"].Visible = true;
                else (e.View as GridView).Columns["nov"].Visible = false;
                if (Convert.ToDouble((e.View as GridView).Columns["dec"].SummaryItem.SummaryValue) > 0)
                   (e.View as GridView).Columns["dec"].Visible = true;
                else (e.View as GridView).Columns["dec"].Visible = false;                    
            }
    }
}