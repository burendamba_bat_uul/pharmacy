﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.journal.tender;
using Pharmacy2016.gui.core.info.ds;
using DevExpress.XtraEditors.Controls;

namespace Pharmacy2016.gui.core.journal.plan
{
    public partial class PlanMonthlyUpForm : DevExpress.XtraEditors.XtraForm
    {
        
        #region Форм анх ачааллах функц

            private static PlanMonthlyUpForm INSTANCE = new PlanMonthlyUpForm();

            public static PlanMonthlyUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            public Int32 dividedNum;

            public DataRowView Months;

            public DataRowView Plan;

            private PlanMonthlyUpForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;

                repositoryItemSpinEditCount.MaxValue = Int32.MaxValue - 1;
                repositoryItemSpinEdit1.MaxValue = Int32.MaxValue - 1;

                dateEditStartDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStartDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEndDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEndDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                initError();   
                initDataSource();
                initBinding();
                reload();  
            }

            private void initDataSource()
            {
                gridControlDivide.DataSource = PlanDataSet.Instance.PlanMonthly;
                repositoryItemGridLookUpEditMed.DataSource = UserDataSet.PackageMedicineBindingSource;
            }

            private void initError()
            {
                textEditName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditStartDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditEndDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

            private void initBinding()
            {
                textEditName.DataBindings.Add(new Binding("EditValue", PlanDataSet.PlanBindingSource, "name", true));
                //spinEditSumCount.DataBindings.Add(new Binding("EditValue", PlanDataSet.PlanBindingSource, "sumCount", true));
                dateEditEndDate.DataBindings.Add(new Binding("EditValue", PlanDataSet.PlanBindingSource, "endDate", true));
                dateEditStartDate.DataBindings.Add(new Binding("EditValue", PlanDataSet.PlanBindingSource, "startDate", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", PlanDataSet.PlanBindingSource, "description", true));
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    
                    actionType = type;
                    if (actionType == 1)
                    {
                        insert();
                    }
                    else if (actionType == 2)
                    {
                        update();
                    }
                    else if (actionType == 3)
                    {
                        require();
                    }

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insert()
            {
                DateTime date = DateTime.Now;
                DateTime now = new DateTime(date.Year, 1, 1);
                DateTime end = new DateTime(date.Year, 12, 31);
                labelControlConfirm.Visible = false;
                checkEditIsConfirm.Visible = false;
                textEditName.Properties.ReadOnly = false;
                dateEditEndDate.Properties.ReadOnly = false;
                dateEditStartDate.Properties.ReadOnly = false;
                memoEditDescription.Properties.ReadOnly = false;
                checkedComboBoxEdit.Properties.ReadOnly = false;
                gridViewDivide.OptionsBehavior.ReadOnly = false;
                buttonCal.Visible = true;
                gridControlDivide.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
                gridControlDivide.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;
                gridViewDivide.Columns["medicineID"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["price"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["count"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["jan"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["feb"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["mar"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["apr"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["may"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["jun"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["jul"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["aug"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["sep"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["oct"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["nov"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["dec"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;

                PlanDataSet.Instance.Plan.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                Plan = (DataRowView)PlanDataSet.PlanBindingSource.AddNew();
                Plan["startDate"] = now; 
                Plan["endDate"] = end;
                Plan.EndEdit();
            }

            private void update()
            {
                labelControlConfirm.Visible = false;
                checkEditIsConfirm.Visible = false;
                textEditName.Properties.ReadOnly = false;
                dateEditEndDate.Properties.ReadOnly = false;
                dateEditStartDate.Properties.ReadOnly = false;
                memoEditDescription.Properties.ReadOnly = false;
                checkedComboBoxEdit.Properties.ReadOnly = false;
                gridViewDivide.OptionsBehavior.ReadOnly = false;
                buttonCal.Visible = true;
                gridControlDivide.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
                gridControlDivide.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;
                gridViewDivide.Columns["medicineID"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["price"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["count"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["jan"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["feb"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["mar"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["apr"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["may"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["jun"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["jul"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["aug"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["sep"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["oct"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["nov"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                gridViewDivide.Columns["dec"].AppearanceCell.BackColor = Color.LightGoldenrodYellow;

                Plan = (DataRowView)PlanDataSet.PlanBindingSource.Current;
                checkEditIsConfirm.EditValue = Plan["isConfirm"];
            }

            private void require()
            {
                labelControlConfirm.Visible = true;
                checkEditIsConfirm.Visible = true;
                textEditName.Properties.ReadOnly = true;
                dateEditEndDate.Properties.ReadOnly = true;
                dateEditStartDate.Properties.ReadOnly = true;
                memoEditDescription.Properties.ReadOnly = true;
                checkedComboBoxEdit.Properties.ReadOnly = true;
                gridViewDivide.OptionsBehavior.ReadOnly = true;
                buttonCal.Visible = false;
                gridControlDivide.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
                gridControlDivide.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
                gridViewDivide.Columns["medicineID"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["price"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["count"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["jan"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["feb"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["mar"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["apr"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["may"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["jun"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["jul"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["aug"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["sep"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["oct"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["nov"].AppearanceCell.BackColor = Color.Transparent;
                gridViewDivide.Columns["dec"].AppearanceCell.BackColor = Color.Transparent;

                checkEditIsConfirm.Focus();
                Plan = (DataRowView)PlanDataSet.PlanBindingSource.Current;
                checkEditIsConfirm.EditValue = Plan["isConfirm"];   
            }

            private void clearData()
            {
                clearErrorText();
                //ClearAndClose();
                checkedComboBoxEdit.SetEditValue(null);
                PlanDataSet.PlanMonthlyBindingSource.CancelEdit();
                PlanDataSet.Instance.PlanMonthly.RejectChanges();
                PlanDataSet.PlanBindingSource.CancelEdit();
                PlanDataSet.Instance.Plan.RejectChanges();
            }

            private void clearErrorText()
            {
                dateEditStartDate.ErrorText = "";
                //gridLookUpEditMedicine.ErrorText = "";
                dateEditEndDate.ErrorText = "";
                textEditName.ErrorText = "";
                gridViewDivide.ClearColumnErrors();
            }

            private void PlanMonthlyUpForm_Shown(object sender, EventArgs e)
            {
                textEditName.Focus();
                if (Plan != null)
                {
                    gridViewDivide.ActiveFilterString = "planID = '" + Plan["id"] + "'";
                }
            }

        #endregion

        #region Формын event

            private void dateEditStartDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditStartDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        dateEditEndDate.Focus();
                    }
                }
            }

            private void dateEditEndDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditEndDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        memoEditDescription.Focus();
                    }
                }
            }

            private void gridViewDivide_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }
          
            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }
            
            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

            private void gridViewDivide_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
            {
                //if (Visible)
                //    selectedRows();
            }

            private void gridControlDivide_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addMultiple();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewDivide.GetFocusedRow() != null)
                    {
                        try
                        {
                            delete();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                }
            }

            private void gridViewDivide_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
            {
                //if(gridViewDivide.GetFocusedRow() != null)
                //    getFocusedRow();
            }

            private void checkedComboBoxEdit_EditValueChanged(object sender, EventArgs e)
            {
                //if (checkedComboBoxEdit.EditValue != null || checkedComboBoxEdit.EditValue != DBNull.Value || !checkedComboBoxEdit.EditValue.ToString().Equals(""))
                //    selectedRows();
            }

            private void buttonCal_Click(object sender, EventArgs e)
            {
                if (checkedComboBoxEdit.EditValue != null || checkedComboBoxEdit.EditValue != DBNull.Value || !checkedComboBoxEdit.EditValue.ToString().Equals(""))
                    selectedRows();
            }

        #endregion

        #region Формын функц

            private void selectedRows()
            {
                for (int i = 0; i < gridViewDivide.SelectedRowsCount; i++)
                {
                    if (gridViewDivide.GetSelectedRows()[i] >= 0)
                    {
                        DataRow row = gridViewDivide.GetDataRow(gridViewDivide.GetSelectedRows()[i]);
                    
                        var ids = (from CheckedListBoxItem item in checkedComboBoxEdit.Properties.Items
                           where item.CheckState == CheckState.Checked
                           select Convert.ToString(item.Value)).ToArray();

                        if (row["count"] != DBNull.Value && ids.Length > 0)
                        {
                            dividedNum = Convert.ToInt32(row["count"].ToString()) / ids.Length;                           
                        }
                        for (var j = 0; j < checkedComboBoxEdit.Properties.Items.Count; j++)
                        {
                            if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("jan"))
                            {
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                    row["jan"] = dividedNum;
                                else
                                    row["jan"] = 0;
                            }
                            else if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("feb"))
                            {
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                    row["feb"] = dividedNum;
                                else
                                    row["feb"] = 0;
                            }
                            else if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("mar"))
                            {
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                    row["mar"] = dividedNum;
                                else
                                    row["mar"] = 0;
                            }
                            else if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("apr"))
                            {
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                    row["apr"] = dividedNum;
                                else
                                    row["apr"] = 0;
                            }
                            else if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("may"))
                            {
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                    row["may"] = dividedNum;
                                else
                                    row["may"] = 0;
                            }
                            else if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("jun"))
                            {
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                    row["jun"] = dividedNum;
                                else
                                    row["jun"] = 0;
                            }
                            else if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("jul"))
                            {
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                    row["jul"] = dividedNum;
                                else
                                    row["jul"] = 0;
                            }
                            else if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("aug"))
                            {
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                    row["aug"] = dividedNum;
                                else
                                    row["aug"] = 0;
                            }
                            else if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("sep"))
                            {
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                    row["sep"] = dividedNum;
                                else
                                    row["sep"] = 0;
                            }
                            else if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("oct"))
                            {
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                    row["oct"] = dividedNum;
                                else
                                    row["oct"] = 0;
                            }
                            else if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("nov"))
                            {
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                    row["nov"] = dividedNum;
                                else
                                    row["nov"] = 0;
                            }
                            else if (checkedComboBoxEdit.Properties.Items[j].Value.ToString().Equals("dec"))
                            {
                                row["dec"] = dividedNum;
                                if (checkedComboBoxEdit.Properties.Items[j].CheckState == CheckState.Checked)
                                {
                                    if (!Convert.ToInt32(row["count"]).Equals(Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(row["dec"])))
                                    {
                                        //XtraMessageBox.Show(Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(row["dec"]) + "");
                                        Int32 plusNum = Convert.ToInt32(row["count"]) - (Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(row["dec"]));
                                        row["dec"] = plusNum + dividedNum;
                                    }
                                    else
                                    {
                                        row["dec"] = dividedNum;
                                    }      
                                }
                                else
                                {
                                    if (!Convert.ToInt32(row["count"]).Equals(Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(dividedNum)))
                                    {
                                        //XtraMessageBox.Show(Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(row["dec"]) + "");
                                        Int32 plusNum = Convert.ToInt32(row["count"]) - (Convert.ToInt32(row["jan"]) + Convert.ToInt32(row["feb"]) + Convert.ToInt32(row["mar"]) + Convert.ToInt32(row["apr"]) + Convert.ToInt32(row["may"]) + Convert.ToInt32(row["jun"]) + Convert.ToInt32(row["jul"]) + Convert.ToInt32(row["aug"]) + Convert.ToInt32(row["sep"]) + Convert.ToInt32(row["oct"]) + Convert.ToInt32(row["nov"]) + Convert.ToInt32(dividedNum));
                                        row["dec"] = dividedNum + plusNum;
                                    }
                                    else
                                    {
                                        row["dec"] = 0;
                                    }  
                                }      
                            }
                            
                            row.EndEdit();
                        }          
                    }
                    else
                    {
                        DataRow row = gridViewDivide.GetDataRow(i);   
                        row["jan"] = 0;
                        row["feb"] = 0;
                        row["mar"] = 0;
                        row["apr"] = 0;
                        row["may"] = 0;
                        row["jun"] = 0;
                        row["jul"] = 0;
                        row["aug"] = 0;
                        row["sep"] = 0;
                        row["oct"] = 0;
                        row["nov"] = 0;
                        row["dec"] = 0;
                        row.EndEdit();
                    }
                }
            }

            private void reload()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                //if (PlanDataSet.Instance.PlanMonthly.Rows.Count == 0)
                //    PlanDataSet.PlanMonthlyTableAdapter.Fill(PlanDataSet.Instance.PlanMonthly, HospitalUtil.getHospitalId());
                if (UserDataSet.Instance.Medicine.Rows.Count == 0)
                    UserDataSet.MedicineTableAdapter.Fill(UserDataSet.Instance.Medicine, HospitalUtil.getHospitalId());
                ProgramUtil.closeWaitDialog();
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (textEditName.EditValue == null || textEditName.EditValue == DBNull.Value || textEditName.EditValue.ToString().Equals(""))
                {
                    errorText = "Төлөвлөгөөний нэрийг оруулна уу!";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditName.ErrorText = errorText;
                    isRight = false;
                    textEditName.Focus();
                }
                if (dateEditStartDate.EditValue == DBNull.Value || dateEditStartDate.EditValue == null || dateEditStartDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Эхлэх огноог оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStartDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditStartDate.Focus();
                    }
                }
                if (dateEditEndDate.EditValue == DBNull.Value || dateEditEndDate.EditValue == null || dateEditEndDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Дуусах огноог оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEndDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEndDate.Focus();
                    }
                }
                if (gridViewDivide.RowCount <= 0)
                {
                    errorText = "Төлөвлөгөөнд эм оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    //dateEditStartDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        //dateEditStartDate.Focus();
                    }
                }
                if (isRight)
                {
                    for (int j = 0; j < gridViewDivide.RowCount; j++)
                    {
                        if (gridViewDivide.GetRowCellValue(j, gridColumnCount) != null && gridViewDivide.GetRowCellValue(j, gridColumnCount) != DBNull.Value && Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnCount)) > 0 &&
                                !Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnCount)).Equals(Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnJan)) + Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnFeb)) + Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnMar)) + Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnApr)) + Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnMay)) + Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnJun)) + Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnJul)) + Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnAug)) + Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnSep)) + Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnOct)) + Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnNov)) + Convert.ToInt32(gridViewDivide.GetRowCellValue(j, gridColumnDec))))
                        {
                            errorText = "Жилийн тоо, саруудад хуваасан тоонууд тэнцэхгүй байна!";
                            gridViewDivide.SetColumnError(gridColumnCount, errorText);
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            isRight = false;
                            break;
                        }
                    }
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                try
                {
                    if (check())
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        
                        Plan["isConfirm"] = checkEditIsConfirm.EditValue;
                        Plan.EndEdit();

                        PlanDataSet.PlanMonthlyBindingSource.EndEdit();
                        PlanDataSet.PlanMonthlyTableAdapter.Update(PlanDataSet.Instance.PlanMonthly);
                        PlanDataSet.PlanBindingSource.EndEdit();
                        PlanDataSet.PlanTableAdapter.Update(PlanDataSet.Instance.Plan);
                        ProgramUtil.closeWaitDialog();
                        Close();
                    }
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Хадгалахад алдаа гарлаа " + ex.ToString());
                }
            }

            public void addNewRow(DataRow row)
            {
                PlanDataSet.Instance.PlanMonthly.idColumn.DefaultValue = getNotExistId();
                PlanDataSet.Instance.PlanMonthly.planIDColumn.DefaultValue = Plan["id"];
                Months = (DataRowView)PlanDataSet.PlanMonthlyBindingSource.AddNew();
                Months["medicineID"] = row["id"];
                Months["medicineName"] = row["name"];
                Months["latinName"] = row["name"];
                Months["typeName"] = row["medicineTypeName"];
                Months["unit"] = row["unitName"];
                Months["shape"] = row["shapeName"];
                Months["quantity"] = row["quantityName"];
                Months["price"] = 0;//row["price"];
                Months["validDate"] = DBNull.Value;//row["validDate"];
                Months["count"] = 0;
                Months["jan"] = 0;
                Months["feb"] = 0;
                Months["mar"] = 0;
                Months["apr"] = 0;
                Months["may"] = 0;
                Months["jun"] = 0;
                Months["jul"] = 0;
                Months["aug"] = 0;
                Months["sep"] = 0;
                Months["oct"] = 0;
                Months["nov"] = 0;
                Months["dec"] = 0;
                //Months["sumPrice"] = Convert.ToDouble(row["count"]) * Convert.ToDouble(row["price"]);
                Months.EndEdit();
            }

            private string getNotExistId()
            {
                string newID = null;
                DataRow[] rows = null;
                do
                {
                    newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    rows = PlanDataSet.Instance.PlanMonthly.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND planID = '" + Plan["id"] + "' AND id = '" + newID + "'");
                } while (rows != null && rows.Length > 0);

                return newID;
            }

            public List<string> gridRows()
            {
                List<string> rows = new List<string>();
                for (int i = 0; i < gridViewDivide.RowCount; i++)
                {
                    rows.Add(gridViewDivide.GetDataRow(i)["medicineID"].ToString());
                }
                return rows;            
            }

        #endregion      
        
        #region Эмийг нэмэх, засах устгах функцууд,

            private void delete()
            {
                for (int i = 0; i < gridViewDivide.SelectedRowsCount; i++)
                {
                    if (gridViewDivide.GetSelectedRows()[i] > 0)
                    {
                        DialogResult dialogResult = XtraMessageBox.Show("Сонгосон мөрүүдийг устгах уу", "Төлөвлөгөөний эм устгах", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                            gridViewDivide.DeleteSelectedRows();
                            ProgramUtil.closeWaitDialog();
                        }
                        else if(dialogResult == DialogResult.No)
                        {
                            return;
                        }
                    }
                }      
            }

            private void addMultiple()
            {
                checkedComboBoxEdit.SetEditValue(null);
                AddMedicineUpForm.Instance.showForm(1, Instance);
            }

        #endregion               

    }
}