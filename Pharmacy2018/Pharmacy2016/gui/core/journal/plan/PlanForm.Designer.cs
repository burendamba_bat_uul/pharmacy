﻿namespace Pharmacy2016.gui.core.journal.plan
{
    partial class PlanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlanForm));
            this.gridViewPlanMonthly = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditMonth = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlPlan = new DevExpress.XtraGrid.GridControl();
            this.bandedGridViewPlan = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bandedGridView2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn34 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn35 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn36 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn37 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn38 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn39 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn40 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn41 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn42 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn43 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn44 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn45 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonConfirm = new DevExpress.XtraEditors.SimpleButton();
            this.checkEditChange = new DevExpress.XtraEditors.CheckEdit();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPagePlan = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPagePlanExp = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlPlanExpMonth = new DevExpress.XtraGrid.GridControl();
            this.gridViewPlanExpMonth = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand29 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn69 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn70 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn71 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn72 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn73 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn74 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand31 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn76 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bandedGridColumn78 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bandedGridColumn82 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn80 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn102 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn75 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridControlPlanExp = new DevExpress.XtraGrid.GridControl();
            this.gridViewPlanExp = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn43 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn42 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn34 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn35 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn36 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn37 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn38 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn39 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumn40 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControlMonth = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditMonth = new DevExpress.XtraEditors.ComboBoxEdit();
            this.radioGroupJournal = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditPlanEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditPlanStart = new DevExpress.XtraEditors.DateEdit();
            this.gridLookUpEditPlan = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlanMonthly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridViewPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPagePlan.SuspendLayout();
            this.xtraTabPagePlanExp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlanExpMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlanExpMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlanExp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlanExp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMonth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupJournal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPlan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewPlanMonthly
            // 
            this.gridViewPlanMonthly.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn10,
            this.gridColumn9,
            this.gridColumn8,
            this.gridColumn2,
            this.gridColumn1,
            this.gridColumn7,
            this.gridColumn20,
            this.gridColumn19,
            this.gridColumn18,
            this.gridColumn11,
            this.gridColumn21,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn6,
            this.gridColumn22});
            this.gridViewPlanMonthly.GridControl = this.gridControlPlan;
            this.gridViewPlanMonthly.LevelIndent = 1;
            this.gridViewPlanMonthly.Name = "gridViewPlanMonthly";
            this.gridViewPlanMonthly.OptionsBehavior.ReadOnly = true;
            this.gridViewPlanMonthly.OptionsDetail.ShowDetailTabs = false;
            this.gridViewPlanMonthly.OptionsPrint.ExpandAllDetails = true;
            this.gridViewPlanMonthly.OptionsPrint.PrintDetails = true;
            this.gridViewPlanMonthly.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPlanMonthly.OptionsView.ShowFooter = true;
            this.gridViewPlanMonthly.OptionsView.ShowGroupPanel = false;
            this.gridViewPlanMonthly.PreviewFieldName = "preRow";
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Нэр";
            this.gridColumn5.CustomizationCaption = "Эмийн нэр";
            this.gridColumn5.FieldName = "medicineName";
            this.gridColumn5.MinWidth = 100;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 100;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "1-р сар";
            this.gridColumn10.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn10.CustomizationCaption = "Нэгдүгээр сар";
            this.gridColumn10.FieldName = "jan";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jan", "{0:n2}")});
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            // 
            // repositoryItemSpinEditMonth
            // 
            this.repositoryItemSpinEditMonth.AutoHeight = false;
            this.repositoryItemSpinEditMonth.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditMonth.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEditMonth.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditMonth.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEditMonth.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditMonth.Mask.EditMask = "n2";
            this.repositoryItemSpinEditMonth.Name = "repositoryItemSpinEditMonth";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "2-р сар";
            this.gridColumn9.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn9.CustomizationCaption = "Хоёрдугаар сар";
            this.gridColumn9.FieldName = "feb";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "feb", "{0:n2}")});
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 3;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "3-р сар";
            this.gridColumn8.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn8.CustomizationCaption = "Гуравдугаар сар";
            this.gridColumn8.FieldName = "mar";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "mar", "{0:n2}")});
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "4-р сар";
            this.gridColumn2.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn2.CustomizationCaption = "Дөрөвдүгээр сар";
            this.gridColumn2.FieldName = "apr";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "apr", "{0:n2}")});
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 5;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "5-р сар";
            this.gridColumn1.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn1.CustomizationCaption = "Тавдугаар сар";
            this.gridColumn1.FieldName = "may";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "may", "{0:n2}")});
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 6;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "6-р сар";
            this.gridColumn7.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn7.CustomizationCaption = "Зургаадугаар сар";
            this.gridColumn7.FieldName = "jun";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jun", "{0:n2}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "7-р сар";
            this.gridColumn20.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn20.CustomizationCaption = "Долоодугаар сар";
            this.gridColumn20.FieldName = "jul";
            this.gridColumn20.MinWidth = 75;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jul", "{0:n2}")});
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 8;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "8-р сар";
            this.gridColumn19.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn19.CustomizationCaption = "Наймдугаар сар";
            this.gridColumn19.FieldName = "aug";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "aug", "{0:n2}")});
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 9;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "9-р сар";
            this.gridColumn18.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn18.CustomizationCaption = "Есдүгээр сар";
            this.gridColumn18.FieldName = "sep";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sep", "{0:n2}")});
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 10;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "10-р сар";
            this.gridColumn11.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn11.CustomizationCaption = "Аравдугаар сар";
            this.gridColumn11.FieldName = "oct";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "oct", "{0:n2}")});
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 11;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "11-р сар";
            this.gridColumn21.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn21.CustomizationCaption = "Арваннэгдүгээр сар";
            this.gridColumn21.FieldName = "nov";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "nov", "{0:n2}")});
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 12;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "12-р сар";
            this.gridColumn3.ColumnEdit = this.repositoryItemSpinEditMonth;
            this.gridColumn3.CustomizationCaption = "Арванхоёрдугаар сар";
            this.gridColumn3.FieldName = "dec";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "dec", "{0:n2}")});
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 13;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Дүн";
            this.gridColumn4.ColumnEdit = this.repositoryItemSpinEdit2;
            this.gridColumn4.CustomizationCaption = "Нийт дүн";
            this.gridColumn4.DisplayFormat.FormatString = "n2";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn4.FieldName = "sumPrice";
            this.gridColumn4.MinWidth = 100;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.gridColumn4.Width = 100;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Жилд авах багцын тоо";
            this.gridColumn6.ColumnEdit = this.repositoryItemSpinEdit2;
            this.gridColumn6.CustomizationCaption = "Жилд авах багцын тоо";
            this.gridColumn6.DisplayFormat.FormatString = "n2";
            this.gridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn6.FieldName = "count";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Үнэ";
            this.gridColumn22.ColumnEdit = this.repositoryItemSpinEdit2;
            this.gridColumn22.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumn22.FieldName = "price";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            // 
            // gridControlPlan
            // 
            this.gridControlPlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPlan.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPlan.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "update"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, false, "Баталгаажуулах", "confirm")});
            this.gridControlPlan.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInc_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.LevelTemplate = this.gridViewPlanMonthly;
            gridLevelNode1.RelationName = "Plan_PlanMonthly";
            this.gridControlPlan.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlPlan.Location = new System.Drawing.Point(0, 0);
            this.gridControlPlan.MainView = this.bandedGridViewPlan;
            this.gridControlPlan.Name = "gridControlPlan";
            this.gridControlPlan.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit2,
            this.repositoryItemSpinEdit2,
            this.repositoryItemSpinEditMonth});
            this.gridControlPlan.Size = new System.Drawing.Size(778, 387);
            this.gridControlPlan.TabIndex = 6;
            this.gridControlPlan.UseEmbeddedNavigator = true;
            this.gridControlPlan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridViewPlan,
            this.gridView2,
            this.bandedGridView2,
            this.gridViewPlanMonthly});
            this.gridControlPlan.ViewRegistered += new DevExpress.XtraGrid.ViewOperationEventHandler(this.gridControlPlan_ViewRegistered);
            // 
            // bandedGridViewPlan
            // 
            this.bandedGridViewPlan.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.bandedGridViewPlan.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn3,
            this.bandedGridColumn2,
            this.bandedGridColumn1,
            this.bandedGridColumn5,
            this.bandedGridColumn4});
            this.bandedGridViewPlan.GridControl = this.gridControlPlan;
            this.bandedGridViewPlan.LevelIndent = 0;
            this.bandedGridViewPlan.Name = "bandedGridViewPlan";
            this.bandedGridViewPlan.OptionsBehavior.ReadOnly = true;
            this.bandedGridViewPlan.OptionsDetail.ShowDetailTabs = false;
            this.bandedGridViewPlan.OptionsNavigation.AutoFocusNewRow = true;
            this.bandedGridViewPlan.OptionsPrint.ExpandAllDetails = true;
            this.bandedGridViewPlan.OptionsPrint.PrintDetails = true;
            this.bandedGridViewPlan.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridViewPlan.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Төлөвлөгөө";
            this.gridBand1.Columns.Add(this.bandedGridColumn3);
            this.gridBand1.Columns.Add(this.bandedGridColumn2);
            this.gridBand1.Columns.Add(this.bandedGridColumn1);
            this.gridBand1.Columns.Add(this.bandedGridColumn5);
            this.gridBand1.Columns.Add(this.bandedGridColumn4);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 848;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.Caption = "Төлөвлөгөөний нэр";
            this.bandedGridColumn3.CustomizationCaption = "Төлөвлөгөөний нэр";
            this.bandedGridColumn3.FieldName = "name";
            this.bandedGridColumn3.MinWidth = 150;
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.Visible = true;
            this.bandedGridColumn3.Width = 200;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "Эхэлсэн огноо";
            this.bandedGridColumn2.ColumnEdit = this.repositoryItemDateEdit2;
            this.bandedGridColumn2.CustomizationCaption = "Эхэлсэн огноо";
            this.bandedGridColumn2.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.bandedGridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bandedGridColumn2.FieldName = "startDate";
            this.bandedGridColumn2.MinWidth = 100;
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 100;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "Дууссан огноо";
            this.bandedGridColumn1.ColumnEdit = this.repositoryItemDateEdit2;
            this.bandedGridColumn1.CustomizationCaption = "Дууссан огноо";
            this.bandedGridColumn1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.bandedGridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.bandedGridColumn1.FieldName = "endDate";
            this.bandedGridColumn1.MinWidth = 100;
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 100;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.Caption = "Тайлбар";
            this.bandedGridColumn5.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn5.FieldName = "description";
            this.bandedGridColumn5.MinWidth = 200;
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.Visible = true;
            this.bandedGridColumn5.Width = 300;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "Баталгаажуулсан эсэх";
            this.bandedGridColumn4.CustomizationCaption = "Баталгаажуулсан эсэх";
            this.bandedGridColumn4.FieldName = "isConfirm";
            this.bandedGridColumn4.MinWidth = 100;
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 148;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17});
            this.gridView2.GridControl = this.gridControlPlan;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Эмийн нэр";
            this.gridColumn12.FieldName = "medicineName";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 0;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Тун хэмжээ";
            this.gridColumn13.FieldName = "unit";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 1;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Хэлбэр";
            this.gridColumn14.FieldName = "shape";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 2;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Хэмжих нэгж";
            this.gridColumn15.FieldName = "quantity";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 3;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Үнэ";
            this.gridColumn16.CustomizationCaption = "Нэгжийг үнэ";
            this.gridColumn16.FieldName = "medicPrice";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 4;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Хүчинтэй хугацаа";
            this.gridColumn17.ColumnEdit = this.repositoryItemDateEdit2;
            this.gridColumn17.FieldName = "validDate";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 5;
            // 
            // bandedGridView2
            // 
            this.bandedGridView2.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand3,
            this.gridBand9,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13,
            this.gridBand14});
            this.bandedGridView2.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn18,
            this.bandedGridColumn20,
            this.bandedGridColumn23,
            this.bandedGridColumn28,
            this.bandedGridColumn29,
            this.bandedGridColumn30,
            this.bandedGridColumn31,
            this.bandedGridColumn32,
            this.bandedGridColumn33,
            this.bandedGridColumn34,
            this.bandedGridColumn35,
            this.bandedGridColumn36,
            this.bandedGridColumn37,
            this.bandedGridColumn38,
            this.bandedGridColumn41,
            this.bandedGridColumn43,
            this.bandedGridColumn44,
            this.bandedGridColumn45,
            this.bandedGridColumn40,
            this.bandedGridColumn39,
            this.bandedGridColumn42});
            this.bandedGridView2.GridControl = this.gridControlPlan;
            this.bandedGridView2.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", this.bandedGridColumn43, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", this.bandedGridColumn45, "{0:c2}")});
            this.bandedGridView2.LevelIndent = 0;
            this.bandedGridView2.Name = "bandedGridView2";
            this.bandedGridView2.OptionsBehavior.ReadOnly = true;
            this.bandedGridView2.OptionsDetail.ShowDetailTabs = false;
            this.bandedGridView2.OptionsNavigation.AutoFocusNewRow = true;
            this.bandedGridView2.OptionsView.ShowAutoFilterRow = true;
            this.bandedGridView2.OptionsView.ShowFooter = true;
            this.bandedGridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Баримт";
            this.gridBand3.Columns.Add(this.bandedGridColumn18);
            this.gridBand3.Columns.Add(this.bandedGridColumn20);
            this.gridBand3.Columns.Add(this.bandedGridColumn23);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 0;
            this.gridBand3.Width = 259;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.Caption = "Дугаар";
            this.bandedGridColumn18.CustomizationCaption = "Дугаар";
            this.bandedGridColumn18.FieldName = "incJournalID";
            this.bandedGridColumn18.MinWidth = 75;
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            this.bandedGridColumn18.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn18.Visible = true;
            this.bandedGridColumn18.Width = 90;
            // 
            // bandedGridColumn20
            // 
            this.bandedGridColumn20.Caption = "Огноо";
            this.bandedGridColumn20.ColumnEdit = this.repositoryItemDateEdit2;
            this.bandedGridColumn20.CustomizationCaption = "Огноо";
            this.bandedGridColumn20.FieldName = "date";
            this.bandedGridColumn20.MinWidth = 75;
            this.bandedGridColumn20.Name = "bandedGridColumn20";
            this.bandedGridColumn20.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn20.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn20.Visible = true;
            this.bandedGridColumn20.Width = 94;
            // 
            // bandedGridColumn23
            // 
            this.bandedGridColumn23.Caption = "Орлогын төрөл";
            this.bandedGridColumn23.CustomizationCaption = "Орлогын төрөл";
            this.bandedGridColumn23.FieldName = "incTypeName";
            this.bandedGridColumn23.MinWidth = 75;
            this.bandedGridColumn23.Name = "bandedGridColumn23";
            this.bandedGridColumn23.Visible = true;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand9.AppearanceHeader.Options.UseFont = true;
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "Нярав";
            this.gridBand9.Columns.Add(this.bandedGridColumn28);
            this.gridBand9.Columns.Add(this.bandedGridColumn29);
            this.gridBand9.Columns.Add(this.bandedGridColumn30);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 1;
            this.gridBand9.Width = 278;
            // 
            // bandedGridColumn28
            // 
            this.bandedGridColumn28.Caption = "Дугаар";
            this.bandedGridColumn28.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.bandedGridColumn28.FieldName = "userID";
            this.bandedGridColumn28.MinWidth = 75;
            this.bandedGridColumn28.Name = "bandedGridColumn28";
            this.bandedGridColumn28.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn28.Visible = true;
            this.bandedGridColumn28.Width = 91;
            // 
            // bandedGridColumn29
            // 
            this.bandedGridColumn29.Caption = "Нэр";
            this.bandedGridColumn29.CustomizationCaption = "Хэрэглэгчийн нэр";
            this.bandedGridColumn29.FieldName = "userName";
            this.bandedGridColumn29.MinWidth = 75;
            this.bandedGridColumn29.Name = "bandedGridColumn29";
            this.bandedGridColumn29.Visible = true;
            this.bandedGridColumn29.Width = 91;
            // 
            // bandedGridColumn30
            // 
            this.bandedGridColumn30.Caption = "Эрх";
            this.bandedGridColumn30.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.bandedGridColumn30.FieldName = "roleName";
            this.bandedGridColumn30.MinWidth = 75;
            this.bandedGridColumn30.Name = "bandedGridColumn30";
            this.bandedGridColumn30.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn30.Visible = true;
            this.bandedGridColumn30.Width = 96;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand11.AppearanceHeader.Options.UseFont = true;
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "Агуулах";
            this.gridBand11.Columns.Add(this.bandedGridColumn31);
            this.gridBand11.Columns.Add(this.bandedGridColumn32);
            this.gridBand11.CustomizationCaption = "Агуулах";
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 2;
            this.gridBand11.Width = 150;
            // 
            // bandedGridColumn31
            // 
            this.bandedGridColumn31.Caption = "Нэр";
            this.bandedGridColumn31.CustomizationCaption = "Агуулахын нэр";
            this.bandedGridColumn31.FieldName = "warehouseName";
            this.bandedGridColumn31.MinWidth = 75;
            this.bandedGridColumn31.Name = "bandedGridColumn31";
            this.bandedGridColumn31.Visible = true;
            // 
            // bandedGridColumn32
            // 
            this.bandedGridColumn32.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn32.CustomizationCaption = "Агуулахын дэлгэрэнгүй";
            this.bandedGridColumn32.FieldName = "warehouseDesc";
            this.bandedGridColumn32.MinWidth = 75;
            this.bandedGridColumn32.Name = "bandedGridColumn32";
            this.bandedGridColumn32.Visible = true;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand12.AppearanceHeader.Options.UseFont = true;
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "Нэмэлт мэдээлэл";
            this.gridBand12.Columns.Add(this.bandedGridColumn33);
            this.gridBand12.Columns.Add(this.bandedGridColumn34);
            this.gridBand12.Columns.Add(this.bandedGridColumn35);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 3;
            this.gridBand12.Width = 281;
            // 
            // bandedGridColumn33
            // 
            this.bandedGridColumn33.Caption = "Бэлтгэн нийлүүлэгч";
            this.bandedGridColumn33.CustomizationCaption = "Бэлтгэн нийлүүлэгч";
            this.bandedGridColumn33.FieldName = "producerName";
            this.bandedGridColumn33.MinWidth = 75;
            this.bandedGridColumn33.Name = "bandedGridColumn33";
            this.bandedGridColumn33.Visible = true;
            this.bandedGridColumn33.Width = 87;
            // 
            // bandedGridColumn34
            // 
            this.bandedGridColumn34.Caption = "Дагалдах баримт";
            this.bandedGridColumn34.CustomizationCaption = "Дагалдах баримт";
            this.bandedGridColumn34.FieldName = "docNumber";
            this.bandedGridColumn34.MinWidth = 75;
            this.bandedGridColumn34.Name = "bandedGridColumn34";
            this.bandedGridColumn34.Visible = true;
            // 
            // bandedGridColumn35
            // 
            this.bandedGridColumn35.Caption = "Тайлбар";
            this.bandedGridColumn35.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn35.FieldName = "description";
            this.bandedGridColumn35.MinWidth = 75;
            this.bandedGridColumn35.Name = "bandedGridColumn35";
            this.bandedGridColumn35.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn35.Visible = true;
            this.bandedGridColumn35.Width = 119;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand13.AppearanceHeader.Options.UseFont = true;
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "Эмийн мэдээлэл";
            this.gridBand13.Columns.Add(this.bandedGridColumn36);
            this.gridBand13.Columns.Add(this.bandedGridColumn37);
            this.gridBand13.Columns.Add(this.bandedGridColumn38);
            this.gridBand13.Columns.Add(this.bandedGridColumn39);
            this.gridBand13.Columns.Add(this.bandedGridColumn40);
            this.gridBand13.Columns.Add(this.bandedGridColumn41);
            this.gridBand13.Columns.Add(this.bandedGridColumn42);
            this.gridBand13.CustomizationCaption = "Эмийн мэдээлэл";
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 4;
            this.gridBand13.Width = 525;
            // 
            // bandedGridColumn36
            // 
            this.bandedGridColumn36.Caption = "Эмийн нэр";
            this.bandedGridColumn36.CustomizationCaption = "Худалдааны нэр";
            this.bandedGridColumn36.FieldName = "medicineName";
            this.bandedGridColumn36.MinWidth = 75;
            this.bandedGridColumn36.Name = "bandedGridColumn36";
            this.bandedGridColumn36.Visible = true;
            // 
            // bandedGridColumn37
            // 
            this.bandedGridColumn37.Caption = "ОУ нэр";
            this.bandedGridColumn37.CustomizationCaption = "Олон улсын нэр";
            this.bandedGridColumn37.FieldName = "latinName";
            this.bandedGridColumn37.MinWidth = 75;
            this.bandedGridColumn37.Name = "bandedGridColumn37";
            this.bandedGridColumn37.Visible = true;
            // 
            // bandedGridColumn38
            // 
            this.bandedGridColumn38.Caption = "Ангилал";
            this.bandedGridColumn38.CustomizationCaption = "Эмийн ангилал";
            this.bandedGridColumn38.FieldName = "medicineTypeName";
            this.bandedGridColumn38.MinWidth = 75;
            this.bandedGridColumn38.Name = "bandedGridColumn38";
            this.bandedGridColumn38.Visible = true;
            // 
            // bandedGridColumn39
            // 
            this.bandedGridColumn39.Caption = "Хүчинтэй хугацаа";
            this.bandedGridColumn39.ColumnEdit = this.repositoryItemDateEdit2;
            this.bandedGridColumn39.CustomizationCaption = "Хүчинтэй хугацаа";
            this.bandedGridColumn39.FieldName = "validDate";
            this.bandedGridColumn39.MinWidth = 75;
            this.bandedGridColumn39.Name = "bandedGridColumn39";
            this.bandedGridColumn39.Visible = true;
            // 
            // bandedGridColumn40
            // 
            this.bandedGridColumn40.Caption = "Хэлбэр";
            this.bandedGridColumn40.CustomizationCaption = "Хэлбэр";
            this.bandedGridColumn40.FieldName = "shape";
            this.bandedGridColumn40.MinWidth = 75;
            this.bandedGridColumn40.Name = "bandedGridColumn40";
            this.bandedGridColumn40.Visible = true;
            // 
            // bandedGridColumn41
            // 
            this.bandedGridColumn41.Caption = "Тун хэмжээ";
            this.bandedGridColumn41.CustomizationCaption = "Тун хэмжээ";
            this.bandedGridColumn41.FieldName = "unitType";
            this.bandedGridColumn41.MinWidth = 75;
            this.bandedGridColumn41.Name = "bandedGridColumn41";
            this.bandedGridColumn41.Visible = true;
            // 
            // bandedGridColumn42
            // 
            this.bandedGridColumn42.Caption = "Хэмжих нэгж";
            this.bandedGridColumn42.CustomizationCaption = "Хэмжих нэгж";
            this.bandedGridColumn42.FieldName = "quantity";
            this.bandedGridColumn42.MinWidth = 75;
            this.bandedGridColumn42.Name = "bandedGridColumn42";
            this.bandedGridColumn42.Visible = true;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand14.AppearanceHeader.Options.UseFont = true;
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "Дүн";
            this.gridBand14.Columns.Add(this.bandedGridColumn43);
            this.gridBand14.Columns.Add(this.bandedGridColumn44);
            this.gridBand14.Columns.Add(this.bandedGridColumn45);
            this.gridBand14.CustomizationCaption = "Дүн";
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 5;
            this.gridBand14.Width = 225;
            // 
            // bandedGridColumn43
            // 
            this.bandedGridColumn43.Caption = "Тоо";
            this.bandedGridColumn43.ColumnEdit = this.repositoryItemSpinEdit2;
            this.bandedGridColumn43.CustomizationCaption = "Тоо";
            this.bandedGridColumn43.FieldName = "count";
            this.bandedGridColumn43.MinWidth = 75;
            this.bandedGridColumn43.Name = "bandedGridColumn43";
            this.bandedGridColumn43.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.bandedGridColumn43.Visible = true;
            // 
            // bandedGridColumn44
            // 
            this.bandedGridColumn44.Caption = "Нэгжийн үнэ";
            this.bandedGridColumn44.ColumnEdit = this.repositoryItemSpinEdit2;
            this.bandedGridColumn44.CustomizationCaption = "Нэгжийн үнэ";
            this.bandedGridColumn44.FieldName = "price";
            this.bandedGridColumn44.MinWidth = 75;
            this.bandedGridColumn44.Name = "bandedGridColumn44";
            this.bandedGridColumn44.Visible = true;
            // 
            // bandedGridColumn45
            // 
            this.bandedGridColumn45.Caption = "Дүн";
            this.bandedGridColumn45.ColumnEdit = this.repositoryItemSpinEdit2;
            this.bandedGridColumn45.CustomizationCaption = "Дүн";
            this.bandedGridColumn45.FieldName = "sumPrice";
            this.bandedGridColumn45.MinWidth = 75;
            this.bandedGridColumn45.Name = "bandedGridColumn45";
            this.bandedGridColumn45.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.bandedGridColumn45.Visible = true;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButtonConfirm);
            this.panelControl2.Controls.Add(this.checkEditChange);
            this.panelControl2.Controls.Add(this.dateEditEnd);
            this.panelControl2.Controls.Add(this.dateEditStart);
            this.panelControl2.Controls.Add(this.simpleButtonLayout);
            this.panelControl2.Controls.Add(this.dropDownButtonPrint);
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(784, 46);
            this.panelControl2.TabIndex = 5;
            // 
            // simpleButtonConfirm
            // 
            this.simpleButtonConfirm.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonConfirm.Image")));
            this.simpleButtonConfirm.Location = new System.Drawing.Point(295, 10);
            this.simpleButtonConfirm.Name = "simpleButtonConfirm";
            this.simpleButtonConfirm.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonConfirm.TabIndex = 12;
            this.simpleButtonConfirm.Text = "Батлах";
            this.simpleButtonConfirm.ToolTip = "Төлөвлөгөө баталгаажуулах";
            this.simpleButtonConfirm.Click += new System.EventHandler(this.simpleButtonConfirm_Click);
            // 
            // checkEditChange
            // 
            this.checkEditChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChange.Location = new System.Drawing.Point(487, 12);
            this.checkEditChange.Name = "checkEditChange";
            this.checkEditChange.Properties.Caption = "Эмийн дэлгэрэнгүй";
            this.checkEditChange.Size = new System.Drawing.Size(122, 19);
            this.checkEditChange.TabIndex = 11;
            this.checkEditChange.CheckedChanged += new System.EventHandler(this.checkEditChange_CheckedChanged);
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(118, 12);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(100, 20);
            this.dateEditEnd.TabIndex = 10;
            this.dateEditEnd.ToolTip = "Дуусах огноо";
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(12, 12);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(100, 20);
            this.dateEditStart.TabIndex = 9;
            this.dateEditStart.ToolTip = "Эхлэх огноо";
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(751, 10);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 8;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(615, 10);
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.dropDownButtonPrint.Size = new System.Drawing.Size(130, 23);
            this.dropDownButtonPrint.TabIndex = 6;
            this.dropDownButtonPrint.Text = "Хэвлэх";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(224, 10);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonReload.TabIndex = 4;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Төлөвлөгөө сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 46);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPagePlan;
            this.xtraTabControl1.Size = new System.Drawing.Size(784, 415);
            this.xtraTabControl1.TabIndex = 7;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPagePlan,
            this.xtraTabPagePlanExp});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            // 
            // xtraTabPagePlan
            // 
            this.xtraTabPagePlan.Controls.Add(this.gridControlPlan);
            this.xtraTabPagePlan.Name = "xtraTabPagePlan";
            this.xtraTabPagePlan.Size = new System.Drawing.Size(778, 387);
            this.xtraTabPagePlan.Text = "Төлөвлөгөө";
            // 
            // xtraTabPagePlanExp
            // 
            this.xtraTabPagePlanExp.Controls.Add(this.gridControlPlanExpMonth);
            this.xtraTabPagePlanExp.Controls.Add(this.gridControlPlanExp);
            this.xtraTabPagePlanExp.Controls.Add(this.panelControl1);
            this.xtraTabPagePlanExp.Name = "xtraTabPagePlanExp";
            this.xtraTabPagePlanExp.Size = new System.Drawing.Size(778, 387);
            this.xtraTabPagePlanExp.Text = "Төлөвлөгөөний гүйцэтгэл";
            // 
            // gridControlPlanExpMonth
            // 
            this.gridControlPlanExpMonth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPlanExpMonth.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPlanExpMonth.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPlanExpMonth.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPlanExpMonth.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPlanExpMonth.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPlanExpMonth.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInc_EmbeddedNavigator_ButtonClick);
            this.gridControlPlanExpMonth.Location = new System.Drawing.Point(0, 70);
            this.gridControlPlanExpMonth.MainView = this.gridViewPlanExpMonth;
            this.gridControlPlanExpMonth.Name = "gridControlPlanExpMonth";
            this.gridControlPlanExpMonth.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit7,
            this.repositoryItemSpinEdit8});
            this.gridControlPlanExpMonth.Size = new System.Drawing.Size(778, 317);
            this.gridControlPlanExpMonth.TabIndex = 11;
            this.gridControlPlanExpMonth.UseEmbeddedNavigator = true;
            this.gridControlPlanExpMonth.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPlanExpMonth});
            // 
            // gridViewPlanExpMonth
            // 
            this.gridViewPlanExpMonth.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand29,
            this.gridBand31});
            this.gridViewPlanExpMonth.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn69,
            this.bandedGridColumn102,
            this.bandedGridColumn75,
            this.bandedGridColumn70,
            this.bandedGridColumn71,
            this.bandedGridColumn72,
            this.bandedGridColumn73,
            this.bandedGridColumn74,
            this.bandedGridColumn76,
            this.bandedGridColumn78,
            this.bandedGridColumn80,
            this.bandedGridColumn82});
            this.gridViewPlanExpMonth.GridControl = this.gridControlPlanExpMonth;
            this.gridViewPlanExpMonth.LevelIndent = 1;
            this.gridViewPlanExpMonth.Name = "gridViewPlanExpMonth";
            this.gridViewPlanExpMonth.OptionsBehavior.ReadOnly = true;
            this.gridViewPlanExpMonth.OptionsDetail.ShowDetailTabs = false;
            this.gridViewPlanExpMonth.OptionsPrint.ExpandAllDetails = true;
            this.gridViewPlanExpMonth.OptionsPrint.PrintDetails = true;
            this.gridViewPlanExpMonth.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPlanExpMonth.OptionsView.ShowFooter = true;
            this.gridViewPlanExpMonth.OptionsView.ShowGroupPanel = false;
            this.gridViewPlanExpMonth.PreviewFieldName = "preRow";
            // 
            // gridBand29
            // 
            this.gridBand29.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand29.AppearanceHeader.Options.UseFont = true;
            this.gridBand29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand29.Caption = "Эм";
            this.gridBand29.Columns.Add(this.bandedGridColumn69);
            this.gridBand29.Columns.Add(this.bandedGridColumn70);
            this.gridBand29.Columns.Add(this.bandedGridColumn71);
            this.gridBand29.Columns.Add(this.bandedGridColumn72);
            this.gridBand29.Columns.Add(this.bandedGridColumn73);
            this.gridBand29.Columns.Add(this.bandedGridColumn74);
            this.gridBand29.CustomizationCaption = "Эмийн бүртгэл";
            this.gridBand29.Name = "gridBand29";
            this.gridBand29.VisibleIndex = 0;
            this.gridBand29.Width = 400;
            // 
            // bandedGridColumn69
            // 
            this.bandedGridColumn69.Caption = "Нэр";
            this.bandedGridColumn69.CustomizationCaption = "Эмийн нэр";
            this.bandedGridColumn69.FieldName = "medicineName";
            this.bandedGridColumn69.MinWidth = 100;
            this.bandedGridColumn69.Name = "bandedGridColumn69";
            this.bandedGridColumn69.Visible = true;
            this.bandedGridColumn69.Width = 100;
            // 
            // bandedGridColumn70
            // 
            this.bandedGridColumn70.Caption = "Ангилал";
            this.bandedGridColumn70.CustomizationCaption = "Эмийн ангилал";
            this.bandedGridColumn70.FieldName = "medicineTypeName";
            this.bandedGridColumn70.MinWidth = 75;
            this.bandedGridColumn70.Name = "bandedGridColumn70";
            this.bandedGridColumn70.Visible = true;
            // 
            // bandedGridColumn71
            // 
            this.bandedGridColumn71.Caption = "ОУ нэр";
            this.bandedGridColumn71.CustomizationCaption = "Олон улсын нэр";
            this.bandedGridColumn71.FieldName = "latinName";
            this.bandedGridColumn71.MinWidth = 75;
            this.bandedGridColumn71.Name = "bandedGridColumn71";
            // 
            // bandedGridColumn72
            // 
            this.bandedGridColumn72.Caption = "Тун хэмжээ";
            this.bandedGridColumn72.CustomizationCaption = "Тун хэмжээ";
            this.bandedGridColumn72.FieldName = "unitType";
            this.bandedGridColumn72.MinWidth = 75;
            this.bandedGridColumn72.Name = "bandedGridColumn72";
            this.bandedGridColumn72.Visible = true;
            // 
            // bandedGridColumn73
            // 
            this.bandedGridColumn73.Caption = "Хэлбэр";
            this.bandedGridColumn73.CustomizationCaption = "Хэлбэр";
            this.bandedGridColumn73.FieldName = "shape";
            this.bandedGridColumn73.MinWidth = 75;
            this.bandedGridColumn73.Name = "bandedGridColumn73";
            this.bandedGridColumn73.Visible = true;
            // 
            // bandedGridColumn74
            // 
            this.bandedGridColumn74.Caption = "Хэмжих нэгж";
            this.bandedGridColumn74.CustomizationCaption = "Хэмжих нэгж";
            this.bandedGridColumn74.FieldName = "quantity";
            this.bandedGridColumn74.MinWidth = 75;
            this.bandedGridColumn74.Name = "bandedGridColumn74";
            this.bandedGridColumn74.Visible = true;
            // 
            // gridBand31
            // 
            this.gridBand31.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand31.AppearanceHeader.Options.UseFont = true;
            this.gridBand31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand31.Caption = "Төлөвлөгөө";
            this.gridBand31.Columns.Add(this.bandedGridColumn76);
            this.gridBand31.Columns.Add(this.bandedGridColumn78);
            this.gridBand31.Columns.Add(this.bandedGridColumn82);
            this.gridBand31.Columns.Add(this.bandedGridColumn80);
            this.gridBand31.CustomizationCaption = "Төлөвлөгөө";
            this.gridBand31.Name = "gridBand31";
            this.gridBand31.VisibleIndex = 1;
            this.gridBand31.Width = 300;
            // 
            // bandedGridColumn76
            // 
            this.bandedGridColumn76.Caption = "Төлөвлөгөө";
            this.bandedGridColumn76.ColumnEdit = this.repositoryItemSpinEdit7;
            this.bandedGridColumn76.CustomizationCaption = "Сарын төлөвлөгөө";
            this.bandedGridColumn76.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn76.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn76.FieldName = "countPlan";
            this.bandedGridColumn76.MinWidth = 75;
            this.bandedGridColumn76.Name = "bandedGridColumn76";
            this.bandedGridColumn76.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "countPlan", "{0:n2}")});
            this.bandedGridColumn76.Visible = true;
            // 
            // repositoryItemSpinEdit7
            // 
            this.repositoryItemSpinEdit7.AutoHeight = false;
            this.repositoryItemSpinEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit7.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit7.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit7.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit7.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit7.Name = "repositoryItemSpinEdit7";
            // 
            // bandedGridColumn78
            // 
            this.bandedGridColumn78.Caption = "Гүйцэтгэл";
            this.bandedGridColumn78.ColumnEdit = this.repositoryItemSpinEdit8;
            this.bandedGridColumn78.CustomizationCaption = "Сарын гүйцэтгэл";
            this.bandedGridColumn78.FieldName = "count";
            this.bandedGridColumn78.MinWidth = 75;
            this.bandedGridColumn78.Name = "bandedGridColumn78";
            this.bandedGridColumn78.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.bandedGridColumn78.Visible = true;
            // 
            // repositoryItemSpinEdit8
            // 
            this.repositoryItemSpinEdit8.AutoHeight = false;
            this.repositoryItemSpinEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit8.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit8.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit8.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit8.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit8.Name = "repositoryItemSpinEdit8";
            // 
            // bandedGridColumn82
            // 
            this.bandedGridColumn82.Caption = "Өссөн дүн";
            this.bandedGridColumn82.ColumnEdit = this.repositoryItemSpinEdit8;
            this.bandedGridColumn82.CustomizationCaption = "Өссөн дүн";
            this.bandedGridColumn82.FieldName = "countInc";
            this.bandedGridColumn82.MinWidth = 75;
            this.bandedGridColumn82.Name = "bandedGridColumn82";
            this.bandedGridColumn82.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "countInc", "{0:n2}")});
            this.bandedGridColumn82.Visible = true;
            // 
            // bandedGridColumn80
            // 
            this.bandedGridColumn80.Caption = "Зөрүү";
            this.bandedGridColumn80.ColumnEdit = this.repositoryItemSpinEdit8;
            this.bandedGridColumn80.CustomizationCaption = "Зөрүү";
            this.bandedGridColumn80.FieldName = "countDis";
            this.bandedGridColumn80.MinWidth = 75;
            this.bandedGridColumn80.Name = "bandedGridColumn80";
            this.bandedGridColumn80.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "countDis", "{0:n2}")});
            this.bandedGridColumn80.Visible = true;
            // 
            // bandedGridColumn102
            // 
            this.bandedGridColumn102.Caption = "Дүн";
            this.bandedGridColumn102.ColumnEdit = this.repositoryItemSpinEdit7;
            this.bandedGridColumn102.CustomizationCaption = "Нийт дүн";
            this.bandedGridColumn102.DisplayFormat.FormatString = "n2";
            this.bandedGridColumn102.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bandedGridColumn102.FieldName = "sumPrice";
            this.bandedGridColumn102.MinWidth = 100;
            this.bandedGridColumn102.Name = "bandedGridColumn102";
            this.bandedGridColumn102.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.bandedGridColumn102.Width = 100;
            // 
            // bandedGridColumn75
            // 
            this.bandedGridColumn75.Caption = "Үнэ";
            this.bandedGridColumn75.ColumnEdit = this.repositoryItemSpinEdit7;
            this.bandedGridColumn75.CustomizationCaption = "Нэгжийн үнэ";
            this.bandedGridColumn75.FieldName = "price";
            this.bandedGridColumn75.MinWidth = 75;
            this.bandedGridColumn75.Name = "bandedGridColumn75";
            // 
            // gridControlPlanExp
            // 
            this.gridControlPlanExp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPlanExp.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPlanExp.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPlanExp.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPlanExp.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPlanExp.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPlanExp.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInc_EmbeddedNavigator_ButtonClick);
            this.gridControlPlanExp.Location = new System.Drawing.Point(0, 70);
            this.gridControlPlanExp.MainView = this.gridViewPlanExp;
            this.gridControlPlanExp.Name = "gridControlPlanExp";
            this.gridControlPlanExp.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit3,
            this.repositoryItemSpinEdit1});
            this.gridControlPlanExp.Size = new System.Drawing.Size(778, 317);
            this.gridControlPlanExp.TabIndex = 7;
            this.gridControlPlanExp.UseEmbeddedNavigator = true;
            this.gridControlPlanExp.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPlanExp});
            // 
            // gridViewPlanExp
            // 
            this.gridViewPlanExp.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand21,
            this.gridBand4,
            this.gridBand5,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8,
            this.gridBand10,
            this.gridBand15,
            this.gridBand16,
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gridBand20});
            this.gridViewPlanExp.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn8,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn11,
            this.bandedGridColumn12,
            this.bandedGridColumn13,
            this.bandedGridColumn14,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.bandedGridColumn19});
            this.gridViewPlanExp.GridControl = this.gridControlPlanExp;
            this.gridViewPlanExp.LevelIndent = 1;
            this.gridViewPlanExp.Name = "gridViewPlanExp";
            this.gridViewPlanExp.OptionsBehavior.ReadOnly = true;
            this.gridViewPlanExp.OptionsDetail.ShowDetailTabs = false;
            this.gridViewPlanExp.OptionsPrint.ExpandAllDetails = true;
            this.gridViewPlanExp.OptionsPrint.PrintDetails = true;
            this.gridViewPlanExp.OptionsView.ShowAutoFilterRow = true;
            this.gridViewPlanExp.OptionsView.ShowFooter = true;
            this.gridViewPlanExp.OptionsView.ShowGroupPanel = false;
            this.gridViewPlanExp.PreviewFieldName = "preRow";
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Эм";
            this.gridBand2.Columns.Add(this.gridColumn28);
            this.gridBand2.Columns.Add(this.gridColumn44);
            this.gridBand2.Columns.Add(this.gridColumn45);
            this.gridBand2.Columns.Add(this.gridColumn46);
            this.gridBand2.Columns.Add(this.gridColumn47);
            this.gridBand2.Columns.Add(this.gridColumn48);
            this.gridBand2.CustomizationCaption = "Эмийн бүртгэл";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 100;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Нэр";
            this.gridColumn28.CustomizationCaption = "Эмийн нэр";
            this.gridColumn28.FieldName = "medicineName";
            this.gridColumn28.MinWidth = 100;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.Width = 100;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Ангилал";
            this.gridColumn44.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn44.FieldName = "medicineTypeName";
            this.gridColumn44.MinWidth = 75;
            this.gridColumn44.Name = "gridColumn44";
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "ОУ нэр";
            this.gridColumn45.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn45.FieldName = "latinName";
            this.gridColumn45.MinWidth = 75;
            this.gridColumn45.Name = "gridColumn45";
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Тун хэмжээ";
            this.gridColumn46.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn46.FieldName = "unitType";
            this.gridColumn46.MinWidth = 75;
            this.gridColumn46.Name = "gridColumn46";
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Хэлбэр";
            this.gridColumn47.CustomizationCaption = "Хэлбэр";
            this.gridColumn47.FieldName = "shape";
            this.gridColumn47.MinWidth = 75;
            this.gridColumn47.Name = "gridColumn47";
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Хэмжих нэгж";
            this.gridColumn48.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn48.FieldName = "quantity";
            this.gridColumn48.MinWidth = 75;
            this.gridColumn48.Name = "gridColumn48";
            // 
            // gridBand21
            // 
            this.gridBand21.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand21.AppearanceHeader.Options.UseFont = true;
            this.gridBand21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand21.Caption = "Жилийн тоо";
            this.gridBand21.Columns.Add(this.gridColumn43);
            this.gridBand21.Columns.Add(this.gridColumn42);
            this.gridBand21.Columns.Add(this.bandedGridColumn19);
            this.gridBand21.Columns.Add(this.gridColumn29);
            this.gridBand21.CustomizationCaption = "Жилийн тоо";
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.VisibleIndex = 1;
            this.gridBand21.Width = 225;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Үнэ";
            this.gridColumn43.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn43.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumn43.FieldName = "price";
            this.gridColumn43.MinWidth = 75;
            this.gridColumn43.Name = "gridColumn43";
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit3.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit3.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit3.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit3.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Төлөвлөсөн";
            this.gridColumn42.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn42.CustomizationCaption = "Жилийн төлөвлөсөн тоо";
            this.gridColumn42.DisplayFormat.FormatString = "n2";
            this.gridColumn42.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn42.FieldName = "count";
            this.gridColumn42.MinWidth = 75;
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumn42.Visible = true;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.Caption = "Хэрэглэсэн";
            this.bandedGridColumn19.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn19.CustomizationCaption = "Жилийн хэрэглэсэн тоо";
            this.bandedGridColumn19.FieldName = "countExp";
            this.bandedGridColumn19.MinWidth = 75;
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "countExp", "{0:n2}")});
            this.bandedGridColumn19.Visible = true;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Төлөвлөсөн";
            this.gridColumn29.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn29.CustomizationCaption = "1-р сар төлөвлөсөн";
            this.gridColumn29.FieldName = "jan";
            this.gridColumn29.MinWidth = 75;
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jan", "{0:n2}")});
            this.gridColumn29.Visible = true;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "1-р сар";
            this.gridBand4.Columns.Add(this.bandedGridColumn6);
            this.gridBand4.CustomizationCaption = "1-р сар";
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 2;
            this.gridBand4.Width = 75;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "Хэрэглэсэн";
            this.bandedGridColumn6.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn6.CustomizationCaption = "1-р сар хэрэглэсэн тоо";
            this.bandedGridColumn6.FieldName = "janExp";
            this.bandedGridColumn6.MinWidth = 75;
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "janExp", "{0:n2}")});
            this.bandedGridColumn6.Visible = true;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "2-р сар";
            this.gridBand5.Columns.Add(this.gridColumn30);
            this.gridBand5.Columns.Add(this.bandedGridColumn7);
            this.gridBand5.CustomizationCaption = "2-р сар";
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 3;
            this.gridBand5.Width = 150;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Төлөвлөсөн";
            this.gridColumn30.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn30.CustomizationCaption = "2-р сар төлөвлөсөн";
            this.gridColumn30.FieldName = "feb";
            this.gridColumn30.MinWidth = 75;
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "feb", "{0:n2}")});
            this.gridColumn30.Visible = true;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "Хэрэглэсэн";
            this.bandedGridColumn7.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn7.CustomizationCaption = "2-р сар хэрэглэсэн тоо";
            this.bandedGridColumn7.FieldName = "febExp";
            this.bandedGridColumn7.MinWidth = 75;
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "febExp", "{0:n2}")});
            this.bandedGridColumn7.Visible = true;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand6.AppearanceHeader.Options.UseFont = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "3-р сар";
            this.gridBand6.Columns.Add(this.gridColumn31);
            this.gridBand6.Columns.Add(this.bandedGridColumn8);
            this.gridBand6.CustomizationCaption = "3-р сар";
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 4;
            this.gridBand6.Width = 150;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Төлөвлөсөн";
            this.gridColumn31.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn31.CustomizationCaption = "3-р сар төлөвлөсөн";
            this.gridColumn31.FieldName = "mar";
            this.gridColumn31.MinWidth = 75;
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "mar", "{0:n2}")});
            this.gridColumn31.Visible = true;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.Caption = "Хэрэглэсэн";
            this.bandedGridColumn8.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn8.CustomizationCaption = "3-р сар хэрэглэсэн тоо";
            this.bandedGridColumn8.FieldName = "marExp";
            this.bandedGridColumn8.MinWidth = 75;
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "marExp", "{0:n2}")});
            this.bandedGridColumn8.Visible = true;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand7.AppearanceHeader.Options.UseFont = true;
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "4-р сар";
            this.gridBand7.Columns.Add(this.gridColumn32);
            this.gridBand7.Columns.Add(this.bandedGridColumn9);
            this.gridBand7.CustomizationCaption = "4-р сар";
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 5;
            this.gridBand7.Width = 150;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Төлөвлөсөн";
            this.gridColumn32.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn32.CustomizationCaption = "4-р сар төлөвлөсөн";
            this.gridColumn32.FieldName = "apr";
            this.gridColumn32.MinWidth = 75;
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "apr", "{0:n2}")});
            this.gridColumn32.Visible = true;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "Хэрэглэсэн";
            this.bandedGridColumn9.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn9.CustomizationCaption = "4-р сар хэрэглэсэн тоо";
            this.bandedGridColumn9.FieldName = "augExp";
            this.bandedGridColumn9.MinWidth = 75;
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "augExp", "{0:n2}")});
            this.bandedGridColumn9.Visible = true;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "5-р сар";
            this.gridBand8.Columns.Add(this.gridColumn33);
            this.gridBand8.Columns.Add(this.bandedGridColumn10);
            this.gridBand8.CustomizationCaption = "5-р сар";
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 6;
            this.gridBand8.Width = 150;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Төлөвлөсөн";
            this.gridColumn33.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn33.CustomizationCaption = "5-р сар төлөвлөсөн";
            this.gridColumn33.FieldName = "may";
            this.gridColumn33.MinWidth = 75;
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "may", "{0:n2}")});
            this.gridColumn33.Visible = true;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.Caption = "Хэрэглэсэн";
            this.bandedGridColumn10.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn10.CustomizationCaption = "5-р сар хэрэглэсэн тоо";
            this.bandedGridColumn10.FieldName = "mayExp";
            this.bandedGridColumn10.MinWidth = 75;
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "mayExp", "{0:n2}")});
            this.bandedGridColumn10.Visible = true;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand10.AppearanceHeader.Options.UseFont = true;
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "6-р сар";
            this.gridBand10.Columns.Add(this.gridColumn34);
            this.gridBand10.Columns.Add(this.bandedGridColumn11);
            this.gridBand10.CustomizationCaption = "6-р сар";
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 7;
            this.gridBand10.Width = 150;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Төлөвлөсөн";
            this.gridColumn34.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn34.CustomizationCaption = "6-р сар төлөвлөсөн";
            this.gridColumn34.FieldName = "jun";
            this.gridColumn34.MinWidth = 75;
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jun", "{0:n2}")});
            this.gridColumn34.Visible = true;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.Caption = "Хэрэглэсэн";
            this.bandedGridColumn11.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn11.CustomizationCaption = "6-р сар хэрэглэсэн тоо";
            this.bandedGridColumn11.FieldName = "junExp";
            this.bandedGridColumn11.MinWidth = 75;
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "junExp", "{0:n2}")});
            this.bandedGridColumn11.Visible = true;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand15.AppearanceHeader.Options.UseFont = true;
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "7-р сар";
            this.gridBand15.Columns.Add(this.gridColumn35);
            this.gridBand15.Columns.Add(this.bandedGridColumn12);
            this.gridBand15.CustomizationCaption = "7-р сар";
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 8;
            this.gridBand15.Width = 150;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Төлөвлөсөн";
            this.gridColumn35.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn35.CustomizationCaption = "7-р сар төлөвлөсөн";
            this.gridColumn35.FieldName = "jul";
            this.gridColumn35.MinWidth = 75;
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jul", "{0:n2}")});
            this.gridColumn35.Visible = true;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.Caption = "Хэрэглэсэн";
            this.bandedGridColumn12.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn12.CustomizationCaption = "7-р сар хэрэглэсэн тоо";
            this.bandedGridColumn12.FieldName = "julExp";
            this.bandedGridColumn12.MinWidth = 75;
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "julExp", "{0:n2}")});
            this.bandedGridColumn12.Visible = true;
            // 
            // gridBand16
            // 
            this.gridBand16.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand16.AppearanceHeader.Options.UseFont = true;
            this.gridBand16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand16.Caption = "8-р сар";
            this.gridBand16.Columns.Add(this.gridColumn36);
            this.gridBand16.Columns.Add(this.bandedGridColumn13);
            this.gridBand16.CustomizationCaption = "8-р сар";
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 9;
            this.gridBand16.Width = 150;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Төлөвлөсөн";
            this.gridColumn36.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn36.CustomizationCaption = "8-р сар төлөвлөсөн";
            this.gridColumn36.FieldName = "aug";
            this.gridColumn36.MinWidth = 75;
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "aug", "{0:n2}")});
            this.gridColumn36.Visible = true;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.Caption = "Хэрэглэсэн";
            this.bandedGridColumn13.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn13.CustomizationCaption = "8-р сар хэрэглэсэн тоо";
            this.bandedGridColumn13.FieldName = "augExp";
            this.bandedGridColumn13.MinWidth = 75;
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "augExp", "{0:n2}")});
            this.bandedGridColumn13.Visible = true;
            // 
            // gridBand17
            // 
            this.gridBand17.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand17.AppearanceHeader.Options.UseFont = true;
            this.gridBand17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand17.Caption = "9-р сар";
            this.gridBand17.Columns.Add(this.gridColumn37);
            this.gridBand17.Columns.Add(this.bandedGridColumn14);
            this.gridBand17.CustomizationCaption = "9-р сар";
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 10;
            this.gridBand17.Width = 150;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Төлөвлөсөн";
            this.gridColumn37.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn37.CustomizationCaption = "9-р сар төлөвлөсөн";
            this.gridColumn37.FieldName = "sep";
            this.gridColumn37.MinWidth = 75;
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sep", "{0:n2}")});
            this.gridColumn37.Visible = true;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.Caption = "Хэрэглэсэн";
            this.bandedGridColumn14.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn14.CustomizationCaption = "9-р сар хэрэглэсэн тоо";
            this.bandedGridColumn14.FieldName = "sepExp";
            this.bandedGridColumn14.MinWidth = 75;
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sepExp", "{0:n2}")});
            this.bandedGridColumn14.Visible = true;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand18.AppearanceHeader.Options.UseFont = true;
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.Caption = "10-р сар";
            this.gridBand18.Columns.Add(this.gridColumn38);
            this.gridBand18.Columns.Add(this.bandedGridColumn15);
            this.gridBand18.CustomizationCaption = "10-р сар";
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = 11;
            this.gridBand18.Width = 150;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Төлөвлөсөн";
            this.gridColumn38.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn38.CustomizationCaption = "10-р сар төлөвлөсөн";
            this.gridColumn38.FieldName = "oct";
            this.gridColumn38.MinWidth = 75;
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "oct", "{0:n2}")});
            this.gridColumn38.Visible = true;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.Caption = "Хэрэглэсэн";
            this.bandedGridColumn15.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn15.CustomizationCaption = "10-р сар хэрэглэсэн тоо";
            this.bandedGridColumn15.FieldName = "octExp";
            this.bandedGridColumn15.MinWidth = 75;
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "octExp", "{0:n2}")});
            this.bandedGridColumn15.Visible = true;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand19.AppearanceHeader.Options.UseFont = true;
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.Caption = "11-р сар";
            this.gridBand19.Columns.Add(this.gridColumn39);
            this.gridBand19.Columns.Add(this.bandedGridColumn16);
            this.gridBand19.CustomizationCaption = "11-р сар";
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 12;
            this.gridBand19.Width = 150;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Төлөвлөсөн";
            this.gridColumn39.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn39.CustomizationCaption = "11-р сар төлөвлөсөн";
            this.gridColumn39.FieldName = "nov";
            this.gridColumn39.MinWidth = 75;
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "nov", "{0:n2}")});
            this.gridColumn39.Visible = true;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.Caption = "Хэрэглэсэн";
            this.bandedGridColumn16.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn16.CustomizationCaption = "11-р сар хэрэглэсэн тоо";
            this.bandedGridColumn16.FieldName = "novExp";
            this.bandedGridColumn16.MinWidth = 75;
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "novExp", "{0:n2}")});
            this.bandedGridColumn16.Visible = true;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand20.AppearanceHeader.Options.UseFont = true;
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.Caption = "12-р сар";
            this.gridBand20.Columns.Add(this.gridColumn40);
            this.gridBand20.Columns.Add(this.bandedGridColumn17);
            this.gridBand20.CustomizationCaption = "12-р сар";
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 13;
            this.gridBand20.Width = 150;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Төлөвлөсөн";
            this.gridColumn40.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn40.CustomizationCaption = "12-р сар төлөвлөсөн";
            this.gridColumn40.FieldName = "dec";
            this.gridColumn40.MinWidth = 75;
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "dec", "{0:n2}")});
            this.gridColumn40.Visible = true;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.Caption = "Хэрэглэсэн";
            this.bandedGridColumn17.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn17.CustomizationCaption = "12-р сар хэрэглэсэн тоо";
            this.bandedGridColumn17.FieldName = "decExp";
            this.bandedGridColumn17.MinWidth = 75;
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "decExp", "{0:n2}")});
            this.bandedGridColumn17.Visible = true;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Дүн";
            this.gridColumn41.ColumnEdit = this.repositoryItemSpinEdit3;
            this.gridColumn41.CustomizationCaption = "Нийт дүн";
            this.gridColumn41.DisplayFormat.FormatString = "n2";
            this.gridColumn41.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn41.FieldName = "sumPrice";
            this.gridColumn41.MinWidth = 100;
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.gridColumn41.Width = 100;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControlMonth);
            this.panelControl1.Controls.Add(this.comboBoxEditMonth);
            this.panelControl1.Controls.Add(this.radioGroupJournal);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.dateEditPlanEnd);
            this.panelControl1.Controls.Add(this.dateEditPlanStart);
            this.panelControl1.Controls.Add(this.gridLookUpEditPlan);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(778, 70);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControlMonth
            // 
            this.labelControlMonth.Location = new System.Drawing.Point(291, 17);
            this.labelControlMonth.Name = "labelControlMonth";
            this.labelControlMonth.Size = new System.Drawing.Size(26, 13);
            this.labelControlMonth.TabIndex = 24;
            this.labelControlMonth.Text = "Сар :";
            // 
            // comboBoxEditMonth
            // 
            this.comboBoxEditMonth.Location = new System.Drawing.Point(323, 14);
            this.comboBoxEditMonth.Name = "comboBoxEditMonth";
            this.comboBoxEditMonth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditMonth.Properties.Items.AddRange(new object[] {
            "1-р сар",
            "2-р сар",
            "3-р сар",
            "4-р сар",
            "5-р сар",
            "6-р сар",
            "7-р сар",
            "8-р сар",
            "9-р сар",
            "10-р сар",
            "11-р сар",
            "12-р сар"});
            this.comboBoxEditMonth.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditMonth.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEditMonth.TabIndex = 23;
            this.comboBoxEditMonth.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditMonth_SelectedIndexChanged);
            // 
            // radioGroupJournal
            // 
            this.radioGroupJournal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupJournal.Location = new System.Drawing.Point(631, 12);
            this.radioGroupJournal.Name = "radioGroupJournal";
            this.radioGroupJournal.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Сар", true, "Journal"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Жил", true, "Detail")});
            this.radioGroupJournal.Size = new System.Drawing.Size(140, 23);
            this.radioGroupJournal.TabIndex = 21;
            this.radioGroupJournal.SelectedIndexChanged += new System.EventHandler(this.radioGroupJournal_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(63, 43);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "Хугацаа :";
            // 
            // dateEditPlanEnd
            // 
            this.dateEditPlanEnd.EditValue = null;
            this.dateEditPlanEnd.Location = new System.Drawing.Point(223, 40);
            this.dateEditPlanEnd.Name = "dateEditPlanEnd";
            this.dateEditPlanEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditPlanEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditPlanEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditPlanEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditPlanEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditPlanEnd.Properties.ReadOnly = true;
            this.dateEditPlanEnd.Size = new System.Drawing.Size(100, 20);
            this.dateEditPlanEnd.TabIndex = 12;
            this.dateEditPlanEnd.ToolTip = "Дуусах огноо";
            // 
            // dateEditPlanStart
            // 
            this.dateEditPlanStart.EditValue = null;
            this.dateEditPlanStart.Location = new System.Drawing.Point(117, 40);
            this.dateEditPlanStart.Name = "dateEditPlanStart";
            this.dateEditPlanStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditPlanStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditPlanStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditPlanStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditPlanStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditPlanStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditPlanStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditPlanStart.Properties.ReadOnly = true;
            this.dateEditPlanStart.Size = new System.Drawing.Size(100, 20);
            this.dateEditPlanStart.TabIndex = 11;
            this.dateEditPlanStart.ToolTip = "Эхлэх огноо";
            // 
            // gridLookUpEditPlan
            // 
            this.gridLookUpEditPlan.EditValue = "";
            this.gridLookUpEditPlan.Location = new System.Drawing.Point(117, 14);
            this.gridLookUpEditPlan.Name = "gridLookUpEditPlan";
            this.gridLookUpEditPlan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPlan.Properties.DisplayMember = "name";
            this.gridLookUpEditPlan.Properties.NullText = "";
            this.gridLookUpEditPlan.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditPlan.Properties.ValueMember = "id";
            this.gridLookUpEditPlan.Properties.View = this.gridView1;
            this.gridLookUpEditPlan.Size = new System.Drawing.Size(150, 20);
            this.gridLookUpEditPlan.TabIndex = 4;
            this.gridLookUpEditPlan.EditValueChanged += new System.EventHandler(this.gridLookUpEditTender_EditValueChanged);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Нэр";
            this.gridColumn23.CustomizationCaption = "Төлөвлөгөөний нэр";
            this.gridColumn23.FieldName = "name";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 0;
            this.gridColumn23.Width = 231;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Эхлэх огноо";
            this.gridColumn24.CustomizationCaption = "Эхлэх огноо";
            this.gridColumn24.FieldName = "startDate";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 2;
            this.gridColumn24.Width = 77;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Дуусах огноо";
            this.gridColumn25.CustomizationCaption = "Дуусах огноо";
            this.gridColumn25.FieldName = "endDate";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 1;
            this.gridColumn25.Width = 89;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Тайлбар";
            this.gridColumn26.CustomizationCaption = "Тайлбар";
            this.gridColumn26.FieldName = "description";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 3;
            this.gridColumn26.Width = 403;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Баталгаажсан";
            this.gridColumn27.CustomizationCaption = "Баталгаажсан эсэх";
            this.gridColumn27.FieldName = "isConfirm";
            this.gridColumn27.MinWidth = 75;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 4;
            this.gridColumn27.Width = 131;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(45, 17);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(66, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Төлөвлөгөө :";
            // 
            // PlanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl2);
            this.Name = "PlanForm";
            this.Text = "Төлөвлөгөө";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlanForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlanMonthly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridViewPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPagePlan.ResumeLayout(false);
            this.xtraTabPagePlanExp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlanExpMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlanExpMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPlanExp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPlanExp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMonth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupJournal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditPlanStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPlan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraGrid.GridControl gridControlPlan;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridViewPlan;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPlanMonthly;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn23;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn30;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn32;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn33;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn34;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn35;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn36;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn37;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn38;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn39;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn40;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn41;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn42;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn43;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn44;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditMonth;
        private DevExpress.XtraEditors.CheckEdit checkEditChange;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePlan;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePlanExp;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPlan;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        //private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit dateEditPlanEnd;
        private DevExpress.XtraEditors.DateEdit dateEditPlanStart;
        private DevExpress.XtraGrid.GridControl gridControlPlanExp;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewPlanExp;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn44;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn45;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn46;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn47;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn48;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn43;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn42;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn30;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn32;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn33;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn34;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn35;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn36;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn37;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn38;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn39;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn40;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn41;
        private DevExpress.XtraEditors.SimpleButton simpleButtonConfirm;
        private DevExpress.XtraEditors.RadioGroup radioGroupJournal;
        private DevExpress.XtraEditors.LabelControl labelControlMonth;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditMonth;
        private DevExpress.XtraGrid.GridControl gridControlPlanExpMonth;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewPlanExpMonth;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn69;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn70;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn71;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn72;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn73;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn74;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand31;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn76;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn78;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn82;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn80;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn102;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn75;
        

    }
}