﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.bal;
using DevExpress.XtraEditors.Controls;



namespace Pharmacy2016.gui.core.journal.census
{
    /*
     * Эмийн тооллого нэмэх, засах цонх.
     * 
     */

    public partial class MedicineCensusUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineCensusUpForm INSTANCE = new MedicineCensusUpForm();

            public static MedicineCensusUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private string id;

            private DataRowView currView;

            private bool isShowFirst = true;

            private MedicineCensusUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                dateEditBalDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditBalDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditIncDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditIncDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                initError();
                initDataSource();
                initBinding();
                reload(false);
            }

            private void initDataSource()
            {
                gridControlDetail.DataSource = JournalDataSet.CensusDetailBindingSource;
                //gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
                gridLookUpEditUser.Properties.DataSource = JournalDataSet.Instance.SystemUserCensus;
                treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
                treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
                //gridLookUpEditCensusUser.Properties.DataSource = InformationDataSet.SystemUserOtherBindingSource;
                gridLookUpEditCensusUser.Properties.DataSource = JournalDataSet.Instance.SystemUserCensusOther;
            }

            private void initError()
            {
                dateEditIncDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditBalDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditCensusUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insert();
                    }
                    else if (actionType == 2)
                    {
                        update();
                    }
                    
                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void initBinding()
            {
                textEditID.DataBindings.Add(new Binding("EditValue", JournalDataSet.CensusJournalBindingSource, "id", true));
                dateEditIncDate.DataBindings.Add(new Binding("EditValue", JournalDataSet.CensusJournalBindingSource, "date", true));
                gridLookUpEditCensusUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.CensusJournalBindingSource, "censusUserID", true));
                dateEditBalDate.DataBindings.Add(new Binding("EditValue", JournalDataSet.CensusJournalBindingSource, "balDate", true));
                gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.CensusJournalBindingSource, "userID", true));
                radioGroupType.DataBindings.Add(new Binding("EditValue", JournalDataSet.CensusJournalBindingSource, "type", true));
                treeListLookUpEditWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.CensusJournalBindingSource, "warehouseID", true));
                treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.CensusJournalBindingSource, "wardID", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", JournalDataSet.CensusJournalBindingSource, "description", true));
            }

            private void insert()
            {
                //this.Text = "Тооллого нэмэх цонх";
                id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                JournalDataSet.Instance.CensusJournal.idColumn.DefaultValue = id;
                JournalDataSet.Instance.CensusDetail.censusJournalIDColumn.DefaultValue = id;
                currView = (DataRowView)JournalDataSet.CensusJournalBindingSource.AddNew();
                DateTime now = DateTime.Now;
                currView["id"] = id;
                currView["date"] = now;

                dateEditBalDate.DateTime = now;
                textEditID.Text = id;
            }

            private void update()
            {
                //this.Text = "Тооллого засах цонх";
                currView = (DataRowView)JournalDataSet.CensusJournalBindingSource.Current;
                id = currView["id"].ToString();
                JournalDataSet.Instance.CensusDetail.censusJournalIDColumn.DefaultValue = id;
            }


            private void clearData()
            {
                isShowFirst = true;
                clearErrorText();
                //clearBinding();

                JournalDataSet.WarehouseOtherBindingSource.Filter = "";
                JournalDataSet.WardOtherBindingSource.Filter = "";
                InformationDataSet.SystemUserBindingSource.Filter = "";

                JournalDataSet.CensusJournalBindingSource.CancelEdit();
                JournalDataSet.CensusDetailBindingSource.CancelEdit();
                JournalDataSet.Instance.CensusJournal.RejectChanges();
                JournalDataSet.Instance.CensusDetail.RejectChanges();
            }

            private void clearBinding()
            {
                textEditID.DataBindings.Clear();
                dateEditIncDate.DataBindings.Clear();
                gridLookUpEditCensusUser.DataBindings.Clear();
                dateEditBalDate.DataBindings.Clear();
                gridLookUpEditUser.DataBindings.Clear();
                radioGroupType.DataBindings.Clear();
                treeListLookUpEditWarehouse.DataBindings.Clear();
                treeListLookUpEditWard.DataBindings.Clear();
                memoEditDescription.DataBindings.Clear();
            }

            private void clearErrorText()
            {
                gridLookUpEditUser.ErrorText = "";
                treeListLookUpEditWarehouse.ErrorText = "";
                treeListLookUpEditWard.ErrorText = "";
                gridLookUpEditCensusUser.ErrorText = "";
                dateEditIncDate.ErrorText = "";
                dateEditBalDate.ErrorText = "";
            }

            private void MedicineExpInUpForm_Shown(object sender, EventArgs e)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID)
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";

                if (actionType == 1)
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID)
                    {
                        gridLookUpEditCensusUser.EditValue = UserUtil.getUserId();
                        currView["censusUserID"] = UserUtil.getUserId();
                        dateEditBalDate.Focus();
                    }
                    else
                    {
                        gridLookUpEditCensusUser.Focus();
                    }
                }
                else if (actionType == 2)
                {
                    dateEditIncDate.Focus();
                }
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
                isShowFirst = false;
            }

        #endregion

        #region Формын event

            private void dateEditIncDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditIncDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditCensusUser.Focus();
                    }
                }
            }

            private void gridLookUpEditCensusUser_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditCensusUser.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        dateEditBalDate.Focus();
                    }
                }
            }

            private void dateEditBalDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditBalDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditUser.Focus();
                    }
                }
            }

            private void dateEditBalDate_EditValueChanged(object sender, EventArgs e)
            {
                if(Visible)
                    reloadMedicine();
            }


            private void gridLookUpEditUser_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditUser.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        if (radioGroupType.SelectedIndex == 0)
                            treeListLookUpEditWarehouse.Focus();
                        else
                            treeListLookUpEditWard.Focus();
                    }
                }
            }

            private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
            {
                if (Visible && gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null && !gridLookUpEditUser.Text.Equals(""))
                {
                    JournalDataSet.WarehouseOtherBindingSource.Filter = UserUtil.getUserWarehouse(gridLookUpEditUser.EditValue.ToString());
                    JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(gridLookUpEditUser.EditValue.ToString());
                    reloadMedicine();
                }
            }

            private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
            {
                if(Visible)
                    changeType();
            }

            private void treeListLookUpEditWarehouse_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditWarehouse.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        memoEditDescription.Focus();
                    }
                }
            }

            private void treeListLookUpEditWard_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditWard.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        memoEditDescription.Focus();
                    }
                }
            }


            private void gridControlDetail_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addRow();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewDetail.GetFocusedDataRow() != null)
                    {
                        deleteRow();
                    }
                }
            }

            private void gridViewDetail_ValidateRow(object sender, ValidateRowEventArgs e)
            {
                //double value = Convert.ToDouble(gridViewDetail.GetRowCellValue(e.RowHandle, gridColumnCount));
                //if (value <= 0)
                //{
                //    e.Valid = false;
                //    gridViewDetail.SetColumnError(gridColumnCount, "Тоо хэмжээг оруулна уу");
                //}
            }

            private void gridViewDetail_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
            {
                e.ExceptionMode = ExceptionMode.NoAction;
            }


            private void simpleButtonNewDoc_Click(object sender, EventArgs e)
            {
                clearData();
                //insert();
                dateEditIncDate.Focus();
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload(true);
            }
            
            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private void reload(bool isReload)
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                //InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN);
                //InformationDataSet.SystemUserOtherTableAdapter.Fill(InformationDataSet.Instance.SystemUserOther, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                if (isReload || JournalDataSet.Instance.SystemUserCensus.Rows.Count == 0)
                    JournalDataSet.SystemUserCensusTableAdapter.Fill(JournalDataSet.Instance.SystemUserCensus, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN);
                if (isReload || JournalDataSet.Instance.SystemUserCensusOther.Rows.Count == 0)
                    JournalDataSet.SystemUserCensusOtherTableAdapter.Fill(JournalDataSet.Instance.SystemUserCensusOther, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                    JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                    JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
                //ProgramUtil.closeWaitDialog();
            }

            private void reloadMedicine()
            {
                if (Visible && dateEditBalDate.EditValue != DBNull.Value && dateEditBalDate.EditValue != null && !dateEditBalDate.Text.Equals("") &&
                   gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null && !gridLookUpEditUser.EditValue.ToString().Equals("") &&
                   ((radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && !treeListLookUpEditWarehouse.EditValue.ToString().Equals("")) ||
                   (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && !treeListLookUpEditWard.EditValue.ToString().Equals(""))))
                {
                    if (gridViewDetail.RowCount == 0 || (!isShowFirst && XtraMessageBox.Show("Тооллогын эмийг устгах уу", "Эмийн тооллого устгах", MessageBoxButtons.YesNo) == DialogResult.Yes))
                    {
                        gridViewDetail.SelectAll();
                        gridViewDetail.DeleteSelectedRows();

                        for (int i = gridViewDetail.RowCount - 1; i >= 0; i--)
                        {
                            gridViewDetail.DeleteRow(i);
                        }
                        JournalDataSet.CensusDetailBindingSource.EndEdit();

                        JournalDataSet.MedicineBalTableAdapter.Fill(JournalDataSet.Instance.MedicineBal, HospitalUtil.getHospitalId(), gridLookUpEditUser.EditValue.ToString(), radioGroupType.SelectedIndex, (radioGroupType.SelectedIndex == 0 ? treeListLookUpEditWarehouse.EditValue.ToString() : ""), (radioGroupType.SelectedIndex == 1 ? treeListLookUpEditWard.EditValue.ToString() : ""), dateEditBalDate.Text);
                        for (int i = 0; i < JournalDataSet.Instance.MedicineBal.Rows.Count; i++)
                        {
                            DataRow row = JournalDataSet.Instance.MedicineBal.Rows[i];
                            JournalDataSet.Instance.CensusDetail.idColumn.DefaultValue = getNotExistId();
                            DataRowView view = (DataRowView)JournalDataSet.CensusDetailBindingSource.AddNew();
                            view["incTypeID"] = row["incTypeID"];
                            view["incTypeName"] = row["incTypeName"];
                            view["medicineID"] = row["medicineID"];
                            view["medicineName"] = row["name"];
                            view["latinName"] = row["latinName"];
                            view["unitType"] = row["unitType"];
                            view["shape"] = row["shape"];
                            view["quantity"] = row["quantity"];
                            view["validDate"] = row["validDate"];
                            view["medicineTypeName"] = row["medicineTypeName"];
                            view["balCount"] = row["count"];
                            view["count"] = 0;
                            view["price"] = row["price"];
                            view.EndEdit();
                        }
                    }
                }
            }

            private string getNotExistId()
            {
                string newID = null;
                DataRow[] rows = null;
                do{
                    newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    rows = JournalDataSet.Instance.CensusDetail.Select("hospitalID = '"+HospitalUtil.getHospitalId()+"' AND censusJournalID = '"+id+"' AND id = '"+newID+"'");   
                }while(rows != null && rows.Length > 0);
                
                return newID;
            }

            private void changeType()
            {
                if (radioGroupType.SelectedIndex == 0)
                {
                    labelControlWarehouse.Location = new Point(384, 108);
                    treeListLookUpEditWard.Location = new Point(451, 27);
                    treeListLookUpEditWarehouse.Location = new Point(451, 105);

                    treeListLookUpEditWard.Visible = false;
                    treeListLookUpEditWarehouse.Visible = true;
                    labelControlWarehouse.Text = "Эмийн сан*:";
                    treeListLookUpEditWard.EditValue = true;
                }
                else if (radioGroupType.SelectedIndex == 1)
                {
                    labelControlWarehouse.Location = new Point(407, 108);
                    treeListLookUpEditWard.Location = new Point(451, 105);
                    treeListLookUpEditWarehouse.Location = new Point(451, 27);

                    treeListLookUpEditWard.Visible = true;
                    treeListLookUpEditWarehouse.Visible = false;
                    labelControlWarehouse.Text = "Тасаг*:";
                    treeListLookUpEditWarehouse.EditValue = true;
                }
            }

            
            private void addRow()
            {
                MedicineBalExpForm.Instance.showForm(2, Instance);
            }

            private void deleteRow()
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн тооллого устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    JournalDataSet.CensusDetailBindingSource.RemoveCurrent();
                }
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditIncDate.EditValue == DBNull.Value || dateEditIncDate.EditValue == null || dateEditIncDate.Text.Equals(""))
                {
                    errorText = "Огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditIncDate.ErrorText = errorText;
                    isRight = false;
                    dateEditIncDate.Focus();
                }
                if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.Text.Equals(""))
                {
                    errorText = "Няравыг сонгоно уу";

                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditUser.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.Text.Equals("")))
                {
                    errorText = "Агуулахыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWarehouse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWarehouse.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals("")))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }
                if (dateEditBalDate.EditValue == DBNull.Value || dateEditBalDate.EditValue == null || dateEditBalDate.Text.Equals(""))
                {
                    errorText = "Тооллогын огноог сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditBalDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditBalDate.Focus();
                    }
                }
                if (gridLookUpEditCensusUser.EditValue == DBNull.Value || gridLookUpEditCensusUser.EditValue == null || gridLookUpEditCensusUser.Text.Equals(""))
                {
                    errorText = "Тоолсон няравыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditCensusUser.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditCensusUser.Focus();
                    }
                }
                if (isRight && actionType == 1)
                {
                    bool value = Convert.ToBoolean(JournalDataSet.QueriesTableAdapter.can_insert_census(HospitalUtil.getHospitalId(), dateEditBalDate.Text, gridLookUpEditUser.EditValue.ToString(), radioGroupType.SelectedIndex, (radioGroupType.SelectedIndex == 0 ? treeListLookUpEditWarehouse.EditValue.ToString() : ""), (radioGroupType.SelectedIndex == 1 ? treeListLookUpEditWard.EditValue.ToString() : "")));
                    if (!value)
                    {
                        isRight = false;
                        errorText = "Нярав, агуулах дээр " + dateEditBalDate.Text + " огноон дээр тооллого хийсэн байна";
                        text += ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditUser.ErrorText = errorText;
                    }
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                    DataRowView censusUser = (DataRowView)gridLookUpEditCensusUser.GetSelectedDataRow();

                    if (radioGroupType.SelectedIndex == 0)
                    {  
                        currView["wardID"] = null;
                        currView["warehouseName"] = warehouse["name"];
                        currView["warehouseDesc"] = warehouse["description"];
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {   
                        currView["wardID"] = treeListLookUpEditWard.EditValue;
                        currView["warehouseName"] = ward["name"];
                        currView["warehouseDesc"] = ward["description"];
                    }
                    currView["userName"] = user["fullName"];
                    currView["roleName"] = user["roleName"];
                    currView["censusUserName"] = gridLookUpEditCensusUser.Text;
                    currView["censusRoleName"] = censusUser["roleName"];
                    currView.EndEdit();
                    
                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        DataRow row = gridViewDetail.GetDataRow(i);
                            
                        row["date"] = dateEditIncDate.EditValue;
                        row["userID"] = gridLookUpEditUser.EditValue;
                        row["balDate"] = dateEditBalDate.EditValue;
                        row["censusUserID"] = gridLookUpEditCensusUser.EditValue;
                        row["type"] = radioGroupType.SelectedIndex;
                        row["userName"] = user["fullName"];
                        row["roleName"] = user["roleName"];
                        row["censusUserName"] = gridLookUpEditCensusUser.Text;
                        row["censusRoleName"] = censusUser["roleName"];
                        row["description"] = memoEditDescription.EditValue;
                        if (radioGroupType.SelectedIndex == 0)
                        {
                            row["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                            row["wardID"] = null;
                            row["warehouseName"] = warehouse["name"];
                            row["warehouseDesc"] = warehouse["description"];
                        }
                        else if (radioGroupType.SelectedIndex == 0)
                        {
                            row["warehouseID"] = null;
                            row["wardID"] = treeListLookUpEditWard.EditValue;
                            row["warehouseName"] = ward["name"];
                            row["warehouseDesc"] = ward["description"];
                        }
                    }

                    JournalDataSet.CensusJournalBindingSource.EndEdit();
                    JournalDataSet.CensusJournalTableAdapter.Update(JournalDataSet.Instance.CensusJournal);

                    JournalDataSet.CensusDetailBindingSource.EndEdit();
                    JournalDataSet.CensusDetailTableAdapter.Update(JournalDataSet.Instance.CensusDetail);

                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }
            

            public bool existMedicine(string medicineID)
            {
                for (int i = 0; i < gridViewDetail.RowCount; i++)
                {
                    if (medicineID.Equals(gridViewDetail.GetDataRow(i)["medicinePrice"].ToString()))
                    {
                        return true;
                    }
                }
                return false;
            }

            public void addMedicine(DataRowView view, object incTypeID, string incTypeText, decimal count, decimal price)
            {
                JournalDataSet.Instance.CensusDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                DataRowView newRow = (DataRowView)JournalDataSet.CensusDetailBindingSource.AddNew();
                newRow["incTypeID"] = incTypeID;
                newRow["incTypeName"] = incTypeText;
                newRow["medicineID"] = view["id"];
                newRow["medicineName"] = view["name"];
                newRow["latinName"] = view["latinName"];
                newRow["unitType"] = view["unitType"];
                newRow["shape"] = view["shape"];
                newRow["quantity"] = view["quantity"];
                newRow["validDate"] = view["validDate"];
                newRow["medicineTypeName"] = view["medicineTypeName"];
                newRow["balCount"] = 0;
                newRow["count"] = count;
                newRow["price"] = price;
                newRow.EndEdit();
            }

        #endregion

    }
}