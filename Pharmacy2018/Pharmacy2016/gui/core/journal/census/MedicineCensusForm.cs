﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.report.journal.inc;



namespace Pharmacy2016.gui.core.journal.census
{
    /*
     * Орлогын журнал харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class MedicineCensusForm : DevExpress.XtraEditors.XtraForm
    {
        
        #region Форм анх ачааллах функц

            private static MedicineCensusForm INSTANCE = new MedicineCensusForm();

            public static MedicineCensusForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 2008;

            private bool isInit = false;

            public DataRowView currView;

            private System.IO.Stream layout_inc = null;
            private System.IO.Stream layout_detail = null;

            private MedicineCensusForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initLayout();
                RoleUtil.addForm(FORM_ID, this);

                gridControlCensus.DataSource = JournalDataSet.CensusJournalBindingSource;

                dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                DateTime date = DateTime.Now;
                dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1);
                dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            }

            private void initLayout()
            {
                layout_inc = new System.IO.MemoryStream();
                gridViewCensus.SaveLayoutToStream(layout_inc);
                layout_inc.Seek(0, System.IO.SeekOrigin.Begin);

                layout_detail = new System.IO.MemoryStream();
                gridViewDetail.SaveLayoutToStream(layout_detail);
                layout_detail.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion
        
        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        radioGroupJournal.SelectedIndex = 0;
                        checkRole();

                        JournalDataSet.Instance.CensusJournal.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        JournalDataSet.Instance.CensusJournal.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        JournalDataSet.Instance.CensusDetail.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        JournalDataSet.Instance.CensusDetail.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                    
                        reload();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                gridControlCensus.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlCensus.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridControlCensus.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                dropDownButtonPrint.Visible = roles[4];
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;

                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("census");
                if (layout != null)
                {
                    gridViewCensus.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewCensus.RestoreLayoutFromStream(layout_inc);
                    layout_inc.Seek(0, System.IO.SeekOrigin.Begin);
                }

                layout = UserUtil.getLayout("censusDetail");
                if (layout != null)
                {
                    gridViewDetail.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewDetail.RestoreLayoutFromStream(layout_detail);
                    layout_detail.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }
            
            private void MedicineIncForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                JournalDataSet.Instance.CensusDetail.Clear();
                JournalDataSet.Instance.CensusJournal.Clear();
            }

        #endregion

        #region Формын event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                saveLayout();
            }

            private void gridControlInc_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        add();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewCensus.RowCount != 0 && gridViewCensus.GetFocusedDataRow() != null)
                    {
                        try
                        {
                            delete();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "update") && gridViewCensus.RowCount != 0 && gridViewCensus.GetFocusedDataRow() != null)
                    {
                        update();
                    }
                }
            }

            private void gridViewDetail_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
            {
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo info = e.Info as DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo;
                info.GroupText = string.Format("{0} (Тоо хэмжээ = {1} Нийт үнэ = {2})", info.GroupValueText, gridViewDetail.GetGroupSummaryDisplayText(e.RowHandle, gridViewDetail.GroupSummary[0] as DevExpress.XtraGrid.GridGroupSummaryItem), gridViewDetail.GetGroupSummaryDisplayText(e.RowHandle, gridViewDetail.GroupSummary[1] as DevExpress.XtraGrid.GridGroupSummaryItem));
            }

            private void radioGroupJournal_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (radioGroupJournal.SelectedIndex == 0)
                {
                    JournalDataSet.CensusDetailBindingSource.DataSource = JournalDataSet.CensusJournalBindingSource;
                    JournalDataSet.CensusDetailBindingSource.DataMember = "CensusJournal_CensusDetail";

                    gridControlCensus.DataSource = JournalDataSet.CensusJournalBindingSource;
                    gridControlCensus.MainView = gridViewCensus;

                    gridControlCensus.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlCensus.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridControlCensus.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;
                    checkRole();
                    barButtonItemPrint.Enabled = true;
                    barButtonItemIncDetail.Enabled = true;
                }
                else if (radioGroupJournal.SelectedIndex == 1)
                {
                    JournalDataSet.CensusDetailBindingSource.DataSource = JournalDataSet.Instance;
                    JournalDataSet.CensusDetailBindingSource.DataMember = "CensusDetail";

                    gridControlCensus.DataSource = JournalDataSet.CensusDetailBindingSource;
                    gridControlCensus.MainView = gridViewDetail;

                    gridControlCensus.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlCensus.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridControlCensus.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
                    barButtonItemPrint.Enabled = false;
                    barButtonItemIncDetail.Enabled = false;
                }
            }

            private void gridViewDetail_DoubleClick(object sender, EventArgs e)
            {
                DataRowView view = (DataRowView)gridViewDetail.GetFocusedRow();
                if (view == null)
                    return;


                radioGroupJournal.SelectedIndex = 0;
                for (int i = 0; i < gridViewCensus.RowCount; i++)
                {
                    DataRow row = gridViewCensus.GetDataRow(i);
                    if (view["censusJournalID"].ToString().Equals(row["id"].ToString()))
                    {
                        gridViewCensus.SelectRow(i);
                        if (!gridViewCensus.GetMasterRowExpanded(i))
                            gridViewCensus.SetMasterRowExpanded(i, true);
                    }
                }
            }

        #endregion

        #region Формын функц

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void reload()
            {
                if(check())
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        JournalDataSet.CensusJournalTableAdapter.Fill(JournalDataSet.Instance.CensusJournal, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                        JournalDataSet.CensusDetailTableAdapter.Fill(JournalDataSet.Instance.CensusDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                        ProgramUtil.closeWaitDialog();
                    }
                    else
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        JournalDataSet.CensusJournalTableAdapter.Fill(JournalDataSet.Instance.CensusJournal, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                        JournalDataSet.CensusDetailTableAdapter.Fill(JournalDataSet.Instance.CensusDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            private void add()
            {
                MedicineCensusUpForm.Instance.showForm(1, Instance);
            }

            private void delete()
            {
                //bool isDeleteDoc = false;
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн тооллого устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    gridViewCensus.DeleteSelectedRows();
                    JournalDataSet.CensusJournalBindingSource.EndEdit();
                    JournalDataSet.CensusJournalTableAdapter.Update(JournalDataSet.Instance.CensusJournal);
                    ProgramUtil.closeWaitDialog();
                }

                //GridView detail = null;

                //if(gridViewCensus.IsFocusedView)
                //{
                //    isDeleteDoc = true;
                //}
                //else
                //{
                //    detail = gridViewCensus.GetDetailView(gridViewCensus.FocusedRowHandle, 0) as GridView;
                //    if (detail.IsFocusedView)
                //    {
                //        isDeleteDoc = detail.RowCount == 1;
                //    }
                //}
                //if (isDeleteDoc)
                //{
                //    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн тооллого устгах", MessageBoxButtons.YesNo);
                //    if (dialogResult == DialogResult.Yes)
                //    {
                //        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                //        gridViewCensus.DeleteSelectedRows();
                //        JournalDataSet.CensusJournalBindingSource.EndEdit();
                //        JournalDataSet.CensusJournalTableAdapter.Update(JournalDataSet.Instance.CensusJournal);
                //        ProgramUtil.closeWaitDialog();
                //    }
                //}
                //else
                //{
                //    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн тооллого устгах", MessageBoxButtons.YesNo);
                //    if (dialogResult == DialogResult.Yes)
                //    {
                //        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                //        detail.DeleteSelectedRows();
                //        JournalDataSet.CensusDetailBindingSource.EndEdit();
                //        JournalDataSet.CensusDetailTableAdapter.Update(JournalDataSet.Instance.CensusDetail);
                //        ProgramUtil.closeWaitDialog();
                //    }
                //}
            }

            private void update()
            {
                MedicineCensusUpForm.Instance.showForm(2, Instance);
            }

            public DataRowView getSelectedRow()
            {
                GridView detail = gridViewCensus.GetDetailView(gridViewCensus.FocusedRowHandle, 0) as GridView;
                string id = detail.GetFocusedDataRow()["id"].ToString();
                for (int i = 0; i < JournalDataSet.CensusDetailBindingSource.Count; i++)
                {
                    DataRowView view = (DataRowView)JournalDataSet.CensusDetailBindingSource[i];
                    if (view["id"].ToString().Equals(id))
                        return view;
                }
                return null;
            }

            private void saveLayout()
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    System.IO.StreamReader reader = null;
                    if (radioGroupJournal.SelectedIndex == 0)
                    {
                        gridViewCensus.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        reader = new System.IO.StreamReader(str);
                        UserUtil.saveUserLayout("census", reader.ReadToEnd());
                    }
                    else
                    {
                        gridViewDetail.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        reader = new System.IO.StreamReader(str);
                        UserUtil.saveUserLayout("censusDetail", reader.ReadToEnd());
                    }
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion
        
        #region Хэвлэх, хөрвүүлэх функц

            private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemPrint.Caption;
                dropDownButtonPrint.Image = barButtonItemPrint.Glyph;

                CensusReport.Instance.initAndShow(3, gridViewCensus.ActiveFilterString);
            }

            private void barButtonItemBegBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemIncDetail.Caption;
                dropDownButtonPrint.Image = barButtonItemIncDetail.Glyph;

                if(gridViewCensus.GetFocusedRow() != null)
                    CensusReport.Instance.initAndShow(2, "id = '" + gridViewCensus.GetFocusedDataRow()["id"].ToString() + "'");
                else
                    XtraMessageBox.Show("Мөр сонгоно уу");
            }

            private void barButtonItemEndBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemEndBal.Caption;
                dropDownButtonPrint.Image = barButtonItemEndBal.Glyph;
            }

            private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemConvert.Caption;
                dropDownButtonPrint.Image = barButtonItemConvert.Glyph;

                if(radioGroupJournal.SelectedIndex == 0)
                {
                    ProgramUtil.convertGrid(gridViewCensus, "Тооллогын журнал");
                }
                else
                {
                    ProgramUtil.convertGrid(gridViewDetail, "Тооллогын гүйлгээ");
                }   
            }

        #endregion
    }
}