﻿namespace Pharmacy2016.gui.core.journal.census
{
    partial class MedicineCensusForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicineCensusForm));
            this.gridViewCensusDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlCensus = new DevExpress.XtraGrid.GridControl();
            this.gridViewCensus = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnUserRole = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnUserName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridViewDetail = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLeft = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.treeListWard = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnWard = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemIncDetail = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEndBal = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.radioGroupJournal = new DevExpress.XtraEditors.RadioGroup();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCensusDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCensus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCensus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            this.dockPanelLeft.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupJournal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewCensusDetail
            // 
            this.gridViewCensusDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn1,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15});
            this.gridViewCensusDetail.GridControl = this.gridControlCensus;
            this.gridViewCensusDetail.LevelIndent = 1;
            this.gridViewCensusDetail.Name = "gridViewCensusDetail";
            this.gridViewCensusDetail.OptionsBehavior.ReadOnly = true;
            this.gridViewCensusDetail.OptionsPrint.ExpandAllDetails = true;
            this.gridViewCensusDetail.OptionsPrint.PrintDetails = true;
            this.gridViewCensusDetail.OptionsView.ShowFooter = true;
            this.gridViewCensusDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Төрөл";
            this.gridColumn12.CustomizationCaption = "Орлогын төрөл";
            this.gridColumn12.FieldName = "incTypeName";
            this.gridColumn12.MinWidth = 75;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 0;
            this.gridColumn12.Width = 94;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Эмийн нэр";
            this.gridColumn4.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn4.FieldName = "medicineName";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 126;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "ОУ нэр";
            this.gridColumn5.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn5.FieldName = "latinName";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 134;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Ангилал";
            this.gridColumn2.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn2.FieldName = "medicineTypeName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 3;
            this.gridColumn2.Width = 91;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тоолсон";
            this.gridColumn6.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn6.CustomizationCaption = "Тоолсон";
            this.gridColumn6.FieldName = "count";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 10;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Нэгжийн үнэ";
            this.gridColumn7.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn7.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumn7.FieldName = "price";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 8;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Дүн";
            this.gridColumn8.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn8.CustomizationCaption = "Дүн";
            this.gridColumn8.FieldName = "sumPrice";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 13;
            this.gridColumn8.Width = 100;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Тун хэмжээ";
            this.gridColumn1.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn1.FieldName = "unitType";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 6;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Хэлбэр";
            this.gridColumn9.CustomizationCaption = "Хэлбэр";
            this.gridColumn9.FieldName = "shape";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Хүчинтэй хугацаа";
            this.gridColumn10.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn10.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn10.FieldName = "validDate";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 4;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Хэмжих нэгж";
            this.gridColumn11.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn11.FieldName = "quantity";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 7;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Үлдэгдэл";
            this.gridColumn13.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn13.CustomizationCaption = "Үлдэгдэл";
            this.gridColumn13.FieldName = "balCount";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "balCount", "{0:n2}")});
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 9;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Дутсан";
            this.gridColumn14.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn14.CustomizationCaption = "Дутсан";
            this.gridColumn14.FieldName = "expCount";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expCount", "{0:n2}")});
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 11;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Илүү гарсан";
            this.gridColumn15.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn15.CustomizationCaption = "Илүү гарсан";
            this.gridColumn15.FieldName = "excessCount";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "excessCount", "{0:n2}")});
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 12;
            // 
            // gridControlCensus
            // 
            this.gridControlCensus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCensus.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlCensus.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlCensus.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlCensus.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlCensus.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlCensus.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "update")});
            this.gridControlCensus.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInc_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.LevelTemplate = this.gridViewCensusDetail;
            gridLevelNode1.RelationName = "CensusJournal_CensusDetail";
            this.gridControlCensus.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlCensus.Location = new System.Drawing.Point(2, 52);
            this.gridControlCensus.MainView = this.gridViewCensus;
            this.gridControlCensus.Name = "gridControlCensus";
            this.gridControlCensus.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemSpinEdit1});
            this.gridControlCensus.Size = new System.Drawing.Size(780, 407);
            this.gridControlCensus.TabIndex = 1;
            this.gridControlCensus.UseEmbeddedNavigator = true;
            this.gridControlCensus.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCensus,
            this.gridViewDetail,
            this.gridViewCensusDetail});
            // 
            // gridViewCensus
            // 
            this.gridViewCensus.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand1,
            this.gridBand9,
            this.gridBand11,
            this.gridBand3});
            this.gridViewCensus.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn5,
            this.gridColumn3,
            this.gridColumnUserRole,
            this.bandedGridColumn20,
            this.bandedGridColumn18,
            this.gridColumnUserName,
            this.bandedGridColumn8,
            this.bandedGridColumn4,
            this.bandedGridColumn2,
            this.bandedGridColumn1,
            this.bandedGridColumn23,
            this.bandedGridColumn28});
            this.gridViewCensus.GridControl = this.gridControlCensus;
            this.gridViewCensus.LevelIndent = 0;
            this.gridViewCensus.Name = "gridViewCensus";
            this.gridViewCensus.OptionsBehavior.ReadOnly = true;
            this.gridViewCensus.OptionsDetail.ShowDetailTabs = false;
            this.gridViewCensus.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewCensus.OptionsPrint.ExpandAllDetails = true;
            this.gridViewCensus.OptionsPrint.PrintDetails = true;
            this.gridViewCensus.OptionsView.ShowAutoFilterRow = true;
            this.gridViewCensus.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Баримт";
            this.gridBand2.Columns.Add(this.bandedGridColumn5);
            this.gridBand2.Columns.Add(this.gridColumn3);
            this.gridBand2.Columns.Add(this.bandedGridColumn28);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 259;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.Caption = "Дугаар";
            this.bandedGridColumn5.CustomizationCaption = "Дугаар";
            this.bandedGridColumn5.FieldName = "id";
            this.bandedGridColumn5.MinWidth = 75;
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn5.Visible = true;
            this.bandedGridColumn5.Width = 90;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Огноо";
            this.gridColumn3.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn3.CustomizationCaption = "Огноо";
            this.gridColumn3.FieldName = "date";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.Width = 94;
            // 
            // bandedGridColumn28
            // 
            this.bandedGridColumn28.Caption = "Тооллогын огноо";
            this.bandedGridColumn28.CustomizationCaption = "Тооллого хийх огноо";
            this.bandedGridColumn28.FieldName = "balDate";
            this.bandedGridColumn28.MinWidth = 75;
            this.bandedGridColumn28.Name = "bandedGridColumn28";
            this.bandedGridColumn28.Visible = true;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Нярав";
            this.gridBand1.Columns.Add(this.gridColumnUserRole);
            this.gridBand1.Columns.Add(this.bandedGridColumn20);
            this.gridBand1.Columns.Add(this.bandedGridColumn18);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 1;
            this.gridBand1.Width = 222;
            // 
            // gridColumnUserRole
            // 
            this.gridColumnUserRole.Caption = "Дугаар";
            this.gridColumnUserRole.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumnUserRole.FieldName = "userID";
            this.gridColumnUserRole.MinWidth = 75;
            this.gridColumnUserRole.Name = "gridColumnUserRole";
            this.gridColumnUserRole.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumnUserRole.Visible = true;
            this.gridColumnUserRole.Width = 109;
            // 
            // bandedGridColumn20
            // 
            this.bandedGridColumn20.Caption = "Нэр";
            this.bandedGridColumn20.CustomizationCaption = "Хэрэглэгчийн нэр";
            this.bandedGridColumn20.FieldName = "userName";
            this.bandedGridColumn20.MinWidth = 75;
            this.bandedGridColumn20.Name = "bandedGridColumn20";
            this.bandedGridColumn20.Visible = true;
            this.bandedGridColumn20.Width = 113;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.Caption = "Эрх";
            this.bandedGridColumn18.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.bandedGridColumn18.FieldName = "roleName";
            this.bandedGridColumn18.MinWidth = 75;
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand9.AppearanceHeader.Options.UseFont = true;
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "Агуулах";
            this.gridBand9.Columns.Add(this.gridColumnUserName);
            this.gridBand9.Columns.Add(this.bandedGridColumn8);
            this.gridBand9.CustomizationCaption = "Агуулах";
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 2;
            this.gridBand9.Width = 237;
            // 
            // gridColumnUserName
            // 
            this.gridColumnUserName.Caption = "Нэр";
            this.gridColumnUserName.CustomizationCaption = "Агуулахын нэр";
            this.gridColumnUserName.FieldName = "warehouseName";
            this.gridColumnUserName.MinWidth = 75;
            this.gridColumnUserName.Name = "gridColumnUserName";
            this.gridColumnUserName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumnUserName.Visible = true;
            this.gridColumnUserName.Width = 118;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn8.CustomizationCaption = "Агуулахын дэлгэрэнгүй";
            this.bandedGridColumn8.FieldName = "warehouseDesc";
            this.bandedGridColumn8.MinWidth = 75;
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.Visible = true;
            this.bandedGridColumn8.Width = 119;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand11.AppearanceHeader.Options.UseFont = true;
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "Тоолсон нярав";
            this.gridBand11.Columns.Add(this.bandedGridColumn2);
            this.gridBand11.Columns.Add(this.bandedGridColumn1);
            this.gridBand11.Columns.Add(this.bandedGridColumn23);
            this.gridBand11.CustomizationCaption = "Тоолсон нярав";
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 3;
            this.gridBand11.Width = 202;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "Дугаар";
            this.bandedGridColumn2.CustomizationCaption = "Тоолсон няравын дугаар";
            this.bandedGridColumn2.FieldName = "censusUserID";
            this.bandedGridColumn2.MinWidth = 75;
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.Visible = true;
            this.bandedGridColumn2.Width = 93;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "Нэр";
            this.bandedGridColumn1.CustomizationCaption = "Тоолсон няравын нэр";
            this.bandedGridColumn1.FieldName = "censusUserName";
            this.bandedGridColumn1.MinWidth = 75;
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.Visible = true;
            this.bandedGridColumn1.Width = 109;
            // 
            // bandedGridColumn23
            // 
            this.bandedGridColumn23.Caption = "Эрх";
            this.bandedGridColumn23.CustomizationCaption = "Тоолсон няравын эрх";
            this.bandedGridColumn23.FieldName = "censusRoleName";
            this.bandedGridColumn23.MinWidth = 75;
            this.bandedGridColumn23.Name = "bandedGridColumn23";
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Нэмэлт мэдээлэл";
            this.gridBand3.Columns.Add(this.bandedGridColumn4);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 4;
            this.gridBand3.Width = 119;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "Тайлбар";
            this.bandedGridColumn4.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn4.FieldName = "description";
            this.bandedGridColumn4.MinWidth = 75;
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 119;
            // 
            // gridViewDetail
            // 
            this.gridViewDetail.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand5,
            this.gridBand10,
            this.gridBand12,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8});
            this.gridViewDetail.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn14,
            this.bandedGridColumn6,
            this.bandedGridColumn29,
            this.bandedGridColumn7,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn25,
            this.bandedGridColumn26,
            this.bandedGridColumn12,
            this.bandedGridColumn13,
            this.bandedGridColumn30,
            this.bandedGridColumn11,
            this.bandedGridColumn24,
            this.bandedGridColumn3,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.bandedGridColumnCount,
            this.bandedGridColumn19,
            this.bandedGridColumnSumPrice,
            this.bandedGridColumn21,
            this.bandedGridColumn22,
            this.bandedGridColumn27,
            this.bandedGridColumn31,
            this.bandedGridColumn32,
            this.bandedGridColumn33});
            this.gridViewDetail.GridControl = this.gridControlCensus;
            this.gridViewDetail.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", this.bandedGridColumnCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", this.bandedGridColumnSumPrice, "{0:c2}")});
            this.gridViewDetail.LevelIndent = 0;
            this.gridViewDetail.Name = "gridViewDetail";
            this.gridViewDetail.OptionsBehavior.Editable = false;
            this.gridViewDetail.OptionsBehavior.ReadOnly = true;
            this.gridViewDetail.OptionsDetail.ShowDetailTabs = false;
            this.gridViewDetail.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewDetail.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDetail.OptionsView.ShowFooter = true;
            this.gridViewDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewDetail.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridViewDetail_CustomDrawGroupRow);
            this.gridViewDetail.DoubleClick += new System.EventHandler(this.gridViewDetail_DoubleClick);
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Баримт";
            this.gridBand4.Columns.Add(this.bandedGridColumn14);
            this.gridBand4.Columns.Add(this.bandedGridColumn6);
            this.gridBand4.Columns.Add(this.bandedGridColumn29);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 259;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.Caption = "Дугаар";
            this.bandedGridColumn14.CustomizationCaption = "Дугаар";
            this.bandedGridColumn14.FieldName = "incJournalID";
            this.bandedGridColumn14.MinWidth = 75;
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn14.Visible = true;
            this.bandedGridColumn14.Width = 90;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "Огноо";
            this.bandedGridColumn6.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumn6.CustomizationCaption = "Огноо";
            this.bandedGridColumn6.FieldName = "date";
            this.bandedGridColumn6.MinWidth = 75;
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn6.Visible = true;
            this.bandedGridColumn6.Width = 94;
            // 
            // bandedGridColumn29
            // 
            this.bandedGridColumn29.Caption = "Тооллогын огноо";
            this.bandedGridColumn29.CustomizationCaption = "Тооллогын огноо";
            this.bandedGridColumn29.FieldName = "balDate";
            this.bandedGridColumn29.MinWidth = 75;
            this.bandedGridColumn29.Name = "bandedGridColumn29";
            this.bandedGridColumn29.Visible = true;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Нярав";
            this.gridBand5.Columns.Add(this.bandedGridColumn7);
            this.gridBand5.Columns.Add(this.bandedGridColumn9);
            this.gridBand5.Columns.Add(this.bandedGridColumn10);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 1;
            this.gridBand5.Width = 182;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "Дугаар";
            this.bandedGridColumn7.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.bandedGridColumn7.FieldName = "userID";
            this.bandedGridColumn7.MinWidth = 75;
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 91;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "Нэр";
            this.bandedGridColumn9.CustomizationCaption = "Хэрэглэгчийн нэр";
            this.bandedGridColumn9.FieldName = "userName";
            this.bandedGridColumn9.MinWidth = 75;
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.Visible = true;
            this.bandedGridColumn9.Width = 91;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.Caption = "Эрх";
            this.bandedGridColumn10.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.bandedGridColumn10.FieldName = "roleName";
            this.bandedGridColumn10.MinWidth = 75;
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn10.Width = 96;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand10.AppearanceHeader.Options.UseFont = true;
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "Агуулах";
            this.gridBand10.Columns.Add(this.bandedGridColumn25);
            this.gridBand10.Columns.Add(this.bandedGridColumn26);
            this.gridBand10.CustomizationCaption = "Агуулах";
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 2;
            this.gridBand10.Width = 150;
            // 
            // bandedGridColumn25
            // 
            this.bandedGridColumn25.Caption = "Нэр";
            this.bandedGridColumn25.CustomizationCaption = "Агуулахын нэр";
            this.bandedGridColumn25.FieldName = "warehouseName";
            this.bandedGridColumn25.MinWidth = 75;
            this.bandedGridColumn25.Name = "bandedGridColumn25";
            this.bandedGridColumn25.Visible = true;
            // 
            // bandedGridColumn26
            // 
            this.bandedGridColumn26.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn26.CustomizationCaption = "Агуулахын дэлгэрэнгүй";
            this.bandedGridColumn26.FieldName = "warehouseDesc";
            this.bandedGridColumn26.MinWidth = 75;
            this.bandedGridColumn26.Name = "bandedGridColumn26";
            this.bandedGridColumn26.Visible = true;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand12.AppearanceHeader.Options.UseFont = true;
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "Тоолсон нярав";
            this.gridBand12.Columns.Add(this.bandedGridColumn12);
            this.gridBand12.Columns.Add(this.bandedGridColumn13);
            this.gridBand12.Columns.Add(this.bandedGridColumn30);
            this.gridBand12.CustomizationCaption = "Тоолсон нярав";
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 3;
            this.gridBand12.Width = 162;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.Caption = "Дугаар";
            this.bandedGridColumn12.CustomizationCaption = "Тоолсон няравын дугаар";
            this.bandedGridColumn12.FieldName = "censusUserID";
            this.bandedGridColumn12.MinWidth = 75;
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.Visible = true;
            this.bandedGridColumn12.Width = 87;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.Caption = "Нэр";
            this.bandedGridColumn13.CustomizationCaption = "Тоолсон няравын нэр";
            this.bandedGridColumn13.FieldName = "censusUserName";
            this.bandedGridColumn13.MinWidth = 75;
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.Visible = true;
            // 
            // bandedGridColumn30
            // 
            this.bandedGridColumn30.Caption = "Эрх";
            this.bandedGridColumn30.CustomizationCaption = "Тоолсон няравын эрх";
            this.bandedGridColumn30.FieldName = "censusRoleName";
            this.bandedGridColumn30.MinWidth = 75;
            this.bandedGridColumn30.Name = "bandedGridColumn30";
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand6.AppearanceHeader.Options.UseFont = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Нэмэлт мэдээлэл";
            this.gridBand6.Columns.Add(this.bandedGridColumn11);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 4;
            this.gridBand6.Width = 119;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.Caption = "Тайлбар";
            this.bandedGridColumn11.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn11.FieldName = "description";
            this.bandedGridColumn11.MinWidth = 75;
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn11.Visible = true;
            this.bandedGridColumn11.Width = 119;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand7.AppearanceHeader.Options.UseFont = true;
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "Эмийн мэдээлэл";
            this.gridBand7.Columns.Add(this.bandedGridColumn24);
            this.gridBand7.Columns.Add(this.bandedGridColumn3);
            this.gridBand7.Columns.Add(this.bandedGridColumn15);
            this.gridBand7.Columns.Add(this.bandedGridColumn16);
            this.gridBand7.Columns.Add(this.bandedGridColumn22);
            this.gridBand7.Columns.Add(this.bandedGridColumn21);
            this.gridBand7.Columns.Add(this.bandedGridColumn17);
            this.gridBand7.Columns.Add(this.bandedGridColumn27);
            this.gridBand7.CustomizationCaption = "Эмийн мэдээлэл";
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 5;
            this.gridBand7.Width = 600;
            // 
            // bandedGridColumn24
            // 
            this.bandedGridColumn24.Caption = "Орлогын төрөл";
            this.bandedGridColumn24.CustomizationCaption = "Орлогын төрөл";
            this.bandedGridColumn24.FieldName = "incTypeName";
            this.bandedGridColumn24.MinWidth = 75;
            this.bandedGridColumn24.Name = "bandedGridColumn24";
            this.bandedGridColumn24.Visible = true;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.Caption = "Эмийн нэр";
            this.bandedGridColumn3.CustomizationCaption = "Худалдааны нэр";
            this.bandedGridColumn3.FieldName = "medicineName";
            this.bandedGridColumn3.MinWidth = 75;
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.Visible = true;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.Caption = "ОУ нэр";
            this.bandedGridColumn15.CustomizationCaption = "Олон улсын нэр";
            this.bandedGridColumn15.FieldName = "latinName";
            this.bandedGridColumn15.MinWidth = 75;
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.Visible = true;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.Caption = "Ангилал";
            this.bandedGridColumn16.CustomizationCaption = "Эмийн ангилал";
            this.bandedGridColumn16.FieldName = "medicineTypeName";
            this.bandedGridColumn16.MinWidth = 75;
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.Visible = true;
            // 
            // bandedGridColumn22
            // 
            this.bandedGridColumn22.Caption = "Хүчинтэй хугацаа";
            this.bandedGridColumn22.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumn22.CustomizationCaption = "Хүчинтэй хугацаа";
            this.bandedGridColumn22.FieldName = "validDate";
            this.bandedGridColumn22.MinWidth = 75;
            this.bandedGridColumn22.Name = "bandedGridColumn22";
            this.bandedGridColumn22.Visible = true;
            // 
            // bandedGridColumn21
            // 
            this.bandedGridColumn21.Caption = "Хэлбэр";
            this.bandedGridColumn21.CustomizationCaption = "Хэлбэр";
            this.bandedGridColumn21.FieldName = "shape";
            this.bandedGridColumn21.MinWidth = 75;
            this.bandedGridColumn21.Name = "bandedGridColumn21";
            this.bandedGridColumn21.Visible = true;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.Caption = "Тун хэмжээ";
            this.bandedGridColumn17.CustomizationCaption = "Тун хэмжээ";
            this.bandedGridColumn17.FieldName = "unitType";
            this.bandedGridColumn17.MinWidth = 75;
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.Visible = true;
            // 
            // bandedGridColumn27
            // 
            this.bandedGridColumn27.Caption = "Хэмжих нэгж";
            this.bandedGridColumn27.CustomizationCaption = "Хэмжих нэгж";
            this.bandedGridColumn27.FieldName = "quantity";
            this.bandedGridColumn27.MinWidth = 75;
            this.bandedGridColumn27.Name = "bandedGridColumn27";
            this.bandedGridColumn27.Visible = true;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "Дүн";
            this.gridBand8.Columns.Add(this.bandedGridColumn19);
            this.gridBand8.Columns.Add(this.bandedGridColumn31);
            this.gridBand8.Columns.Add(this.bandedGridColumnCount);
            this.gridBand8.Columns.Add(this.bandedGridColumn32);
            this.gridBand8.Columns.Add(this.bandedGridColumn33);
            this.gridBand8.Columns.Add(this.bandedGridColumnSumPrice);
            this.gridBand8.CustomizationCaption = "Дүн";
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 6;
            this.gridBand8.Width = 450;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.Caption = "Нэгжийн үнэ";
            this.bandedGridColumn19.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn19.CustomizationCaption = "Нэгжийн үнэ";
            this.bandedGridColumn19.FieldName = "price";
            this.bandedGridColumn19.MinWidth = 75;
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.Visible = true;
            // 
            // bandedGridColumn31
            // 
            this.bandedGridColumn31.Caption = "Үлдэгдэл";
            this.bandedGridColumn31.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn31.CustomizationCaption = "Үлдэгдэл";
            this.bandedGridColumn31.FieldName = "balCount";
            this.bandedGridColumn31.MinWidth = 75;
            this.bandedGridColumn31.Name = "bandedGridColumn31";
            this.bandedGridColumn31.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "balCount", "{0:n2}")});
            this.bandedGridColumn31.Visible = true;
            // 
            // bandedGridColumnCount
            // 
            this.bandedGridColumnCount.Caption = "Тоолсон";
            this.bandedGridColumnCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnCount.CustomizationCaption = "Тоолсон";
            this.bandedGridColumnCount.FieldName = "count";
            this.bandedGridColumnCount.MinWidth = 75;
            this.bandedGridColumnCount.Name = "bandedGridColumnCount";
            this.bandedGridColumnCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.bandedGridColumnCount.Visible = true;
            // 
            // bandedGridColumn32
            // 
            this.bandedGridColumn32.Caption = "Зөрсөн";
            this.bandedGridColumn32.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn32.CustomizationCaption = "Зөрсөн";
            this.bandedGridColumn32.FieldName = "expCount";
            this.bandedGridColumn32.MinWidth = 75;
            this.bandedGridColumn32.Name = "bandedGridColumn32";
            this.bandedGridColumn32.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "expCount", "{0:n2}")});
            this.bandedGridColumn32.Visible = true;
            // 
            // bandedGridColumn33
            // 
            this.bandedGridColumn33.Caption = "Илүү гарсан";
            this.bandedGridColumn33.CustomizationCaption = "Илүү гарсан";
            this.bandedGridColumn33.FieldName = "excessCount";
            this.bandedGridColumn33.MinWidth = 75;
            this.bandedGridColumn33.Name = "bandedGridColumn33";
            this.bandedGridColumn33.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "excessCount", "{0:n2}")});
            this.bandedGridColumn33.Visible = true;
            // 
            // bandedGridColumnSumPrice
            // 
            this.bandedGridColumnSumPrice.Caption = "Дүн";
            this.bandedGridColumnSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnSumPrice.CustomizationCaption = "Дүн";
            this.bandedGridColumnSumPrice.FieldName = "sumPrice";
            this.bandedGridColumnSumPrice.MinWidth = 75;
            this.bandedGridColumnSumPrice.Name = "bandedGridColumnSumPrice";
            this.bandedGridColumnSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.bandedGridColumnSumPrice.Visible = true;
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLeft});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanelLeft
            // 
            this.dockPanelLeft.Controls.Add(this.dockPanel1_Container);
            this.dockPanelLeft.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelLeft.ID = new System.Guid("32690437-4757-43e6-b1e2-053573c2cee2");
            this.dockPanelLeft.Location = new System.Drawing.Point(0, 0);
            this.dockPanelLeft.Name = "dockPanelLeft";
            this.dockPanelLeft.Options.ShowCloseButton = false;
            this.dockPanelLeft.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelLeft.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelLeft.SavedIndex = 0;
            this.dockPanelLeft.Size = new System.Drawing.Size(200, 461);
            this.dockPanelLeft.Text = "Тасаг";
            this.dockPanelLeft.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.treeListWard);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(192, 434);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // treeListWard
            // 
            this.treeListWard.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnWard});
            this.treeListWard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListWard.Location = new System.Drawing.Point(0, 0);
            this.treeListWard.Name = "treeListWard";
            this.treeListWard.BeginUnboundLoad();
            this.treeListWard.AppendNode(new object[] {
            "Хавдар судлалын үндэсний төв"}, -1);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 1"}, 0);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 2"}, 0);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 3"}, 0);
            this.treeListWard.EndUnboundLoad();
            this.treeListWard.OptionsBehavior.Editable = false;
            this.treeListWard.OptionsBehavior.ExpandNodeOnDrag = false;
            this.treeListWard.OptionsView.ShowColumns = false;
            this.treeListWard.OptionsView.ShowIndicator = false;
            this.treeListWard.Size = new System.Drawing.Size(192, 434);
            this.treeListWard.TabIndex = 0;
            // 
            // treeListColumnWard
            // 
            this.treeListColumnWard.Caption = "Тасаг";
            this.treeListColumnWard.FieldName = "ward";
            this.treeListColumnWard.MinWidth = 52;
            this.treeListColumnWard.Name = "treeListColumnWard";
            this.treeListColumnWard.OptionsColumn.ReadOnly = true;
            this.treeListColumnWard.Visible = true;
            this.treeListColumnWard.VisibleIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControlCensus);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 461);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButtonLayout);
            this.panelControl2.Controls.Add(this.dropDownButtonPrint);
            this.panelControl2.Controls.Add(this.radioGroupJournal);
            this.panelControl2.Controls.Add(this.dateEditEnd);
            this.panelControl2.Controls.Add(this.dateEditStart);
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(780, 50);
            this.panelControl2.TabIndex = 1;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(747, 10);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 8;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.DropDownControl = this.popupMenu;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(641, 10);
            this.dropDownButtonPrint.MenuManager = this.barManager;
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.barManager.SetPopupContextMenu(this.dropDownButtonPrint, this.popupMenu);
            this.dropDownButtonPrint.Size = new System.Drawing.Size(100, 23);
            this.dropDownButtonPrint.TabIndex = 6;
            this.dropDownButtonPrint.Text = "Хэвлэх";
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemIncDetail),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemEndBal),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "Хэвлэх /Бүгд/";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 0;
            this.barButtonItemPrint.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.LargeGlyph")));
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemIncDetail
            // 
            this.barButtonItemIncDetail.Caption = "Хэвлэх /Сонгосон/";
            this.barButtonItemIncDetail.Description = "Хэвлэх /Сонгосон/";
            this.barButtonItemIncDetail.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemIncDetail.Glyph")));
            this.barButtonItemIncDetail.Id = 1;
            this.barButtonItemIncDetail.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemIncDetail.LargeGlyph")));
            this.barButtonItemIncDetail.Name = "barButtonItemIncDetail";
            this.barButtonItemIncDetail.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemBegBal_ItemClick);
            // 
            // barButtonItemEndBal
            // 
            this.barButtonItemEndBal.Caption = "Эцсийн үлдэгдэл";
            this.barButtonItemEndBal.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.Glyph")));
            this.barButtonItemEndBal.Id = 2;
            this.barButtonItemEndBal.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.LargeGlyph")));
            this.barButtonItemEndBal.Name = "barButtonItemEndBal";
            this.barButtonItemEndBal.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemEndBal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEndBal_ItemClick);
            // 
            // barButtonItemConvert
            // 
            this.barButtonItemConvert.Caption = "Хөрвүүлэх";
            this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
            this.barButtonItemConvert.Id = 3;
            this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
            this.barButtonItemConvert.Name = "barButtonItemConvert";
            this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemPrint,
            this.barButtonItemIncDetail,
            this.barButtonItemEndBal,
            this.barButtonItemConvert});
            this.barManager.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 461);
            this.barDockControlBottom.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 461);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(784, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 461);
            // 
            // radioGroupJournal
            // 
            this.radioGroupJournal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupJournal.Location = new System.Drawing.Point(509, 10);
            this.radioGroupJournal.Name = "radioGroupJournal";
            this.radioGroupJournal.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Баримт", true, "Journal"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Гүйлгээ", true, "Detail")});
            this.radioGroupJournal.Size = new System.Drawing.Size(126, 23);
            this.radioGroupJournal.TabIndex = 5;
            this.radioGroupJournal.SelectedIndexChanged += new System.EventHandler(this.radioGroupJournal_SelectedIndexChanged);
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(116, 12);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(100, 20);
            this.dateEditEnd.TabIndex = 3;
            this.dateEditEnd.ToolTip = "Дуусах огноо";
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(10, 12);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(100, 20);
            this.dateEditStart.TabIndex = 2;
            this.dateEditStart.ToolTip = "Эхлэх огноо";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(222, 10);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonReload.TabIndex = 4;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Орлогын журналыг шүүх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // MedicineCensusForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MedicineCensusForm";
            this.ShowInTaskbar = false;
            this.Text = "Тооллогын журнал";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MedicineIncForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCensusDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCensus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCensus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            this.dockPanelLeft.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupJournal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLeft;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraTreeList.TreeList treeListWard;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnWard;
        private DevExpress.XtraGrid.GridControl gridControlCensus;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCensusDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewCensus;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnUserRole;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnUserName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.RadioGroup radioGroupJournal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewDetail;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnSumPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn21;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemIncDetail;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEndBal;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn24;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn27;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn23;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn33;
        
    }
}