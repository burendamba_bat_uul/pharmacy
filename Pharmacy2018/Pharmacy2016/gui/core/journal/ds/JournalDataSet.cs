﻿using System.Windows.Forms;
using System.ComponentModel;
using System.Data;


namespace Pharmacy2016.gui.core.journal.ds
{
    
    public partial class JournalDataSet
    {
        partial class IncJournalDataTable
        {
        }
        
        #region DataSet-н обьект

            private static JournalDataSet INSTANCE = new JournalDataSet();

            public static JournalDataSet Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

        #endregion

        #region TableAdapter, BindingSource-ууд

            // QueriesTableAdapter
            private static readonly JournalDataSetTableAdapters.QueriesTableAdapter INSTANCE_QUERY_TABLE_ADAPTER = new JournalDataSetTableAdapters.QueriesTableAdapter();
            public static JournalDataSetTableAdapters.QueriesTableAdapter QueriesTableAdapter
            {
                get
                {
                    return INSTANCE_QUERY_TABLE_ADAPTER;
                }
            }
                    

            // Эмийн ерөнхий мэдээлэл
            private static readonly JournalDataSetTableAdapters.MedicineOtherTableAdapter INSTANCE_MEDICINE_OTHER_TABLE_ADAPTER = new JournalDataSetTableAdapters.MedicineOtherTableAdapter();
            public static JournalDataSetTableAdapters.MedicineOtherTableAdapter MedicineOtherTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINE_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINE_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineOtherBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINE_OTHER_BINDING_SOURCE;
                }
            }



            // Эмийн эхний үлдэгдэл
            private static readonly JournalDataSetTableAdapters.BegBalTableAdapter INSTANCE_BEG_BAL_TABLE_ADAPTER = new JournalDataSetTableAdapters.BegBalTableAdapter();
            public static JournalDataSetTableAdapters.BegBalTableAdapter BegBalTableAdapter
            {
                get
                {
                    return INSTANCE_BEG_BAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_BEG_BAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource BegBalBindingSource
            {
                get
                {
                    return INSTANCE_BEG_BAL_BINDING_SOURCE;
                }
            }



            // Орлогын журнал
            private static readonly JournalDataSetTableAdapters.IncJournalTableAdapter INSTANCE_INC_JOURNAL_TABLE_ADAPTER = new JournalDataSetTableAdapters.IncJournalTableAdapter();
            public static JournalDataSetTableAdapters.IncJournalTableAdapter IncJournalTableAdapter
            {
                get
                {
                    return INSTANCE_INC_JOURNAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_INC_JOURNAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource IncJournalBindingSource
            {
                get
                {
                    return INSTANCE_INC_JOURNAL_BINDING_SOURCE;
                }
            }


            // Орлогын журналын эм
            private static readonly JournalDataSetTableAdapters.IncDetailTableAdapter INSTANCE_INC_DETAIL_TABLE_ADAPTER = new JournalDataSetTableAdapters.IncDetailTableAdapter();
            public static JournalDataSetTableAdapters.IncDetailTableAdapter IncDetailTableAdapter
            {
                get
                {
                    return INSTANCE_INC_DETAIL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_INC_DETAIL_BINDING_SOURCE = new BindingSource();
            public static BindingSource IncDetailBindingSource
            {
                get
                {
                    return INSTANCE_INC_DETAIL_BINDING_SOURCE;
                }
            }



            // Зарлагын журнал
            private static readonly JournalDataSetTableAdapters.ExpJournalTableAdapter INSTANCE_EXP_JOURNAL_TABLE_ADAPTER = new JournalDataSetTableAdapters.ExpJournalTableAdapter();
            public static JournalDataSetTableAdapters.ExpJournalTableAdapter ExpJournalTableAdapter
            {
                get
                {
                    return INSTANCE_EXP_JOURNAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_EXP_JOURNAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource ExpJournalBindingSource
            {
                get
                {
                    return INSTANCE_EXP_JOURNAL_BINDING_SOURCE;
                }
            }


            // Зарлагын журналын эм
            private static readonly JournalDataSetTableAdapters.ExpDetailTableAdapter INSTANCE_EXP_DETAIL_TABLE_ADAPTER = new JournalDataSetTableAdapters.ExpDetailTableAdapter();
            public static JournalDataSetTableAdapters.ExpDetailTableAdapter ExpDetailTableAdapter
            {
                get
                {
                    return INSTANCE_EXP_DETAIL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_EXP_DETAIL_BINDING_SOURCE = new BindingSource();
            public static BindingSource ExpDetailBindingSource
            {
                get
                {
                    return INSTANCE_EXP_DETAIL_BINDING_SOURCE;
                }
            }



            // Эмийн үлдэгдэл
            private static readonly JournalDataSetTableAdapters.MedicineBalTableAdapter INSTANCE_MEDICINE_BAL_TABLE_ADAPTER = new JournalDataSetTableAdapters.MedicineBalTableAdapter();
            public static JournalDataSetTableAdapters.MedicineBalTableAdapter MedicineBalTableAdapter
            {
                get
                {
                    return INSTANCE_MEDICINE_BAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MEDICINE_BAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource MedicineBalBindingSource
            {
                get
                {
                    return INSTANCE_MEDICINE_BAL_BINDING_SOURCE;
                }
            }



            // Хөдөлгөөний журнал
            private static readonly JournalDataSetTableAdapters.MotJournalTableAdapter INSTANCE_MOT_JOURNAL_TABLE_ADAPTER = new JournalDataSetTableAdapters.MotJournalTableAdapter();
            public static JournalDataSetTableAdapters.MotJournalTableAdapter MotJournalTableAdapter
            {
                get
                {
                    return INSTANCE_MOT_JOURNAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MOT_JOURNAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource MotJournalBindingSource
            {
                get
                {
                    return INSTANCE_MOT_JOURNAL_BINDING_SOURCE;
                }
            }


            // Хөдөлгөөний журналын эм
            private static readonly JournalDataSetTableAdapters.MotDetailTableAdapter INSTANCE_MOT_DETAIL_TABLE_ADAPTER = new JournalDataSetTableAdapters.MotDetailTableAdapter();
            public static JournalDataSetTableAdapters.MotDetailTableAdapter MotDetailTableAdapter
            {
                get
                {
                    return INSTANCE_MOT_DETAIL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_MOT_DETAIL_BINDING_SOURCE = new BindingSource();
            public static BindingSource MotDetailBindingSource
            {
                get
                {
                    return INSTANCE_MOT_DETAIL_BINDING_SOURCE;
                }
            }



            // Үлдэгдэл
            private static readonly JournalDataSetTableAdapters.BalTableAdapter INSTANCE_BAL_TABLE_ADAPTER = new JournalDataSetTableAdapters.BalTableAdapter();
            public static JournalDataSetTableAdapters.BalTableAdapter BalTableAdapter
            {
                get
                {
                    return INSTANCE_BAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_BAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource BalBindingSource
            {
                get
                {
                    return INSTANCE_BAL_BINDING_SOURCE;
                }
            }



            // Эмчлүүлэгчид олгох эм
            private static readonly JournalDataSetTableAdapters.PatientMedicineTableAdapter INSTANCE_PATIENT_MEDICINE_TABLE_ADAPTER = new JournalDataSetTableAdapters.PatientMedicineTableAdapter();
            public static JournalDataSetTableAdapters.PatientMedicineTableAdapter PatientMedicineTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_MEDICINE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_MEDICINE_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientMedicineBindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_MEDICINE_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_MEDICINE_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientMedicineOtherBindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_MEDICINE_OTHER_BINDING_SOURCE;
                }
            }



            // Захиалгын хуудас
            private static readonly JournalDataSetTableAdapters.OrderGoodsTableAdapter INSTANCE_ORDER_GOODS_TABLE_ADAPTER = new JournalDataSetTableAdapters.OrderGoodsTableAdapter();
            public static JournalDataSetTableAdapters.OrderGoodsTableAdapter OrderGoodsTableAdapter
            {
                get
                {
                    return INSTANCE_ORDER_GOODS_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ORDER_GOODS_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrderGoodsBindingSource
            {
                get
                {
                    return INSTANCE_ORDER_GOODS_BINDING_SOURCE;
                }
            }


            // Захиалгын хуудасны эм
            private static readonly JournalDataSetTableAdapters.OrderGoodsDetailTableAdapter INSTANCE_ORDER_GOODS_DETAIL_TABLE_ADAPTER = new JournalDataSetTableAdapters.OrderGoodsDetailTableAdapter();
            public static JournalDataSetTableAdapters.OrderGoodsDetailTableAdapter OrderGoodsDetailTableAdapter
            {
                get
                {
                    return INSTANCE_ORDER_GOODS_DETAIL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ORDER_GOODS_DETAIL_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrderGoodsDetailBindingSource
            {
                get
                {
                    return INSTANCE_ORDER_GOODS_DETAIL_BINDING_SOURCE;
                }
            }

            // Захиалгын хуудасны батлах
            private static readonly JournalDataSetTableAdapters.OrderStageTableAdapter INSTANCE_ORDER_STAGE_TABLE_ADAPTER = new JournalDataSetTableAdapters.OrderStageTableAdapter();
            public static JournalDataSetTableAdapters.OrderStageTableAdapter OrderStageTableAdapter
            {
                get
                {
                    return INSTANCE_ORDER_STAGE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ORDER_STAGE_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrderStageBindingSource
            {
                get
                {
                    return INSTANCE_ORDER_STAGE_BINDING_SOURCE;
                }
            }



            // Захиалгын хуудас татах
            private static readonly JournalDataSetTableAdapters.OrderGoodsOtherTableAdapter INSTANCE_ORDER_GOODS_OTHER_TABLE_ADAPTER = new JournalDataSetTableAdapters.OrderGoodsOtherTableAdapter();
            public static JournalDataSetTableAdapters.OrderGoodsOtherTableAdapter OrderGoodsOtherTableAdapter
            {
                get
                {
                    return INSTANCE_ORDER_GOODS_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ORDER_GOODS_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrderGoodsOtherBindingSource
            {
                get
                {
                    return INSTANCE_ORDER_GOODS_OTHER_BINDING_SOURCE;
                }
            }


            // Захиалгын хуудасны эм татах
            private static readonly JournalDataSetTableAdapters.OrderGoodsOtherDetailTableAdapter INSTANCE_ORDER_GOODS_OTHER_DETAIL_TABLE_ADAPTER = new JournalDataSetTableAdapters.OrderGoodsOtherDetailTableAdapter();
            public static JournalDataSetTableAdapters.OrderGoodsOtherDetailTableAdapter OrderGoodsOtherDetailTableAdapter
            {
                get
                {
                    return INSTANCE_ORDER_GOODS_OTHER_DETAIL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ORDER_GOODS_OTHER_DETAIL_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrderGoodsOtherDetailBindingSource
            {
                get
                {
                    return INSTANCE_ORDER_GOODS_OTHER_DETAIL_BINDING_SOURCE;
                }
            }


            // Захиалгын хуудасны эмийн үлдэгдэл
            private static readonly JournalDataSetTableAdapters.OrderBalTableAdapter INSTANCE_ORDER_BAL_TABLE_ADAPTER = new JournalDataSetTableAdapters.OrderBalTableAdapter();
            public static JournalDataSetTableAdapters.OrderBalTableAdapter OrderBalTableAdapter
            {
                get
                {
                    return INSTANCE_ORDER_BAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ORDER_BAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrderBalBindingSource
            {
                get
                {
                    return INSTANCE_ORDER_BAL_BINDING_SOURCE;
                }
            }


            // Эмчлүүлэгч
            private static readonly JournalDataSetTableAdapters.PatientOtherTableAdapter INSTANCE_PATIENT_OTHER_TABLE_ADAPTER = new JournalDataSetTableAdapters.PatientOtherTableAdapter();
            public static JournalDataSetTableAdapters.PatientOtherTableAdapter PatientOtherTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientOtherBindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_OTHER_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_OTHER_1_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientOther1BindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_OTHER_1_BINDING_SOURCE;
                }
            }


            // Агуулах
            private static readonly JournalDataSetTableAdapters.WarehouseOtherTableAdapter INSTANCE_WAREHOUSE_OTHER_TABLE_ADAPTER = new JournalDataSetTableAdapters.WarehouseOtherTableAdapter();
            public static JournalDataSetTableAdapters.WarehouseOtherTableAdapter WarehouseOtherTableAdapter
            {
                get
                {
                    return INSTANCE_WAREHOUSE_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_WAREHOUSE_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource WarehouseOtherBindingSource
            {
                get
                {
                    return INSTANCE_WAREHOUSE_OTHER_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_WAREHOUSE_OTHER_1_BINDING_SOURCE = new BindingSource();
            public static BindingSource WarehouseOther1BindingSource
            {
                get
                {
                    return INSTANCE_WAREHOUSE_OTHER_1_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_WAREHOUSE_OTHER_2_BINDING_SOURCE = new BindingSource();
            public static BindingSource WarehouseOther2BindingSource
            {
                get
                {
                    return INSTANCE_WAREHOUSE_OTHER_2_BINDING_SOURCE;
                }
            }


            // Тасаг
            private static readonly JournalDataSetTableAdapters.WardOtherTableAdapter INSTANCE_WARD_OTHER_TABLE_ADAPTER = new JournalDataSetTableAdapters.WardOtherTableAdapter();
            public static JournalDataSetTableAdapters.WardOtherTableAdapter WardOtherTableAdapter
            {
                get
                {
                    return INSTANCE_WARD_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_WARD_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource WardOtherBindingSource
            {
                get
                {
                    return INSTANCE_WARD_OTHER_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_WARD_OTHER_1_BINDING_SOURCE = new BindingSource();
            public static BindingSource WardOther1BindingSource
            {
                get
                {
                    return INSTANCE_WARD_OTHER_1_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_WARD_OTHER_2_BINDING_SOURCE = new BindingSource();
            public static BindingSource WardOther2BindingSource
            {
                get
                {
                    return INSTANCE_WARD_OTHER_2_BINDING_SOURCE;
                }
            }


            // Тооллогын журнал
            private static readonly JournalDataSetTableAdapters.CensusJournalTableAdapter INSTANCE_CENSUS_JOURNAL_TABLE_ADAPTER = new JournalDataSetTableAdapters.CensusJournalTableAdapter();
            public static JournalDataSetTableAdapters.CensusJournalTableAdapter CensusJournalTableAdapter
            {
                get
                {
                    return INSTANCE_CENSUS_JOURNAL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_CENSUS_JOURNAL_BINDING_SOURCE = new BindingSource();
            public static BindingSource CensusJournalBindingSource
            {
                get
                {
                    return INSTANCE_CENSUS_JOURNAL_BINDING_SOURCE;
                }
            }


            // Тооллогын журналын гүйлгээ
            private static readonly JournalDataSetTableAdapters.CensusDetailTableAdapter INSTANCE_CENSUS_DETAIL_TABLE_ADAPTER = new JournalDataSetTableAdapters.CensusDetailTableAdapter();
            public static JournalDataSetTableAdapters.CensusDetailTableAdapter CensusDetailTableAdapter
            {
                get
                {
                    return INSTANCE_CENSUS_DETAIL_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_CENSUS_DETAIL_BINDING_SOURCE = new BindingSource();
            public static BindingSource CensusDetailBindingSource
            {
                get
                {
                    return INSTANCE_CENSUS_DETAIL_BINDING_SOURCE;
                }
            }


            // Тендерийн мэдээллийг авах
            private static readonly JournalDataSetTableAdapters.TenderOtherTableAdapter INSTANCE_TENDER_OTHER_TABLE_ADAPTER = new JournalDataSetTableAdapters.TenderOtherTableAdapter();
            public static JournalDataSetTableAdapters.TenderOtherTableAdapter TenderOtherTableAdapter
            {
                get
                {
                    return INSTANCE_TENDER_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_TENDER_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource TenderOtherBindingSource
            {
                get
                {
                    return INSTANCE_TENDER_OTHER_BINDING_SOURCE;
                }
            }


            // Эм зүйчид ирсэн эмийн түүвэр мэдээллийг авах
            private static readonly JournalDataSetTableAdapters.DrugJournalSumTableAdapter INSTANCE_DRUG_JOURNAL_SUM_TABLE_ADAPTER = new JournalDataSetTableAdapters.DrugJournalSumTableAdapter();
            public static JournalDataSetTableAdapters.DrugJournalSumTableAdapter DrugJournalSumTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_JOURNAL_SUM_TABLE_ADAPTER;
                }
            }

            // Эм зүйчид ирсэн эмийн түүвэр мэдээллийг авах
            private static readonly JournalDataSetTableAdapters.DrugDetailSumTableAdapter INSTANCE_DRUG_DETAIL_SUM_TABLE_ADAPTER = new JournalDataSetTableAdapters.DrugDetailSumTableAdapter();
            public static JournalDataSetTableAdapters.DrugDetailSumTableAdapter DrugDetailSumTableAdapter
            {
                get
                {
                    return INSTANCE_DRUG_DETAIL_SUM_TABLE_ADAPTER;
                }
            }


            // Хэрэглэгч 
            private static readonly JournalDataSetTableAdapters.SystemUserBegBalTableAdapter INSTANCE_SYSTEM_USER_BEG_BAL_TABLE_ADAPTER = new JournalDataSetTableAdapters.SystemUserBegBalTableAdapter();
            public static JournalDataSetTableAdapters.SystemUserBegBalTableAdapter SystemUserBegBalTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_BEG_BAL_TABLE_ADAPTER;
                }
            }

            private static readonly JournalDataSetTableAdapters.SystemUserCensusTableAdapter INSTANCE_SYSTEM_USER_CENSUS_TABLE_ADAPTER = new JournalDataSetTableAdapters.SystemUserCensusTableAdapter();
            public static JournalDataSetTableAdapters.SystemUserCensusTableAdapter SystemUserCensusTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_CENSUS_TABLE_ADAPTER;
                }
            }

            private static readonly JournalDataSetTableAdapters.SystemUserCensusOtherTableAdapter INSTANCE_SYSTEM_USER_CENSUS_OTHER_TABLE_ADAPTER = new JournalDataSetTableAdapters.SystemUserCensusOtherTableAdapter();
            public static JournalDataSetTableAdapters.SystemUserCensusOtherTableAdapter SystemUserCensusOtherTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_CENSUS_OTHER_TABLE_ADAPTER;
                }
            }

            private static readonly JournalDataSetTableAdapters.SystemUserExpTableAdapter INSTANCE_SYSTEM_USER_EXP_TABLE_ADAPTER = new JournalDataSetTableAdapters.SystemUserExpTableAdapter();
            public static JournalDataSetTableAdapters.SystemUserExpTableAdapter SystemUserExpTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_EXP_TABLE_ADAPTER;
                }
            }
            private static readonly BindingSource INSTANCE_SYSTEM_USER_EXP_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUserExpBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_EXP_BINDING_SOURCE;
                }
            }

            private static readonly JournalDataSetTableAdapters.SystemUserExpOtherTableAdapter INSTANCE_SYSTEM_USER_EXP_OTHER_TABLE_ADAPTER = new JournalDataSetTableAdapters.SystemUserExpOtherTableAdapter();
            public static JournalDataSetTableAdapters.SystemUserExpOtherTableAdapter SystemUserExpOtherTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_EXP_OTHER_TABLE_ADAPTER;
                }
            }
            private static readonly BindingSource INSTANCE_SYSTEM_USER_EXP_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUserExpOtherBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_EXP_OTHER_BINDING_SOURCE;
                }
            }


            private static readonly JournalDataSetTableAdapters.SystemUserIncTableAdapter INSTANCE_SYSTEM_USER_INC_TABLE_ADAPTER = new JournalDataSetTableAdapters.SystemUserIncTableAdapter();
            public static JournalDataSetTableAdapters.SystemUserIncTableAdapter SystemUserIncTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_INC_TABLE_ADAPTER;
                }
            }
            private static readonly BindingSource INSTANCE_SYSTEM_USER_INC_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUserIncBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_INC_BINDING_SOURCE;
                }
            }

            private static readonly JournalDataSetTableAdapters.SystemUserOrderTableAdapter INSTANCE_SYSTEM_USER_ORDER_TABLE_ADAPTER = new JournalDataSetTableAdapters.SystemUserOrderTableAdapter();
            public static JournalDataSetTableAdapters.SystemUserOrderTableAdapter SystemUserOrderTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_ORDER_TABLE_ADAPTER;
                }
            }
            private static readonly BindingSource INSTANCE_SYSTEM_USER_ORDER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUserOrderBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_ORDER_BINDING_SOURCE;
                }
            }

            private static readonly JournalDataSetTableAdapters.SystemUserOrderOtherTableAdapter INSTANCE_SYSTEM_USER_ORDER_OTHER_TABLE_ADAPTER = new JournalDataSetTableAdapters.SystemUserOrderOtherTableAdapter();
            public static JournalDataSetTableAdapters.SystemUserOrderOtherTableAdapter SystemUserOrderOtherTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_ORDER_OTHER_TABLE_ADAPTER;
                }
            }
            private static readonly BindingSource INSTANCE_SYSTEM_USER_ORDER_OTHER_BINDING_SOURCE = new BindingSource();
            public static BindingSource SystemUserOrderOtherBindingSource
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_ORDER_OTHER_BINDING_SOURCE;
                }
            }

            private static readonly JournalDataSetTableAdapters.SystemUserJournalTableAdapter INSTANCE_SYSTEM_USER_JOURNAL_TABLE_ADAPTER = new JournalDataSetTableAdapters.SystemUserJournalTableAdapter();
            public static JournalDataSetTableAdapters.SystemUserJournalTableAdapter SystemUserJournalTableAdapter
            {
                get
                {
                    return INSTANCE_SYSTEM_USER_JOURNAL_TABLE_ADAPTER;
                }
            }

        #endregion

        // DataSet-ийг ачаалах функц.
        public static void initialize()
        {
            ((ISupportInitialize)(Instance)).BeginInit();
            

            Instance.DataSetName = "JournalDataSet";
            Instance.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;

            
            //QueriesTableAdapter.InitCommandCollection();


            // Эмийн ерөнхий мэдээлэл
            MedicineOtherTableAdapter.Initialize();
            ((ISupportInitialize)(MedicineOtherBindingSource)).BeginInit();
            MedicineOtherBindingSource.DataMember = "MedicineOther";
            MedicineOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineOtherBindingSource)).EndInit();


            // Эмийн Эхний үлдэгдэл
            BegBalTableAdapter.Initialize();
            ((ISupportInitialize)(BegBalBindingSource)).BeginInit();
            BegBalBindingSource.DataMember = "BegBal";
            BegBalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(BegBalBindingSource)).EndInit();


            // Орлогын журнал
            IncJournalTableAdapter.Initialize();
            ((ISupportInitialize)(IncJournalBindingSource)).BeginInit();
            IncJournalBindingSource.DataMember = "IncJournal";
            IncJournalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(IncJournalBindingSource)).EndInit();


            // Орлогын журналын эм
            IncDetailTableAdapter.Initialize();
            ((ISupportInitialize)(IncDetailBindingSource)).BeginInit();
            IncDetailBindingSource.DataMember = "IncJournal_IncDetail";
            IncDetailBindingSource.DataSource = IncJournalBindingSource;
            ((ISupportInitialize)(IncDetailBindingSource)).EndInit();


            // Зарлагын журнал
            ExpJournalTableAdapter.Initialize();
            ((ISupportInitialize)(ExpJournalBindingSource)).BeginInit();
            ExpJournalBindingSource.DataMember = "ExpJournal";
            ExpJournalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(ExpJournalBindingSource)).EndInit();


            // Зарлагын журналын эм
            ExpDetailTableAdapter.Initialize();
            ((ISupportInitialize)(ExpDetailBindingSource)).BeginInit();
            ExpDetailBindingSource.DataMember = "ExpJournal_ExpDetail";
            ExpDetailBindingSource.DataSource = ExpJournalBindingSource;
            ((ISupportInitialize)(ExpDetailBindingSource)).EndInit();


            // Эмийн үлдэгдэл
            MedicineBalTableAdapter.Initialize();
            ((ISupportInitialize)(MedicineBalBindingSource)).BeginInit();
            MedicineBalBindingSource.DataMember = "MedicineBal";
            MedicineBalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MedicineBalBindingSource)).EndInit();



            // Хөдөлгөөний журнал
            MotJournalTableAdapter.Initialize();
            ((ISupportInitialize)(MotJournalBindingSource)).BeginInit();
            MotJournalBindingSource.DataMember = "MotJournal";
            MotJournalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(MotJournalBindingSource)).EndInit();


            // Хөдөлгөөний журналын эм
            MotDetailTableAdapter.Initialize();
            ((ISupportInitialize)(MotDetailBindingSource)).BeginInit();
            MotDetailBindingSource.DataMember = "MotJournal_MotDetail";
            MotDetailBindingSource.DataSource = MotJournalBindingSource;
            ((ISupportInitialize)(MotDetailBindingSource)).EndInit();


            // Үлдэгдэл
            BalTableAdapter.Initialize();
            ((ISupportInitialize)(BalBindingSource)).BeginInit();
            BalBindingSource.DataMember = "Bal";
            BalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(BalBindingSource)).EndInit();



            // Эмчлүүлэгчид олгох эм
            PatientMedicineTableAdapter.Initialize();
            ((ISupportInitialize)(PatientMedicineBindingSource)).BeginInit();
            PatientMedicineBindingSource.DataMember = "PatientMedicine";
            PatientMedicineBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientMedicineBindingSource)).EndInit();

            ((ISupportInitialize)(PatientMedicineOtherBindingSource)).BeginInit();
            PatientMedicineOtherBindingSource.DataMember = "PatientMedicine";
            PatientMedicineOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientMedicineOtherBindingSource)).EndInit();


            // Захиалгын хуудас
            OrderGoodsTableAdapter.Initialize();
            ((ISupportInitialize)(OrderGoodsBindingSource)).BeginInit();
            OrderGoodsBindingSource.DataMember = "OrderGoods";
            OrderGoodsBindingSource.DataSource = Instance;
            ((ISupportInitialize)(OrderGoodsBindingSource)).EndInit();


            // Захиалгын хуудасны эм
            OrderGoodsDetailTableAdapter.Initialize();
            ((ISupportInitialize)(OrderGoodsDetailBindingSource)).BeginInit();
            OrderGoodsDetailBindingSource.DataMember = "OrderGoods_OrderGoodsDetail";
            OrderGoodsDetailBindingSource.DataSource = OrderGoodsBindingSource;
            ((ISupportInitialize)(OrderGoodsDetailBindingSource)).EndInit();

            // Захиалгын хуудас батлах
            OrderStageTableAdapter.Initialize();
            ((ISupportInitialize)(OrderStageBindingSource)).BeginInit();
            OrderStageBindingSource.DataMember = "OrderStage";
            OrderStageBindingSource.DataSource = Instance;
            ((ISupportInitialize)(OrderStageBindingSource)).EndInit();

            // Захиалгын хуудас татах
            OrderGoodsOtherTableAdapter.Initialize();
            ((ISupportInitialize)(OrderGoodsOtherBindingSource)).BeginInit();
            OrderGoodsOtherBindingSource.DataMember = "OrderGoodsOther";
            OrderGoodsOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(OrderGoodsOtherBindingSource)).EndInit();


            // Захиалгын хуудасны эм татах
            OrderGoodsOtherDetailTableAdapter.Initialize();
            ((ISupportInitialize)(OrderGoodsOtherDetailBindingSource)).BeginInit();
            OrderGoodsOtherDetailBindingSource.DataMember = "OrderGoodsOther_OrderGoodsOtherDetail";
            OrderGoodsOtherDetailBindingSource.DataSource = OrderGoodsOtherBindingSource;
            ((ISupportInitialize)(OrderGoodsOtherDetailBindingSource)).EndInit();


            // Захиалгын хуудасны эм татах
            OrderBalTableAdapter.Initialize();
            ((ISupportInitialize)(OrderBalBindingSource)).BeginInit();
            OrderBalBindingSource.DataMember = "OrderBal";
            OrderBalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(OrderBalBindingSource)).EndInit();


            // Эмчлүүлэгч
            PatientOtherTableAdapter.Initialize();
            ((ISupportInitialize)(PatientOtherBindingSource)).BeginInit();
            PatientOtherBindingSource.DataMember = "PatientOther";
            PatientOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientOtherBindingSource)).EndInit();

            ((ISupportInitialize)(PatientOther1BindingSource)).BeginInit();
            PatientOther1BindingSource.DataMember = "PatientOther";
            PatientOther1BindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientOther1BindingSource)).EndInit();


            // Агуулах
            WarehouseOtherTableAdapter.Initialize();
            ((ISupportInitialize)(WarehouseOtherBindingSource)).BeginInit();
            WarehouseOtherBindingSource.DataMember = "WarehouseOther";
            WarehouseOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(WarehouseOtherBindingSource)).EndInit();
            ((ISupportInitialize)(WarehouseOther1BindingSource)).BeginInit();
            WarehouseOther1BindingSource.DataMember = "WarehouseOther";
            WarehouseOther1BindingSource.DataSource = Instance;
            ((ISupportInitialize)(WarehouseOther1BindingSource)).EndInit();
            ((ISupportInitialize)(WarehouseOther2BindingSource)).BeginInit();
            WarehouseOther2BindingSource.DataMember = "WarehouseOther";
            WarehouseOther2BindingSource.DataSource = Instance;
            ((ISupportInitialize)(WarehouseOther2BindingSource)).EndInit();


            // Тасаг
            WardOtherTableAdapter.Initialize();
            ((ISupportInitialize)(WardOtherBindingSource)).BeginInit();
            WardOtherBindingSource.DataMember = "WardOther";
            WardOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(WardOtherBindingSource)).EndInit();
            ((ISupportInitialize)(WardOther1BindingSource)).BeginInit();
            WardOther1BindingSource.DataMember = "WardOther";
            WardOther1BindingSource.DataSource = Instance;
            ((ISupportInitialize)(WardOther1BindingSource)).EndInit();
            ((ISupportInitialize)(WardOther2BindingSource)).BeginInit();
            WardOther2BindingSource.DataMember = "WardOther";
            WardOther2BindingSource.DataSource = Instance;
            ((ISupportInitialize)(WardOther2BindingSource)).EndInit();


            // Тооллогын журнал
            CensusJournalTableAdapter.Initialize();
            ((ISupportInitialize)(CensusJournalBindingSource)).BeginInit();
            CensusJournalBindingSource.DataMember = "CensusJournal";
            CensusJournalBindingSource.DataSource = Instance;
            ((ISupportInitialize)(CensusJournalBindingSource)).EndInit();


            // Тооллогын журналын гүйлгээ
            CensusDetailTableAdapter.Initialize();
            ((ISupportInitialize)(CensusDetailBindingSource)).BeginInit();
            CensusDetailBindingSource.DataMember = "CensusJournal_CensusDetail";
            CensusDetailBindingSource.DataSource = CensusJournalBindingSource;
            ((ISupportInitialize)(CensusDetailBindingSource)).EndInit();


            // Тооллогын журнал
            TenderOtherTableAdapter.Initialize();
            ((ISupportInitialize)(TenderOtherBindingSource)).BeginInit();
            TenderOtherBindingSource.DataMember = "TenderOther";
            TenderOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(TenderOtherBindingSource)).EndInit();


            // Эм зүйчид ирсэн эмийн түүвэр мэдээллийг авах
            DrugDetailSumTableAdapter.Initialize();


            // Эм зүйчид ирсэн эмийн түүвэр мэдээллийг авах
            DrugJournalSumTableAdapter.Initialize();


            // Хэрэглэгч эхний үлдэгдэл
            SystemUserBegBalTableAdapter.Initialize();
            SystemUserCensusTableAdapter.Initialize();
            SystemUserCensusOtherTableAdapter.Initialize();

            SystemUserExpTableAdapter.Initialize();
            ((ISupportInitialize)(SystemUserExpBindingSource)).BeginInit();
            SystemUserExpBindingSource.DataMember = "SystemUserExp";
            SystemUserExpBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUserExpBindingSource)).EndInit();

            SystemUserExpOtherTableAdapter.Initialize();
            ((ISupportInitialize)(SystemUserExpOtherBindingSource)).BeginInit();
            SystemUserExpOtherBindingSource.DataMember = "SystemUserExpOther";
            SystemUserExpOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUserExpOtherBindingSource)).EndInit();

            SystemUserIncTableAdapter.Initialize();
            ((ISupportInitialize)(SystemUserIncBindingSource)).BeginInit();
            SystemUserIncBindingSource.DataMember = "SystemUserInc";
            SystemUserIncBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUserIncBindingSource)).EndInit();

            SystemUserOrderTableAdapter.Initialize();
            ((ISupportInitialize)(SystemUserOrderBindingSource)).BeginInit();
            SystemUserOrderBindingSource.DataMember = "SystemUserOrder";
            SystemUserOrderBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUserOrderBindingSource)).EndInit();

            SystemUserOrderOtherTableAdapter.Initialize();
            ((ISupportInitialize)(SystemUserOrderOtherBindingSource)).BeginInit();
            SystemUserOrderOtherBindingSource.DataMember = "SystemUserOrderOther";
            SystemUserOrderOtherBindingSource.DataSource = Instance;
            ((ISupportInitialize)(SystemUserOrderOtherBindingSource)).EndInit();

            SystemUserJournalTableAdapter.Initialize();

            ((ISupportInitialize)(Instance)).EndInit();
        }

        public static void changeConnectionString()
        {
            MedicineOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            BegBalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            IncJournalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            IncDetailTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            ExpJournalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            ExpDetailTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MotJournalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MotDetailTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            MedicineBalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            BalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PatientMedicineTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            OrderGoodsTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            OrderGoodsDetailTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            OrderGoodsOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            OrderGoodsOtherDetailTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            OrderStageTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            OrderBalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PatientOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            WarehouseOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            WardOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            CensusJournalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            CensusDetailTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            TenderOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugDetailSumTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            DrugJournalSumTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;

            SystemUserBegBalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserCensusTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserCensusOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserExpTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserExpOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserIncTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserOrderTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserOrderOtherTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            SystemUserJournalTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
        }

    }
}


namespace Pharmacy2016.gui.core.journal.ds.JournalDataSetTableAdapters {
    partial class MedicineOtherTableAdapter
    {
    }
    
    
    public partial class IncJournalTableAdapter {
    }
}

namespace Pharmacy2016.gui.core.journal.ds.JournalDataSetTableAdapters
{
    
    
    public partial class MedicineOtherTableAdapter {
    }
}
