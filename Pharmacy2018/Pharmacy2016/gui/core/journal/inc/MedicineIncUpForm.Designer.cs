﻿namespace Pharmacy2016.gui.core.journal.inc
{
    partial class MedicineIncUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicineIncUpForm));
            this.labelControlWarehouse = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditProducer = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditIncDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditMedicine = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spinEditCount = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditPrice = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditSumPrice = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEditDocNumber = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.gridLookUpEditIncType = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.treeListLookUpEditWarehouse = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridLookUpEditUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupType = new DevExpress.XtraEditors.RadioGroup();
            this.treeListLookUpEditWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.simpleButtonNewDoc = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlQuantity = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditConMedicine = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlConMedicine = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditTender = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEditMultiSelection = new DevExpress.XtraEditors.CheckEdit();
            this.popupContainerEditMedicineBal = new DevExpress.XtraEditors.PopupContainerEdit();
            this.gridControlMed = new DevExpress.XtraGrid.GridControl();
            this.gridViewMed = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLatinName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSerial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnValidatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDupCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPrice = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.popupContainerControlMed = new DevExpress.XtraEditors.PopupContainerControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.serial = new DevExpress.XtraEditors.TextEdit();
            this.dateEditValidDate = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditProducer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIncDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIncDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSumPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDocNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIncType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditConMedicine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMultiSelection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditMedicineBal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMed)).BeginInit();
            this.popupContainerControlMed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditValidDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditValidDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControlWarehouse
            // 
            this.labelControlWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlWarehouse.Location = new System.Drawing.Point(100, 147);
            this.labelControlWarehouse.Name = "labelControlWarehouse";
            this.labelControlWarehouse.Size = new System.Drawing.Size(61, 13);
            this.labelControlWarehouse.TabIndex = 0;
            this.labelControlWarehouse.Text = "Эмийн сан*:";
            // 
            // gridLookUpEditProducer
            // 
            this.gridLookUpEditProducer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditProducer.Location = new System.Drawing.Point(167, 196);
            this.gridLookUpEditProducer.Name = "gridLookUpEditProducer";
            this.gridLookUpEditProducer.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditProducer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditProducer.Properties.DisplayMember = "name";
            this.gridLookUpEditProducer.Properties.NullText = "";
            this.gridLookUpEditProducer.Properties.ValueMember = "id";
            this.gridLookUpEditProducer.Properties.View = this.gridView1;
            this.gridLookUpEditProducer.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditProducer_Properties_ButtonClick);
            this.gridLookUpEditProducer.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditProducer.TabIndex = 8;
            this.gridLookUpEditProducer.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditProducer_KeyUp);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn22,
            this.gridColumn23});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Нэр";
            this.gridColumn22.CustomizationCaption = "Бэлтгэн нийлүүлэгчийн нэр";
            this.gridColumn22.FieldName = "name";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 0;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Улс";
            this.gridColumn23.CustomizationCaption = "Улсын нэр";
            this.gridColumn23.FieldName = "countryName";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(53, 199);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(108, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Бэлтгэн нийлүүлэгч*:";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(120, 67);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(41, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Огноо*:";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(95, 350);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(66, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Тоо хэмжээ*:";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(89, 376);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(72, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Нэгжийн үнэ*:";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl6.Location = new System.Drawing.Point(132, 402);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(29, 13);
            this.labelControl6.TabIndex = 8;
            this.labelControl6.Text = "Дүн*:";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(316, 486);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 21;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(397, 486);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 22;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // dateEditIncDate
            // 
            this.dateEditIncDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditIncDate.EditValue = null;
            this.dateEditIncDate.Location = new System.Drawing.Point(167, 64);
            this.dateEditIncDate.Name = "dateEditIncDate";
            this.dateEditIncDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditIncDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditIncDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditIncDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditIncDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditIncDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditIncDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditIncDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditIncDate.TabIndex = 2;
            this.dateEditIncDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditIncDate_KeyUp);
            // 
            // textEditID
            // 
            this.textEditID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditID.Location = new System.Drawing.Point(167, 38);
            this.textEditID.Name = "textEditID";
            this.textEditID.Properties.ReadOnly = true;
            this.textEditID.Size = new System.Drawing.Size(200, 20);
            this.textEditID.TabIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl7.Location = new System.Drawing.Point(62, 41);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(99, 13);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "Баримтын дугаар*:";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.memoEditDescription.Location = new System.Drawing.Point(167, 249);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(305, 40);
            this.memoEditDescription.TabIndex = 10;
            this.memoEditDescription.KeyUp += new System.Windows.Forms.KeyEventHandler(this.memoEditDescription_KeyUp);
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(112, 251);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(49, 13);
            this.labelControl8.TabIndex = 18;
            this.labelControl8.Text = "Тайлбар :";
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl9.Location = new System.Drawing.Point(100, 324);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(61, 13);
            this.labelControl9.TabIndex = 21;
            this.labelControl9.Text = "Эмийн нэр*:";
            // 
            // gridLookUpEditMedicine
            // 
            this.gridLookUpEditMedicine.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditMedicine.EditValue = "";
            this.gridLookUpEditMedicine.Location = new System.Drawing.Point(167, 321);
            this.gridLookUpEditMedicine.Name = "gridLookUpEditMedicine";
            this.gridLookUpEditMedicine.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditMedicine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMedicine.Properties.DisplayMember = "name";
            this.gridLookUpEditMedicine.Properties.NullText = "";
            this.gridLookUpEditMedicine.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditMedicine.Properties.ValueMember = "id";
            this.gridLookUpEditMedicine.Properties.View = this.gridView3;
            this.gridLookUpEditMedicine.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditMedicine.TabIndex = 12;
            this.gridLookUpEditMedicine.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditMedicine_ButtonClick);
            this.gridLookUpEditMedicine.EditValueChanged += new System.EventHandler(this.gridLookUpEditMedicine_EditValueChanged);
            this.gridLookUpEditMedicine.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditMedicine_KeyUp);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn1,
            this.gridColumn21,
            this.gridColumn29,
            this.gridColumn30});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Нэр";
            this.gridColumn15.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn15.FieldName = "name";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "ОУ нэр";
            this.gridColumn16.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn16.FieldName = "latinName";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 1;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Ангилал";
            this.gridColumn17.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn17.FieldName = "medicineTypeName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 2;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Тун хэмжээ";
            this.gridColumn18.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn18.FieldName = "unitType";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 5;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Үнэ";
            this.gridColumn19.CustomizationCaption = "Үнэ";
            this.gridColumn19.DisplayFormat.FormatString = "n2";
            this.gridColumn19.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn19.FieldName = "price";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 7;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Бар код";
            this.gridColumn20.CustomizationCaption = "Бар код";
            this.gridColumn20.FieldName = "barcode";
            this.gridColumn20.MinWidth = 75;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 9;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Хэлбэр";
            this.gridColumn1.CustomizationCaption = "Хэлбэр";
            this.gridColumn1.FieldName = "shape";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 4;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Хүчинтэй хугацаа";
            this.gridColumn21.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn21.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.gridColumn21.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn21.FieldName = "validDate";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 8;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Хэмжих нэгж";
            this.gridColumn29.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn29.FieldName = "quantity";
            this.gridColumn29.MinWidth = 75;
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 6;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Сериал";
            this.gridColumn30.CustomizationCaption = "Эмийн сериал";
            this.gridColumn30.FieldName = "serial";
            this.gridColumn30.MinWidth = 75;
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 3;
            // 
            // spinEditCount
            // 
            this.spinEditCount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditCount.EnterMoveNextControl = true;
            this.spinEditCount.Location = new System.Drawing.Point(167, 347);
            this.spinEditCount.Name = "spinEditCount";
            this.spinEditCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditCount.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditCount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.EditFormat.FormatString = "n2";
            this.spinEditCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.Mask.EditMask = "n2";
            this.spinEditCount.Size = new System.Drawing.Size(100, 20);
            this.spinEditCount.TabIndex = 13;
            this.spinEditCount.EditValueChanged += new System.EventHandler(this.spinEditCount_ValueChanged);
            // 
            // spinEditPrice
            // 
            this.spinEditPrice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditPrice.EnterMoveNextControl = true;
            this.spinEditPrice.Location = new System.Drawing.Point(167, 373);
            this.spinEditPrice.Name = "spinEditPrice";
            this.spinEditPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditPrice.Properties.DisplayFormat.FormatString = "n3";
            this.spinEditPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.EditFormat.FormatString = "n3";
            this.spinEditPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.Mask.EditMask = "n3";
            this.spinEditPrice.Size = new System.Drawing.Size(100, 20);
            this.spinEditPrice.TabIndex = 14;
            this.spinEditPrice.EditValueChanged += new System.EventHandler(this.spinEditCount_ValueChanged);
            // 
            // spinEditSumPrice
            // 
            this.spinEditSumPrice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditSumPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditSumPrice.Location = new System.Drawing.Point(167, 399);
            this.spinEditSumPrice.Name = "spinEditSumPrice";
            this.spinEditSumPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditSumPrice.Properties.DisplayFormat.FormatString = "n3";
            this.spinEditSumPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditSumPrice.Properties.EditFormat.FormatString = "n3";
            this.spinEditSumPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditSumPrice.Properties.Mask.EditMask = "n3";
            this.spinEditSumPrice.Properties.ReadOnly = true;
            this.spinEditSumPrice.Size = new System.Drawing.Size(100, 20);
            this.spinEditSumPrice.TabIndex = 25;
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl10.Location = new System.Drawing.Point(65, 226);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(96, 13);
            this.labelControl10.TabIndex = 28;
            this.labelControl10.Text = "Дагалдах баримт :";
            // 
            // textEditDocNumber
            // 
            this.textEditDocNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditDocNumber.EnterMoveNextControl = true;
            this.textEditDocNumber.Location = new System.Drawing.Point(167, 223);
            this.textEditDocNumber.Name = "textEditDocNumber";
            this.textEditDocNumber.Size = new System.Drawing.Size(200, 20);
            this.textEditDocNumber.TabIndex = 9;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 486);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 23;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // gridLookUpEditIncType
            // 
            this.gridLookUpEditIncType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditIncType.EditValue = "";
            this.gridLookUpEditIncType.Location = new System.Drawing.Point(167, 170);
            this.gridLookUpEditIncType.Name = "gridLookUpEditIncType";
            this.gridLookUpEditIncType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditIncType.Properties.DisplayMember = "name";
            this.gridLookUpEditIncType.Properties.NullText = "";
            this.gridLookUpEditIncType.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditIncType.Properties.ValueMember = "id";
            this.gridLookUpEditIncType.Properties.View = this.gridView4;
            this.gridLookUpEditIncType.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditIncType.TabIndex = 6;
            this.gridLookUpEditIncType.EditValueChanged += new System.EventHandler(this.gridLookUpEditIncType_EditValueChanged);
            this.gridLookUpEditIncType.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditIncType_KeyUp);
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn27,
            this.gridColumn28});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Нэр";
            this.gridColumn27.CustomizationCaption = "Орлогын төрлийн нэр";
            this.gridColumn27.FieldName = "name";
            this.gridColumn27.MinWidth = 75;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 0;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Тайлбар";
            this.gridColumn28.CustomizationCaption = "Орлогын төрлийн тайлбар";
            this.gridColumn28.FieldName = "description";
            this.gridColumn28.MinWidth = 75;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 1;
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl11.Location = new System.Drawing.Point(73, 173);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(88, 13);
            this.labelControl11.TabIndex = 31;
            this.labelControl11.Text = "Орлогын төрөл*:";
            // 
            // treeListLookUpEditWarehouse
            // 
            this.treeListLookUpEditWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWarehouse.EditValue = "0";
            this.treeListLookUpEditWarehouse.Location = new System.Drawing.Point(167, 144);
            this.treeListLookUpEditWarehouse.Name = "treeListLookUpEditWarehouse";
            this.treeListLookUpEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWarehouse.Properties.DisplayMember = "name";
            this.treeListLookUpEditWarehouse.Properties.NullText = "";
            this.treeListLookUpEditWarehouse.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEditWarehouse.Properties.ValueMember = "id";
            this.treeListLookUpEditWarehouse.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWarehouse.TabIndex = 5;
            this.treeListLookUpEditWarehouse.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWarehouse_KeyUp);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1,
            this.treeListColumn3});
            this.treeListLookUpEdit1TreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListLookUpEdit1TreeList.KeyFieldName = "id";
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(151, 180);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsBehavior.PopulateServiceColumns = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.ParentFieldName = "pid";
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Нэр";
            this.treeListColumn1.CustomizationCaption = "Агуулахын нэр";
            this.treeListColumn1.FieldName = "name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraTreeList.FilterPopupMode.List;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 400;
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "Дугаар";
            this.treeListColumn3.CustomizationCaption = "Агуулахын дугаар";
            this.treeListColumn3.FieldName = "number";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 1;
            this.treeListColumn3.Width = 270;
            // 
            // gridLookUpEditUser
            // 
            this.gridLookUpEditUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditUser.EditValue = "";
            this.gridLookUpEditUser.Location = new System.Drawing.Point(167, 90);
            this.gridLookUpEditUser.Name = "gridLookUpEditUser";
            this.gridLookUpEditUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditUser.Properties.NullText = "";
            this.gridLookUpEditUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditUser.Properties.ValueMember = "id";
            this.gridLookUpEditUser.Properties.View = this.gridView2;
            this.gridLookUpEditUser.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditUser.TabIndex = 3;
            this.gridLookUpEditUser.EditValueChanged += new System.EventHandler(this.gridLookUpEditUser_EditValueChanged);
            this.gridLookUpEditUser.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditUser_KeyUp);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Дугаар";
            this.gridColumn2.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn2.FieldName = "id";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Овог";
            this.gridColumn8.CustomizationCaption = "Овог";
            this.gridColumn8.FieldName = "lastName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Нэр";
            this.gridColumn9.CustomizationCaption = "Нэр";
            this.gridColumn9.FieldName = "firstName";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Эрх";
            this.gridColumn11.CustomizationCaption = "Эрх";
            this.gridColumn11.FieldName = "roleName";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Тасаг";
            this.gridColumn12.CustomizationCaption = "Тасаг";
            this.gridColumn12.FieldName = "wardName";
            this.gridColumn12.MinWidth = 75;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl13.Location = new System.Drawing.Point(120, 93);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(41, 13);
            this.labelControl13.TabIndex = 35;
            this.labelControl13.Text = "Нярав*:";
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl14.Location = new System.Drawing.Point(62, 120);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(99, 13);
            this.labelControl14.TabIndex = 37;
            this.labelControl14.Text = "Агуулахын төрөл*:";
            // 
            // radioGroupType
            // 
            this.radioGroupType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupType.EnterMoveNextControl = true;
            this.radioGroupType.Location = new System.Drawing.Point(167, 116);
            this.radioGroupType.Name = "radioGroupType";
            this.radioGroupType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Эмийн сан"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Тасаг")});
            this.radioGroupType.Size = new System.Drawing.Size(200, 22);
            this.radioGroupType.TabIndex = 4;
            this.radioGroupType.SelectedIndexChanged += new System.EventHandler(this.radioGroupType_SelectedIndexChanged);
            // 
            // treeListLookUpEditWard
            // 
            this.treeListLookUpEditWard.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWard.Location = new System.Drawing.Point(167, 12);
            this.treeListLookUpEditWard.Name = "treeListLookUpEditWard";
            this.treeListLookUpEditWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditWard.Properties.NullText = "";
            this.treeListLookUpEditWard.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditWard.Properties.ValueMember = "id";
            this.treeListLookUpEditWard.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWard.TabIndex = 5;
            this.treeListLookUpEditWard.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWard_KeyUp);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(85, 222);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Нэр";
            this.treeListColumn2.FieldName = "name";
            this.treeListColumn2.MinWidth = 32;
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // simpleButtonNewDoc
            // 
            this.simpleButtonNewDoc.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.simpleButtonNewDoc.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonNewDoc.Image")));
            this.simpleButtonNewDoc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButtonNewDoc.Location = new System.Drawing.Point(373, 36);
            this.simpleButtonNewDoc.Name = "simpleButtonNewDoc";
            this.simpleButtonNewDoc.Size = new System.Drawing.Size(70, 23);
            this.simpleButtonNewDoc.TabIndex = 24;
            this.simpleButtonNewDoc.Text = "Шинэ";
            this.simpleButtonNewDoc.ToolTip = "Шинэ баримт үүсгэх";
            this.simpleButtonNewDoc.Click += new System.EventHandler(this.simpleButtonNewDoc_Click);
            // 
            // labelControlQuantity
            // 
            this.labelControlQuantity.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlQuantity.Location = new System.Drawing.Point(273, 350);
            this.labelControlQuantity.Name = "labelControlQuantity";
            this.labelControlQuantity.Size = new System.Drawing.Size(8, 13);
            this.labelControlQuantity.TabIndex = 38;
            this.labelControlQuantity.Text = "ш";
            // 
            // gridLookUpEditConMedicine
            // 
            this.gridLookUpEditConMedicine.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditConMedicine.EditValue = "";
            this.gridLookUpEditConMedicine.Location = new System.Drawing.Point(167, 295);
            this.gridLookUpEditConMedicine.Name = "gridLookUpEditConMedicine";
            this.gridLookUpEditConMedicine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditConMedicine.Properties.DisplayMember = "name";
            this.gridLookUpEditConMedicine.Properties.NullText = "";
            this.gridLookUpEditConMedicine.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditConMedicine.Properties.ValueMember = "id";
            this.gridLookUpEditConMedicine.Properties.View = this.gridView5;
            this.gridLookUpEditConMedicine.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditConMedicine.TabIndex = 11;
            this.gridLookUpEditConMedicine.EditValueChanged += new System.EventHandler(this.gridLookUpEditConMedicine_EditValueChanged);
            this.gridLookUpEditConMedicine.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditConMedicine_KeyUp);
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn26});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ShowAutoFilterRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Нэр";
            this.gridColumn3.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn3.FieldName = "name";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ОУ нэр";
            this.gridColumn4.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn4.FieldName = "latinName";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Ангилал";
            this.gridColumn5.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn5.FieldName = "medicineTypeName";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тун хэмжээ";
            this.gridColumn6.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn6.FieldName = "unitType";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Үнэ";
            this.gridColumn7.CustomizationCaption = "Үнэ";
            this.gridColumn7.DisplayFormat.FormatString = "n2";
            this.gridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn7.FieldName = "price";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Хэлбэр";
            this.gridColumn13.CustomizationCaption = "Хэлбэр";
            this.gridColumn13.FieldName = "shape";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 3;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Хүчинтэй хугацаа";
            this.gridColumn14.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn14.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.gridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn14.FieldName = "validDate";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 7;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Хэмжих нэгж";
            this.gridColumn26.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn26.FieldName = "quantity";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 5;
            // 
            // labelControlConMedicine
            // 
            this.labelControlConMedicine.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlConMedicine.Location = new System.Drawing.Point(82, 298);
            this.labelControlConMedicine.Name = "labelControlConMedicine";
            this.labelControlConMedicine.Size = new System.Drawing.Size(79, 13);
            this.labelControlConMedicine.TabIndex = 40;
            this.labelControlConMedicine.Text = "Тендерийн эм*:";
            // 
            // gridLookUpEditTender
            // 
            this.gridLookUpEditTender.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditTender.Location = new System.Drawing.Point(287, 399);
            this.gridLookUpEditTender.Name = "gridLookUpEditTender";
            this.gridLookUpEditTender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditTender.Properties.DisplayMember = "tenderNumber";
            this.gridLookUpEditTender.Properties.NullText = "";
            this.gridLookUpEditTender.Properties.ValueMember = "tenderID";
            this.gridLookUpEditTender.Properties.View = this.gridView6;
            this.gridLookUpEditTender.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditProducer_Properties_ButtonClick);
            this.gridLookUpEditTender.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditTender.TabIndex = 7;
            this.gridLookUpEditTender.EditValueChanged += new System.EventHandler(this.gridLookUpEditTender_EditValueChanged);
            this.gridLookUpEditTender.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditTender_KeyUp);
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn25,
            this.gridColumn24,
            this.gridColumn10});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ShowAutoFilterRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Дугаар";
            this.gridColumn25.CustomizationCaption = "Тендерийн дугаар";
            this.gridColumn25.FieldName = "tenderNumber";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 0;
            this.gridColumn25.Width = 133;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Огноо";
            this.gridColumn24.CustomizationCaption = "Тендерийн огноо";
            this.gridColumn24.FieldName = "date";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 1;
            this.gridColumn24.Width = 147;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Бэлтгэн нийлүүлэгч";
            this.gridColumn10.CustomizationCaption = "Бэлтгэн нийлүүлэгч";
            this.gridColumn10.FieldName = "supplierName";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            this.gridColumn10.Width = 651;
            // 
            // checkEditMultiSelection
            // 
            this.checkEditMultiSelection.Location = new System.Drawing.Point(373, 321);
            this.checkEditMultiSelection.Name = "checkEditMultiSelection";
            this.checkEditMultiSelection.Properties.Caption = "олон сонгох";
            this.checkEditMultiSelection.Size = new System.Drawing.Size(88, 19);
            this.checkEditMultiSelection.TabIndex = 52;
            this.checkEditMultiSelection.CheckedChanged += new System.EventHandler(this.checkEditMultiSelection_CheckedChanged);
            // 
            // popupContainerEditMedicineBal
            // 
            this.popupContainerEditMedicineBal.Location = new System.Drawing.Point(287, 347);
            this.popupContainerEditMedicineBal.Name = "popupContainerEditMedicineBal";
            this.popupContainerEditMedicineBal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditMedicineBal.Properties.PopupFormMinSize = new System.Drawing.Size(500, 300);
            this.popupContainerEditMedicineBal.Size = new System.Drawing.Size(199, 20);
            this.popupContainerEditMedicineBal.TabIndex = 54;
            this.popupContainerEditMedicineBal.Visible = false;
            this.popupContainerEditMedicineBal.CloseUp += new DevExpress.XtraEditors.Controls.CloseUpEventHandler(this.popupContainerEditMedicineBal_CloseUp);
            // 
            // gridControlMed
            // 
            this.gridControlMed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMed.Location = new System.Drawing.Point(0, 0);
            this.gridControlMed.MainView = this.gridViewMed;
            this.gridControlMed.Name = "gridControlMed";
            this.gridControlMed.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemSpinEditPrice});
            this.gridControlMed.Size = new System.Drawing.Size(262, 78);
            this.gridControlMed.TabIndex = 56;
            this.gridControlMed.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMed});
            // 
            // gridViewMed
            // 
            this.gridViewMed.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumName,
            this.gridColumnLatinName,
            this.gridColumnType,
            this.gridColumnUnit,
            this.gridColumnSerial,
            this.gridColumnValidatedDate,
            this.gridColumnDupCount,
            this.gridColumnPrice});
            this.gridViewMed.GridControl = this.gridControlMed;
            this.gridViewMed.Name = "gridViewMed";
            this.gridViewMed.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewMed.OptionsSelection.MultiSelect = true;
            this.gridViewMed.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridViewMed.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumName
            // 
            this.gridColumName.Caption = "Нэр";
            this.gridColumName.FieldName = "name";
            this.gridColumName.MinWidth = 100;
            this.gridColumName.Name = "gridColumName";
            this.gridColumName.OptionsColumn.ReadOnly = true;
            this.gridColumName.Visible = true;
            this.gridColumName.VisibleIndex = 1;
            this.gridColumName.Width = 100;
            // 
            // gridColumnLatinName
            // 
            this.gridColumnLatinName.Caption = "О.У нэр";
            this.gridColumnLatinName.FieldName = "latinName";
            this.gridColumnLatinName.MinWidth = 100;
            this.gridColumnLatinName.Name = "gridColumnLatinName";
            this.gridColumnLatinName.OptionsColumn.ReadOnly = true;
            this.gridColumnLatinName.Visible = true;
            this.gridColumnLatinName.VisibleIndex = 6;
            this.gridColumnLatinName.Width = 100;
            // 
            // gridColumnType
            // 
            this.gridColumnType.Caption = "Ангилал";
            this.gridColumnType.FieldName = "medicineTypeName";
            this.gridColumnType.MinWidth = 100;
            this.gridColumnType.Name = "gridColumnType";
            this.gridColumnType.OptionsColumn.ReadOnly = true;
            this.gridColumnType.Visible = true;
            this.gridColumnType.VisibleIndex = 7;
            this.gridColumnType.Width = 100;
            // 
            // gridColumnUnit
            // 
            this.gridColumnUnit.Caption = "Тун хэмжээ";
            this.gridColumnUnit.FieldName = "unitType";
            this.gridColumnUnit.MinWidth = 100;
            this.gridColumnUnit.Name = "gridColumnUnit";
            this.gridColumnUnit.OptionsColumn.ReadOnly = true;
            this.gridColumnUnit.Visible = true;
            this.gridColumnUnit.VisibleIndex = 8;
            this.gridColumnUnit.Width = 100;
            // 
            // gridColumnSerial
            // 
            this.gridColumnSerial.Caption = "Сериал";
            this.gridColumnSerial.FieldName = "serial";
            this.gridColumnSerial.MinWidth = 75;
            this.gridColumnSerial.Name = "gridColumnSerial";
            this.gridColumnSerial.Visible = true;
            this.gridColumnSerial.VisibleIndex = 4;
            // 
            // gridColumnValidatedDate
            // 
            this.gridColumnValidatedDate.Caption = "Хүчинтэй хугацаа";
            this.gridColumnValidatedDate.FieldName = "validDate";
            this.gridColumnValidatedDate.MinWidth = 75;
            this.gridColumnValidatedDate.Name = "gridColumnValidatedDate";
            this.gridColumnValidatedDate.Visible = true;
            this.gridColumnValidatedDate.VisibleIndex = 5;
            this.gridColumnValidatedDate.Width = 100;
            // 
            // gridColumnDupCount
            // 
            this.gridColumnDupCount.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnDupCount.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnDupCount.Caption = "Тоо";
            this.gridColumnDupCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnDupCount.FieldName = "count";
            this.gridColumnDupCount.MinWidth = 75;
            this.gridColumnDupCount.Name = "gridColumnDupCount";
            this.gridColumnDupCount.Visible = true;
            this.gridColumnDupCount.VisibleIndex = 2;
            this.gridColumnDupCount.Width = 100;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n0";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n0";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n0";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnPrice
            // 
            this.gridColumnPrice.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnPrice.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnPrice.Caption = "Үнэ";
            this.gridColumnPrice.ColumnEdit = this.repositoryItemSpinEditPrice;
            this.gridColumnPrice.FieldName = "price";
            this.gridColumnPrice.MinWidth = 75;
            this.gridColumnPrice.Name = "gridColumnPrice";
            this.gridColumnPrice.Visible = true;
            this.gridColumnPrice.VisibleIndex = 3;
            this.gridColumnPrice.Width = 100;
            // 
            // repositoryItemSpinEditPrice
            // 
            this.repositoryItemSpinEditPrice.AutoHeight = false;
            this.repositoryItemSpinEditPrice.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPrice.DisplayFormat.FormatString = "c2";
            this.repositoryItemSpinEditPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditPrice.EditFormat.FormatString = "c2";
            this.repositoryItemSpinEditPrice.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditPrice.Mask.EditMask = "c2";
            this.repositoryItemSpinEditPrice.Name = "repositoryItemSpinEditPrice";
            // 
            // popupContainerControlMed
            // 
            this.popupContainerControlMed.Controls.Add(this.gridControlMed);
            this.popupContainerControlMed.Location = new System.Drawing.Point(12, 12);
            this.popupContainerControlMed.Name = "popupContainerControlMed";
            this.popupContainerControlMed.Size = new System.Drawing.Size(262, 78);
            this.popupContainerControlMed.TabIndex = 57;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(114, 428);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(47, 13);
            this.labelControl1.TabIndex = 58;
            this.labelControl1.Text = "Сериал*:";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl12.Location = new System.Drawing.Point(62, 456);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(100, 13);
            this.labelControl12.TabIndex = 59;
            this.labelControl12.Text = "Хүчинтэй хугацаа*:";
            // 
            // serial
            // 
            this.serial.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.serial.EnterMoveNextControl = true;
            this.serial.Location = new System.Drawing.Point(167, 425);
            this.serial.Name = "serial";
            this.serial.Size = new System.Drawing.Size(200, 20);
            this.serial.TabIndex = 60;
            // 
            // dateEditValidDate
            // 
            this.dateEditValidDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditValidDate.EditValue = null;
            this.dateEditValidDate.Location = new System.Drawing.Point(167, 453);
            this.dateEditValidDate.Name = "dateEditValidDate";
            this.dateEditValidDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditValidDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditValidDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditValidDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditValidDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditValidDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditValidDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditValidDate.Properties.Mask.IgnoreMaskBlank = false;
            this.dateEditValidDate.Properties.MaxLength = 15;
            this.dateEditValidDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditValidDate.TabIndex = 61;
            // 
            // MedicineIncUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(484, 521);
            this.Controls.Add(this.dateEditValidDate);
            this.Controls.Add(this.serial);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.popupContainerControlMed);
            this.Controls.Add(this.popupContainerEditMedicineBal);
            this.Controls.Add(this.checkEditMultiSelection);
            this.Controls.Add(this.gridLookUpEditTender);
            this.Controls.Add(this.gridLookUpEditConMedicine);
            this.Controls.Add(this.labelControlConMedicine);
            this.Controls.Add(this.labelControlQuantity);
            this.Controls.Add(this.simpleButtonNewDoc);
            this.Controls.Add(this.treeListLookUpEditWard);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.radioGroupType);
            this.Controls.Add(this.gridLookUpEditUser);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.treeListLookUpEditWarehouse);
            this.Controls.Add(this.gridLookUpEditIncType);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.textEditDocNumber);
            this.Controls.Add(this.spinEditSumPrice);
            this.Controls.Add(this.spinEditPrice);
            this.Controls.Add(this.spinEditCount);
            this.Controls.Add(this.gridLookUpEditMedicine);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.memoEditDescription);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.textEditID);
            this.Controls.Add(this.dateEditIncDate);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonSave);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.gridLookUpEditProducer);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControlWarehouse);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MedicineIncUpForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Орлого";
            this.Shown += new System.EventHandler(this.MedicineIncInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditProducer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIncDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditIncDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSumPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDocNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIncType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditConMedicine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMultiSelection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditMedicineBal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMed)).EndInit();
            this.popupContainerControlMed.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.serial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditValidDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditValidDate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControlWarehouse;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditProducer;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.DateEdit dateEditIncDate;
        private DevExpress.XtraEditors.TextEdit textEditID;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMedicine;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.SpinEdit spinEditCount;
        private DevExpress.XtraEditors.SpinEdit spinEditPrice;
        private DevExpress.XtraEditors.SpinEdit spinEditSumPrice;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit textEditDocNumber;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditIncType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWarehouse;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.RadioGroup radioGroupType;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWard;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonNewDoc;
        private DevExpress.XtraEditors.LabelControl labelControlQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditConMedicine;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.LabelControl labelControlConMedicine;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditTender;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraEditors.CheckEdit checkEditMultiSelection;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditMedicineBal;
        private DevExpress.XtraGrid.GridControl gridControlMed;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMed;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLatinName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnUnit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSerial;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnValidatedDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDupCount;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlMed;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPrice;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit serial;
        private DevExpress.XtraEditors.DateEdit dateEditValidDate;
    }
}