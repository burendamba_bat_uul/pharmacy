﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.report.journal.inc;
using Pharmacy2016.gui.core.journal.exp;
using Pharmacy2016.report.journal.inc;
using Pharmacy2016.gui.report.ds;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid;



namespace Pharmacy2016.gui.core.journal.inc
{
    /*
     * Орлогын журнал харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class MedicineIncForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static MedicineIncForm INSTANCE = new MedicineIncForm();

        public static MedicineIncForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private readonly int FORM_ID = 2001;

        private bool isInit = false;

        public DataRowView currView;

        private BaseEdit inplaceEditor;

        private System.IO.Stream layout_inc = null;
        private System.IO.Stream layout_detail = null;

        private MedicineIncForm()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            this.MdiParent = MainForm.Instance;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            initLayout();
            RoleUtil.addForm(FORM_ID, this);

            gridControlInc.DataSource = JournalDataSet.IncJournalBindingSource;

            dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
            dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

            DateTime date = DateTime.Now;
            dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1);
            dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }

        private void initLayout()
        {
            layout_inc = new System.IO.MemoryStream();
            gridViewInc.SaveLayoutToStream(layout_inc);
            layout_inc.Seek(0, System.IO.SeekOrigin.Begin);

            layout_detail = new System.IO.MemoryStream();
            gridViewDetail.SaveLayoutToStream(layout_detail);
            layout_detail.Seek(0, System.IO.SeekOrigin.Begin);
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm()
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }

                if (Visible)
                {
                    Select();
                }
                else
                {
                    radioGroupJournal.SelectedIndex = 0;
                    checkRole();

                    JournalDataSet.Instance.IncJournal.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    JournalDataSet.Instance.IncJournal.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                    JournalDataSet.Instance.IncDetail.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    JournalDataSet.Instance.IncDetail.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                    reload();
                    Show();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                clearData();
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void checkRole()
        {
            bool[] roles = UserUtil.getWindowRole(FORM_ID);

            gridControlInc.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
            gridControlInc.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
            gridControlInc.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
            //gridControlInc.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = roles[1];
            simpleButtonDownload.Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
            dropDownButtonPrint.Visible = roles[4];
            simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;

            checkLayout();
        }

        private void checkLayout()
        {
            string layout = UserUtil.getLayout("inc");
            if (layout != null)
            {
                gridViewInc.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewInc.RestoreLayoutFromStream(layout_inc);
                layout_inc.Seek(0, System.IO.SeekOrigin.Begin);
            }

            layout = UserUtil.getLayout("incDetail");
            if (layout != null)
            {
                gridViewDetail.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewDetail.RestoreLayoutFromStream(layout_detail);
                layout_detail.Seek(0, System.IO.SeekOrigin.Begin);
            }
        }

        private void MedicineIncForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            clearData();
            Hide();
        }

        private void clearData()
        {
            JournalDataSet.Instance.IncDetail.Clear();
            JournalDataSet.Instance.IncJournal.Clear();
        }

        #endregion

        #region Формын event

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            try
            {
                reload();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void simpleButtonDownload_Click(object sender, EventArgs e)
        {
            downloadOrder();
        }

        private void simpleButtonLayout_Click(object sender, EventArgs e)
        {
            saveLayout();
        }

        private void gridControlInc_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    add();
                }
                else if (String.Equals(e.Button.Tag, "delete") && gridViewInc.RowCount != 0 && gridViewInc.GetFocusedDataRow() != null)
                {
                    try
                    {
                        DataRow row_del = gridViewInc.GetFocusedDataRow();
                        JournalDataSet.Instance.ExpJournal.Clear();
                        JournalDataSet.ExpJournalTableAdapter.Fill(JournalDataSet.Instance.ExpJournal, HospitalUtil.getHospitalId(),
                                Convert.ToString(row_del["supplierID"]), dateEditStart.Text, dateEditEnd.Text);
                        DataRow[] rows = JournalDataSet.Instance.ExpJournal.Select("incJournalID = '" + row_del["id"] + "'");
                        if (rows.Length <= 0)
                        {
                            delete();
                        }
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
                else if (String.Equals(e.Button.Tag, "update") && gridViewInc.RowCount != 0 && gridViewInc.GetFocusedDataRow() != null)
                {
                    DataRow row_update = gridViewInc.GetFocusedDataRow();
                    JournalDataSet.Instance.ExpJournal.Clear();
                    JournalDataSet.ExpJournalTableAdapter.Fill(JournalDataSet.Instance.ExpJournal, HospitalUtil.getHospitalId(),
                                Convert.ToString(row_update["supplierID"]), dateEditStart.Text, dateEditEnd.Text);
                    DataRow[] rows_up = JournalDataSet.Instance.ExpJournal.Select("incJournalID = '" + row_update["id"] + "'");
                    if (rows_up.Length <= 0)
                    {
                        update();
                    }
                }
                //if (String.Equals(e.Button.Tag, "order"))
                //{
                //    downloadOrder();
                //}
            }
        }

        private void gridViewDetail_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo info = e.Info as DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo;
            info.GroupText = string.Format("{0} (Тоо хэмжээ = {1} Нийт үнэ = {2})", info.GroupValueText, gridViewDetail.GetGroupSummaryDisplayText(e.RowHandle, gridViewDetail.GroupSummary[0] as DevExpress.XtraGrid.GridGroupSummaryItem), gridViewDetail.GetGroupSummaryDisplayText(e.RowHandle, gridViewDetail.GroupSummary[1] as DevExpress.XtraGrid.GridGroupSummaryItem));
        }

        private void gridViewIncDetail_RowClick(object sender, RowClickEventArgs e)
        {
            GridView detailView = sender as GridView;
            gridViewInc.FocusedRowHandle = detailView.SourceRowHandle;
        }

        private void radioGroupJournal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioGroupJournal.SelectedIndex == 0)
            {
                JournalDataSet.IncDetailBindingSource.DataSource = JournalDataSet.IncJournalBindingSource;
                JournalDataSet.IncDetailBindingSource.DataMember = "IncJournal_IncDetail";

                gridControlInc.DataSource = JournalDataSet.IncJournalBindingSource;
                gridControlInc.MainView = gridViewInc;

                gridControlInc.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                gridControlInc.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                gridControlInc.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;
                checkRole();
            }
            else if (radioGroupJournal.SelectedIndex == 1)
            {
                JournalDataSet.IncDetailBindingSource.DataSource = JournalDataSet.Instance;
                JournalDataSet.IncDetailBindingSource.DataMember = "IncDetail";

                gridControlInc.DataSource = JournalDataSet.IncDetailBindingSource;
                gridControlInc.MainView = gridViewDetail;

                gridControlInc.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                gridControlInc.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                gridControlInc.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
            }
        }

        private void gridViewDetail_DoubleClick(object sender, EventArgs e)
        {
            DataRowView view = (DataRowView)gridViewDetail.GetFocusedRow();
            if (view == null)
                return;


            radioGroupJournal.SelectedIndex = 0;
            for (int i = 0; i < gridViewInc.RowCount; i++)
            {
                DataRow row = gridViewInc.GetDataRow(i);
                if (view["incJournalID"].ToString().Equals(row["id"].ToString()))
                {
                    gridViewInc.SelectRow(i);
                    if (!gridViewInc.GetMasterRowExpanded(i))
                        gridViewInc.SetMasterRowExpanded(i, true);
                }
            }
        }

        private void gridViewInc_ShownEditor(object sender, EventArgs e)
        {
            inplaceEditor = ((GridView)sender).ActiveEditor;
            inplaceEditor.DoubleClick += new EventHandler(ActiveEditor_DoubleClick);
        }

        private void gridViewIncDetail_ShownEditor(object sender, EventArgs e)
        {
            inplaceEditor = ((GridView)sender).ActiveEditor;
            inplaceEditor.DoubleClick += new EventHandler(ActiveEditor_DoubleClick);
        }

        private void ActiveEditor_DoubleClick(object sender, System.EventArgs e)
        {
            BaseEdit editor = (BaseEdit)sender;
            GridControl grid = (GridControl)editor.Parent;
            Point pt = grid.PointToClient(Control.MousePosition);
            GridView view = (GridView)grid.FocusedView;
            DoRowDoubleClick(view, pt);
        }

        #endregion

        #region Формын функц

        private bool check()
        {
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();

            if (dateEditStart.Text == "")
            {
                errorText = "Эхлэх огноог оруулна уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }
            if (dateEditEnd.Text == "")
            {
                errorText = "Дуусах огноог оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditEnd.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    dateEditEnd.Focus();
                }
            }
            if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
            {
                errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }
            if (!isRight)
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        private void reload()
        {
            if (check())
            {
                string userid = "";
                // 2016-10-17 Орлогын журналь хэрэглэгч зөвхөн өөрийн мэдээлэл хардаг болгож засав.
                if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    JournalDataSet.IncJournalTableAdapter.Fill(JournalDataSet.Instance.IncJournal, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                    JournalDataSet.IncDetailTableAdapter.Fill(JournalDataSet.Instance.IncDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                    ProgramUtil.closeWaitDialog();
                    userid = " and e.[userID] = '" + UserUtil.getUserId() + "'";
                }
                else if (UserUtil.getUserRoleId() == RoleUtil.ACCOUNTANT_ID)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    JournalDataSet.IncJournalTableAdapter.Fill(JournalDataSet.Instance.IncJournal, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    JournalDataSet.IncDetailTableAdapter.Fill(JournalDataSet.Instance.IncDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    ProgramUtil.closeWaitDialog();
                }
                else
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    JournalDataSet.IncJournalTableAdapter.Fill(JournalDataSet.Instance.IncJournal, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    JournalDataSet.IncDetailTableAdapter.Fill(JournalDataSet.Instance.IncDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    ProgramUtil.closeWaitDialog();
                }
                SqlConnection myConnection = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                string oString = "SELECT ex.[incJournalID], format(sum(ex.[price] * ex.[count]),'N','en-us') as amount " +
                                 "FROM [IncJournal] as e " +
                                 "inner join " +
                                 "[IncDetail] as ex on e.[id] = ex.[incJournalID] " +
                                 "where e.[hospitalID] = '" + HospitalUtil.getHospitalId() + "' " +
                                 "and e.[date] between '" + dateEditStart.Text + "' and '" + dateEditEnd.Text + "' " + userid +
                                 "group by ex.[incJournalID]";
                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                myConnection.Open();
                Hashtable amountlist = new Hashtable();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        amountlist.Add(oReader["incJournalID"].ToString(), oReader["amount"].ToString());
                    }
                    myConnection.Close();
                }
                for (var i = 0; i < gridViewInc.RowCount; i++)
                {
                    gridViewInc.SetRowCellValue(i, bandedGridColumn29, amountlist[gridViewInc.GetRowCellValue(i, gridViewInc.Columns["id"]).ToString()]);
                }
            }
        }

        private void add()
        {
            MedicineIncUpForm.Instance.showForm(1, Instance);
        }

        private void delete()
        {
            bool isDeleteDoc = false;
            GridView detail = null;

            if (gridViewInc.IsFocusedView)
            {
                isDeleteDoc = true;
            }
            else
            {
                detail = gridViewInc.GetDetailView(gridViewInc.FocusedRowHandle, 0) as GridView;
                if (detail.IsFocusedView)
                {
                    isDeleteDoc = detail.RowCount == 1;
                }
            }
            string date = gridViewInc.GetRowCellValue(gridViewInc.FocusedRowHandle, gridViewInc.Columns["date"]).ToString();
            if (ProgramUtil.checkLockMedicine(date))
            {
                if (isDeleteDoc)
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн орлого устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        gridViewInc.DeleteSelectedRows();
                        JournalDataSet.IncJournalBindingSource.EndEdit();


                        JournalDataSet.IncJournalTableAdapter.Update(JournalDataSet.Instance.IncJournal);
                        ProgramUtil.closeWaitDialog();
                    }
                }
                else
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн гүйлгээ устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        detail.DeleteSelectedRows();
                        JournalDataSet.IncDetailBindingSource.EndEdit();
                        JournalDataSet.IncDetailTableAdapter.Update(JournalDataSet.Instance.IncDetail);
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }
            else
            {
                XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
            }

        }

        private void update()
        {
            if (!gridViewInc.GetMasterRowExpanded(gridViewInc.FocusedRowHandle))
                gridViewInc.SetMasterRowExpanded(gridViewInc.FocusedRowHandle, true);

            MedicineIncUpForm.Instance.showForm(2, Instance);
        }

        private void downloadOrder()
        {
            OrderDownForm.Instance.showForm(2, Instance);
        }

        public DataRowView getSelectedRow()
        {
            //gridViewInc.FocusedRowHandle = gridViewIncDetail.SourceRowHandle;
            GridView detail = gridViewInc.GetDetailView(gridViewInc.FocusedRowHandle, 0) as GridView;
            string id = detail.GetFocusedDataRow()["id"].ToString();
            for (int i = 0; i < JournalDataSet.IncDetailBindingSource.Count; i++)
            {
                DataRowView view = (DataRowView)JournalDataSet.IncDetailBindingSource[i];
                if (view["id"].ToString().Equals(id))
                    return view;
            }
            return null;
        }

        public DateTime getStartDate()
        {
            return dateEditStart.DateTime;
        }

        public DateTime getEndDate()
        {
            return dateEditEnd.DateTime;
        }

        private void saveLayout()
        {
            try
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                System.IO.Stream str = new System.IO.MemoryStream();
                System.IO.StreamReader reader = null;
                if (radioGroupJournal.SelectedIndex == 0)
                {
                    gridViewInc.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("inc", reader.ReadToEnd());
                }
                else
                {
                    gridViewDetail.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("incDetail", reader.ReadToEnd());
                }
                reader.Close();
                str.Close();

                ProgramUtil.closeWaitDialog();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void DoRowDoubleClick(GridView view, Point pt)
        {
            GridHitInfo info = view.CalcHitInfo(pt);
            if (info.InRow || info.InRowCell)
            {
                if (view.GetFocusedDataRow() != null)
                    update();
            }
        }

        #endregion

        #region Хэвлэх, хөрвүүлэх функц

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItemPrint.Caption;
            dropDownButtonPrint.Image = barButtonItemPrint.Glyph;

            DataRow row = gridViewInc.GetFocusedDataRow();
            if (gridViewInc.GetFocusedDataRow() != null)
                IncDocReport.Instance.initAndShow(row["id"].ToString());
            else
                XtraMessageBox.Show("Мөр сонгоно уу");
            //IncReport.Instance.initAndShow(1, gridViewInc.ActiveFilterString);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItem1.Caption;
            dropDownButtonPrint.Image = barButtonItem1.Glyph;

            IncReport.Instance.initAndShow(1, gridViewInc.ActiveFilterString);
        }

        private void barButtonItemBegBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItemIncDetail.Caption;
            dropDownButtonPrint.Image = barButtonItemIncDetail.Glyph;

            if (gridViewInc.GetFocusedRow() != null)
                IncReport.Instance.initAndShow(2, "id = '" + gridViewInc.GetFocusedDataRow()["id"].ToString() + "'");
            else
                XtraMessageBox.Show("Мөр сонгоно уу");
        }

        private void barButtonItemCustomize_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ProgramUtil.closeAlertDialog();
            if (gridViewInc.RowCount == 0 && gridViewInc.GetFocusedDataRow() == null)
            {
                string errorText = "Хэвлэх мөр алга байна";
                string text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                return;
            }
            List<string[]> colList = new List<string[]>();
            DataRow row = gridViewInc.GetFocusedDataRow();
            foreach (DevExpress.XtraGrid.Columns.GridColumn cols in gridViewInc.Columns.Where(x => (x.FieldName != "warehouseDesc" && x.FieldName != "incTypeName" && x.FieldName != "id" && x.FieldName != "roleName" && x.FieldName != "userID")))
            {
                colList.Add(new string[] { cols.FieldName, cols.CustomizationCaption });
            }
            foreach (DevExpress.XtraGrid.Columns.GridColumn cols in gridViewIncDetail.Columns.Where(x => (x.FieldName != "count" && x.FieldName != "price" && x.FieldName != "sumPrice")))
            {
                colList.Add(new string[] { cols.FieldName, cols.Caption });
            }

            ReportUpForm.Instance.showForm(colList, MedicineIncForm.Instance, row, dateEditStart.DateTime, dateEditEnd.DateTime, "");
            dropDownButtonPrint.Text = barButtonItemCustomize.Caption;
            dropDownButtonPrint.Image = barButtonItemCustomize.Glyph;
        }

        private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItemConvert.Caption;
            dropDownButtonPrint.Image = barButtonItemConvert.Glyph;

            if (radioGroupJournal.SelectedIndex == 0)
            {
                ProgramUtil.convertGrid(gridViewInc, "Орлогын журнал");
            }
            else
            {
                ProgramUtil.convertGrid(gridViewDetail, "Орлогын гүйлгээ");
            }
        }

        #endregion

        private void gridControlInc_Click(object sender, EventArgs e)
        {

        }
        public class Medicine
        {
            public string medicineID { get; set; }
            public string hospitalID { get; set; }
            public string name { get; set; }
            public string latinName { get; set; }
            public string unitID { get; set; }
            public string shapeID { get; set; }
            public string quantityID { get; set; }
            public decimal package { get; set; }
            public decimal price { get; set; }
            public string validDate { get; set; }
            public string medicineSeries { get; set; }
            public string medicineTypeID { get; set; }
            public string medicineTypeInterID { get; set; }
            public string barcode { get; set; }
            public string serial { get; set; }
            public string parmaStandart { get; set; }
            public string description { get; set; }
            public string producerCountryID { get; set; }
            public string producerID { get; set; }
            public string registerID { get; set; }
            public string btkusID { get; set; }
            public string packageID { get; set; }

        }

        public class Main
        {
            public string medicineID { get; set; }
            public int count { get; set; }
        }

        public class MainMedicine
        {
            public string medicineID { get; set; }
            public decimal price { get; set; }
        }


        //Эхний үлдэгдлийг цэгцлэх
        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
            myConn.Open();
            int serCnt = 200000;
            string sqlmain1 = "SELECT medicineID, count(*) as count from ( " +
                              "SELECT [medicineID], [price] " +
                              "FROM [dbo].[BegBal] group by medicineID, price " +
                              "Union " +
                              "SELECT [medicineID], [price] " +
                              "FROM [dbo].[IncDetail] group by medicineID, price " +
                              "Union " +
                              "SELECT [medicineID], [price] " +
                              "FROM [dbo].[ExpDetail] group by medicineID, price ) as aa group by [medicineID] having count(*) > 1 order by medicineID";
            SqlCommand oCmdmain1 = new SqlCommand(sqlmain1, myConn);
            List<Main> main1 = new List<Main>();
            using (SqlDataReader oReaderMain1 = oCmdmain1.ExecuteReader())
            {
                while (oReaderMain1.Read())
                {
                    if (oReaderMain1.HasRows)
                    {
                        Main mainone1 = new Main();
                        mainone1.medicineID = oReaderMain1["medicineID"].ToString();
                        mainone1.count = Convert.ToInt32(oReaderMain1["count"]);
                        main1.Add(mainone1);
                    }
                }
            }
            foreach (Main one1 in main1)
            {
                serCnt = serCnt + 1;
                string serial = "Siticom_" + serCnt;
                List<decimal> allprice1 = new List<decimal>();
                Medicine med1 = new Medicine();
                string sqlInc1 = "SELECT [price] FROM [dbo].[BegBal] where medicineID = '" + one1.medicineID + "' group by price " +
                                 "Union " +
                                 "SELECT [price] FROM [dbo].[IncDetail] where medicineID = '" + one1.medicineID + "' group by price " +
                                 "Union " +
                                 "SELECT [price] FROM [dbo].[ExpDetail] where medicineID = '" + one1.medicineID + "' group by price ";
                SqlCommand oCmdInc1 = new SqlCommand(sqlInc1, myConn);
                using (SqlDataReader oReaderInc1 = oCmdInc1.ExecuteReader())
                {
                    while (oReaderInc1.Read())
                    {
                        if (oReaderInc1.HasRows)
                        {
                            allprice1.Add(Convert.ToDecimal(oReaderInc1["price"]));
                        }
                    }
                }

                string sqlMed1 = "SELECT id, hospitalID, name, latinName, unitID, shapeID, quantityID, package, price, validDate, " +
                                "medicineSeries, medicineTypeID, medicineTypeInterID, barcode, serial, parmaStandart, description, producerCountryID, " +
                                "producerID, registerID, btkusID, packageID FROM [dbo].[Medicine] where id = '" + one1.medicineID + "'";
                SqlCommand oCmdMed1 = new SqlCommand(sqlMed1, myConn);
                using (SqlDataReader oReaderMed1 = oCmdMed1.ExecuteReader())
                {
                    while (oReaderMed1.Read())
                    {
                        if (oReaderMed1.HasRows)
                        {
                            med1.medicineID = oReaderMed1["id"].ToString();
                            med1.hospitalID = oReaderMed1["hospitalID"].ToString();
                            med1.name = oReaderMed1["name"].ToString();
                            med1.latinName = oReaderMed1["latinName"].ToString();
                            med1.unitID = oReaderMed1["unitID"].ToString(); ;
                            med1.shapeID = oReaderMed1["shapeID"].ToString();
                            med1.quantityID = oReaderMed1["quantityID"].ToString();
                            med1.package = Convert.ToDecimal(oReaderMed1["package"]);
                            med1.price = Convert.ToDecimal(oReaderMed1["price"]);
                            med1.validDate = oReaderMed1["validDate"].ToString();
                            med1.medicineSeries = oReaderMed1["medicineSeries"].ToString();
                            med1.medicineTypeID = oReaderMed1["medicineTypeID"].ToString();
                            med1.medicineTypeInterID = oReaderMed1["medicineTypeInterID"].ToString();
                            med1.barcode = oReaderMed1["barcode"].ToString();
                            med1.serial = oReaderMed1["serial"].ToString();
                            med1.parmaStandart = oReaderMed1["parmaStandart"].ToString();
                            med1.description = oReaderMed1["description"].ToString();
                            med1.producerCountryID = oReaderMed1["producerCountryID"].ToString();
                            med1.producerID = oReaderMed1["producerID"].ToString();
                            med1.registerID = oReaderMed1["registerID"].ToString();
                            med1.btkusID = oReaderMed1["btkusID"].ToString();
                            med1.packageID = oReaderMed1["packageID"].ToString();
                        }
                    }
                }
                if (!string.IsNullOrEmpty(med1.serial))
                {
                    serial = med1.serial;
                }
                bool priceNull1 = true;
                foreach (decimal price1 in allprice1)
                {
                    if (med1.price == 0 && priceNull1 == true)
                    {
                        string sqlAddFirst1 = "update [dbo].[Medicine] set price = '" + price1 + "', [serial] = '" + serial + "'  where id = '" + med1.medicineID + "'";
                        SqlCommand oCmdAddFirst1 = new SqlCommand(sqlAddFirst1, myConn);
                        using (SqlDataReader oReaderAddFirst1 = oCmdAddFirst1.ExecuteReader())
                        {
                            while (oReaderAddFirst1.Read())
                            {
                            }
                        }
                        priceNull1 = false;
                    }
                    else if (med1.price != price1)
                    {

                        string newID1 = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                        string sqlAddMed1 = "INSERT INTO [dbo].[Medicine] ([id] ,[hospitalID] ,[name], [latinName] ,[unitID] " +
                                          ",[shapeID] ,[quantityID] ,[package] ,[price] ,[validDate] ,[medicineSeries] " +
                                          ",[medicineTypeID] ,[medicineTypeInterID] ,[barcode] ,[serial] ,[parmaStandart] " +
                                          ",[description] ,[producerCountryID] ,[producerID] ,[registerID] ,[btkusID] ,[packageID]) " +
                                          "VALUES ('" + newID1 + "', '" + med1.hospitalID + "', N'" + med1.name + "', N'" + med1.latinName + "' " +
                                          ", '" + med1.unitID + "', '" + med1.shapeID + "', '" + med1.quantityID + "', '" + med1.package + "' " +
                                          ", '" + price1 + "', '" + med1.validDate + "', N'" + med1.medicineSeries + "', '" + med1.medicineTypeID + "' " +
                                          ", '" + med1.medicineTypeInterID + "', N'" + med1.barcode + "', N'" + serial + "', N'" + med1.parmaStandart + "' " +
                                          ", N'" + med1.description + "', '" + med1.producerCountryID + "', '" + med1.producerID + "', '" + med1.registerID + "' " +
                                          ", '" + med1.btkusID + "', '" + med1.packageID + "');";
                        SqlCommand oCmdAddMed1 = new SqlCommand(sqlAddMed1, myConn);
                        using (SqlDataReader oReaderAddMed1 = oCmdAddMed1.ExecuteReader())
                        {
                            while (oReaderAddMed1.Read())
                            {
                            }
                        }

                        string sqlSetBeg = "update BegBal set medicineID = '" + newID1 + "' where medicineID = '" + med1.medicineID + "' and price = '" + price1 + "';";
                        SqlCommand oCmdSetBeg = new SqlCommand(sqlSetBeg, myConn);
                        using (SqlDataReader oReaderSetBeg = oCmdSetBeg.ExecuteReader())
                        {
                            while (oReaderSetBeg.Read())
                            {
                            }
                        }

                        string sqlSetExp1 = "update ExpDetail set medicineID = '" + newID1 + "' where medicineID = '" + med1.medicineID + "' and price = '" + price1 + "';";
                        SqlCommand oCmdSetExp1 = new SqlCommand(sqlSetExp1, myConn);
                        using (SqlDataReader oReaderSetExp1 = oCmdSetExp1.ExecuteReader())
                        {
                            while (oReaderSetExp1.Read())
                            {
                            }
                        }

                        string sqlSetInc1 = "update IncDetail set medicineID = '" + newID1 + "' where medicineID = '" + med1.medicineID + "' and price = '" + price1 + "';";
                        SqlCommand oCmdSetInc1 = new SqlCommand(sqlSetInc1, myConn);
                        using (SqlDataReader oReaderSetInc1 = oCmdSetInc1.ExecuteReader())
                        {
                            while (oReaderSetInc1.Read())
                            {
                            }
                        }
                    }
                }         
            }
            myConn.Close();
            XtraMessageBox.Show("Duuslaa");


        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
            myConn.Open();
            string sqlmain1 = "SELECT [medicineID], [price] " +
                              "FROM [dbo].[BegBal] group by medicineID, price";
            SqlCommand oCmdmain1 = new SqlCommand(sqlmain1, myConn);
            List<MainMedicine> main1 = new List<MainMedicine>();
            using (SqlDataReader oReaderMain1 = oCmdmain1.ExecuteReader())
            {
                while (oReaderMain1.Read())
                {
                    if (oReaderMain1.HasRows)
                    {
                        MainMedicine mainone1 = new MainMedicine();
                        mainone1.medicineID = oReaderMain1["medicineID"].ToString();
                        mainone1.price = Convert.ToInt32(oReaderMain1["price"]);
                        main1.Add(mainone1);
                    }
                }
            }

            string sqlmain2 = "SELECT [medicineID], [price] " +
                             "FROM [dbo].[ExpDetail] group by medicineID, price";
            SqlCommand oCmdmain2 = new SqlCommand(sqlmain2, myConn);
            using (SqlDataReader oReaderMain2 = oCmdmain2.ExecuteReader())
            {
                while (oReaderMain2.Read())
                {
                    if (oReaderMain2.HasRows)
                    {
                        MainMedicine mainone2 = new MainMedicine();
                        mainone2.medicineID = oReaderMain2["medicineID"].ToString();
                        mainone2.price = Convert.ToInt32(oReaderMain2["price"]);
                        main1.Add(mainone2);
                    }
                }
            }

            string sqlmain3 = "SELECT [medicineID], [price] " +
                             "FROM [dbo].[IncDetail] group by medicineID, price";
            SqlCommand oCmdmain3 = new SqlCommand(sqlmain3, myConn);
            using (SqlDataReader oReaderMain3 = oCmdmain3.ExecuteReader())
            {
                while (oReaderMain3.Read())
                {
                    if (oReaderMain3.HasRows)
                    {
                        MainMedicine mainone3 = new MainMedicine();
                        mainone3.medicineID = oReaderMain3["medicineID"].ToString();
                        mainone3.price = Convert.ToInt32(oReaderMain3["price"]);
                        main1.Add(mainone3);
                    }
                }
            }

            foreach (MainMedicine one in main1)
            {
                string sqlAddFirst1 = "update [dbo].[Medicine] set price = '" + one.price + "'  where id = '" + one.medicineID + "'";
                SqlCommand oCmdAddFirst1 = new SqlCommand(sqlAddFirst1, myConn);
                using (SqlDataReader oReaderAddFirst1 = oCmdAddFirst1.ExecuteReader())
                {
                    while (oReaderAddFirst1.Read())
                    {
                    }
                }
            }
            myConn.Close();
            XtraMessageBox.Show("Duuslaa");
        }



    }
}