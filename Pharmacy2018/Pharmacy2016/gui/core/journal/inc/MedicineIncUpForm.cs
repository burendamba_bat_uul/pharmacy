﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.info.medicine;
using Pharmacy2016.gui.core.info.conf;
using System.Data.SqlClient;



namespace Pharmacy2016.gui.core.journal.inc
{
    /*
     * Эмийн орлого нэмэх, засах цонх.
     * 
     */

    public partial class MedicineIncUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static MedicineIncUpForm INSTANCE = new MedicineIncUpForm();

        public static MedicineIncUpForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private int actionType;

        private Double confirmCount;

        private DataRowView docView;

        private DataRowView currView;

        private int[] rowID;

        private MedicineIncUpForm()
        {

        }


        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            spinEditCount.Properties.MaxValue = Decimal.MaxValue - 1;
            spinEditPrice.Properties.MaxValue = Decimal.MaxValue - 1;

            dateEditIncDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditIncDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

            initError();
            initDataSource();
            initBinding();
            reload(false);
        }

        private void initDataSource()
        {
            //gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
            gridLookUpEditUser.Properties.DataSource = JournalDataSet.SystemUserIncBindingSource;
            treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
            treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
            gridLookUpEditIncType.Properties.DataSource = InformationDataSet.IncTypeOtherBindingSource;
            gridLookUpEditProducer.Properties.DataSource = UserDataSet.SupplierBindingSource;
            gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineOtherBindingSource;

            gridLookUpEditTender.Properties.DataSource = JournalDataSet.Instance.Tender;
            gridLookUpEditConMedicine.Properties.DataSource = JournalDataSet.Instance.TenderMedicine;
            gridControlMed.DataSource = JournalDataSet.MedicineOtherBindingSource;
            popupContainerControlMed.Controls.Add(gridControlMed);
            popupContainerEditMedicineBal.Properties.PopupControl = popupContainerControlMed;
        }

        private void initError()
        {
            gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditIncType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            dateEditIncDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            spinEditPrice.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            popupContainerEditMedicineBal.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            serial.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            dateEditValidDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm(int type, XtraForm form)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }

                //JournalDataSet.WarehouseOtherBindingSource.Filter = UserUtil.getFilterWarehouse();
                actionType = type;
                if (actionType == 1)
                {
                    insert();
                }
                else if (actionType == 2)
                {
                    update();
                }

                ShowDialog(form);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clearData();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void initBinding()
        {
            gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "userID", true));
            treeListLookUpEditWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "warehouseID", true));
            treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "wardID", true));
            radioGroupType.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "type", true));
            gridLookUpEditIncType.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "incTypeID", true));
            gridLookUpEditTender.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "tenderID", true));
            gridLookUpEditProducer.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "supplierID", true));
            textEditID.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "id", true));
            textEditDocNumber.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "docNumber", true));
            dateEditIncDate.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "date", true));
            memoEditDescription.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "description", true));

            //gridLookUpEditMedicine.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncDetailBindingSource, "medicineID", true));
        }

        private void insert()
        {
            //this.Text = "Орлого нэмэх цонх";
            string id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            JournalDataSet.Instance.IncJournal.idColumn.DefaultValue = id;
            JournalDataSet.Instance.IncDetail.incJournalIDColumn.DefaultValue = id;
            docView = (DataRowView)JournalDataSet.IncJournalBindingSource.AddNew();
            DateTime now = DateTime.Now;
            docView["id"] = id;
            docView["date"] = now;
            docView["incTypeID"] = 1;
            dateEditIncDate.DateTime = now;
            textEditID.Text = id;
            checkEditMultiSelection.Visible = true;
            simpleButtonNewDoc.Enabled = true;
            gridLookUpEditUser.Enabled = true;
            radioGroupType.Enabled = true;
            treeListLookUpEditWarehouse.Enabled = true;
            treeListLookUpEditWard.Enabled = true;
            gridLookUpEditConMedicine.Enabled = true;
            gridLookUpEditIncType.Enabled = true;
            gridLookUpEditUser.Enabled = true;
            treeListLookUpEditWarehouse.Enabled = true;
            treeListLookUpEditWard.Enabled = true;
            textEditDocNumber.Enabled = true;
            gridLookUpEditProducer.Enabled = true;
            textEditID.Enabled = true;
            dateEditIncDate.Enabled = true;
            gridLookUpEditUser.Enabled = true;
            gridLookUpEditTender.Enabled = true;
            confirmCount = -1;
            JournalDataSet.MedicineOtherBindingSource.Filter = "";
            addRow();
        }

        private void update()
        {
            //this.Text = "Орлого засах цонх";
            docView = (DataRowView)JournalDataSet.IncJournalBindingSource.Current;
            currView = MedicineIncForm.Instance.getSelectedRow();
            JournalDataSet.Instance.IncDetail.incJournalIDColumn.DefaultValue = docView["id"];
            gridLookUpEditMedicine.EditValue = currView["medicineID"];
            gridLookUpEditConMedicine.EditValue = currView["conMedicineID"];
            spinEditPrice.EditValue = currView["price"];
            spinEditCount.EditValue = currView["count"];
            labelControlQuantity.Text = currView["quantity"].ToString();
            serial.Text = currView["serial"].ToString();
            dateEditValidDate.Text = currView["validDate"].ToString();
            checkEditMultiSelection.Visible = false;


            if (Convert.ToInt16(docView["IncTypeID"]) == 2)
            {
                if (JournalDataSet.Instance.OrderGoodsDetail.Rows.Count == 0)
                    JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(),
                        Convert.ToString(docView["userID"]), MedicineIncForm.Instance.getStartDate().Date.ToString(),
                        MedicineIncForm.Instance.getEndDate().Date.ToString());
                JournalDataSet.MedicineOtherBindingSource.Filter = "packageID='" + currView["conMedicineID"] + "'";
                simpleButtonNewDoc.Enabled = false;
                gridLookUpEditUser.Enabled = false;
                radioGroupType.Enabled = false;
                treeListLookUpEditWarehouse.Enabled = false;
                treeListLookUpEditWard.Enabled = false;
                gridLookUpEditConMedicine.Enabled = true;
                gridLookUpEditIncType.Enabled = true;
                gridLookUpEditUser.Enabled = false;
                treeListLookUpEditWarehouse.Enabled = false;
                treeListLookUpEditWard.Enabled = false;
                textEditDocNumber.Enabled = true;
                gridLookUpEditProducer.Enabled = true;
                gridLookUpEditTender.Enabled = true;
                textEditID.Enabled = false;
                dateEditIncDate.Enabled = false;
                gridLookUpEditUser.Enabled = false;
                DataRow[] rows = JournalDataSet.Instance.OrderGoodsDetail.Select("orderGoodsID = '" + docView["orderGoodsID"] + "'");
                if (rows.Length > 0)
                {
                    for (int i = 0; i < rows.Length; i++)
                    {
                        if (Convert.ToString(currView["id"]) == Convert.ToString(rows[i]["expDetailID"]))
                        {
                            confirmCount = Convert.ToDouble(rows[i]["confirmCount"]);
                            // MessageBox.Show(Convert.ToString(confirmCount));
                        }
                    }
                }
            }
            else
            {
                simpleButtonNewDoc.Enabled = false;
                gridLookUpEditUser.Enabled = false;
                radioGroupType.Enabled = false;
                treeListLookUpEditWarehouse.Enabled = false;
                treeListLookUpEditWard.Enabled = false;
                gridLookUpEditConMedicine.Enabled = false;
                gridLookUpEditIncType.Enabled = true;
                gridLookUpEditUser.Enabled = false;
                treeListLookUpEditWarehouse.Enabled = false;
                treeListLookUpEditWard.Enabled = false;
                textEditDocNumber.Enabled = true;
                gridLookUpEditProducer.Enabled = true;
                gridLookUpEditTender.Enabled = true;
                textEditID.Enabled = false;
                dateEditIncDate.Enabled = false;
                gridLookUpEditUser.Enabled = false;
                confirmCount = -1;
                JournalDataSet.MedicineOtherBindingSource.Filter = "";
            }
        }

        private void addRow()
        {
            JournalDataSet.Instance.IncDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            currView = (DataRowView)JournalDataSet.IncDetailBindingSource.AddNew();
            clearMedicine();
        }

        private void clearData()
        {
            clearErrorText();
            //clearBinding();
            clearMedicine();
            checkEditMultiSelection.Checked = false;
            gridViewMed.ClearSelection();
            JournalDataSet.WardOtherBindingSource.Filter = "";
            JournalDataSet.WarehouseOtherBindingSource.Filter = "";
            JournalDataSet.SystemUserIncBindingSource.Filter = "";

            JournalDataSet.IncJournalBindingSource.CancelEdit();
            JournalDataSet.IncDetailBindingSource.CancelEdit();
            JournalDataSet.Instance.IncJournal.RejectChanges();
            JournalDataSet.Instance.IncDetail.RejectChanges();
        }

        private void clearBinding()
        {
            gridLookUpEditUser.DataBindings.Clear();
            treeListLookUpEditWarehouse.DataBindings.Clear();
            treeListLookUpEditWard.DataBindings.Clear();
            radioGroupType.DataBindings.Clear();
            gridLookUpEditIncType.DataBindings.Clear();
            gridLookUpEditTender.DataBindings.Clear();
            gridLookUpEditProducer.DataBindings.Clear();
            textEditID.DataBindings.Clear();
            textEditDocNumber.DataBindings.Clear();
            dateEditIncDate.DataBindings.Clear();
            memoEditDescription.DataBindings.Clear();
        }

        private void clearErrorText()
        {
            treeListLookUpEditWarehouse.ErrorText = "";
            gridLookUpEditIncType.ErrorText = "";
            gridLookUpEditMedicine.ErrorText = "";
            dateEditIncDate.ErrorText = "";
            spinEditCount.ErrorText = "";
            spinEditPrice.ErrorText = "";
            popupContainerEditMedicineBal.ErrorText = "";
            serial.ErrorText = "";
            dateEditValidDate.ErrorText = "";
        }

        private void clearMedicine()
        {
            gridLookUpEditConMedicine.EditValue = null;
            gridLookUpEditMedicine.EditValue = null;
            spinEditCount.Value = 0;
            spinEditPrice.Value = 0;
            spinEditSumPrice.Value = 0;
            serial.Text = "";
            dateEditValidDate.Text = "";
        }

        private void MedicineIncInUpForm_Shown(object sender, EventArgs e)
        {
            if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.ACCOUNTANT_ID)
                JournalDataSet.SystemUserIncBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";

            if (actionType == 1)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID)
                {
                    gridLookUpEditUser.EditValue = UserUtil.getUserId();
                    docView["userID"] = UserUtil.getUserId();
                    treeListLookUpEditWarehouse.Focus();
                }
                else
                {
                    gridLookUpEditUser.Focus();
                }
            }
            else if (actionType == 2)
            {
                gridLookUpEditMedicine.Focus();
            }
            ProgramUtil.IsShowPopup = true;
            ProgramUtil.IsSaveEvent = false;
            reloadTender(false);
            changeIncType();
            changeType();
            changeTender();
        }

        #endregion

        #region Формын event

        private void dateEditIncDate_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    dateEditIncDate.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditUser.Focus();
                }
            }
        }

        private void gridLookUpEditUser_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditUser.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    radioGroupType.Focus();
                    //if (radioGroupType.SelectedIndex == 0)
                    //    treeListLookUpEditWarehouse.Focus();
                    //else
                    //    treeListLookUpEditWard.Focus();
                }
            }
        }

        private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible && gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null)
            {
                JournalDataSet.WarehouseOtherBindingSource.Filter = UserUtil.getUserWarehouse(gridLookUpEditUser.EditValue.ToString());
                JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(gridLookUpEditUser.EditValue.ToString());
            }
        }


        private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeType();
        }

        private void treeListLookUpEditWarehouse_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditWarehouse.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditIncType.Focus();
                }
            }
        }

        private void treeListLookUpEditWard_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditWard.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditIncType.Focus();
                }
            }
        }


        private void gridLookUpEditIncType_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditIncType.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    if (gridLookUpEditIncType.EditValue != DBNull.Value && gridLookUpEditIncType.EditValue != null && Convert.ToInt32(gridLookUpEditIncType.EditValue) == 2)
                        gridLookUpEditTender.Focus();
                    else
                        gridLookUpEditProducer.Focus();
                }
            }
        }

        private void gridLookUpEditIncType_EditValueChanged(object sender, EventArgs e)
        {
            //if (Visible && gridLookUpEditIncType.EditValue != DBNull.Value && gridLookUpEditIncType.EditValue != null)
            if (gridLookUpEditIncType.EditValue != DBNull.Value && gridLookUpEditIncType.EditValue != null)
                changeIncType();
        }

        private void gridLookUpEditTender_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible && gridLookUpEditTender.EditValue != DBNull.Value && gridLookUpEditTender.EditValue != null && !gridLookUpEditTender.Equals(""))
            {
                changeTender();
                DataRowView view = (DataRowView)gridLookUpEditTender.GetSelectedDataRow();
                if (view != null)
                    gridLookUpEditProducer.EditValue = view["supplierID"];
            }
        }

        private void gridLookUpEditTender_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditTender.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    textEditDocNumber.Focus();
                }
            }
        }

        private void gridLookUpEditProducer_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditProducer.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    textEditDocNumber.Focus();
                }
            }
        }

        private void gridLookUpEditProducer_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "add"))
            {
                SupplierInForm.Instance.showForm(1, Instance);
            }
        }

        private void memoEditDescription_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (gridLookUpEditIncType.EditValue != DBNull.Value && gridLookUpEditIncType.EditValue != null && Convert.ToInt32(gridLookUpEditIncType.EditValue) == 2)
                    gridLookUpEditConMedicine.Focus();
                else
                    gridLookUpEditMedicine.Focus();
            }
        }


        private void gridLookUpEditConMedicine_EditValueChanged(object sender, EventArgs e)
        {
            DataRowView view = (DataRowView)gridLookUpEditConMedicine.GetSelectedDataRow();
            if (gridLookUpEditConMedicine.EditValue != null && gridLookUpEditConMedicine.EditValue != DBNull.Value && view != null)
            {
                reload(true);
                if (gridLookUpEditIncType.EditValue != DBNull.Value && gridLookUpEditIncType.EditValue != null && !gridLookUpEditIncType.Text.Equals("") && Convert.ToInt32(gridLookUpEditIncType.EditValue) == 2)
                    gridLookUpEditMedicine.EditValue = view["id"];
                else
                    gridLookUpEditMedicine.EditValue = null;
            }
        }

        private void gridLookUpEditConMedicine_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    if (!ProgramUtil.IsSaveEvent)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditConMedicine.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsSaveEvent = false;
                    }
                }
                else
                {
                    spinEditCount.Focus();
                }
            }
        }

        private void gridLookUpEditMedicine_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    if (!ProgramUtil.IsSaveEvent)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditMedicine.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsSaveEvent = false;
                    }
                }
                else
                {
                    spinEditCount.Focus();
                }
            }
        }

        private void gridLookUpEditMedicine_EditValueChanged(object sender, EventArgs e)
        {
            DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
            if (gridLookUpEditMedicine.EditValue != null && gridLookUpEditMedicine.EditValue != DBNull.Value && view != null)
            {
                labelControlQuantity.Text = view["quantity"].ToString();
                spinEditPrice.Value = Convert.ToDecimal(view["price"]);
                serial.Text = view["serial"].ToString();
                dateEditValidDate.Text = view["validDate"].ToString();
            }
        }

        private void gridLookUpEditMedicine_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "add"))
            {
                MedicineInUpForm.Instance.showForm(3, Instance);
            }
        }

        private void spinEditCount_ValueChanged(object sender, EventArgs e)
        {
            calcPrice();
            if (confirmCount > 0)
            {
                if (Convert.ToDouble(spinEditCount.Value) > confirmCount)
                {
                    XtraMessageBox.Show("Баталсан тооноос их байна! Баталсан тооны хэмжээгээр орууллаа.");
                    spinEditCount.Value = Convert.ToDecimal(confirmCount);
                }
            }
        }


        private void simpleButtonNewDoc_Click(object sender, EventArgs e)
        {
            clearData();
            insert();
            dateEditIncDate.Focus();
        }

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            reload(true);
            reloadTender(true);
        }

        private void simpleButtonSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void checkEditMultiSelection_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEditMultiSelection.Checked)
            {
                spinEditCount.ReadOnly = true;
                spinEditPrice.ReadOnly = true;
                spinEditSumPrice.ReadOnly = true;
                popupContainerEditMedicineBal.Visible = true;
                popupContainerEditMedicineBal.Location = new Point(167, 321);
                gridLookUpEditMedicine.Visible = false;
                serial.ReadOnly = true;
                dateEditValidDate.ReadOnly = true;
            }
            else
            {
                spinEditCount.ReadOnly = false;
                spinEditPrice.ReadOnly = false;
                spinEditSumPrice.ReadOnly = false;
                gridLookUpEditMedicine.Visible = true;
                popupContainerEditMedicineBal.Visible = false;
                popupContainerEditMedicineBal.Location = new Point(287, 347);
                serial.ReadOnly = false;
                dateEditValidDate.ReadOnly = false;
            }
        }

        private void popupContainerEditMedicineBal_CloseUp(object sender, DevExpress.XtraEditors.Controls.CloseUpEventArgs e)
        {
            rowID = gridViewMed.GetSelectedRows();
        }

        #endregion

        #region Формын функц

        private void reload(bool isReload)
        {
            //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
            //InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN);
            if (isReload || JournalDataSet.Instance.SystemUserInc.Rows.Count == 0)
                JournalDataSet.SystemUserIncTableAdapter.Fill(JournalDataSet.Instance.SystemUserInc, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
            if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
            if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
            if (isReload || InformationDataSet.Instance.IncTypeOther.Rows.Count == 0)
                InformationDataSet.IncTypeOtherTableAdapter.Fill(InformationDataSet.Instance.IncTypeOther, HospitalUtil.getHospitalId());
            if (isReload || JournalDataSet.Instance.MedicineOther.Rows.Count == 0)
            {
                if (gridLookUpEditConMedicine.EditValue != null)
                {
                    JournalDataSet.MedicineOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOther, HospitalUtil.getHospitalId(), gridLookUpEditConMedicine.EditValue.ToString());
                }
                else
                {
                    JournalDataSet.MedicineOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOther, HospitalUtil.getHospitalId(), "");
                }
            }

            if (isReload || UserDataSet.Instance.Supplier.Rows.Count == 0)
                UserDataSet.SupplierTableAdapter.Fill(UserDataSet.Instance.Supplier, HospitalUtil.getHospitalId());
            //ProgramUtil.closeWaitDialog();
        }

        private void reloadTender(bool isReload)
        {
            if (isReload || JournalDataSet.Instance.TenderOther.Rows.Count == 0)
            {
                JournalDataSet.TenderOtherTableAdapter.Fill(JournalDataSet.Instance.TenderOther, HospitalUtil.getHospitalId(), dateEditIncDate.Text);

                List<string> list = new List<string>();
                JournalDataSet.Instance.Tender.Clear();
                for (int i = 0; i < JournalDataSet.Instance.TenderOther.Rows.Count; i++)
                {
                    DataRow row = JournalDataSet.Instance.TenderOther.Rows[i];
                    if (list.IndexOf(row["tenderID"].ToString()) == -1)
                    {
                        list.Add(row["tenderID"].ToString());

                        DataRow newRow = JournalDataSet.Instance.Tender.NewRow();
                        newRow["tenderID"] = row["tenderID"];
                        newRow["date"] = row["date"];
                        newRow["tenderNumber"] = row["tenderNumber"];
                        newRow["supplierID"] = row["supplierID"];
                        newRow["supplierName"] = row["supplierName"];
                        newRow.EndEdit();
                        JournalDataSet.Instance.Tender.Rows.Add(newRow);
                    }
                }
            }
        }

        private void calcPrice()
        {
            spinEditSumPrice.Value = spinEditCount.Value * spinEditPrice.Value;
        }

        private void changeType()
        {
            int x = labelControl14.Location.X;
            if (radioGroupType.SelectedIndex == 0)
            {
                labelControlWarehouse.Location = new Point(x + 38, 147);
                treeListLookUpEditWard.Location = new Point(167, 12);
                treeListLookUpEditWarehouse.Location = new Point(x + 105, 144);

                treeListLookUpEditWard.Visible = false;
                treeListLookUpEditWarehouse.Visible = true;
                labelControlWarehouse.Text = "Эмийн сан*:";
            }
            else if (radioGroupType.SelectedIndex == 1)
            {
                labelControlWarehouse.Location = new Point(x + 61, 147);
                treeListLookUpEditWard.Location = new Point(x + 105, 144);
                treeListLookUpEditWarehouse.Location = new Point(167, 12);

                treeListLookUpEditWard.Visible = true;
                treeListLookUpEditWarehouse.Visible = false;
                labelControlWarehouse.Text = "Тасаг*:";
            }
        }

        private void changeIncType()
        {
            int x = labelControl11.Location.X;
            int value = Convert.ToInt32(gridLookUpEditIncType.EditValue);
            if (value == 2)
            {
                gridLookUpEditConMedicine.Properties.ReadOnly = false;
                gridLookUpEditTender.Location = new Point(x + 94, 196);
                gridLookUpEditProducer.Location = new Point(167, 425);
                gridLookUpEditTender.Visible = true;
                gridLookUpEditProducer.Visible = false;
            }
            else
            {
                gridLookUpEditConMedicine.Properties.ReadOnly = true;
                gridLookUpEditTender.Location = new Point(167, 425);
                gridLookUpEditProducer.Location = new Point(x + 94, 196);
                gridLookUpEditTender.Visible = false;
                gridLookUpEditProducer.Visible = true;
            }
        }

        private void changeTender()
        {
            JournalDataSet.Instance.TenderMedicine.Clear();
            DataRow[] rows = JournalDataSet.Instance.TenderOther.Select("tenderID = '" + gridLookUpEditTender.EditValue + "'");
            for (int i = 0; i < rows.Length; i++)
            {
                DataRow newRow = JournalDataSet.Instance.TenderMedicine.NewRow();
                newRow["packageID"] = rows[i]["packageID"];
                newRow["packageCount"] = rows[i]["packageCount"];
                newRow["id"] = rows[i]["id"];
                newRow["price"] = rows[i]["price"];
                newRow["count"] = rows[i]["count"];
                newRow["name"] = rows[i]["name"];
                newRow["latinName"] = rows[i]["latinName"];
                newRow["medicineTypeName"] = rows[i]["medicineTypeName"];
                newRow["unitType"] = rows[i]["unitType"];
                newRow["shape"] = rows[i]["shape"];
                newRow["quantity"] = rows[i]["quantity"];
                newRow["validDate"] = rows[i]["validDate"];
                newRow["barcode"] = rows[i]["barcode"];
                newRow.EndEdit();
                JournalDataSet.Instance.TenderMedicine.Rows.Add(newRow);
            }
        }

        private bool check()
        {
            //Instance.Validate();
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();

            if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.Text.Equals(""))
            {
                errorText = "Няравыг сонгоно уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditUser.ErrorText = errorText;
                isRight = false;
                gridLookUpEditUser.Focus();
            }
            if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.Text.Equals("")))
            {
                errorText = "Агуулахыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWarehouse.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWarehouse.Focus();
                }
            }
            if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals("")))
            {
                errorText = "Тасгийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWard.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWard.Focus();
                }
            }
            if (dateEditIncDate.EditValue == DBNull.Value || dateEditIncDate.EditValue == null || dateEditIncDate.Text.Equals(""))
            {
                errorText = "Огноог оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditIncDate.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    dateEditIncDate.Focus();
                }
            }
            if (gridLookUpEditIncType.EditValue == DBNull.Value || gridLookUpEditIncType.EditValue == null || gridLookUpEditIncType.Text.Equals(""))
            {
                errorText = "Орлогын төрлийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditIncType.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditIncType.Focus();
                }
            }
            if (gridLookUpEditIncType.EditValue != DBNull.Value && gridLookUpEditIncType.EditValue != null && !gridLookUpEditIncType.Text.Equals("") && Convert.ToInt32(gridLookUpEditIncType.EditValue) == 2)
            {
                if (gridLookUpEditTender.EditValue == DBNull.Value || gridLookUpEditTender.EditValue == null || gridLookUpEditTender.Text.Equals(""))
                {
                    errorText = "Тендерийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditTender.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditTender.Focus();
                    }
                }
            }
            else
            {
                if (gridLookUpEditProducer.EditValue == DBNull.Value || gridLookUpEditProducer.EditValue == null || gridLookUpEditProducer.Text.Equals(""))
                {
                    errorText = "Бэлтгэн нийлүүлэгчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditProducer.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditProducer.Focus();
                    }
                }
            }

            if ((gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.Text.Equals("")) && !checkEditMultiSelection.Checked)
            {
                errorText = "Эмийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditMedicine.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
            }
            if ((serial.EditValue == DBNull.Value || serial.EditValue == null || serial.Text.Equals("")) && checkEditMultiSelection.Checked == false)
            {
                errorText = "Эмийн сериалийг оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                serial.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
            }
            if ((dateEditValidDate.EditValue == DBNull.Value || dateEditValidDate.EditValue == null || dateEditValidDate.Text.Equals("")) && checkEditMultiSelection.Checked == false)
            {
                errorText = "Хүчинтэй хугацааг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditValidDate.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
            }
            if (spinEditPrice.Value == 0 && !checkEditMultiSelection.Checked)
            {
                errorText = "Үнийг оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                spinEditPrice.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    spinEditPrice.Focus();
                }
            }
            if (spinEditCount.Value == 0 && !checkEditMultiSelection.Checked)
            {
                errorText = "Тоо, хэмжээг оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                spinEditCount.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    spinEditCount.Focus();
                }
            }
            //if (serial.Text == "" && checkEditMultiSelection.Checked == false)
            //{
            //    errorText = "Серийн дугаар оруулна уу";
            //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
            //    spinEditCount.ErrorText = errorText;
            //    if (isRight)
            //    {
            //        isRight = false;
            //        spinEditCount.Focus();
            //    }
            //}
            //if ((dateEditValidDate.EditValue == DBNull.Value || dateEditIncDate.EditValue == null || dateEditIncDate.Text.Equals("")) && checkEditMultiSelection.Checked == false)
            //{
            //    errorText = "Хүчинтэй хугацааг оруулна уу";
            //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
            //    dateEditIncDate.ErrorText = errorText;
            //    if (isRight)
            //    {
            //        isRight = false;
            //        dateEditIncDate.Focus();
            //    }
            //}
            if (isRight)
            {
                DataRow[] rows = JournalDataSet.Instance.IncDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND incJournalID = '" + docView["id"] + "' AND id <> '" + currView["id"] + "'");
                for (int i = 0; rows != null && i < rows.Length; i++)
                {
                    if (gridLookUpEditMedicine.EditValue.ToString().Equals(rows[i]["medicineID"].ToString()) && spinEditPrice.Value == Convert.ToDecimal(rows[i]["price"]))
                    {
                        errorText = "Эм, үнэ давхацсан байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditMedicine.ErrorText = errorText;
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                        break;
                    }
                }
            }
            if (isRight && checkEditMultiSelection.Checked)
            {
                if (rowID.Length == 0)
                {
                    errorText = "Эмээс сонгоно уу!";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    popupContainerEditMedicineBal.ErrorText = errorText;
                    isRight = false;
                    popupContainerEditMedicineBal.Focus();
                }
                else
                {
                    for (int i = 0; i < rowID.Length; i++)
                    {
                        if (Convert.ToDouble(gridViewMed.GetRowCellValue(rowID[i], gridColumnDupCount)) == 0 || Convert.ToDouble(gridViewMed.GetRowCellValue(rowID[i], gridColumnPrice)) == 0)
                        {
                            errorText = "Эмийн тоо, үнийг оруулна уу!";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            popupContainerEditMedicineBal.ErrorText = errorText;
                            isRight = false;
                            popupContainerEditMedicineBal.Focus();
                            break;
                        }
                        else if (gridViewMed.GetRowCellValue(rowID[i], gridColumnSerial) == "") {
                            errorText = "Эмийн серийн дугаарыг оруулна уу!";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            popupContainerEditMedicineBal.ErrorText = errorText;
                            isRight = false;
                            popupContainerEditMedicineBal.Focus();
                            break;
                        }
                        else if (gridViewMed.GetRowCellValue(rowID[i], gridColumnValidatedDate) == "")
                        {
                            errorText = "Эмийн хүчинтэй хугацааг оруулна уу!";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            popupContainerEditMedicineBal.ErrorText = errorText;
                            isRight = false;
                            popupContainerEditMedicineBal.Focus();
                            break;
                        }
                    }
                }

            }
            if (!isRight && !text.Equals(""))
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        private void save()
        {
            ProgramUtil.IsSaveEvent = true;
            ProgramUtil.IsShowPopup = true;
            if (check())
            {
                if (ProgramUtil.checkLockMedicine(dateEditIncDate.EditValue.ToString()))
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);             

                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                    DataRowView tender = (DataRowView)gridLookUpEditTender.GetSelectedDataRow();
                    DataRowView medicine = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                    if (radioGroupType.SelectedIndex == 0)
                    {
                        docView["wardID"] = null;
                        docView["warehouseName"] = warehouse["name"];
                        docView["warehouseDesc"] = warehouse["description"];

                        currView["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                        currView["wardID"] = null;
                        currView["warehouseName"] = warehouse["name"];
                        currView["warehouseDesc"] = warehouse["description"];
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {
                        docView["warehouseID"] = null;
                        docView["warehouseName"] = ward["name"];
                        docView["warehouseDesc"] = ward["description"];

                        currView["warehouseID"] = null;
                        currView["wardID"] = treeListLookUpEditWard.EditValue;
                        currView["warehouseName"] = ward["name"];
                        currView["warehouseDesc"] = ward["description"];
                    }
                    docView["userName"] = user["fullName"];
                    docView["roleName"] = user["roleName"];
                    docView["incTypeName"] = gridLookUpEditIncType.Text;
                    if (Convert.ToInt32(gridLookUpEditIncType.EditValue) == 2)
                    {
                        docView["supplierID"] = tender["supplierID"];
                        docView["supplierName"] = tender["supplierName"];

                        currView["tenderID"] = gridLookUpEditTender.EditValue;
                        currView["supplierID"] = tender["supplierID"];
                        currView["supplierName"] = tender["supplierName"];
                    }
                    else
                    {
                        docView["tenderID"] = null;
                        docView["supplierName"] = gridLookUpEditProducer.Text;

                        currView["tenderID"] = null;
                        currView["supplierID"] = gridLookUpEditProducer.EditValue;
                        currView["supplierName"] = gridLookUpEditProducer.Text;
                    }
                    docView.EndEdit();
                    if (checkEditMultiSelection.Checked)
                    {
                        if (JournalDataSet.IncDetailBindingSource.Current != null)
                            JournalDataSet.IncDetailBindingSource.RemoveCurrent();
                        addMultipleRow();
                    }
                    else
                    {
                        //Орлогоос эм бүртгэх
                        string newID = "";
                        if (!ProgramUtil.checkHaveMedicine(gridLookUpEditMedicine.EditValue.ToString(), spinEditPrice.EditValue.ToString(), dateEditValidDate.EditValue.ToString(), serial.Text))
                        {
                            SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                            myConn.Open();
                            string query = "SELECT * FROM [Medicine] WHERE [id] = '" + gridLookUpEditMedicine.EditValue.ToString() + "'";
                            SqlCommand oCmd = new SqlCommand(query, myConn);
                            using (SqlDataReader oReader = oCmd.ExecuteReader())
                            {
                                while (oReader.Read())
                                {
                                    if (oReader.HasRows)
                                    {
                                        UserDataSet.Instance.Medicine.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                                        UserDataSet.Instance.Medicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                                        newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                                        UserDataSet.Instance.Medicine.idColumn.DefaultValue = newID;
                                        DataRowView view = (DataRowView)UserDataSet.MedicineBindingSource.AddNew();
                                        view["name"] = oReader["name"].ToString();
                                        view["latinName"] = oReader["latinName"].ToString();
                                        view["barcode"] = "";
                                        view["unitID"] = oReader["unitID"].ToString();
                                        view["shapeID"] = oReader["shapeID"].ToString();
                                        view["quantityID"] = oReader["quantityID"].ToString();
                                        view["medicineSeries"] = "";
                                        view["serial"] = serial.EditValue;
                                        view["description"] = "";
                                        view["packageId"] = oReader["packageId"].ToString();
                                        view["medicineTypeId"] = oReader["medicineTypeId"].ToString();
                                        view["medicineTypeInterId"] = oReader["medicineTypeInterId"].ToString();
                                        view["producerCountryID"] = oReader["producerCountryID"].ToString();
                                        view["producerID"] = oReader["producerID"].ToString();
                                        view["registerID"] = oReader["registerID"].ToString();
                                        view["btkusID"] = oReader["btkusID"].ToString();
                                        view["validDate"] = dateEditValidDate.EditValue;
                                        view["price"] = spinEditPrice.EditValue;

                                        view["PackageMedicineName"] = "x";
                                        view["medicineTypeName"] = "x";
                                        view["medicineTypeInterName"] = "x";
                                        view["shapeName"] = "x";
                                        view["unitName"] = "x";
                                        view["producerCountryName"] = "x";
                                        view["producerName"] = "x";
                                        view["quantityName"] = "x";
                                        view["parmaStandart"] = "x";
                                        view["alterName"] = "x";
                                        view["soloName"] = "x";
                                        view.EndEdit();
                                        UserDataSet.MedicineTableAdapter.Update(UserDataSet.Instance.Medicine);
                                    }
                                }
                            }
                            myConn.Close();
                            currView["medicineID"] = newID;
                        }
                        else {
                            currView["medicineID"] = gridLookUpEditMedicine.EditValue;
                        }

                        currView["date"] = dateEditIncDate.EditValue;
                        currView["docNumber"] = textEditDocNumber.EditValue;
                        currView["description"] = memoEditDescription.EditValue;
                        currView["userID"] = gridLookUpEditUser.EditValue;
                        currView["type"] = radioGroupType.SelectedIndex;
                        currView["incTypeID"] = gridLookUpEditIncType.EditValue;
                        currView["userName"] = user["fullName"];
                        currView["roleName"] = user["roleName"];
                        currView["incTypeName"] = gridLookUpEditIncType.Text;
                        
                        currView["conMedicineID"] = gridLookUpEditConMedicine.EditValue;
                        currView["medicineName"] = medicine["name"];
                        currView["latinName"] = medicine["latinName"];
                        currView["unitType"] = medicine["unitType"];
                        currView["shape"] = medicine["shape"];
                        currView["quantity"] = medicine["quantity"];
                        
                        currView["medicineTypeName"] = medicine["medicineTypeName"];
                        currView["price"] = spinEditPrice.EditValue;
                        currView["count"] = spinEditCount.EditValue;
                        currView.EndEdit();


                        for (int i = 0; i < JournalDataSet.IncDetailBindingSource.Count; i++)
                        {
                            DataRowView rows = (DataRowView)JournalDataSet.IncDetailBindingSource[i];
                            if (rows["incJournalID"].ToString().Equals(docView["id"].ToString()) && !rows["id"].ToString().Equals(currView["id"].ToString()))
                            {
                                rows["date"] = dateEditIncDate.EditValue;
                                rows["docNumber"] = textEditDocNumber.EditValue;
                                rows["description"] = memoEditDescription.EditValue;
                                rows["userID"] = gridLookUpEditUser.EditValue;
                                rows["type"] = radioGroupType.SelectedIndex;
                                rows["incTypeID"] = gridLookUpEditIncType.EditValue;
                                rows["incTypeName"] = gridLookUpEditIncType.Text;
                                rows["userName"] = user["fullName"];
                                rows["roleName"] = user["roleName"];

                                if (radioGroupType.SelectedIndex == 0)
                                {
                                    rows["wardID"] = null;
                                    rows["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                                    rows["warehouseName"] = warehouse["name"];
                                    rows["warehouseDesc"] = warehouse["description"];
                                }
                                else if (radioGroupType.SelectedIndex == 1)
                                {
                                    rows["wardID"] = treeListLookUpEditWard.EditValue;
                                    rows["warehouseID"] = null;
                                    rows["warehouseName"] = ward["name"];
                                    rows["warehouseDesc"] = ward["description"];
                                }

                                if (Convert.ToInt32(gridLookUpEditIncType.EditValue) == 2)
                                {
                                    rows["tenderID"] = gridLookUpEditTender.EditValue;
                                    rows["supplierID"] = tender["supplierID"];
                                    rows["supplierName"] = tender["supplierName"];
                                }
                                else
                                {
                                    rows["tenderID"] = null;
                                    rows["supplierID"] = gridLookUpEditProducer.EditValue;
                                    rows["supplierName"] = gridLookUpEditProducer.Text;
                                }
                                rows.EndEdit();
                            }
                        }
                    }
                    JournalDataSet.IncJournalTableAdapter.Update(JournalDataSet.Instance.IncJournal);
                    JournalDataSet.IncDetailTableAdapter.Update(JournalDataSet.Instance.IncDetail);

                    ProgramUtil.closeWaitDialog();
                    if (checkEditMultiSelection.Checked)
                        Close();
                    else
                        addRow();
                    if (Convert.ToInt32(gridLookUpEditIncType.EditValue) == 2)
                        gridLookUpEditConMedicine.Focus();
                    else
                        gridLookUpEditMedicine.Focus();
                }
                else
                {
                    XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
                }  
            }

        }

        private void addMultipleRow()
        {
            for (int i = 0; i < rowID.Length; i++)
            {
                DataRow medicine = gridViewMed.GetDataRow(rowID[i]);
                DataRow newRow = JournalDataSet.Instance.IncDetail.NewRow();
                //Орлогоос эм бүртгэх
                string newID = "";
                if (!ProgramUtil.checkHaveMedicine(medicine["id"].ToString(), medicine["price"].ToString(), medicine["validDate"].ToString(), medicine["serial"].ToString()))
                {
                    SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                    myConn.Open();
                    string query = "SELECT * FROM [Medicine] WHERE [id] = '" + medicine["id"].ToString() + "'";
                    SqlCommand oCmd = new SqlCommand(query, myConn);
                    using (SqlDataReader oReader = oCmd.ExecuteReader())
                    {
                        while (oReader.Read())
                        {
                            if (oReader.HasRows)
                            {
                                UserDataSet.Instance.Medicine.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                                UserDataSet.Instance.Medicine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                                newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                                UserDataSet.Instance.Medicine.idColumn.DefaultValue = newID;
                                DataRowView view = (DataRowView)UserDataSet.MedicineBindingSource.AddNew();
                                view["name"] = oReader["name"].ToString();
                                view["latinName"] = oReader["latinName"].ToString();
                                view["barcode"] = "";
                                view["unitID"] = oReader["unitID"].ToString();
                                view["shapeID"] = oReader["shapeID"].ToString();
                                view["quantityID"] = oReader["quantityID"].ToString();
                                view["medicineSeries"] = "";
                                view["serial"] = medicine["serial"].ToString();
                                view["description"] = "";
                                view["packageId"] = oReader["packageId"].ToString();
                                view["medicineTypeId"] = oReader["medicineTypeId"].ToString();
                                view["medicineTypeInterId"] = oReader["medicineTypeInterId"].ToString();
                                view["producerCountryID"] = oReader["producerCountryID"].ToString();
                                view["producerID"] = oReader["producerID"].ToString();
                                view["registerID"] = oReader["registerID"].ToString();
                                view["btkusID"] = oReader["btkusID"].ToString();
                                view["validDate"] = medicine["validDate"].ToString();
                                view["price"] = medicine["price"].ToString();

                                view["PackageMedicineName"] = "x";
                                view["medicineTypeName"] = "x";
                                view["medicineTypeInterName"] = "x";
                                view["shapeName"] = "x";
                                view["unitName"] = "x";
                                view["producerCountryName"] = "x";
                                view["producerName"] = "x";
                                view["quantityName"] = "x";
                                view["parmaStandart"] = "x";
                                view["alterName"] = "x";
                                view["soloName"] = "x";
                                view.EndEdit();
                                UserDataSet.MedicineTableAdapter.Update(UserDataSet.Instance.Medicine);
                            }
                        }
                    }
                    myConn.Close();
                    newRow["medicineID"] = newID;
                }
                else
                {
                    newRow["medicineID"] = medicine["id"];
                }


                newRow["id"] = getNotExistId();
                newRow["incJournalID"] = docView["id"];
                newRow["date"] = dateEditIncDate.EditValue;
                newRow["userID"] = gridLookUpEditUser.EditValue;
                newRow["type"] = radioGroupType.SelectedIndex;
                newRow["incTypeID"] = gridLookUpEditIncType.EditValue;
                newRow["tenderID"] = gridLookUpEditTender.EditValue;
                newRow["supplierID"] = gridLookUpEditProducer.EditValue;
                newRow["docNumber"] = textEditDocNumber.EditValue;
                newRow["description"] = memoEditDescription.EditValue;
                newRow["medicineName"] = medicine["name"];
                newRow["latinName"] = medicine["latinName"];
                newRow["medicineTypeName"] = medicine["medicineTypeName"];
                newRow["unitType"] = medicine["unitType"];
                newRow["shape"] = medicine["shape"];
                newRow["quantity"] = medicine["quantity"];
                newRow["validDate"] = medicine["validDate"];
                newRow["price"] = medicine["price"];
                newRow["count"] = medicine["count"];
                JournalDataSet.Instance.IncDetail.Rows.Add(newRow);
            }
        }

        private string getNotExistId()
        {
            string newID = null;
            DataRow[] rows = null;
            do
            {
                newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                rows = JournalDataSet.Instance.IncDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND incJournalID = '" + docView["id"] + "' AND id = '" + newID + "'");
            } while (rows != null && rows.Length > 0);

            return newID;
        }

        // Шинээр нэмсэн эмийг авч байна
        public void addMedicine(DataRowView view)
        {
            DataRowView row = (DataRowView)JournalDataSet.MedicineOtherBindingSource.AddNew();
            row["id"] = view["id"];
            row["name"] = view["name"];
            row["latinName"] = view["latinName"];
            row["unitType"] = view["unitName"];
            row["price"] = view["price"];
            row["medicineTypeName"] = view["medicineTypeName"];
            row["barcode"] = view["barcode"];
            row["shape"] = view["shapeName"];
            row["validDate"] = view["validDate"];
            row["serial"] = view["serial"];
            row["quantity"] = view["quantityName"];
            row.EndEdit();
            gridLookUpEditMedicine.EditValue = view["id"];
        }

        // Шинээр нэмсэн бэлгэн нийлүүлэгчийг авч байна
        public void addProducer(string id)
        {
            gridLookUpEditProducer.EditValue = id;
        }

        #endregion

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }
    }
}