﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.info.medicine;



namespace Pharmacy2016.gui.core.journal.inc
{
    /*
     * Эмийн орлого нэмэх, засах цонх.
     * 
     */

    public partial class MedicineIncInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineIncInUpForm INSTANCE = new MedicineIncInUpForm();

            public static MedicineIncInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private string id;

            private MedicineIncInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                spinEditCount.Properties.MaxValue = Decimal.MaxValue - 1;
                spinEditPrice.Properties.MaxValue = Decimal.MaxValue - 1;

                initError();
                initDataSource();
                initBinding();
                reload(false);
            }

            private void initDataSource()
            {
                gridControlDetail.DataSource = JournalDataSet.IncDetailBindingSource;
                gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
                treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
                treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
                gridLookUpEditIncType.Properties.DataSource = InformationDataSet.IncTypeOtherBindingSource;
                gridLookUpEditProducer.Properties.DataSource = UserDataSet.ProducerBindingSource;
                gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineOtherBindingSource;
            }

            private void initError()
            {
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditIncType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditIncDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditPrice.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    
                    actionType = type;
                    if (actionType == 1)
                    {
                        insertInc();
                    }
                    else if (actionType == 2)
                    {
                        updateInc();
                    }

                    ShowDialog(MedicineIncForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void initBinding()
            {
                gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "userID", true));
                treeListLookUpEditWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "warehouseID", true));
                treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "wardID", true));
                radioGroupType.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "type", true));
                gridLookUpEditIncType.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "incTypeID", true));
                gridLookUpEditProducer.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "producerID", true));
                textEditID.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "id", true));
                textEditDocNumber.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "docNumber", true));
                dateEditIncDate.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "date", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncJournalBindingSource, "description", true));

                gridLookUpEditMedicine.DataBindings.Add(new Binding("EditValue", JournalDataSet.IncDetailBindingSource, "medicineID", true));
            }

            public void insertInc()
            {
                this.Text = "Орлого нэмэх цонх";
                id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                JournalDataSet.Instance.IncJournal.idColumn.DefaultValue = id;
                JournalDataSet.Instance.IncDetail.incJournalIDColumn.DefaultValue = id;
                
                MedicineIncForm.Instance.currView = (DataRowView)JournalDataSet.IncJournalBindingSource.AddNew();
                DateTime now = DateTime.Now;
                MedicineIncForm.Instance.currView["id"] = id;
                MedicineIncForm.Instance.currView["date"] = now;

                dateEditIncDate.DateTime = now;
                textEditID.Text = id;
            }

            public void updateInc()
            {
                this.Text = "Орлого засах цонх";
                id = MedicineIncForm.Instance.currView["id"].ToString();
                JournalDataSet.Instance.IncDetail.incJournalIDColumn.DefaultValue = id;
            }


            private void clearData()
            {
                clearErrorText();
                //clearBinding();
                clearMedicine();

                JournalDataSet.WardOtherBindingSource.Filter = "";
                JournalDataSet.WarehouseOtherBindingSource.Filter = "";
                InformationDataSet.SystemUserBindingSource.Filter = "";

                JournalDataSet.IncJournalBindingSource.CancelEdit();
                JournalDataSet.IncDetailBindingSource.CancelEdit();
                JournalDataSet.Instance.IncJournal.RejectChanges();
                JournalDataSet.Instance.IncDetail.RejectChanges();
            }

            private void clearBinding()
            {
                gridLookUpEditUser.DataBindings.Clear();
                treeListLookUpEditWarehouse.DataBindings.Clear();
                treeListLookUpEditWard.DataBindings.Clear();
                radioGroupType.DataBindings.Clear();
                gridLookUpEditIncType.DataBindings.Clear();
                gridLookUpEditProducer.DataBindings.Clear();
                textEditID.DataBindings.Clear();
                textEditDocNumber.DataBindings.Clear();
                dateEditIncDate.DataBindings.Clear();
                memoEditDescription.DataBindings.Clear();

                gridLookUpEditMedicine.DataBindings.Clear();
            }

            private void clearErrorText()
            {
                treeListLookUpEditWarehouse.ErrorText = "";
                gridLookUpEditIncType.ErrorText = "";
                gridLookUpEditMedicine.ErrorText = "";
                dateEditIncDate.ErrorText = "";
                spinEditCount.ErrorText = "";
                spinEditPrice.ErrorText = "";
            }

            private void clearMedicine()
            {
                gridLookUpEditMedicine.EditValue = null;
                textEditBarcode.Text = "";
                spinEditCount.Value = 0;
                spinEditPrice.Value = 0;
                spinEditSumPrice.Value = 0;
            }

            private void MedicineIncInUpForm_Shown(object sender, EventArgs e)
            {
                gridViewDetail.FocusedRowHandle = 0;
                gridViewDetail_BeforeLeaveRow(gridViewDetail, new RowAllowEventArgs(gridViewDetail.FocusedRowHandle, true));

                if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID)
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";

                if (actionType == 1)
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID)
                    {
                        gridLookUpEditUser.EditValue = UserUtil.getUserId();
                        treeListLookUpEditWarehouse.Focus();
                    }
                    else
                    {
                        gridLookUpEditUser.Focus();
                    }
                }
                else if (actionType == 2)
                {
                    setValueFromGrid();
                    gridLookUpEditMedicine.Focus();
                }
            }

        #endregion

        #region Формын event

            private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null)
                {
                    
                    if (actionType == 1 && (gridLookUpEditIncType.EditValue != null && gridLookUpEditIncType.EditValue != DBNull.Value && !gridLookUpEditIncType.EditValue.ToString().Equals("")) &&
                        (radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && !treeListLookUpEditWarehouse.EditValue.ToString().Equals("")) &&
                        (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && !treeListLookUpEditWard.EditValue.ToString().Equals("")) && gridViewDetail.RowCount == 0)
                    {
                        addRow();
                        gridLookUpEditMedicine.Focus();
                    }
                }
            }

            private void gridLookUpEditIncType_EditValueChanged(object sender, EventArgs e)
            {
                if (actionType == 1 && gridViewDetail.RowCount == 0 && 
                        (gridLookUpEditUser.EditValue != null && gridLookUpEditUser.EditValue != DBNull.Value && !gridLookUpEditUser.EditValue.ToString().Equals("")) &&
                        (gridLookUpEditIncType.EditValue != null && gridLookUpEditIncType.EditValue != DBNull.Value && !gridLookUpEditIncType.EditValue.ToString().Equals("")) &&
                        ((radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && !treeListLookUpEditWarehouse.EditValue.ToString().Equals("")) ||
                        (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && !treeListLookUpEditWard.EditValue.ToString().Equals(""))))
                {
                    reload(true);
                    addRow();
                    gridLookUpEditMedicine.Focus();
                }
            }

            private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeType();
            }

            private void gridLookUpEditProducer_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    ProducerInForm.Instance.showForm(1, Instance);
                }
            }


            private void gridControlDetail_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add") && (gridViewDetail.RowCount == 0 || (gridLookUpEditMedicine.ErrorText == "" && gridLookUpEditMedicine.EditValue != null && gridLookUpEditMedicine.EditValue != DBNull.Value && checkMedicine())))
                    {
                        addRow();
                        gridLookUpEditMedicine.Focus();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewDetail.GetFocusedDataRow() != null)
                    {
                        deleteRow();
                    }
                }
            }

            private void gridViewDetail_BeforeLeaveRow(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
            {
                if (this.Visible && gridViewDetail.GetDataRow(e.RowHandle) != null &&
                    (gridViewDetail.GetDataRow(e.RowHandle)["medicineID"] == DBNull.Value
                    || Convert.ToDecimal(gridViewDetail.GetDataRow(e.RowHandle)["price"]) == 0
                    || Convert.ToDecimal(gridViewDetail.GetDataRow(e.RowHandle)["count"]) == 0))
                {
                    e.Allow = e.RowHandle < 0;
                    if (!e.Allow)
                        spinEditCount.ErrorText = "Тоо, хэмжээг оруулна уу";
                }
                else if (this.Visible && e.RowHandle >= 0 && (gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != DBNull.Value 
                    && Convert.ToDecimal(gridViewDetail.GetRowCellValue(e.RowHandle, "price")) != 0))
                {
                    bool isAllow = true;
                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        if (e.RowHandle != i && gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != null && gridViewDetail.GetRowCellValue(i, "medicineID") != null &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != DBNull.Value && gridViewDetail.GetRowCellValue(i, "medicineID") != DBNull.Value &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID").ToString().Equals(gridViewDetail.GetRowCellValue(i, "medicineID").ToString()) &&
                            Convert.ToDecimal(gridViewDetail.GetRowCellValue(e.RowHandle, "price")) == Convert.ToDecimal(gridViewDetail.GetRowCellValue(i, "price")))
                        {
                            gridLookUpEditMedicine.ErrorText = "Эм, үнэ давхацсан байна";
                            spinEditPrice.ErrorText = "Эм, үнэ давхацсан байна";
                            spinEditPrice.Focus();
                            isAllow = false;
                            break;
                        }
                    }
                    e.Allow = isAllow;
                }
            }

            private void gridViewDetail_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                gridLookUpEditMedicine.Focus();
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridViewDetail_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
            {
                if(Visible)
                    setValueFromGrid();
            }


            private void gridLookUpEditMedicine_EditValueChanged(object sender, EventArgs e)
            {
                DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                if (gridLookUpEditMedicine.EditValue != null && gridLookUpEditMedicine.EditValue.ToString() != "" && view != null)
                {
                    textEditBarcode.Text = view["barcode"].ToString();
                    if(spinEditPrice.Value == 0)
                    {
                        spinEditPrice.Value = Convert.ToDecimal(view["price"]);
                    }
                }
            }

            private void gridLookUpEditMedicine_Validating(object sender, CancelEventArgs e)
            {
                //checkMedicine();
                if (!e.Cancel && gridLookUpEditMedicine.EditValue != DBNull.Value && gridLookUpEditMedicine.EditValue != null)
                {
                    endEdit();
                }
            }

            private void gridLookUpEditMedicine_InvalidValue(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridLookUpEditMedicine_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    MedicineInUpForm.Instance.showForm(3, Instance);
                }   
            }


            private void textEditBarcode_EditValueChanged(object sender, EventArgs e)
            {
                if (! (textEditBarcode.Text = textEditBarcode.Text.Trim()).Equals(""))
                {
                    for (int i = 0; i < JournalDataSet.Instance.MedicineOther.Rows.Count; i++)
                    {
                        if (JournalDataSet.Instance.MedicineOther.Rows[i]["barcode"].ToString().Equals(textEditBarcode.Text))
                        {
                            gridLookUpEditMedicine.EditValue = JournalDataSet.Instance.MedicineOther.Rows[i]["id"];
                            spinEditCount.Focus();
                            break;
                        }
                    }
                }
            }


            private void spinEditCount_ValueChanged(object sender, EventArgs e)
            {
                calcPrice();
            }

            private void spinEditCount_Leave(object sender, EventArgs e)
            {
                endEdit();
            }

            private void spinEditCount_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    gridControlDetail.Focus();
                }
            }

            
            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload(true);
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private void reload(bool isReload)
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN);
                if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                    JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                    JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
                if (isReload || InformationDataSet.Instance.IncTypeOther.Rows.Count == 0)
                    InformationDataSet.IncTypeOtherTableAdapter.Fill(InformationDataSet.Instance.IncTypeOther, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.MedicineOther.Rows.Count == 0)
                    JournalDataSet.MedicineOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOther, HospitalUtil.getHospitalId(), "");
                if (isReload || UserDataSet.Instance.Producer.Rows.Count == 0)
                    UserDataSet.ProducerTableAdapter.Fill(UserDataSet.Instance.Producer, HospitalUtil.getHospitalId());
                //ProgramUtil.closeWaitDialog();
            }

            private void setValueFromGrid()
            {
                DataRowView view = (DataRowView)JournalDataSet.IncDetailBindingSource.Current;
                if (view != null)
                {
                    spinEditPrice.EditValue = view["price"];
                    spinEditCount.EditValue = view["count"];
                    spinEditSumPrice.EditValue = view["sumPrice"];

                    DataRowView row = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                    if (row != null)
                    {
                        textEditBarcode.Text = row["barcode"].ToString();
                    }
                }
            }

            private void endEdit()
            {
                DataRowView row = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                DataRowView view = (DataRowView)JournalDataSet.IncDetailBindingSource.Current;
                if (row == null || view == null)
                    return;

                view["medicineID"] = gridLookUpEditMedicine.EditValue;
                view["medicineName"] = row["name"];
                view["latinName"] = row["latinName"];
                view["unitType"] = row["unitType"];
                view["shape"] = row["shape"];
                view["validDate"] = row["validDate"];
                view["medicineTypeName"] = row["medicineTypeName"];
                view["price"] = spinEditPrice.EditValue;
                view["count"] = spinEditCount.EditValue;
                view.EndEdit();
            }

            private void calcPrice()
            {
                spinEditSumPrice.Value = spinEditCount.Value * spinEditPrice.Value;
            }

            private void changeType()
            {
                if (radioGroupType.SelectedIndex == 0)
                {
                    labelControlWarehouse.Location = new Point(67, 111);
                    treeListLookUpEditWard.Location = new Point(125, 4);
                    treeListLookUpEditWarehouse.Location = new Point(125, 110);

                    treeListLookUpEditWard.Visible = false;
                    treeListLookUpEditWarehouse.Visible = true;
                    labelControlWarehouse.Text = "Агуулах*:";
                }
                else if (radioGroupType.SelectedIndex == 1)
                {
                    labelControlWarehouse.Location = new Point(81, 111);
                    treeListLookUpEditWard.Location = new Point(125, 110);
                    treeListLookUpEditWarehouse.Location = new Point(125, 4);

                    treeListLookUpEditWard.Visible = true;
                    treeListLookUpEditWarehouse.Visible = false;
                    labelControlWarehouse.Text = "Тасаг*:";
                }
            }


            private void addRow()
            {
                JournalDataSet.Instance.IncDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                DataRowView view = (DataRowView)JournalDataSet.IncDetailBindingSource.AddNew();
                reload(true);
                clearMedicine();
                gridViewDetail.Focus();
            }

            private void deleteRow()
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн орлого устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    JournalDataSet.IncDetailBindingSource.RemoveCurrent();
                    setValueFromGrid();
                }
            }

            private bool check()
            {
                //Instance.Validate();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.EditValue.ToString().Equals(""))
                {
                    errorText = "Няравыг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
                if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.EditValue.ToString().Equals("")))
                {
                    errorText = "Агуулахыг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWarehouse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWarehouse.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.EditValue.ToString().Equals("")))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }
                if (dateEditIncDate.EditValue == DBNull.Value || dateEditIncDate.EditValue == null || dateEditIncDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditIncDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditIncDate.Focus();
                    }
                }
                if (gridLookUpEditIncType.EditValue == DBNull.Value || gridLookUpEditIncType.EditValue == null || gridLookUpEditIncType.EditValue.ToString().Equals(""))
                {
                    errorText = "Орлогын төрлийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditIncType.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditIncType.Focus();
                    }
                }

                if (isRight)
                    isRight = checkMedicine();
                else
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private bool checkMedicine()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
                if (spinEditPrice.Value == 0)
                {
                    errorText = "Үнийг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditPrice.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditPrice.Focus();
                    }
                }
                if (spinEditCount.Value == 0)
                {
                    errorText = "Тоо, хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditCount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditCount.Focus();
                    }
                }
                for (int i = 0; isRight && i < gridViewDetail.RowCount; i++)
                {
                    if (gridViewDetail.FocusedRowHandle != i && gridLookUpEditMedicine.EditValue != null
                        && gridLookUpEditMedicine.EditValue.ToString().Equals(gridViewDetail.GetRowCellValue(i, "medicineID").ToString())
                        && spinEditPrice.Value == Convert.ToDecimal(gridViewDetail.GetRowCellValue(i, "price")))
                    {
                        errorText = "Эм, үнэ давхацсан байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditMedicine.ErrorText = errorText;
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                        break;
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();

                    if (radioGroupType.SelectedIndex == 0)
                    {
                        MedicineIncForm.Instance.currView["wardID"] = null;
                        MedicineIncForm.Instance.currView["warehouseName"] = warehouse["name"];
                        MedicineIncForm.Instance.currView["warehouseDesc"] = warehouse["description"];
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {
                        MedicineIncForm.Instance.currView["warehouseID"] = null;
                        MedicineIncForm.Instance.currView["warehouseName"] = ward["name"];
                        MedicineIncForm.Instance.currView["warehouseDesc"] = ward["description"];
                    }

                    MedicineIncForm.Instance.currView["userName"] = user["fullName"];
                    MedicineIncForm.Instance.currView["roleName"] = user["roleName"];
                    MedicineIncForm.Instance.currView["producerName"] = gridLookUpEditProducer.Text;
                    MedicineIncForm.Instance.currView["incTypeName"] = gridLookUpEditIncType.Text;

                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        DataRow row = gridViewDetail.GetDataRow(i);
                        row["date"] = dateEditIncDate.EditValue;
                        row["docNumber"] = textEditDocNumber.EditValue;
                        row["description"] = memoEditDescription.EditValue;
                        row["userID"] = gridLookUpEditUser.EditValue;
                        row["type"] = radioGroupType.SelectedIndex;
                        if (radioGroupType.SelectedIndex == 0)
                        {
                            row["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                            row["wardID"] = null;
                            row["warehouseName"] = warehouse["name"];
                            row["warehouseDesc"] = warehouse["description"];
                        }
                        else if (radioGroupType.SelectedIndex == 1)
                        {
                            row["warehouseID"] = null;
                            row["wardID"] = treeListLookUpEditWard.EditValue;
                            row["warehouseName"] = ward["name"];
                            row["warehouseDesc"] = ward["description"];
                        }
                        row["userName"] = user["fullName"];
                        row["roleName"] = user["roleName"];
                        row["producerID"] = gridLookUpEditProducer.EditValue;
                        row["producerName"] = gridLookUpEditProducer.Text;
                        row["incTypeID"] = gridLookUpEditIncType.EditValue;
                        row["incTypeName"] = gridLookUpEditIncType.Text;
                    }

                    JournalDataSet.IncJournalBindingSource.EndEdit();
                    JournalDataSet.IncJournalTableAdapter.Update(JournalDataSet.Instance.IncJournal);

                    JournalDataSet.IncDetailBindingSource.EndEdit();
                    JournalDataSet.IncDetailTableAdapter.Update(JournalDataSet.Instance.IncDetail);

                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }


            // Шинээр нэмсэн эмийг авч байна
            public void addMedicine(DataRowView view)
            {
                DataRowView row = (DataRowView)JournalDataSet.MedicineOtherBindingSource.AddNew();
                row["id"] = view["id"];
                row["name"] = view["name"];
                row["latinName"] = view["latinName"];
                row["unitType"] = view["unitName"];
                row["price"] = view["price"];
                row["medicineTypeName"] = view["medicineTypeName"];
                row["barcode"] = view["barcode"];
                row["shape"] = view["shapeName"];
                row["validDate"] = view["validDate"];
                row.EndEdit();
                gridLookUpEditMedicine.EditValue = view["id"];
            }

            // Шинээр нэмсэн бэлгэн нийлүүлэгчийг авч байна
            public void addProducer(string id)
            {
                gridLookUpEditProducer.EditValue = id;
            }

        #endregion

    }
}