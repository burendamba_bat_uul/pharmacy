﻿namespace Pharmacy2016.gui.core.journal.mot
{
    partial class MedicineMotForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicineMotForm));
            this.gridViewMotDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlMot = new DevExpress.XtraGrid.GridControl();
            this.gridViewMot = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnUserRole = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnUserName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridViewDetail = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn34 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn35 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLeft = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.treeListWard = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnWard = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemMot = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEndBal = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.radioGroupJournal = new DevExpress.XtraEditors.RadioGroup();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMotDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            this.dockPanelLeft.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupJournal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewMotDetail
            // 
            this.gridViewMotDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn1,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            this.gridViewMotDetail.GridControl = this.gridControlMot;
            this.gridViewMotDetail.LevelIndent = 1;
            this.gridViewMotDetail.Name = "gridViewMotDetail";
            this.gridViewMotDetail.OptionsBehavior.ReadOnly = true;
            this.gridViewMotDetail.OptionsView.ShowFooter = true;
            this.gridViewMotDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Эмийн нэр";
            this.gridColumn4.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn4.FieldName = "medicineName";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 150;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "ОУ нэр";
            this.gridColumn5.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn5.FieldName = "latinName";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            this.gridColumn5.Width = 159;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Ангилал";
            this.gridColumn2.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn2.FieldName = "medicineTypeName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 109;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тоо";
            this.gridColumn6.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn6.CustomizationCaption = "Тоо";
            this.gridColumn6.FieldName = "count";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 7;
            this.gridColumn6.Width = 80;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Үнэ";
            this.gridColumn7.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn7.CustomizationCaption = "Үнэ";
            this.gridColumn7.FieldName = "price";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 8;
            this.gridColumn7.Width = 71;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Нийт үнэ";
            this.gridColumn8.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn8.CustomizationCaption = "Нийт үнэ";
            this.gridColumn8.FieldName = "sumPrice";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 9;
            this.gridColumn8.Width = 116;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Тун хэмжээ";
            this.gridColumn1.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn1.FieldName = "unitType";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 6;
            this.gridColumn1.Width = 73;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Хэлбэр";
            this.gridColumn9.CustomizationCaption = "Хэлбэр";
            this.gridColumn9.FieldName = "shape";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Хүчинтэй хугацаа";
            this.gridColumn10.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn10.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn10.FieldName = "validDate";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 4;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Орлогын төрөл";
            this.gridColumn11.CustomizationCaption = "Орлогын төрөл";
            this.gridColumn11.FieldName = "incTypeName";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            // 
            // gridControlMot
            // 
            this.gridControlMot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMot.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlMot.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlMot.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlMot.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlMot.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlMot.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "update")});
            this.gridControlMot.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlMot_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.LevelTemplate = this.gridViewMotDetail;
            gridLevelNode1.RelationName = "MotJournal_MotDetail";
            this.gridControlMot.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlMot.Location = new System.Drawing.Point(2, 52);
            this.gridControlMot.MainView = this.gridViewMot;
            this.gridControlMot.Name = "gridControlMot";
            this.gridControlMot.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemSpinEdit1});
            this.gridControlMot.Size = new System.Drawing.Size(780, 407);
            this.gridControlMot.TabIndex = 1;
            this.gridControlMot.UseEmbeddedNavigator = true;
            this.gridControlMot.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMot,
            this.gridViewDetail,
            this.gridViewMotDetail});
            // 
            // gridViewMot
            // 
            this.gridViewMot.Appearance.OddRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.gridViewMot.Appearance.OddRow.Options.UseForeColor = true;
            this.gridViewMot.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand11,
            this.gridBand1,
            this.gridBand12,
            this.gridBand9,
            this.gridBand3});
            this.gridViewMot.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn5,
            this.gridColumn3,
            this.bandedGridColumn2,
            this.bandedGridColumn8,
            this.bandedGridColumn28,
            this.bandedGridColumn29,
            this.gridColumnUserRole,
            this.gridColumnUserName,
            this.bandedGridColumn23,
            this.bandedGridColumn30,
            this.bandedGridColumn31,
            this.bandedGridColumn24,
            this.bandedGridColumn4,
            this.bandedGridColumn22,
            this.bandedGridColumn18});
            this.gridViewMot.GridControl = this.gridControlMot;
            this.gridViewMot.LevelIndent = 0;
            this.gridViewMot.Name = "gridViewMot";
            this.gridViewMot.OptionsBehavior.ReadOnly = true;
            this.gridViewMot.OptionsDetail.ShowDetailTabs = false;
            this.gridViewMot.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewMot.OptionsPrint.PrintDetails = true;
            this.gridViewMot.OptionsView.ShowAutoFilterRow = true;
            this.gridViewMot.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Баримт";
            this.gridBand2.Columns.Add(this.bandedGridColumn5);
            this.gridBand2.Columns.Add(this.gridColumn3);
            this.gridBand2.Columns.Add(this.bandedGridColumn2);
            this.gridBand2.CustomizationCaption = "Баримт";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 259;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.Caption = "Дугаар";
            this.bandedGridColumn5.CustomizationCaption = "Дугаар";
            this.bandedGridColumn5.FieldName = "id";
            this.bandedGridColumn5.MinWidth = 75;
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn5.Visible = true;
            this.bandedGridColumn5.Width = 90;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Огноо";
            this.gridColumn3.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn3.CustomizationCaption = "Огноо";
            this.gridColumn3.FieldName = "date";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            this.gridColumn3.Width = 94;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "Дагалдах баримт";
            this.bandedGridColumn2.CustomizationCaption = "Дагалдах баримт";
            this.bandedGridColumn2.FieldName = "docNumber";
            this.bandedGridColumn2.MinWidth = 75;
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.Visible = true;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand11.AppearanceHeader.Options.UseFont = true;
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "Нярав";
            this.gridBand11.Columns.Add(this.bandedGridColumn8);
            this.gridBand11.Columns.Add(this.bandedGridColumn28);
            this.gridBand11.Columns.Add(this.bandedGridColumn29);
            this.gridBand11.CustomizationCaption = "Нярав";
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 1;
            this.gridBand11.Width = 182;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.Caption = "Дугаар";
            this.bandedGridColumn8.CustomizationCaption = "Шилжүүлэх агуулахын дугаар";
            this.bandedGridColumn8.FieldName = "userID";
            this.bandedGridColumn8.MinWidth = 75;
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.Visible = true;
            this.bandedGridColumn8.Width = 99;
            // 
            // bandedGridColumn28
            // 
            this.bandedGridColumn28.Caption = "Нэр";
            this.bandedGridColumn28.CustomizationCaption = "Хэрэглэгчийн нэр";
            this.bandedGridColumn28.FieldName = "userName";
            this.bandedGridColumn28.MinWidth = 75;
            this.bandedGridColumn28.Name = "bandedGridColumn28";
            this.bandedGridColumn28.Visible = true;
            this.bandedGridColumn28.Width = 83;
            // 
            // bandedGridColumn29
            // 
            this.bandedGridColumn29.Caption = "Эрх";
            this.bandedGridColumn29.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.bandedGridColumn29.FieldName = "roleName";
            this.bandedGridColumn29.MinWidth = 75;
            this.bandedGridColumn29.Name = "bandedGridColumn29";
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Шилжүүлэх агуулах";
            this.gridBand1.Columns.Add(this.gridColumnUserRole);
            this.gridBand1.Columns.Add(this.gridColumnUserName);
            this.gridBand1.CustomizationCaption = "Шилжүүлэх агуулах";
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 2;
            this.gridBand1.Width = 187;
            // 
            // gridColumnUserRole
            // 
            this.gridColumnUserRole.Caption = "Нэр";
            this.gridColumnUserRole.CustomizationCaption = "Шилжүүлэх агуулахын нэр";
            this.gridColumnUserRole.FieldName = "warehouseName";
            this.gridColumnUserRole.MinWidth = 75;
            this.gridColumnUserRole.Name = "gridColumnUserRole";
            this.gridColumnUserRole.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumnUserRole.Visible = true;
            this.gridColumnUserRole.Width = 91;
            // 
            // gridColumnUserName
            // 
            this.gridColumnUserName.Caption = "Дэлгэрэнгүй";
            this.gridColumnUserName.CustomizationCaption = "Шилжүүлэх агуулахын дэлгэрэнгүй";
            this.gridColumnUserName.FieldName = "warehouseDesc";
            this.gridColumnUserName.MinWidth = 75;
            this.gridColumnUserName.Name = "gridColumnUserName";
            this.gridColumnUserName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumnUserName.Visible = true;
            this.gridColumnUserName.Width = 96;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand12.AppearanceHeader.Options.UseFont = true;
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "Хүлээн авах нярав";
            this.gridBand12.Columns.Add(this.bandedGridColumn23);
            this.gridBand12.Columns.Add(this.bandedGridColumn30);
            this.gridBand12.Columns.Add(this.bandedGridColumn31);
            this.gridBand12.CustomizationCaption = "Хүлээн авах нярав";
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 3;
            this.gridBand12.Width = 177;
            // 
            // bandedGridColumn23
            // 
            this.bandedGridColumn23.Caption = "Дугаар";
            this.bandedGridColumn23.CustomizationCaption = "Хүлээн авах хэрэглэгчийн дугаар";
            this.bandedGridColumn23.FieldName = "incUserID";
            this.bandedGridColumn23.MinWidth = 75;
            this.bandedGridColumn23.Name = "bandedGridColumn23";
            this.bandedGridColumn23.Visible = true;
            this.bandedGridColumn23.Width = 88;
            // 
            // bandedGridColumn30
            // 
            this.bandedGridColumn30.Caption = "Нэр";
            this.bandedGridColumn30.CustomizationCaption = "Хүлээн авах хэрэглэгчийн нэр";
            this.bandedGridColumn30.FieldName = "incUserName";
            this.bandedGridColumn30.MinWidth = 75;
            this.bandedGridColumn30.Name = "bandedGridColumn30";
            this.bandedGridColumn30.Visible = true;
            this.bandedGridColumn30.Width = 89;
            // 
            // bandedGridColumn31
            // 
            this.bandedGridColumn31.Caption = "Эрх";
            this.bandedGridColumn31.CustomizationCaption = "Хүлээн авах хэрэглэгчийн эрх";
            this.bandedGridColumn31.FieldName = "incRoleName";
            this.bandedGridColumn31.MinWidth = 75;
            this.bandedGridColumn31.Name = "bandedGridColumn31";
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand9.AppearanceHeader.Options.UseFont = true;
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "Хүлээн авах агуулах";
            this.gridBand9.Columns.Add(this.bandedGridColumn24);
            this.gridBand9.Columns.Add(this.bandedGridColumn4);
            this.gridBand9.CustomizationCaption = "Хүлээн авах агуулах";
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 4;
            this.gridBand9.Width = 194;
            // 
            // bandedGridColumn24
            // 
            this.bandedGridColumn24.Caption = "Нэр";
            this.bandedGridColumn24.CustomizationCaption = "Хүлээн авах агуулахын нэр";
            this.bandedGridColumn24.FieldName = "incWarehouseName";
            this.bandedGridColumn24.MinWidth = 75;
            this.bandedGridColumn24.Name = "bandedGridColumn24";
            this.bandedGridColumn24.Visible = true;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn4.CustomizationCaption = "Хүлээн авах агуулахын дэлгэрэнгүй";
            this.bandedGridColumn4.FieldName = "incWarehouseDesc";
            this.bandedGridColumn4.MinWidth = 75;
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 119;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Нэмэлт мэдээлэл";
            this.gridBand3.Columns.Add(this.bandedGridColumn18);
            this.gridBand3.Columns.Add(this.bandedGridColumn22);
            this.gridBand3.CustomizationCaption = "Нэмэлт мэдээлэл";
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 5;
            this.gridBand3.Width = 150;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.Caption = "Захиалгын дугаар";
            this.bandedGridColumn18.CustomizationCaption = "Захиалгын дугаар";
            this.bandedGridColumn18.FieldName = "medicineOrderID";
            this.bandedGridColumn18.MinWidth = 75;
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            this.bandedGridColumn18.Visible = true;
            // 
            // bandedGridColumn22
            // 
            this.bandedGridColumn22.Caption = "Тайлбар";
            this.bandedGridColumn22.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn22.FieldName = "description";
            this.bandedGridColumn22.MinWidth = 75;
            this.bandedGridColumn22.Name = "bandedGridColumn22";
            this.bandedGridColumn22.Visible = true;
            // 
            // gridViewDetail
            // 
            this.gridViewDetail.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand5,
            this.gridBand13,
            this.gridBand10,
            this.gridBand14,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8});
            this.gridViewDetail.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn14,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn32,
            this.bandedGridColumn33,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn11,
            this.bandedGridColumn13,
            this.bandedGridColumn3,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn17,
            this.bandedGridColumnCount,
            this.bandedGridColumn19,
            this.bandedGridColumnSumPrice,
            this.bandedGridColumn25,
            this.bandedGridColumn34,
            this.bandedGridColumn35,
            this.bandedGridColumn26,
            this.bandedGridColumn27,
            this.bandedGridColumn1,
            this.bandedGridColumn12,
            this.bandedGridColumn21,
            this.bandedGridColumn20});
            this.gridViewDetail.GridControl = this.gridControlMot;
            this.gridViewDetail.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", this.bandedGridColumnCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", this.bandedGridColumnSumPrice, "{0:c2}")});
            this.gridViewDetail.LevelIndent = 0;
            this.gridViewDetail.Name = "gridViewDetail";
            this.gridViewDetail.OptionsBehavior.ReadOnly = true;
            this.gridViewDetail.OptionsDetail.ShowDetailTabs = false;
            this.gridViewDetail.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewDetail.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDetail.OptionsView.ShowFooter = true;
            this.gridViewDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewDetail.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridViewDetail_CustomDrawGroupRow);
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Баримт";
            this.gridBand4.Columns.Add(this.bandedGridColumn14);
            this.gridBand4.Columns.Add(this.bandedGridColumn6);
            this.gridBand4.Columns.Add(this.bandedGridColumn13);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 259;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.Caption = "Дугаар";
            this.bandedGridColumn14.CustomizationCaption = "Дугаар";
            this.bandedGridColumn14.FieldName = "motJournalID";
            this.bandedGridColumn14.MinWidth = 75;
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn14.Visible = true;
            this.bandedGridColumn14.Width = 90;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "Огноо";
            this.bandedGridColumn6.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumn6.CustomizationCaption = "Огноо";
            this.bandedGridColumn6.FieldName = "date";
            this.bandedGridColumn6.MinWidth = 75;
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn6.Visible = true;
            this.bandedGridColumn6.Width = 94;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.Caption = "Дагалдах баримт";
            this.bandedGridColumn13.CustomizationCaption = "Дагалдах баримт";
            this.bandedGridColumn13.FieldName = "docNumber";
            this.bandedGridColumn13.MinWidth = 75;
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.Visible = true;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Нярав";
            this.gridBand5.Columns.Add(this.bandedGridColumn7);
            this.gridBand5.Columns.Add(this.bandedGridColumn32);
            this.gridBand5.Columns.Add(this.bandedGridColumn33);
            this.gridBand5.CustomizationCaption = "Нярав";
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 1;
            this.gridBand5.Width = 166;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "Дугаар";
            this.bandedGridColumn7.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.bandedGridColumn7.FieldName = "userID";
            this.bandedGridColumn7.MinWidth = 75;
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 91;
            // 
            // bandedGridColumn32
            // 
            this.bandedGridColumn32.Caption = "Нэр";
            this.bandedGridColumn32.CustomizationCaption = "Хэрэглэгчийн нэр";
            this.bandedGridColumn32.FieldName = "userName";
            this.bandedGridColumn32.MinWidth = 75;
            this.bandedGridColumn32.Name = "bandedGridColumn32";
            this.bandedGridColumn32.Visible = true;
            // 
            // bandedGridColumn33
            // 
            this.bandedGridColumn33.Caption = "Эрх";
            this.bandedGridColumn33.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.bandedGridColumn33.FieldName = "roleName";
            this.bandedGridColumn33.MinWidth = 75;
            this.bandedGridColumn33.Name = "bandedGridColumn33";
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand13.AppearanceHeader.Options.UseFont = true;
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "Агуулах";
            this.gridBand13.Columns.Add(this.bandedGridColumn9);
            this.gridBand13.Columns.Add(this.bandedGridColumn10);
            this.gridBand13.CustomizationCaption = "Агуулах";
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 2;
            this.gridBand13.Width = 187;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "Нэр";
            this.bandedGridColumn9.CustomizationCaption = "Агуулахын нэр";
            this.bandedGridColumn9.FieldName = "warehouseName";
            this.bandedGridColumn9.MinWidth = 75;
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.Visible = true;
            this.bandedGridColumn9.Width = 91;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn10.CustomizationCaption = "Агуулахын дэлгэрэнгүй";
            this.bandedGridColumn10.FieldName = "warehouseDesc";
            this.bandedGridColumn10.MinWidth = 75;
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn10.Visible = true;
            this.bandedGridColumn10.Width = 96;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand10.AppearanceHeader.Options.UseFont = true;
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "Хүлээн авах нярав";
            this.gridBand10.Columns.Add(this.bandedGridColumn25);
            this.gridBand10.Columns.Add(this.bandedGridColumn34);
            this.gridBand10.Columns.Add(this.bandedGridColumn35);
            this.gridBand10.CustomizationCaption = "Хүлээн авах нярав";
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 3;
            this.gridBand10.Width = 150;
            // 
            // bandedGridColumn25
            // 
            this.bandedGridColumn25.Caption = "Дугаар";
            this.bandedGridColumn25.CustomizationCaption = "Хүлээн авах хэрэглэгчийн дугаар";
            this.bandedGridColumn25.FieldName = "incUserID";
            this.bandedGridColumn25.MinWidth = 75;
            this.bandedGridColumn25.Name = "bandedGridColumn25";
            this.bandedGridColumn25.Visible = true;
            // 
            // bandedGridColumn34
            // 
            this.bandedGridColumn34.Caption = "Нэр";
            this.bandedGridColumn34.CustomizationCaption = "Хүлээн авах хэрэглэгчийн нэр";
            this.bandedGridColumn34.FieldName = "incUserName";
            this.bandedGridColumn34.MinWidth = 75;
            this.bandedGridColumn34.Name = "bandedGridColumn34";
            this.bandedGridColumn34.Visible = true;
            // 
            // bandedGridColumn35
            // 
            this.bandedGridColumn35.Caption = "Эрх";
            this.bandedGridColumn35.CustomizationCaption = "Хүлээн авах хэрэглэгчийн эрх";
            this.bandedGridColumn35.FieldName = "incRoleName";
            this.bandedGridColumn35.MinWidth = 75;
            this.bandedGridColumn35.Name = "bandedGridColumn35";
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand14.AppearanceHeader.Options.UseFont = true;
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "Хүлээн авах агуулах";
            this.gridBand14.Columns.Add(this.bandedGridColumn26);
            this.gridBand14.Columns.Add(this.bandedGridColumn27);
            this.gridBand14.CustomizationCaption = "Хүлээн авах агуулах";
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 4;
            this.gridBand14.Width = 150;
            // 
            // bandedGridColumn26
            // 
            this.bandedGridColumn26.Caption = "Нэр";
            this.bandedGridColumn26.CustomizationCaption = "Хүлээн авах агуулахын нэр";
            this.bandedGridColumn26.FieldName = "incWarehouseName";
            this.bandedGridColumn26.MinWidth = 75;
            this.bandedGridColumn26.Name = "bandedGridColumn26";
            this.bandedGridColumn26.Visible = true;
            // 
            // bandedGridColumn27
            // 
            this.bandedGridColumn27.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn27.CustomizationCaption = "Хүлээн авах агуулахын дэлгэрэнгүй";
            this.bandedGridColumn27.FieldName = "incWarehouseDesc";
            this.bandedGridColumn27.MinWidth = 75;
            this.bandedGridColumn27.Name = "bandedGridColumn27";
            this.bandedGridColumn27.Visible = true;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand6.AppearanceHeader.Options.UseFont = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Нэмэлт мэдээлэл";
            this.gridBand6.Columns.Add(this.bandedGridColumn20);
            this.gridBand6.Columns.Add(this.bandedGridColumn11);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 5;
            this.gridBand6.Width = 194;
            // 
            // bandedGridColumn20
            // 
            this.bandedGridColumn20.Caption = "Захиалгын дугаар";
            this.bandedGridColumn20.CustomizationCaption = "Захиалгын дугаар";
            this.bandedGridColumn20.FieldName = "medicineOrderID";
            this.bandedGridColumn20.MinWidth = 75;
            this.bandedGridColumn20.Name = "bandedGridColumn20";
            this.bandedGridColumn20.Visible = true;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.Caption = "Тайлбар";
            this.bandedGridColumn11.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn11.FieldName = "description";
            this.bandedGridColumn11.MinWidth = 75;
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn11.Visible = true;
            this.bandedGridColumn11.Width = 119;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand7.AppearanceHeader.Options.UseFont = true;
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "Эмийн мэдээлэл";
            this.gridBand7.Columns.Add(this.bandedGridColumn3);
            this.gridBand7.Columns.Add(this.bandedGridColumn15);
            this.gridBand7.Columns.Add(this.bandedGridColumn16);
            this.gridBand7.Columns.Add(this.bandedGridColumn21);
            this.gridBand7.Columns.Add(this.bandedGridColumn12);
            this.gridBand7.Columns.Add(this.bandedGridColumn1);
            this.gridBand7.Columns.Add(this.bandedGridColumn17);
            this.gridBand7.CustomizationCaption = "Эмийн мэдээлэл";
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 6;
            this.gridBand7.Width = 525;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.Caption = "Эмийн нэр";
            this.bandedGridColumn3.CustomizationCaption = "Худалдааны нэр";
            this.bandedGridColumn3.FieldName = "medicineName";
            this.bandedGridColumn3.MinWidth = 75;
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.Visible = true;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.Caption = "ОУ нэр";
            this.bandedGridColumn15.CustomizationCaption = "Олон улсын нэр";
            this.bandedGridColumn15.FieldName = "latinName";
            this.bandedGridColumn15.MinWidth = 75;
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.Visible = true;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.Caption = "Ангилал";
            this.bandedGridColumn16.CustomizationCaption = "Ангилал";
            this.bandedGridColumn16.FieldName = "medicineTypeName";
            this.bandedGridColumn16.MinWidth = 75;
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.Visible = true;
            // 
            // bandedGridColumn21
            // 
            this.bandedGridColumn21.Caption = "Орлогын төрөл";
            this.bandedGridColumn21.CustomizationCaption = "Орлогын төрөл";
            this.bandedGridColumn21.FieldName = "incTypeName";
            this.bandedGridColumn21.MinWidth = 75;
            this.bandedGridColumn21.Name = "bandedGridColumn21";
            this.bandedGridColumn21.Visible = true;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.Caption = "Хүчинтэй хугацаа";
            this.bandedGridColumn12.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumn12.CustomizationCaption = "Хүчинтэй хугацаа";
            this.bandedGridColumn12.FieldName = "validDate";
            this.bandedGridColumn12.MinWidth = 75;
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.Visible = true;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "Хэлбэр";
            this.bandedGridColumn1.CustomizationCaption = "Хэлбэр";
            this.bandedGridColumn1.FieldName = "shape";
            this.bandedGridColumn1.MinWidth = 75;
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.Visible = true;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.Caption = "Тун хэмжээ";
            this.bandedGridColumn17.CustomizationCaption = "Тун хэмжээ";
            this.bandedGridColumn17.FieldName = "unitType";
            this.bandedGridColumn17.MinWidth = 75;
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.Visible = true;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "Нийт дүн";
            this.gridBand8.Columns.Add(this.bandedGridColumnCount);
            this.gridBand8.Columns.Add(this.bandedGridColumn19);
            this.gridBand8.Columns.Add(this.bandedGridColumnSumPrice);
            this.gridBand8.CustomizationCaption = "Нийт дүн";
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 7;
            this.gridBand8.Width = 225;
            // 
            // bandedGridColumnCount
            // 
            this.bandedGridColumnCount.Caption = "Тоо";
            this.bandedGridColumnCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnCount.CustomizationCaption = "Тоо";
            this.bandedGridColumnCount.FieldName = "count";
            this.bandedGridColumnCount.MinWidth = 75;
            this.bandedGridColumnCount.Name = "bandedGridColumnCount";
            this.bandedGridColumnCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.bandedGridColumnCount.Visible = true;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.Caption = "Үнэ";
            this.bandedGridColumn19.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn19.CustomizationCaption = "Үнэ";
            this.bandedGridColumn19.FieldName = "price";
            this.bandedGridColumn19.MinWidth = 75;
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.Visible = true;
            // 
            // bandedGridColumnSumPrice
            // 
            this.bandedGridColumnSumPrice.Caption = "Нийт үнэ";
            this.bandedGridColumnSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnSumPrice.CustomizationCaption = "Нийт үнэ";
            this.bandedGridColumnSumPrice.FieldName = "sumPrice";
            this.bandedGridColumnSumPrice.MinWidth = 75;
            this.bandedGridColumnSumPrice.Name = "bandedGridColumnSumPrice";
            this.bandedGridColumnSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.bandedGridColumnSumPrice.Visible = true;
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLeft});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanelLeft
            // 
            this.dockPanelLeft.Controls.Add(this.dockPanel1_Container);
            this.dockPanelLeft.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelLeft.ID = new System.Guid("32690437-4757-43e6-b1e2-053573c2cee2");
            this.dockPanelLeft.Location = new System.Drawing.Point(0, 0);
            this.dockPanelLeft.Name = "dockPanelLeft";
            this.dockPanelLeft.Options.ShowCloseButton = false;
            this.dockPanelLeft.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelLeft.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelLeft.SavedIndex = 0;
            this.dockPanelLeft.Size = new System.Drawing.Size(200, 461);
            this.dockPanelLeft.Text = "Тасаг";
            this.dockPanelLeft.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.treeListWard);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(192, 434);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // treeListWard
            // 
            this.treeListWard.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnWard});
            this.treeListWard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListWard.Location = new System.Drawing.Point(0, 0);
            this.treeListWard.Name = "treeListWard";
            this.treeListWard.BeginUnboundLoad();
            this.treeListWard.AppendNode(new object[] {
            "Хавдар судлалын үндэсний төв"}, -1);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 1"}, 0);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 2"}, 0);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 3"}, 0);
            this.treeListWard.EndUnboundLoad();
            this.treeListWard.OptionsBehavior.Editable = false;
            this.treeListWard.OptionsBehavior.ExpandNodeOnDrag = false;
            this.treeListWard.OptionsView.ShowColumns = false;
            this.treeListWard.OptionsView.ShowIndicator = false;
            this.treeListWard.Size = new System.Drawing.Size(192, 434);
            this.treeListWard.TabIndex = 0;
            // 
            // treeListColumnWard
            // 
            this.treeListColumnWard.Caption = "Тасаг";
            this.treeListColumnWard.FieldName = "ward";
            this.treeListColumnWard.MinWidth = 52;
            this.treeListColumnWard.Name = "treeListColumnWard";
            this.treeListColumnWard.OptionsColumn.ReadOnly = true;
            this.treeListColumnWard.Visible = true;
            this.treeListColumnWard.VisibleIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridControlMot);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 461);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButtonLayout);
            this.panelControl2.Controls.Add(this.dropDownButtonPrint);
            this.panelControl2.Controls.Add(this.radioGroupJournal);
            this.panelControl2.Controls.Add(this.dateEditEnd);
            this.panelControl2.Controls.Add(this.dateEditStart);
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(780, 50);
            this.panelControl2.TabIndex = 1;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(747, 10);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 8;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.DropDownControl = this.popupMenu;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(611, 10);
            this.dropDownButtonPrint.MenuManager = this.barManager;
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.barManager.SetPopupContextMenu(this.dropDownButtonPrint, this.popupMenu);
            this.dropDownButtonPrint.Size = new System.Drawing.Size(130, 23);
            this.dropDownButtonPrint.TabIndex = 6;
            this.dropDownButtonPrint.Text = "Хэвлэх";
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemMot),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemEndBal),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "Хэвлэх";
            this.barButtonItemPrint.Description = "Шилжүүлгийн тайлан хэвлэх";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 0;
            this.barButtonItemPrint.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.LargeGlyph")));
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemMot
            // 
            this.barButtonItemMot.Caption = "Гүйлгээ хэвлэх";
            this.barButtonItemMot.Description = "Шилжүүлгийн гүйлгээ хэвлэх";
            this.barButtonItemMot.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemMot.Glyph")));
            this.barButtonItemMot.Id = 1;
            this.barButtonItemMot.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemMot.LargeGlyph")));
            this.barButtonItemMot.Name = "barButtonItemMot";
            this.barButtonItemMot.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemBegBal_ItemClick);
            // 
            // barButtonItemEndBal
            // 
            this.barButtonItemEndBal.Caption = "Эцсийн үлдэгдэл";
            this.barButtonItemEndBal.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.Glyph")));
            this.barButtonItemEndBal.Id = 2;
            this.barButtonItemEndBal.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.LargeGlyph")));
            this.barButtonItemEndBal.Name = "barButtonItemEndBal";
            this.barButtonItemEndBal.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemEndBal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEndBal_ItemClick);
            // 
            // barButtonItemConvert
            // 
            this.barButtonItemConvert.Caption = "Хөрвүүлэх";
            this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
            this.barButtonItemConvert.Id = 3;
            this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
            this.barButtonItemConvert.Name = "barButtonItemConvert";
            this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemPrint,
            this.barButtonItemMot,
            this.barButtonItemEndBal,
            this.barButtonItemConvert});
            this.barManager.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 461);
            this.barDockControlBottom.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 461);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(784, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 461);
            // 
            // radioGroupJournal
            // 
            this.radioGroupJournal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupJournal.Location = new System.Drawing.Point(479, 10);
            this.radioGroupJournal.Name = "radioGroupJournal";
            this.radioGroupJournal.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Баримт", true, "Journal"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Гүйлгээ", true, "Detail")});
            this.radioGroupJournal.Size = new System.Drawing.Size(126, 23);
            this.radioGroupJournal.TabIndex = 5;
            this.radioGroupJournal.SelectedIndexChanged += new System.EventHandler(this.radioGroupJournal_SelectedIndexChanged);
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(116, 12);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(100, 20);
            this.dateEditEnd.TabIndex = 3;
            this.dateEditEnd.ToolTip = "Дуусах огноо";
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(10, 12);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(100, 20);
            this.dateEditStart.TabIndex = 2;
            this.dateEditStart.ToolTip = "Эхлэх огноо";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(222, 10);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonReload.TabIndex = 4;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Шилжүүлгийн журналыг шүүх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // MedicineMotForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MedicineMotForm";
            this.ShowInTaskbar = false;
            this.Text = "Шилжүүлэгийн журнал";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MedicineMotForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMotDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            this.dockPanelLeft.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupJournal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLeft;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraTreeList.TreeList treeListWard;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnWard;
        private DevExpress.XtraGrid.GridControl gridControlMot;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMotDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewMot;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnUserRole;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnUserName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.RadioGroup radioGroupJournal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewDetail;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnSumPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn24;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemMot;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEndBal;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn21;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn29;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn30;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn32;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn33;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn34;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn35;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        
    }
}