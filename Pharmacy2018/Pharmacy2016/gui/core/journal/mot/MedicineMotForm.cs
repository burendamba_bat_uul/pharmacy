﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.report.journal.mot;



namespace Pharmacy2016.gui.core.journal.mot
{
    /*
     * Шилжүүлгийн журнал харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class MedicineMotForm : DevExpress.XtraEditors.XtraForm
    {
        
        #region Форм анх ачааллах функц

            private static MedicineMotForm INSTANCE = new MedicineMotForm();

            public static MedicineMotForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 2003;

            private bool isInit = false;

            public DataRowView currView;

            private System.IO.Stream layout_mot = null;
            private System.IO.Stream layout_detail = null;

            private MedicineMotForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initLayout();
                RoleUtil.addForm(FORM_ID, this);

                gridControlMot.DataSource = JournalDataSet.MotJournalBindingSource;

                DateTime date = DateTime.Now;
                dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1);
                dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            }

            private void initLayout()
            {
                layout_mot = new System.IO.MemoryStream();
                gridViewMot.SaveLayoutToStream(layout_mot);
                layout_mot.Seek(0, System.IO.SeekOrigin.Begin);

                layout_detail = new System.IO.MemoryStream();
                gridViewDetail.SaveLayoutToStream(layout_detail);
                layout_detail.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion
        
        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        radioGroupJournal.SelectedIndex = 0;
                        checkRole();

                        JournalDataSet.Instance.MotJournal.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        JournalDataSet.Instance.MotDetail.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        JournalDataSet.Instance.MotJournal.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        JournalDataSet.Instance.MotDetail.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        reload();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                gridControlMot.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = roles[1];
                gridControlMot.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = roles[3];
                gridControlMot.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = roles[2];
                dropDownButtonPrint.Visible = roles[4];

                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("mot");
                if (layout != null)
                {
                    gridViewMot.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewMot.RestoreLayoutFromStream(layout_mot);
                    layout_mot.Seek(0, System.IO.SeekOrigin.Begin);
                }

                layout = UserUtil.getLayout("motDetail");
                if (layout != null)
                {
                    gridViewDetail.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewDetail.RestoreLayoutFromStream(layout_detail);
                    layout_detail.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }
            
            private void MedicineMotForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                JournalDataSet.Instance.MotDetail.Clear();
                JournalDataSet.Instance.MotJournal.Clear();
            }

        #endregion

        #region Формын event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                saveLayout();
            }

            private void gridControlMot_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        add();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewMot.GetFocusedDataRow() != null)
                    {
                        try
                        {
                            delete();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "update") && gridViewMot.GetFocusedDataRow() != null)
                    {
                        update();
                    }
                }
            }

            private void gridViewDetail_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
            {
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo info = e.Info as DevExpress.XtraGrid.Views.Grid.ViewInfo.GridGroupRowInfo;
                info.GroupText = string.Format("{0} (Тоо хэмжээ = {1} Нийт үнэ = {2})", info.GroupValueText, gridViewDetail.GetGroupSummaryDisplayText(e.RowHandle, gridViewDetail.GroupSummary[0] as DevExpress.XtraGrid.GridGroupSummaryItem), gridViewDetail.GetGroupSummaryDisplayText(e.RowHandle, gridViewDetail.GroupSummary[1] as DevExpress.XtraGrid.GridGroupSummaryItem));
            }

            private void radioGroupJournal_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (radioGroupJournal.SelectedIndex == 0)
                {
                    JournalDataSet.MotDetailBindingSource.DataSource = JournalDataSet.MotJournalBindingSource;
                    JournalDataSet.MotDetailBindingSource.DataMember = "MotJournal_MotDetail";

                    gridControlMot.DataSource = JournalDataSet.MotJournalBindingSource;
                    gridControlMot.MainView = gridViewMot;

                    gridControlMot.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlMot.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridControlMot.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;
                    checkRole();
                }
                else if (radioGroupJournal.SelectedIndex == 1)
                {
                    JournalDataSet.MotDetailBindingSource.DataSource = JournalDataSet.Instance;
                    JournalDataSet.MotDetailBindingSource.DataMember = "MotDetail";

                    gridControlMot.DataSource = JournalDataSet.MotDetailBindingSource;
                    gridControlMot.MainView = gridViewDetail;

                    gridControlMot.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlMot.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridControlMot.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
                }
            }

        #endregion

        #region Формын функц

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void reload()
            {
                if(check())
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        JournalDataSet.MotJournalTableAdapter.Fill(JournalDataSet.Instance.MotJournal, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                        JournalDataSet.MotDetailTableAdapter.Fill(JournalDataSet.Instance.MotDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                        ProgramUtil.closeWaitDialog();
                    }
                    else
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        JournalDataSet.MotJournalTableAdapter.Fill(JournalDataSet.Instance.MotJournal, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                        JournalDataSet.MotDetailTableAdapter.Fill(JournalDataSet.Instance.MotDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            private void add()
            {
                MedicineMotInUpForm.Instance.showForm(1);
            }

            private void delete()
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн шилжүүлэг устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    gridViewMot.DeleteSelectedRows();
                    JournalDataSet.MotJournalBindingSource.EndEdit();
                    JournalDataSet.MotJournalTableAdapter.Update(JournalDataSet.Instance.MotJournal);
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void update()
            {
                if (gridViewMot.RowCount == 0 || gridViewMot.GetFocusedDataRow() == null)
                    return;

                currView = (DataRowView)JournalDataSet.MotJournalBindingSource.Current;
                MedicineMotInUpForm.Instance.showForm(2);
            }

            public DateTime getStartDate()
            {
                return dateEditStart.DateTime;
            }

            public DateTime getEndDate()
            {
                return dateEditEnd.DateTime;
            }

            private void saveLayout()
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    System.IO.StreamReader reader = null;
                    if (radioGroupJournal.SelectedIndex == 0)
                    {
                        gridViewMot.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        reader = new System.IO.StreamReader(str);
                        UserUtil.saveUserLayout("mot", reader.ReadToEnd());
                    }
                    else
                    {
                        gridViewDetail.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        reader = new System.IO.StreamReader(str);
                        UserUtil.saveUserLayout("motDetail", reader.ReadToEnd());
                    }
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion
        
        #region Хэвлэх, хөрвүүлэх функц

            private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemPrint.Caption;
                dropDownButtonPrint.Image = barButtonItemPrint.Glyph;

                MotReport.Instance.initAndShow(1, "");
            }

            private void barButtonItemBegBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemMot.Caption;
                dropDownButtonPrint.Image = barButtonItemMot.Glyph;

                if (gridViewMot.GetFocusedRow() != null)
                    MotReport.Instance.initAndShow(1, "id = '"+gridViewMot.GetFocusedDataRow()["id"].ToString()+"'");
                else
                    XtraMessageBox.Show("Мөр сонгоно уу");
            }

            private void barButtonItemEndBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemEndBal.Caption;
                dropDownButtonPrint.Image = barButtonItemEndBal.Glyph;
            }

            private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemConvert.Caption;
                dropDownButtonPrint.Image = barButtonItemConvert.Glyph;
                if(radioGroupJournal.SelectedIndex == 0)
                    ProgramUtil.convertGrid(gridViewMot, "Шилжүүлгийн журнал");
                else
                    ProgramUtil.convertGrid(gridViewDetail, "Шилжүүлгийн гүйлгээ");
            }

        #endregion

    }
}