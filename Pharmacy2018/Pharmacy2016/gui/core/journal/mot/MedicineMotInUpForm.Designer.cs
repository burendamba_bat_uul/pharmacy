﻿namespace Pharmacy2016.gui.core.journal.mot
{
    partial class MedicineMotInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicineMotInUpForm));
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditExpDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlDetail = new DevExpress.XtraGrid.GridControl();
            this.gridViewDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditMedicine = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spinEditCount = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditPrice = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditSumPrice = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEditDocNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControlWarehouse = new DevExpress.XtraEditors.LabelControl();
            this.spinEditBal = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEditBarcode = new DevExpress.XtraEditors.TextEdit();
            this.treeListLookUpEditWarehouse = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn4 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListLookUpEditIncWarehouse = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.gridLookUpEditUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupType = new DevExpress.XtraEditors.RadioGroup();
            this.treeListLookUpEditWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList2 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn5 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridLookUpEditIncUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.treeListLookUpEditIncWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList3 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn6 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupIncType = new DevExpress.XtraEditors.RadioGroup();
            this.labelControlIncWarehouse = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSumPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDocNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBarcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditIncWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIncUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditIncWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIncType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Location = new System.Drawing.Point(374, 59);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(75, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Хүлээн авагч*:";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Location = new System.Drawing.Point(408, 33);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(41, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Огноо*:";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl4.Location = new System.Drawing.Point(53, 491);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(66, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Тоо хэмжээ*:";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl5.Location = new System.Drawing.Point(422, 465);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(27, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Үнэ*:";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl6.Location = new System.Drawing.Point(395, 491);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(54, 13);
            this.labelControl6.TabIndex = 8;
            this.labelControl6.Text = "Нийт үнэ*:";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(516, 526);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 21;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(597, 526);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 22;
            this.simpleButtonCancel.Text = "Болих";
            this.simpleButtonCancel.ToolTip = "Болих";
            // 
            // dateEditExpDate
            // 
            this.dateEditExpDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditExpDate.EditValue = null;
            this.dateEditExpDate.Location = new System.Drawing.Point(455, 30);
            this.dateEditExpDate.Name = "dateEditExpDate";
            this.dateEditExpDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditExpDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditExpDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditExpDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditExpDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditExpDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditExpDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditExpDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditExpDate.TabIndex = 5;
            this.dateEditExpDate.EditValueChanged += new System.EventHandler(this.dateEditExpDate_EditValueChanged);
            // 
            // textEditID
            // 
            this.textEditID.Location = new System.Drawing.Point(125, 30);
            this.textEditID.Name = "textEditID";
            this.textEditID.Properties.ReadOnly = true;
            this.textEditID.Size = new System.Drawing.Size(200, 20);
            this.textEditID.TabIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(20, 33);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(99, 13);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "Баримтын дугаар*:";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditDescription.Location = new System.Drawing.Point(125, 162);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(530, 40);
            this.memoEditDescription.TabIndex = 10;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(70, 164);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(49, 13);
            this.labelControl8.TabIndex = 18;
            this.labelControl8.Text = "Тайлбар :";
            // 
            // gridControlDetail
            // 
            this.gridControlDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlDetail.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 11, true, true, "Захиалга татах", "order")});
            this.gridControlDetail.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlDetail_EmbeddedNavigator_ButtonClick);
            this.gridControlDetail.Location = new System.Drawing.Point(12, 208);
            this.gridControlDetail.MainView = this.gridViewDetail;
            this.gridControlDetail.Name = "gridControlDetail";
            this.gridControlDetail.Size = new System.Drawing.Size(660, 222);
            this.gridControlDetail.TabIndex = 10;
            this.gridControlDetail.UseEmbeddedNavigator = true;
            this.gridControlDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDetail});
            // 
            // gridViewDetail
            // 
            this.gridViewDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn31});
            this.gridViewDetail.GridControl = this.gridControlDetail;
            this.gridViewDetail.Name = "gridViewDetail";
            this.gridViewDetail.OptionsBehavior.ReadOnly = true;
            this.gridViewDetail.OptionsView.ShowFooter = true;
            this.gridViewDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewDetail.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewDetail_FocusedRowChanged);
            this.gridViewDetail.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewDetail_InvalidRowException);
            this.gridViewDetail.BeforeLeaveRow += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.gridViewDetail_BeforeLeaveRow);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Эмийн нэр";
            this.gridColumn2.CustomizationCaption = "Эмийн нэр";
            this.gridColumn2.FieldName = "medicineName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 155;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ОУ нэр";
            this.gridColumn3.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn3.FieldName = "latinName";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 154;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Тун хэмжээ";
            this.gridColumn4.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn4.FieldName = "unitType";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 84;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Тоо";
            this.gridColumn5.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn5.CustomizationCaption = "Тоо";
            this.gridColumn5.FieldName = "count";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 67;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Үнэ";
            this.gridColumn6.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn6.CustomizationCaption = "Үнэ";
            this.gridColumn6.FieldName = "price";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 67;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Нийт үнэ";
            this.gridColumn7.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn7.CustomizationCaption = "Нийт үнэ";
            this.gridColumn7.FieldName = "sumPrice";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            this.gridColumn7.Width = 78;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Орлогын төрөл";
            this.gridColumn31.CustomizationCaption = "Орлогын төрөл";
            this.gridColumn31.FieldName = "incTypeName";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 2;
            this.gridColumn31.Width = 90;
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl9.Location = new System.Drawing.Point(96, 439);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(23, 13);
            this.labelControl9.TabIndex = 21;
            this.labelControl9.Text = "Эм*:";
            // 
            // gridLookUpEditMedicine
            // 
            this.gridLookUpEditMedicine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gridLookUpEditMedicine.EditValue = "";
            this.gridLookUpEditMedicine.Location = new System.Drawing.Point(125, 436);
            this.gridLookUpEditMedicine.Name = "gridLookUpEditMedicine";
            this.gridLookUpEditMedicine.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditMedicine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMedicine.Properties.DisplayMember = "name";
            this.gridLookUpEditMedicine.Properties.NullText = "";
            this.gridLookUpEditMedicine.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditMedicine.Properties.ValueMember = "medicinePrice";
            //this.gridLookUpEditMedicine.Properties.ValueMember = "incType";
            this.gridLookUpEditMedicine.Properties.View = this.gridView3;
            this.gridLookUpEditMedicine.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditMedicine_Properties_ButtonClick);
            this.gridLookUpEditMedicine.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditMedicine.TabIndex = 11;
            this.gridLookUpEditMedicine.InvalidValue += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(this.gridLookUpEditMedicine_InvalidValue);
            this.gridLookUpEditMedicine.EditValueChanged += new System.EventHandler(this.gridLookUpEditMedicine_EditValueChanged);
            this.gridLookUpEditMedicine.Validating += new System.ComponentModel.CancelEventHandler(this.gridLookUpEditMedicine_Validating);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn27,
            this.gridColumn20,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Нэр";
            this.gridColumn15.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn15.FieldName = "name";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "ОУ нэр";
            this.gridColumn16.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn16.FieldName = "latinName";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 1;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Ангилал";
            this.gridColumn17.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn17.FieldName = "medicineTypeName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 2;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Тун хэмжээ";
            this.gridColumn18.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn18.FieldName = "unitType";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 5;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Үнэ";
            this.gridColumn19.CustomizationCaption = "Үнэ";
            this.gridColumn19.DisplayFormat.FormatString = "n2";
            this.gridColumn19.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn19.FieldName = "price";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 6;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Үлдэгдэл";
            this.gridColumn27.CustomizationCaption = "Үлдэгдэл";
            this.gridColumn27.DisplayFormat.FormatString = "n2";
            this.gridColumn27.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn27.FieldName = "count";
            this.gridColumn27.MinWidth = 75;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 7;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Бар код";
            this.gridColumn20.CustomizationCaption = "Бар код";
            this.gridColumn20.FieldName = "barcode";
            this.gridColumn20.MinWidth = 75;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 9;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Хэлбэр";
            this.gridColumn28.CustomizationCaption = "Хэлбэр";
            this.gridColumn28.FieldName = "shape";
            this.gridColumn28.MinWidth = 75;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 4;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Хүчинтэй хугацаа";
            this.gridColumn29.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn29.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.gridColumn29.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn29.FieldName = "validDate";
            this.gridColumn29.MinWidth = 75;
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 8;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Орлогын төрөл";
            this.gridColumn30.CustomizationCaption = "Орлогын төрөл";
            this.gridColumn30.FieldName = "incTypeName";
            this.gridColumn30.MinWidth = 75;
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 3;
            // 
            // spinEditCount
            // 
            this.spinEditCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spinEditCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditCount.Location = new System.Drawing.Point(125, 488);
            this.spinEditCount.Name = "spinEditCount";
            this.spinEditCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditCount.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditCount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.EditFormat.FormatString = "n2";
            this.spinEditCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.Mask.EditMask = "n2";
            this.spinEditCount.Size = new System.Drawing.Size(100, 20);
            this.spinEditCount.TabIndex = 12;
            this.spinEditCount.EditValueChanged += new System.EventHandler(this.spinEditCount_ValueChanged);
            this.spinEditCount.Leave += new System.EventHandler(this.spinEditCount_Leave);
            this.spinEditCount.Validated += new System.EventHandler(this.spinEditCount_Validated);
            // 
            // spinEditPrice
            // 
            this.spinEditPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.spinEditPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditPrice.Location = new System.Drawing.Point(455, 462);
            this.spinEditPrice.Name = "spinEditPrice";
            this.spinEditPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditPrice.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.EditFormat.FormatString = "n2";
            this.spinEditPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.Mask.EditMask = "n2";
            this.spinEditPrice.Properties.ReadOnly = true;
            this.spinEditPrice.Size = new System.Drawing.Size(100, 20);
            this.spinEditPrice.TabIndex = 14;
            // 
            // spinEditSumPrice
            // 
            this.spinEditSumPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.spinEditSumPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditSumPrice.Location = new System.Drawing.Point(455, 488);
            this.spinEditSumPrice.Name = "spinEditSumPrice";
            this.spinEditSumPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditSumPrice.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditSumPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditSumPrice.Properties.EditFormat.FormatString = "n2";
            this.spinEditSumPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditSumPrice.Properties.Mask.EditMask = "n2";
            this.spinEditSumPrice.Properties.ReadOnly = true;
            this.spinEditSumPrice.Size = new System.Drawing.Size(100, 20);
            this.spinEditSumPrice.TabIndex = 15;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(23, 139);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(96, 13);
            this.labelControl10.TabIndex = 28;
            this.labelControl10.Text = "Дагалдах баримт :";
            // 
            // textEditDocNumber
            // 
            this.textEditDocNumber.Location = new System.Drawing.Point(125, 136);
            this.textEditDocNumber.Name = "textEditDocNumber";
            this.textEditDocNumber.Size = new System.Drawing.Size(200, 20);
            this.textEditDocNumber.TabIndex = 9;
            // 
            // labelControlWarehouse
            // 
            this.labelControlWarehouse.Location = new System.Drawing.Point(67, 113);
            this.labelControlWarehouse.Name = "labelControlWarehouse";
            this.labelControlWarehouse.Size = new System.Drawing.Size(52, 13);
            this.labelControlWarehouse.TabIndex = 29;
            this.labelControlWarehouse.Text = "Агуулах*:";
            // 
            // spinEditBal
            // 
            this.spinEditBal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spinEditBal.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditBal.Location = new System.Drawing.Point(455, 436);
            this.spinEditBal.Name = "spinEditBal";
            this.spinEditBal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditBal.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditBal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditBal.Properties.EditFormat.FormatString = "n2";
            this.spinEditBal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditBal.Properties.Mask.EditMask = "n2";
            this.spinEditBal.Properties.ReadOnly = true;
            this.spinEditBal.Size = new System.Drawing.Size(100, 20);
            this.spinEditBal.TabIndex = 13;
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl12.Location = new System.Drawing.Point(392, 439);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(57, 13);
            this.labelControl12.TabIndex = 33;
            this.labelControl12.Text = "Үлдэгдэл*:";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 526);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 23;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl2.Location = new System.Drawing.Point(75, 464);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(44, 13);
            this.labelControl2.TabIndex = 42;
            this.labelControl2.Text = "Баркод :";
            // 
            // textEditBarcode
            // 
            this.textEditBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textEditBarcode.Location = new System.Drawing.Point(125, 462);
            this.textEditBarcode.Name = "textEditBarcode";
            this.textEditBarcode.Size = new System.Drawing.Size(200, 20);
            this.textEditBarcode.TabIndex = 16;
            this.textEditBarcode.EditValueChanged += new System.EventHandler(this.textEditBarcode_EditValueChanged);
            // 
            // treeListLookUpEditWarehouse
            // 
            this.treeListLookUpEditWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWarehouse.EditValue = "0";
            this.treeListLookUpEditWarehouse.Location = new System.Drawing.Point(125, 110);
            this.treeListLookUpEditWarehouse.Name = "treeListLookUpEditWarehouse";
            this.treeListLookUpEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWarehouse.Properties.DisplayMember = "name";
            this.treeListLookUpEditWarehouse.Properties.NullText = "";
            this.treeListLookUpEditWarehouse.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEditWarehouse.Properties.ValueMember = "id";
            this.treeListLookUpEditWarehouse.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWarehouse.TabIndex = 4;
            this.treeListLookUpEditWarehouse.EditValueChanged += new System.EventHandler(this.treeListLookUpEditWarehouse_EditValueChanged);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1,
            this.treeListColumn3});
            this.treeListLookUpEdit1TreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListLookUpEdit1TreeList.KeyFieldName = "id";
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(155, 217);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsBehavior.PopulateServiceColumns = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.ParentFieldName = "pid";
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Нэр";
            this.treeListColumn1.CustomizationCaption = "Агуулахын нэр";
            this.treeListColumn1.FieldName = "name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraTreeList.FilterPopupMode.List;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 400;
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "Дугаар";
            this.treeListColumn3.CustomizationCaption = "Агуулахын дугаар";
            this.treeListColumn3.FieldName = "number";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 1;
            this.treeListColumn3.Width = 96;
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn4,
            this.treeListColumn2});
            this.treeList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(30, 161);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsBehavior.PopulateServiceColumns = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumn4
            // 
            this.treeListColumn4.Caption = "Нэр";
            this.treeListColumn4.CustomizationCaption = "Агуулахын нэр";
            this.treeListColumn4.FieldName = "name";
            this.treeListColumn4.Name = "treeListColumn4";
            this.treeListColumn4.OptionsColumn.AllowMove = false;
            this.treeListColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraTreeList.FilterPopupMode.List;
            this.treeListColumn4.Visible = true;
            this.treeListColumn4.VisibleIndex = 0;
            this.treeListColumn4.Width = 400;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Дугаар";
            this.treeListColumn2.CustomizationCaption = "Агуулахын дугаар";
            this.treeListColumn2.FieldName = "number";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 1;
            this.treeListColumn2.Width = 96;
            // 
            // treeListLookUpEditIncWarehouse
            // 
            this.treeListLookUpEditIncWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditIncWarehouse.EditValue = "0";
            this.treeListLookUpEditIncWarehouse.Location = new System.Drawing.Point(455, 110);
            this.treeListLookUpEditIncWarehouse.Name = "treeListLookUpEditIncWarehouse";
            this.treeListLookUpEditIncWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditIncWarehouse.Properties.DisplayMember = "name";
            this.treeListLookUpEditIncWarehouse.Properties.NullText = "";
            this.treeListLookUpEditIncWarehouse.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditIncWarehouse.Properties.ValueMember = "id";
            this.treeListLookUpEditIncWarehouse.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditIncWarehouse.TabIndex = 8;
            // 
            // gridLookUpEditUser
            // 
            this.gridLookUpEditUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditUser.EditValue = "";
            this.gridLookUpEditUser.Location = new System.Drawing.Point(125, 56);
            this.gridLookUpEditUser.Name = "gridLookUpEditUser";
            this.gridLookUpEditUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditUser.Properties.NullText = "";
            this.gridLookUpEditUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditUser.Properties.ValueMember = "id";
            this.gridLookUpEditUser.Properties.View = this.gridView2;
            this.gridLookUpEditUser.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditUser.TabIndex = 2;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Овог";
            this.gridColumn8.CustomizationCaption = "Овог";
            this.gridColumn8.FieldName = "lastName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Нэр";
            this.gridColumn9.CustomizationCaption = "Нэр";
            this.gridColumn9.FieldName = "firstName";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Регистер";
            this.gridColumn10.CustomizationCaption = "Регистер";
            this.gridColumn10.FieldName = "id";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Эрх";
            this.gridColumn11.CustomizationCaption = "Эрх";
            this.gridColumn11.FieldName = "roleName";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Тасаг";
            this.gridColumn12.CustomizationCaption = "Тасаг";
            this.gridColumn12.FieldName = "wardName";
            this.gridColumn12.MinWidth = 75;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Ажлын газар";
            this.gridColumn13.CustomizationCaption = "Ажлын газар";
            this.gridColumn13.FieldName = "organizationName";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Албан тушаал";
            this.gridColumn14.CustomizationCaption = "Албан тушаал";
            this.gridColumn14.FieldName = "positionName";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 6;
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl13.Location = new System.Drawing.Point(78, 59);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(41, 13);
            this.labelControl13.TabIndex = 45;
            this.labelControl13.Text = "Нярав*:";
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl14.Location = new System.Drawing.Point(79, 86);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(40, 13);
            this.labelControl14.TabIndex = 47;
            this.labelControl14.Text = "Төрөл*:";
            // 
            // radioGroupType
            // 
            this.radioGroupType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupType.Location = new System.Drawing.Point(125, 82);
            this.radioGroupType.Name = "radioGroupType";
            this.radioGroupType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Эмийн сан"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Тасаг")});
            this.radioGroupType.Size = new System.Drawing.Size(200, 22);
            this.radioGroupType.TabIndex = 3;
            this.radioGroupType.SelectedIndexChanged += new System.EventHandler(this.radioGroupType_SelectedIndexChanged);
            // 
            // treeListLookUpEditWard
            // 
            this.treeListLookUpEditWard.Location = new System.Drawing.Point(125, 4);
            this.treeListLookUpEditWard.Name = "treeListLookUpEditWard";
            this.treeListLookUpEditWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditWard.Properties.NullText = "";
            this.treeListLookUpEditWard.Properties.TreeList = this.treeList2;
            this.treeListLookUpEditWard.Properties.ValueMember = "id";
            this.treeListLookUpEditWard.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWard.TabIndex = 4;
            // 
            // treeList2
            // 
            this.treeList2.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn5});
            this.treeList2.KeyFieldName = "id";
            this.treeList2.Location = new System.Drawing.Point(151, 256);
            this.treeList2.Name = "treeList2";
            this.treeList2.OptionsBehavior.EnableFiltering = true;
            this.treeList2.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList2.ParentFieldName = "pid";
            this.treeList2.Size = new System.Drawing.Size(400, 200);
            this.treeList2.TabIndex = 0;
            // 
            // treeListColumn5
            // 
            this.treeListColumn5.Caption = "Нэр";
            this.treeListColumn5.FieldName = "name";
            this.treeListColumn5.Name = "treeListColumn5";
            this.treeListColumn5.Visible = true;
            this.treeListColumn5.VisibleIndex = 0;
            // 
            // gridLookUpEditIncUser
            // 
            this.gridLookUpEditIncUser.EditValue = "";
            this.gridLookUpEditIncUser.Location = new System.Drawing.Point(455, 56);
            this.gridLookUpEditIncUser.Name = "gridLookUpEditIncUser";
            this.gridLookUpEditIncUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditIncUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditIncUser.Properties.NullText = "";
            this.gridLookUpEditIncUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditIncUser.Properties.ValueMember = "id";
            this.gridLookUpEditIncUser.Properties.View = this.gridView4;
            this.gridLookUpEditIncUser.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditIncUser.TabIndex = 6;
            this.gridLookUpEditIncUser.EditValueChanged += new System.EventHandler(this.gridLookUpEditIncUser_EditValueChanged);
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Нэр";
            this.gridColumn21.CustomizationCaption = "Нэр";
            this.gridColumn21.FieldName = "firstName";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Регистер";
            this.gridColumn22.CustomizationCaption = "Регистер";
            this.gridColumn22.FieldName = "id";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Эрх";
            this.gridColumn23.CustomizationCaption = "Эрх";
            this.gridColumn23.FieldName = "roleName";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Тасаг";
            this.gridColumn24.CustomizationCaption = "Тасаг";
            this.gridColumn24.FieldName = "wardName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Ажлын газар";
            this.gridColumn25.CustomizationCaption = "Ажлын газар";
            this.gridColumn25.FieldName = "organizationName";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Албан тушаал";
            this.gridColumn26.CustomizationCaption = "Албан тушаал";
            this.gridColumn26.FieldName = "positionName";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            // 
            // treeListLookUpEditIncWard
            // 
            this.treeListLookUpEditIncWard.Location = new System.Drawing.Point(455, 4);
            this.treeListLookUpEditIncWard.Name = "treeListLookUpEditIncWard";
            this.treeListLookUpEditIncWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditIncWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditIncWard.Properties.NullText = "";
            this.treeListLookUpEditIncWard.Properties.TreeList = this.treeList3;
            this.treeListLookUpEditIncWard.Properties.ValueMember = "id";
            this.treeListLookUpEditIncWard.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditIncWard.TabIndex = 8;
            // 
            // treeList3
            // 
            this.treeList3.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn6});
            this.treeList3.KeyFieldName = "id";
            this.treeList3.Location = new System.Drawing.Point(90, 306);
            this.treeList3.Name = "treeList3";
            this.treeList3.OptionsBehavior.EnableFiltering = true;
            this.treeList3.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList3.ParentFieldName = "pid";
            this.treeList3.Size = new System.Drawing.Size(400, 200);
            this.treeList3.TabIndex = 0;
            // 
            // treeListColumn6
            // 
            this.treeListColumn6.Caption = "Нэр";
            this.treeListColumn6.FieldName = "name";
            this.treeListColumn6.Name = "treeListColumn6";
            this.treeListColumn6.Visible = true;
            this.treeListColumn6.VisibleIndex = 0;
            // 
            // labelControl15
            // 
            this.labelControl15.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl15.Location = new System.Drawing.Point(409, 86);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(40, 13);
            this.labelControl15.TabIndex = 52;
            this.labelControl15.Text = "Төрөл*:";
            // 
            // radioGroupIncType
            // 
            this.radioGroupIncType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupIncType.Location = new System.Drawing.Point(455, 82);
            this.radioGroupIncType.Name = "radioGroupIncType";
            this.radioGroupIncType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Эмийн сан"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Тасаг")});
            this.radioGroupIncType.Size = new System.Drawing.Size(200, 22);
            this.radioGroupIncType.TabIndex = 7;
            this.radioGroupIncType.SelectedIndexChanged += new System.EventHandler(this.radioGroupIncType_SelectedIndexChanged);
            // 
            // labelControlIncWarehouse
            // 
            this.labelControlIncWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlIncWarehouse.Location = new System.Drawing.Point(397, 113);
            this.labelControlIncWarehouse.Name = "labelControlIncWarehouse";
            this.labelControlIncWarehouse.Size = new System.Drawing.Size(52, 13);
            this.labelControlIncWarehouse.TabIndex = 53;
            this.labelControlIncWarehouse.Text = "Агуулах*:";
            // 
            // MedicineMotInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(684, 561);
            this.Controls.Add(this.labelControlIncWarehouse);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.radioGroupIncType);
            this.Controls.Add(this.treeListLookUpEditIncWard);
            this.Controls.Add(this.gridLookUpEditIncUser);
            this.Controls.Add(this.treeListLookUpEditWard);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.radioGroupType);
            this.Controls.Add(this.gridLookUpEditUser);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.treeListLookUpEditIncWarehouse);
            this.Controls.Add(this.treeListLookUpEditWarehouse);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.textEditBarcode);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.spinEditBal);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.labelControlWarehouse);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.textEditDocNumber);
            this.Controls.Add(this.spinEditSumPrice);
            this.Controls.Add(this.spinEditPrice);
            this.Controls.Add(this.spinEditCount);
            this.Controls.Add(this.gridLookUpEditMedicine);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.gridControlDetail);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.memoEditDescription);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.textEditID);
            this.Controls.Add(this.dateEditExpDate);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonSave);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MedicineMotInUpForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Шилжүүлэг нэмэх";
            this.Shown += new System.EventHandler(this.MedicineMotInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSumPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDocNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBarcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditIncWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIncUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditIncWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIncType.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.DateEdit dateEditExpDate;
        private DevExpress.XtraEditors.TextEdit textEditID;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraGrid.GridControl gridControlDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDetail;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMedicine;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.SpinEdit spinEditCount;
        private DevExpress.XtraEditors.SpinEdit spinEditPrice;
        private DevExpress.XtraEditors.SpinEdit spinEditSumPrice;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit textEditDocNumber;
        private DevExpress.XtraEditors.LabelControl labelControlWarehouse;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraEditors.SpinEdit spinEditBal;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEditBarcode;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWarehouse;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn4;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditIncWarehouse;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.RadioGroup radioGroupType;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWard;
        private DevExpress.XtraTreeList.TreeList treeList2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn5;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditIncUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditIncWard;
        private DevExpress.XtraTreeList.TreeList treeList3;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn6;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.RadioGroup radioGroupIncType;
        private DevExpress.XtraEditors.LabelControl labelControlIncWarehouse;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
    }
}