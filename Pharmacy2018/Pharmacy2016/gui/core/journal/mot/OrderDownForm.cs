﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;

namespace Pharmacy2016.gui.core.journal.mot
{
    /*
     * Захиалгын хуудаснаас сонгон татах цонх.
     * 
     */

    public partial class OrderDownForm : DevExpress.XtraEditors.XtraForm
    {
        
        #region Форм анх ачааллах функц

            private static OrderDownForm INSTANCE = new OrderDownForm();

            public static OrderDownForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private OrderDownForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                gridControlOrder.DataSource = JournalDataSet.OrderGoodsOtherBindingSource;
            }

        #endregion
        
        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    dateEditStart.DateTime = MedicineMotForm.Instance.getStartDate();
                    dateEditEnd.DateTime = MedicineMotForm.Instance.getEndDate();
                    reload();

                    ShowDialog(MedicineMotInUpForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void clearData()
            {
                JournalDataSet.Instance.OrderGoodsOtherDetail.Clear();
                JournalDataSet.Instance.OrderGoodsOther.Clear();
            }

            private void OrderDownForm_Shown(object sender, EventArgs e)
            {
                gridControlOrder.Focus();
            }

        #endregion

        #region Формын event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonDownload_Click(object sender, EventArgs e)
            {
                download();
            }

        #endregion

        #region Формын функц

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void reload()
            {
                if (check())
                {
                    //JournalDataSet.MedicineOrderOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOrderOther, HospitalUtil.getHospitalId(), MedicineMotInUpForm.Instance.getWarehouse(), dateEditStart.Text, dateEditEnd.Text);
                    //JournalDataSet.MedicineOrderDetailOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOrderDetailOther, HospitalUtil.getHospitalId(), MedicineMotInUpForm.Instance.getWarehouse(), dateEditStart.Text, dateEditEnd.Text);
                }
            }

            private void download()
            {
                if (gridViewOrder.RowCount == 0 || gridViewOrder.GetFocusedRow() == null)
                {
                    Close();
                }
                else
                {
                    //DataRow row = gridViewOrder.GetDataRow(gridViewOrder.FocusedRowHandle);
                    //DataRow[] rows = JournalDataSet.Instance.MedicineOrderDetailOther.Select("medicineOrderID = '" + row["id"].ToString() + "'");
                    //if (rows.Length == 0)
                    //{
                    //    ProgramUtil.showAlertDialog(Instance, Instance.Text, "Мөрийн тоо 0 байна. Та мэдээллээ шалгана уу");
                    //    return;
                    //}
                    //MedicineMotInUpForm.Instance.downloadOrder(rows, row["id"].ToString(), row["warehouseID"].ToString());
                    Close();
                }
            }

        #endregion

    }
}