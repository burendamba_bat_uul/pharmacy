﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.bal;


namespace Pharmacy2016.gui.core.journal.mot
{
    /*  
     * Эмийн шилжүүлэг нэмэх, засах цонх.
     * 
     */

    public partial class MedicineMotInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineMotInUpForm INSTANCE = new MedicineMotInUpForm();

            public static MedicineMotInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private string id;

            private string medicineOrderID;

            private MedicineMotInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                spinEditCount.Properties.MaxValue = Decimal.MaxValue - 1;

                initError();
                initBinding();
                initDataSource();
                reload(false);
            }

            private void initDataSource()
            {
                gridControlDetail.DataSource = JournalDataSet.MotDetailBindingSource;
                gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
                treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
                treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
                gridLookUpEditIncUser.Properties.DataSource = InformationDataSet.SystemUserOtherBindingSource;
                treeListLookUpEditIncWarehouse.Properties.DataSource = JournalDataSet.WarehouseOther1BindingSource;
                treeListLookUpEditIncWard.Properties.DataSource = JournalDataSet.WardOther1BindingSource;
                gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineBalBindingSource;
            }

            private void initError()
            {
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditIncUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditIncWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditIncWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditExpDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    
                    actionType = type;
                    if (actionType == 1)
                    {
                        insertMot();
                    }
                    else if (actionType == 2)
                    {
                        updateMot();
                    }
                    
                    gridControlDetail.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = (UserUtil.getUserRoleId() != RoleUtil.PHARMACIST_ID);

                    ShowDialog(MedicineMotForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void initBinding()
            {
                gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "userID", true));
                radioGroupType.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "type", true));
                treeListLookUpEditWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "warehouseID", true));
                treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "wardID", true));
                gridLookUpEditIncUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "incUserID", true));
                radioGroupIncType.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "incType", true));
                treeListLookUpEditIncWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "incWarehouseID", true));
                treeListLookUpEditIncWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "incWardID", true));
                textEditID.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "id", true));
                textEditDocNumber.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "docNumber", true));
                dateEditExpDate.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "date", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotJournalBindingSource, "description", true));

                //gridLookUpEditMedicine.DataBindings.Add(new Binding("EditValue", JournalDataSet.MotDetailBindingSource, "medicineID", true));
            }

            private void insertMot()
            {
                this.Text = "Шилжүүлэг нэмэх цонх";
                id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                JournalDataSet.Instance.MotJournal.idColumn.DefaultValue = id;
                JournalDataSet.Instance.MotDetail.motJournalIDColumn.DefaultValue = id;
                MedicineMotForm.Instance.currView = (DataRowView)JournalDataSet.MotJournalBindingSource.AddNew();
                DateTime now = DateTime.Now;
                MedicineMotForm.Instance.currView["id"] = id;
                MedicineMotForm.Instance.currView["date"] = now;

                dateEditExpDate.DateTime = now;
                textEditID.Text = id;
                //treeListLookUpEditWarehouse.Enabled = true;
                gridLookUpEditIncUser.Enabled = true;
                treeListLookUpEditIncWarehouse.Enabled = true;
                treeListLookUpEditIncWard.Enabled = true;
                gridControlDetail.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;
            }

            private void updateMot()
            {
                this.Text = "Шилжүүлэг засах цонх";
                id = MedicineMotForm.Instance.currView["id"].ToString();
                JournalDataSet.Instance.MotDetail.motJournalIDColumn.DefaultValue = id;
                if (MedicineMotForm.Instance.currView["medicineOrderID"] != DBNull.Value && MedicineMotForm.Instance.currView["medicineOrderID"] != null)
                {
                    medicineOrderID = MedicineMotForm.Instance.currView["medicineOrderID"].ToString();
                    gridLookUpEditIncUser.Enabled = false;
                    treeListLookUpEditIncWarehouse.Enabled = false;
                    treeListLookUpEditIncWard.Enabled = false;
                }
                else
                {
                    gridLookUpEditIncUser.Enabled = true;
                    treeListLookUpEditIncWarehouse.Enabled = true;
                    treeListLookUpEditIncWard.Enabled = true;
                }

                //treeListLookUpEditWarehouse.Enabled = false;
                gridControlDetail.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
            }


            private void clearData()
            {
                clearErrorText();
                //clearBinding();
                clearMedicine();

                medicineOrderID = null;
                JournalDataSet.WarehouseOtherBindingSource.Filter = "";
                JournalDataSet.WarehouseOther1BindingSource.Filter = "";
                JournalDataSet.WardOtherBindingSource.Filter = "";
                JournalDataSet.WardOther1BindingSource.Filter = "";
                InformationDataSet.SystemUserBindingSource.Filter = "";
                InformationDataSet.SystemUserOtherBindingSource.Filter = "";

                JournalDataSet.MotJournalBindingSource.CancelEdit();
                JournalDataSet.MotDetailBindingSource.CancelEdit();
                JournalDataSet.Instance.MotJournal.RejectChanges();
                JournalDataSet.Instance.MotDetail.RejectChanges();
            }

            private void clearBinding()
            {
                gridLookUpEditUser.DataBindings.Clear();
                treeListLookUpEditWarehouse.DataBindings.Clear();
                treeListLookUpEditWard.DataBindings.Clear();
                gridLookUpEditIncUser.DataBindings.Clear();
                treeListLookUpEditIncWarehouse.DataBindings.Clear();
                treeListLookUpEditIncWard.DataBindings.Clear();
                textEditID.DataBindings.Clear();
                textEditDocNumber.DataBindings.Clear();
                dateEditExpDate.DataBindings.Clear();
                memoEditDescription.DataBindings.Clear();
            }

            private void clearErrorText()
            {
                treeListLookUpEditWarehouse.ErrorText = "";
                treeListLookUpEditIncWarehouse.ErrorText = "";
                gridLookUpEditMedicine.ErrorText = "";
                dateEditExpDate.ErrorText = "";
                spinEditCount.ErrorText = "";
                spinEditPrice.ErrorText = "";
            }

            private void clearMedicine()
            {
                gridLookUpEditMedicine.EditValue = null;
                textEditBarcode.Text = "";
                spinEditBal.Value = 0;
                spinEditCount.Value = 0;
                spinEditPrice.Value = 0;
                spinEditSumPrice.Value = 0;
            }

            private void MedicineMotInUpForm_Shown(object sender, EventArgs e)
            {
                gridViewDetail.FocusedRowHandle = 0;
                gridViewDetail_BeforeLeaveRow(gridViewDetail, new RowAllowEventArgs(gridViewDetail.FocusedRowHandle, true));

                if (actionType == 1)
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                    {
                        gridLookUpEditUser.EditValue = UserUtil.getUserId();
                        treeListLookUpEditWarehouse.Focus();
                    }
                    else
                    {
                        gridLookUpEditUser.Focus();
                    }
                }
                else if (actionType == 2)
                {
                    setValueFromGrid();
                    gridLookUpEditMedicine.Focus();
                }

                if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                {
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                    //InformationDataSet.SystemUserOtherBindingSource.Filter = "id <> '" + UserUtil.getUserId() + "'";
                }
            }

        #endregion

        #region Формын event

            private void dateEditExpDate_EditValueChanged(object sender, EventArgs e)
            {
                reloadMedicine();
            }

            private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null)
                {
                    
                    if (actionType == 1 && gridViewDetail.RowCount == 0 &&
                        ((radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && !treeListLookUpEditWarehouse.EditValue.ToString().Equals("")) ||
                        (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && !treeListLookUpEditWard.EditValue.ToString().Equals(""))))
                    {
                        addRow();
                        gridLookUpEditMedicine.Focus();
                    }
                }
            }

            private void gridLookUpEditIncUser_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditIncUser.EditValue != DBNull.Value && gridLookUpEditIncUser.EditValue != null)
                {
                    //JournalDataSet.WarehouseOther1BindingSource.Filter = UserUtil.getFilterWarehouse();
                    //JournalDataSet.WardOther1BindingSource.Filter = UserUtil.getFilterWard();
                }
            }

            private void treeListLookUpEditWarehouse_EditValueChanged(object sender, EventArgs e)
            {
                if (actionType == 1 && gridViewDetail.RowCount == 0 && (gridLookUpEditUser.EditValue != null && gridLookUpEditUser.EditValue != DBNull.Value && !gridLookUpEditUser.EditValue.ToString().Equals("")) &&
                    ((radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && !treeListLookUpEditWarehouse.EditValue.ToString().Equals("")) ||
                     (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && !treeListLookUpEditWard.EditValue.ToString().Equals(""))))
                {
                    addRow();
                    gridLookUpEditMedicine.Focus();
                }
                reloadMedicine();
            }

            private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeType();
            }

            private void radioGroupIncType_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeIncType();
            }


            private void gridControlDetail_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add") && (gridViewDetail.RowCount == 0 || (gridLookUpEditMedicine.ErrorText == "" && gridLookUpEditMedicine.EditValue != null && gridLookUpEditMedicine.EditValue != DBNull.Value && checkMedicine())))
                    {
                        addRow();
                        gridLookUpEditMedicine.Focus();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewDetail.GetFocusedDataRow() != null)
                    {
                        deleteRow();
                    }
                    else if (String.Equals(e.Button.Tag, "order"))
                    {
                        showOrder();
                    }
                }
            }

            private void gridViewDetail_BeforeLeaveRow(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
            {
                if (this.Visible && gridViewDetail.GetDataRow(e.RowHandle) != null &&
                    (gridViewDetail.GetDataRow(e.RowHandle)["medicineID"] == DBNull.Value
                    || Convert.ToDecimal(gridViewDetail.GetDataRow(e.RowHandle)["price"]) == 0
                    || Convert.ToDecimal(gridViewDetail.GetDataRow(e.RowHandle)["count"]) == 0))
                {
                    e.Allow = e.RowHandle < 0;
                    if (!e.Allow)
                        spinEditCount.ErrorText = "Тоо, хэмжээг оруулна уу";
                }
                else if(this.Visible && (gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != DBNull.Value 
                    && Convert.ToDecimal(gridViewDetail.GetRowCellValue(e.RowHandle, "price")) != 0))
                {
                    bool isAllow = true;
                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        if (e.RowHandle != i && gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != null && gridViewDetail.GetRowCellValue(i, "medicineID") != null &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != DBNull.Value && gridViewDetail.GetRowCellValue(i, "medicineID") != DBNull.Value &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "incTypeID") != null && gridViewDetail.GetRowCellValue(i, "incTypeID") != null &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "incTypeID") != DBNull.Value && gridViewDetail.GetRowCellValue(i, "incTypeID") != DBNull.Value &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID").ToString().Equals(gridViewDetail.GetRowCellValue(i, "medicineID").ToString()) &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "incTypeID").ToString().Equals(gridViewDetail.GetRowCellValue(i, "incTypeID").ToString()) &&
                            Convert.ToDecimal(gridViewDetail.GetRowCellValue(e.RowHandle, "price")) == Convert.ToDecimal(gridViewDetail.GetRowCellValue(i, "price")))
                        {
                            gridLookUpEditMedicine.ErrorText = "Эм, үнэ, орлогын төрөл давхацсан байна";
                            gridLookUpEditMedicine.Focus();
                            isAllow = false;
                            break;
                        }
                    }
                    e.Allow = isAllow;
                }
            }

            private void gridViewDetail_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                gridLookUpEditMedicine.Focus();
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridViewDetail_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
            {
                if(Visible)
                    setValueFromGrid();
            }


            private void spinEditCount_ValueChanged(object sender, EventArgs e)
            {
                calcPrice();
                if (spinEditBal.Value < spinEditCount.Value)
                {
                    spinEditCount.ForeColor = Color.Red;
                }
                else
                {
                    spinEditCount.ForeColor = Color.Black;
                }
                //endEdit();
            }

            private void spinEditCount_Leave(object sender, EventArgs e)
            {   
                endEdit();
            }

            private void spinEditCount_Validated(object sender, EventArgs e)
            {
                endEdit();
            }

            
            private void gridLookUpEditMedicine_EditValueChanged(object sender, EventArgs e)
            {
                DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                if (view != null)
                {
                    textEditBarcode.Text = view["barcode"].ToString();
                    spinEditPrice.Value = Convert.ToDecimal(view["price"]);
                    spinEditBal.Value = Convert.ToDecimal(view["count"]);
                }
            }

            private void gridLookUpEditMedicine_Validating(object sender, CancelEventArgs e)
            {
                //checkMedicine();
                if (!e.Cancel && gridLookUpEditMedicine.EditValue != DBNull.Value && gridLookUpEditMedicine.EditValue != null)
                {
                    endEdit();
                }
            }

            private void gridLookUpEditMedicine_InvalidValue(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridLookUpEditMedicine_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    MedicineBalExpForm.Instance.showForm(2, Instance);
                }   
            }


            private void textEditBarcode_EditValueChanged(object sender, EventArgs e)
            {
                if (Visible && !(textEditBarcode.Text = textEditBarcode.Text.Trim()).Equals(""))
                {
                    for (int i = 0; i < JournalDataSet.Instance.MedicineBal.Rows.Count; i++)
                    {
                        if (JournalDataSet.Instance.MedicineBal.Rows[i]["barcode"].ToString().Equals(textEditBarcode.Text))
                        {
                            gridLookUpEditMedicine.EditValue = JournalDataSet.Instance.MedicineBal.Rows[i]["medicinePrice"];
                            spinEditCount.Focus();
                            break;
                        }
                    }
                }
            }


            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload(true);
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private void reload(bool isReload)
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                InformationDataSet.SystemUserOtherTableAdapter.Fill(InformationDataSet.Instance.SystemUserOther, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                    JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                    JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
                //ProgramUtil.closeWaitDialog();
            }

            private void reloadMedicine()
            {
                JournalDataSet.MedicineBalTableAdapter.Fill(JournalDataSet.Instance.MedicineBal, HospitalUtil.getHospitalId(), gridLookUpEditUser.EditValue.ToString(), radioGroupType.SelectedIndex, (radioGroupType.SelectedIndex == 0 ? treeListLookUpEditWarehouse.EditValue.ToString() : null), (radioGroupType.SelectedIndex == 1 ? treeListLookUpEditWard.EditValue.ToString() : null), dateEditExpDate.Text);
            }


            private void changeType()
            {
                if (radioGroupType.SelectedIndex == 0)
                {
                    labelControlWarehouse.Location = new Point(67, 113);
                    treeListLookUpEditWard.Location = new Point(125, 4);
                    treeListLookUpEditWarehouse.Location = new Point(125, 110);

                    treeListLookUpEditWard.Visible = false;
                    treeListLookUpEditWarehouse.Visible = true;
                    labelControlWarehouse.Text = "Агуулах*:";
                }
                else if (radioGroupType.SelectedIndex == 1)
                {
                    labelControlWarehouse.Location = new Point(81, 113);
                    treeListLookUpEditWard.Location = new Point(125, 110);
                    treeListLookUpEditWarehouse.Location = new Point(125, 4);

                    treeListLookUpEditWard.Visible = true;
                    treeListLookUpEditWarehouse.Visible = false;
                    labelControlWarehouse.Text = "Тасаг*:";
                }
            }

            private void changeIncType()
            {
                if (radioGroupIncType.SelectedIndex == 0)
                {
                    labelControlIncWarehouse.Location = new Point(397, 113);
                    treeListLookUpEditIncWard.Location = new Point(455, 4);
                    treeListLookUpEditIncWarehouse.Location = new Point(455, 110);

                    treeListLookUpEditIncWard.Visible = false;
                    treeListLookUpEditIncWarehouse.Visible = true;
                    labelControlIncWarehouse.Text = "Агуулах*:";
                }
                else if (radioGroupIncType.SelectedIndex == 1)
                {
                    labelControlIncWarehouse.Location = new Point(411, 113);
                    treeListLookUpEditIncWard.Location = new Point(455, 110);
                    treeListLookUpEditIncWarehouse.Location = new Point(455, 4);

                    treeListLookUpEditIncWard.Visible = true;
                    treeListLookUpEditIncWarehouse.Visible = false;
                    labelControlIncWarehouse.Text = "Тасаг*:";
                }
            }


            private void setValueFromGrid()
            {
                DataRowView view = (DataRowView)JournalDataSet.MotDetailBindingSource.Current;

                if (view != null && view["medicineID"] != DBNull.Value && view["medicineID"] != null)
                {
                    gridLookUpEditMedicine.EditValue = view["medicinePrice"];
                    spinEditPrice.EditValue = view["price"];
                    spinEditCount.EditValue = view["count"];
                    spinEditSumPrice.EditValue = view["sumPrice"];
                    DataRowView row = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                    if (row != null)
                    {
                        spinEditBal.EditValue = row["count"];
                    }
                    else
                    {
                        spinEditBal.Value = 0;
                    }
                }
                if (spinEditBal.Value < spinEditCount.Value)
                {
                    spinEditCount.ForeColor = Color.Red;
                }
                else
                {
                    spinEditCount.ForeColor = Color.Black;
                }
            }
        
            private void endEdit()
            {
                DataRowView row = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                DataRowView view = (DataRowView)JournalDataSet.MotDetailBindingSource.Current;
                if (row == null || view == null)
                    return;

                view["medicineID"] = row["medicineID"];
                view["medicineName"] = row["name"];
                view["latinName"] = row["latinName"];
                view["unitType"] = row["unitType"];
                view["shape"] = row["shape"];
                view["validDate"] = row["validDate"];
                view["medicineTypeName"] = row["medicineTypeName"];
                view["incTypeID"] = row["incTypeID"];
                view["incTypeName"] = row["incTypeName"];
                view["price"] = spinEditPrice.EditValue;
                view["count"] = spinEditCount.EditValue;
                view.EndEdit();
            }

            private void calcPrice()
            {
                spinEditSumPrice.Value = spinEditCount.Value * spinEditPrice.Value;
            }
            
            
            private void addRow()
            {
                JournalDataSet.Instance.MotDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                JournalDataSet.MotDetailBindingSource.AddNew();
                clearMedicine();
                gridViewDetail.Focus();
            }

            private void deleteRow()
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн шилжүүлэг устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    JournalDataSet.MotDetailBindingSource.RemoveCurrent();
                    setValueFromGrid();
                }
            }

            private void showOrder()
            {
                if (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.EditValue.ToString().Equals(""))
                {
                    treeListLookUpEditWarehouse.ErrorText = "Шилжүүлэх агуулахыг сонгоно уу";
                    treeListLookUpEditWarehouse.Focus();
                }
                else
                {
                    OrderDownForm.Instance.showForm();
                }
            }

            public void downloadOrder(DataRow[] rows, string id, string incWarehouseID)
            {
                if (rows.Length > 0)
                {
                    treeListLookUpEditIncWarehouse.EditValue = incWarehouseID;
                    treeListLookUpEditIncWarehouse.Enabled = false;
                    medicineOrderID = id;
                    textEditDocNumber.Text = medicineOrderID;

                    if(gridViewDetail.RowCount > 0 && (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.Text.Equals("")))
                        JournalDataSet.MotDetailBindingSource.RemoveCurrent();

                    for (int i = 0, j = 0; i < rows.Length; i++)
                    {
                        bool exist = false;
                        for (j = 0; j < gridViewDetail.RowCount; j++)
                        {
                            if (gridViewDetail.GetDataRow(j)["medicineID"].ToString().Equals(rows[i]["medicineID"].ToString()))
                            {
                                exist = true;
                                break;
                            }
                        }
                        if (exist)
                        {
                            gridViewDetail.GetDataRow(j)["count"] = rows[i]["count"];
                            gridViewDetail.GetDataRow(j).EndEdit();
                        }
                        else
                        {
                            DataRowCollection medicineRows = JournalDataSet.Instance.MedicineBal.Rows;
                            for (j = 0; j < medicineRows.Count; j++)
                            {
                                if (medicineRows[j]["medicineID"].ToString().Equals(rows[i]["medicineID"].ToString()))
                                {
                                    exist = true;
                                    break;
                                }
                            }
                            if (exist)
                            {
                                JournalDataSet.Instance.MotDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                                DataRowView view = (DataRowView)JournalDataSet.MotDetailBindingSource.AddNew();

                                view["medicineID"] = medicineRows[j]["medicineID"];
                                view["medicineName"] = medicineRows[j]["name"];
                                view["latinName"] = medicineRows[j]["latinName"];
                                view["unitType"] = medicineRows[j]["unitType"];
                                view["shape"] = medicineRows[j]["shape"];
                                view["validDate"] = medicineRows[j]["validDate"];
                                view["medicineTypeName"] = medicineRows[j]["medicineTypeName"];
                                view["incTypeID"] = medicineRows[j]["incTypeID"];
                                view["incTypeName"] = medicineRows[j]["incTypeName"];
                                view["price"] = medicineRows[j]["price"];
                                view["count"] = rows[i]["count"];
                                view.EndEdit();
                            }
                        }
                    }
                    setValueFromGrid();
                }
            }

            private bool check()
            {
                //Instance.Validate();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.EditValue.ToString().Equals(""))
                {
                    errorText = "Няравыг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
                if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.EditValue.ToString().Equals("")))
                {
                    errorText = "Агуулахыг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWarehouse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWarehouse.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.EditValue.ToString().Equals("")))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }
                if (dateEditExpDate.EditValue == DBNull.Value || dateEditExpDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditExpDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditExpDate.Focus();
                    }
                }
                if (gridLookUpEditIncUser.EditValue == DBNull.Value || gridLookUpEditIncUser.EditValue == null || gridLookUpEditIncUser.EditValue.ToString().Equals(""))
                {
                    errorText = "Няравыг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditIncUser.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditIncUser.Focus();
                }
                if (radioGroupIncType.SelectedIndex == 0 && (treeListLookUpEditIncWarehouse.EditValue == DBNull.Value || treeListLookUpEditIncWarehouse.EditValue == null || treeListLookUpEditIncWarehouse.EditValue.ToString().Equals("")))
                {
                    errorText = "Агуулахыг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditIncWarehouse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditIncWarehouse.Focus();
                    }
                }
                if (radioGroupIncType.SelectedIndex == 1 && (treeListLookUpEditIncWard.EditValue == DBNull.Value || treeListLookUpEditIncWard.EditValue == null || treeListLookUpEditIncWard.EditValue.ToString().Equals("")))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditIncWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditIncWard.Focus();
                    }
                }
                
                if (isRight && gridLookUpEditUser.EditValue.ToString().Equals(gridLookUpEditIncUser.EditValue.ToString()) && 
                    radioGroupType.SelectedIndex == radioGroupIncType.SelectedIndex &&
                    ((radioGroupIncType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue.ToString().Equals(treeListLookUpEditIncWarehouse.EditValue.ToString())) ||
                     (radioGroupIncType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue.ToString().Equals(treeListLookUpEditIncWard.EditValue.ToString()))) )
                {
                    errorText = "Нярав, Хүлээн авагч хоёр ижил байна";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditIncWarehouse.ErrorText = errorText;
                    isRight = false;
                    treeListLookUpEditIncWarehouse.Focus();
                }

                if (isRight)
                    isRight = checkMedicine();
                else
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private bool checkMedicine()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                    
                }
                if (spinEditCount.Value == 0)
                {
                    errorText = "Тоо, хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditCount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditCount.Focus();
                    }
                }
                for (int i = 0; isRight && i < gridViewDetail.RowCount; i++)
                {
                    if (gridViewDetail.FocusedRowHandle != i && gridLookUpEditMedicine.EditValue.ToString().Equals(gridViewDetail.GetDataRow(i)["medicinePrice"].ToString()))
                    {
                        errorText = "Эм давхацсан байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditMedicine.ErrorText = errorText;
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                        break;
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    if (ProgramUtil.checkLockMedicine(dateEditExpDate.EditValue.ToString()))
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                        DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                        DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                        DataRowView incUser = (DataRowView)gridLookUpEditIncUser.GetSelectedDataRow();
                        DataRowView incWard = (DataRowView)treeListLookUpEditIncWard.GetSelectedDataRow();
                        DataRowView incWarehouse = (DataRowView)treeListLookUpEditIncWarehouse.GetSelectedDataRow();

                        MedicineMotForm.Instance.currView["userName"] = user["fullName"];
                        MedicineMotForm.Instance.currView["roleName"] = user["roleName"];
                        MedicineMotForm.Instance.currView["incUserName"] = incUser["fullName"];
                        MedicineMotForm.Instance.currView["incRoleName"] = incUser["roleName"];
                        if (radioGroupType.SelectedIndex == 0)
                        {
                            MedicineMotForm.Instance.currView["wardID"] = null;
                            MedicineMotForm.Instance.currView["warehouseName"] = warehouse["name"];
                            MedicineMotForm.Instance.currView["warehouseDesc"] = warehouse["description"];
                        }
                        else if (radioGroupType.SelectedIndex == 1)
                        {
                            MedicineMotForm.Instance.currView["warehouseID"] = null;
                            MedicineMotForm.Instance.currView["warehouseName"] = ward["name"];
                            MedicineMotForm.Instance.currView["warehouseDesc"] = ward["description"];
                        }
                        if (radioGroupIncType.SelectedIndex == 0)
                        {
                            MedicineMotForm.Instance.currView["incWardID"] = null;
                            MedicineMotForm.Instance.currView["incWarehouseName"] = incWarehouse["name"];
                            MedicineMotForm.Instance.currView["incWarehouseDesc"] = incWarehouse["description"];
                        }
                        else if (radioGroupIncType.SelectedIndex == 1)
                        {
                            MedicineMotForm.Instance.currView["incWarehouseID"] = null;
                            MedicineMotForm.Instance.currView["incWarehouseName"] = incWard["name"];
                            MedicineMotForm.Instance.currView["incWarehouseDesc"] = incWard["description"];
                        }
                        MedicineMotForm.Instance.currView["medicineOrderID"] = medicineOrderID;

                        for (int i = 0; i < gridViewDetail.RowCount; i++)
                        {
                            DataRow row = gridViewDetail.GetDataRow(i);

                            row["date"] = dateEditExpDate.EditValue;
                            row["docNumber"] = textEditDocNumber.EditValue;
                            row["description"] = memoEditDescription.EditValue;
                            row["userID"] = gridLookUpEditUser.EditValue;
                            row["incUserID"] = gridLookUpEditIncUser.EditValue;
                            row["medicineOrderID"] = medicineOrderID;

                            if (radioGroupType.SelectedIndex == 0)
                            {
                                row["wardID"] = null;
                                row["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                                row["warehouseName"] = warehouse["name"];
                                row["warehouseDesc"] = warehouse["description"];
                            }
                            else if (radioGroupType.SelectedIndex == 1)
                            {
                                row["warehouseID"] = null;
                                row["wardID"] = treeListLookUpEditWard.EditValue;
                                row["warehouseName"] = ward["name"];
                                row["warehouseDesc"] = ward["description"];
                            }
                            if (radioGroupIncType.SelectedIndex == 0)
                            {
                                row["incWardID"] = null;
                                row["incwarehouseID"] = treeListLookUpEditIncWarehouse.EditValue;
                                row["incWarehouseName"] = incWarehouse["name"];
                                row["incWarehouseDesc"] = incWarehouse["description"];
                            }
                            else if (radioGroupIncType.SelectedIndex == 1)
                            {
                                row["incWarehouseID"] = null;
                                row["incWardID"] = treeListLookUpEditIncWard.EditValue;
                                row["incWarehouseName"] = incWard["name"];
                                row["incWarehouseDesc"] = incWard["description"];
                            }
                        }

                        JournalDataSet.MotJournalBindingSource.EndEdit();
                        JournalDataSet.MotJournalTableAdapter.Update(JournalDataSet.Instance.MotJournal);

                        JournalDataSet.MotDetailBindingSource.EndEdit();
                        JournalDataSet.MotDetailTableAdapter.Update(JournalDataSet.Instance.MotDetail);

                        ProgramUtil.closeWaitDialog();
                        Close();
                    }
                    else
                    {
                        XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
                    }                    
                }
            }
            
            
            // Сонгосон шилжүүлэгчийг буцаана
            public string getWarehouse()
            {
                return treeListLookUpEditWarehouse.EditValue.ToString();
            }

            // Мөрийн тоог буцаана
            public int getRowCount()
            {
                return gridViewDetail.RowCount;
            }
            
            
            public bool existMedicine(string medicineID)
            {
                for (int i = 0; i < gridViewDetail.RowCount; i++)
                {
                    if (medicineID.Equals(gridViewDetail.GetDataRow(i)["medicinePrice"].ToString()))
                    {
                        return true;
                    }
                }
                return false;
            }

            public void addMedicine(DataRowView view, object incTypeID, string incTypeText, decimal count, decimal price)
            {
                DataRowView row = (DataRowView)JournalDataSet.MedicineBalBindingSource.AddNew();
                row["medicineID"] = view["id"];
                row["name"] = view["name"];
                row["latinName"] = view["latinName"];
                row["medicineTypeName"] = view["medicineTypeName"];
                row["unitType"] = view["unitType"];
                row["barcode"] = view["barcode"];
                row["shape"] = view["shape"];
                row["validDate"] = view["validDate"];
                row["incTypeID"] = incTypeID;
                row["incTypeName"] = incTypeText;
                row["price"] = price;
                row["count"] = count;
                row.EndEdit();

                gridLookUpEditMedicine.EditValue = view["id"].ToString() + incTypeID.ToString() + price;

                spinEditCount.Value = count;
                spinEditPrice.Value = price;
                endEdit();
            }

        #endregion

    }
}