﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.inc;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.gui.core.info.medicine;
using System.Data.SqlClient;




namespace Pharmacy2016.gui.core.journal.exp
{
    /*
     * Захиалгын хуудаснаас сонгон татах цонх.
     * 
     */

    public partial class OrderDownForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static OrderDownForm INSTANCE = new OrderDownForm();

        public static OrderDownForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private int type = 0;

        private OrderDownForm()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            dateEditExpDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditExpDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;
            dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
            dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

            initDataSource();
            initError();
            reload(false);
        }

        private void initDataSource()
        {
            gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
            treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
            treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
            gridLookUpEditOrder.Properties.DataSource = JournalDataSet.OrderGoodsOtherBindingSource;
            gridControlOrder.DataSource = JournalDataSet.Instance.OrderGoodsOtherDetail1;
            repositoryItemGridLookUpEdit2.DataSource = JournalDataSet.MedicineBalBindingSource;
        }

        private void initError()
        {
            dateEditExpDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;

            dateEditEnd.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditOrder.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm(int type, XtraForm form)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }
                this.type = type;
                dateEditExpDate.DateTime = DateTime.Now;
                if (type == 1)
                {
                    gridColumnPrice.OptionsColumn.ReadOnly = true;
                    gridColumnPrice.AppearanceCell.BackColor = Color.Transparent;
                    dateEditStart.DateTime = MedicineExpForm.Instance.getStartDate();
                    dateEditEnd.DateTime = MedicineExpForm.Instance.getEndDate();
                    repositoryItemGridLookUpEdit2.DataSource = JournalDataSet.MedicineBalBindingSource;
                    JournalDataSet.MedicineBalBindingSource.Filter = "count>0";
                    repositoryItemGridLookUpEdit2.ValueMember = "medicinePrice";
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridColumnDesc.Visible = true;
                    gridColumn2.Visible = true;
                    gridColumn2.VisibleIndex = 1;
                    gridColumn28.Visible = true;
                    gridColumn28.VisibleIndex = 2;
                    //gridColumnPrice.re
                }
                else if (type == 2)
                {
                    gridColumnPrice.OptionsColumn.ReadOnly = false;
                    gridColumnPrice.AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                    dateEditStart.DateTime = MedicineIncForm.Instance.getStartDate();
                    dateEditEnd.DateTime = MedicineIncForm.Instance.getEndDate();
                    repositoryItemGridLookUpEdit2.DataSource = JournalDataSet.MedicineOtherBindingSource;
                    JournalDataSet.MedicineBalBindingSource.Filter = "count>0";
                    repositoryItemGridLookUpEdit2.ValueMember = "id";
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridColumnDesc.Visible = false;
                    gridColumn2.Visible = false;
                    gridColumn28.Visible = false;
                }
                reload(false);

                ShowDialog(form);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clearData();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void clearData()
        {
            clearOrder();

            JournalDataSet.Instance.OrderGoodsOtherDetail1.Clear();
            JournalDataSet.Instance.OrderGoodsOtherDetail.Clear();
            JournalDataSet.Instance.OrderGoodsOther.Clear();
            //JournalDataSet.Instance.Bal.Clear();
        }

        private void clearOrder()
        {
            checkEditChooseAll.Checked = false;
            dateEditExpDate.EditValue = null;
            gridLookUpEditUser.EditValue = null;
            treeListLookUpEditWard.EditValue = null;
            treeListLookUpEditWarehouse.EditValue = null;
            gridLookUpEditOrder.EditValue = null;
        }

        private void OrderDownForm_Shown(object sender, EventArgs e)
        {
            if (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
            {
                gridLookUpEditUser.Properties.ReadOnly = false;
                gridLookUpEditUser.Focus();
            }
            else
            {
                gridLookUpEditUser.Properties.ReadOnly = true;
                gridLookUpEditUser.EditValue = UserUtil.getUserId();
                radioGroupType.Focus();
            }
        }

        private void gridControlOrder_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            /* if (e.Button.ButtonType == NavigatorButtonType.Custom)
             {
                 if (String.Equals(e.Button.Tag, "add"))
                 {
                     addNotList();
                 }
                 if (String.Equals(e.Button.Tag, "delete"))
                 {
                     deleteSelectedRow();
                 }
                 
             }
             **/
        }

        #endregion

        #region Формын event

        private void dateEditExpDate_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    dateEditExpDate.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditUser.Focus();
                }
            }
        }

        private void dateEditExpDate_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible && type == 1)
                reloadMedicine();
        }

        private void gridLookUpEditUser_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditUser.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    if (radioGroupType.SelectedIndex == 0)
                        treeListLookUpEditWarehouse.Focus();
                    else
                        treeListLookUpEditWard.Focus();
                }
            }
        }

        private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible && gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null)
            {
                reloadOrder();
                if (type == 1)
                    reloadMedicine();

                JournalDataSet.WarehouseOtherBindingSource.Filter = UserUtil.getUserWarehouse(gridLookUpEditUser.EditValue.ToString());
                JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(gridLookUpEditUser.EditValue.ToString());
            }
        }

        private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Visible)
                changeType();
        }

        private void treeListLookUpEditWarehouse_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    ((TreeListLookUpEdit)sender).ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditOrder.Focus();
                }
            }
        }

        private void treeListLookUpEditWard_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditWard.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditOrder.Focus();
                }
            }
        }


        private void dateEditStart_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    dateEditStart.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    dateEditEnd.Focus();
                }
            }
        }

        private void dateEditEnd_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    dateEditEnd.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditOrder.Focus();
                }
            }
        }

        private void gridLookUpEditOrder_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditOrder.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridControlOrder.Focus();
                }
            }
        }

        private void gridLookUpEditOrder_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            if (Visible)
            {
                DataRowView row = (DataRowView)gridLookUpEditOrder.GetSelectedDataRow();
                if (e.Value != DBNull.Value && e.Value != null && row != null)
                {
                    e.DisplayText = row["userName"].ToString();
                }
                else
                    e.DisplayText = "";
            }
        }

        private void gridLookUpEditOrder_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible)
                changeOrder();
        }

        private void gridLookUpEditOrder_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "reload"))
                reloadOrder();
        }

        private void checkEditChooseAll_CheckedChanged(object sender, EventArgs e)
        {
            giveAll();
        }

        private void gridViewOrderDetail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == gridColumnMedicine)
            {
                DataRow medicine = getGiveMedicine(gridViewOrderDetail.GetRowCellValue(e.RowHandle, gridColumnMedicine).ToString(), gridViewOrderDetail.GetRowCellValue(e.RowHandle, gridColumnPrice).ToString());
                if (medicine != null)
                {
                    if (type == 1)
                    {
                        DataRow[] row = JournalDataSet.Instance.MedicineBal.Select("medicinePrice = '" + e.Value + "'");
                        if (Convert.ToString(row[0]["packageName"]) != gridViewOrderDetail.GetRowCellValue(e.RowHandle, gridColumn4).ToString())
                        {
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["medicinePrice"] = null;
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["giveMedicineID"] = null;
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["confirmMedicineID"] = null;
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["incTypeID"] = 0;
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["price"] = 0;
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["giveCount"] = 0;
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["endCount"] = 0;
                            XtraMessageBox.Show("Тухайн багцын эмнүүдээс сонгоно уу.");
                        }


                        else
                        {
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["giveMedicineID"] = row[0]["medicineID"];
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["confirmMedicineID"] = row[0]["medicineID"];
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["incTypeID"] = 4;
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["price"] = row[0]["price"];
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["giveCount"] = 0;
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["endCount"] = row[0]["count"];
                        }
                    }
                    else if (type == 2)
                    {
                        DataRow[] row = JournalDataSet.Instance.MedicineOther.Select("id = '" + e.Value + "'");
                        if (Convert.ToString(row[0]["packageName"]) != gridViewOrderDetail.GetRowCellValue(e.RowHandle, gridColumn4).ToString())
                        {
                            gridViewOrderDetail.SetFocusedRowCellValue(gridColumnMedicine, null);
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["giveMedicineID"] = null;
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["confirmMedicineID"] = null;
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["incTypeID"] = 0;
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["giveCount"] = 0;
                            XtraMessageBox.Show("Тухайн багцын эмнүүдээс сонгоно уу.");
                        }
                        else
                        {
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["giveMedicineID"] = row[0]["id"];
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["confirmMedicineID"] = row[0]["id"];
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["incTypeID"] = 4;
                        }
                    }
                }
            }
            if (e.Column == gridColumnGiveCount)
            {
                if (!string.IsNullOrEmpty(gridViewOrderDetail.GetRowCellValue(e.RowHandle, gridColumnMedicine).ToString()))
                {
                    if (type == 1)
                    {
                        DataRow[] row = JournalDataSet.Instance.MedicineBal.Select("medicinePrice = '" + gridViewOrderDetail.GetRowCellValue(e.RowHandle, gridColumnMedicine).ToString() + "'");
                        gridViewOrderDetail.GetDataRow(e.RowHandle)["endCount"] = row[0]["count"];
                        if (Convert.ToDecimal(gridViewOrderDetail.GetDataRow(e.RowHandle)["giveCount"]) <= Convert.ToDecimal(gridViewOrderDetail.GetDataRow(e.RowHandle)["confirmCount"]))
                        {
                            if (gridViewOrderDetail.GetDataRow(e.RowHandle)["endCount"] != null)
                            {
                                if (Convert.ToDecimal(gridViewOrderDetail.GetDataRow(e.RowHandle)["giveCount"]) > Convert.ToDecimal(gridViewOrderDetail.GetDataRow(e.RowHandle)["endCount"]))
                                {
                                    XtraMessageBox.Show("Үлдэгдэл хүрэлцэхгүй байна. Байгаа үлдэгдлийн хэмжээгээр бичлээ");
                                    gridViewOrderDetail.GetDataRow(e.RowHandle)["giveCount"] = gridViewOrderDetail.GetDataRow(e.RowHandle)["endCount"];
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToDecimal(gridViewOrderDetail.GetDataRow(e.RowHandle)["confirmCount"]) > Convert.ToDecimal(gridViewOrderDetail.GetDataRow(e.RowHandle)["endCount"]))
                            {
                                XtraMessageBox.Show("Үлдэгдэл хүрэлцэхгүй байна. Байгаа үлдэгдлийн хэмжээгээр бичлээ");
                                gridViewOrderDetail.GetDataRow(e.RowHandle)["giveCount"] = gridViewOrderDetail.GetDataRow(e.RowHandle)["endCount"];
                            }
                            else
                            {
                                XtraMessageBox.Show("Баталсан тооноос их байна. Баталсан тооны хэмжээгээр бичлээ");
                                gridViewOrderDetail.GetDataRow(e.RowHandle)["giveCount"] = gridViewOrderDetail.GetDataRow(e.RowHandle)["confirmCount"];
                            }
                        }
                    }
                    if (type == 2)
                    {
                        if (Convert.ToDecimal(gridViewOrderDetail.GetDataRow(e.RowHandle)["giveCount"]) > Convert.ToDecimal(gridViewOrderDetail.GetDataRow(e.RowHandle)["confirmCount"]))
                        {
                            XtraMessageBox.Show("Баталсан тооноос их байна. Баталсан тооны хэмжээгээр бичлээ");
                            gridViewOrderDetail.GetDataRow(e.RowHandle)["giveCount"] = Convert.ToDecimal(gridViewOrderDetail.GetDataRow(e.RowHandle)["confirmCount"]);
                        }
                    }
                }
                else {
                    XtraMessageBox.Show("Эмээ сонгоно уу.");
                    gridViewOrderDetail.GetDataRow(e.RowHandle)["giveCount"] = 0;
                }              
            }
        }


        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            reload(true);
        }

        private void simpleButtonDownload_Click(object sender, EventArgs e)
        {
            download();
        }

        #endregion

        #region Формын функц

        private void giveAll()
        {           
                if (type == 1)
                {
                    if (checkEditChooseAll.Checked)
                    {
                        for (int i = 0; i < gridViewOrderDetail.RowCount; i++)
                        {
                            DataRow match = getGiveMedicine(gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine).ToString(), gridViewOrderDetail.GetRowCellValue(i, gridColumnPrice).ToString());
                            if (match != null)
                            {
                                if (Convert.ToDouble(gridViewOrderDetail.GetRowCellValue(i, gridColumnConfirm)) <= Convert.ToDouble(gridViewOrderDetail.GetRowCellValue(i, gridColumnEndCount)))
                                {
                                    gridViewOrderDetail.SetRowCellValue(i, gridColumnGiveCount, gridViewOrderDetail.GetRowCellValue(i, gridColumnConfirm));
                                }
                                else
                                {
                                    gridViewOrderDetail.SetRowCellValue(i, gridColumnGiveCount, gridViewOrderDetail.GetRowCellValue(i, gridColumnEndCount));
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < gridViewOrderDetail.RowCount; i++)
                        {
                            DataRow match = getGiveMedicine(gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine).ToString(), gridViewOrderDetail.GetRowCellValue(i, gridColumnPrice).ToString());
                            if (match != null)
                            {
                                gridViewOrderDetail.SetRowCellValue(i, gridColumnGiveCount, 0);
                            }

                        }
                    }
                }
                if (type == 2)
                {
                    if (checkEditChooseAll.Checked)
                    {
                        for (int i = 0; i < gridViewOrderDetail.RowCount; i++)
                        {
                            DataRow match = getGiveMedicine(gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine).ToString(), gridViewOrderDetail.GetRowCellValue(i, gridColumnPrice).ToString());
                            if (match != null)
                            {
                                gridViewOrderDetail.SetRowCellValue(i, gridColumnGiveCount, gridViewOrderDetail.GetRowCellValue(i, gridColumnConfirm));
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < gridViewOrderDetail.RowCount; i++)
                        {
                            DataRow match = getGiveMedicine(gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine).ToString(), gridViewOrderDetail.GetRowCellValue(i, gridColumnPrice).ToString());
                            if (match != null)
                            {
                                gridViewOrderDetail.SetRowCellValue(i, gridColumnGiveCount, 0);
                            }
                        }
                    }
                }         
        }

        private bool checkOrder()
        {
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();

            if (dateEditStart.Text == "")
            {
                errorText = "Эхлэх огноог оруулна уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }
            if (dateEditEnd.Text == "")
            {
                errorText = "Дуусах огноог оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditEnd.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    dateEditEnd.Focus();
                }
            }
            if (!gridLookUpEditUser.Properties.ReadOnly && (gridLookUpEditUser.EditValue == null || gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.Text.Equals("")))
            {
                errorText = "Няравыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditUser.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
            }
            if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
            {
                errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }

            if (!isRight)
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        private void changeType()
        {
            int x = labelControl14.Location.X;
            int orderType = 0;
            if (radioGroupType.SelectedIndex == 0)
            {
                labelControlWarehouse.Location = new Point(x + 38, 121);
                treeListLookUpEditWard.Location = new Point(x + 105, 12);
                treeListLookUpEditWarehouse.Location = new Point(x + 105, 118);
                treeListLookUpEditWard.EditValue = null;
                treeListLookUpEditWard.Text = "";
                treeListLookUpEditWard.Visible = false;
                treeListLookUpEditWarehouse.Visible = true;
                labelControlWarehouse.Text = "Эмийн сан*:";
                orderType = 0;
            }
            else if (radioGroupType.SelectedIndex == 1)
            {
                labelControlWarehouse.Location = new Point(x + 61, 121);
                treeListLookUpEditWard.Location = new Point(x + 105, 118);
                treeListLookUpEditWarehouse.Location = new Point(x + 105, 12);
                treeListLookUpEditWarehouse.EditValue = null;
                treeListLookUpEditWarehouse.Text = "";
                treeListLookUpEditWard.Visible = true;
                treeListLookUpEditWarehouse.Visible = false;
                labelControlWarehouse.Text = "Тасаг*:";
                orderType = 1;
            }
            gridLookUpEditOrder.EditValue = null;
            gridLookUpEditOrder.Text = "";

            string user = "";
            if (gridLookUpEditUser.Visible) user = gridLookUpEditUser.EditValue.ToString();
            else user = UserUtil.getUserId();
            JournalDataSet.Instance.EnforceConstraints = false;

            JournalDataSet.OrderGoodsOtherTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsOther, HospitalUtil.getHospitalId(), user, orderType, dateEditStart.Text, dateEditEnd.Text);
            JournalDataSet.OrderGoodsOtherDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsOtherDetail, HospitalUtil.getHospitalId(), user, orderType, dateEditStart.Text, dateEditEnd.Text);
            JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(), user, dateEditStart.Text, dateEditEnd.Text);

        }

        private void reloadOrder()
        {
            if (checkOrder())
            {
                string user = "";
                if (gridLookUpEditUser.Visible)
                    user = gridLookUpEditUser.EditValue.ToString();
                else
                    user = UserUtil.getUserId();

                int orderType = type == 1 ? 0 : 1;
                JournalDataSet.Instance.EnforceConstraints = false;

                JournalDataSet.OrderGoodsOtherTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsOther, HospitalUtil.getHospitalId(), user, orderType, dateEditStart.Text, dateEditEnd.Text);
                JournalDataSet.OrderGoodsOtherDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsOtherDetail, HospitalUtil.getHospitalId(), user, orderType, dateEditStart.Text, dateEditEnd.Text);
                JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(), user, dateEditStart.Text, dateEditEnd.Text);
            }
        }

        private void reload(bool isReload)
        {
            if (type == 1)
            {
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
            }
            else if (type == 2)
            {
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                if (isReload || JournalDataSet.Instance.MedicineOther.Rows.Count == 0)
                    JournalDataSet.MedicineOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOther, HospitalUtil.getHospitalId(), "");
                DateTime thisDay = DateTime.Today;
                if (isReload || PlanDataSet.Instance.TenderDetail.Rows.Count == 0)
                    PlanDataSet.TenderDetailTableAdapter.Fill(PlanDataSet.Instance.TenderDetail, HospitalUtil.getHospitalId(), "2001-01-01", thisDay.ToString("d"));
            }

            if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
            if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
            JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
        }

        private void reloadMedicine()
        {
            if (Visible && dateEditExpDate.EditValue != DBNull.Value && dateEditExpDate.EditValue != null && !dateEditExpDate.Text.Equals("") &&
            gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null && !gridLookUpEditUser.Text.Equals("") &&
            ((radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && !treeListLookUpEditWarehouse.Text.Equals("")) ||
            (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && !treeListLookUpEditWard.Text.Equals(""))))
            {
                JournalDataSet.MedicineBalTableAdapter.Fill(JournalDataSet.Instance.MedicineBal, HospitalUtil.getHospitalId(), gridLookUpEditUser.EditValue.ToString(), radioGroupType.SelectedIndex, (radioGroupType.SelectedIndex == 0 ? treeListLookUpEditWarehouse.EditValue.ToString() : ""), (radioGroupType.SelectedIndex == 1 ? treeListLookUpEditWard.EditValue.ToString() : ""), dateEditExpDate.Text);
            }
        }

        private void changeOrder()
        {
            JournalDataSet.Instance.OrderGoodsOtherDetail1.Clear();
            DataRow[] rows = JournalDataSet.Instance.OrderGoodsDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() +
                "' AND orderGoodsID = '" + gridLookUpEditOrder.EditValue + "'");
            for (int i = 0; i < rows.Length; i++)
            {
                DataRow newRow = JournalDataSet.Instance.OrderGoodsOtherDetail1.NewRow();
                newRow["orderGoodsID"] = rows[i]["orderGoodsID"];
                newRow["id"] = rows[i]["id"];
                newRow["medicineID"] = rows[i]["medicineID"];
                newRow["medicineName"] = rows[i]["medicineName"];
                newRow["latinName"] = rows[i]["latinName"];
                newRow["unitType"] = rows[i]["unitType"];
                newRow["shape"] = rows[i]["shape"];
                newRow["quantity"] = rows[i]["quantity"];
                newRow["count"] = rows[i]["count"];
                newRow["confirmCount"] = rows[i]["confirmCount"];
                newRow["giveMedicineID"] = rows[i]["giveMedicineID"];
                newRow["giveCount"] = rows[i]["giveCount"];
                newRow["description"] = "Хоосон";
                newRow.EndEdit();
                JournalDataSet.Instance.OrderGoodsOtherDetail1.Rows.Add(newRow);
            }
            if (type == 1)
            {
                if (gridColumnPrice.OptionsColumn.ReadOnly)
                {
                    gridColumnPrice.OptionsColumn.ReadOnly = false;
                }
                for (int i = 0; i < gridViewOrderDetail.RowCount; i++)
                {
                    DataRow row = gridViewOrderDetail.GetDataRow(i);
                    DataRow[] medicine = JournalDataSet.Instance.MedicineBal.Select("medicineID = '" + row["medicineID"] + "'");
                    if (medicine.Length > 0)
                    {
                        row["medicinePrice"] = medicine[0]["medicinePrice"];
                        row["price"] = medicine[0]["price"];
                    }

                }
                gridColumnPrice.OptionsColumn.ReadOnly = true;
            }
            if (type == 2)
            {
                for (int i = 0; i < gridViewOrderDetail.RowCount; i++)
                {
                    DataRow row = gridViewOrderDetail.GetDataRow(i);
                    DataRow[] row_order = JournalDataSet.Instance.OrderGoodsOther.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND ID = '" + gridLookUpEditOrder.EditValue + "'");
                    if (row_order.Length > 0)
                    {
                        DataRow[] row_tend = PlanDataSet.Instance.TenderDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND tenderJournalID = '" + row_order[0]["tenderID"] + "' AND medicineID = '" + row["medicineID"] + "'");
                        if (row_tend.Length > 0)
                        {
                            //XtraMessageBox.Show(row_tend[0]["medicinePrice"].ToString());
                            if (gridColumnPrice.OptionsColumn.ReadOnly)
                            {
                                gridColumnPrice.OptionsColumn.ReadOnly = false;
                            }
                            gridViewOrderDetail.SetRowCellValue(i, gridColumnPrice, row_tend[0]["medicinePrice"]);
                            //gridColumnPrice.OptionsColumn.ReadOnly = true;
                        }
                    }
                }
            }

        }

        private bool check()
        {
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();
            if (dateEditExpDate.EditValue == DBNull.Value || dateEditExpDate.EditValue == null || dateEditExpDate.Text.Equals(""))
            {
                errorText = "Огноог оруулна уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditExpDate.ErrorText = errorText;
                isRight = false;
                dateEditExpDate.Focus();
            }
            if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.Text.Equals(""))
            {
                errorText = "Няравыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditUser.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
            }
            if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.Text.Equals("")))
            {
                errorText = "Агуулахыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWarehouse.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWarehouse.Focus();
                }
            }
            if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals("")))
            {
                errorText = "Тасгийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWard.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWard.Focus();
                }
            }
            if (gridLookUpEditOrder.EditValue == DBNull.Value || gridLookUpEditOrder.EditValue == null || gridLookUpEditOrder.Text.Equals(""))
            {
                errorText = "Захиалгын хуудсыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditOrder.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditOrder.Focus();
                }
            }
            if (isRight)
            {
                bool exist = false;
                for (int i = 0; i < gridViewOrderDetail.RowCount; i++)
                {
                    if (gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine) != null && gridViewOrderDetail.GetRowCellValue(i, "medicinePrice") != null && gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine) != DBNull.Value && Convert.ToDouble(gridViewOrderDetail.GetRowCellValue(i, gridColumnGiveCount)) > 0)
                    {
                        exist = true;
                        break;
                    }
                }
                if (exist)
                {
                    exist = false;
                    for (int i = 0; i < gridViewOrderDetail.RowCount; i++)
                    {
                        if (gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine) != null && gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine) != DBNull.Value && Convert.ToDouble(gridViewOrderDetail.GetRowCellValue(i, gridColumnGiveCount)) > 0)
                        {
                            for (int j = i + 1; j < gridViewOrderDetail.RowCount; j++)
                            {
                                if (gridViewOrderDetail.GetRowCellValue(j, gridColumnMedicine) != null && gridViewOrderDetail.GetRowCellValue(j, gridColumnMedicine) != DBNull.Value && Convert.ToDouble(gridViewOrderDetail.GetRowCellValue(j, gridColumnGiveCount)) > 0 &&
                                    gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine).ToString().Equals(gridViewOrderDetail.GetRowCellValue(j, gridColumnMedicine).ToString()))
                                {
                                    gridViewOrderDetail.FocusedRowHandle = j;
                                    exist = true;
                                    break;
                                }
                            }
                            if (exist)
                                break;
                        }
                    }
                    if (exist)
                    {
                        isRight = false;
                        errorText = "Шилжүүлэх эм давхацсан байна";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridViewOrderDetail.Focus();
                    }
                }
                else
                {
                    isRight = exist;
                    errorText = "Шилжүүлэх эм болон тоо хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridViewOrderDetail.Focus();
                }
            }

            if (!isRight && !text.Equals(""))
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        private void download()
        {
            if (check())
            {
                ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                DataRowView order = (DataRowView)gridLookUpEditOrder.GetSelectedDataRow();

                string id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());

                if (type == 1)
                {
                    string incid = JournalDataSet.QueriesTableAdapter.get_new_id(order["userID"].ToString());
                    JournalDataSet.Instance.IncJournal.idColumn.DefaultValue = incid;
                    JournalDataSet.Instance.IncDetail.incJournalIDColumn.DefaultValue = incid;
                    DataRowView IncView = (DataRowView)JournalDataSet.IncJournalBindingSource.AddNew();

                    IncView["hospitalID"] = HospitalUtil.getHospitalId();
                    IncView["actionUserID"] = UserUtil.getUserId();
                    IncView["date"] = dateEditExpDate.EditValue;
                    IncView["userID"] = order["userID"];
                    IncView["userName"] = order["userName"];
                    IncView["roleName"] = order["roleName"];
                    IncView["type"] = order["type"];
                    IncView["incTypeID"] = 4;


                    IncView["incTypeName"] = "Шилжүүлэг";
                    if (radioGroupType.SelectedIndex == 0)
                    {
                        IncView["wardID"] = null;
                        IncView["warehouseID"] = order["warehouseID"];
                        IncView["warehouseName"] = order["warehouseName"];
                        IncView["warehouseDesc"] = order["warehouseDesc"];
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {
                        IncView["wardID"] = order["wardID"];
                        IncView["warehouseID"] = null;
                        IncView["warehouseName"] = order["warehouseName"];
                        IncView["warehouseDesc"] = order["warehouseDesc"];
                    }
                    IncView["orderGoodsID"] = order["id"];
                    IncView["tenderID"] = null;
                    IncView["supplierID"] = gridLookUpEditUser.EditValue;
                    IncView["supplierName"] = gridLookUpEditUser.Text;
                    IncView.EndEdit();

                    long newINCID = Convert.ToInt64(JournalDataSet.QueriesTableAdapter.get_new_id(order["userID"].ToString()));
                    for (int i = 0; i < gridViewOrderDetail.RowCount; i++, newINCID++)
                    {
                        if (gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine) != null &&
                            gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine) != DBNull.Value &&
                            Convert.ToDouble(gridViewOrderDetail.GetRowCellValue(i, gridColumnGiveCount)) > 0)
                        {
                            JournalDataSet.Instance.IncDetail.idColumn.DefaultValue = newINCID;
                            DataRowView incnewRow = (DataRowView)JournalDataSet.IncDetailBindingSource.AddNew();

                            incnewRow["date"] = dateEditExpDate.EditValue;
                            incnewRow["userID"] = order["userID"];
                            incnewRow["hospitalID"] = HospitalUtil.getHospitalId();
                            incnewRow["actionUserID"] = UserUtil.getUserId();
                            incnewRow["userName"] = order["userName"];
                            incnewRow["roleName"] = order["roleName"];
                            incnewRow["type"] = order["type"];
                            if (radioGroupType.SelectedIndex == 0)
                            {
                                incnewRow["wardID"] = null;
                                incnewRow["warehouseID"] = order["warehouseID"];
                                incnewRow["warehouseName"] = order["warehouseName"];
                                incnewRow["warehouseDesc"] = order["warehouseDesc"];
                            }
                            else if (radioGroupType.SelectedIndex == 1)
                            {
                                incnewRow["wardID"] = order["wardID"];
                                incnewRow["warehouseID"] = null;
                                incnewRow["warehouseName"] = order["warehouseName"];
                                incnewRow["warehouseDesc"] = order["warehouseDesc"];
                            }
                            incnewRow["tenderID"] = null;
                            incnewRow["supplierID"] = gridLookUpEditUser.EditValue;
                            incnewRow["supplierName"] = gridLookUpEditUser.Text;
                            incnewRow["conMedicineID"] = gridViewOrderDetail.GetDataRow(i)["medicineID"];

                            DataRow incmedicine = getGiveMedicine(gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine).ToString(), gridViewOrderDetail.GetRowCellValue(i, gridColumnPrice).ToString());
                            incnewRow["orderGoodsDetailID"] = gridViewOrderDetail.GetDataRow(i)["id"];
                            incnewRow["medicineID"] = incmedicine["medicineID"];
                            incnewRow["medicineName"] = incmedicine["name"];
                            incnewRow["latinName"] = incmedicine["latinName"];
                            incnewRow["unitType"] = incmedicine["unitType"];
                            incnewRow["shape"] = incmedicine["shape"];
                            incnewRow["quantity"] = incmedicine["quantity"];
                            incnewRow["validDate"] = incmedicine["validDate"];
                            incnewRow["medicineTypeName"] = incmedicine["medicineTypeName"];
                            incnewRow["incTypeID"] = 4;
                            incnewRow["incTypeName"] = "Шилжүүлэг";
                            incnewRow["price"] = gridViewOrderDetail.GetRowCellValue(i, gridColumnPrice);
                            incnewRow["count"] = gridViewOrderDetail.GetRowCellValue(i, gridColumnGiveCount);
                            incnewRow["serial"] = incmedicine["serial"];
                            incnewRow.EndEdit();
                        }
                    }
                    JournalDataSet.IncJournalTableAdapter.Update(JournalDataSet.Instance.IncJournal);
                    JournalDataSet.IncDetailTableAdapter.Update(JournalDataSet.Instance.IncDetail);

                    JournalDataSet.Instance.ExpJournal.idColumn.DefaultValue = id;
                    JournalDataSet.Instance.ExpDetail.expJournalIDColumn.DefaultValue = id;

                    DataRowView docView = (DataRowView)JournalDataSet.ExpJournalBindingSource.AddNew();

                    docView["date"] = dateEditExpDate.EditValue;
                    docView["userID"] = gridLookUpEditUser.EditValue;
                    docView["userName"] = user["fullName"];
                    docView["roleName"] = user["roleName"];
                    docView["incJournalID"] = incid;
                    docView["type"] = radioGroupType.SelectedIndex;
                    if (radioGroupType.SelectedIndex == 0)
                    {
                        docView["wardID"] = null;
                        docView["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                        docView["warehouseName"] = warehouse["name"];
                        docView["warehouseDesc"] = warehouse["description"];
                        docView["expType"] = 3;
                        //docView["expTypeID"] = 3;
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {
                        docView["wardID"] = treeListLookUpEditWard.EditValue;
                        docView["warehouseID"] = null;
                        docView["warehouseName"] = ward["name"];
                        docView["warehouseDesc"] = ward["description"];
                        docView["expType"] = 4;
                        //docView["expTypeID"] = 4;
                    }
                    //2016-10-18 Зарлагын журналь тасаг эсвэл агуулахаас хамаараад wtype дуур утга оноов.
                    docView["expUserID"] = order["userID"];
                    if (Convert.ToInt32(order["type"]) == 0)
                    {
                        docView["wtype"] = 0;
                        docView["expWarehouseID"] = order["warehouseID"];
                        docView["expWardID"] = null;
                        docView["expType"] = 3;
                    }
                    else if (Convert.ToInt32(order["type"]) == 1)
                    {
                        docView["wtype"] = 1;
                        docView["expWarehouseID"] = null;
                        docView["expWardID"] = order["wardID"];
                        docView["expType"] = 4;
                    }
                    docView["expUserName"] = order["userName"];
                    docView["expRoleName"] = order["roleName"];
                    docView["expWarehouseName"] = order["warehouseName"];
                    docView["expWarehouseDesc"] = order["warehouseDesc"];
                    docView["orderGoodsID"] = order["id"];
                    docView.EndEdit();

                    long newID = Convert.ToInt64(JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId()));
                    for (int i = 0; i < gridViewOrderDetail.RowCount; i++, newID++)
                    {
                        if (gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine) != null &&
                            gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine) != DBNull.Value &&
                            Convert.ToDouble(gridViewOrderDetail.GetRowCellValue(i, gridColumnGiveCount)) > 0)
                        {
                            JournalDataSet.Instance.ExpDetail.idColumn.DefaultValue = newID;
                            DataRowView newRow = (DataRowView)JournalDataSet.ExpDetailBindingSource.AddNew();

                            newRow["date"] = dateEditExpDate.EditValue;
                            newRow["userID"] = gridLookUpEditUser.EditValue;
                            newRow["userName"] = user["fullName"];
                            newRow["roleName"] = user["roleName"];
                            newRow["type"] = radioGroupType.SelectedIndex;
                            if (radioGroupType.SelectedIndex == 0)
                            {
                                newRow["wardID"] = null;
                                newRow["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                                newRow["warehouseName"] = warehouse["name"];
                                newRow["warehouseDesc"] = warehouse["description"];
                            }
                            else if (radioGroupType.SelectedIndex == 1)
                            {
                                newRow["warehouseID"] = null;
                                newRow["wardID"] = treeListLookUpEditWard.EditValue;
                                newRow["warehouseName"] = ward["name"];
                                newRow["warehouseDesc"] = ward["description"];
                            }
                            newRow["expUserID"] = order["userID"];
                            if (Convert.ToInt32(order["type"]) == 0)
                            {
                                newRow["expType"] = 3;
                                newRow["expWarehouseID"] = order["warehouseID"];
                                newRow["expWardID"] = null;
                            }
                            else if (Convert.ToInt32(order["type"]) == 1)
                            {
                                newRow["expType"] = 4;
                                newRow["expWarehouseID"] = null;
                                newRow["expWardID"] = order["wardID"];
                            }
                            newRow["expUserName"] = order["userName"];
                            newRow["expRoleName"] = order["roleName"];
                            newRow["expWarehouseName"] = order["warehouseName"];
                            newRow["expWarehouseDesc"] = order["warehouseDesc"];

                            DataRow medicine = getGiveMedicine(gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine).ToString(), gridViewOrderDetail.GetRowCellValue(i, gridColumnPrice).ToString());
                            if (medicine == null)
                            {
                                XtraMessageBox.Show("Олгох эм олдсонгүй !");
                                ProgramUtil.closeWaitDialog();
                                JournalDataSet.ExpDetailBindingSource.CancelEdit();
                                JournalDataSet.Instance.ExpDetail.RejectChanges();
                                JournalDataSet.Instance.ExpJournal.RejectChanges();
                                return;
                            }

                            newRow["orderGoodsDetailID"] = gridViewOrderDetail.GetDataRow(i)["id"];
                            newRow["medicineID"] = medicine["medicineID"];
                            newRow["medicineName"] = medicine["name"];
                            newRow["latinName"] = medicine["latinName"];
                            newRow["unitType"] = medicine["unitType"];
                            newRow["shape"] = medicine["shape"];
                            newRow["quantity"] = medicine["quantity"];
                            newRow["validDate"] = medicine["validDate"];
                            newRow["medicineTypeName"] = medicine["medicineTypeName"];                         
                            newRow["incTypeID"] = 1;
                            newRow["incTypeName"] = medicine["incTypeName"];
                            newRow["serial"] = medicine["serial"];
                            newRow["price"] = gridViewOrderDetail.GetRowCellValue(i, gridColumnPrice);
                            newRow["count"] = gridViewOrderDetail.GetRowCellValue(i, gridColumnGiveCount);
                            newRow["description"] = gridViewOrderDetail.GetRowCellValue(i, gridColumnDesc);
                            newRow["detialDesc"] = gridViewOrderDetail.GetRowCellValue(i, gridColumnDesc);
                            newRow.EndEdit();
                        }
                    }
                    JournalDataSet.ExpJournalTableAdapter.Update(JournalDataSet.Instance.ExpJournal);
                    JournalDataSet.ExpDetailTableAdapter.Update(JournalDataSet.Instance.ExpDetail);
                }
                else if (type == 2)
                {
                    JournalDataSet.Instance.IncJournal.idColumn.DefaultValue = id;
                    JournalDataSet.Instance.IncDetail.incJournalIDColumn.DefaultValue = id;

                    DataRowView docView = (DataRowView)JournalDataSet.IncJournalBindingSource.AddNew();

                    docView["date"] = dateEditExpDate.EditValue;
                    docView["userID"] = gridLookUpEditUser.EditValue;
                    docView["userName"] = user["fullName"];
                    docView["roleName"] = user["roleName"];
                    docView["type"] = radioGroupType.SelectedIndex;
                    docView["incTypeID"] = 2;
                    docView["incTypeName"] = "Тендер";

                    if (radioGroupType.SelectedIndex == 0)
                    {
                        docView["wardID"] = null;
                        docView["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                        docView["warehouseName"] = warehouse["name"];
                        docView["warehouseDesc"] = warehouse["description"];
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {
                        docView["wardID"] = treeListLookUpEditWard.EditValue;
                        docView["warehouseID"] = null;
                        docView["warehouseName"] = ward["name"];
                        docView["warehouseDesc"] = ward["description"];
                    }
                    docView["orderGoodsID"] = order["id"];
                    docView["tenderID"] = order["tenderID"];
                    docView["supplierID"] = order["supplierID"];
                    docView["supplierName"] = order["supplierName"];
                    docView.EndEdit();

                    long newID = Convert.ToInt64(JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId()));
                    for (int i = 0; i < gridViewOrderDetail.RowCount; i++, newID++)
                    {
                        if (gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine) != null && gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine) != DBNull.Value && Convert.ToDouble(gridViewOrderDetail.GetRowCellValue(i, gridColumnGiveCount)) > 0)
                        {
                            JournalDataSet.Instance.IncDetail.idColumn.DefaultValue = newID;
                            DataRowView newRow = (DataRowView)JournalDataSet.IncDetailBindingSource.AddNew();

                            newRow["date"] = dateEditExpDate.EditValue;
                            newRow["userID"] = gridLookUpEditUser.EditValue;
                            newRow["userName"] = user["fullName"];
                            newRow["roleName"] = user["roleName"];
                            newRow["type"] = radioGroupType.SelectedIndex;
                            if (radioGroupType.SelectedIndex == 0)
                            {
                                newRow["wardID"] = null;
                                newRow["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                                newRow["warehouseName"] = warehouse["name"];
                                newRow["warehouseDesc"] = warehouse["description"];
                            }
                            else if (radioGroupType.SelectedIndex == 1)
                            {
                                newRow["warehouseID"] = null;
                                newRow["wardID"] = treeListLookUpEditWard.EditValue;
                                newRow["warehouseName"] = ward["name"];
                                newRow["warehouseDesc"] = ward["description"];
                            }
                            newRow["tenderID"] = order["tenderID"];
                            newRow["supplierID"] = order["supplierID"];
                            newRow["supplierName"] = order["supplierName"];
                            newRow["conMedicineID"] = gridViewOrderDetail.GetDataRow(i)["medicineID"];

                            DataRow medicine = getGiveMedicine(gridViewOrderDetail.GetRowCellValue(i, gridColumnMedicine).ToString(), gridViewOrderDetail.GetRowCellValue(i, gridColumnPrice).ToString());
                            newRow["orderGoodsDetailID"] = gridViewOrderDetail.GetDataRow(i)["id"];
                            newRow["medicineID"] = medicine["id"];
                            newRow["medicineName"] = medicine["name"];
                            newRow["latinName"] = medicine["latinName"];
                            newRow["unitType"] = medicine["unitType"];
                            newRow["shape"] = medicine["shape"];
                            newRow["quantity"] = medicine["quantity"];
                            newRow["validDate"] = medicine["validDate"];
                            newRow["medicineTypeName"] = medicine["medicineTypeName"];
                            newRow["incTypeID"] = 2;
                            newRow["incTypeName"] = "Тендер";
                            newRow["price"] = gridViewOrderDetail.GetRowCellValue(i, gridColumnPrice);
                            newRow["count"] = gridViewOrderDetail.GetRowCellValue(i, gridColumnGiveCount);
                            newRow["serial"] = medicine["serial"];
                            newRow.EndEdit();
                        }
                    }
                    JournalDataSet.IncJournalTableAdapter.Update(JournalDataSet.Instance.IncJournal);
                    JournalDataSet.IncDetailTableAdapter.Update(JournalDataSet.Instance.IncDetail);
                }

                ProgramUtil.closeWaitDialog();
                Close();
            }
        }

        private DataRow getGiveMedicine(string medicinePrice, string price)
        {
            if (type == 1)
            {
                for (int i = 0; i < JournalDataSet.Instance.MedicineBal.Rows.Count; i++)
                {
                    DataRow row = JournalDataSet.Instance.MedicineBal.Rows[i];
                    if (row["medicinePrice"].ToString().Equals(medicinePrice))
                    {
                        //if(row["price"].ToString().Equals(price))
                        return row;
                    }

                }
            }
            else if (type == 2)
            {
                for (int i = 0; i < JournalDataSet.Instance.MedicineOther.Rows.Count; i++)
                {
                    DataRow row = JournalDataSet.Instance.MedicineOther.Rows[i];
                    if (row["id"].ToString().Equals(medicinePrice))
                        //if (row["price"].ToString().Equals(price))
                        return row;
                }
            }

            return null;
        }

        private void addNotList()
        {
            DataRow NotInListRow = JournalDataSet.Instance.OrderGoodsOtherDetail1.NewRow();
            DataRowView Exist = (DataRowView)gridViewOrderDetail.GetFocusedRow();
            if (Exist == null) return;
            NotInListRow["orderGoodsID"] = Exist["orderGoodsID"];
            NotInListRow["id"] = Exist["id"];
            NotInListRow["medicineID"] = Exist["medicineID"];
            NotInListRow["medicineName"] = Exist["medicineName"];
            NotInListRow["latinName"] = Exist["latinName"];
            NotInListRow["unitType"] = Exist["unitType"];
            NotInListRow["shape"] = Exist["shape"];
            NotInListRow["quantity"] = Exist["quantity"];
            NotInListRow["count"] = 0;
            NotInListRow["confirmCount"] = 0;
            NotInListRow["giveMedicineID"] = 0;
            NotInListRow["giveCount"] = 0;
            NotInListRow.EndEdit();
            JournalDataSet.Instance.OrderGoodsOtherDetail1.Rows.Add(NotInListRow);
        }

        private void deleteSelectedRow()
        {
            if (gridViewOrderDetail.GetFocusedRow() != null)
            {
                gridViewOrderDetail.DeleteSelectedRows();
            }
        }

        #endregion

        private void repositoryItemGridLookUpEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "add"))
            {
                if (type == 2) MedicineInUpForm.Instance.showForm(3, Instance);
            }
        }

        public void addMedicine(DataRowView view)
        {
            if (type == 2)
            {
                reload(true);
                DataRow[] row = JournalDataSet.Instance.MedicineOther.Select("id = '" + view["id"] + "'");
                if (Convert.ToString(row[0]["packageName"]) != gridViewOrderDetail.GetFocusedRowCellValue(gridColumn4).ToString())
                {
                    gridViewOrderDetail.SetFocusedRowCellValue(gridColumnMedicine, null);
                    gridViewOrderDetail.GetFocusedDataRow()["giveMedicineID"] = null;
                    gridViewOrderDetail.GetFocusedDataRow()["confirmMedicineID"] = null;
                    gridViewOrderDetail.GetFocusedDataRow()["incTypeID"] = 0;
                    gridViewOrderDetail.GetFocusedDataRow()["giveCount"] = 0;
                    XtraMessageBox.Show("Тухайн багцын эм биш байна.");
                }
                else
                {
                    gridViewOrderDetail.SetFocusedRowCellValue(gridColumnMedicine, view["id"]);
                    gridViewOrderDetail.GetFocusedDataRow()["giveMedicineID"] = row[0]["id"];
                    gridViewOrderDetail.GetFocusedDataRow()["confirmMedicineID"] = row[0]["id"];
                    gridViewOrderDetail.GetFocusedDataRow()["incTypeID"] = 4;
                }
            }
        }

        private void gridControlOrder_Click(object sender, EventArgs e)
        {

        }
    }
}