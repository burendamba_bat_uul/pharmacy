﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.report.journal.exp;


namespace Pharmacy2016.gui.core.journal.exp
{
    public partial class MedicineChooseDoctor : DevExpress.XtraEditors.XtraForm
    {
        private static MedicineChooseDoctor INSTANCE = new MedicineChooseDoctor();

        public static MedicineChooseDoctor Instance
        {
            get
            {
                return INSTANCE;
            }
        }
        public MedicineChooseDoctor()
        {
        }
        private bool isInit = false;
        string start = "";
        string end = "";

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            initDataSource();
            initBinding();
            reload(false);
        }

        private void initDataSource()
        {
        //    DataSet ds = DatabaseConnection.SelectQuery("");
        //    gridLookUpEditDctr.Properties.DataSource = ds.Tables[0];
            gridLookUpEditDctr.Properties.DataSource = InformationDataSet.SystemUserOtherBindingSource;
        }

        private void initBinding()
        {
            //gridLookUpEditDctr.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "doctorID", true));
        }
        private void reload(bool isReload)
        {
            //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
            //InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
            if (isReload || InformationDataSet.Instance.SystemUserOther.Rows.Count == 0)
                InformationDataSet.SystemUserOtherTableAdapter.Fill(InformationDataSet.Instance.SystemUserOther, HospitalUtil.getHospitalId(), SystemUtil.DOCTOR);
            //ProgramUtil.closeWaitDialog();
        }

        public void showForm(int type, XtraForm form, string dateEditStart, string dateEditEnd)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }
                start = dateEditStart;
                end = dateEditEnd;


                ShowDialog(form);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
            }
        }

        private void gridLookUpEditDctr_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditDctr.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                }
            }
        }


        private void ChooseDctr_Click(object sender, EventArgs e)
        {
            ExpDoctorReport.Instance.initAndShow(gridLookUpEditDctr.EditValue.ToString(), start, end);
            gridLookUpEditDctr.EditValue = null;
            Close();
        }
        
    }
}