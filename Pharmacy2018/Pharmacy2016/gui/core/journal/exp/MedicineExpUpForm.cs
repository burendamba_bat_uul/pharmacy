﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.bal;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList;
using System.Data.SqlClient;



namespace Pharmacy2016.gui.core.journal.exp
{
    /*
     * Эмийн зарлага нэмэх, засах цонх.
     * 
     */

    public partial class MedicineExpUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц


        private static MedicineExpUpForm INSTANCE = new MedicineExpUpForm();

        public static MedicineExpUpForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }
        public class Multi_data
        {
            public string code { get; set; }
            public double quant { get; set; }
            public double price { get; set; }
        }

        public class Multi_draw
        {
            public string name { get; set; }
            public string validDate { get; set; }
        }

        private bool isInit = false;

        private int actionType;

        private Double confirmCount;

        private DataRowView docView;

        private DataRowView currView;

        private List<TreeListNode> nodes;

        private int[] rowID;

        private Dictionary<string, Multi_draw> dict_draw = new Dictionary<string, Multi_draw>();

        private Dictionary<string, Multi_data> dict = new Dictionary<string, Multi_data>();

        private MedicineExpUpForm()
        {

        }


        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            spinEditCount.Properties.MaxValue = Decimal.MaxValue - 1;

            dateEditExpDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditExpDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

            initNode();
            initError();
            initDataSource();
            initBinding();
            reload(false);
        }

        private void initDataSource()
        {
            //gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
            gridLookUpEditUser.Properties.DataSource = JournalDataSet.SystemUserExpBindingSource;
            treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
            treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
            //gridLookUpEditExpUser.Properties.DataSource = InformationDataSet.SystemUserOtherBindingSource;
            gridLookUpEditExpUser.Properties.DataSource = JournalDataSet.Instance.SystemUserExpOther;
            treeListLookUpEditExpWard.Properties.DataSource = JournalDataSet.WardOther1BindingSource;
            treeListLookUpEditExpWarehouse.Properties.DataSource = JournalDataSet.WarehouseOther1BindingSource;
            gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineBalBindingSource;
            gridLookUpEditExpType.Properties.DataSource = InformationDataSet.ExpTypeOtherBindingSource;
            gridLookUpEditDoctor.Properties.DataSource = InformationDataSet.SystemUserOtherBindingSource;
            gridControlMed.DataSource = JournalDataSet.MedicineBalBindingSource;
            popupContainerControlMed.Controls.Add(gridControlMed);
            popupContainerEditMedicineBal.Properties.PopupControl = popupContainerControlMed;
        }

        private void initError()
        {
            gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditExpUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditExpType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditExpWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditExpWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            dateEditExpDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditExpType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            popupContainerEditMedicineBal.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
        }

        private void initNode()
        {
            nodes = new List<TreeListNode>();
            TreeList tl = treeListLookUpEditExpType.Properties.TreeList;
            tl.BeginUnboundLoad();
            tl.Nodes.Clear();
            TreeListNode rootNode1 = tl.AppendNode(new object[] { 10, "Зарлага", null }, -1);
            TreeListNode childNode1 = tl.AppendNode(new object[] { 1, "Эмийн сан", "Зарлага-эмийн сан" }, rootNode1);
            TreeListNode childNode2 = tl.AppendNode(new object[] { 2, "Тасаг", "Зарлага-тасаг" }, rootNode1);
            TreeListNode rootNode2 = tl.AppendNode(new object[] { 20, "Шилжүүлэг", null }, -1);
            if (radioGroupType.SelectedIndex == 0)
            {
                TreeListNode childNode3 = tl.AppendNode(new object[] { 3, "Эмийн сан", "Шилжүүлэг-эмийн сан" }, rootNode2);
                nodes.Add(childNode3);
            }
            else
            {
                TreeListNode childNode4 = tl.AppendNode(new object[] { 4, "Тасаг", "Шилжүүлэг-тасаг" }, rootNode2);
                nodes.Add(childNode4);
            }
            tl.EndUnboundLoad();
            tl.ExpandAll();
            nodes.Add(childNode1);
            nodes.Add(childNode2);
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm(int type, XtraForm form)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }
                confirmCount = -1;
                actionType = type;
                if (actionType == 1)
                {
                    insert();
                }
                else if (actionType == 2)
                {
                    update();
                }

                changeType();
                changeExpType();

                ShowDialog(form);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clearData();
                MedicineExpForm.Instance.refreshData();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void initBinding()
        {
            gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "userID", true));
            radioGroupType.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "type", true));
            treeListLookUpEditWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "warehouseID", true));
            treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "wardID", true));
            treeListLookUpEditExpType.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "expType", true));
            gridLookUpEditExpUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "expUserID", true));
            gridLookUpEditExpType.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "expTypeID", true));
            treeListLookUpEditExpWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "expWarehouseID", true));
            treeListLookUpEditExpWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "expWardID", true));
            textEditID.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "id", true));
            textEditDocNumber.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "docNumber", true));
            dateEditExpDate.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "date", true));
            memoEditDescription.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "description", true));
            gridLookUpEditDoctor.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "doctorID", true));
        }

        private void insert()
        {
            this.Text = "Зарлага нэмэх цонх";
            string id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            JournalDataSet.Instance.ExpJournal.idColumn.DefaultValue = id;
            JournalDataSet.Instance.ExpDetail.expJournalIDColumn.DefaultValue = id;

            docView = (DataRowView)JournalDataSet.ExpJournalBindingSource.AddNew();
            DateTime now = DateTime.Now;
            docView["id"] = id;
            docView["date"] = now;
            checkEditMultiSelection.Visible = true;
            dateEditExpDate.DateTime = now;
            textEditID.Text = id;

            JournalDataSet.MedicineBalBindingSource.Filter = "count>0";
            simpleButtonNewDoc.Enabled = true;
            gridLookUpEditUser.Enabled = true;
            radioGroupType.Enabled = true;
            treeListLookUpEditWarehouse.Enabled = true;
            treeListLookUpEditWard.Enabled = true;
            treeListLookUpEditExpType.Enabled = true;
            gridLookUpEditExpType.Enabled = true;
            gridLookUpEditUser.Enabled = true;
            treeListLookUpEditExpWarehouse.Enabled = true;
            treeListLookUpEditExpWard.Enabled = true;
            textEditDocNumber.Enabled = true;
            gridLookUpEditDoctor.Enabled = true;
            textEditID.Enabled = true;
            dateEditExpDate.Enabled = true;
            gridLookUpEditExpUser.Enabled = true;
            confirmCount = -1;

            addRow();
        }

        private void update()
        {
            this.Text = "Зарлага засах цонх";
            docView = (DataRowView)JournalDataSet.ExpJournalBindingSource.Current;
            radioGroupType.EditValue = docView["type"];
            currView = MedicineExpForm.Instance.getSelectedRow();
            JournalDataSet.Instance.ExpDetail.expJournalIDColumn.DefaultValue = docView["id"];
            checkEditMultiSelection.Visible = false;
            gridLookUpEditMedicine.EditValue = currView["medicinePrice"];
            spinEditPrice.EditValue = currView["price"];
            spinEditCount.EditValue = currView["count"];
            labelControlQuantity.Text = currView["quantity"].ToString();
            if ((Convert.ToInt16(docView["expType"]) == 3) || (Convert.ToInt16(docView["expType"]) == 4))
            {
                if (UserDataSet.Instance.Medicine.Rows.Count == 0)
                    UserDataSet.MedicineTableAdapter.Fill(UserDataSet.Instance.Medicine, HospitalUtil.getHospitalId());
                if (JournalDataSet.Instance.OrderGoodsDetail.Rows.Count == 0)
                    JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(),
                        Convert.ToString(docView["userID"]), MedicineExpForm.Instance.getStartDate().Date.ToString(),
                        MedicineExpForm.Instance.getEndDate().Date.ToString());
                DataRow[] rows_medicine = UserDataSet.Instance.Medicine.Select("id = '" + currView["medicineID"] + "'");
                if (rows_medicine.Length > 0)
                {
                    if (JournalDataSet.Instance.MedicineBal.Rows.Count == 0)
                        JournalDataSet.MedicineBalTableAdapter.Fill(JournalDataSet.Instance.MedicineBal, HospitalUtil.getHospitalId(),
                            Convert.ToString(docView["userID"]), Convert.ToInt16(docView["type"]), Convert.ToString(docView["warehouseID"]),
                            Convert.ToString(docView["wardID"]), dateEditExpDate.Text);
                    JournalDataSet.MedicineBalBindingSource.Filter = "count>0 AND packageID='" + rows_medicine[0]["packageID"] + "'";
                }
                simpleButtonNewDoc.Enabled = false;
                gridLookUpEditUser.Enabled = false;
                radioGroupType.Enabled = false;
                treeListLookUpEditWarehouse.Enabled = false;
                treeListLookUpEditWard.Enabled = false;
                treeListLookUpEditExpType.Enabled = false;
                gridLookUpEditExpType.Enabled = false;
                gridLookUpEditUser.Enabled = false;
                treeListLookUpEditExpWarehouse.Enabled = false;
                treeListLookUpEditExpWard.Enabled = false;
                textEditDocNumber.Enabled = false;
                gridLookUpEditDoctor.Enabled = false;
                textEditID.Enabled = false;
                dateEditExpDate.Enabled = false;
                gridLookUpEditExpUser.Enabled = false;
                DataRow[] rows = JournalDataSet.Instance.OrderGoodsDetail.Select("orderGoodsID = '" + docView["orderGoodsID"] + "'");
                if (rows.Length > 0)
                {
                    for (int i = 0; i < rows.Length; i++)
                    {
                        if (Convert.ToString(currView["id"]) == Convert.ToString(rows[i]["expDetailID"]))
                        {
                            confirmCount = Convert.ToDouble(rows[i]["confirmCount"]);
                            //MessageBox.Show(Convert.ToString(confirmCount));
                        }
                    }
                }
            }
            else
            {
                JournalDataSet.MedicineBalBindingSource.Filter = "count>0";
                simpleButtonNewDoc.Enabled = false;
                gridLookUpEditUser.Enabled = false;
                radioGroupType.Enabled = false;
                treeListLookUpEditWarehouse.Enabled = false;
                treeListLookUpEditWard.Enabled = false;
                treeListLookUpEditExpType.Enabled = false;
                gridLookUpEditExpType.Enabled = false;
                gridLookUpEditUser.Enabled = false;
                treeListLookUpEditExpWarehouse.Enabled = false;
                treeListLookUpEditExpWard.Enabled = false;
                textEditDocNumber.Enabled = false;
                gridLookUpEditDoctor.Enabled = false;
                textEditID.Enabled = false;
                dateEditExpDate.Enabled = false;
                gridLookUpEditExpUser.Enabled = false;
                confirmCount = -1;
            }

        }

        private void addRow()
        {
            JournalDataSet.Instance.ExpDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            currView = (DataRowView)JournalDataSet.ExpDetailBindingSource.AddNew();
            clearMedicine();
        }


        private void clearData()
        {
            clearErrorText();
            //clearBinding();
            clearMedicine();
            checkEditMultiSelection.Checked = false;
            JournalDataSet.WarehouseOtherBindingSource.Filter = "";
            JournalDataSet.WardOtherBindingSource.Filter = "";
            JournalDataSet.SystemUserExpBindingSource.Filter = "";
            JournalDataSet.WarehouseOther1BindingSource.Filter = "";
            JournalDataSet.WardOther1BindingSource.Filter = "";

            JournalDataSet.ExpJournalBindingSource.CancelEdit();
            JournalDataSet.ExpDetailBindingSource.CancelEdit();
            JournalDataSet.Instance.ExpJournal.RejectChanges();
            JournalDataSet.Instance.ExpDetail.RejectChanges();
        }

        private void clearBinding()
        {
            treeListLookUpEditWarehouse.DataBindings.Clear();
            gridLookUpEditExpUser.DataBindings.Clear();
            gridLookUpEditExpType.DataBindings.Clear();
            textEditID.DataBindings.Clear();
            textEditDocNumber.DataBindings.Clear();
            dateEditExpDate.DataBindings.Clear();
            memoEditDescription.DataBindings.Clear();
            treeListLookUpEditExpType.DataBindings.Clear();
            treeListLookUpEditWard.DataBindings.Clear();
        }

        private void clearErrorText()
        {
            gridLookUpEditUser.ErrorText = "";
            treeListLookUpEditWarehouse.ErrorText = "";
            treeListLookUpEditWard.ErrorText = "";
            treeListLookUpEditExpType.ErrorText = "";
            gridLookUpEditExpUser.ErrorText = "";
            gridLookUpEditExpType.ErrorText = "";
            treeListLookUpEditExpWarehouse.ErrorText = "";
            treeListLookUpEditExpWard.ErrorText = "";
            gridLookUpEditMedicine.ErrorText = "";
            dateEditExpDate.ErrorText = "";
            spinEditCount.ErrorText = "";
            popupContainerEditMedicineBal.ErrorText = "";
        }

        private void clearMedicine()
        {
            gridLookUpEditMedicine.EditValue = null;
            spinEditCount.ForeColor = Color.Black;
            spinEditBal.Value = 0;
            spinEditCount.Value = 0;
            spinEditPrice.Value = 0;
            spinEditSumPrice.Value = 0;
            gridViewMed.ClearSelection();
        }

        private void MedicineExpInUpForm_Shown(object sender, EventArgs e)
        {
            spinEditBal.BackColor = Color.Transparent;
            if (actionType == 1)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                {
                    gridLookUpEditUser.EditValue = UserUtil.getUserId();
                    docView["userID"] = UserUtil.getUserId();
                    treeListLookUpEditWarehouse.Focus();
                }
                else
                {
                    gridLookUpEditUser.Focus();
                }
            }
            else if (actionType == 2)
            {
                if (Visible)
                {
                    changeType();
                    reloadMedicine();
                }


                DataRowView bal = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                DataRowView exp = MedicineExpForm.Instance.getSelectedRow();
                if (bal != null && exp != null)
                    spinEditBal.EditValue = Convert.ToDouble(bal["count"]);// -Convert.ToDouble(exp["count"]);

                /*if (spinEditBal.Value < 0)
                    spinEditBal.BackColor = Color.Red;
                else
                    spinEditBal.BackColor = Color.Transparent;
                */
                gridLookUpEditMedicine.Focus();
            }

            if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.ACCOUNTANT_ID)
                JournalDataSet.SystemUserExpBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";

            ProgramUtil.IsShowPopup = true;
            ProgramUtil.IsSaveEvent = false;
        }

        #endregion

        #region Формын event

        private void dateEditExpDate_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    dateEditExpDate.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditUser.Focus();
                }
            }
        }

        private void dateEditExpDate_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible)
                reloadMedicine();
        }

        private void gridLookUpEditUser_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditUser.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    if (radioGroupType.SelectedIndex == 0)
                        treeListLookUpEditWarehouse.Focus();
                    else
                        treeListLookUpEditWard.Focus();
                }
            }
        }

        private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible)
                reloadMedicine();

            if (Visible && gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null)
            {
                JournalDataSet.WarehouseOtherBindingSource.Filter = UserUtil.getUserWarehouse(gridLookUpEditUser.EditValue.ToString());
                JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(gridLookUpEditUser.EditValue.ToString());
            }
        }


        private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Visible)
                changeType();
            initNode();
        }

        private void treeListLookUpEditWarehouse_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible)
                reloadMedicine();
        }

        private void treeListLookUpEditWarehouse_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditWarehouse.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    treeListLookUpEditExpType.Focus();
                }
            }
        }

        private void treeListLookUpEditWard_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditWard.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    treeListLookUpEditExpType.Focus();
                }
            }
        }

        private void gridLookUpEditDoctor_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditDoctor.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    memoEditDescription.Focus();
                }
            }
        }

        private void treeListLookUpEditExpType_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditExpType.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditExpType.Focus();
                }
            }
        }

        private void treeListLookUpEditExpType_QueryCloseUp(object sender, CancelEventArgs e)
        {
            TreeList tl = treeListLookUpEditExpType.Properties.TreeList;
            TreeListNode focusedNode = tl.FocusedNode;
            if (treeListLookUpEditExpType != null && focusedNode != null)
            {
                int id = Convert.ToInt32(focusedNode.GetValue("id"));
                e.Cancel = (id == 10 || id == 20);
            }
        }

        private void treeListLookUpEditExpType_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            if (e.Value != null && e.Value != DBNull.Value && !e.Value.ToString().Equals(""))
                e.DisplayText = getNodeText(Convert.ToInt32(e.Value));
            else
                e.DisplayText = "";
        }

        private void treeListLookUpEditExpType_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible)
                changeExpType();
        }

        private void gridLookUpEditExpType_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditExpType.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditExpUser.Focus();
                }
            }
        }


        private void gridLookUpEditExpUser_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditExpUser.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    if (treeListLookUpEditExpType.EditValue == null || treeListLookUpEditExpType.EditValue == DBNull.Value || treeListLookUpEditExpType.Text.Equals(""))
                    {
                        treeListLookUpEditExpWarehouse.Focus();
                    }
                    else
                    {
                        int expType = Convert.ToInt32(treeListLookUpEditExpType.EditValue);
                        if (expType == 1 || expType == 3)
                        {
                            treeListLookUpEditExpWarehouse.Focus();
                        }
                        else
                        {
                            treeListLookUpEditExpWard.Focus();
                        }
                    }
                }
            }
        }

        private void gridLookUpEditExpUser_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible && gridLookUpEditExpUser.EditValue != DBNull.Value && gridLookUpEditExpUser.EditValue != null)
            {
                JournalDataSet.WarehouseOther1BindingSource.Filter = UserUtil.getUserWarehouse(gridLookUpEditExpUser.EditValue.ToString());
                JournalDataSet.WardOther1BindingSource.Filter = UserUtil.getUserWard(gridLookUpEditExpUser.EditValue.ToString());
            }
        }

        private void gridLookUpEditExpUser_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "delete"))
            {
                gridLookUpEditExpUser.EditValue = null;
            }
        }


        private void treeListLookUpEditExpWarehouse_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditExpWarehouse.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    textEditDocNumber.Focus();
                }
            }
        }

        private void treeListLookUpEditExpWarehouse_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "delete"))
            {
                treeListLookUpEditExpWarehouse.EditValue = null;
            }
        }

        private void treeListLookUpEditExpWard_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditExpWard.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    textEditDocNumber.Focus();
                }
            }
        }

        private void treeListLookUpEditExpWard_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "delete"))
            {
                treeListLookUpEditExpWard.EditValue = null;
            }
        }


        private void gridLookUpEditMedicine_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    if (!ProgramUtil.IsSaveEvent)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditMedicine.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsSaveEvent = false;
                    }
                }
                else
                {
                    spinEditCount.Focus();
                }
            }
        }

        private void gridLookUpEditMedicine_EditValueChanged(object sender, EventArgs e)
        {
            DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
            rowID = gridViewMed.GetSelectedRows();
            spinEditCount.Value = 0;
            if (Visible && view != null)
            {
                labelControlQuantity.Text = view["quantity"].ToString();
                spinEditPrice.Value = Convert.ToDecimal(view["price"]);
                spinEditBal.Value = Convert.ToDecimal(view["count"]);
                if (confirmCount > 0)
                {
                    if (confirmCount > Convert.ToDouble(view["count"]))
                    {
                        spinEditCount.Value = Convert.ToDecimal(view["count"]);
                    }
                    else spinEditCount.Value = Convert.ToDecimal(confirmCount);
                }

                //MessageBox.Show(Convert.ToString(confirmCount));
                currView["medicineID"] = view["medicineID"];
                currView["medicineName"] = view["name"];
                currView["latinName"] = view["latinName"];
                currView["unitType"] = view["unitType"];
                currView["shape"] = view["shape"];
                currView["quantity"] = view["quantity"];
                currView["validDate"] = view["validDate"];
                currView["serial"] = view["serial"];
                currView["medicineTypeName"] = view["medicineTypeName"];
                currView["incTypeID"] = view["incTypeID"];
                currView["incTypeName"] = view["incTypeName"];
                currView.EndEdit();
            }

        }

        private void gridLookUpEditMedicine_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "add"))
            {
                MedicineBalExpForm.Instance.showForm(1, Instance);
            }
            else
            {
                if (Convert.ToInt32(gridView3.DataRowCount) > 0)
                {
                    int k = 0;
                    dict_draw.Clear();
                    for (int i = 0; i < gridView3.DataRowCount; i++)
                    {
                        DataRow[] rows = JournalDataSet.Instance.MedicineBal.Select("name = '" + gridView3.GetRowCellValue(i, gridColumn15) + "'", "validDate ASC");
                        if (rows.Length > 1)
                        {
                            if (Convert.ToString(gridView3.GetRowCellValue(i, gridColumnValidDate)) == Convert.ToString(rows[0]["validDate"]))
                            {
                                Multi_draw draw = new Multi_draw();
                                draw.name = Convert.ToString(gridView3.GetRowCellValue(i, gridColumn15));
                                draw.validDate = Convert.ToString(gridView3.GetRowCellValue(i, gridColumnValidDate));
                                dict_draw.Add(Convert.ToString(k), draw);
                                k++;
                            }
                        }
                    }
                }
            }
        }


        private void spinEditCount_ValueChanged(object sender, EventArgs e)
        {
            calcPrice();
            if (Visible)
            {
                if (confirmCount > 0)
                {
                    if (Convert.ToDouble(spinEditCount.Value) > confirmCount)
                    {
                        if (Convert.ToDouble(spinEditBal.Value) < confirmCount)
                        {
                            XtraMessageBox.Show("Үлдэгдэл хүрэхгүй байна!");
                            spinEditCount.Value = spinEditBal.Value;
                        }
                        else
                        {
                            XtraMessageBox.Show("Баталсан тооноос их байна!");
                            spinEditCount.Value = Convert.ToDecimal(confirmCount);
                        }
                    }
                    else
                    {
                        if (spinEditBal.Value < spinEditCount.Value)
                        {
                            XtraMessageBox.Show("Үлдэгдэл хүрэхгүй байна!");
                            spinEditCount.Value = spinEditBal.Value;
                        }
                    }

                }
                else
                {
                    if (spinEditBal.Value < spinEditCount.Value)
                    {
                        XtraMessageBox.Show("Үлдэгдэл хүрэхгүй байна!");
                        //spinEditCount.ForeColor = Color.Red;
                        spinEditCount.Value = spinEditBal.Value;
                    }
                    else
                    {
                        spinEditCount.ForeColor = Color.Black;
                    }
                }
            }
        }


        private void simpleButtonNewDoc_Click(object sender, EventArgs e)
        {
            clearData();
            insert();
            dateEditExpDate.Focus();
        }

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            reload(true);
        }

        private void simpleButtonSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void gridView3_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

            if (!e.DisplayText.Equals(""))
            {
                Multi_draw chk_draw = new Multi_draw();
                bool chk_existed = true;
                int x = 0;
                while (chk_existed)
                {
                    if (dict_draw.Count > x)
                    {
                        chk_draw = dict_draw[Convert.ToString(x)];
                        if (chk_draw.name == Convert.ToString(gridView3.GetRowCellValue(e.RowHandle, gridColumn15)))
                        {
                            if (chk_draw.validDate == Convert.ToString(gridView3.GetRowCellValue(e.RowHandle, gridColumnValidDate)))
                            {
                                e.Appearance.BackColor = Color.DarkSalmon;
                                chk_existed = false;
                            }
                        }
                        x++;
                    }
                    else chk_existed = false;
                }


            }

            if (!e.DisplayText.Equals(""))
            {
                if (Convert.ToDateTime(gridView3.GetRowCellValue(e.RowHandle, gridColumnValidDate)) < DateTime.Now)
                {
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.ForeColor = Color.White;
                }
                else if ((Convert.ToDateTime(gridView3.GetRowCellValue(e.RowHandle, gridColumnValidDate)) - DateTime.Now).TotalDays <= 7)
                {
                    e.Appearance.BackColor = Color.Gold;
                }
            }
        }

        private void checkEditMultiSelection_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEditMultiSelection.Checked)
            {
                spinEditCount.ReadOnly = true;
                spinEditBal.ReadOnly = true;
                spinEditPrice.ReadOnly = true;
                spinEditSumPrice.ReadOnly = true;
                popupContainerEditMedicineBal.Visible = true;
                popupContainerEditMedicineBal.Location = new Point(167, 372);
                gridLookUpEditMedicine.Visible = false;
                gridViewMed.FindFilterText = "";
            }
            else
            {
                spinEditCount.ReadOnly = false;
                spinEditBal.ReadOnly = false;
                spinEditPrice.ReadOnly = false;
                spinEditSumPrice.ReadOnly = false;
                gridLookUpEditMedicine.Visible = true;
                popupContainerEditMedicineBal.Visible = false;
                popupContainerEditMedicineBal.Location = new Point(287, 398);
            }
        }

        private void popupContainerEditMedicineBal_CloseUp(object sender, DevExpress.XtraEditors.Controls.CloseUpEventArgs e)
        {
            rowID = gridViewMed.GetSelectedRows();
            dict.Clear();
            for (int i = 0; i < rowID.Length; i++)
            {
                DataRow medicine = gridViewMed.GetDataRow(rowID[i]);
                Multi_data temp = new Multi_data();
                temp.code = medicine["medicineID"].ToString();

                temp.quant = Convert.ToDouble(medicine["dupCount"]);
                temp.price = Convert.ToDouble(medicine["price"]);
                dict.Add(Convert.ToString(i), temp);
            }
        }

        private void gridViewMed_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (e.Action == CollectionChangeAction.Remove)
            {
                gridViewMed.SetRowCellValue(gridViewMed.FocusedRowHandle, gridColumnDupCount, 0);
            }
            if (gridViewMed.SelectedRowsCount == gridViewMed.RowCount || gridViewMed.SelectedRowsCount == 0)
            {

                for (int a = 0; a < gridViewMed.RowCount; a++)
                {
                    gridViewMed.SetRowCellValue(a, gridColumnDupCount, 0);
                }

                for (int j = 0; j < gridViewMed.GetSelectedRows().Length; j++)
                {
                    DataRow current = gridViewMed.GetDataRow(gridViewMed.GetSelectedRows()[j]);
                    current["dupCount"] = current["count"];
                }
            }
        }

        #endregion

        #region Формын функц

        private void addMultipleRow()
        {
            for (int i = 0; i < rowID.Length; i++)
            {
                DataRow medicine = gridViewMed.GetDataRow(rowID[i]);
                DataRow newRow = JournalDataSet.Instance.ExpDetail.NewRow();
                newRow["id"] = getNotExistId();
                newRow["expJournalID"] = docView["id"];
                newRow["date"] = dateEditExpDate.EditValue;
                newRow["userID"] = gridLookUpEditUser.EditValue;
                newRow["type"] = radioGroupType.SelectedIndex;
                newRow["expType"] = treeListLookUpEditExpType.EditValue;
                newRow["expUserID"] = gridLookUpEditExpUser.EditValue;
                newRow["docNumber"] = textEditDocNumber.EditValue;
                newRow["description"] = memoEditDescription.EditValue;
                newRow["medicineID"] = medicine["medicineID"];
                newRow["medicineName"] = medicine["name"];
                newRow["latinName"] = medicine["latinName"];
                newRow["medicineTypeName"] = medicine["medicineTypeName"];
                newRow["unitType"] = medicine["unitType"];
                newRow["shape"] = medicine["shape"];
                newRow["quantity"] = medicine["quantity"];
                newRow["validDate"] = medicine["validDate"];
                newRow["incTypeID"] = "1";
                newRow["incTypeName"] = medicine["incTypeName"];
                newRow["price"] = medicine["price"];
                Multi_data dat = new Multi_data();
                bool chk_exist = true;
                int z = 0;
                while (chk_exist)
                {
                    if (dict.Count > z)
                    {
                        dat = dict[Convert.ToString(z)];
                        if (dat.code == medicine["medicineID"].ToString())
                        {
                            if (dat.price == Convert.ToDouble(medicine["price"]))
                            {
                                newRow["count"] = dat.quant.ToString();
                                chk_exist = false;
                            }
                        }
                        z++;
                    }
                    else chk_exist = false;
                }
                // if (dict.ContainsKey(medicine["medicineID"].ToString()))
                // {
                //    newRow["count"] = dict[medicine["medicineID"].ToString()];
                // }
                JournalDataSet.Instance.ExpDetail.Rows.Add(newRow);
            }
        }

        private string getNotExistId()
        {
            string newID = null;
            DataRow[] rows = null;
            do
            {
                newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                rows = JournalDataSet.Instance.ExpDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND expJournalID = '" + docView["id"] + "' AND id = '" + newID + "'");
            } while (rows != null && rows.Length > 0);

            return newID;
        }

        private void reload(bool isReload)
        {
            //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
            //InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
            if (isReload || InformationDataSet.Instance.SystemUserOther.Rows.Count == 0)
                InformationDataSet.SystemUserOtherTableAdapter.Fill(InformationDataSet.Instance.SystemUserOther, HospitalUtil.getHospitalId(), SystemUtil.DOCTOR);
            if (isReload || JournalDataSet.Instance.SystemUserExp.Rows.Count == 0)
                JournalDataSet.SystemUserExpTableAdapter.Fill(JournalDataSet.Instance.SystemUserExp, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
            if (isReload || JournalDataSet.Instance.SystemUserExpOther.Rows.Count == 0)
                JournalDataSet.SystemUserExpOtherTableAdapter.Fill(JournalDataSet.Instance.SystemUserExpOther, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
            if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
            if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
            if (isReload || InformationDataSet.Instance.ExpTypeOther.Rows.Count == 0)
                InformationDataSet.ExpTypeOtherTableAdapter.Fill(InformationDataSet.Instance.ExpTypeOther, HospitalUtil.getHospitalId());
            //ProgramUtil.closeWaitDialog();
        }

        private void reloadMedicine()
        {
            if (Visible && dateEditExpDate.EditValue != DBNull.Value && dateEditExpDate.EditValue != null && !dateEditExpDate.Text.Equals("") &&
                gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null && !gridLookUpEditUser.EditValue.ToString().Equals("") &&
                ((radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && !treeListLookUpEditWarehouse.EditValue.ToString().Equals("")) ||
                (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && !treeListLookUpEditWard.EditValue.ToString().Equals(""))))
            {
                JournalDataSet.MedicineBalTableAdapter.Fill(JournalDataSet.Instance.MedicineBal, HospitalUtil.getHospitalId(), gridLookUpEditUser.EditValue.ToString(), radioGroupType.SelectedIndex, (radioGroupType.SelectedIndex == 0 ? treeListLookUpEditWarehouse.EditValue.ToString() : ""), (radioGroupType.SelectedIndex == 1 ? treeListLookUpEditWard.EditValue.ToString() : ""), dateEditExpDate.Text);
            }
        }

        private void changeType()
        {
            if (radioGroupType.SelectedIndex == 0)
            {
                labelControlWarehouse.Location = new Point(100, 147);
                treeListLookUpEditWard.Location = new Point(167, 12);
                treeListLookUpEditWarehouse.Location = new Point(167, 144);

                treeListLookUpEditWard.Visible = false;
                treeListLookUpEditWarehouse.Visible = true;
                labelControlWarehouse.Text = "Эмийн сан*:";
            }
            else if (radioGroupType.SelectedIndex == 1)
            {
                labelControlWarehouse.Location = new Point(123, 147);
                treeListLookUpEditWard.Location = new Point(167, 144);
                treeListLookUpEditWarehouse.Location = new Point(167, 12);

                treeListLookUpEditWard.Visible = true;
                treeListLookUpEditWarehouse.Visible = false;
                labelControlWarehouse.Text = "Тасаг*:";
            }
        }

        private void changeExpType()
        {
            if (treeListLookUpEditExpType.EditValue == null || treeListLookUpEditExpType.EditValue == DBNull.Value || treeListLookUpEditExpType.Text.Equals(""))
                return;

            int expType = Convert.ToInt32(treeListLookUpEditExpType.EditValue);
            if (expType == 1 || expType == 3)
            {
                labelControlExpWarehouse.Location = new Point(39, 251);
                treeListLookUpEditExpWard.Location = new Point(167, 476);
                treeListLookUpEditExpWarehouse.Location = new Point(167, 248);

                treeListLookUpEditExpWard.Visible = false;
                treeListLookUpEditExpWarehouse.Visible = true;
                labelControlExpWarehouse.Text = "Хүлээн авах эмийн сан*:";
            }
            else if (expType == 2 || expType == 4)
            {
                labelControlExpWarehouse.Location = new Point(60, 251);
                treeListLookUpEditExpWard.Location = new Point(167, 248);
                treeListLookUpEditExpWarehouse.Location = new Point(167, 476);

                treeListLookUpEditExpWard.Visible = true;
                treeListLookUpEditExpWarehouse.Visible = false;
                labelControlExpWarehouse.Text = "Хүлээн авах тасаг*:";
            }
            if (expType == 1 || expType == 2)
            {
                gridLookUpEditExpType.Properties.ReadOnly = false;
            }
            else if (expType == 3 || expType == 4)
            {
                gridLookUpEditExpType.Properties.ReadOnly = true;
            }
        }

        private void calcPrice()
        {
            spinEditSumPrice.Value = spinEditCount.Value * spinEditPrice.Value;
        }

        private string getNodeText(int value)
        {
            for (int i = 0; i < nodes.Count; i++)
            {
                if (Convert.ToInt32(nodes[i].GetValue("id")) == value)
                    return nodes[i].GetValue("text").ToString();
            }
            return "";
        }

        private bool check()
        {
            //Instance.Validate();
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();
            if (dateEditExpDate.EditValue == DBNull.Value || dateEditExpDate.EditValue == null || dateEditExpDate.Text.Equals(""))
            {
                errorText = "Огноог оруулна уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditExpDate.ErrorText = errorText;
                isRight = false;
                dateEditExpDate.Focus();
            }
            if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.Text.Equals(""))
            {
                errorText = "Няравыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditUser.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
            }
            if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.Text.Equals("")))
            {
                errorText = "Агуулахыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWarehouse.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWarehouse.Focus();
                }
            }
            if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals("")))
            {
                errorText = "Тасгийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWard.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWard.Focus();
                }
            }

            if (treeListLookUpEditExpType.EditValue == DBNull.Value || treeListLookUpEditExpType.EditValue == null || treeListLookUpEditExpType.Text.Equals(""))
            {
                errorText = "Гүйлгээний төрлийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditExpType.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditExpType.Focus();
                }
            }
            else
            {
                int expType = Convert.ToInt32(treeListLookUpEditExpType.EditValue);
                if (expType == 3 || expType == 4)
                {
                    if (gridLookUpEditExpUser.EditValue == DBNull.Value || gridLookUpEditExpUser.EditValue == null || gridLookUpEditExpUser.Text.Equals(""))
                    {
                        errorText = "Зарлагадах няравыг сонгоно уу";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditExpUser.ErrorText = errorText;
                        if (isRight)
                        {
                            isRight = false;
                            gridLookUpEditExpUser.Focus();
                        }
                    }
                    if (expType == 3 && (treeListLookUpEditExpWarehouse.EditValue == DBNull.Value || treeListLookUpEditExpWarehouse.EditValue == null || treeListLookUpEditExpWarehouse.Text.Equals("")))
                    {
                        errorText = "Зарлагдах эмийн санг сонгоно уу";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        treeListLookUpEditExpWarehouse.ErrorText = errorText;
                        if (isRight)
                        {
                            isRight = false;
                            treeListLookUpEditExpWarehouse.Focus();
                        }
                    }
                    if (expType == 4 && (treeListLookUpEditExpWard.EditValue == DBNull.Value || treeListLookUpEditExpWard.EditValue == null || treeListLookUpEditExpWard.Text.Equals("")))
                    {
                        errorText = "Зарлагдах тасгийг сонгоно уу";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        treeListLookUpEditExpWard.ErrorText = errorText;
                        if (isRight)
                        {
                            isRight = false;
                            treeListLookUpEditExpWard.Focus();
                        }
                    }
                }
                else
                {
                    if (gridLookUpEditExpType.EditValue == DBNull.Value || gridLookUpEditExpType.EditValue == null || gridLookUpEditExpType.Text.Equals(""))
                    {
                        errorText = "Зарлагын төрлийг сонгоно уу";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditExpType.ErrorText = errorText;
                        if (isRight)
                        {
                            isRight = false;
                            gridLookUpEditExpType.Focus();
                        }
                    }
                }
            }

            if ((gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.Text.Equals("")) && checkEditMultiSelection.Checked == false)
            {
                errorText = "Эмийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditMedicine.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
            }
            if (rowID == null && checkEditMultiSelection.Checked)
            {
                errorText = "Эмнүүдээс сонгож чагтлана уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                popupContainerEditMedicineBal.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    popupContainerEditMedicineBal.Focus();
                }
            }
            else if (rowID != null && rowID.Length == 0 && checkEditMultiSelection.Checked)
            {
                errorText = "Эмнүүдээс сонгож чагтлана уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                popupContainerEditMedicineBal.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    popupContainerEditMedicineBal.Focus();
                }
            }
            if (isRight)
            {
                for (int i = 0; i < rowID.Length; i++)
                {
                    if (Convert.ToDouble(gridViewMed.GetRowCellValue(rowID[i], gridColumnDupCount)) == 0 || Convert.ToDouble(gridViewMed.GetRowCellValue(rowID[i], gridColumnPrice)) == 0)
                    {
                        errorText = "Эмийн тоо ширхэгийг оруулна уу!";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        popupContainerEditMedicineBal.ErrorText = errorText;
                        isRight = false;
                        popupContainerEditMedicineBal.Focus();
                        break;
                    }
                }
            }
            if (spinEditCount.Value == 0 && checkEditMultiSelection.Checked == false)
            {
                errorText = "Тоо, хэмжээг оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                spinEditCount.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    spinEditCount.Focus();
                }
            }
            if (checkEditMultiSelection.Checked == false && isRight && existMedicine(gridLookUpEditMedicine.EditValue.ToString()))
            {
                errorText = "Эм, үнэ давхацсан байна";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditMedicine.ErrorText = errorText;
                isRight = false;
                gridLookUpEditMedicine.Focus();
            }
            if (!isRight && !text.Equals(""))
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        public bool existMedicine(string medicinePrice)
        {
            DataRow[] rows = JournalDataSet.Instance.ExpDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND expJournalID = '" + docView["id"] + "' AND id <> '" + currView["id"] + "'");
            for (int i = 0; rows != null && i < rows.Length; i++)
            {
                if (medicinePrice.Equals(rows[i]["medicinePrice"].ToString()))
                    return true;
            }

            return false;
        }

        private void save()
        {
            ProgramUtil.IsSaveEvent = true;
            ProgramUtil.IsShowPopup = true;
            if (check())
            {
                if (ProgramUtil.checkLockMedicine(dateEditExpDate.EditValue.ToString()))
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                    DataRowView expUser = (DataRowView)gridLookUpEditExpUser.GetSelectedDataRow();
                    DataRowView expTypeRow = (DataRowView)gridLookUpEditExpType.GetSelectedDataRow();
                    DataRowView expWarehouse = (DataRowView)treeListLookUpEditExpWarehouse.GetSelectedDataRow();
                    DataRowView expWard = (DataRowView)treeListLookUpEditExpWard.GetSelectedDataRow();
                    //DataRowView medicine = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                    DataRowView doctor = (DataRowView)gridLookUpEditDoctor.GetSelectedDataRow();
                    int expType = Convert.ToInt32(treeListLookUpEditExpType.EditValue);
                    string incid = "";
                    bool update = false;
                    SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                    myConn.Open();
                    string query = "select * from ExpJournal where expType in ('3','4') and id = '" + textEditID.EditValue + "'";
                    SqlCommand oCmd = new SqlCommand(query, myConn);
                    using (SqlDataReader oReader = oCmd.ExecuteReader())
                    {
                        while (oReader.Read())
                        {
                            if (oReader.HasRows)
                            {
                                incid = oReader["incJournalID"].ToString();
                                update = true;
                            }
                        }
                    }
                    myConn.Close();
                    if ((expType == 3 || expType == 4) && update == false)
                    {
                        incid = JournalDataSet.QueriesTableAdapter.get_new_id(gridLookUpEditExpUser.EditValue.ToString());


                        JournalDataSet.Instance.IncJournal.idColumn.DefaultValue = incid;
                        JournalDataSet.Instance.IncDetail.incJournalIDColumn.DefaultValue = incid;
                        DataRowView IncView = (DataRowView)JournalDataSet.IncJournalBindingSource.AddNew();

                        IncView["hospitalID"] = HospitalUtil.getHospitalId();
                        IncView["actionUserID"] = UserUtil.getUserId();
                        IncView["date"] = dateEditExpDate.EditValue;
                        IncView["userID"] = gridLookUpEditExpUser.EditValue;
                        IncView["userName"] = expUser["fullName"];
                        IncView["roleName"] = expUser["roleName"];
                        IncView["type"] = radioGroupType.SelectedIndex;
                        IncView["incTypeID"] = 4;
                        IncView["incTypeName"] = "Шилжүүлэг";
                        if (radioGroupType.SelectedIndex == 0)
                        {
                            IncView["wardID"] = null;
                            IncView["warehouseID"] = treeListLookUpEditExpWarehouse.EditValue;
                            IncView["warehouseName"] = expWarehouse["name"];
                            IncView["warehouseDesc"] = expWarehouse["description"];
                        }
                        else if (radioGroupType.SelectedIndex == 1)
                        {
                            IncView["wardID"] = treeListLookUpEditExpWard.EditValue;
                            IncView["warehouseID"] = null;
                            IncView["warehouseName"] = expWard["name"];
                            IncView["warehouseDesc"] = expWard["description"];
                        }
                        //if (expType == 3)
                        //{
                        //    IncView["wardID"] = null;
                        //    IncView["warehouseID"] = treeListLookUpEditExpWarehouse.EditValue;
                        //    IncView["warehouseName"] = expWarehouse["name"];
                        //    IncView["warehouseDesc"] = expWarehouse["description"];
                        //}
                        //else if (expType == 4)
                        //{
                        //    IncView["wardID"] = treeListLookUpEditExpWard.EditValue;
                        //    IncView["warehouseID"] = null;
                        //    IncView["warehouseName"] = expWard["name"];
                        //    IncView["warehouseDesc"] = expWard["description"];
                        //}
                        IncView["orderGoodsID"] = null;
                        IncView["tenderID"] = null;
                        IncView["supplierID"] = gridLookUpEditUser.EditValue;
                        IncView["supplierName"] = gridLookUpEditUser.Text;
                        IncView.EndEdit();

                        if (checkEditMultiSelection.Checked == false)
                        {
                            long newINCID = Convert.ToInt64(JournalDataSet.QueriesTableAdapter.get_new_id(gridLookUpEditExpUser.EditValue.ToString()));
                            JournalDataSet.Instance.IncDetail.idColumn.DefaultValue = newINCID;
                            DataRowView incnewRow = (DataRowView)JournalDataSet.IncDetailBindingSource.AddNew();

                            incnewRow["date"] = dateEditExpDate.EditValue;
                            incnewRow["userID"] = gridLookUpEditExpUser.EditValue.ToString();
                            incnewRow["hospitalID"] = HospitalUtil.getHospitalId();
                            incnewRow["actionUserID"] = UserUtil.getUserId();
                            incnewRow["userName"] = expUser["fullName"];
                            incnewRow["roleName"] = expUser["roleName"];
                            incnewRow["type"] = radioGroupType.SelectedIndex;
                            if (radioGroupType.SelectedIndex == 0)
                            {
                                incnewRow["wardID"] = null;
                                incnewRow["warehouseID"] = treeListLookUpEditExpWarehouse.EditValue;
                                incnewRow["warehouseName"] = expWarehouse["name"];
                                incnewRow["warehouseDesc"] = expWarehouse["description"];
                            }
                            else if (radioGroupType.SelectedIndex == 1)
                            {
                                incnewRow["wardID"] = treeListLookUpEditExpWard.EditValue;
                                incnewRow["warehouseID"] = null;
                                incnewRow["warehouseName"] = expWard["name"];
                                incnewRow["warehouseDesc"] = expWard["description"];
                            }
                            //if (expType == 3)
                            //{
                            //    IncView["wardID"] = null;
                            //    IncView["warehouseID"] = treeListLookUpEditExpWarehouse.EditValue;
                            //    IncView["warehouseName"] = expWarehouse["name"];
                            //    IncView["warehouseDesc"] = expWarehouse["description"];
                            //}
                            //else if (expType == 4)
                            //{
                            //    IncView["wardID"] = treeListLookUpEditExpWard.EditValue;
                            //    IncView["warehouseID"] = null;
                            //    IncView["warehouseName"] = expWard["name"];
                            //    IncView["warehouseDesc"] = expWard["description"];
                            //}
                            incnewRow["tenderID"] = null;
                            incnewRow["supplierID"] = gridLookUpEditUser.EditValue;
                            incnewRow["supplierName"] = gridLookUpEditUser.Text;

                            DataRow[] incmedicine = JournalDataSet.Instance.MedicineBal.Select("medicineID = '" + gridLookUpEditMedicine.EditValue + "'");
                            //DataRow[] incmedicine = gridView3.get
                            incnewRow["conMedicineID"] = incmedicine[0]["packageID"];
                            incnewRow["orderGoodsDetailID"] = null;
                            incnewRow["medicineID"] = incmedicine[0]["medicineID"];
                            incnewRow["medicineName"] = incmedicine[0]["name"];
                            incnewRow["latinName"] = incmedicine[0]["latinName"];
                            incnewRow["unitType"] = incmedicine[0]["unitType"];
                            incnewRow["shape"] = incmedicine[0]["shape"];
                            incnewRow["quantity"] = incmedicine[0]["quantity"];
                            incnewRow["validDate"] = incmedicine[0]["validDate"];
                            incnewRow["medicineTypeName"] = incmedicine[0]["medicineTypeName"];
                            incnewRow["incTypeID"] = 4;
                            incnewRow["incTypeName"] = "Шилжүүлэг";
                            incnewRow["price"] = spinEditPrice.Value;
                            incnewRow["count"] = spinEditCount.Value;
                            incnewRow["serial"] = incmedicine[0]["serial"];
                            incnewRow.EndEdit();
                        }
                        else
                        {
                            long newINCID = Convert.ToInt64(JournalDataSet.QueriesTableAdapter.get_new_id(gridLookUpEditExpUser.EditValue.ToString()));
                            for (int x = 0; x < rowID.Length; x++, newINCID++)
                            {
                                JournalDataSet.Instance.IncDetail.idColumn.DefaultValue = newINCID;
                                DataRowView incnewRow = (DataRowView)JournalDataSet.IncDetailBindingSource.AddNew();

                                DataRow incmedicine = gridViewMed.GetDataRow(rowID[x]);

                                incnewRow["date"] = dateEditExpDate.EditValue;
                                incnewRow["userID"] = gridLookUpEditExpUser.EditValue.ToString();
                                incnewRow["hospitalID"] = HospitalUtil.getHospitalId();
                                incnewRow["actionUserID"] = UserUtil.getUserId();
                                incnewRow["userName"] = expUser["fullName"];
                                incnewRow["roleName"] = expUser["roleName"];
                                incnewRow["type"] = radioGroupType.SelectedIndex;
                                if (radioGroupType.SelectedIndex == 0)
                                {
                                    incnewRow["wardID"] = null;
                                    incnewRow["warehouseID"] = treeListLookUpEditExpWarehouse.EditValue;
                                    incnewRow["warehouseName"] = expWarehouse["name"];
                                    incnewRow["warehouseDesc"] = expWarehouse["description"];
                                }
                                else if (radioGroupType.SelectedIndex == 1)
                                {
                                    incnewRow["wardID"] = treeListLookUpEditExpWard.EditValue;
                                    incnewRow["warehouseID"] = null;
                                    incnewRow["warehouseName"] = expWard["name"];
                                    incnewRow["warehouseDesc"] = expWard["description"];
                                }
                                //if (expType == 3)
                                //{
                                //    IncView["wardID"] = null;
                                //    IncView["warehouseID"] = treeListLookUpEditExpWarehouse.EditValue;
                                //    IncView["warehouseName"] = expWarehouse["name"];
                                //    IncView["warehouseDesc"] = expWarehouse["description"];
                                //}
                                //else if (expType == 4)
                                //{
                                //    IncView["wardID"] = treeListLookUpEditExpWard.EditValue;
                                //    IncView["warehouseID"] = null;
                                //    IncView["warehouseName"] = expWard["name"];
                                //    IncView["warehouseDesc"] = expWard["description"];
                                //}
                                incnewRow["tenderID"] = null;
                                incnewRow["supplierID"] = gridLookUpEditUser.EditValue;
                                incnewRow["supplierName"] = gridLookUpEditUser.Text;

                                incnewRow["orderGoodsDetailID"] = null;
                                incnewRow["conMedicineID"] = incmedicine["packageID"];
                                incnewRow["medicineID"] = incmedicine["medicineID"];
                                incnewRow["medicineName"] = incmedicine["name"];
                                incnewRow["latinName"] = incmedicine["latinName"];
                                incnewRow["unitType"] = incmedicine["unitType"];
                                incnewRow["shape"] = incmedicine["shape"];
                                incnewRow["quantity"] = incmedicine["quantity"];
                                incnewRow["validDate"] = incmedicine["validDate"];
                                incnewRow["medicineTypeName"] = incmedicine["medicineTypeName"];
                                incnewRow["incTypeID"] = 4;
                                incnewRow["incTypeName"] = "Шилжүүлэг";
                                incnewRow["price"] = incmedicine["price"];
                                incnewRow["serial"] = incmedicine["serial"];
                                Multi_data dat = new Multi_data();
                                bool chk_exist = true;
                                int z = 0;
                                while (chk_exist)
                                {
                                    if (dict.Count > z)
                                    {
                                        dat = dict[Convert.ToString(z)];
                                        if (dat.code == incmedicine["medicineID"].ToString())
                                        {
                                            if (dat.price == Convert.ToDouble(incmedicine["price"]))
                                            {
                                                incnewRow["count"] = dat.quant.ToString();
                                                chk_exist = false;
                                            }
                                        }
                                        z++;
                                    }
                                    else chk_exist = false;
                                }
                                incnewRow.EndEdit();
                            }
                        }
                        JournalDataSet.IncJournalTableAdapter.Update(JournalDataSet.Instance.IncJournal);
                        JournalDataSet.IncDetailTableAdapter.Update(JournalDataSet.Instance.IncDetail);

                    }

                    docView["userName"] = user["fullName"];
                    docView["roleName"] = user["roleName"];
                    if (radioGroupType.SelectedIndex == 0)
                    {
                        docView["wardID"] = null;
                        docView["warehouseName"] = warehouse["name"];
                        docView["warehouseDesc"] = warehouse["description"];

                        currView["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                        currView["wardID"] = null;
                        currView["warehouseName"] = warehouse["name"];
                        currView["warehouseDesc"] = warehouse["description"];
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {
                        docView["warehouseID"] = null;
                        docView["warehouseName"] = ward["name"];
                        docView["warehouseDesc"] = ward["description"];

                        currView["warehouseID"] = null;
                        currView["wardID"] = treeListLookUpEditWard.EditValue;
                        currView["warehouseName"] = ward["name"];
                        currView["warehouseDesc"] = ward["description"];
                    }

                    currView["expUserID"] = gridLookUpEditExpUser.EditValue;
                    if (expUser != null)
                    {
                        docView["expUserName"] = expUser["fullName"];
                        docView["expRoleName"] = expUser["roleName"];

                        currView["expUserName"] = expUser["fullName"];
                        currView["expRoleName"] = expUser["roleName"];
                    }
                    if (doctor != null)
                    {
                        docView["doctorName"] = doctor["fullName"];
                    }
                    if ((expType == 1 || expType == 3))
                    {
                        docView["expWardID"] = null;
                        docView["wtype"] = 0;

                        currView["expWarehouseID"] = treeListLookUpEditExpWarehouse.EditValue;
                        currView["expWardID"] = null;
                        if (expWarehouse != null)
                        {
                            docView["expWarehouseName"] = expWarehouse["name"];
                            docView["expWarehouseDesc"] = expWarehouse["description"];

                            currView["expWarehouseName"] = expWarehouse["name"];
                            currView["expWarehouseDesc"] = expWarehouse["description"];
                        }
                        else
                        {
                            docView["expWarehouseName"] = null;
                            docView["expWarehouseDesc"] = null;

                            currView["expWarehouseName"] = null;
                            currView["expWarehouseDesc"] = null;
                        }
                    }
                    else if ((expType == 2 || expType == 4))
                    {
                        docView["expWarehouseID"] = null;
                        docView["wtype"] = 1;

                        currView["expWarehouseID"] = null;
                        currView["expWardID"] = treeListLookUpEditExpWard.EditValue;
                        if (expWard != null)
                        {
                            docView["expWarehouseName"] = expWard["name"];
                            docView["expWarehouseDesc"] = expWard["description"];

                            currView["expWarehouseName"] = expWard["name"];
                            currView["expWarehouseDesc"] = expWard["description"];
                        }
                        else
                        {
                            docView["expWarehouseName"] = null;
                            docView["expWarehouseDesc"] = null;

                            currView["expWarehouseName"] = null;
                            currView["expWarehouseDesc"] = null;
                        }
                    }
                    if (expType == 1 || expType == 2)
                    {
                        docView["expTypeName"] = expTypeRow["name"];
                    }
                    else if (expType == 3 || expType == 4)
                    {
                        docView["expTypeName"] = null;
                        docView["incJournalID"] = incid;
                    }
                    docView.EndEdit();


                    if (checkEditMultiSelection.Checked == false)
                    {
                        currView["price"] = spinEditPrice.EditValue;
                        currView["count"] = spinEditCount.EditValue;



                        currView["incTypeID"] = "1";
                        currView.EndEdit();
                    }
                    else
                    {
                        JournalDataSet.ExpDetailBindingSource.RemoveCurrent();
                        addMultipleRow();
                    }

                    if (checkEditMultiSelection.Checked == false)
                    {
                        for (int i = 0; i < JournalDataSet.ExpDetailBindingSource.Count; i++)
                        {
                            DataRowView rows = (DataRowView)JournalDataSet.ExpDetailBindingSource[i];
                            if (rows["expJournalID"].ToString().Equals(docView["id"].ToString()) &&
                                !rows["id"].ToString().Equals(currView["id"].ToString()))
                            {
                                rows["date"] = dateEditExpDate.EditValue;
                                rows["userID"] = gridLookUpEditUser.EditValue;
                                rows["type"] = radioGroupType.SelectedIndex;
                                rows["expType"] = treeListLookUpEditExpType.EditValue;
                                rows["expUserID"] = gridLookUpEditExpUser.EditValue;
                                rows["docNumber"] = textEditDocNumber.EditValue;
                                rows["description"] = memoEditDescription.EditValue;

                                if (radioGroupType.SelectedIndex == 0)
                                {
                                    rows["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                                    rows["wardID"] = null;
                                    rows["warehouseName"] = warehouse["name"];
                                    rows["warehouseDesc"] = warehouse["description"];
                                }
                                else if (radioGroupType.SelectedIndex == 0)
                                {
                                    rows["warehouseID"] = null;
                                    rows["wardID"] = treeListLookUpEditWard.EditValue;
                                    rows["warehouseName"] = ward["name"];
                                    rows["warehouseDesc"] = ward["description"];
                                }
                                if (expUser != null)
                                {
                                    rows["expUserName"] = expUser["fullName"];
                                    rows["expRoleName"] = expUser["roleName"];
                                }
                                if ((expType == 1 || expType == 3))
                                {
                                    rows["expWarehouseID"] = treeListLookUpEditExpWarehouse.EditValue;
                                    rows["expWardID"] = null;
                                    if (expWarehouse != null)
                                    {
                                        rows["expWarehouseName"] = expWarehouse["name"];
                                        rows["expWarehouseDesc"] = expWarehouse["description"];
                                    }
                                    else
                                    {
                                        rows["expWarehouseName"] = null;
                                        rows["expWarehouseDesc"] = null;
                                    }
                                }
                                else if ((expType == 2 || expType == 4))
                                {
                                    rows["expWarehouseID"] = null;
                                    rows["expWardID"] = treeListLookUpEditExpWard.EditValue;
                                    if (expWard != null)
                                    {
                                        rows["expWarehouseName"] = expWard["name"];
                                        rows["expWarehouseDesc"] = expWard["description"];
                                    }
                                    else
                                    {
                                        rows["expWarehouseName"] = null;
                                        rows["expWarehouseDesc"] = null;
                                    }
                                }
                                if (expType == 1 || expType == 2)
                                {
                                    rows["expTypeName"] = expTypeRow["name"];
                                }
                                else if (expType == 3 || expType == 4)
                                {
                                    rows["expTypeName"] = null;
                                }
                                rows.EndEdit();
                            }
                        }
                    }
                    //DataRow[] rows = JournalDataSet.Instance.ExpDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND expJournalID = '" + docView["id"] + "' AND id <> '" + currView["id"] + "'");
                    //for (int i = 0; rows != null && i < rows.Length; i++)
                    //{
                    //    rows[i]["date"] = dateEditExpDate.EditValue;
                    //    rows[i]["userID"] = gridLookUpEditUser.EditValue;
                    //    rows[i]["type"] = radioGroupType.SelectedIndex;
                    //    rows[i]["expType"] = treeListLookUpEditExpType.EditValue;
                    //    rows[i]["expUserID"] = gridLookUpEditExpUser.EditValue;
                    //    rows[i]["docNumber"] = textEditDocNumber.EditValue;
                    //    rows[i]["description"] = memoEditDescription.EditValue;

                    //    if (radioGroupType.SelectedIndex == 0)
                    //    {
                    //        rows[i]["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                    //        rows[i]["wardID"] = null;
                    //        rows[i]["warehouseName"] = warehouse["name"];
                    //        rows[i]["warehouseDesc"] = warehouse["description"];
                    //    }
                    //    else if (radioGroupType.SelectedIndex == 0)
                    //    {
                    //        rows[i]["warehouseID"] = null;
                    //        rows[i]["wardID"] = treeListLookUpEditWard.EditValue;
                    //        rows[i]["warehouseName"] = ward["name"];
                    //        rows[i]["warehouseDesc"] = ward["description"];
                    //    }
                    //    if (expUser != null)
                    //    {
                    //        rows[i]["expUserName"] = expUser["fullName"];
                    //        rows[i]["expRoleName"] = expUser["roleName"];
                    //    }
                    //    if ((expType == 1 || expType == 3))
                    //    {
                    //        rows[i]["expWarehouseID"] = treeListLookUpEditExpWarehouse.EditValue;
                    //        rows[i]["expWardID"] = null;
                    //        if (expWarehouse != null)
                    //        {
                    //            rows[i]["expWarehouseName"] = expWarehouse["name"];
                    //            rows[i]["expWarehouseDesc"] = expWarehouse["description"];
                    //        }
                    //        else
                    //        {
                    //            rows[i]["expWarehouseName"] = null;
                    //            rows[i]["expWarehouseDesc"] = null;
                    //        }
                    //    }
                    //    else if ((expType == 2 || expType == 4))
                    //    {
                    //        rows[i]["expWarehouseID"] = null;
                    //        rows[i]["expWardID"] = treeListLookUpEditExpWard.EditValue;
                    //        if (expWard != null)
                    //        {
                    //            rows[i]["expWarehouseName"] = expWard["name"];
                    //            rows[i]["expWarehouseDesc"] = expWard["description"];
                    //        }
                    //        else
                    //        {
                    //            rows[i]["expWarehouseName"] = null;
                    //            rows[i]["expWarehouseDesc"] = null;
                    //        }
                    //    }
                    //    if (expType == 1 || expType == 2)
                    //    {
                    //        rows[i]["expTypeName"] = expTypeRow["name"];
                    //    }
                    //    else if (expType == 3 || expType == 4)
                    //    {
                    //        rows[i]["expTypeName"] = null;
                    //    }
                    //    rows[i].EndEdit();
                    //}

                    JournalDataSet.ExpJournalTableAdapter.Update(JournalDataSet.Instance.ExpJournal);
                    JournalDataSet.ExpDetailTableAdapter.Update(JournalDataSet.Instance.ExpDetail);

                    ProgramUtil.closeWaitDialog();

                    if (checkEditMultiSelection.Checked == false)
                    {
                        if (expType == 3 || expType == 4)
                        {
                            Close();
                        }
                        else
                        {
                            addRow();
                            gridLookUpEditMedicine.Focus();
                            reloadMedicine();
                        }
                    }
                    else
                    {
                        Close();
                    }
                }
                else
                {
                    XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
                }
            }
        }

        public void addMedicine(DataRowView view, object incTypeID, string incTypeText, decimal count, decimal price)
        {


            DataRowView row = (DataRowView)JournalDataSet.MedicineBalBindingSource.AddNew();
            row["medicineID"] = view["id"];
            row["name"] = view["name"];
            row["latinName"] = view["latinName"];
            row["medicineTypeName"] = view["medicineTypeName"];
            row["unitType"] = view["unitType"];
            row["barcode"] = view["barcode"];
            row["shape"] = view["shape"];
            row["quantity"] = view["quantity"];
            row["validDate"] = view["validDate"];
            row["incTypeID"] = incTypeID;
            row["incTypeName"] = incTypeText;
            row["price"] = price;
            row["count"] = 0;
            row.EndEdit();

            //gridLookUpEditMedicine.EditValue = view["id"].ToString() + incTypeID.ToString() + price;
            gridLookUpEditMedicine.EditValue = view["id"].ToString();

            spinEditCount.Value = count;
            spinEditPrice.Value = price;
        }

        #endregion

        private void gridViewMed_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == gridColumnDupCount)
            {
                if (Convert.ToDouble(e.Value) > Convert.ToDouble(gridViewMed.GetFocusedRowCellValue(gridColumnBal)))
                {
                    XtraMessageBox.Show("Үлдэгдэл хүрэхгүй байна!");
                    gridViewMed.SetFocusedRowCellValue(gridColumnDupCount, gridViewMed.GetFocusedRowCellValue(gridColumnBal));
                }
            }
        }

        private void popupContainerEditMedicineBal_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (Convert.ToInt32(gridViewMed.DataRowCount) > 0)
            {
                int k = 0;
                dict_draw.Clear();
                for (int i = 0; i < gridViewMed.DataRowCount; i++)
                {
                    DataRow[] rows = JournalDataSet.Instance.MedicineBal.Select("name = '" + gridViewMed.GetRowCellValue(i, gridColumName) + "'", "validDate ASC");
                    if (rows.Length > 1)
                    {
                        if (Convert.ToString(gridViewMed.GetRowCellValue(i, gridColumnValidatedDate)) == Convert.ToString(rows[0]["validDate"]))
                        {
                            Multi_draw draw = new Multi_draw();
                            draw.name = Convert.ToString(gridViewMed.GetRowCellValue(i, gridColumName));
                            draw.validDate = Convert.ToString(gridViewMed.GetRowCellValue(i, gridColumnValidatedDate));
                            dict_draw.Add(Convert.ToString(k), draw);
                            k++;
                        }
                    }
                }
            }
        }

        private void popupContainerEditMedicineBal_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(gridViewMed.DataRowCount) > 0)
            {
                int k = 0;
                dict_draw.Clear();
                for (int i = 0; i < gridViewMed.DataRowCount; i++)
                {
                    DataRow[] rows = JournalDataSet.Instance.MedicineBal.Select("name = '" + gridViewMed.GetRowCellValue(i, gridColumName) + "'", "validDate ASC");
                    if (rows.Length > 1)
                    {
                        if (Convert.ToString(gridViewMed.GetRowCellValue(i, gridColumnValidatedDate)) == Convert.ToString(rows[0]["validDate"]))
                        {
                            Multi_draw draw = new Multi_draw();
                            draw.name = Convert.ToString(gridViewMed.GetRowCellValue(i, gridColumName));
                            draw.validDate = Convert.ToString(gridViewMed.GetRowCellValue(i, gridColumnValidatedDate));
                            dict_draw.Add(Convert.ToString(k), draw);
                            k++;
                        }
                    }
                }
            }
        }

        private void gridViewMed_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.DisplayText.Equals(""))
            {
                Multi_draw chk_draw = new Multi_draw();
                bool chk_existed = true;
                int x = 0;
                while (chk_existed)
                {
                    if (dict_draw.Count > x)
                    {
                        chk_draw = dict_draw[Convert.ToString(x)];
                        if (chk_draw.name == Convert.ToString(gridViewMed.GetRowCellValue(e.RowHandle, gridColumName)))
                        {
                            if (chk_draw.validDate == Convert.ToString(gridViewMed.GetRowCellValue(e.RowHandle, gridColumnValidatedDate)))
                            {
                                e.Appearance.BackColor = Color.DarkSalmon;
                                chk_existed = false;
                            }
                        }
                        x++;
                    }
                    else chk_existed = false;
                }


            }

            if (!e.DisplayText.Equals(""))
            {
                if (Convert.ToDateTime(gridViewMed.GetRowCellValue(e.RowHandle, gridColumnValidatedDate)) < DateTime.Now)
                {
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.ForeColor = Color.White;
                }
                else if ((Convert.ToDateTime(gridViewMed.GetRowCellValue(e.RowHandle, gridColumnValidatedDate)) - DateTime.Now).TotalDays <= 7)
                {
                    e.Appearance.BackColor = Color.Gold;
                }
            }
        }

        private void gridLookUpEditMedicine_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(gridView3.DataRowCount) > 0)
            {
                int k = 0;
                dict_draw.Clear();
                for (int i = 0; i < gridView3.DataRowCount; i++)
                {
                    DataRow[] rows = JournalDataSet.Instance.MedicineBal.Select("name = '" + gridView3.GetRowCellValue(i, gridColumn15) + "'", "validDate ASC");
                    if (rows.Length > 1)
                    {
                        if (Convert.ToString(gridView3.GetRowCellValue(i, gridColumnValidDate)) == Convert.ToString(rows[0]["validDate"]))
                        {
                            Multi_draw draw = new Multi_draw();
                            draw.name = Convert.ToString(gridView3.GetRowCellValue(i, gridColumn15));
                            draw.validDate = Convert.ToString(gridView3.GetRowCellValue(i, gridColumnValidDate));
                            dict_draw.Add(Convert.ToString(k), draw);
                            k++;
                        }
                    }
                }
            }
        }

        private void gridViewMed_CustomRowFilter(object sender, RowFilterEventArgs e)
        {

        }

        private void gridViewMed_ColumnFilterChanged(object sender, EventArgs e)
        {
            for (int a = 0; a < gridViewMed.RowCount; a++)
            {
                gridViewMed.SetRowCellValue(a, gridColumnDupCount, 0);
            }
        }

        private void gridLookUpEditDoctor_EditValueChanged(object sender, EventArgs e)
        {

        }


    }
}