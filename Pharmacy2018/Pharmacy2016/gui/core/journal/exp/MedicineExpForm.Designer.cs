﻿namespace Pharmacy2016.gui.core.journal.exp
{
    partial class MedicineExpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicineExpForm));
            this.gridViewExpDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlExp = new DevExpress.XtraGrid.GridControl();
            this.gridViewExp = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn5 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnUserName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn37 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn21 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn22 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn18 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnUserRole = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn8 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn32 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn24 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn23 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn25 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn31 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnDoctor = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn4 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn41 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridViewDetail = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn14 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn28 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn39 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn30 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn20 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn9 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn10 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn33 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn34 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn35 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn27 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn26 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn40 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn11 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn15 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn29 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn38 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn12 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn36 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn19 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnSumPrice = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLeft = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.treeListWard = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnWard = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageExp = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageDetail = new DevExpress.XtraTab.XtraTabPage();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridFieldMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldExpTypeName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMedicine = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPrice = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSumPrice = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldIncTypeName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonDownload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemExp = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCustomize = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.radioGroupJournal = new DevExpress.XtraEditors.RadioGroup();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExpDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            this.dockPanelLeft.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageExp.SuspendLayout();
            this.xtraTabPageDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupJournal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewExpDetail
            // 
            this.gridViewExpDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn1,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumnDesc});
            this.gridViewExpDetail.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridViewExpDetail.GridControl = this.gridControlExp;
            this.gridViewExpDetail.LevelIndent = 1;
            this.gridViewExpDetail.Name = "gridViewExpDetail";
            this.gridViewExpDetail.OptionsBehavior.Editable = false;
            this.gridViewExpDetail.OptionsBehavior.ReadOnly = true;
            this.gridViewExpDetail.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExpDetail.OptionsView.ShowAutoFilterRow = true;
            this.gridViewExpDetail.OptionsView.ShowFooter = true;
            this.gridViewExpDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewExpDetail.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gridViewExpDetail_RowClick);
            this.gridViewExpDetail.ShownEditor += new System.EventHandler(this.gridViewExpDetail_ShownEditor);
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Эмийн нэр";
            this.gridColumn4.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn4.FieldName = "medicineName";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 150;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "ОУ нэр";
            this.gridColumn5.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn5.FieldName = "latinName";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            this.gridColumn5.Width = 159;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Ангилал";
            this.gridColumn2.CustomizationCaption = "Ангилал";
            this.gridColumn2.FieldName = "medicineTypeName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 3;
            this.gridColumn2.Width = 109;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тоо";
            this.gridColumn6.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn6.CustomizationCaption = "Тоо";
            this.gridColumn6.FieldName = "count";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 9;
            this.gridColumn6.Width = 80;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Нэгжийн үнэ";
            this.gridColumn7.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn7.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumn7.FieldName = "price";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 10;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Дүн";
            this.gridColumn8.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn8.CustomizationCaption = "Дүн";
            this.gridColumn8.FieldName = "sumPrice";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 11;
            this.gridColumn8.Width = 116;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Тун хэмжээ";
            this.gridColumn1.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn1.FieldName = "unitType";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Хэлбэр";
            this.gridColumn9.CustomizationCaption = "Хэлбэр";
            this.gridColumn9.FieldName = "shape";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 6;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Хүчинтэй хугацаа";
            this.gridColumn10.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn10.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn10.FieldName = "validDate";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 5;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Орлогын төрөл";
            this.gridColumn11.CustomizationCaption = "Орлогын төрөл";
            this.gridColumn11.FieldName = "incTypeName";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Хэмжих нэгж";
            this.gridColumn12.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn12.FieldName = "quantity";
            this.gridColumn12.MinWidth = 75;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 8;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Сериал";
            this.gridColumn13.CustomizationCaption = "Эмийн сериал дугаар";
            this.gridColumn13.FieldName = "serial";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 4;
            // 
            // gridColumnDesc
            // 
            this.gridColumnDesc.Caption = "Эмийн тайлбар";
            this.gridColumnDesc.FieldName = "detialDesc";
            this.gridColumnDesc.MinWidth = 100;
            this.gridColumnDesc.Name = "gridColumnDesc";
            this.gridColumnDesc.Visible = true;
            this.gridColumnDesc.VisibleIndex = 2;
            this.gridColumnDesc.Width = 100;
            // 
            // gridControlExp
            // 
            this.gridControlExp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlExp.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlExp.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlExp.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlExp.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlExp.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlExp.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "update"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 11, true, false, "Захиалгын хуудас татах", "download")});
            this.gridControlExp.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlExp_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.LevelTemplate = this.gridViewExpDetail;
            gridLevelNode1.RelationName = "ExpJournal_ExpDetail";
            this.gridControlExp.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlExp.Location = new System.Drawing.Point(0, 0);
            this.gridControlExp.MainView = this.gridViewExp;
            this.gridControlExp.Name = "gridControlExp";
            this.gridControlExp.Size = new System.Drawing.Size(774, 401);
            this.gridControlExp.TabIndex = 1;
            this.gridControlExp.UseEmbeddedNavigator = true;
            this.gridControlExp.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExp,
            this.gridViewDetail,
            this.gridViewExpDetail});
            this.gridControlExp.Click += new System.EventHandler(this.gridControlExp_Click);
            // 
            // gridViewExp
            // 
            this.gridViewExp.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand11,
            this.gridBand1,
            this.gridBand9,
            this.gridBand13,
            this.gridBand3,
            this.gridBand15});
            this.gridViewExp.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn5,
            this.gridColumn3,
            this.bandedGridColumn21,
            this.gridColumnUserRole,
            this.bandedGridColumn8,
            this.bandedGridColumn22,
            this.gridColumnUserName,
            this.bandedGridColumn32,
            this.bandedGridColumn24,
            this.bandedGridColumn23,
            this.bandedGridColumn25,
            this.bandedGridColumn31,
            this.bandedGridColumn2,
            this.bandedGridColumn4,
            this.bandedGridColumn18,
            this.bandedGridColumn37,
            this.bandedGridColumnDoctor,
            this.bandedGridColumn41});
            this.gridViewExp.GridControl = this.gridControlExp;
            this.gridViewExp.LevelIndent = 0;
            this.gridViewExp.Name = "gridViewExp";
            this.gridViewExp.OptionsBehavior.ReadOnly = true;
            this.gridViewExp.OptionsDetail.ShowDetailTabs = false;
            this.gridViewExp.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewExp.OptionsPrint.PrintDetails = true;
            this.gridViewExp.OptionsView.ShowAutoFilterRow = true;
            this.gridViewExp.OptionsView.ShowGroupPanel = false;
            this.gridViewExp.ShownEditor += new System.EventHandler(this.gridViewExp_ShownEditor);
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Баримт";
            this.gridBand2.Columns.Add(this.bandedGridColumn5);
            this.gridBand2.Columns.Add(this.gridColumn3);
            this.gridBand2.Columns.Add(this.gridColumnUserName);
            this.gridBand2.Columns.Add(this.bandedGridColumn37);
            this.gridBand2.CustomizationCaption = "Баримт";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 302;
            // 
            // bandedGridColumn5
            // 
            this.bandedGridColumn5.Caption = "Дугаар";
            this.bandedGridColumn5.CustomizationCaption = "Дугаар";
            this.bandedGridColumn5.FieldName = "id";
            this.bandedGridColumn5.MinWidth = 75;
            this.bandedGridColumn5.Name = "bandedGridColumn5";
            this.bandedGridColumn5.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn5.Visible = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Огноо";
            this.gridColumn3.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn3.CustomizationCaption = "Огноо";
            this.gridColumn3.FieldName = "date";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn3.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.gridColumn3.Visible = true;
            // 
            // gridColumnUserName
            // 
            this.gridColumnUserName.Caption = "Гүйлгээний төрөл";
            this.gridColumnUserName.CustomizationCaption = "Гүйлгээний төрөл";
            this.gridColumnUserName.FieldName = "expTypeText";
            this.gridColumnUserName.MinWidth = 75;
            this.gridColumnUserName.Name = "gridColumnUserName";
            this.gridColumnUserName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumnUserName.Visible = true;
            this.gridColumnUserName.Width = 77;
            // 
            // bandedGridColumn37
            // 
            this.bandedGridColumn37.Caption = "Зарлагын төрөл";
            this.bandedGridColumn37.CustomizationCaption = "Зарлагын төрөл";
            this.bandedGridColumn37.FieldName = "expTypeName";
            this.bandedGridColumn37.MinWidth = 75;
            this.bandedGridColumn37.Name = "bandedGridColumn37";
            this.bandedGridColumn37.Visible = true;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand11.AppearanceHeader.Options.UseFont = true;
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "Нярав";
            this.gridBand11.Columns.Add(this.bandedGridColumn21);
            this.gridBand11.Columns.Add(this.bandedGridColumn22);
            this.gridBand11.Columns.Add(this.bandedGridColumn18);
            this.gridBand11.CustomizationCaption = "Нярав";
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 1;
            this.gridBand11.Width = 150;
            // 
            // bandedGridColumn21
            // 
            this.bandedGridColumn21.Caption = "Дугаар";
            this.bandedGridColumn21.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.bandedGridColumn21.FieldName = "userID";
            this.bandedGridColumn21.MinWidth = 75;
            this.bandedGridColumn21.Name = "bandedGridColumn21";
            this.bandedGridColumn21.Visible = true;
            // 
            // bandedGridColumn22
            // 
            this.bandedGridColumn22.Caption = "Нэр";
            this.bandedGridColumn22.CustomizationCaption = "Хэрэглэгчийн нэр";
            this.bandedGridColumn22.FieldName = "userName";
            this.bandedGridColumn22.MinWidth = 75;
            this.bandedGridColumn22.Name = "bandedGridColumn22";
            this.bandedGridColumn22.Visible = true;
            // 
            // bandedGridColumn18
            // 
            this.bandedGridColumn18.Caption = "Эрх";
            this.bandedGridColumn18.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.bandedGridColumn18.FieldName = "roleName";
            this.bandedGridColumn18.MinWidth = 75;
            this.bandedGridColumn18.Name = "bandedGridColumn18";
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Агуулах";
            this.gridBand1.Columns.Add(this.gridColumnUserRole);
            this.gridBand1.Columns.Add(this.bandedGridColumn8);
            this.gridBand1.CustomizationCaption = "Агуулах";
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 2;
            this.gridBand1.Width = 150;
            // 
            // gridColumnUserRole
            // 
            this.gridColumnUserRole.Caption = "Нэр";
            this.gridColumnUserRole.CustomizationCaption = "Агуулахын нэр";
            this.gridColumnUserRole.FieldName = "warehouseName";
            this.gridColumnUserRole.MinWidth = 75;
            this.gridColumnUserRole.Name = "gridColumnUserRole";
            this.gridColumnUserRole.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumnUserRole.Visible = true;
            // 
            // bandedGridColumn8
            // 
            this.bandedGridColumn8.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn8.CustomizationCaption = "Агуулахын дэлгэрэнгүй";
            this.bandedGridColumn8.FieldName = "warehouseDesc";
            this.bandedGridColumn8.MinWidth = 75;
            this.bandedGridColumn8.Name = "bandedGridColumn8";
            this.bandedGridColumn8.Visible = true;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand9.AppearanceHeader.Options.UseFont = true;
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "Хүлээн авах нярав";
            this.gridBand9.Columns.Add(this.bandedGridColumn32);
            this.gridBand9.Columns.Add(this.bandedGridColumn24);
            this.gridBand9.Columns.Add(this.bandedGridColumn23);
            this.gridBand9.CustomizationCaption = "Хүлээн авах нярав";
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 3;
            this.gridBand9.Width = 150;
            // 
            // bandedGridColumn32
            // 
            this.bandedGridColumn32.Caption = "Дугаар";
            this.bandedGridColumn32.CustomizationCaption = "Хүлээн авах хэрэглэгчийн дугаар";
            this.bandedGridColumn32.FieldName = "expUserID";
            this.bandedGridColumn32.MinWidth = 75;
            this.bandedGridColumn32.Name = "bandedGridColumn32";
            this.bandedGridColumn32.Visible = true;
            // 
            // bandedGridColumn24
            // 
            this.bandedGridColumn24.Caption = "Нэр";
            this.bandedGridColumn24.CustomizationCaption = "Хүлээн авах хэрэглэгчийн нэр";
            this.bandedGridColumn24.FieldName = "expUserName";
            this.bandedGridColumn24.MinWidth = 75;
            this.bandedGridColumn24.Name = "bandedGridColumn24";
            this.bandedGridColumn24.Visible = true;
            // 
            // bandedGridColumn23
            // 
            this.bandedGridColumn23.Caption = "Эрх";
            this.bandedGridColumn23.CustomizationCaption = "Зарлагадах хэрэглэгчийн эрх";
            this.bandedGridColumn23.FieldName = "expRoleName";
            this.bandedGridColumn23.MinWidth = 75;
            this.bandedGridColumn23.Name = "bandedGridColumn23";
            this.bandedGridColumn23.Width = 66;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand13.AppearanceHeader.Options.UseFont = true;
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "Хүлээн авах агуулах";
            this.gridBand13.Columns.Add(this.bandedGridColumn25);
            this.gridBand13.Columns.Add(this.bandedGridColumn31);
            this.gridBand13.CustomizationCaption = "Хүлээн авах агуулах";
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 4;
            this.gridBand13.Width = 150;
            // 
            // bandedGridColumn25
            // 
            this.bandedGridColumn25.Caption = "Нэр";
            this.bandedGridColumn25.CustomizationCaption = "Хүлээн авах агуулахын нэр";
            this.bandedGridColumn25.FieldName = "expWarehouseName";
            this.bandedGridColumn25.MinWidth = 75;
            this.bandedGridColumn25.Name = "bandedGridColumn25";
            this.bandedGridColumn25.Visible = true;
            // 
            // bandedGridColumn31
            // 
            this.bandedGridColumn31.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn31.CustomizationCaption = "Хүлээн авах агуулахын дэлгэрэнгүй";
            this.bandedGridColumn31.FieldName = "expWarehouseDesc";
            this.bandedGridColumn31.MinWidth = 75;
            this.bandedGridColumn31.Name = "bandedGridColumn31";
            this.bandedGridColumn31.Visible = true;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Нэмэлт мэдээлэл";
            this.gridBand3.Columns.Add(this.bandedGridColumnDoctor);
            this.gridBand3.Columns.Add(this.bandedGridColumn2);
            this.gridBand3.Columns.Add(this.bandedGridColumn4);
            this.gridBand3.CustomizationCaption = "Нэмэлт мэдээлэл";
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 5;
            this.gridBand3.Width = 228;
            // 
            // bandedGridColumnDoctor
            // 
            this.bandedGridColumnDoctor.Caption = "Эмч";
            this.bandedGridColumnDoctor.FieldName = "doctorName";
            this.bandedGridColumnDoctor.MinWidth = 75;
            this.bandedGridColumnDoctor.Name = "bandedGridColumnDoctor";
            this.bandedGridColumnDoctor.Visible = true;
            // 
            // bandedGridColumn2
            // 
            this.bandedGridColumn2.Caption = "Дагалдах баримт";
            this.bandedGridColumn2.CustomizationCaption = "Дагалдах баримт";
            this.bandedGridColumn2.FieldName = "docNumber";
            this.bandedGridColumn2.MinWidth = 75;
            this.bandedGridColumn2.Name = "bandedGridColumn2";
            this.bandedGridColumn2.Visible = true;
            // 
            // bandedGridColumn4
            // 
            this.bandedGridColumn4.Caption = "Тайлбар";
            this.bandedGridColumn4.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn4.FieldName = "description";
            this.bandedGridColumn4.MinWidth = 75;
            this.bandedGridColumn4.Name = "bandedGridColumn4";
            this.bandedGridColumn4.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn4.Visible = true;
            this.bandedGridColumn4.Width = 78;
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand15.AppearanceHeader.Options.UseFont = true;
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "Нийт дүн";
            this.gridBand15.Columns.Add(this.bandedGridColumn41);
            this.gridBand15.CustomizationCaption = "Нийт дүн";
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 6;
            this.gridBand15.Width = 75;
            // 
            // bandedGridColumn41
            // 
            this.bandedGridColumn41.Caption = "Нийт дүн";
            this.bandedGridColumn41.CustomizationCaption = "Нийт дүн";
            this.bandedGridColumn41.FieldName = "totalAmount";
            this.bandedGridColumn41.Name = "bandedGridColumn41";
            this.bandedGridColumn41.Visible = true;
            // 
            // gridViewDetail
            // 
            this.gridViewDetail.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4,
            this.gridBand12,
            this.gridBand5,
            this.gridBand10,
            this.gridBand14,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8});
            this.gridViewDetail.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumn14,
            this.bandedGridColumn6,
            this.bandedGridColumn7,
            this.bandedGridColumn30,
            this.bandedGridColumn20,
            this.bandedGridColumn9,
            this.bandedGridColumn10,
            this.bandedGridColumn11,
            this.bandedGridColumn40,
            this.bandedGridColumn13,
            this.bandedGridColumn29,
            this.bandedGridColumn3,
            this.bandedGridColumn15,
            this.bandedGridColumn16,
            this.bandedGridColumn1,
            this.bandedGridColumn17,
            this.bandedGridColumn12,
            this.bandedGridColumn36,
            this.bandedGridColumnCount,
            this.bandedGridColumn19,
            this.bandedGridColumnSumPrice,
            this.bandedGridColumn28,
            this.bandedGridColumn33,
            this.bandedGridColumn27,
            this.bandedGridColumn35,
            this.bandedGridColumn34,
            this.bandedGridColumn26,
            this.bandedGridColumn39,
            this.bandedGridColumn38});
            this.gridViewDetail.GridControl = this.gridControlExp;
            this.gridViewDetail.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", this.bandedGridColumnCount, "{0:n2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", this.bandedGridColumnSumPrice, "{0:c2}")});
            this.gridViewDetail.LevelIndent = 0;
            this.gridViewDetail.Name = "gridViewDetail";
            this.gridViewDetail.OptionsBehavior.Editable = false;
            this.gridViewDetail.OptionsBehavior.ReadOnly = true;
            this.gridViewDetail.OptionsDetail.ShowDetailTabs = false;
            this.gridViewDetail.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewDetail.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDetail.OptionsView.ShowFooter = true;
            this.gridViewDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewDetail.CustomDrawGroupRow += new DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventHandler(this.gridViewDetail_CustomDrawGroupRow);
            this.gridViewDetail.DoubleClick += new System.EventHandler(this.gridViewDetail_DoubleClick);
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand4.AppearanceHeader.Options.UseFont = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Баримт";
            this.gridBand4.Columns.Add(this.bandedGridColumn14);
            this.gridBand4.Columns.Add(this.bandedGridColumn6);
            this.gridBand4.Columns.Add(this.bandedGridColumn28);
            this.gridBand4.Columns.Add(this.bandedGridColumn39);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 334;
            // 
            // bandedGridColumn14
            // 
            this.bandedGridColumn14.Caption = "Дугаар";
            this.bandedGridColumn14.CustomizationCaption = "Дугаар";
            this.bandedGridColumn14.FieldName = "expJournalID";
            this.bandedGridColumn14.MinWidth = 75;
            this.bandedGridColumn14.Name = "bandedGridColumn14";
            this.bandedGridColumn14.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn14.Visible = true;
            this.bandedGridColumn14.Width = 90;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "Огноо";
            this.bandedGridColumn6.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumn6.CustomizationCaption = "Огноо";
            this.bandedGridColumn6.FieldName = "date";
            this.bandedGridColumn6.MinWidth = 75;
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn6.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            this.bandedGridColumn6.Visible = true;
            this.bandedGridColumn6.Width = 94;
            // 
            // bandedGridColumn28
            // 
            this.bandedGridColumn28.Caption = "Гүйлгээний төрөл";
            this.bandedGridColumn28.CustomizationCaption = "Зарлагын төрөл";
            this.bandedGridColumn28.FieldName = "expTypeText";
            this.bandedGridColumn28.MinWidth = 75;
            this.bandedGridColumn28.Name = "bandedGridColumn28";
            this.bandedGridColumn28.Visible = true;
            // 
            // bandedGridColumn39
            // 
            this.bandedGridColumn39.Caption = "Зарлагын төрөл";
            this.bandedGridColumn39.CustomizationCaption = "Зарлагын төрөл";
            this.bandedGridColumn39.FieldName = "expTypeName";
            this.bandedGridColumn39.MinWidth = 75;
            this.bandedGridColumn39.Name = "bandedGridColumn39";
            this.bandedGridColumn39.Visible = true;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand12.AppearanceHeader.Options.UseFont = true;
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "Нярав";
            this.gridBand12.Columns.Add(this.bandedGridColumn7);
            this.gridBand12.Columns.Add(this.bandedGridColumn30);
            this.gridBand12.Columns.Add(this.bandedGridColumn20);
            this.gridBand12.CustomizationCaption = "Нярав";
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 1;
            this.gridBand12.Width = 166;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "Дугаар";
            this.bandedGridColumn7.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.bandedGridColumn7.FieldName = "userID";
            this.bandedGridColumn7.MinWidth = 75;
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn7.Visible = true;
            this.bandedGridColumn7.Width = 91;
            // 
            // bandedGridColumn30
            // 
            this.bandedGridColumn30.Caption = "Нэр";
            this.bandedGridColumn30.CustomizationCaption = "Хэрэглэгчийн нэр";
            this.bandedGridColumn30.FieldName = "userName";
            this.bandedGridColumn30.MinWidth = 75;
            this.bandedGridColumn30.Name = "bandedGridColumn30";
            this.bandedGridColumn30.Visible = true;
            // 
            // bandedGridColumn20
            // 
            this.bandedGridColumn20.Caption = "Эрх";
            this.bandedGridColumn20.CustomizationCaption = "Хэрэглэгчийн эрх";
            this.bandedGridColumn20.FieldName = "roleName";
            this.bandedGridColumn20.MinWidth = 75;
            this.bandedGridColumn20.Name = "bandedGridColumn20";
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand5.AppearanceHeader.Options.UseFont = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Агуулах";
            this.gridBand5.Columns.Add(this.bandedGridColumn9);
            this.gridBand5.Columns.Add(this.bandedGridColumn10);
            this.gridBand5.CustomizationCaption = "Агуулах";
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 2;
            this.gridBand5.Width = 187;
            // 
            // bandedGridColumn9
            // 
            this.bandedGridColumn9.Caption = "Нэр";
            this.bandedGridColumn9.CustomizationCaption = "Агуулахын нэр";
            this.bandedGridColumn9.FieldName = "warehouseName";
            this.bandedGridColumn9.MinWidth = 75;
            this.bandedGridColumn9.Name = "bandedGridColumn9";
            this.bandedGridColumn9.Visible = true;
            this.bandedGridColumn9.Width = 91;
            // 
            // bandedGridColumn10
            // 
            this.bandedGridColumn10.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn10.CustomizationCaption = "Агуулахын дэлгэрэнгүй";
            this.bandedGridColumn10.FieldName = "warehouseDesc";
            this.bandedGridColumn10.MinWidth = 75;
            this.bandedGridColumn10.Name = "bandedGridColumn10";
            this.bandedGridColumn10.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn10.Visible = true;
            this.bandedGridColumn10.Width = 96;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand10.AppearanceHeader.Options.UseFont = true;
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "Хүлээн авах нярав";
            this.gridBand10.Columns.Add(this.bandedGridColumn33);
            this.gridBand10.Columns.Add(this.bandedGridColumn34);
            this.gridBand10.Columns.Add(this.bandedGridColumn35);
            this.gridBand10.CustomizationCaption = "Хүлээн авах нярав";
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 3;
            this.gridBand10.Width = 150;
            // 
            // bandedGridColumn33
            // 
            this.bandedGridColumn33.Caption = "Дугаар";
            this.bandedGridColumn33.CustomizationCaption = "Хүлээн авах хэрэглэгчийн дугаар";
            this.bandedGridColumn33.FieldName = "expUserID";
            this.bandedGridColumn33.MinWidth = 75;
            this.bandedGridColumn33.Name = "bandedGridColumn33";
            this.bandedGridColumn33.Visible = true;
            // 
            // bandedGridColumn34
            // 
            this.bandedGridColumn34.Caption = "Нэр";
            this.bandedGridColumn34.CustomizationCaption = "Хүлээн авах агуулахын нэр";
            this.bandedGridColumn34.FieldName = "expWarehouseName";
            this.bandedGridColumn34.MinWidth = 75;
            this.bandedGridColumn34.Name = "bandedGridColumn34";
            this.bandedGridColumn34.Visible = true;
            // 
            // bandedGridColumn35
            // 
            this.bandedGridColumn35.Caption = "Эрх";
            this.bandedGridColumn35.CustomizationCaption = "Хүлээн авах хэрэглэгчийн эрх";
            this.bandedGridColumn35.FieldName = "expRoleName";
            this.bandedGridColumn35.MinWidth = 75;
            this.bandedGridColumn35.Name = "bandedGridColumn35";
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand14.AppearanceHeader.Options.UseFont = true;
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "Хүлээн авах агуулах";
            this.gridBand14.Columns.Add(this.bandedGridColumn27);
            this.gridBand14.Columns.Add(this.bandedGridColumn26);
            this.gridBand14.CustomizationCaption = "Хүлээн авах агуулах";
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 4;
            this.gridBand14.Width = 150;
            // 
            // bandedGridColumn27
            // 
            this.bandedGridColumn27.Caption = "Нэр";
            this.bandedGridColumn27.CustomizationCaption = "Хүлээн авах хэрэглэгчийн эрх";
            this.bandedGridColumn27.FieldName = "expUserName";
            this.bandedGridColumn27.MinWidth = 75;
            this.bandedGridColumn27.Name = "bandedGridColumn27";
            this.bandedGridColumn27.Visible = true;
            // 
            // bandedGridColumn26
            // 
            this.bandedGridColumn26.Caption = "Дэлгэрэнгүй";
            this.bandedGridColumn26.CustomizationCaption = "Хүлээн авах агуулахын дэлгэрэнгүй";
            this.bandedGridColumn26.FieldName = "expWarehouseDesc";
            this.bandedGridColumn26.MinWidth = 75;
            this.bandedGridColumn26.Name = "bandedGridColumn26";
            this.bandedGridColumn26.Visible = true;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand6.AppearanceHeader.Options.UseFont = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Нэмэлт мэдээлэл";
            this.gridBand6.Columns.Add(this.bandedGridColumn40);
            this.gridBand6.Columns.Add(this.bandedGridColumn13);
            this.gridBand6.Columns.Add(this.bandedGridColumn11);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 5;
            this.gridBand6.Width = 269;
            // 
            // bandedGridColumn40
            // 
            this.bandedGridColumn40.Caption = "Эмч";
            this.bandedGridColumn40.CustomizationCaption = "Эмч";
            this.bandedGridColumn40.FieldName = "doctorName";
            this.bandedGridColumn40.MinWidth = 75;
            this.bandedGridColumn40.Name = "bandedGridColumn40";
            this.bandedGridColumn40.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn40.Visible = true;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.Caption = "Дагалдах баримт";
            this.bandedGridColumn13.CustomizationCaption = "Дагалдах баримт";
            this.bandedGridColumn13.FieldName = "docNumber";
            this.bandedGridColumn13.MinWidth = 75;
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.Visible = true;
            // 
            // bandedGridColumn11
            // 
            this.bandedGridColumn11.Caption = "Тайлбар";
            this.bandedGridColumn11.CustomizationCaption = "Тайлбар";
            this.bandedGridColumn11.FieldName = "description";
            this.bandedGridColumn11.MinWidth = 75;
            this.bandedGridColumn11.Name = "bandedGridColumn11";
            this.bandedGridColumn11.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn11.Visible = true;
            this.bandedGridColumn11.Width = 119;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand7.AppearanceHeader.Options.UseFont = true;
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "Эмийн мэдээлэл";
            this.gridBand7.Columns.Add(this.bandedGridColumn3);
            this.gridBand7.Columns.Add(this.bandedGridColumn15);
            this.gridBand7.Columns.Add(this.bandedGridColumn16);
            this.gridBand7.Columns.Add(this.bandedGridColumn29);
            this.gridBand7.Columns.Add(this.bandedGridColumn38);
            this.gridBand7.Columns.Add(this.bandedGridColumn12);
            this.gridBand7.Columns.Add(this.bandedGridColumn1);
            this.gridBand7.Columns.Add(this.bandedGridColumn17);
            this.gridBand7.Columns.Add(this.bandedGridColumn36);
            this.gridBand7.CustomizationCaption = "Эмийн мэдээлэл";
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 6;
            this.gridBand7.Width = 675;
            // 
            // bandedGridColumn3
            // 
            this.bandedGridColumn3.Caption = "Эмийн нэр";
            this.bandedGridColumn3.CustomizationCaption = "Худалдааны нэр";
            this.bandedGridColumn3.FieldName = "medicineName";
            this.bandedGridColumn3.MinWidth = 75;
            this.bandedGridColumn3.Name = "bandedGridColumn3";
            this.bandedGridColumn3.Visible = true;
            // 
            // bandedGridColumn15
            // 
            this.bandedGridColumn15.Caption = "ОУ нэр";
            this.bandedGridColumn15.CustomizationCaption = "Олон улсын нэр";
            this.bandedGridColumn15.FieldName = "latinName";
            this.bandedGridColumn15.MinWidth = 75;
            this.bandedGridColumn15.Name = "bandedGridColumn15";
            this.bandedGridColumn15.Visible = true;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.Caption = "Ангилал";
            this.bandedGridColumn16.CustomizationCaption = "Ангилал";
            this.bandedGridColumn16.FieldName = "medicineTypeName";
            this.bandedGridColumn16.MinWidth = 75;
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.Visible = true;
            // 
            // bandedGridColumn29
            // 
            this.bandedGridColumn29.Caption = "Орлогын төрөл";
            this.bandedGridColumn29.CustomizationCaption = "Орлогын төрөл";
            this.bandedGridColumn29.FieldName = "incTypeName";
            this.bandedGridColumn29.MinWidth = 75;
            this.bandedGridColumn29.Name = "bandedGridColumn29";
            this.bandedGridColumn29.Visible = true;
            // 
            // bandedGridColumn38
            // 
            this.bandedGridColumn38.Caption = "Сериал";
            this.bandedGridColumn38.CustomizationCaption = "Эмийн сериал дугаар";
            this.bandedGridColumn38.FieldName = "serial";
            this.bandedGridColumn38.MinWidth = 75;
            this.bandedGridColumn38.Name = "bandedGridColumn38";
            this.bandedGridColumn38.Visible = true;
            // 
            // bandedGridColumn12
            // 
            this.bandedGridColumn12.Caption = "Хүчинтэй хугацаа";
            this.bandedGridColumn12.ColumnEdit = this.repositoryItemDateEdit1;
            this.bandedGridColumn12.CustomizationCaption = "Хүчинтэй хугацаа";
            this.bandedGridColumn12.FieldName = "validDate";
            this.bandedGridColumn12.MinWidth = 75;
            this.bandedGridColumn12.Name = "bandedGridColumn12";
            this.bandedGridColumn12.Visible = true;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Caption = "Хэлбэр";
            this.bandedGridColumn1.CustomizationCaption = "Хэлбэр";
            this.bandedGridColumn1.FieldName = "shape";
            this.bandedGridColumn1.MinWidth = 75;
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            this.bandedGridColumn1.Visible = true;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.Caption = "Тун хэмжээ";
            this.bandedGridColumn17.CustomizationCaption = "Тун хэмжээ";
            this.bandedGridColumn17.FieldName = "unitType";
            this.bandedGridColumn17.MinWidth = 75;
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.Visible = true;
            // 
            // bandedGridColumn36
            // 
            this.bandedGridColumn36.Caption = "Хэмжих нэгж";
            this.bandedGridColumn36.CustomizationCaption = "Хэмжих нэгж";
            this.bandedGridColumn36.FieldName = "quantity";
            this.bandedGridColumn36.MinWidth = 75;
            this.bandedGridColumn36.Name = "bandedGridColumn36";
            this.bandedGridColumn36.Visible = true;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand8.AppearanceHeader.Options.UseFont = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "Дүн";
            this.gridBand8.Columns.Add(this.bandedGridColumnCount);
            this.gridBand8.Columns.Add(this.bandedGridColumn19);
            this.gridBand8.Columns.Add(this.bandedGridColumnSumPrice);
            this.gridBand8.CustomizationCaption = "Дүн";
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 7;
            this.gridBand8.Width = 225;
            // 
            // bandedGridColumnCount
            // 
            this.bandedGridColumnCount.Caption = "Тоо";
            this.bandedGridColumnCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnCount.CustomizationCaption = "Тоо";
            this.bandedGridColumnCount.FieldName = "count";
            this.bandedGridColumnCount.MinWidth = 75;
            this.bandedGridColumnCount.Name = "bandedGridColumnCount";
            this.bandedGridColumnCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.bandedGridColumnCount.Visible = true;
            // 
            // bandedGridColumn19
            // 
            this.bandedGridColumn19.Caption = "Нэгжийн үнэ";
            this.bandedGridColumn19.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumn19.CustomizationCaption = "Нэгжийн үнэ";
            this.bandedGridColumn19.FieldName = "price";
            this.bandedGridColumn19.MinWidth = 75;
            this.bandedGridColumn19.Name = "bandedGridColumn19";
            this.bandedGridColumn19.Visible = true;
            // 
            // bandedGridColumnSumPrice
            // 
            this.bandedGridColumnSumPrice.Caption = "Дүн";
            this.bandedGridColumnSumPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnSumPrice.CustomizationCaption = "Дүн";
            this.bandedGridColumnSumPrice.FieldName = "sumPrice";
            this.bandedGridColumnSumPrice.MinWidth = 75;
            this.bandedGridColumnSumPrice.Name = "bandedGridColumnSumPrice";
            this.bandedGridColumnSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.bandedGridColumnSumPrice.Visible = true;
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLeft});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanelLeft
            // 
            this.dockPanelLeft.Controls.Add(this.dockPanel1_Container);
            this.dockPanelLeft.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelLeft.ID = new System.Guid("32690437-4757-43e6-b1e2-053573c2cee2");
            this.dockPanelLeft.Location = new System.Drawing.Point(0, 0);
            this.dockPanelLeft.Name = "dockPanelLeft";
            this.dockPanelLeft.Options.ShowCloseButton = false;
            this.dockPanelLeft.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelLeft.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelLeft.SavedIndex = 0;
            this.dockPanelLeft.Size = new System.Drawing.Size(200, 461);
            this.dockPanelLeft.Text = "Тасаг";
            this.dockPanelLeft.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.treeListWard);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(192, 434);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // treeListWard
            // 
            this.treeListWard.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnWard});
            this.treeListWard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListWard.Location = new System.Drawing.Point(0, 0);
            this.treeListWard.Name = "treeListWard";
            this.treeListWard.BeginUnboundLoad();
            this.treeListWard.AppendNode(new object[] {
            "Хавдар судлалын үндэсний төв"}, -1);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 1"}, 0);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 2"}, 0);
            this.treeListWard.AppendNode(new object[] {
            "Тасаг 3"}, 0);
            this.treeListWard.EndUnboundLoad();
            this.treeListWard.OptionsBehavior.Editable = false;
            this.treeListWard.OptionsBehavior.ExpandNodeOnDrag = false;
            this.treeListWard.OptionsView.ShowColumns = false;
            this.treeListWard.OptionsView.ShowIndicator = false;
            this.treeListWard.Size = new System.Drawing.Size(192, 434);
            this.treeListWard.TabIndex = 0;
            // 
            // treeListColumnWard
            // 
            this.treeListColumnWard.Caption = "Тасаг";
            this.treeListColumnWard.FieldName = "ward";
            this.treeListColumnWard.MinWidth = 52;
            this.treeListColumnWard.Name = "treeListColumnWard";
            this.treeListColumnWard.OptionsColumn.ReadOnly = true;
            this.treeListColumnWard.Visible = true;
            this.treeListColumnWard.VisibleIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.xtraTabControl);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 461);
            this.panelControl1.TabIndex = 0;
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl.Location = new System.Drawing.Point(2, 52);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageExp;
            this.xtraTabControl.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.xtraTabControl.Size = new System.Drawing.Size(780, 407);
            this.xtraTabControl.TabIndex = 2;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageExp,
            this.xtraTabPageDetail});
            // 
            // xtraTabPageExp
            // 
            this.xtraTabPageExp.Controls.Add(this.gridControlExp);
            this.xtraTabPageExp.Name = "xtraTabPageExp";
            this.xtraTabPageExp.Size = new System.Drawing.Size(774, 401);
            this.xtraTabPageExp.Text = "xtraTabPage1";
            // 
            // xtraTabPageDetail
            // 
            this.xtraTabPageDetail.Controls.Add(this.pivotGridControl);
            this.xtraTabPageDetail.Name = "xtraTabPageDetail";
            this.xtraTabPageDetail.Size = new System.Drawing.Size(774, 401);
            this.xtraTabPageDetail.Text = "xtraTabPage2";
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridFieldMonth,
            this.pivotGridFieldDate,
            this.pivotGridFieldExpTypeName,
            this.pivotGridFieldMedicine,
            this.pivotGridFieldPrice,
            this.pivotGridFieldCount,
            this.pivotGridFieldSumPrice,
            this.pivotGridFieldIncTypeName});
            this.pivotGridControl.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsFilterPopup.IsRadioMode = true;
            this.pivotGridControl.OptionsSelection.CellSelection = false;
            this.pivotGridControl.OptionsSelection.MultiSelect = false;
            this.pivotGridControl.Size = new System.Drawing.Size(774, 401);
            this.pivotGridControl.TabIndex = 3;
            this.pivotGridControl.CustomDrawFieldValue += new DevExpress.XtraPivotGrid.PivotCustomDrawFieldValueEventHandler(this.pivotGridControl_CustomDrawFieldValue);
            // 
            // pivotGridFieldMonth
            // 
            this.pivotGridFieldMonth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldMonth.AreaIndex = 0;
            this.pivotGridFieldMonth.Caption = "Сар";
            this.pivotGridFieldMonth.FieldName = "date";
            this.pivotGridFieldMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.pivotGridFieldMonth.Name = "pivotGridFieldMonth";
            this.pivotGridFieldMonth.UnboundFieldName = "pivotGridFieldMonth";
            // 
            // pivotGridFieldDate
            // 
            this.pivotGridFieldDate.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldDate.AreaIndex = 1;
            this.pivotGridFieldDate.Caption = "Өдөр";
            this.pivotGridFieldDate.FieldName = "date";
            this.pivotGridFieldDate.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateDay;
            this.pivotGridFieldDate.Name = "pivotGridFieldDate";
            this.pivotGridFieldDate.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            this.pivotGridFieldDate.UnboundFieldName = "pivotGridFieldDate";
            this.pivotGridFieldDate.Width = 30;
            // 
            // pivotGridFieldExpTypeName
            // 
            this.pivotGridFieldExpTypeName.Appearance.Value.Options.UseTextOptions = true;
            this.pivotGridFieldExpTypeName.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotGridFieldExpTypeName.Appearance.Value.TextOptions.HotkeyPrefix = DevExpress.Utils.HKeyPrefix.Show;
            this.pivotGridFieldExpTypeName.Appearance.Value.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisWord;
            this.pivotGridFieldExpTypeName.Appearance.Value.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.pivotGridFieldExpTypeName.Appearance.Value.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.pivotGridFieldExpTypeName.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldExpTypeName.AreaIndex = 2;
            this.pivotGridFieldExpTypeName.Caption = "Хариуцагч";
            this.pivotGridFieldExpTypeName.FieldName = "expName";
            this.pivotGridFieldExpTypeName.MinWidth = 0;
            this.pivotGridFieldExpTypeName.Name = "pivotGridFieldExpTypeName";
            this.pivotGridFieldExpTypeName.Width = 50;
            // 
            // pivotGridFieldMedicine
            // 
            this.pivotGridFieldMedicine.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldMedicine.AreaIndex = 0;
            this.pivotGridFieldMedicine.Caption = "Эм";
            this.pivotGridFieldMedicine.FieldName = "medicineName";
            this.pivotGridFieldMedicine.Name = "pivotGridFieldMedicine";
            this.pivotGridFieldMedicine.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            this.pivotGridFieldMedicine.Width = 180;
            // 
            // pivotGridFieldPrice
            // 
            this.pivotGridFieldPrice.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldPrice.AreaIndex = 2;
            this.pivotGridFieldPrice.Caption = "Үнэ";
            this.pivotGridFieldPrice.FieldName = "price";
            this.pivotGridFieldPrice.Name = "pivotGridFieldPrice";
            this.pivotGridFieldPrice.UnboundFieldName = "pivotGridFieldPrice";
            this.pivotGridFieldPrice.ValueFormat.FormatString = "n";
            this.pivotGridFieldPrice.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldPrice.Width = 70;
            // 
            // pivotGridFieldCount
            // 
            this.pivotGridFieldCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldCount.AreaIndex = 0;
            this.pivotGridFieldCount.Caption = "Тоо";
            this.pivotGridFieldCount.CellFormat.FormatString = "n";
            this.pivotGridFieldCount.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.FieldName = "count";
            this.pivotGridFieldCount.GrandTotalCellFormat.FormatString = "n2";
            this.pivotGridFieldCount.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Name = "pivotGridFieldCount";
            this.pivotGridFieldCount.Options.ReadOnly = true;
            this.pivotGridFieldCount.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Value;
            this.pivotGridFieldCount.TotalValueFormat.FormatString = "n2";
            this.pivotGridFieldCount.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.ValueFormat.FormatString = "n";
            this.pivotGridFieldCount.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldCount.Width = 50;
            // 
            // pivotGridFieldSumPrice
            // 
            this.pivotGridFieldSumPrice.AreaIndex = 0;
            this.pivotGridFieldSumPrice.Caption = "Нийт үнэ";
            this.pivotGridFieldSumPrice.CellFormat.FormatString = "n0";
            this.pivotGridFieldSumPrice.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.FieldName = "sumPrice";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.Name = "pivotGridFieldSumPrice";
            this.pivotGridFieldSumPrice.Options.ReadOnly = true;
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.ValueFormat.FormatString = "n";
            this.pivotGridFieldSumPrice.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridFieldIncTypeName
            // 
            this.pivotGridFieldIncTypeName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldIncTypeName.AreaIndex = 1;
            this.pivotGridFieldIncTypeName.Caption = "Төрөл";
            this.pivotGridFieldIncTypeName.FieldName = "incTypeName";
            this.pivotGridFieldIncTypeName.Name = "pivotGridFieldIncTypeName";
            this.pivotGridFieldIncTypeName.Width = 80;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButtonDownload);
            this.panelControl2.Controls.Add(this.simpleButtonLayout);
            this.panelControl2.Controls.Add(this.dropDownButtonPrint);
            this.panelControl2.Controls.Add(this.radioGroupJournal);
            this.panelControl2.Controls.Add(this.dateEditEnd);
            this.panelControl2.Controls.Add(this.dateEditStart);
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(780, 50);
            this.panelControl2.TabIndex = 1;
            // 
            // simpleButtonDownload
            // 
            this.simpleButtonDownload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonDownload.Image")));
            this.simpleButtonDownload.Location = new System.Drawing.Point(293, 10);
            this.simpleButtonDownload.Name = "simpleButtonDownload";
            this.simpleButtonDownload.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonDownload.TabIndex = 8;
            this.simpleButtonDownload.Text = "Татах";
            this.simpleButtonDownload.ToolTip = "Захиалгын хуудас татах";
            this.simpleButtonDownload.Click += new System.EventHandler(this.simpleButtonDownload_Click);
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(747, 10);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 7;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.DropDownControl = this.popupMenu;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(641, 10);
            this.dropDownButtonPrint.MenuManager = this.barManager;
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.barManager.SetPopupContextMenu(this.dropDownButtonPrint, this.popupMenu);
            this.dropDownButtonPrint.Size = new System.Drawing.Size(100, 23);
            this.dropDownButtonPrint.TabIndex = 6;
            this.dropDownButtonPrint.Text = "Хэвлэх";
            this.dropDownButtonPrint.Click += new System.EventHandler(this.dropDownButtonPrint_Click);
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemExp),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemCustomize),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "Зарлагын баримт";
            this.barButtonItemPrint.Description = "Зарлагын баримт";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 0;
            this.barButtonItemPrint.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.LargeGlyph")));
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Хэвлэх /Бүгд/";
            this.barButtonItem3.Glyph = global::Pharmacy2016.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
            this.barButtonItem3.Id = 8;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barButtonItemExp
            // 
            this.barButtonItemExp.Caption = "Хэвлэх /Сонгосон/";
            this.barButtonItemExp.Description = "Зарлагын гүйлгээ хэвлэх";
            this.barButtonItemExp.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemExp.Glyph")));
            this.barButtonItemExp.Id = 1;
            this.barButtonItemExp.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemExp.LargeGlyph")));
            this.barButtonItemExp.Name = "barButtonItemExp";
            this.barButtonItemExp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemBegBal_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Хэвлэх /Эмчээр/";
            this.barButtonItem4.Glyph = global::Pharmacy2016.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
            this.barButtonItem4.Id = 9;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Нэхэмжлэх хэвлэх";
            this.barButtonItem2.Glyph = global::Pharmacy2016.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirect;
            this.barButtonItem2.Id = 7;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItemCustomize
            // 
            this.barButtonItemCustomize.Caption = "Тохиргоо";
            this.barButtonItemCustomize.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCustomize.Glyph")));
            this.barButtonItemCustomize.Id = 2;
            this.barButtonItemCustomize.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCustomize.LargeGlyph")));
            this.barButtonItemCustomize.Name = "barButtonItemCustomize";
            this.barButtonItemCustomize.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCustomize_ItemClick);
            // 
            // barButtonItemConvert
            // 
            this.barButtonItemConvert.Caption = "Хөрвүүлэх";
            this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
            this.barButtonItemConvert.Id = 3;
            this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
            this.barButtonItemConvert.Name = "barButtonItemConvert";
            this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemPrint,
            this.barButtonItemExp,
            this.barButtonItemCustomize,
            this.barButtonItemConvert,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4});
            this.barManager.MaxItemId = 10;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 461);
            this.barDockControlBottom.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 461);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(784, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 461);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Нэхэмжлэх";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 6;
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // radioGroupJournal
            // 
            this.radioGroupJournal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupJournal.Location = new System.Drawing.Point(385, 10);
            this.radioGroupJournal.Name = "radioGroupJournal";
            this.radioGroupJournal.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Баримт", true, "Journal"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Гүйлгээ", true, "Detail"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Дэлгэрэнгүй", true, "expTypeDetail")});
            this.radioGroupJournal.Size = new System.Drawing.Size(250, 23);
            this.radioGroupJournal.TabIndex = 5;
            this.radioGroupJournal.SelectedIndexChanged += new System.EventHandler(this.radioGroupJournal_SelectedIndexChanged);
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(116, 13);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(100, 20);
            this.dateEditEnd.TabIndex = 3;
            this.dateEditEnd.ToolTip = "Дуусах огноо";
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(10, 13);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(100, 20);
            this.dateEditStart.TabIndex = 2;
            this.dateEditStart.ToolTip = "Эхлэх огноо";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(222, 10);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonReload.TabIndex = 4;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Зарлагын журналыг шүүх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // MedicineExpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MedicineExpForm";
            this.ShowInTaskbar = false;
            this.Text = "Зарлагын журнал";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MedicineExpForm_FormClosing);
            this.Load += new System.EventHandler(this.MedicineExpForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExpDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            this.dockPanelLeft.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageExp.ResumeLayout(false);
            this.xtraTabPageDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupJournal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLeft;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraTreeList.TreeList treeListWard;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnWard;
        private DevExpress.XtraGrid.GridControl gridControlExp;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewExpDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewExp;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnUserRole;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnUserName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn4;
        private DevExpress.XtraEditors.RadioGroup radioGroupJournal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewDetail;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn9;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn19;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnSumPrice;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn22;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn23;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn24;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn28;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn26;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExp;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCustomize;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn29;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageExp;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageDetail;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMonth;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDate;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldExpTypeName;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMedicine;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPrice;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldCount;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSumPrice;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldIncTypeName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn18;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn30;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn32;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn31;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn33;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn34;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn35;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn36;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn37;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn39;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDownload;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn38;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnDoctor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDesc;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn40;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn41;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        
    }
}