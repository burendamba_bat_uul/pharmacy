﻿namespace Pharmacy2016.gui.core.journal.exp
{
    partial class MedicineExpUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicineExpUpForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.labelControlWarehouse = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditExpDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditMedicine = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnValidDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spinEditCount = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditPrice = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditSumPrice = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEditDocNumber = new DevExpress.XtraEditors.TextEdit();
            this.gridLookUpEditExpUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlExpWarehouse = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditBal = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.treeListLookUpEditWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListLookUpEditWarehouse = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupType = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.treeListLookUpEditExpWarehouse = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList2 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn4 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn5 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListLookUpEditExpWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList3 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn6 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonNewDoc = new DevExpress.XtraEditors.SimpleButton();
            this.treeListLookUpEditExpType = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList4 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn7 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn9 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn10 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControlQuantity = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditExpType = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditDoctor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditMultiSelection = new DevExpress.XtraEditors.CheckEdit();
            this.gridControlMed = new DevExpress.XtraGrid.GridControl();
            this.gridViewMed = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLatinName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSerial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnValidatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnBal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnDupCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerEditMedicineBal = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlMed = new DevExpress.XtraEditors.PopupContainerControl();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSumPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDocNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditExpUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditExpWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditExpWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditExpType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditExpType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMultiSelection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditMedicineBal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMed)).BeginInit();
            this.popupContainerControlMed.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControlWarehouse
            // 
            this.labelControlWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlWarehouse.Location = new System.Drawing.Point(100, 147);
            this.labelControlWarehouse.Name = "labelControlWarehouse";
            this.labelControlWarehouse.Size = new System.Drawing.Size(61, 13);
            this.labelControlWarehouse.TabIndex = 0;
            this.labelControlWarehouse.Text = "Эмийн сан*:";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(120, 67);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(41, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Огноо*:";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(95, 401);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(66, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Тоо хэмжээ*:";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(89, 453);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(72, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Нэгжийн үнэ*:";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl6.Location = new System.Drawing.Point(132, 479);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(29, 13);
            this.labelControl6.TabIndex = 8;
            this.labelControl6.Text = "Дүн*:";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(316, 526);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 21;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(397, 526);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 22;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // dateEditExpDate
            // 
            this.dateEditExpDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditExpDate.EditValue = null;
            this.dateEditExpDate.Location = new System.Drawing.Point(167, 64);
            this.dateEditExpDate.Name = "dateEditExpDate";
            this.dateEditExpDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditExpDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditExpDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditExpDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditExpDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditExpDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditExpDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditExpDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditExpDate.TabIndex = 2;
            this.dateEditExpDate.EditValueChanged += new System.EventHandler(this.dateEditExpDate_EditValueChanged);
            this.dateEditExpDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditExpDate_KeyUp);
            // 
            // textEditID
            // 
            this.textEditID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditID.Location = new System.Drawing.Point(167, 38);
            this.textEditID.Name = "textEditID";
            this.textEditID.Properties.ReadOnly = true;
            this.textEditID.Size = new System.Drawing.Size(200, 20);
            this.textEditID.TabIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl7.Location = new System.Drawing.Point(62, 41);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(99, 13);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "Баримтын дугаар*:";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.memoEditDescription.EnterMoveNextControl = true;
            this.memoEditDescription.Location = new System.Drawing.Point(167, 326);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(305, 40);
            this.memoEditDescription.TabIndex = 12;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(112, 328);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(49, 13);
            this.labelControl8.TabIndex = 18;
            this.labelControl8.Text = "Тайлбар :";
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl9.Location = new System.Drawing.Point(100, 375);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(61, 13);
            this.labelControl9.TabIndex = 21;
            this.labelControl9.Text = "Эмийн нэр*:";
            // 
            // gridLookUpEditMedicine
            // 
            this.gridLookUpEditMedicine.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditMedicine.EditValue = "";
            this.gridLookUpEditMedicine.Location = new System.Drawing.Point(167, 372);
            this.gridLookUpEditMedicine.Name = "gridLookUpEditMedicine";
            this.gridLookUpEditMedicine.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditMedicine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMedicine.Properties.DisplayMember = "name";
            this.gridLookUpEditMedicine.Properties.NullText = "";
            this.gridLookUpEditMedicine.Properties.PopupFormMinSize = new System.Drawing.Size(600, 0);
            this.gridLookUpEditMedicine.Properties.PopupFormSize = new System.Drawing.Size(600, 300);
            this.gridLookUpEditMedicine.Properties.UseReadOnlyAppearance = false;
            this.gridLookUpEditMedicine.Properties.ValueMember = "medicinePrice";
            this.gridLookUpEditMedicine.Properties.View = this.gridView3;
            this.gridLookUpEditMedicine.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditMedicine_Properties_ButtonClick);
            this.gridLookUpEditMedicine.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditMedicine.TabIndex = 13;
            this.gridLookUpEditMedicine.EditValueChanged += new System.EventHandler(this.gridLookUpEditMedicine_EditValueChanged);
            this.gridLookUpEditMedicine.Click += new System.EventHandler(this.gridLookUpEditMedicine_Click);
            this.gridLookUpEditMedicine.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditMedicine_KeyUp);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn27,
            this.gridColumn19,
            this.gridColumn25,
            this.gridColumn18,
            this.gridColumn28,
            this.gridColumnValidDate,
            this.gridColumn20,
            this.gridColumn31,
            this.gridColumn17,
            this.gridColumn16,
            this.gridColumn6});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn25, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Нэр";
            this.gridColumn15.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn15.FieldName = "name";
            this.gridColumn15.MinWidth = 100;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 100;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Үлдэгдэл";
            this.gridColumn27.CustomizationCaption = "Үлдэгдэл";
            this.gridColumn27.DisplayFormat.FormatString = "n2";
            this.gridColumn27.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn27.FieldName = "count";
            this.gridColumn27.MinWidth = 75;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 1;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Үнэ";
            this.gridColumn19.CustomizationCaption = "Үнэ";
            this.gridColumn19.DisplayFormat.FormatString = "n2";
            this.gridColumn19.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn19.FieldName = "price";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 2;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Эмийн багц";
            this.gridColumn25.CustomizationCaption = "Эмийн багц";
            this.gridColumn25.FieldName = "packageName";
            this.gridColumn25.MinWidth = 100;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 3;
            this.gridColumn25.Width = 100;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Тун хэмжээ";
            this.gridColumn18.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn18.FieldName = "unitType";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 5;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Хэлбэр";
            this.gridColumn28.CustomizationCaption = "Хэлбэр";
            this.gridColumn28.FieldName = "shape";
            this.gridColumn28.MinWidth = 75;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 6;
            // 
            // gridColumnValidDate
            // 
            this.gridColumnValidDate.Caption = "Хүчинтэй хугацаа";
            this.gridColumnValidDate.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumnValidDate.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.gridColumnValidDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumnValidDate.FieldName = "validDate";
            this.gridColumnValidDate.MinWidth = 75;
            this.gridColumnValidDate.Name = "gridColumnValidDate";
            this.gridColumnValidDate.Visible = true;
            this.gridColumnValidDate.VisibleIndex = 4;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Бар код";
            this.gridColumn20.CustomizationCaption = "Бар код";
            this.gridColumn20.FieldName = "barcode";
            this.gridColumn20.MinWidth = 75;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 7;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Орлогын төрөл";
            this.gridColumn31.CustomizationCaption = "Орлогын төрөл";
            this.gridColumn31.FieldName = "incTypeName";
            this.gridColumn31.MinWidth = 75;
            this.gridColumn31.Name = "gridColumn31";
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Ангилал";
            this.gridColumn17.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn17.FieldName = "medicineTypeName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 8;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "ОУ нэр";
            this.gridColumn16.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn16.FieldName = "latinName";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 9;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Сериал";
            this.gridColumn6.CustomizationCaption = "Сериал";
            this.gridColumn6.FieldName = "serial";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 10;
            // 
            // spinEditCount
            // 
            this.spinEditCount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditCount.EnterMoveNextControl = true;
            this.spinEditCount.Location = new System.Drawing.Point(167, 398);
            this.spinEditCount.Name = "spinEditCount";
            this.spinEditCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditCount.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditCount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.EditFormat.FormatString = "n2";
            this.spinEditCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.Mask.EditMask = "n2";
            this.spinEditCount.Size = new System.Drawing.Size(100, 20);
            this.spinEditCount.TabIndex = 14;
            this.spinEditCount.EditValueChanged += new System.EventHandler(this.spinEditCount_ValueChanged);
            // 
            // spinEditPrice
            // 
            this.spinEditPrice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditPrice.Location = new System.Drawing.Point(167, 450);
            this.spinEditPrice.Name = "spinEditPrice";
            this.spinEditPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditPrice.Properties.DisplayFormat.FormatString = "n3";
            this.spinEditPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.EditFormat.FormatString = "n3";
            this.spinEditPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditPrice.Properties.Mask.EditMask = "n3";
            this.spinEditPrice.Properties.ReadOnly = true;
            this.spinEditPrice.Size = new System.Drawing.Size(100, 20);
            this.spinEditPrice.TabIndex = 25;
            // 
            // spinEditSumPrice
            // 
            this.spinEditSumPrice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditSumPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditSumPrice.Location = new System.Drawing.Point(167, 476);
            this.spinEditSumPrice.Name = "spinEditSumPrice";
            this.spinEditSumPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditSumPrice.Properties.DisplayFormat.FormatString = "n3";
            this.spinEditSumPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditSumPrice.Properties.EditFormat.FormatString = "n3";
            this.spinEditSumPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditSumPrice.Properties.Mask.EditMask = "n3";
            this.spinEditSumPrice.Properties.ReadOnly = true;
            this.spinEditSumPrice.Size = new System.Drawing.Size(100, 20);
            this.spinEditSumPrice.TabIndex = 26;
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl10.Location = new System.Drawing.Point(65, 277);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(96, 13);
            this.labelControl10.TabIndex = 28;
            this.labelControl10.Text = "Дагалдах баримт :";
            // 
            // textEditDocNumber
            // 
            this.textEditDocNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditDocNumber.EnterMoveNextControl = true;
            this.textEditDocNumber.Location = new System.Drawing.Point(167, 274);
            this.textEditDocNumber.Name = "textEditDocNumber";
            this.textEditDocNumber.Size = new System.Drawing.Size(200, 20);
            this.textEditDocNumber.TabIndex = 10;
            // 
            // gridLookUpEditExpUser
            // 
            this.gridLookUpEditExpUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditExpUser.EditValue = "";
            this.gridLookUpEditExpUser.Location = new System.Drawing.Point(167, 222);
            this.gridLookUpEditExpUser.Name = "gridLookUpEditExpUser";
            this.gridLookUpEditExpUser.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditExpUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Арилгах", "delete", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditExpUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditExpUser.Properties.NullText = "";
            this.gridLookUpEditExpUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditExpUser.Properties.ValueMember = "id";
            this.gridLookUpEditExpUser.Properties.View = this.gridView4;
            this.gridLookUpEditExpUser.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditExpUser_Properties_ButtonClick);
            this.gridLookUpEditExpUser.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditExpUser.TabIndex = 8;
            this.gridLookUpEditExpUser.EditValueChanged += new System.EventHandler(this.gridLookUpEditExpUser_EditValueChanged);
            this.gridLookUpEditExpUser.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditExpUser_KeyUp);
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn1,
            this.gridColumn21,
            this.gridColumn23,
            this.gridColumn24});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Дугаар";
            this.gridColumn3.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn3.FieldName = "id";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Нэр";
            this.gridColumn21.CustomizationCaption = "Нэр";
            this.gridColumn21.FieldName = "firstName";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Эрх";
            this.gridColumn23.CustomizationCaption = "Эрх";
            this.gridColumn23.FieldName = "roleName";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Тасаг";
            this.gridColumn24.CustomizationCaption = "Тасаг";
            this.gridColumn24.FieldName = "wardName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // labelControlExpWarehouse
            // 
            this.labelControlExpWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlExpWarehouse.Location = new System.Drawing.Point(60, 251);
            this.labelControlExpWarehouse.Name = "labelControlExpWarehouse";
            this.labelControlExpWarehouse.Size = new System.Drawing.Size(101, 13);
            this.labelControlExpWarehouse.TabIndex = 29;
            this.labelControlExpWarehouse.Text = "Хүлээн авах тасаг*:";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(62, 173);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(99, 13);
            this.labelControl2.TabIndex = 32;
            this.labelControl2.Text = "Гүйлгээний төрөл*:";
            // 
            // spinEditBal
            // 
            this.spinEditBal.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditBal.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditBal.Location = new System.Drawing.Point(167, 424);
            this.spinEditBal.Name = "spinEditBal";
            this.spinEditBal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditBal.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditBal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditBal.Properties.EditFormat.FormatString = "n2";
            this.spinEditBal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditBal.Properties.Mask.EditMask = "n2";
            this.spinEditBal.Properties.ReadOnly = true;
            this.spinEditBal.Size = new System.Drawing.Size(100, 20);
            this.spinEditBal.TabIndex = 24;
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl12.Location = new System.Drawing.Point(104, 427);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(57, 13);
            this.labelControl12.TabIndex = 33;
            this.labelControl12.Text = "Үлдэгдэл*:";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 526);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 23;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // treeListLookUpEditWard
            // 
            this.treeListLookUpEditWard.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWard.Location = new System.Drawing.Point(167, 12);
            this.treeListLookUpEditWard.Name = "treeListLookUpEditWard";
            this.treeListLookUpEditWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditWard.Properties.NullText = "";
            this.treeListLookUpEditWard.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditWard.Properties.ValueMember = "id";
            this.treeListLookUpEditWard.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWard.TabIndex = 5;
            this.treeListLookUpEditWard.Visible = false;
            this.treeListLookUpEditWard.EditValueChanged += new System.EventHandler(this.treeListLookUpEditWarehouse_EditValueChanged);
            this.treeListLookUpEditWard.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWard_KeyUp);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(142, 155);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Нэр";
            this.treeListColumn2.FieldName = "name";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // treeListLookUpEditWarehouse
            // 
            this.treeListLookUpEditWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWarehouse.EditValue = "0";
            this.treeListLookUpEditWarehouse.Location = new System.Drawing.Point(167, 144);
            this.treeListLookUpEditWarehouse.Name = "treeListLookUpEditWarehouse";
            this.treeListLookUpEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWarehouse.Properties.DisplayMember = "name";
            this.treeListLookUpEditWarehouse.Properties.NullText = "";
            this.treeListLookUpEditWarehouse.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEditWarehouse.Properties.ValueMember = "id";
            this.treeListLookUpEditWarehouse.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWarehouse.TabIndex = 5;
            this.treeListLookUpEditWarehouse.EditValueChanged += new System.EventHandler(this.treeListLookUpEditWarehouse_EditValueChanged);
            this.treeListLookUpEditWarehouse.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWarehouse_KeyUp);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1,
            this.treeListColumn3});
            this.treeListLookUpEdit1TreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListLookUpEdit1TreeList.KeyFieldName = "id";
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(155, 217);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsBehavior.PopulateServiceColumns = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.ParentFieldName = "pid";
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Нэр";
            this.treeListColumn1.CustomizationCaption = "Агуулахын нэр";
            this.treeListColumn1.FieldName = "name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraTreeList.FilterPopupMode.List;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 400;
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "Дугаар";
            this.treeListColumn3.CustomizationCaption = "Агуулахын дугаар";
            this.treeListColumn3.FieldName = "number";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 1;
            this.treeListColumn3.Width = 270;
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl14.Location = new System.Drawing.Point(62, 120);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(99, 13);
            this.labelControl14.TabIndex = 44;
            this.labelControl14.Text = "Агуулахын төрөл*:";
            // 
            // radioGroupType
            // 
            this.radioGroupType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupType.EnterMoveNextControl = true;
            this.radioGroupType.Location = new System.Drawing.Point(167, 116);
            this.radioGroupType.Name = "radioGroupType";
            this.radioGroupType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Эмийн сан"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Тасаг")});
            this.radioGroupType.Size = new System.Drawing.Size(200, 22);
            this.radioGroupType.TabIndex = 4;
            this.radioGroupType.SelectedIndexChanged += new System.EventHandler(this.radioGroupType_SelectedIndexChanged);
            // 
            // gridLookUpEditUser
            // 
            this.gridLookUpEditUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditUser.EditValue = "";
            this.gridLookUpEditUser.Location = new System.Drawing.Point(167, 90);
            this.gridLookUpEditUser.Name = "gridLookUpEditUser";
            this.gridLookUpEditUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditUser.Properties.NullText = "";
            this.gridLookUpEditUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditUser.Properties.ValueMember = "id";
            this.gridLookUpEditUser.Properties.View = this.gridView2;
            this.gridLookUpEditUser.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditUser.TabIndex = 3;
            this.gridLookUpEditUser.EditValueChanged += new System.EventHandler(this.gridLookUpEditUser_EditValueChanged);
            this.gridLookUpEditUser.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditUser_KeyUp);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Дугаар";
            this.gridColumn2.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn2.FieldName = "id";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Овог";
            this.gridColumn8.CustomizationCaption = "Овог";
            this.gridColumn8.FieldName = "lastName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Нэр";
            this.gridColumn9.CustomizationCaption = "Нэр";
            this.gridColumn9.FieldName = "firstName";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Эрх";
            this.gridColumn11.CustomizationCaption = "Эрх";
            this.gridColumn11.FieldName = "roleName";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Тасаг";
            this.gridColumn12.CustomizationCaption = "Тасаг";
            this.gridColumn12.FieldName = "wardName";
            this.gridColumn12.MinWidth = 75;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl13.Location = new System.Drawing.Point(120, 93);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(41, 13);
            this.labelControl13.TabIndex = 43;
            this.labelControl13.Text = "Нярав*:";
            // 
            // treeListLookUpEditExpWarehouse
            // 
            this.treeListLookUpEditExpWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditExpWarehouse.EditValue = "0";
            this.treeListLookUpEditExpWarehouse.Location = new System.Drawing.Point(167, 248);
            this.treeListLookUpEditExpWarehouse.Name = "treeListLookUpEditExpWarehouse";
            this.treeListLookUpEditExpWarehouse.Properties.ActionButtonIndex = 1;
            this.treeListLookUpEditExpWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Арилгах", "delete", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditExpWarehouse.Properties.DisplayMember = "name";
            this.treeListLookUpEditExpWarehouse.Properties.NullText = "";
            this.treeListLookUpEditExpWarehouse.Properties.TreeList = this.treeList2;
            this.treeListLookUpEditExpWarehouse.Properties.ValueMember = "id";
            this.treeListLookUpEditExpWarehouse.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.treeListLookUpEditExpWarehouse_Properties_ButtonClick);
            this.treeListLookUpEditExpWarehouse.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditExpWarehouse.TabIndex = 9;
            this.treeListLookUpEditExpWarehouse.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditExpWarehouse_KeyUp);
            // 
            // treeList2
            // 
            this.treeList2.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn4,
            this.treeListColumn5});
            this.treeList2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList2.KeyFieldName = "id";
            this.treeList2.Location = new System.Drawing.Point(157, 234);
            this.treeList2.Name = "treeList2";
            this.treeList2.OptionsBehavior.EnableFiltering = true;
            this.treeList2.OptionsBehavior.PopulateServiceColumns = true;
            this.treeList2.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList2.ParentFieldName = "pid";
            this.treeList2.Size = new System.Drawing.Size(400, 200);
            this.treeList2.TabIndex = 0;
            // 
            // treeListColumn4
            // 
            this.treeListColumn4.Caption = "Нэр";
            this.treeListColumn4.CustomizationCaption = "Агуулахын нэр";
            this.treeListColumn4.FieldName = "name";
            this.treeListColumn4.Name = "treeListColumn4";
            this.treeListColumn4.OptionsColumn.AllowMove = false;
            this.treeListColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraTreeList.FilterPopupMode.List;
            this.treeListColumn4.Visible = true;
            this.treeListColumn4.VisibleIndex = 0;
            this.treeListColumn4.Width = 400;
            // 
            // treeListColumn5
            // 
            this.treeListColumn5.Caption = "Дугаар";
            this.treeListColumn5.CustomizationCaption = "Агуулахын дугаар";
            this.treeListColumn5.FieldName = "number";
            this.treeListColumn5.Name = "treeListColumn5";
            this.treeListColumn5.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumn5.Visible = true;
            this.treeListColumn5.VisibleIndex = 1;
            this.treeListColumn5.Width = 270;
            // 
            // treeListLookUpEditExpWard
            // 
            this.treeListLookUpEditExpWard.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditExpWard.Location = new System.Drawing.Point(167, 502);
            this.treeListLookUpEditExpWard.Name = "treeListLookUpEditExpWard";
            this.treeListLookUpEditExpWard.Properties.ActionButtonIndex = 1;
            this.treeListLookUpEditExpWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "Арилгах", "delete", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditExpWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditExpWard.Properties.NullText = "";
            this.treeListLookUpEditExpWard.Properties.TreeList = this.treeList3;
            this.treeListLookUpEditExpWard.Properties.ValueMember = "id";
            this.treeListLookUpEditExpWard.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.treeListLookUpEditExpWard_Properties_ButtonClick);
            this.treeListLookUpEditExpWard.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditExpWard.TabIndex = 9;
            this.treeListLookUpEditExpWard.Visible = false;
            this.treeListLookUpEditExpWard.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditExpWard_KeyUp);
            // 
            // treeList3
            // 
            this.treeList3.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn6});
            this.treeList3.KeyFieldName = "id";
            this.treeList3.Location = new System.Drawing.Point(151, 256);
            this.treeList3.Name = "treeList3";
            this.treeList3.OptionsBehavior.EnableFiltering = true;
            this.treeList3.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList3.ParentFieldName = "pid";
            this.treeList3.Size = new System.Drawing.Size(400, 200);
            this.treeList3.TabIndex = 0;
            // 
            // treeListColumn6
            // 
            this.treeListColumn6.Caption = "Нэр";
            this.treeListColumn6.FieldName = "name";
            this.treeListColumn6.Name = "treeListColumn6";
            this.treeListColumn6.Visible = true;
            this.treeListColumn6.VisibleIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(58, 225);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(103, 13);
            this.labelControl1.TabIndex = 45;
            this.labelControl1.Text = "Хүлээн авах нярав*:";
            // 
            // simpleButtonNewDoc
            // 
            this.simpleButtonNewDoc.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.simpleButtonNewDoc.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonNewDoc.Image")));
            this.simpleButtonNewDoc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButtonNewDoc.Location = new System.Drawing.Point(373, 36);
            this.simpleButtonNewDoc.Name = "simpleButtonNewDoc";
            this.simpleButtonNewDoc.Size = new System.Drawing.Size(70, 23);
            this.simpleButtonNewDoc.TabIndex = 24;
            this.simpleButtonNewDoc.Text = "Шинэ";
            this.simpleButtonNewDoc.ToolTip = "Шинэ баримт үүсгэх";
            this.simpleButtonNewDoc.Click += new System.EventHandler(this.simpleButtonNewDoc_Click);
            // 
            // treeListLookUpEditExpType
            // 
            this.treeListLookUpEditExpType.EditValue = "";
            this.treeListLookUpEditExpType.Location = new System.Drawing.Point(167, 170);
            this.treeListLookUpEditExpType.Name = "treeListLookUpEditExpType";
            this.treeListLookUpEditExpType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditExpType.Properties.DisplayMember = "text";
            this.treeListLookUpEditExpType.Properties.NullText = "";
            this.treeListLookUpEditExpType.Properties.TreeList = this.treeList4;
            this.treeListLookUpEditExpType.Properties.ValueMember = "id";
            this.treeListLookUpEditExpType.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditExpType.TabIndex = 6;
            this.treeListLookUpEditExpType.QueryCloseUp += new System.ComponentModel.CancelEventHandler(this.treeListLookUpEditExpType_QueryCloseUp);
            this.treeListLookUpEditExpType.EditValueChanged += new System.EventHandler(this.treeListLookUpEditExpType_EditValueChanged);
            this.treeListLookUpEditExpType.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.treeListLookUpEditExpType_CustomDisplayText);
            this.treeListLookUpEditExpType.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditExpType_KeyUp);
            // 
            // treeList4
            // 
            this.treeList4.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn7,
            this.treeListColumn9,
            this.treeListColumn10});
            this.treeList4.KeyFieldName = "";
            this.treeList4.Location = new System.Drawing.Point(0, 0);
            this.treeList4.Name = "treeList4";
            this.treeList4.OptionsBehavior.EnableFiltering = true;
            this.treeList4.OptionsBehavior.PopulateServiceColumns = true;
            this.treeList4.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList4.ParentFieldName = "";
            this.treeList4.Size = new System.Drawing.Size(400, 200);
            this.treeList4.TabIndex = 0;
            // 
            // treeListColumn7
            // 
            this.treeListColumn7.Caption = "id";
            this.treeListColumn7.FieldName = "id";
            this.treeListColumn7.Name = "treeListColumn7";
            this.treeListColumn7.Width = 100;
            // 
            // treeListColumn9
            // 
            this.treeListColumn9.Caption = "Нэр";
            this.treeListColumn9.FieldName = "name";
            this.treeListColumn9.Name = "treeListColumn9";
            this.treeListColumn9.Visible = true;
            this.treeListColumn9.VisibleIndex = 0;
            this.treeListColumn9.Width = 128;
            // 
            // treeListColumn10
            // 
            this.treeListColumn10.Caption = "text";
            this.treeListColumn10.FieldName = "text";
            this.treeListColumn10.Name = "treeListColumn10";
            this.treeListColumn10.Width = 146;
            // 
            // labelControlQuantity
            // 
            this.labelControlQuantity.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlQuantity.Location = new System.Drawing.Point(273, 401);
            this.labelControlQuantity.Name = "labelControlQuantity";
            this.labelControlQuantity.Size = new System.Drawing.Size(8, 13);
            this.labelControlQuantity.TabIndex = 46;
            this.labelControlQuantity.Text = "ш";
            // 
            // gridLookUpEditExpType
            // 
            this.gridLookUpEditExpType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditExpType.EditValue = "";
            this.gridLookUpEditExpType.Location = new System.Drawing.Point(167, 196);
            this.gridLookUpEditExpType.Name = "gridLookUpEditExpType";
            this.gridLookUpEditExpType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditExpType.Properties.DisplayMember = "name";
            this.gridLookUpEditExpType.Properties.NullText = "";
            this.gridLookUpEditExpType.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditExpType.Properties.ValueMember = "id";
            this.gridLookUpEditExpType.Properties.View = this.gridView1;
            this.gridLookUpEditExpType.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditExpType.TabIndex = 7;
            this.gridLookUpEditExpType.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditExpType_KeyUp);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Нэр";
            this.gridColumn4.CustomizationCaption = "Орлогын төрлийн нэр";
            this.gridColumn4.FieldName = "name";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Тайлбар";
            this.gridColumn5.CustomizationCaption = "Орлогын төрлийн тайлбар";
            this.gridColumn5.FieldName = "description";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl11.Location = new System.Drawing.Point(69, 199);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(92, 13);
            this.labelControl11.TabIndex = 48;
            this.labelControl11.Text = "Зарлагын төрөл*:";
            // 
            // gridLookUpEditDoctor
            // 
            this.gridLookUpEditDoctor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditDoctor.EditValue = "";
            this.gridLookUpEditDoctor.Location = new System.Drawing.Point(167, 300);
            this.gridLookUpEditDoctor.Name = "gridLookUpEditDoctor";
            this.gridLookUpEditDoctor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDoctor.Properties.DisplayMember = "fullName";
            this.gridLookUpEditDoctor.Properties.NullText = "";
            this.gridLookUpEditDoctor.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditDoctor.Properties.ValueMember = "id";
            this.gridLookUpEditDoctor.Properties.View = this.gridView5;
            this.gridLookUpEditDoctor.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditDoctor.TabIndex = 11;
            this.gridLookUpEditDoctor.EditValueChanged += new System.EventHandler(this.gridLookUpEditDoctor_EditValueChanged);
            this.gridLookUpEditDoctor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditDoctor_KeyUp);
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn10,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn22});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ShowAutoFilterRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Дугаар";
            this.gridColumn7.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn7.FieldName = "id";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Овог";
            this.gridColumn10.CustomizationCaption = "Овог";
            this.gridColumn10.FieldName = "lastName";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Нэр";
            this.gridColumn13.CustomizationCaption = "Нэр";
            this.gridColumn13.FieldName = "firstName";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 1;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Эрх";
            this.gridColumn14.CustomizationCaption = "Эрх";
            this.gridColumn14.FieldName = "roleName";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 3;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Тасаг";
            this.gridColumn22.CustomizationCaption = "Тасаг";
            this.gridColumn22.FieldName = "wardName";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 4;
            // 
            // labelControl15
            // 
            this.labelControl15.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl15.Location = new System.Drawing.Point(135, 303);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(26, 13);
            this.labelControl15.TabIndex = 50;
            this.labelControl15.Text = "Эмч :";
            // 
            // checkEditMultiSelection
            // 
            this.checkEditMultiSelection.Location = new System.Drawing.Point(373, 373);
            this.checkEditMultiSelection.Name = "checkEditMultiSelection";
            this.checkEditMultiSelection.Properties.Caption = "олон сонгох";
            this.checkEditMultiSelection.Size = new System.Drawing.Size(88, 19);
            this.checkEditMultiSelection.TabIndex = 51;
            this.checkEditMultiSelection.CheckedChanged += new System.EventHandler(this.checkEditMultiSelection_CheckedChanged);
            // 
            // gridControlMed
            // 
            this.gridControlMed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMed.Location = new System.Drawing.Point(0, 0);
            this.gridControlMed.MainView = this.gridViewMed;
            this.gridControlMed.Name = "gridControlMed";
            this.gridControlMed.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1});
            this.gridControlMed.Size = new System.Drawing.Size(302, 98);
            this.gridControlMed.TabIndex = 52;
            this.gridControlMed.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMed});
            // 
            // gridViewMed
            // 
            this.gridViewMed.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumName,
            this.gridColumnLatinName,
            this.gridColumnType,
            this.gridColumnUnit,
            this.gridColumnPrice,
            this.gridColumnSerial,
            this.gridColumnValidatedDate,
            this.gridColumnBal,
            this.gridColumnDupCount});
            this.gridViewMed.GridControl = this.gridControlMed;
            this.gridViewMed.Name = "gridViewMed";
            this.gridViewMed.OptionsFind.AlwaysVisible = true;
            this.gridViewMed.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewMed.OptionsSelection.MultiSelect = true;
            this.gridViewMed.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridViewMed.OptionsView.ShowGroupPanel = false;
            this.gridViewMed.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewMed_CustomDrawCell);
            this.gridViewMed.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewMed_SelectionChanged);
            this.gridViewMed.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewMed_CellValueChanged);
            this.gridViewMed.ColumnFilterChanged += new System.EventHandler(this.gridViewMed_ColumnFilterChanged);
            this.gridViewMed.CustomRowFilter += new DevExpress.XtraGrid.Views.Base.RowFilterEventHandler(this.gridViewMed_CustomRowFilter);
            // 
            // gridColumName
            // 
            this.gridColumName.Caption = "Нэр";
            this.gridColumName.FieldName = "name";
            this.gridColumName.MinWidth = 100;
            this.gridColumName.Name = "gridColumName";
            this.gridColumName.OptionsColumn.ReadOnly = true;
            this.gridColumName.Visible = true;
            this.gridColumName.VisibleIndex = 1;
            this.gridColumName.Width = 100;
            // 
            // gridColumnLatinName
            // 
            this.gridColumnLatinName.Caption = "О.У нэр";
            this.gridColumnLatinName.FieldName = "latinName";
            this.gridColumnLatinName.MinWidth = 100;
            this.gridColumnLatinName.Name = "gridColumnLatinName";
            this.gridColumnLatinName.OptionsColumn.ReadOnly = true;
            this.gridColumnLatinName.Visible = true;
            this.gridColumnLatinName.VisibleIndex = 5;
            this.gridColumnLatinName.Width = 100;
            // 
            // gridColumnType
            // 
            this.gridColumnType.Caption = "Ангилал";
            this.gridColumnType.FieldName = "medicineTypeName";
            this.gridColumnType.MinWidth = 100;
            this.gridColumnType.Name = "gridColumnType";
            this.gridColumnType.OptionsColumn.ReadOnly = true;
            this.gridColumnType.Visible = true;
            this.gridColumnType.VisibleIndex = 7;
            this.gridColumnType.Width = 100;
            // 
            // gridColumnUnit
            // 
            this.gridColumnUnit.Caption = "Тун хэмжээ";
            this.gridColumnUnit.FieldName = "unitType";
            this.gridColumnUnit.MinWidth = 100;
            this.gridColumnUnit.Name = "gridColumnUnit";
            this.gridColumnUnit.OptionsColumn.ReadOnly = true;
            this.gridColumnUnit.Visible = true;
            this.gridColumnUnit.VisibleIndex = 8;
            this.gridColumnUnit.Width = 100;
            // 
            // gridColumnPrice
            // 
            this.gridColumnPrice.Caption = "Үнэ";
            this.gridColumnPrice.DisplayFormat.FormatString = "c2";
            this.gridColumnPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnPrice.FieldName = "price";
            this.gridColumnPrice.MinWidth = 75;
            this.gridColumnPrice.Name = "gridColumnPrice";
            this.gridColumnPrice.OptionsColumn.ReadOnly = true;
            this.gridColumnPrice.Visible = true;
            this.gridColumnPrice.VisibleIndex = 4;
            this.gridColumnPrice.Width = 100;
            // 
            // gridColumnSerial
            // 
            this.gridColumnSerial.Caption = "Сериал";
            this.gridColumnSerial.FieldName = "serial";
            this.gridColumnSerial.MinWidth = 75;
            this.gridColumnSerial.Name = "gridColumnSerial";
            this.gridColumnSerial.OptionsColumn.ReadOnly = true;
            this.gridColumnSerial.Visible = true;
            this.gridColumnSerial.VisibleIndex = 9;
            // 
            // gridColumnValidatedDate
            // 
            this.gridColumnValidatedDate.Caption = "Хүчинтэй хугацаа";
            this.gridColumnValidatedDate.FieldName = "validDate";
            this.gridColumnValidatedDate.MinWidth = 75;
            this.gridColumnValidatedDate.Name = "gridColumnValidatedDate";
            this.gridColumnValidatedDate.OptionsColumn.ReadOnly = true;
            this.gridColumnValidatedDate.Visible = true;
            this.gridColumnValidatedDate.VisibleIndex = 6;
            this.gridColumnValidatedDate.Width = 100;
            // 
            // gridColumnBal
            // 
            this.gridColumnBal.Caption = "Үлдэгдэл";
            this.gridColumnBal.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnBal.FieldName = "count";
            this.gridColumnBal.MinWidth = 75;
            this.gridColumnBal.Name = "gridColumnBal";
            this.gridColumnBal.OptionsColumn.ReadOnly = true;
            this.gridColumnBal.Visible = true;
            this.gridColumnBal.VisibleIndex = 2;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n0";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n0";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n0";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnDupCount
            // 
            this.gridColumnDupCount.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnDupCount.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnDupCount.Caption = "Тоо";
            this.gridColumnDupCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnDupCount.FieldName = "dupCount";
            this.gridColumnDupCount.MinWidth = 75;
            this.gridColumnDupCount.Name = "gridColumnDupCount";
            this.gridColumnDupCount.Visible = true;
            this.gridColumnDupCount.VisibleIndex = 3;
            this.gridColumnDupCount.Width = 100;
            // 
            // popupContainerEditMedicineBal
            // 
            this.popupContainerEditMedicineBal.Location = new System.Drawing.Point(287, 398);
            this.popupContainerEditMedicineBal.Name = "popupContainerEditMedicineBal";
            this.popupContainerEditMedicineBal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditMedicineBal.Properties.PopupFormMinSize = new System.Drawing.Size(500, 300);
            this.popupContainerEditMedicineBal.Size = new System.Drawing.Size(199, 20);
            this.popupContainerEditMedicineBal.TabIndex = 53;
            this.popupContainerEditMedicineBal.Visible = false;
            this.popupContainerEditMedicineBal.CloseUp += new DevExpress.XtraEditors.Controls.CloseUpEventHandler(this.popupContainerEditMedicineBal_CloseUp);
            this.popupContainerEditMedicineBal.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.popupContainerEditMedicineBal_ButtonClick);
            this.popupContainerEditMedicineBal.Click += new System.EventHandler(this.popupContainerEditMedicineBal_Click);
            // 
            // popupContainerControlMed
            // 
            this.popupContainerControlMed.Controls.Add(this.gridControlMed);
            this.popupContainerControlMed.Location = new System.Drawing.Point(12, 8);
            this.popupContainerControlMed.Name = "popupContainerControlMed";
            this.popupContainerControlMed.Size = new System.Drawing.Size(302, 98);
            this.popupContainerControlMed.TabIndex = 54;
            // 
            // MedicineExpUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(484, 561);
            this.Controls.Add(this.popupContainerControlMed);
            this.Controls.Add(this.popupContainerEditMedicineBal);
            this.Controls.Add(this.checkEditMultiSelection);
            this.Controls.Add(this.gridLookUpEditDoctor);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.gridLookUpEditExpType);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.labelControlQuantity);
            this.Controls.Add(this.treeListLookUpEditExpType);
            this.Controls.Add(this.simpleButtonNewDoc);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.treeListLookUpEditExpWard);
            this.Controls.Add(this.treeListLookUpEditExpWarehouse);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.radioGroupType);
            this.Controls.Add(this.gridLookUpEditUser);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.treeListLookUpEditWarehouse);
            this.Controls.Add(this.treeListLookUpEditWard);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.spinEditBal);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.gridLookUpEditExpUser);
            this.Controls.Add(this.labelControlExpWarehouse);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.textEditDocNumber);
            this.Controls.Add(this.spinEditSumPrice);
            this.Controls.Add(this.spinEditPrice);
            this.Controls.Add(this.spinEditCount);
            this.Controls.Add(this.gridLookUpEditMedicine);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.memoEditDescription);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.textEditID);
            this.Controls.Add(this.dateEditExpDate);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonSave);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControlWarehouse);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MedicineExpUpForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Зарлага";
            this.Shown += new System.EventHandler(this.MedicineExpInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditSumPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDocNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditExpUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditExpWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditExpWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditExpType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditExpType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMultiSelection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditMedicineBal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMed)).EndInit();
            this.popupContainerControlMed.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControlWarehouse;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.DateEdit dateEditExpDate;
        private DevExpress.XtraEditors.TextEdit textEditID;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMedicine;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.SpinEdit spinEditCount;
        private DevExpress.XtraEditors.SpinEdit spinEditPrice;
        private DevExpress.XtraEditors.SpinEdit spinEditSumPrice;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit textEditDocNumber;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditExpUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraEditors.LabelControl labelControlExpWarehouse;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraEditors.SpinEdit spinEditBal;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnValidDate;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWard;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWarehouse;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.RadioGroup radioGroupType;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditExpWarehouse;
        private DevExpress.XtraTreeList.TreeList treeList2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn4;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn5;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditExpWard;
        private DevExpress.XtraTreeList.TreeList treeList3;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn6;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonNewDoc;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditExpType;
        private DevExpress.XtraTreeList.TreeList treeList4;
        private DevExpress.XtraEditors.LabelControl labelControlQuantity;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn7;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn9;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditExpType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDoctor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.CheckEdit checkEditMultiSelection;
        private DevExpress.XtraGrid.GridControl gridControlMed;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMed;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLatinName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnUnit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnValidatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDupCount;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditMedicineBal;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlMed;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSerial;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBal;
        public DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
    }
}