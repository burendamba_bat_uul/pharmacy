﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.bal;



namespace Pharmacy2016.gui.core.journal.exp
{
    /*
     * Эмийн зарлага нэмэх, засах цонх.
     * 
     */

    public partial class MedicineExpInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineExpInUpForm INSTANCE = new MedicineExpInUpForm();

            public static MedicineExpInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private string id;

            private MedicineExpInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                spinEditCount.Properties.MaxValue = Decimal.MaxValue - 1;

                initError();
                initDataSource();
                initBinding();
                reload(false);
            }

            private void initDataSource()
            {
                gridControlDetail.DataSource = JournalDataSet.ExpDetailBindingSource;
                gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
                treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
                treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
                gridLookUpEditExpUser.Properties.DataSource = InformationDataSet.SystemUserOtherBindingSource;
                treeListLookUpEditExpWard.Properties.DataSource = JournalDataSet.WardOther1BindingSource;
                treeListLookUpEditExpWarehouse.Properties.DataSource = JournalDataSet.WarehouseOther1BindingSource;
                gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineBalBindingSource;
            }

            private void initError()
            {
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditExpUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditExpWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditExpWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditExpDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                comboBoxEditType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    
                    actionType = type;
                    if (actionType == 1)
                    {
                        insertExp();
                    }
                    else if (actionType == 2)
                    {
                        updateExp();
                    }
                    //initBinding();

                    ShowDialog(MedicineExpForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void initBinding()
            {
                gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "userID", true));
                radioGroupType.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "type", true));
                treeListLookUpEditWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "warehouseID", true));
                treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "wardID", true));
                gridLookUpEditExpUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "expUserID", true));
                treeListLookUpEditExpWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "expWarehouseID", true));
                treeListLookUpEditExpWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "expWardID", true));
                textEditID.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "id", true));
                textEditDocNumber.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "docNumber", true));
                dateEditExpDate.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "date", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "description", true));
                //comboBoxEditType.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpJournalBindingSource, "expType", true));
                
                //gridLookUpEditMedicine.DataBindings.Add(new Binding("EditValue", JournalDataSet.ExpDetailBindingSource, "medicineID", true));
            }

            private void insertExp()
            {
                this.Text = "Зарлага нэмэх цонх";
                id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                JournalDataSet.Instance.ExpJournal.idColumn.DefaultValue = id;
                JournalDataSet.Instance.ExpDetail.expJournalIDColumn.DefaultValue = id;
                MedicineExpForm.Instance.currView = (DataRowView)JournalDataSet.ExpJournalBindingSource.AddNew();
                DateTime now = DateTime.Now;
                MedicineExpForm.Instance.currView["id"] = id;
                MedicineExpForm.Instance.currView["date"] = now;

                dateEditExpDate.DateTime = now;
                textEditID.Text = id;
                comboBoxEditType.SelectedIndex = 0;
            }

            private void updateExp()
            {
                this.Text = "Зарлага засах цонх";
                id = MedicineExpForm.Instance.currView["id"].ToString();
                JournalDataSet.Instance.ExpDetail.expJournalIDColumn.DefaultValue = id;
                comboBoxEditType.SelectedIndex = Convert.ToInt32(MedicineExpForm.Instance.currView["expType"]);
            }


            private void clearData()
            {
                clearErrorText();
                //clearBinding();
                clearMedicine();

                JournalDataSet.WarehouseOtherBindingSource.Filter = "";
                JournalDataSet.WardOtherBindingSource.Filter = "";
                InformationDataSet.SystemUserBindingSource.Filter = "";

                JournalDataSet.ExpJournalBindingSource.CancelEdit();
                JournalDataSet.ExpDetailBindingSource.CancelEdit();
                JournalDataSet.Instance.ExpJournal.RejectChanges();
                JournalDataSet.Instance.ExpDetail.RejectChanges();
            }

            private void clearBinding()
            {
                treeListLookUpEditWarehouse.DataBindings.Clear();
                gridLookUpEditExpUser.DataBindings.Clear();
                textEditID.DataBindings.Clear();
                textEditDocNumber.DataBindings.Clear();
                dateEditExpDate.DataBindings.Clear();
                memoEditDescription.DataBindings.Clear();
                //comboBoxEditType.DataBindings.Clear();
                treeListLookUpEditWard.DataBindings.Clear();
                //gridLookUpEditMedicine.DataBindings.Clear();
            }

            private void clearErrorText()
            {
                gridLookUpEditUser.ErrorText = "";
                treeListLookUpEditWarehouse.ErrorText = "";
                treeListLookUpEditWard.ErrorText = "";
                comboBoxEditType.ErrorText = "";
                gridLookUpEditExpUser.ErrorText = "";
                treeListLookUpEditExpWarehouse.ErrorText = "";
                treeListLookUpEditExpWard.ErrorText = "";
                gridLookUpEditMedicine.ErrorText = "";
                dateEditExpDate.ErrorText = "";
                spinEditCount.ErrorText = "";
            }

            private void clearMedicine()
            {
                gridLookUpEditMedicine.EditValue = null;
                textEditBarcode.Text = "";
                spinEditBal.Value = 0;
                spinEditCount.Value = 0;
                spinEditPrice.Value = 0;
                spinEditSumPrice.Value = 0;
            }

            private void MedicineExpInUpForm_Shown(object sender, EventArgs e)
            {
                gridViewDetail.FocusedRowHandle = 0;
                //gridViewDetail_BeforeLeaveRow(gridViewDetail, new RowAllowEventArgs(gridViewDetail.FocusedRowHandle, true));

                if (actionType == 1)
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                    {
                        gridLookUpEditUser.EditValue = UserUtil.getUserId();
                        treeListLookUpEditWarehouse.Focus();
                    }
                    else
                    {
                        gridLookUpEditUser.Focus();
                    }
                }
                else if (actionType == 2)
                {
                    setValueFromGrid();
                    gridLookUpEditMedicine.Focus();
                }

                if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
            }

        #endregion

        #region Формын event

            private void dateEditExpDate_EditValueChanged(object sender, EventArgs e)
            {
                reloadMedicine();
            }

            private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null)
                {
                    
                    if (actionType == 1 && gridViewDetail.RowCount == 0 && 
                        ((radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && !treeListLookUpEditWarehouse.EditValue.ToString().Equals("")) ||
                        (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && !treeListLookUpEditWard.EditValue.ToString().Equals(""))))
                    {
                        addRow();
                        gridLookUpEditMedicine.Focus();
                    }
                }
            }

            private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeType();
            }

            private void treeListLookUpEditWarehouse_EditValueChanged(object sender, EventArgs e)
            {
                if (actionType == 1 && gridViewDetail.RowCount == 0 && (gridLookUpEditUser.EditValue != null && gridLookUpEditUser.EditValue != DBNull.Value && !gridLookUpEditUser.EditValue.ToString().Equals("")) &&
                    ((radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && !treeListLookUpEditWarehouse.EditValue.ToString().Equals("")) ||
                     (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && !treeListLookUpEditWard.EditValue.ToString().Equals(""))))
                {
                    addRow();
                    gridLookUpEditMedicine.Focus();
                }
                reloadMedicine();
            }

            private void comboBoxEditType_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeExpType();
            }


            private void gridControlDetail_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add") && (gridViewDetail.RowCount == 0 || (gridLookUpEditMedicine.ErrorText == "" && gridLookUpEditMedicine.EditValue != null && gridLookUpEditMedicine.EditValue != DBNull.Value && checkMedicine())))
                    {
                        addRow();
                        gridLookUpEditMedicine.Focus();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewDetail.GetFocusedDataRow() != null)
                    {
                        deleteRow();
                    }
                }
            }

            private void gridViewDetail_BeforeLeaveRow(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
            {
                if (this.Visible && gridViewDetail.GetDataRow(e.RowHandle) != null && 
                    (gridViewDetail.GetDataRow(e.RowHandle)["medicineID"] == DBNull.Value
                    || Convert.ToDecimal(gridViewDetail.GetDataRow(e.RowHandle)["price"]) == 0
                    || Convert.ToDecimal(gridViewDetail.GetDataRow(e.RowHandle)["count"]) == 0))
                {
                    e.Allow = e.RowHandle <= 0;
                    if (!e.Allow)
                        spinEditCount.ErrorText = "Тоо, хэмжээг оруулна уу";
                }
                else if(this.Visible && (gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != DBNull.Value
                    && Convert.ToDecimal(gridViewDetail.GetRowCellValue(e.RowHandle, "price")) != 0))
                {
                    bool isAllow = true;
                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        if (e.RowHandle != i && gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != null && gridViewDetail.GetRowCellValue(i, "medicineID") != null &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != DBNull.Value && gridViewDetail.GetRowCellValue(i, "medicineID") != DBNull.Value &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "incTypeID") != null && gridViewDetail.GetRowCellValue(i, "incTypeID") != null &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "incTypeID") != DBNull.Value && gridViewDetail.GetRowCellValue(i, "incTypeID") != DBNull.Value &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID").ToString().Equals(gridViewDetail.GetRowCellValue(i, "medicineID").ToString()) &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "incTypeID").ToString().Equals(gridViewDetail.GetRowCellValue(i, "incTypeID").ToString()) &&
                            Convert.ToDecimal(gridViewDetail.GetRowCellValue(e.RowHandle, "price")) == Convert.ToDecimal(gridViewDetail.GetRowCellValue(i, "price")))
                        {
                            gridLookUpEditMedicine.ErrorText = "Эм, үнэ, орлогын төрөл давхацсан байна";
                            gridLookUpEditMedicine.Focus();
                            isAllow = false;
                            break;
                        }
                    }
                    e.Allow = isAllow;
                }
            }

            private void gridViewDetail_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                gridLookUpEditMedicine.Focus();
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridViewDetail_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
            {
                if(Visible)
                    setValueFromGrid();
            }

            
            private void gridLookUpEditMedicine_EditValueChanged(object sender, EventArgs e)
            {
                DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                if (Visible && view != null)
                {
                    textEditBarcode.Text = view["barcode"].ToString();
                    spinEditPrice.Value = Convert.ToDecimal(view["price"]);
                    spinEditBal.Value = Convert.ToDecimal(view["count"]);
                }
            }

            private void gridLookUpEditMedicine_Validating(object sender, CancelEventArgs e)
            {
                if (Visible && !e.Cancel && gridLookUpEditMedicine.EditValue != DBNull.Value && gridLookUpEditMedicine.EditValue != null)
                {
                    endEdit();
                }
            }

            private void gridLookUpEditMedicine_InvalidValue(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridLookUpEditMedicine_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    MedicineBalExpForm.Instance.showForm(1, Instance);
                }   
            }


            private void textEditBarcode_EditValueChanged(object sender, EventArgs e)
            {
                if (Visible && !(textEditBarcode.Text = textEditBarcode.Text.Trim()).Equals(""))
                {
                    for (int i = 0; i < JournalDataSet.Instance.MedicineBal.Rows.Count; i++)
                    {
                        if (JournalDataSet.Instance.MedicineBal.Rows[i]["barcode"].ToString().Equals(textEditBarcode.Text))
                        {
                            gridLookUpEditMedicine.EditValue = JournalDataSet.Instance.MedicineBal.Rows[i]["medicinePrice"];
                            spinEditCount.Focus();
                            break;
                        }
                    }
                }
            }


            private void spinEditCount_ValueChanged(object sender, EventArgs e)
            {
                if (Visible)
                {
                    calcPrice();
                    if (spinEditBal.Value < spinEditCount.Value)
                    {
                        spinEditCount.ForeColor = Color.Red;
                    }
                    else
                    {
                        spinEditCount.ForeColor = Color.Black;
                    }
                }
            }

            private void spinEditCount_Leave(object sender, EventArgs e)
            {
                if (Visible)
                {
                    endEdit();
                }
            }

            private void spinEditCount_Validated(object sender, EventArgs e)
            {
                if(Visible)
                    endEdit();
            }
            
            
            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload(true);
            }
            
            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private void reload(bool isReload)
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                InformationDataSet.SystemUserOtherTableAdapter.Fill(InformationDataSet.Instance.SystemUserOther, HospitalUtil.getHospitalId(), SystemUtil.ALL_USER);
                if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                    JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                    JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
                //ProgramUtil.closeWaitDialog();
            }

            private void reloadMedicine()
            {
                JournalDataSet.MedicineBalTableAdapter.Fill(JournalDataSet.Instance.MedicineBal, HospitalUtil.getHospitalId(), gridLookUpEditUser.EditValue.ToString(), radioGroupType.SelectedIndex, (radioGroupType.SelectedIndex == 0 ? treeListLookUpEditWarehouse.EditValue.ToString() : null), (radioGroupType.SelectedIndex == 1 ? treeListLookUpEditWard.EditValue.ToString() : null), dateEditExpDate.Text);
            }

            private void changeExpType()
            {
                if (comboBoxEditType.SelectedIndex == 0 || comboBoxEditType.SelectedIndex == 1)
                {
                    treeListLookUpEditExpWard.Visible = false;
                    treeListLookUpEditExpWarehouse.Visible = false;
                    treeListLookUpEditExpWard.Location = new Point(455, 4);
                    treeListLookUpEditExpWarehouse.Location = new Point(455, 4);

                    labelControlExpUser.Text = "Хариуцагч*:";
                    labelControlExpUser.Location = new Point(386, 85);
                    gridLookUpEditExpUser.Visible = true;
                    gridLookUpEditExpUser.Location = new Point(455, 82);
                }
                else if (comboBoxEditType.SelectedIndex == 2)
                {
                    treeListLookUpEditExpWard.Visible = false;
                    gridLookUpEditExpUser.Visible = false;
                    treeListLookUpEditExpWard.Location = new Point(455, 4);
                    gridLookUpEditExpUser.Location = new Point(455, 4);

                    labelControlExpUser.Text = "Агуулах*:";
                    labelControlExpUser.Location = new Point(397, 85);
                    treeListLookUpEditExpWarehouse.Visible = true;
                    treeListLookUpEditExpWarehouse.Location = new Point(455, 82);
                }
                else if (comboBoxEditType.SelectedIndex == 3)
                {
                    treeListLookUpEditExpWarehouse.Visible = false;
                    gridLookUpEditExpUser.Visible = false;
                    gridLookUpEditExpUser.Location = new Point(455, 4);
                    treeListLookUpEditExpWarehouse.Location = new Point(455, 4);

                    labelControlExpUser.Text = "Тасаг*:";
                    labelControlExpUser.Location = new Point(411, 85);
                    treeListLookUpEditExpWard.Visible = true;
                    treeListLookUpEditExpWard.Location = new Point(455, 82);
                }
            }

            private void changeType()
            {
                if (radioGroupType.SelectedIndex == 0)
                {
                    labelControlWarehouse.Location = new Point(67, 113);
                    treeListLookUpEditWard.Location = new Point(125, 4);
                    treeListLookUpEditWarehouse.Location = new Point(125, 110);

                    treeListLookUpEditWard.Visible = false;
                    treeListLookUpEditWarehouse.Visible = true;
                    labelControlWarehouse.Text = "Агуулах*:";
                }
                else if (radioGroupType.SelectedIndex == 1)
                {
                    labelControlWarehouse.Location = new Point(81, 113);
                    treeListLookUpEditWard.Location = new Point(125, 110);
                    treeListLookUpEditWarehouse.Location = new Point(125, 4);

                    treeListLookUpEditWard.Visible = true;
                    treeListLookUpEditWarehouse.Visible = false;
                    labelControlWarehouse.Text = "Тасаг*:";
                }
            }

            private void setValueFromGrid()
            {
                DataRowView view = (DataRowView)JournalDataSet.ExpDetailBindingSource.Current;

                if (view != null && view["medicinePrice"] != null && view["medicinePrice"] != DBNull.Value && !view["medicinePrice"].ToString().Equals(""))
                {
                    gridLookUpEditMedicine.EditValue = view["medicinePrice"];
                    spinEditPrice.EditValue = view["price"];
                    spinEditCount.EditValue = view["count"];
                    spinEditSumPrice.EditValue = view["sumPrice"];
                    DataRowView row = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                    if (row != null)
                    {
                        textEditBarcode.Text = row["barcode"].ToString();
                        spinEditBal.EditValue = row["count"];
                    }
                    else
                    {
                        spinEditBal.Value = 0;
                    }
                }
                
                if (spinEditBal.Value < spinEditCount.Value)
                {
                    spinEditCount.ForeColor = Color.Red;
                }
                else
                {
                    spinEditCount.ForeColor = Color.Black;
                }
            }

            private void endEdit()
            {
                DataRowView row = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                DataRowView view = (DataRowView)JournalDataSet.ExpDetailBindingSource.Current;
                if (row == null || view == null)
                    return;

                view["medicineID"] = row["medicineID"];
                view["medicineName"] = row["name"];
                view["latinName"] = row["latinName"];
                view["unitType"] = row["unitType"];
                view["shape"] = row["shape"];
                view["validDate"] = row["validDate"];
                view["medicineTypeName"] = row["medicineTypeName"];
                view["incTypeID"] = row["incTypeID"];
                view["incTypeName"] = row["incTypeName"];
                view["price"] = spinEditPrice.EditValue;
                view["count"] = spinEditCount.EditValue;
                view.EndEdit();
            }

            private void calcPrice()
            {
                spinEditSumPrice.Value = spinEditCount.Value * spinEditPrice.Value;
            }
            
            
            private void addRow()
            {
                JournalDataSet.Instance.ExpDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                JournalDataSet.ExpDetailBindingSource.AddNew();
                //gridViewDetail.Focus();
                clearMedicine();
            }

            private void deleteRow()
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн зарлага устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    JournalDataSet.ExpDetailBindingSource.RemoveCurrent();
                    setValueFromGrid();
                }
            }

            private bool check()
            {
                //Instance.Validate();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.EditValue.ToString().Equals(""))
                {
                    errorText = "Няравыг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
                if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.EditValue.ToString().Equals("")))
                {
                    errorText = "Агуулахыг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWarehouse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWarehouse.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.EditValue.ToString().Equals("")))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }
                if (dateEditExpDate.EditValue == DBNull.Value || dateEditExpDate.EditValue == null || dateEditExpDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditExpDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditExpDate.Focus();
                    }
                }
                if ((comboBoxEditType.SelectedIndex == 0 || comboBoxEditType.SelectedIndex == 1) && (gridLookUpEditExpUser.EditValue == DBNull.Value || gridLookUpEditExpUser.EditValue == null || gridLookUpEditExpUser.Text.Equals("")))
                {
                    errorText = "Хариуцагчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditExpUser.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditExpUser.Focus();
                    }
                }
                if (comboBoxEditType.SelectedIndex == 2 && (treeListLookUpEditExpWarehouse.EditValue == DBNull.Value || treeListLookUpEditExpWarehouse.EditValue == null || treeListLookUpEditExpWarehouse.Text.Equals("")))
                {
                    errorText = "Агуулахыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditExpWarehouse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditExpWarehouse.Focus();
                    }
                }
                if (comboBoxEditType.SelectedIndex == 3 && (treeListLookUpEditExpWard.EditValue == DBNull.Value || treeListLookUpEditExpWard.EditValue == null || treeListLookUpEditExpWard.Text.Equals("")))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditExpWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditExpWard.Focus();
                    }
                }
                if (isRight)
                    isRight = checkMedicine();
                else
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private bool checkMedicine()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
                if (spinEditCount.Value == 0)
                {
                    errorText = "Тоо, хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditCount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditCount.Focus();
                    }
                }
                for (int i = 0; isRight && i < gridViewDetail.RowCount; i++)
                {
                    if (gridViewDetail.FocusedRowHandle != i && gridLookUpEditMedicine.EditValue.ToString().Equals(gridViewDetail.GetDataRow(i)["medicinePrice"].ToString()))
                    {
                        errorText = "Эм, үнэ давхацсан байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditMedicine.ErrorText = errorText;
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                        break;
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                    DataRowView expUser = (DataRowView)gridLookUpEditExpUser.GetSelectedDataRow();
                    DataRowView expWarehouse = (DataRowView)treeListLookUpEditExpWarehouse.GetSelectedDataRow();
                    DataRowView expWard = (DataRowView)treeListLookUpEditExpWard.GetSelectedDataRow();

                    MedicineExpForm.Instance.currView["userName"] = user["fullName"];
                    MedicineExpForm.Instance.currView["roleName"] = user["roleName"];
                    MedicineExpForm.Instance.currView["expType"] = comboBoxEditType.SelectedIndex;
                    if(radioGroupType.SelectedIndex == 0)
                    {
                        MedicineExpForm.Instance.currView["wardID"] = null;
                        MedicineExpForm.Instance.currView["warehouseName"] = warehouse["name"];
                        MedicineExpForm.Instance.currView["warehouseDesc"] = warehouse["description"];
                    }
                    else if (radioGroupType.SelectedIndex == 0)
                    {
                        MedicineExpForm.Instance.currView["warehouseID"] = null;
                        MedicineExpForm.Instance.currView["warehouseName"] = ward["name"];
                        MedicineExpForm.Instance.currView["warehouseDesc"] = ward["description"];
                    }
                    if (comboBoxEditType.SelectedIndex == 0 || comboBoxEditType.SelectedIndex == 1)
                    {
                        MedicineExpForm.Instance.currView["expName"] = expUser["fullName"];
                        MedicineExpForm.Instance.currView["expDesc"] = expUser["roleName"];
                        MedicineExpForm.Instance.currView["expWarehouseID"] = null;
                        MedicineExpForm.Instance.currView["expWardID"] = null;
                    }
                    else if (comboBoxEditType.SelectedIndex == 2)
                    {
                        MedicineExpForm.Instance.currView["expName"] = expWarehouse["name"];
                        MedicineExpForm.Instance.currView["expDesc"] = expWarehouse["description"];
                        MedicineExpForm.Instance.currView["expUserID"] = null;
                        MedicineExpForm.Instance.currView["expWardID"] = null;
                    }
                    else if (comboBoxEditType.SelectedIndex == 3)
                    {
                        MedicineExpForm.Instance.currView["expName"] = expWard["name"];
                        MedicineExpForm.Instance.currView["expDesc"] = expWard["description"];
                        MedicineExpForm.Instance.currView["expUserID"] = null;
                        MedicineExpForm.Instance.currView["expWarehouseID"] = null;
                    }
                    
                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        DataRow row = gridViewDetail.GetDataRow(i);
                            
                        row["date"] = dateEditExpDate.EditValue;
                        row["userID"] = gridLookUpEditUser.EditValue;
                        row["type"] = radioGroupType.SelectedIndex;
                        row["expType"] = comboBoxEditType.SelectedIndex;

                        row["expUserID"] = gridLookUpEditExpUser.EditValue;
                        row["expWardID"] = treeListLookUpEditWard.EditValue;
                        row["docNumber"] = textEditDocNumber.EditValue;
                        row["description"] = memoEditDescription.EditValue;
                        if (radioGroupType.SelectedIndex == 0)
                        {
                            row["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                            row["wardID"] = null;
                            row["warehouseName"] = warehouse["name"];
                            row["warehouseDesc"] = warehouse["description"];
                        }
                        else if (radioGroupType.SelectedIndex == 0)
                        {
                            row["warehouseID"] = null;
                            row["wardID"] = treeListLookUpEditWard.EditValue;
                            row["warehouseName"] = ward["name"];
                            row["warehouseDesc"] = ward["description"];
                        }
                        if (comboBoxEditType.SelectedIndex == 0 || comboBoxEditType.SelectedIndex == 1)
                        {
                            row["expName"] = expUser["fullName"];
                            row["expDesc"] = expUser["roleName"];
                            row["expUserID"] = gridLookUpEditExpUser.EditValue;
                            row["expWarehouseID"] = null;
                            row["expWardID"] = null;
                        }
                        else if (comboBoxEditType.SelectedIndex == 2)
                        {
                            row["expName"] = expWarehouse["name"];
                            row["expDesc"] = expWarehouse["description"];
                            row["expUserID"] = null;
                            row["expWarehouseID"] = treeListLookUpEditExpWarehouse.EditValue;
                            row["expWardID"] = null;
                        }
                        else if (comboBoxEditType.SelectedIndex == 3)
                        {
                            row["expName"] = expWard["name"];
                            row["expDesc"] = expWard["description"];
                            row["expUserID"] = null;
                            row["expWarehouseID"] = null;
                            row["expWardID"] = treeListLookUpEditExpWard.EditValue;
                        }
                    }

                    JournalDataSet.ExpJournalBindingSource.EndEdit();
                    JournalDataSet.ExpJournalTableAdapter.Update(JournalDataSet.Instance.ExpJournal);

                    JournalDataSet.ExpDetailBindingSource.EndEdit();
                    JournalDataSet.ExpDetailTableAdapter.Update(JournalDataSet.Instance.ExpDetail);

                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }


            public bool existMedicine(string medicineID)
            {
                for (int i = 0; i < gridViewDetail.RowCount; i++)
                {
                    if (medicineID.Equals(gridViewDetail.GetDataRow(i)["medicinePrice"].ToString()))
                    {
                        return true;
                    }
                }
                return false;
            }

            public void addMedicine(DataRowView view, object incTypeID, string incTypeText, decimal count, decimal price)
            {
                DataRowView row = (DataRowView)JournalDataSet.MedicineBalBindingSource.AddNew();
                row["medicineID"] = view["id"];
                row["name"] = view["name"];
                row["latinName"] = view["latinName"];
                row["medicineTypeName"] = view["medicineTypeName"];
                row["unitType"] = view["unitType"];
                row["barcode"] = view["barcode"];
                row["shape"] = view["shape"];
                row["validDate"] = view["validDate"];
                row["incTypeID"] = incTypeID;
                row["incTypeName"] = incTypeText;
                row["price"] = price;
                row["count"] = count;
                row.EndEdit();

                gridLookUpEditMedicine.EditValue = view["id"].ToString() + incTypeID.ToString() + price;

                spinEditCount.Value = count;
                spinEditPrice.Value = price;
                endEdit();
            }

        #endregion

    }
}