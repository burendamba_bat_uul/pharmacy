﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.report.journal.exp;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using Pharmacy2016.report.journal.exp;
using Pharmacy2016.gui.report.ds;
using DevExpress.XtraGrid;



namespace Pharmacy2016.gui.core.journal.exp
{
    /*
     * Эмийн зарлагын журнал харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class MedicineExpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static MedicineExpForm INSTANCE = new MedicineExpForm();

        public static MedicineExpForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private readonly int FORM_ID = 2002;

        private bool isInit = false;

        public DataRowView currView;

        private BaseEdit inplaceEditor;

        private System.IO.Stream layout_exp = null;
        private System.IO.Stream layout_detail = null;

        private MedicineExpForm()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            this.MdiParent = MainForm.Instance;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;
            initLayout();
            RoleUtil.addForm(FORM_ID, this);

            pivotGridFieldExpTypeName.ColumnValueLineCount = 10;

            gridControlExp.DataSource = JournalDataSet.ExpJournalBindingSource;
            pivotGridControl.DataSource = JournalDataSet.ExpDetailBindingSource;

            dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
            dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

            DateTime date = DateTime.Now;
            dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1);
            dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }

        private void initLayout()
        {
            layout_exp = new System.IO.MemoryStream();
            gridViewExp.SaveLayoutToStream(layout_exp);
            layout_exp.Seek(0, System.IO.SeekOrigin.Begin);

            layout_detail = new System.IO.MemoryStream();
            gridViewDetail.SaveLayoutToStream(layout_detail);
            layout_detail.Seek(0, System.IO.SeekOrigin.Begin);
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm()
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }

                if (Visible)
                {
                    Select();
                }
                else
                {
                    radioGroupJournal.SelectedIndex = 0;
                    checkRole();

                    JournalDataSet.Instance.ExpJournal.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    JournalDataSet.Instance.ExpDetail.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    JournalDataSet.Instance.ExpJournal.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                    JournalDataSet.Instance.ExpDetail.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                    reload();

                    Show();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                clearData();
            }
            finally
            {
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void checkRole()
        {
            bool[] roles = UserUtil.getWindowRole(FORM_ID);

            gridControlExp.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
            gridControlExp.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
            gridControlExp.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
            //gridControlExp.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = roles[1];
            simpleButtonDownload.Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
            dropDownButtonPrint.Visible = roles[4];
            simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;

            checkLayout();
        }

        private void checkLayout()
        {
            string layout = UserUtil.getLayout("exp");
            if (layout != null)
            {
                gridViewExp.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewExp.RestoreLayoutFromStream(layout_exp);
                layout_exp.Seek(0, System.IO.SeekOrigin.Begin);
            }

            layout = UserUtil.getLayout("expDetail");
            if (layout != null)
            {
                gridViewDetail.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewDetail.RestoreLayoutFromStream(layout_detail);
                layout_detail.Seek(0, System.IO.SeekOrigin.Begin);
            }
        }

        private void MedicineExpForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            clearData();
            Hide();
        }

        private void clearData()
        {
            JournalDataSet.Instance.ExpDetail.Clear();
            JournalDataSet.Instance.ExpJournal.Clear();
        }

        #endregion

        #region Формын event

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            try
            {
                reload();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void simpleButtonDownload_Click(object sender, EventArgs e)
        {
            downloadOrder();
        }

        private void simpleButtonLayout_Click(object sender, EventArgs e)
        {
            saveLayout();
        }

        private void gridControlExp_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    addExp();
                }
                else if (String.Equals(e.Button.Tag, "delete") && gridViewExp.RowCount != 0 && gridViewExp.GetFocusedDataRow() != null)
                {
                    try
                    {
                        deleteExp();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
                else if (String.Equals(e.Button.Tag, "update") && gridViewExp.RowCount != 0 && gridViewExp.GetFocusedDataRow() != null)
                {
                    updateExp();
                }
                //else if (String.Equals(e.Button.Tag, "download"))
                //{
                //    downloadOrder();
                //}
            }
        }

        private void gridViewDetail_CustomDrawGroupRow(object sender, DevExpress.XtraGrid.Views.Base.RowObjectCustomDrawEventArgs e)
        {
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;
            info.GroupText = string.Format("{0} (Тоо хэмжээ = {1} Нийт үнэ = {2})", info.GroupValueText, gridViewDetail.GetGroupSummaryDisplayText(e.RowHandle, gridViewDetail.GroupSummary[0] as DevExpress.XtraGrid.GridGroupSummaryItem), gridViewDetail.GetGroupSummaryDisplayText(e.RowHandle, gridViewDetail.GroupSummary[1] as DevExpress.XtraGrid.GridGroupSummaryItem));
        }

        private void gridViewExpDetail_RowClick(object sender, RowClickEventArgs e)
        {
            GridView detailView = sender as GridView;
            gridViewExp.FocusedRowHandle = detailView.SourceRowHandle;
        }


        private void radioGroupJournal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioGroupJournal.SelectedIndex == 0)
            {
                JournalDataSet.ExpDetailBindingSource.DataSource = JournalDataSet.ExpJournalBindingSource;
                JournalDataSet.ExpDetailBindingSource.DataMember = "ExpJournal_ExpDetail";

                gridControlExp.DataSource = JournalDataSet.ExpJournalBindingSource;
                gridControlExp.MainView = gridViewExp;

                gridControlExp.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                gridControlExp.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                gridControlExp.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;
                xtraTabControl.SelectedTabPage = xtraTabPageExp;
                checkRole();
            }
            else if (radioGroupJournal.SelectedIndex == 1)
            {
                refreshData();
                JournalDataSet.ExpDetailBindingSource.DataSource = JournalDataSet.Instance;
                JournalDataSet.ExpDetailBindingSource.DataMember = "ExpDetail";

                gridControlExp.DataSource = JournalDataSet.ExpDetailBindingSource;
                gridControlExp.MainView = gridViewDetail;

                gridControlExp.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                gridControlExp.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                gridControlExp.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
                xtraTabControl.SelectedTabPage = xtraTabPageExp;
            }
            else if (radioGroupJournal.SelectedIndex == 2)
            {
                JournalDataSet.ExpDetailBindingSource.DataSource = JournalDataSet.Instance;
                JournalDataSet.ExpDetailBindingSource.DataMember = "ExpDetail";
                xtraTabControl.SelectedTabPage = xtraTabPageDetail;
            }
        }

        private void pivotGridControl_CustomDrawFieldValue(object sender, DevExpress.XtraPivotGrid.PivotCustomDrawFieldValueEventArgs e)
        {
            if (e.Field == pivotGridFieldExpTypeName)
            {
                string caption = ((DevExpress.Utils.Drawing.HeaderObjectInfoArgs)e.Info).Caption;
                ((DevExpress.Utils.Drawing.HeaderObjectInfoArgs)e.Info).Caption = string.Empty;
                e.Painter.DrawObject(e.Info);
                Rectangle rec = new Rectangle(e.Bounds.X, e.Bounds.Y + 10, e.Bounds.Width, e.Bounds.Height - 10);
                e.GraphicsCache.DrawVString(caption, e.Appearance.GetFont(), e.Appearance.GetForeBrush(e.GraphicsCache), rec, e.Appearance.GetStringFormat(), -90);
                e.Handled = true;
            }
        }

        private void gridViewDetail_DoubleClick(object sender, EventArgs e)
        {
            DataRowView view = (DataRowView)gridViewDetail.GetFocusedRow();
            if (view == null)
                return;


            radioGroupJournal.SelectedIndex = 0;
            for (int i = 0; i < gridViewExp.RowCount; i++)
            {
                DataRow row = gridViewExp.GetDataRow(i);
                if (view["expJournalID"].ToString().Equals(row["id"].ToString()))
                {
                    gridViewExp.SelectRow(i);
                    if (!gridViewExp.GetMasterRowExpanded(i))
                        gridViewExp.SetMasterRowExpanded(i, true);
                }
            }
        }

        private void gridViewExp_ShownEditor(object sender, EventArgs e)
        {
            inplaceEditor = ((GridView)sender).ActiveEditor;
            inplaceEditor.DoubleClick += new EventHandler(ActiveEditor_DoubleClick);
        }

        private void gridViewExpDetail_ShownEditor(object sender, EventArgs e)
        {
            inplaceEditor = ((GridView)sender).ActiveEditor;
            inplaceEditor.DoubleClick += new EventHandler(ActiveEditor_DoubleClick);
        }

        private void ActiveEditor_DoubleClick(object sender, System.EventArgs e)
        {
            BaseEdit editor = (BaseEdit)sender;
            GridControl grid = (GridControl)editor.Parent;
            Point pt = grid.PointToClient(Control.MousePosition);
            GridView view = (GridView)grid.FocusedView;
            DoRowDoubleClick(view, pt);
        }

        #endregion

        #region Формын функц

        private bool check()
        {
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();

            if (dateEditStart.Text == "")
            {
                errorText = "Эхлэх огноог оруулна уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }
            if (dateEditEnd.Text == "")
            {
                errorText = "Дуусах огноог оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditEnd.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    dateEditEnd.Focus();
                }
            }
            if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
            {
                errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }
            if (!isRight)
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        private void reload()
        {
            if (check())
            {
                string userid = "";
                // 2016-10-17 Зарлагын журналь хэрэглэгч зөвхөн өөрийн мэдээлэл хардаг болгож засав.
                if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    JournalDataSet.ExpJournalTableAdapter.Fill(JournalDataSet.Instance.ExpJournal, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                    JournalDataSet.ExpDetailTableAdapter.Fill(JournalDataSet.Instance.ExpDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                    ProgramUtil.closeWaitDialog();
                    userid = " and e.[userID] = '" + UserUtil.getUserId() + "'";
                }
                else if (UserUtil.getUserRoleId() == RoleUtil.ACCOUNTANT_ID)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    JournalDataSet.ExpJournalTableAdapter.Fill(JournalDataSet.Instance.ExpJournal, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    JournalDataSet.ExpDetailTableAdapter.Fill(JournalDataSet.Instance.ExpDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    ProgramUtil.closeWaitDialog();
                }
                else
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    JournalDataSet.ExpJournalTableAdapter.Fill(JournalDataSet.Instance.ExpJournal, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    JournalDataSet.ExpDetailTableAdapter.Fill(JournalDataSet.Instance.ExpDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    ProgramUtil.closeWaitDialog();
                }
                SqlConnection myConnection = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
                string oString = "SELECT ex.[expJournalID], format(sum(ex.[price] * ex.[count]),'N','en-us') as amount " +
                                 "FROM [ExpJournal] as e " +
                                 "inner join " +
                                 "[ExpDetail] as ex on e.[id] = ex.[expJournalID] " +
                                 "where e.[hospitalID] = '" + HospitalUtil.getHospitalId() + "' " +
                                 "and e.[date] between '" + dateEditStart.Text + "' and '" + dateEditEnd.Text + "' " + userid +
                                 "group by ex.[expJournalID]";
                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                myConnection.Open();
                Hashtable amountlist = new Hashtable();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        amountlist.Add(oReader["expJournalID"].ToString(), oReader["amount"].ToString());
                    }
                    myConnection.Close();
                }
                for (var i = 0; i < gridViewExp.RowCount; i++)
                {
                    gridViewExp.SetRowCellValue(i, bandedGridColumn41, amountlist[gridViewExp.GetRowCellValue(i, gridViewExp.Columns["id"]).ToString()]);
                }
            }
        }

        private void addExp()
        {
            MedicineExpUpForm.Instance.showForm(1, this);
        }

        private void deleteExp()
        {
            bool isDeleteDoc = false;
            GridView detail = null;
            if (gridViewExp.IsFocusedView)
            {
                isDeleteDoc = true;
            }
            else
            {
                detail = gridViewExp.GetDetailView(gridViewExp.FocusedRowHandle, 0) as GridView;
                if (detail.IsFocusedView)
                {
                    //isDeleteDoc = detail.RowCount == 1;

                }
                //if(detail.GetSelectedRows().Count() > 0){
                //    isDeleteDoc = true;
                //}
            }
            string date = gridViewExp.GetRowCellValue(gridViewExp.FocusedRowHandle, gridViewExp.Columns["date"]).ToString();
            if (ProgramUtil.checkLockMedicine(date)){
                if (isDeleteDoc)
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн зарлагын баримт устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {

                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);

                        gridViewExp.DeleteSelectedRows();
                        JournalDataSet.ExpJournalBindingSource.EndEdit();
                        JournalDataSet.ExpJournalTableAdapter.Update(JournalDataSet.Instance.ExpJournal);
                        ProgramUtil.closeWaitDialog();
                    }
                }
                else
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн зарлагын гүйлгээ устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        detail.DeleteSelectedRows();
                        JournalDataSet.ExpDetailBindingSource.EndEdit();
                        JournalDataSet.ExpDetailTableAdapter.Update(JournalDataSet.Instance.ExpDetail);
                        ProgramUtil.closeWaitDialog();
                    }

                }
            }
            else
            {
                XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
            }
        }

        private void updateExp()
        {
            if (!gridViewExp.GetMasterRowExpanded(gridViewExp.FocusedRowHandle))
                gridViewExp.SetMasterRowExpanded(gridViewExp.FocusedRowHandle, true);
            GridView detail = gridViewExp.GetDetailView(gridViewExp.FocusedRowHandle, 0) as GridView;
            if (detail.GetFocusedRow() != null)
            {
                MedicineExpUpForm.Instance.showForm(2, this);
            }
        }

        private void downloadOrder()
        {
            OrderDownForm.Instance.showForm(1, Instance);
        }

        private void saveLayout()
        {
            try
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                System.IO.Stream str = new System.IO.MemoryStream();
                System.IO.StreamReader reader = null;
                if (radioGroupJournal.SelectedIndex == 0)
                {
                    gridViewExp.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("exp", reader.ReadToEnd());
                }
                else
                {
                    gridViewDetail.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("expDetail", reader.ReadToEnd());
                }
                reader.Close();
                str.Close();

                ProgramUtil.closeWaitDialog();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        public DataRowView getSelectedRow()
        {
            GridView detail = gridViewExp.GetDetailView(gridViewExp.FocusedRowHandle, 0) as GridView;
            string id = detail.GetFocusedDataRow()["id"].ToString();
            for (int i = 0; i < JournalDataSet.ExpDetailBindingSource.Count; i++)
            {
                DataRowView view = (DataRowView)JournalDataSet.ExpDetailBindingSource[i];
                if (view["id"].ToString().Equals(id))
                    return view;
            }
            return null;
        }

        public DateTime getStartDate()
        {
            return dateEditStart.DateTime;
        }

        public DateTime getEndDate()
        {
            return dateEditEnd.DateTime;
        }

        public void refreshData()
        {
            gridViewDetail.RefreshData();
        }

        private void DoRowDoubleClick(GridView view, Point pt)
        {
            GridHitInfo info = view.CalcHitInfo(pt);
            if (info.InRow || info.InRowCell)
            {
                if (view.GetFocusedDataRow() != null)
                    updateExp();
            }
        }

        #endregion

        #region Хэвлэх, хөрвүүлэх функц

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItemPrint.Caption;
            dropDownButtonPrint.Image = barButtonItemPrint.Glyph;
            if (gridViewExp.GetFocusedRow() != null)
            {
                DataRow row = gridViewExp.GetFocusedDataRow();
                ExpDocReport.Instance.initAndShow(row["id"].ToString(), row["userName"].ToString(), row["expUserName"].ToString());
            }
            else
            {
                XtraMessageBox.Show("Мөр сонгоно уу");
            }
            //ExpReport.Instance.initAndShow(1, gridViewExp.ActiveFilterString);
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItem3.Caption;
            dropDownButtonPrint.Image = barButtonItem3.Glyph;

            ExpReport.Instance.initAndShow(1, gridViewExp.ActiveFilterString);
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MedicineChooseDoctor.Instance.showForm(1, Instance, dateEditStart.Text.ToString(), dateEditEnd.Text.ToString());
            //dropDownButtonPrint.Text = barButtonItem3.Caption;
            //dropDownButtonPrint.Image = barButtonItem3.Glyph;


        }

        private void barButtonItemBegBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItemExp.Caption;
            dropDownButtonPrint.Image = barButtonItemExp.Glyph;

            if (gridViewExp.GetFocusedRow() != null)
                ExpReport.Instance.initAndShow(2, "id = '" + gridViewExp.GetFocusedDataRow()["id"].ToString() + "'");
            else
                XtraMessageBox.Show("Мөр сонгоно уу");
        }

        private void barButtonItemCustomize_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ProgramUtil.closeAlertDialog();
            if (gridViewExp.RowCount == 0 && gridViewExp.GetFocusedDataRow() == null)
            {
                string errorText = "Хэвлэх мөр алга байна";
                string text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                return;
            }
            List<string[]> colList = new List<string[]>();
            DataRow row = gridViewExp.GetFocusedDataRow();
            foreach (DevExpress.XtraGrid.Columns.GridColumn cols in gridViewExp.Columns.Where(x => (x.FieldName != "warehouseDesc" && x.FieldName != "expTypeName" && x.FieldName != "id" && x.FieldName != "roleName" && x.FieldName != "userID" && x.FieldName != "expUserID" && x.FieldName != "expRoleName" && x.FieldName != "expWarehouseDesc" && x.FieldName != "expWarehouseName" && x.FieldName != "doctorName" && x.FieldName != "incTypeName" && x.FieldName != "expTypeText")))
            {
                colList.Add(new string[] { cols.FieldName, cols.CustomizationCaption });
            }
            foreach (DevExpress.XtraGrid.Columns.GridColumn cols in gridViewExpDetail.Columns.Where(x => (x.FieldName != "count" && x.FieldName != "price" && x.FieldName != "sumPrice" && x.FieldName != "incTypeName")))
            {
                colList.Add(new string[] { cols.FieldName, cols.Caption });
            }

            ReportUpForm.Instance.showForm(colList, MedicineExpForm.Instance, row, dateEditStart.DateTime, dateEditEnd.DateTime, "");
            dropDownButtonPrint.Text = barButtonItemCustomize.Caption;
            dropDownButtonPrint.Image = barButtonItemCustomize.Glyph;
        }

        private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItemConvert.Caption;
            dropDownButtonPrint.Image = barButtonItemConvert.Glyph;
            if (radioGroupJournal.SelectedIndex == 0)
                ProgramUtil.convertGrid(gridViewExp, "Зарлагын журнал");
            else if (radioGroupJournal.SelectedIndex == 1)
                ProgramUtil.convertGrid(gridViewDetail, "Зарлагын гүйлгээ");
            else if (radioGroupJournal.SelectedIndex == 2)
                ProgramUtil.convertGrid(pivotGridControl, "Зарлагын дэлгэрэнгүй");
        }

        #endregion

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItem2.Caption;
            dropDownButtonPrint.Image = barButtonItem2.Glyph;

            if (gridViewExp.GetFocusedRow() != null)
            {
                string[] val = Convert.ToDateTime(gridViewExp.GetFocusedDataRow()["date"]).AddDays(5).ToString().Split(' ');
                ReportBillingForm.Instance.showForm(1, gridViewExp.GetFocusedDataRow()["userName"].ToString(), "id = '" + gridViewExp.GetFocusedDataRow()["id"].ToString() + "'", val[0]);
            }
            else
                XtraMessageBox.Show("Мөр сонгоно уу");
        }

        private void gridControlExp_Click(object sender, EventArgs e)
        {

        }

        private void dropDownButtonPrint_Click(object sender, EventArgs e)
        {

        }

        private void MedicineExpForm_Load(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }







    }
}