﻿namespace Pharmacy2016.gui.core.journal.exp
{
    partial class OrderDownForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderDownForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridViewOrderDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnConfirm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMedicine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnGiveCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnEndCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnConfirmMedicine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlOrder = new DevExpress.XtraGrid.GridControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditOrder = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditExpDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.treeListLookUpEditWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListLookUpEditWarehouse = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControlWarehouse = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupType = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditChooseAll = new DevExpress.XtraEditors.CheckEdit();
            this.gridLookUpEditUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDownload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOrderDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChooseAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridViewOrderDetail
            // 
            this.gridViewOrderDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumnConfirm,
            this.gridColumnMedicine,
            this.gridColumnGiveCount,
            this.gridColumnPrice,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumnDesc,
            this.gridColumnEndCount,
            this.gridColumnConfirmMedicine});
            this.gridViewOrderDetail.GridControl = this.gridControlOrder;
            this.gridViewOrderDetail.LevelIndent = 1;
            this.gridViewOrderDetail.Name = "gridViewOrderDetail";
            this.gridViewOrderDetail.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewOrderDetail.OptionsView.ShowFooter = true;
            this.gridViewOrderDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewOrderDetail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewOrderDetail_CellValueChanged);
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Захиалсан эмийн багц";
            this.gridColumn4.CustomizationCaption = "Захиалсан эмийн багц";
            this.gridColumn4.FieldName = "medicineName";
            this.gridColumn4.MinWidth = 120;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 205;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Захиалсан тоо";
            this.gridColumn5.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumn5.CustomizationCaption = "Захиалсан тоо";
            this.gridColumn5.FieldName = "count";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 96;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnConfirm
            // 
            this.gridColumnConfirm.Caption = "Баталсан тоо";
            this.gridColumnConfirm.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnConfirm.CustomizationCaption = "Баталсан тоо";
            this.gridColumnConfirm.FieldName = "confirmCount";
            this.gridColumnConfirm.MinWidth = 75;
            this.gridColumnConfirm.Name = "gridColumnConfirm";
            this.gridColumnConfirm.OptionsColumn.ReadOnly = true;
            this.gridColumnConfirm.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "confirmCount", "{0:n2}")});
            this.gridColumnConfirm.Visible = true;
            this.gridColumnConfirm.VisibleIndex = 3;
            this.gridColumnConfirm.Width = 96;
            // 
            // gridColumnMedicine
            // 
            this.gridColumnMedicine.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnMedicine.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnMedicine.Caption = "Олгосон эм";
            this.gridColumnMedicine.ColumnEdit = this.repositoryItemGridLookUpEdit2;
            this.gridColumnMedicine.CustomizationCaption = "Олгосон эм";
            this.gridColumnMedicine.FieldName = "medicinePrice";
            this.gridColumnMedicine.MinWidth = 75;
            this.gridColumnMedicine.Name = "gridColumnMedicine";
            this.gridColumnMedicine.Visible = true;
            this.gridColumnMedicine.VisibleIndex = 4;
            this.gridColumnMedicine.Width = 231;
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.ActionButtonIndex = 1;
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.DisplayMember = "name";
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.PopupFormSize = new System.Drawing.Size(500, 300);
            this.repositoryItemGridLookUpEdit2.ValueMember = "medicinePrice";
            this.repositoryItemGridLookUpEdit2.View = this.repositoryItemGridLookUpEdit2View;
            this.repositoryItemGridLookUpEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemGridLookUpEdit2_ButtonClick);
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn21,
            this.gridColumn2,
            this.gridColumn28,
            this.gridColumn3,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn27,
            this.gridColumn22,
            this.gridColumn26,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn1});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowAutoFilterRow = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Нэр";
            this.gridColumn21.CustomizationCaption = "Нэр";
            this.gridColumn21.FieldName = "name";
            this.gridColumn21.MinWidth = 120;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 0;
            this.gridColumn21.Width = 120;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Үлдэгдэл";
            this.gridColumn2.CustomizationCaption = "Үлдэгдэл";
            this.gridColumn2.DisplayFormat.FormatString = "n2";
            this.gridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn2.FieldName = "count";
            this.gridColumn2.MinWidth = 100;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 100;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Үнэ";
            this.gridColumn28.CustomizationCaption = "Үнэ";
            this.gridColumn28.DisplayFormat.FormatString = "c2";
            this.gridColumn28.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn28.FieldName = "price";
            this.gridColumn28.MinWidth = 75;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 2;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.gridColumn3.AppearanceCell.Options.UseBackColor = true;
            this.gridColumn3.Caption = "Эмийн багц";
            this.gridColumn3.CustomizationCaption = "Эмийн багц";
            this.gridColumn3.FieldName = "packageName";
            this.gridColumn3.MinWidth = 100;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            this.gridColumn3.Width = 100;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Тун хэмжээ";
            this.gridColumn23.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn23.FieldName = "unitType";
            this.gridColumn23.MinWidth = 100;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 4;
            this.gridColumn23.Width = 100;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Хэлбэр";
            this.gridColumn24.CustomizationCaption = "Хэлбэр";
            this.gridColumn24.FieldName = "shape";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 5;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Хэмжих нэгж";
            this.gridColumn25.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn25.FieldName = "quantity";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 6;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Хүчинтэй хугацаа";
            this.gridColumn27.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn27.FieldName = "validDate";
            this.gridColumn27.MinWidth = 75;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 7;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "ОУ нэр";
            this.gridColumn22.CustomizationCaption = "ОУ нэр";
            this.gridColumn22.FieldName = "latinName";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 8;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Ангилал";
            this.gridColumn26.CustomizationCaption = "Ангилал";
            this.gridColumn26.FieldName = "medicineTypeName";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 9;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Тоо хэмжээ";
            this.gridColumn29.CustomizationCaption = "Тоо хэмжээ";
            this.gridColumn29.FieldName = "count";
            this.gridColumn29.MinWidth = 75;
            this.gridColumn29.Name = "gridColumn29";
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Баркод";
            this.gridColumn30.CustomizationCaption = "Баркод";
            this.gridColumn30.FieldName = "barcode";
            this.gridColumn30.MinWidth = 75;
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 11;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Сериал";
            this.gridColumn1.CustomizationCaption = "Сериал";
            this.gridColumn1.FieldName = "serial";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 10;
            // 
            // gridColumnGiveCount
            // 
            this.gridColumnGiveCount.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnGiveCount.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnGiveCount.Caption = "Олгосон тоо";
            this.gridColumnGiveCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnGiveCount.CustomizationCaption = "Олгосон тоо";
            this.gridColumnGiveCount.FieldName = "giveCount";
            this.gridColumnGiveCount.MinWidth = 75;
            this.gridColumnGiveCount.Name = "gridColumnGiveCount";
            this.gridColumnGiveCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "giveCount", "{0:n2}")});
            this.gridColumnGiveCount.Visible = true;
            this.gridColumnGiveCount.VisibleIndex = 5;
            this.gridColumnGiveCount.Width = 123;
            // 
            // gridColumnPrice
            // 
            this.gridColumnPrice.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnPrice.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnPrice.Caption = "Үнэ";
            this.gridColumnPrice.ColumnEdit = this.repositoryItemSpinEdit2;
            this.gridColumnPrice.FieldName = "price";
            this.gridColumnPrice.MinWidth = 75;
            this.gridColumnPrice.Name = "gridColumnPrice";
            this.gridColumnPrice.Visible = true;
            this.gridColumnPrice.VisibleIndex = 6;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.DisplayFormat.FormatString = "c2";
            this.repositoryItemSpinEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.EditFormat.FormatString = "c2";
            this.repositoryItemSpinEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.Mask.EditMask = "c2";
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "ОУ нэр";
            this.gridColumn36.CustomizationCaption = "ОУ нэр";
            this.gridColumn36.FieldName = "latinName";
            this.gridColumn36.MinWidth = 75;
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 180;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Тун хэмжээ";
            this.gridColumn37.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn37.FieldName = "unitType";
            this.gridColumn37.MinWidth = 80;
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 1;
            this.gridColumn37.Width = 80;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Хэлбэр";
            this.gridColumn38.CustomizationCaption = "Хэлбэр";
            this.gridColumn38.FieldName = "shape";
            this.gridColumn38.MinWidth = 75;
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Хэмжих нэгж";
            this.gridColumn39.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn39.FieldName = "quantity";
            this.gridColumn39.MinWidth = 75;
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            // 
            // gridColumnDesc
            // 
            this.gridColumnDesc.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnDesc.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnDesc.Caption = "Тайлбар";
            this.gridColumnDesc.FieldName = "description";
            this.gridColumnDesc.MinWidth = 100;
            this.gridColumnDesc.Name = "gridColumnDesc";
            this.gridColumnDesc.Visible = true;
            this.gridColumnDesc.VisibleIndex = 7;
            this.gridColumnDesc.Width = 100;
            // 
            // gridColumnEndCount
            // 
            this.gridColumnEndCount.Caption = "Үлдэгдэл";
            this.gridColumnEndCount.CustomizationCaption = "Үлдэгдэл";
            this.gridColumnEndCount.FieldName = "endCount";
            this.gridColumnEndCount.Name = "gridColumnEndCount";
            // 
            // gridColumnConfirmMedicine
            // 
            this.gridColumnConfirmMedicine.Caption = "Өгөх эм";
            this.gridColumnConfirmMedicine.CustomizationCaption = "Өгөх эм";
            this.gridColumnConfirmMedicine.FieldName = "confirmMedicineID";
            this.gridColumnConfirmMedicine.Name = "gridColumnConfirmMedicine";
            // 
            // gridControlOrder
            // 
            this.gridControlOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlOrder.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlOrder.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlOrder.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlOrder.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlOrder.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlOrder.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Эм орлуулах", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete")});
            this.gridControlOrder.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlOrder_EmbeddedNavigator_ButtonClick);
            this.gridControlOrder.Location = new System.Drawing.Point(0, 201);
            this.gridControlOrder.MainView = this.gridViewOrderDetail;
            this.gridControlOrder.Name = "gridControlOrder";
            this.gridControlOrder.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemGridLookUpEdit2,
            this.repositoryItemSpinEdit2});
            this.gridControlOrder.Size = new System.Drawing.Size(767, 260);
            this.gridControlOrder.TabIndex = 10;
            this.gridControlOrder.UseEmbeddedNavigator = true;
            this.gridControlOrder.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOrderDetail});
            this.gridControlOrder.Click += new System.EventHandler(this.gridControlOrder_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.gridLookUpEditOrder);
            this.panelControl1.Controls.Add(this.dateEditExpDate);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.treeListLookUpEditWard);
            this.panelControl1.Controls.Add(this.treeListLookUpEditWarehouse);
            this.panelControl1.Controls.Add(this.labelControlWarehouse);
            this.panelControl1.Controls.Add(this.labelControl14);
            this.panelControl1.Controls.Add(this.radioGroupType);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.checkEditChooseAll);
            this.panelControl1.Controls.Add(this.gridLookUpEditUser);
            this.panelControl1.Controls.Add(this.dateEditEnd);
            this.panelControl1.Controls.Add(this.dateEditStart);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(767, 201);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(180, 173);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(104, 13);
            this.labelControl5.TabIndex = 55;
            this.labelControl5.Text = "Захиалгын хуудас*:";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(216, 147);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(68, 13);
            this.labelControl4.TabIndex = 54;
            this.labelControl4.Text = "Шүүх огноо*:";
            // 
            // gridLookUpEditOrder
            // 
            this.gridLookUpEditOrder.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditOrder.EditValue = "";
            this.gridLookUpEditOrder.Location = new System.Drawing.Point(290, 170);
            this.gridLookUpEditOrder.Name = "gridLookUpEditOrder";
            this.gridLookUpEditOrder.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditOrder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Захиалгын хуудас шүүх", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("gridLookUpEditOrder.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Захиалгын хуудас шүүх", "reload", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditOrder.Properties.DisplayMember = "userName";
            this.gridLookUpEditOrder.Properties.NullText = "";
            this.gridLookUpEditOrder.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditOrder.Properties.ValueMember = "id";
            this.gridLookUpEditOrder.Properties.View = this.gridView1;
            this.gridLookUpEditOrder.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditOrder_Properties_ButtonClick);
            this.gridLookUpEditOrder.Size = new System.Drawing.Size(200, 22);
            this.gridLookUpEditOrder.TabIndex = 5;
            this.gridLookUpEditOrder.EditValueChanged += new System.EventHandler(this.gridLookUpEditOrder_EditValueChanged);
            this.gridLookUpEditOrder.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.gridLookUpEditOrder_CustomDisplayText);
            this.gridLookUpEditOrder.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditOrder_KeyUp);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn32,
            this.gridColumn31,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Огноо";
            this.gridColumn32.CustomizationCaption = "Огноо";
            this.gridColumn32.FieldName = "date";
            this.gridColumn32.MinWidth = 75;
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 0;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Дугаар";
            this.gridColumn31.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn31.FieldName = "userID";
            this.gridColumn31.MinWidth = 75;
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 2;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Нярав";
            this.gridColumn33.CustomizationCaption = "Нярав";
            this.gridColumn33.FieldName = "userName";
            this.gridColumn33.MinWidth = 75;
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 3;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Агуулах";
            this.gridColumn34.CustomizationCaption = "Агуулах";
            this.gridColumn34.FieldName = "warehouseName";
            this.gridColumn34.MinWidth = 75;
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 4;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Тайлбар";
            this.gridColumn35.CustomizationCaption = "Тайлбар";
            this.gridColumn35.FieldName = "description";
            this.gridColumn35.MinWidth = 75;
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 1;
            // 
            // dateEditExpDate
            // 
            this.dateEditExpDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditExpDate.EditValue = null;
            this.dateEditExpDate.Location = new System.Drawing.Point(290, 38);
            this.dateEditExpDate.Name = "dateEditExpDate";
            this.dateEditExpDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditExpDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditExpDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditExpDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditExpDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditExpDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditExpDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditExpDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditExpDate.TabIndex = 1;
            this.dateEditExpDate.EditValueChanged += new System.EventHandler(this.dateEditExpDate_EditValueChanged);
            this.dateEditExpDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditExpDate_KeyUp);
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(243, 41);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(41, 13);
            this.labelControl3.TabIndex = 52;
            this.labelControl3.Text = "Огноо*:";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(243, 67);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(41, 13);
            this.labelControl2.TabIndex = 50;
            this.labelControl2.Text = "Нярав*:";
            // 
            // treeListLookUpEditWard
            // 
            this.treeListLookUpEditWard.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWard.Location = new System.Drawing.Point(290, 12);
            this.treeListLookUpEditWard.Name = "treeListLookUpEditWard";
            this.treeListLookUpEditWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditWard.Properties.NullText = "";
            this.treeListLookUpEditWard.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditWard.Properties.ValueMember = "id";
            this.treeListLookUpEditWard.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWard.TabIndex = 4;
            this.treeListLookUpEditWard.Visible = false;
            this.treeListLookUpEditWard.EditValueChanged += new System.EventHandler(this.dateEditExpDate_EditValueChanged);
            this.treeListLookUpEditWard.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWard_KeyUp);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(117, 135);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Нэр";
            this.treeListColumn2.FieldName = "name";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // treeListLookUpEditWarehouse
            // 
            this.treeListLookUpEditWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWarehouse.EditValue = "0";
            this.treeListLookUpEditWarehouse.Location = new System.Drawing.Point(290, 118);
            this.treeListLookUpEditWarehouse.Name = "treeListLookUpEditWarehouse";
            this.treeListLookUpEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWarehouse.Properties.DisplayMember = "name";
            this.treeListLookUpEditWarehouse.Properties.NullText = "";
            this.treeListLookUpEditWarehouse.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEditWarehouse.Properties.ValueMember = "id";
            this.treeListLookUpEditWarehouse.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWarehouse.TabIndex = 4;
            this.treeListLookUpEditWarehouse.EditValueChanged += new System.EventHandler(this.dateEditExpDate_EditValueChanged);
            this.treeListLookUpEditWarehouse.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWarehouse_KeyUp);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1,
            this.treeListColumn3});
            this.treeListLookUpEdit1TreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListLookUpEdit1TreeList.KeyFieldName = "id";
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(145, 6);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsBehavior.PopulateServiceColumns = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.ParentFieldName = "pid";
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Нэр";
            this.treeListColumn1.CustomizationCaption = "Агуулахын нэр";
            this.treeListColumn1.FieldName = "name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraTreeList.FilterPopupMode.List;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 400;
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "Дугаар";
            this.treeListColumn3.CustomizationCaption = "Агуулахын дугаар";
            this.treeListColumn3.FieldName = "number";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 1;
            this.treeListColumn3.Width = 270;
            // 
            // labelControlWarehouse
            // 
            this.labelControlWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlWarehouse.Location = new System.Drawing.Point(246, 121);
            this.labelControlWarehouse.Name = "labelControlWarehouse";
            this.labelControlWarehouse.Size = new System.Drawing.Size(38, 13);
            this.labelControlWarehouse.TabIndex = 47;
            this.labelControlWarehouse.Text = "Тасаг*:";
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl14.Location = new System.Drawing.Point(185, 94);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(99, 13);
            this.labelControl14.TabIndex = 46;
            this.labelControl14.Text = "Агуулахын төрөл*:";
            // 
            // radioGroupType
            // 
            this.radioGroupType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupType.EditValue = 0;
            this.radioGroupType.EnterMoveNextControl = true;
            this.radioGroupType.Location = new System.Drawing.Point(290, 90);
            this.radioGroupType.Name = "radioGroupType";
            this.radioGroupType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Эмийн сан"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Тасаг")});
            this.radioGroupType.Size = new System.Drawing.Size(200, 22);
            this.radioGroupType.TabIndex = 3;
            this.radioGroupType.SelectedIndexChanged += new System.EventHandler(this.radioGroupType_SelectedIndexChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelControl1.Location = new System.Drawing.Point(650, 173);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(79, 13);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Бүгдийг олгох :";
            // 
            // checkEditChooseAll
            // 
            this.checkEditChooseAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChooseAll.Location = new System.Drawing.Point(735, 170);
            this.checkEditChooseAll.Name = "checkEditChooseAll";
            this.checkEditChooseAll.Properties.Caption = "";
            this.checkEditChooseAll.Size = new System.Drawing.Size(20, 19);
            this.checkEditChooseAll.TabIndex = 8;
            this.checkEditChooseAll.CheckedChanged += new System.EventHandler(this.checkEditChooseAll_CheckedChanged);
            // 
            // gridLookUpEditUser
            // 
            this.gridLookUpEditUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditUser.EditValue = "";
            this.gridLookUpEditUser.Location = new System.Drawing.Point(290, 64);
            this.gridLookUpEditUser.Name = "gridLookUpEditUser";
            this.gridLookUpEditUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditUser.Properties.NullText = "";
            this.gridLookUpEditUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditUser.Properties.ValueMember = "id";
            this.gridLookUpEditUser.Properties.View = this.gridView2;
            this.gridLookUpEditUser.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditUser.TabIndex = 2;
            this.gridLookUpEditUser.EditValueChanged += new System.EventHandler(this.gridLookUpEditUser_EditValueChanged);
            this.gridLookUpEditUser.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditUser_KeyUp);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Дугаар";
            this.gridColumn15.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn15.FieldName = "id";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Овог";
            this.gridColumn16.CustomizationCaption = "Овог";
            this.gridColumn16.FieldName = "lastName";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 2;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Нэр";
            this.gridColumn17.CustomizationCaption = "Нэр";
            this.gridColumn17.FieldName = "firstName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 1;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Эрх";
            this.gridColumn18.CustomizationCaption = "Эрх";
            this.gridColumn18.FieldName = "roleName";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 3;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Тасаг";
            this.gridColumn19.CustomizationCaption = "Тасаг";
            this.gridColumn19.FieldName = "wardName";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 4;
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(395, 144);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(95, 20);
            this.dateEditEnd.TabIndex = 7;
            this.dateEditEnd.ToolTip = "Дуусах огноо";
            this.dateEditEnd.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditEnd_KeyUp);
            // 
            // dateEditStart
            // 
            this.dateEditStart.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(290, 144);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(95, 20);
            this.dateEditStart.TabIndex = 6;
            this.dateEditStart.ToolTip = "Эхлэх огноо";
            this.dateEditStart.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditStart_KeyUp);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Controls.Add(this.simpleButtonDownload);
            this.panelControl2.Controls.Add(this.simpleButtonCancel);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 461);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(767, 50);
            this.panelControl2.TabIndex = 1;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 15);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 23;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // simpleButtonDownload
            // 
            this.simpleButtonDownload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonDownload.Location = new System.Drawing.Point(629, 15);
            this.simpleButtonDownload.Name = "simpleButtonDownload";
            this.simpleButtonDownload.Size = new System.Drawing.Size(60, 23);
            this.simpleButtonDownload.TabIndex = 21;
            this.simpleButtonDownload.Text = "Татах";
            this.simpleButtonDownload.ToolTip = "Захиалгын хуудас татах";
            this.simpleButtonDownload.Click += new System.EventHandler(this.simpleButtonDownload_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(695, 15);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(60, 23);
            this.simpleButtonCancel.TabIndex = 22;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // OrderDownForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(767, 511);
            this.Controls.Add(this.gridControlOrder);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrderDownForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Захиалгын хуудас сонгох";
            this.Shown += new System.EventHandler(this.OrderDownForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOrderDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditExpDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChooseAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDownload;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraGrid.GridControl gridControlOrder;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewOrderDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMedicine;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGiveCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConfirm;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit checkEditChooseAll;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.RadioGroup radioGroupType;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWarehouse;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private DevExpress.XtraEditors.LabelControl labelControlWarehouse;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWard;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditOrder;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraEditors.DateEdit dateEditExpDate;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDesc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEndCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConfirmMedicine;
        
    }
}