﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using DevExpress.XtraGrid.Views.Base;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.info.medicine;
using Pharmacy2016.gui.core.info.conf;



namespace Pharmacy2016.gui.core.journal.order
{
    /*
     * Эмийн захиалга нэмэх, засах цонх.
     * 
     */

    public partial class MedicineOrderUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static MedicineOrderUpForm INSTANCE = new MedicineOrderUpForm();

        public static MedicineOrderUpForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private int actionType;

        private DataRowView docView;

        private DataRowView currView;

        private int[] rowID;

        private MedicineOrderUpForm()
        {

        }


        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            spinEditCount.Properties.MaxValue = Decimal.MaxValue - 1;

            dateEditOrderDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditOrderDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

            initError();
            initBinding();
            initDataSource();
            reload(false);
        }

        private void initDataSource()
        {
            //gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
            gridLookUpEditUser.Properties.DataSource = JournalDataSet.SystemUserOrderBindingSource;
            treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
            treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
            //gridLookUpEditExpUser.Properties.DataSource = InformationDataSet.SystemUserOtherBindingSource;
            gridLookUpEditExpUser.Properties.DataSource = JournalDataSet.Instance.SystemUserOrderOther;
            gridLookUpEditTender.Properties.DataSource = JournalDataSet.Instance.Tender;
            gridLookUpEditProducer.Properties.DataSource = UserDataSet.SupplierBindingSource;
            //gridControlMed.DataSource = JournalDataSet.MedicineOtherBindingSource;
            gridControlMed.DataSource = UserDataSet.PackageMedicineBindingSource;
            popupContainerControlMed.Controls.Add(gridControlMed);
            popupContainerEditMedicineBal.Properties.PopupControl = popupContainerControlMed;
        }

        private void initError()
        {
            dateEditOrderDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            comboBoxEditOrderType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditExpUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditTender.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            popupContainerEditMedicineBal.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm(int type, XtraForm form)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }

                actionType = type;
                if (actionType == 1)
                {
                    insert();
                }
                else if (actionType == 2)
                {
                    update();
                }

                ShowDialog(form);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clearData();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void initBinding()
        {
            textEditID.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "id", true));
            dateEditOrderDate.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "date", true));
            //comboBoxEditOrderType.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "orderType", true));
            gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "userID", true));
            radioGroupType.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "type", true));
            treeListLookUpEditWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "warehouseID", true));
            treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "wardID", true));
            gridLookUpEditExpUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "expUserID", true));
            gridLookUpEditTender.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "tenderID", true));
            memoEditDescription.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "description", true));
        }

        private void insert()
        {
            //this.Text = "Захиалгын хуудас нэмэх цонх";
            string id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            JournalDataSet.Instance.OrderGoods.idColumn.DefaultValue = id;
            JournalDataSet.Instance.OrderGoodsDetail.orderGoodsIDColumn.DefaultValue = id;

            docView = (DataRowView)JournalDataSet.OrderGoodsBindingSource.AddNew();
            DateTime now = DateTime.Now;
            docView["id"] = id;
            docView["date"] = now;

            dateEditOrderDate.DateTime = now;
            textEditID.Text = id;
            comboBoxEditOrderType.SelectedIndex = 0;
            checkEditMultiSelection.Visible = true;
            addRow();
        }

        private void update()
        {
            checkEditMultiSelection.Checked = false;
            gridLookUpEditMedicine.Visible = true;
            popupContainerEditMedicineBal.Visible = false;
            popupContainerEditMedicineBal.Location = new Point(287, 347);
            checkEditMultiSelection.Visible = false;
            //this.Text = "Захиалгын хуудас засах цонх";
            docView = (DataRowView)JournalDataSet.OrderGoodsBindingSource.Current;
            currView = MedicineOrderForm.Instance.getSelectedRow();
            JournalDataSet.Instance.OrderGoodsDetail.orderGoodsIDColumn.DefaultValue = docView["id"];

            comboBoxEditOrderType.SelectedIndex = Convert.ToInt32(docView["orderType"]);
            gridLookUpEditMedicine.EditValue = currView["medicineID"];
            spinEditCount.EditValue = currView["count"];
            labelControlQuantity.Text = currView["quantity"].ToString();

        }

        private void addRow()
        {
            JournalDataSet.Instance.OrderGoodsDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            currView = (DataRowView)JournalDataSet.OrderGoodsDetailBindingSource.AddNew();
            clearMedicine();
        }


        private void clearData()
        {
            clearErrorText();
            clearMedicine();
            //clearBinding();

            checkEditMultiSelection.Checked = false;
            gridViewMed.ClearSelection();

            JournalDataSet.SystemUserOrderBindingSource.Filter = "";
            JournalDataSet.WarehouseOtherBindingSource.Filter = "";
            JournalDataSet.WardOtherBindingSource.Filter = "";

            JournalDataSet.OrderGoodsBindingSource.CancelEdit();
            JournalDataSet.OrderGoodsDetailBindingSource.CancelEdit();
            JournalDataSet.Instance.OrderGoods.RejectChanges();
            JournalDataSet.Instance.OrderGoodsDetail.RejectChanges();
        }

        private void clearBinding()
        {
            textEditID.DataBindings.Clear();
            dateEditOrderDate.DataBindings.Clear();
            //comboBoxEditOrderType.DataBindings.Clear();
            gridLookUpEditUser.DataBindings.Clear();
            radioGroupType.DataBindings.Clear();
            treeListLookUpEditWarehouse.DataBindings.Clear();
            treeListLookUpEditWard.DataBindings.Clear();
            gridLookUpEditExpUser.DataBindings.Clear();
            gridLookUpEditTender.DataBindings.Clear();
            memoEditDescription.DataBindings.Clear();
        }

        private void clearErrorText()
        {
            dateEditOrderDate.ErrorText = "";
            gridLookUpEditUser.ErrorText = "";
            treeListLookUpEditWarehouse.ErrorText = "";
            treeListLookUpEditWard.ErrorText = "";
            comboBoxEditOrderType.ErrorText = "";
            gridLookUpEditExpUser.ErrorText = "";
            gridLookUpEditTender.ErrorText = "";
            gridLookUpEditMedicine.ErrorText = "";
            spinEditCount.ErrorText = "";
            popupContainerEditMedicineBal.ErrorText = "";
        }

        private void clearMedicine()
        {
            gridLookUpEditMedicine.EditValue = null;
            spinEditCount.Value = 0;
        }

        private void MedicineOrderInUpForm_Shown(object sender, EventArgs e)
        {
            if (UserUtil.getUserRoleId() != RoleUtil.ADMIN_ID)
                JournalDataSet.SystemUserOrderBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";

            if (actionType == 1)
            {
                if (UserUtil.getUserRoleId() != RoleUtil.ADMIN_ID)
                {
                    gridLookUpEditUser.EditValue = UserUtil.getUserId();
                    docView["userID"] = UserUtil.getUserId();
                    treeListLookUpEditWarehouse.Focus();
                }
                else
                {
                    gridLookUpEditUser.Focus();
                }
                comboBoxEditOrderType.SelectedIndex = 0;
            }
            else if (actionType == 2)
            {
                gridLookUpEditMedicine.Focus();
                comboBoxEditOrderType.SelectedIndex = Convert.ToInt32(docView["orderType"]);
            }

            ProgramUtil.IsShowPopup = true;
            ProgramUtil.IsSaveEvent = false;
            reloadTender(true);
            changeType();
            changeOrderType();
            if (Visible && gridLookUpEditTender.EditValue != DBNull.Value && gridLookUpEditTender.EditValue != null && !gridLookUpEditTender.Equals(""))
            {
                changeTender();
            }
        }

        #endregion

        #region Формын event

        private void dateEditOrderDate_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    dateEditOrderDate.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditUser.Focus();
                }
            }
            reloadTender(true);
        }

        private void gridLookUpEditUser_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditUser.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    radioGroupType.Focus();
                    //if (radioGroupType.SelectedIndex == 0)
                    //    treeListLookUpEditWarehouse.Focus();
                    //else
                    //    treeListLookUpEditWard.Focus();
                }
            }
        }

        private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible && gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null)
            {
                JournalDataSet.WarehouseOtherBindingSource.Filter = UserUtil.getUserWarehouse(gridLookUpEditUser.EditValue.ToString());
                JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(gridLookUpEditUser.EditValue.ToString());
            }
        }


        private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeType();
        }

        private void treeListLookUpEditWarehouse_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    ((TreeListLookUpEdit)sender).ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    comboBoxEditOrderType.Focus();
                    //if(radioGroupOrderType.SelectedIndex == 0)
                    //    gridLookUpEditExpUser.Focus();
                    //else if (radioGroupOrderType.SelectedIndex == 1)
                    //    gridLookUpEditTender.Focus();
                }
            }
        }


        private void comboBoxEditOrderType_EditValueChanged(object sender, EventArgs e)
        {
            if (comboBoxEditOrderType.EditValue != DBNull.Value && comboBoxEditOrderType.EditValue != null)
            {
                changeOrderType();
            }
        }

        private void comboBoxEditOrderType_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    comboBoxEditOrderType.ShowPopup();
                }
                else
                {
                    if (comboBoxEditOrderType.EditValue != DBNull.Value && comboBoxEditOrderType.EditValue != null)
                    {
                        ProgramUtil.IsShowPopup = true;
                        int value = comboBoxEditOrderType.SelectedIndex;
                        if (value == 0)
                        {
                            gridLookUpEditExpUser.Focus();
                        }
                        else if (value == 1)
                        {
                            gridLookUpEditTender.Focus();
                        }
                        else if (value == 2)
                        {
                            gridLookUpEditProducer.Focus();
                        }
                    }
                }
            }
        }


        private void gridLookUpEditTender_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    ((GridLookUpEdit)sender).ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    memoEditDescription.Focus();
                }
            }
        }

        private void gridLookUpEditTender_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible && gridLookUpEditTender.EditValue != DBNull.Value && gridLookUpEditTender.EditValue != null && !gridLookUpEditTender.Equals(""))
            {
                changeTender();
            }
        }

        private void gridLookUpEditProducer_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "add"))
            {
                SupplierInForm.Instance.showForm(5, Instance);
            }
        }


        private void gridLookUpEditMedicine_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    if (!ProgramUtil.IsSaveEvent)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditMedicine.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsSaveEvent = false;
                    }
                }
                else
                {
                    spinEditCount.Focus();
                }
            }
        }


        private void simpleButtonNewDoc_Click(object sender, EventArgs e)
        {
            clearData();
            insert();
            dateEditOrderDate.Focus();
        }

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            reload(true);
            reloadTender(true);
        }

        private void simpleButtonSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void gridLookUpEditMedicine_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "add") && (comboBoxEditOrderType.SelectedIndex == 0 || comboBoxEditOrderType.SelectedIndex == 2))
            {
                //MedicineInUpForm.Instance.showForm(3, Instance);
                PackageMedicine.Instance.showForm(3, Instance);
                Activate();
            }
        }

        #endregion

        #region Формын функц

        private void reload(bool isReload)
        {
            //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
            //InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
            //InformationDataSet.SystemUserOtherTableAdapter.Fill(InformationDataSet.Instance.SystemUserOther, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
            if (isReload || JournalDataSet.Instance.SystemUserOrder.Rows.Count == 0)
                JournalDataSet.SystemUserOrderTableAdapter.Fill(JournalDataSet.Instance.SystemUserOrder, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
            if (isReload || JournalDataSet.Instance.SystemUserOrderOther.Rows.Count == 0)
                JournalDataSet.SystemUserOrderOtherTableAdapter.Fill(JournalDataSet.Instance.SystemUserOrderOther, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
            if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
            if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
            if (isReload || UserDataSet.Instance.Producer.Rows.Count == 0)
                UserDataSet.ProducerTableAdapter.Fill(UserDataSet.Instance.Producer, HospitalUtil.getHospitalId());
            //                if (isReload || JournalDataSet.Instance.MedicineOther.Rows.Count == 0)
            //                    JournalDataSet.MedicineOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOther, HospitalUtil.getHospitalId());
            if (isReload || UserDataSet.Instance.PackageMedicine.Rows.Count == 0)
                UserDataSet.PackageMedicineTableAdapter.Fill(UserDataSet.Instance.PackageMedicine, HospitalUtil.getHospitalId());
            if (isReload || UserDataSet.Instance.Supplier.Rows.Count == 0)
                UserDataSet.SupplierTableAdapter.Fill(UserDataSet.Instance.Supplier, HospitalUtil.getHospitalId());
            //ProgramUtil.closeWaitDialog();
        }

        private void reloadTender(bool isReload)
        {
            if (isReload || JournalDataSet.Instance.TenderOther.Rows.Count == 0 && dateEditOrderDate.Text != "")
            {
                JournalDataSet.TenderOtherTableAdapter.Fill(JournalDataSet.Instance.TenderOther, HospitalUtil.getHospitalId(), dateEditOrderDate.Text);

                List<string> list = new List<string>();
                JournalDataSet.Instance.Tender.Clear();
                for (int i = 0; i < JournalDataSet.Instance.TenderOther.Rows.Count; i++)
                {
                    DataRow row = JournalDataSet.Instance.TenderOther.Rows[i];
                    if (list.IndexOf(row["tenderID"].ToString()) == -1)
                    {
                        list.Add(row["tenderID"].ToString());

                        DataRow newRow = JournalDataSet.Instance.Tender.NewRow();
                        newRow["tenderID"] = row["tenderID"];
                        newRow["date"] = row["date"];
                        newRow["tenderNumber"] = row["tenderNumber"];
                        newRow["supplierID"] = row["supplierID"];
                        newRow["supplierName"] = row["supplierName"];
                        newRow.EndEdit();
                        JournalDataSet.Instance.Tender.Rows.Add(newRow);
                    }
                }
            }
        }

        private void changeType()
        {
            int x = labelControl14.Location.X;
            if (radioGroupType.SelectedIndex == 0)
            {
                labelControlWarehouse.Location = new Point(x + 38, 147);
                treeListLookUpEditWard.Location = new Point(157, 12);
                treeListLookUpEditWarehouse.Location = new Point(x + 105, 144);

                treeListLookUpEditWard.Visible = false;
                treeListLookUpEditWarehouse.Visible = true;
                labelControlWarehouse.Text = "Эмийн сан*:";
            }
            else if (radioGroupType.SelectedIndex == 1)
            {
                labelControlWarehouse.Location = new Point(x + 61, 147);
                treeListLookUpEditWard.Location = new Point(x + 105, 144);
                treeListLookUpEditWarehouse.Location = new Point(157, 12);

                treeListLookUpEditWard.Visible = true;
                treeListLookUpEditWarehouse.Visible = false;
                labelControlWarehouse.Text = "Тасаг*:";
            }
        }

        private void changeOrderType()
        {
            int x = labelControl5.Location.X;
            int value = comboBoxEditOrderType.SelectedIndex;
            if (value == 0)
            {
                labelControlExpUser.Text = "Захиалах нярав*:";
                labelControlExpUser.Location = new Point(x + 7, 201);
                gridLookUpEditExpUser.Visible = true;
                gridLookUpEditExpUser.Location = new Point(x + 104, 198);
                gridLookUpEditTender.Visible = false;
                gridLookUpEditTender.Location = new Point(157, 323);
                gridLookUpEditProducer.Visible = false;
                gridLookUpEditProducer.Location = new Point(157, 349);
                //gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineOtherBindingSource;
                gridLookUpEditMedicine.Properties.DataSource = UserDataSet.PackageMedicineBindingSource;
                gridControlMed.DataSource = UserDataSet.PackageMedicineBindingSource;
                labelControlExpUser.Visible = true;
                gridColumn18.FieldName = "unitName";
                gridColumn1.FieldName = "shapeName";
                gridColumn20.FieldName = "quantityName";
                gridColumnUnit.FieldName = "unitName";
                gridColumnShape.FieldName = "shapeName";
                gridColumnQuantity.FieldName = "quantityName";
                checkEditMultiSelection.Enabled = true;
                gridLookUpEditMedicine.Enabled = true;
            }
            else if (value == 1)
            {
                labelControlExpUser.Text = "Тендер*:";
                labelControlExpUser.Location = new Point(x + 51, 201);
                gridLookUpEditExpUser.Visible = false;
                gridLookUpEditExpUser.Location = new Point(157, 323);
                gridLookUpEditTender.Visible = true;
                gridLookUpEditTender.Location = new Point(x + 104, 198);
                gridLookUpEditProducer.Visible = false;
                gridLookUpEditProducer.Location = new Point(157, 349);
                gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.Instance.TenderMedicine;
                gridControlMed.DataSource = JournalDataSet.Instance.TenderMedicine;
                labelControlExpUser.Visible = true;
                gridColumn18.FieldName = "unitType";
                gridColumn1.FieldName = "shape";
                gridColumn20.FieldName = "quantity";
                gridColumnUnit.FieldName = "unitType";
                gridColumnShape.FieldName = "shape";
                gridColumnQuantity.FieldName = "quantity";
                checkEditMultiSelection.Enabled = false;
                gridLookUpEditMedicine.Enabled = false;
            }
            else if (value == 2)
            {
                labelControlExpUser.Text = "Гадаад :";
                labelControlExpUser.Location = new Point(x + 51, 201);
                gridLookUpEditExpUser.Visible = false;
                gridLookUpEditExpUser.Location = new Point(157, 349);
                gridLookUpEditTender.Visible = false;
                gridLookUpEditTender.Location = new Point(157, 323);
                gridLookUpEditProducer.Visible = true;
                gridLookUpEditProducer.Location = new Point(x + 104, 198);
                gridLookUpEditMedicine.Properties.DataSource = UserDataSet.PackageMedicineBindingSource;
                gridControlMed.DataSource = UserDataSet.PackageMedicineBindingSource;
                labelControlExpUser.Visible = true;
                gridColumn18.FieldName = "unitName";
                gridColumn1.FieldName = "shapeName";
                gridColumn20.FieldName = "quantityName";
                gridColumnUnit.FieldName = "unitName";
                gridColumnShape.FieldName = "shapeName";
                gridColumnQuantity.FieldName = "quantityName";
                checkEditMultiSelection.Enabled = true;
                gridLookUpEditMedicine.Enabled = true;
            }
            else if (value == 3)
            {
                labelControlExpUser.Text = "Захиалах нярав*:";
                labelControlExpUser.Location = new Point(x + 7, 201);
                gridLookUpEditExpUser.Visible = true;
                gridLookUpEditExpUser.Location = new Point(x + 104, 198);
                gridLookUpEditTender.Visible = false;
                gridLookUpEditTender.Location = new Point(157, 323);
                gridLookUpEditProducer.Visible = false;
                gridLookUpEditProducer.Location = new Point(157, 349);
                gridLookUpEditMedicine.Properties.DataSource = UserDataSet.PackageMedicineBindingSource;
                gridControlMed.DataSource = UserDataSet.PackageMedicineBindingSource;
                labelControlExpUser.Visible = true;
                gridColumn18.FieldName = "unitName";
                gridColumn1.FieldName = "shapeName";
                gridColumn20.FieldName = "quantityName";
                gridColumnUnit.FieldName = "unitName";
                gridColumnShape.FieldName = "shapeName";
                gridColumnQuantity.FieldName = "quantityName";
                checkEditMultiSelection.Enabled = true;
                gridLookUpEditMedicine.Enabled = true;
            }
        }

        private void changeTender()
        {
            JournalDataSet.Instance.TenderMedicine.Clear();
            DataRow[] rows = JournalDataSet.Instance.TenderOther.Select("tenderID = '" + gridLookUpEditTender.EditValue + "'");
            for (int i = 0; i < rows.Length; i++)
            {
                DataRow newRow = JournalDataSet.Instance.TenderMedicine.NewRow();
                newRow["packageID"] = rows[i]["packageID"];
                newRow["packageCount"] = rows[i]["packageCount"];
                newRow["id"] = rows[i]["id"];
                newRow["price"] = rows[i]["price"];
                newRow["count"] = 0;//rows[i]["count"];
                newRow["name"] = rows[i]["name"];
                newRow["latinName"] = rows[i]["name"];
                newRow["medicineTypeName"] = rows[i]["medicineTypeName"];
                newRow["unitType"] = rows[i]["unitType"];
                newRow["shape"] = rows[i]["shape"];
                newRow["quantity"] = rows[i]["quantity"];
                newRow["validDate"] = DBNull.Value;//rows[i]["validDate"];
                newRow["barcode"] = rows[i]["barcode"];
                newRow.EndEdit();
                JournalDataSet.Instance.TenderMedicine.Rows.Add(newRow);
            }
            checkEditMultiSelection.Enabled = true;
            gridLookUpEditMedicine.Enabled = true;
        }


        private bool check()
        {
            //Instance.Validate();
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();

            if (dateEditOrderDate.EditValue == DBNull.Value || dateEditOrderDate.EditValue == null || dateEditOrderDate.EditValue.ToString().Equals(""))
            {
                errorText = "Огноог оруулна уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditOrderDate.ErrorText = errorText;
                isRight = false;
                dateEditOrderDate.Focus();
            }
            if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.Text.Equals(""))
            {
                errorText = "Няравыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditUser.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
            }
            if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.Text.Equals("")))
            {
                errorText = "Агуулахыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWarehouse.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWarehouse.Focus();
                }
            }
            if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals("")))
            {
                errorText = "Тасгийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWard.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWard.Focus();
                }
            }

            if (comboBoxEditOrderType.EditValue != DBNull.Value && comboBoxEditOrderType.EditValue != null)
            {
                int value = comboBoxEditOrderType.SelectedIndex;
                if (value == 0 && (gridLookUpEditExpUser.EditValue == DBNull.Value || gridLookUpEditExpUser.EditValue == null || gridLookUpEditExpUser.Text.Equals("")))
                {
                    errorText = "Захиалга өгөх няравыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditExpUser.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditExpUser.Focus();
                    }
                }
                if (value == 1 && (gridLookUpEditTender.EditValue == DBNull.Value || gridLookUpEditTender.EditValue == null || gridLookUpEditTender.Text.Equals("")))
                {
                    errorText = "Бэлтгэн нийлүүлэгчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditTender.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditTender.Focus();
                    }
                }
                DataRowView ward = (DataRowView)gridLookUpEditExpUser.GetSelectedDataRow();
                if (value == 0 && isRight && gridLookUpEditUser.EditValue.ToString().Equals(gridLookUpEditExpUser.EditValue.ToString()) && radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.Text == ward["wardName"].ToString())
                {
                    errorText = "Нярав, захиалга өгөх няравын тасаг ижил байж болохгүй";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditExpUser.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditExpUser.Focus();
                    }
                }
            }
            if ((gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals("")) && !checkEditMultiSelection.Checked)
            {
                errorText = "Эмийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditMedicine.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
            }
            if (spinEditCount.Value == 0 && !checkEditMultiSelection.Checked)
            {
                errorText = "Тоо, хэмжээг оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                spinEditCount.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    spinEditCount.Focus();
                }
            }
            if (isRight && !checkEditMultiSelection.Checked)
            {
                DataRow[] rows = JournalDataSet.Instance.OrderGoodsDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND orderGoodsID = '" + docView["id"] + "' AND id <> '" + currView["id"] + "'");
                for (int i = 0; rows != null && i < rows.Length; i++)
                {
                    if (gridLookUpEditMedicine.EditValue.ToString().Equals(rows[i]["medicineID"].ToString()))
                    {
                        errorText = "Эмийн нэр давхацсан байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditMedicine.ErrorText = errorText;
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                        break;
                    }
                }
            }

            if (isRight && checkEditMultiSelection.Checked)
            {
                if (rowID.Length == 0)
                {
                    errorText = "Эмээс сонгоно уу!";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    popupContainerEditMedicineBal.ErrorText = errorText;
                    isRight = false;
                    popupContainerEditMedicineBal.Focus();
                }
                else
                {
                    for (int i = 0; i < rowID.Length; i++)
                    {
                        DataRow[] rows_chk = JournalDataSet.Instance.OrderGoodsDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND orderGoodsID = '" + docView["id"] + "' AND id <> '" + currView["id"] + "' AND medicineName = '" + gridViewMed.GetRowCellValue(rowID[i], gridColumName) + "' AND unitType = '" + gridViewMed.GetRowCellValue(rowID[i], gridColumnUnit) + "'");
                        if (rows_chk.Length > 0)
                        {
                            errorText = "Эм давхацсан байна";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            popupContainerEditMedicineBal.ErrorText = errorText;
                            isRight = false;
                            popupContainerEditMedicineBal.Focus();
                            break;
                        }
                        if (Convert.ToDouble(gridViewMed.GetRowCellValue(rowID[i], gridColumnDupCount)) == 0)
                        {
                            errorText = "Эмийн тоог оруулна уу!";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            popupContainerEditMedicineBal.ErrorText = errorText;
                            isRight = false;
                            popupContainerEditMedicineBal.Focus();
                            break;
                        }
                    }
                }

            }
            if (!isRight && !text.Equals(""))
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        private void save()
        {
            ProgramUtil.IsSaveEvent = true;
            ProgramUtil.IsShowPopup = true;
            if (check())
            {

                if (ProgramUtil.checkLockMedicine(dateEditOrderDate.EditValue.ToString()))
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);

                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                    DataRowView expUser = (DataRowView)gridLookUpEditExpUser.GetSelectedDataRow();
                    DataRowView tender = (DataRowView)gridLookUpEditTender.GetSelectedDataRow();
                    DataRowView medicine = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                    DataRowView producer = (DataRowView)gridLookUpEditProducer.GetSelectedDataRow();
                    int value = comboBoxEditOrderType.SelectedIndex;

                    docView["userName"] = user["fullName"];
                    docView["roleName"] = user["roleName"];
                    docView["orderType"] = comboBoxEditOrderType.SelectedIndex;
                    if (radioGroupType.SelectedIndex == 0)
                    {
                        docView["wardID"] = null;
                        docView["warehouseName"] = warehouse["name"];
                        docView["warehouseDesc"] = warehouse["description"];

                        currView["wardID"] = null;
                        currView["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                        currView["warehouseName"] = warehouse["name"];
                        currView["warehouseDesc"] = warehouse["description"];
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {
                        docView["warehouseID"] = null;
                        docView["warehouseName"] = ward["name"];
                        docView["warehouseDesc"] = ward["description"];

                        currView["warehouseID"] = null;
                        currView["wardID"] = treeListLookUpEditWard.EditValue;
                        currView["warehouseName"] = ward["name"];
                        currView["warehouseDesc"] = ward["description"];
                    }

                    if (value == 0)
                    {
                        docView["expUserName"] = expUser["fullName"];
                        docView["tenderNumber"] = null;

                        currView["expUserID"] = gridLookUpEditExpUser.EditValue;
                        currView["supplierID"] = null;
                        currView["tenderID"] = null;
                        currView["expUserName"] = expUser["fullName"];
                        currView["tenderNumber"] = null;
                    }
                    else if (value == 1)
                    {
                        docView["expUserID"] = null;
                        docView["supplierID"] = tender["supplierID"];
                        docView["expUserName"] = tender["supplierName"];
                        docView["tenderNumber"] = tender["tenderNumber"];

                        currView["expUserID"] = null;
                        currView["tenderID"] = gridLookUpEditTender.EditValue;
                        currView["supplierID"] = tender["supplierID"];
                        currView["expUserName"] = tender["supplierName"];
                        currView["tenderNumber"] = tender["tenderNumber"];
                    }
                    else if (value == 2)
                    {
                        docView["expUserID"] = null;
                        docView["tenderID"] = null;
                        docView["tenderNumber"] = null;

                        if (producer != null)
                        {
                            docView["expUserName"] = producer["name"];
                            docView["supplierID"] = producer["id"];

                            currView["supplierID"] = gridLookUpEditProducer.EditValue;
                            currView["expUserName"] = producer["name"];
                        }
                        else
                        {
                            docView["expUserName"] = null;

                            currView["supplierID"] = null;
                            currView["expUserName"] = null;
                        }

                        currView["expUserID"] = null;
                        currView["tenderID"] = null;
                        currView["tenderNumber"] = null;
                    }
                    else if (value == 3)
                    {
                        docView["expUserName"] = expUser["fullName"];
                        docView["tenderNumber"] = null;
                        docView["tenderID"] = null;
                        docView["supplierID"] = null;

                        currView["expUserID"] = null;
                        currView["tenderID"] = null;
                        currView["tenderNumber"] = null;
                        currView["supplierID"] = null;
                        currView["expUserName"] = null;
                    }
                    docView.EndEdit();

                    if (checkEditMultiSelection.Checked)
                    {
                        if (JournalDataSet.OrderGoodsDetailBindingSource.Current != null)
                            JournalDataSet.OrderGoodsDetailBindingSource.RemoveCurrent();
                        addMultipleRow();
                    }
                    else
                    {
                        currView["date"] = dateEditOrderDate.EditValue;
                        currView["description"] = memoEditDescription.EditValue;
                        currView["userID"] = gridLookUpEditUser.EditValue;
                        currView["type"] = radioGroupType.SelectedIndex;
                        currView["orderType"] = comboBoxEditOrderType.SelectedIndex;
                        currView["userName"] = user["fullName"];
                        currView["roleName"] = user["roleName"];

                        currView["medicineID"] = gridLookUpEditMedicine.EditValue;
                        currView["medicineName"] = medicine["name"];
                        currView["latinName"] = medicine["name"];
                        if (value == 1)
                        {
                            currView["unitType"] = medicine["unitType"];
                            currView["shape"] = medicine["shape"];
                            currView["quantity"] = medicine["quantity"];
                        }
                        else
                        {
                            /*     MessageBox.Show(medicine["unitName"].ToString());
                                 MessageBox.Show(medicine["shapeName"].ToString()); 
                                 MessageBox.Show(medicine["quantityName"].ToString());
                                 MessageBox.Show(medicine.Row.ToString());
                              */
                            currView["unitType"] = medicine["unitName"];
                            currView["shape"] = medicine["shapeName"];
                            currView["quantity"] = medicine["quantityName"];
                        }
                        currView["validDate"] = DBNull.Value;
                        currView["medicineTypeName"] = medicine["medicineTypeName"];

                        currView["count"] = spinEditCount.EditValue;

                        currView.EndEdit();

                        for (int i = 0; i < JournalDataSet.OrderGoodsDetailBindingSource.Count; i++)
                        {
                            DataRowView row = (DataRowView)JournalDataSet.OrderGoodsDetailBindingSource[i];
                            if (row["orderGoodsID"].ToString().Equals(docView["id"].ToString()) &&
                                !row["id"].ToString().Equals(currView["id"].ToString()))
                            {
                                row["date"] = dateEditOrderDate.EditValue;
                                row["description"] = memoEditDescription.EditValue;
                                row["userID"] = gridLookUpEditUser.EditValue;
                                row["type"] = radioGroupType.SelectedIndex;
                                row["orderType"] = comboBoxEditOrderType.SelectedIndex;
                                row["userName"] = user["fullName"];
                                row["roleName"] = user["roleName"];

                                if (radioGroupType.SelectedIndex == 0)
                                {
                                    row["wardID"] = null;
                                    row["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                                    row["warehouseName"] = warehouse["name"];
                                    row["warehouseDesc"] = warehouse["description"];
                                }
                                else if (radioGroupType.SelectedIndex == 1)
                                {
                                    row["warehouseID"] = null;
                                    row["wardID"] = treeListLookUpEditWard.EditValue;
                                    row["warehouseName"] = ward["name"];
                                    row["warehouseDesc"] = ward["description"];
                                }

                                if (value == 0)
                                {
                                    row["expUserID"] = gridLookUpEditExpUser.EditValue;
                                    row["supplierID"] = null;
                                    row["tenderID"] = null;
                                    row["expUserName"] = expUser["fullName"];
                                    row["tenderNumber"] = null;
                                }
                                else if (value == 1)
                                {
                                    row["expUserID"] = null;
                                    row["tenderID"] = gridLookUpEditTender.EditValue;
                                    row["supplierID"] = tender["supplierID"];
                                    row["expUserName"] = tender["supplierName"];
                                    row["tenderNumber"] = tender["tenderNumber"];
                                }
                                else if (value == 2)
                                {
                                    row["expUserID"] = null;
                                    row["tenderID"] = null;
                                    row["tenderNumber"] = null;
                                    if (producer != null)
                                    {
                                        row["supplierID"] = gridLookUpEditProducer.EditValue;
                                        row["expUserName"] = producer["name"];
                                    }
                                    else
                                    {
                                        row["supplierID"] = null;
                                        row["expUserName"] = null;
                                    }
                                }
                                else if (value == 3)
                                {
                                    row["expUserID"] = null;
                                    row["tenderID"] = null;
                                    row["tenderNumber"] = null;
                                    row["supplierID"] = null;
                                    row["expUserName"] = null;
                                }
                                row.EndEdit();
                            }
                        }
                    }
                    JournalDataSet.OrderGoodsTableAdapter.Update(JournalDataSet.Instance.OrderGoods);
                    JournalDataSet.OrderGoodsDetailTableAdapter.Update(JournalDataSet.Instance.OrderGoodsDetail);

                    ProgramUtil.closeWaitDialog();

                    /*           DataRow[] rows = JournalDataSet.Instance.OrderGoodsDetail.Select("id = '" + currView["id"] + "'");
                               if (rows.Length > 0)
                               {
                                   MessageBox.Show(rows.Length.ToString());
                               }
                               */
                    if (checkEditMultiSelection.Checked)
                        Close();
                    if (UserUtil.getUserRoleId() == RoleUtil.TREATMENT_DIRECTOR_ID)
                    {
                        Close();
                    }
                    else
                    {
                        addRow();
                        gridLookUpEditMedicine.Focus();
                    }
                }
                else
                {
                    XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
                }               
            }
        }

        private void addMultipleRow()
        {
            for (int i = 0; i < rowID.Length; i++)
            {
                DataRow medicine = gridViewMed.GetDataRow(rowID[i]);
                DataRow newRow = JournalDataSet.Instance.OrderGoodsDetail.NewRow();
                newRow["id"] = getNotExistId();
                newRow["orderGoodsID"] = docView["id"];
                newRow["date"] = dateEditOrderDate.EditValue;
                newRow["description"] = memoEditDescription.EditValue;
                newRow["userID"] = gridLookUpEditUser.EditValue;
                newRow["type"] = radioGroupType.SelectedIndex;
                newRow["orderType"] = comboBoxEditOrderType.SelectedIndex;
                newRow["userName"] = docView["userName"];
                newRow["roleName"] = docView["roleName"];

                newRow["medicineID"] = medicine["id"];
                newRow["medicineName"] = medicine["name"];
                newRow["latinName"] = medicine["name"];
                if (comboBoxEditOrderType.SelectedIndex == 1)
                {
                    newRow["unitType"] = medicine["unitType"];
                    newRow["shape"] = medicine["shape"];
                    newRow["quantity"] = medicine["quantity"];
                }
                else
                {
                    newRow["unitType"] = medicine["unitName"];
                    newRow["shape"] = medicine["shapeName"];
                    newRow["quantity"] = medicine["quantityName"];
                }
                newRow["validDate"] = DBNull.Value;
                newRow["medicineTypeName"] = medicine["medicineTypeName"];
                newRow["count"] = medicine["count"];
                JournalDataSet.Instance.OrderGoodsDetail.Rows.Add(newRow);
            }
        }

        private string getNotExistId()
        {
            string newID = null;
            DataRow[] rows = null;
            DateTime thisDay = DateTime.Today;
            do
            {
                newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                rows = JournalDataSet.Instance.OrderGoodsDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND orderGoodsID = '" + docView["id"] + "' AND id = '" + newID + "'");
            } while (rows != null && rows.Length > 0);

            return newID;
        }

        // Шинээр нэмсэн бэлгэн нийлүүлэгчийг авч байна
        public void addProducer(string id)
        {
            gridLookUpEditProducer.EditValue = id;
        }

        public void addMedicine(DataRowView view)
        {
            /*DataRowView row = (DataRowView)JournalDataSet.MedicineOtherBindingSource.AddNew();
            row["id"] = view["id"];
            row["name"] = view["name"];
            row["latinName"] = view["latinName"];
            row["unitType"] = view["unitName"];
            row["price"] = view["price"];
            row["medicineTypeName"] = view["medicineTypeName"];
            row["barcode"] = view["barcode"];
            row["shape"] = view["shapeName"];
            row["validDate"] = view["validDate"];
            row["serial"] = view["serial"];
            row["quantity"] = view["quantityName"];
            row.EndEdit();*/
            gridLookUpEditMedicine.EditValue = view["id"];
        }

        #endregion

        private void checkEditMultiSelection_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEditMultiSelection.Checked)
            {
                spinEditCount.ReadOnly = true;
                popupContainerEditMedicineBal.Visible = true;
                popupContainerEditMedicineBal.Location = new Point(157, 271);
                gridLookUpEditMedicine.Visible = false;
            }
            else
            {
                spinEditCount.ReadOnly = false;
                gridLookUpEditMedicine.Visible = true;
                popupContainerEditMedicineBal.Visible = false;
                popupContainerEditMedicineBal.Location = new Point(287, 347);
            }
        }

        private void popupContainerEditMedicineBal_CloseUp(object sender, DevExpress.XtraEditors.Controls.CloseUpEventArgs e)
        {
            rowID = gridViewMed.GetSelectedRows();
        }

        private void gridViewMed_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column == gridColumnDupCount)
            {
                if (Convert.ToInt32(e.Value) < 0)
                {
                    gridViewMed.SetFocusedRowCellValue(gridColumnDupCount, 0);
                    XtraMessageBox.Show("Хасах утга оруулах боломжгүй ...");
                }
            }
        }

        private void comboBoxEditOrderType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void MedicineOrderUpForm_Load(object sender, EventArgs e)
        {

        }
    }
}