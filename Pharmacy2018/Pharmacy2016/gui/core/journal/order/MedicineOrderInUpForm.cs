﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using DevExpress.XtraGrid.Views.Base;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;



namespace Pharmacy2016.gui.core.journal.order
{
    /*
     * Эмийн захиалга нэмэх, засах цонх.
     * 
     */

    public partial class MedicineOrderInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineOrderInUpForm INSTANCE = new MedicineOrderInUpForm();

            public static MedicineOrderInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private string id;

            private MedicineOrderInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                spinEditCount.Properties.MaxValue = Decimal.MaxValue - 1;
                
                initError();
                initBinding();
                initDataSource();
                reload(false);
            }

            private void initDataSource()
            {
                gridControlDetail.DataSource = JournalDataSet.OrderGoodsDetailBindingSource;
                gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
                treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
                treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
                gridLookUpEditExpUser.Properties.DataSource = InformationDataSet.SystemUserOtherBindingSource;
                treeListLookUpEditExpWard.Properties.DataSource = JournalDataSet.WardOther1BindingSource;
                treeListLookUpEditExpWarehouse.Properties.DataSource = JournalDataSet.WarehouseOther1BindingSource;
                gridLookUpEditProducer.Properties.DataSource = UserDataSet.ProducerBindingSource;
                gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineOtherBindingSource;
            }

            private void initError()
            {
                dateEditOrderDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditExpUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditExpWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditExpWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditProducer.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insertOrder();
                    }
                    else if (actionType == 2)
                    {
                        updateOrder();
                    }
                    else if (actionType == 3)
                    {
                        copyOrder();
                    }

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void initBinding()
            {
                textEditID.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "id", true));
                dateEditOrderDate.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "date", true));
                radioGroupOrderType.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "orderType", true));
                gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "userID", true));
                radioGroupType.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "type", true));
                treeListLookUpEditWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "warehouseID", true));
                treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "wardID", true));
                gridLookUpEditExpUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "expUserID", true));
                radioGroupExpType.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "expType", true));
                treeListLookUpEditExpWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "expWarehouseID", true));
                treeListLookUpEditExpWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "expWardID", true));
                gridLookUpEditProducer.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "producerID", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "description", true));
                gridLookUpEditMedicine.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsDetailBindingSource, "medicineID", true));
                //spinEditCount.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsDetailBindingSource, "count", true));
                checkEditIsConfirm.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "isConfirm", true));
            }

            private void insertOrder()
            {
                //this.Text = "Захиалгын хуудас нэмэх цонх";
                id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                JournalDataSet.Instance.OrderGoods.idColumn.DefaultValue = id;
                JournalDataSet.Instance.OrderGoodsDetail.orderGoodsIDColumn.DefaultValue = id;

                MedicineOrderForm.Instance.currView = (DataRowView)JournalDataSet.OrderGoodsBindingSource.AddNew();
                DateTime now = DateTime.Now;
                MedicineOrderForm.Instance.currView["id"] = id;
                MedicineOrderForm.Instance.currView["date"] = now;

                dateEditOrderDate.DateTime = now;
                textEditID.Text = id;

                checkRole(false);
            }

            private void updateOrder()
            {
                //this.Text = "Захиалгын хуудас засах цонх";
                id = MedicineOrderForm.Instance.currView["id"].ToString();
                JournalDataSet.Instance.OrderGoodsDetail.orderGoodsIDColumn.DefaultValue = id;
                checkRole(UserUtil.getUserRoleId() == RoleUtil.TREATMENT_DIRECTOR_ID);
            }

            private void checkRole(bool isReadOnly)
            {
                labelControlIsConfirm.Visible = isReadOnly;
                checkEditIsConfirm.Visible = isReadOnly;

                dateEditOrderDate.Properties.ReadOnly = isReadOnly;
                gridLookUpEditUser.Properties.ReadOnly = isReadOnly;
                radioGroupType.Properties.ReadOnly = isReadOnly;
                treeListLookUpEditWarehouse.Properties.ReadOnly = isReadOnly;
                treeListLookUpEditWard.Properties.ReadOnly = isReadOnly;
                radioGroupOrderType.Properties.ReadOnly = isReadOnly;
                gridLookUpEditProducer.Properties.ReadOnly = isReadOnly;
                gridLookUpEditExpUser.Properties.ReadOnly = isReadOnly;
                radioGroupExpType.Properties.ReadOnly = isReadOnly;
                treeListLookUpEditExpWard.Properties.ReadOnly = isReadOnly;
                treeListLookUpEditExpWarehouse.Properties.ReadOnly = isReadOnly;
                gridLookUpEditMedicine.Properties.ReadOnly = isReadOnly;
                memoEditDescription.Properties.ReadOnly = isReadOnly;
                spinEditCount.Properties.ReadOnly = isReadOnly;

                gridControlDetail.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !isReadOnly;
                gridControlDetail.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !isReadOnly;
            }
            
            private void copyOrder()
            {
                //this.Text = "Захиалгын хуудас нэмэх цонх";
                id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                JournalDataSet.Instance.OrderGoods.idColumn.DefaultValue = id;
                JournalDataSet.Instance.OrderGoodsDetail.orderGoodsIDColumn.DefaultValue = id;

                DataRowView copyView = MedicineOrderForm.Instance.currView;
                MedicineOrderForm.Instance.currView = (DataRowView)JournalDataSet.OrderGoodsBindingSource.AddNew();
                MedicineOrderForm.Instance.currView["orderType"] = copyView["orderType"];
                MedicineOrderForm.Instance.currView["userID"] = copyView["userID"];
                MedicineOrderForm.Instance.currView["type"] = copyView["type"];
                MedicineOrderForm.Instance.currView["warehouseID"] = copyView["warehouseID"];
                MedicineOrderForm.Instance.currView["wardID"] = copyView["wardID"];
                MedicineOrderForm.Instance.currView["expUserID"] = copyView["expUserID"];
                MedicineOrderForm.Instance.currView["expType"] = copyView["expType"];
                MedicineOrderForm.Instance.currView["expWarehouseID"] = copyView["expWarehouseID"];
                MedicineOrderForm.Instance.currView["expWardID"] = copyView["expWardID"];
                MedicineOrderForm.Instance.currView["producerID"] = copyView["producerID"];

                DateTime now = DateTime.Now;
                MedicineOrderForm.Instance.currView["id"] = id;
                MedicineOrderForm.Instance.currView["date"] = now;
                dateEditOrderDate.DateTime = now;
                textEditID.Text = id;

                DataRow[] rows = JournalDataSet.Instance.OrderGoodsDetail.Select("hospitalID = '" + copyView["hospitalID"] + "' AND orderGoodsID = '" + copyView["id"] + "'");
                for (int i = 0; i < rows.Length; i++)
                {
                    JournalDataSet.Instance.OrderGoodsDetail.idColumn.DefaultValue = getNotExistId();
                    DataRowView view = (DataRowView)JournalDataSet.OrderGoodsDetailBindingSource.AddNew();

                    view["medicineID"] = rows[i]["medicineID"];
                    view["medicineName"] = rows[i]["medicineName"];
                    view["latinName"] = rows[i]["latinName"];
                    view["unitType"] = rows[i]["unitType"];
                    view["shape"] = rows[i]["shape"];
                    view["quantity"] = rows[i]["quantity"];
                    view["validDate"] = rows[i]["validDate"];
                    view["medicineTypeName"] = rows[i]["medicineTypeName"];
                    view["count"] = rows[i]["count"];
                    view.EndEdit();
                }

                checkRole(false);
            }

            private string getNotExistId()
            {
                string newID = null;
                DataRow[] rows = null;
                do
                {
                    newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    rows = JournalDataSet.Instance.OrderGoodsDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND orderGoodsID = '" + id + "' AND id = '" + newID + "'");
                } while (rows != null && rows.Length > 0);

                return newID;
            }


            private void clearData()
            {
                clearErrorText();
                clearMedicine();
                //clearBinding();

                JournalDataSet.WarehouseOtherBindingSource.Filter = "";
                JournalDataSet.WardOtherBindingSource.Filter = "";
                InformationDataSet.SystemUserBindingSource.Filter = "";
                JournalDataSet.WarehouseOther1BindingSource.Filter = "";
                JournalDataSet.WardOther1BindingSource.Filter = "";

                JournalDataSet.OrderGoodsBindingSource.CancelEdit();
                JournalDataSet.OrderGoodsDetailBindingSource.CancelEdit();
                JournalDataSet.Instance.OrderGoods.RejectChanges();
                JournalDataSet.Instance.OrderGoodsDetail.RejectChanges();
            }

            private void clearBinding()
            {
                textEditID.DataBindings.Clear();
                dateEditOrderDate.DataBindings.Clear();
                radioGroupOrderType.DataBindings.Clear();
                gridLookUpEditUser.DataBindings.Clear();
                radioGroupType.DataBindings.Clear();
                treeListLookUpEditWarehouse.DataBindings.Clear();
                treeListLookUpEditWard.DataBindings.Clear();
                gridLookUpEditExpUser.DataBindings.Clear();
                radioGroupExpType.DataBindings.Clear();
                treeListLookUpEditExpWarehouse.DataBindings.Clear();
                treeListLookUpEditExpWard.DataBindings.Clear();
                gridLookUpEditProducer.DataBindings.Clear();
                memoEditDescription.DataBindings.Clear();
                gridLookUpEditMedicine.DataBindings.Clear();
                checkEditIsConfirm.DataBindings.Clear();
            }

            private void clearErrorText()
            {
                dateEditOrderDate.ErrorText = "";
                gridLookUpEditUser.ErrorText = "";
                treeListLookUpEditWarehouse.ErrorText = "";
                treeListLookUpEditWard.ErrorText = "";
                gridLookUpEditExpUser.ErrorText = "";
                treeListLookUpEditExpWarehouse.ErrorText = "";
                treeListLookUpEditExpWard.ErrorText = "";
                gridLookUpEditProducer.ErrorText = "";
                gridLookUpEditMedicine.ErrorText = "";
                spinEditCount.ErrorText = "";
            }

            private void clearMedicine()
            {
                gridLookUpEditMedicine.EditValue = null;
                spinEditCount.Value = 0;
            }

            private void MedicineOrderInUpForm_Shown(object sender, EventArgs e)
            {
                //gridViewDetail.FocusedRowHandle = 0;
                //gridViewDetail_BeforeLeaveRow(gridViewDetail, new RowAllowEventArgs(gridViewDetail.FocusedRowHandle, true));

                if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";

                if (actionType == 1)
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                    {
                        gridLookUpEditUser.EditValue = UserUtil.getUserId();
                        treeListLookUpEditWarehouse.Focus();
                    }
                    else
                    {
                        gridLookUpEditUser.Focus();
                    }
                }
                else if (actionType == 2)
                {
                    setValueFromGrid();
                    gridLookUpEditMedicine.Focus();
                }
                else if (actionType == 3)
                {
                    dateEditOrderDate.Focus();
                }
                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
                changeType();
                changeOrderType();
                //changeExpType();
            }

        #endregion

        #region Формын event

            private void dateEditOrderDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditOrderDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditUser.Focus();
                    }
                }
            }

            private void gridLookUpEditUser_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditUser.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        if (radioGroupType.SelectedIndex == 0)
                            treeListLookUpEditWarehouse.Focus();
                        else
                            treeListLookUpEditWard.Focus();
                    }
                }
            }

            private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeType();
            }

            private void treeListLookUpEditWarehouse_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        ((TreeListLookUpEdit)sender).ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        if(radioGroupOrderType.SelectedIndex == 0)
                            gridLookUpEditExpUser.Focus();
                        else if (radioGroupOrderType.SelectedIndex == 1)
                            gridLookUpEditProducer.Focus();
                    }
                }
            }

            private void radioGroupOrderType_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeOrderType();
            }

            private void gridLookUpEditExpUser_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        ((GridLookUpEdit)sender).ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        if (radioGroupExpType.SelectedIndex == 0)
                            treeListLookUpEditExpWarehouse.Focus();
                        else if (radioGroupOrderType.SelectedIndex == 1)
                            treeListLookUpEditExpWard.Focus();
                    }
                }
            }

            private void radioGroupExpType_SelectedIndexChanged(object sender, EventArgs e)
            {
                changeExpType();
            }

            private void treeListLookUpEditExpWarehouse_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        ((TreeListLookUpEdit)sender).ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        memoEditDescription.Focus();
                    }
                }
            }

            private void treeListLookUpEditWarehouse_EditValueChanged(object sender, EventArgs e)
            {
                //if (actionType == 1 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue.ToString() != "" && gridViewDetail.RowCount == 0)
                //{
                //    addRow();
                //    gridLookUpEditMedicine.Focus();
                //}
            }


            private void gridControlDetail_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add") && (gridViewDetail.RowCount == 0 || (gridLookUpEditMedicine.ErrorText == "" && gridLookUpEditMedicine.EditValue != null && gridLookUpEditMedicine.EditValue != DBNull.Value && checkMedicine())))
                    {
                        addRow();
                        gridLookUpEditMedicine.Focus();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewDetail.GetFocusedDataRow() != null)
                    {
                        deleteRow();
                    }
                    else if (String.Equals(e.Button.Tag, "download"))
                    {
                        showDownloadForm();
                    }
                }
            }

            private void gridControlDetail_KeyUp(object sender, KeyEventArgs e)
            {
                if(e.KeyCode == Keys.Add)
                {
                    addRow();
                    gridLookUpEditMedicine.Focus();
                }
                else if (e.KeyCode == Keys.Divide)
                {
                    deleteRow();
                }
            }

            private void gridViewDetail_BeforeLeaveRow(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
            {
                if (this.Visible && gridViewDetail.GetDataRow(e.RowHandle) != null && (gridViewDetail.GetDataRow(e.RowHandle)["medicineID"] == DBNull.Value || Convert.ToDecimal(gridViewDetail.GetDataRow(e.RowHandle)["count"]) == 0))
                {
                    e.Allow = e.RowHandle < 0;
                    if(!e.Allow)
                        spinEditCount.ErrorText = "Тоо, хэмжээг оруулна уу";
                }
                else if (this.Visible && e.RowHandle >= 0 && (gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != DBNull.Value && gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != null))
                {
                    bool isAllow = true;
                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        if (e.RowHandle != i && gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID") != null && gridViewDetail.GetRowCellValue(i, "medicineID") != DBNull.Value && gridViewDetail.GetRowCellValue(i, "medicineID") != null &&
                            gridViewDetail.GetRowCellValue(e.RowHandle, "medicineID").ToString().Equals(gridViewDetail.GetRowCellValue(i, "medicineID").ToString()) )
                        {
                            gridLookUpEditMedicine.ErrorText = "Эм давхацсан байна";
                            gridLookUpEditMedicine.Focus();
                            isAllow = false;
                            break;
                        }
                    }
                    e.Allow = isAllow;
                }
            }

            private void gridViewDetail_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
            {
                gridLookUpEditMedicine.Focus();
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridViewDetail_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
            {
                if(Visible)
                    setValueFromGrid();
            }


            private void gridLookUpEditMedicine_EditValueChanged(object sender, EventArgs e)
            {
                //DataRowView view = gridLookUpEditMedicine.GetSelectedDataRow();
            }

            private void gridLookUpEditMedicine_Validating(object sender, CancelEventArgs e)
            {
                if (!e.Cancel && gridLookUpEditMedicine.EditValue != DBNull.Value && gridLookUpEditMedicine.EditValue != null)
                {
                    endEdit();
                }
            }

            private void gridLookUpEditMedicine_InvalidValue(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
            {
                e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
            }

            private void gridLookUpEditMedicine_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditMedicine.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        spinEditCount.Focus();
                    }
                }
            }

            private void spinEditCount_Leave(object sender, EventArgs e)
            {
                endEdit();
            }

            private void spinEditCount_KeyUp(object sender, KeyEventArgs e)
            {
                if(e.KeyCode == Keys.Enter)
                {
                    gridControlDetail.Focus();
                }
            }


            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload(true);
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                saveOrder();
            }

        #endregion

        #region Формын функц

            private void reload(bool isReload)
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
                InformationDataSet.SystemUserOtherTableAdapter.Fill(InformationDataSet.Instance.SystemUserOther, HospitalUtil.getHospitalId(), SystemUtil.ALL_USER);
                if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                    JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                    JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
                if (isReload || UserDataSet.Instance.Producer.Rows.Count == 0)
                    UserDataSet.ProducerTableAdapter.Fill(UserDataSet.Instance.Producer, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.MedicineOther.Rows.Count == 0)
                    JournalDataSet.MedicineOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOther, HospitalUtil.getHospitalId(), "");
                //ProgramUtil.closeWaitDialog();
            }

            private void changeOrderType()
            {
                if(radioGroupOrderType.SelectedIndex == 0)
                {
                    labelControlExpUser.Text = "Захиалах нярав*:";
                    labelControlExpUser.Location = new Point(358, 93);
                    gridLookUpEditExpUser.Visible = true;
                    gridLookUpEditExpUser.Location = new Point(455, 90);
                    gridLookUpEditProducer.Visible = false;
                    gridLookUpEditProducer.Location = new Point(125, 465);
                    radioGroupExpType.Visible = true;
                    labelControlExpType.Visible = true;
                    labelControlExpWarehouse.Visible = true;
                    changeExpType();
                }
                else if (radioGroupOrderType.SelectedIndex == 1)
                {
                    labelControlExpUser.Text = "Бэлтгэн нийлүүлэгч*:";
                    labelControlExpUser.Location = new Point(341, 93);
                    gridLookUpEditExpUser.Visible = false;
                    gridLookUpEditExpUser.Location = new Point(125, 465);
                    gridLookUpEditProducer.Visible = true;
                    gridLookUpEditProducer.Location = new Point(455, 90);
                    radioGroupExpType.Visible = false;
                    treeListLookUpEditExpWarehouse.Visible = false;
                    treeListLookUpEditExpWard.Visible = false;
                    labelControlExpType.Visible = false;
                    labelControlExpWarehouse.Visible = false;
                }
            }

            private void changeType()
            {
                if (radioGroupType.SelectedIndex == 0)
                {
                    labelControlWarehouse.Location = new Point(58, 149);
                    treeListLookUpEditWard.Location = new Point(125, 12);
                    treeListLookUpEditWarehouse.Location = new Point(125, 146);

                    treeListLookUpEditWard.Visible = false;
                    treeListLookUpEditWarehouse.Visible = true;
                    labelControlWarehouse.Text = "Эмийн сан*:";
                }
                else if (radioGroupType.SelectedIndex == 1)
                {
                    labelControlWarehouse.Location = new Point(81, 149);
                    treeListLookUpEditWard.Location = new Point(125, 146);
                    treeListLookUpEditWarehouse.Location = new Point(125, 12);

                    treeListLookUpEditWard.Visible = true;
                    treeListLookUpEditWarehouse.Visible = false;
                    labelControlWarehouse.Text = "Тасаг*:";
                }
            }

            private void changeExpType()
            {
                if (radioGroupExpType.SelectedIndex == 0)
                {
                    labelControlExpWarehouse.Location = new Point(388, 149);
                    treeListLookUpEditExpWard.Location = new Point(455, 12);
                    treeListLookUpEditExpWarehouse.Location = new Point(455, 146);

                    treeListLookUpEditExpWard.Visible = false;
                    treeListLookUpEditExpWarehouse.Visible = true;
                    labelControlExpWarehouse.Text = "Эмийн сан*:";
                }
                else if (radioGroupType.SelectedIndex == 1)
                {
                    labelControlExpWarehouse.Location = new Point(411, 149);
                    treeListLookUpEditExpWard.Location = new Point(455, 146);
                    treeListLookUpEditExpWarehouse.Location = new Point(455, 12);

                    treeListLookUpEditExpWard.Visible = true;
                    treeListLookUpEditExpWarehouse.Visible = false;
                    labelControlExpWarehouse.Text = "Тасаг*:";
                }
            }


            private void setValueFromGrid()
            {
                DataRowView view = (DataRowView)JournalDataSet.OrderGoodsDetailBindingSource.Current;
                if (view != null)
                {
                    spinEditCount.EditValue = view["count"];
                }
            }

            private void endEdit()
            {
                DataRowView row = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                DataRowView view = (DataRowView)JournalDataSet.OrderGoodsDetailBindingSource.Current;
                if (row == null || view == null)
                    return;

                view["medicineID"] = gridLookUpEditMedicine.EditValue;
                view["medicineName"] = row["name"];
                view["latinName"] = row["latinName"];
                view["unitType"] = row["unitType"];
                view["shape"] = row["shape"];
                view["quantity"] = row["quantity"];
                view["validDate"] = row["validDate"];
                view["medicineTypeName"] = row["medicineTypeName"];
                view["count"] = spinEditCount.EditValue;
                view.EndEdit();
            }

            private void addRow()
            {
                JournalDataSet.Instance.OrderGoodsDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                DataRowView view = (DataRowView)JournalDataSet.OrderGoodsDetailBindingSource.AddNew();
                clearMedicine();
                gridViewDetail.Focus();
            }

            private void deleteRow()
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Захиалгын хуудас устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    JournalDataSet.OrderGoodsDetailBindingSource.RemoveCurrent();
                }
            }

            private bool checkOrder()
            {
                //Instance.Validate();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditOrderDate.EditValue == DBNull.Value || dateEditOrderDate.EditValue == null || dateEditOrderDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditOrderDate.ErrorText = errorText;
                    isRight = false;
                    dateEditOrderDate.Focus();
                }
                if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.Text.Equals(""))
                {
                    errorText = "Няравыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditUser.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.Text.Equals("")))
                {
                    errorText = "Агуулахыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWarehouse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWarehouse.Focus();
                    }
                }
                if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals("")))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }


                if (radioGroupOrderType.SelectedIndex == 0)
                {
                    if (gridLookUpEditExpUser.EditValue == DBNull.Value || gridLookUpEditExpUser.EditValue == null || gridLookUpEditExpUser.Text.Equals(""))
                    {
                        errorText = "Захиалга өгөх няравыг сонгоно уу";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditExpUser.ErrorText = errorText;
                        if (isRight)
                        {
                            isRight = false;
                            gridLookUpEditExpUser.Focus();
                        }
                    }
                    if (radioGroupExpType.SelectedIndex == 0 && (treeListLookUpEditExpWarehouse.EditValue == DBNull.Value || treeListLookUpEditExpWarehouse.EditValue == null || treeListLookUpEditExpWarehouse.Text.Equals("")))
                    {
                        errorText = "Захиалга өгөх агуулахыг сонгоно уу";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        treeListLookUpEditExpWarehouse.ErrorText = errorText;
                        if (isRight)
                        {
                            isRight = false;
                            treeListLookUpEditExpWarehouse.Focus();
                        }
                    }
                    if (radioGroupExpType.SelectedIndex == 1 && (treeListLookUpEditExpWard.EditValue == DBNull.Value || treeListLookUpEditExpWard.EditValue == null || treeListLookUpEditExpWard.Text.Equals("")))
                    {
                        errorText = "Захиалга өгөх тасгийг сонгоно уу";
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        treeListLookUpEditExpWard.ErrorText = errorText;
                        if (isRight)
                        {
                            isRight = false;
                            treeListLookUpEditExpWard.Focus();
                        }
                    }
                }
                if (radioGroupOrderType.SelectedIndex == 1 && (gridLookUpEditProducer.EditValue == DBNull.Value || gridLookUpEditProducer.EditValue == null || gridLookUpEditProducer.Text.Equals("")))
                {
                    errorText = "Бэлтгэн нийлүүлэгчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditProducer.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditProducer.Focus();
                    }
                }

                
                
                if (isRight && radioGroupOrderType.SelectedIndex == 0)
                    isRight = checkMedicine();
                else
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private bool checkMedicine()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
                if (spinEditCount.Value == 0)
                {
                    errorText = "Тоо, хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditCount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditCount.Focus();
                    }
                }
                for (int i = 0; isRight && i < gridViewDetail.RowCount; i++)
                {
                    if (gridViewDetail.FocusedRowHandle != i && gridLookUpEditMedicine.EditValue != null
                        && gridLookUpEditMedicine.EditValue.ToString().Equals(gridViewDetail.GetRowCellValue(i, "medicineID").ToString()))
                    {
                        errorText = "Эм давхацсан байна";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridLookUpEditMedicine.ErrorText = errorText;
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                        break;
                    }
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void saveOrder()
            {
                if (checkOrder())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);

                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                    DataRowView expUser = (DataRowView)gridLookUpEditExpUser.GetSelectedDataRow();
                    DataRowView producer = (DataRowView)gridLookUpEditProducer.GetSelectedDataRow();
                    DataRowView expWarehouse = (DataRowView)treeListLookUpEditExpWarehouse.GetSelectedDataRow();
                    DataRowView expWard = (DataRowView)treeListLookUpEditExpWard.GetSelectedDataRow();
                    //DataRowView medicine = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();

                    MedicineOrderForm.Instance.currView["userName"] = user["fullName"];
                    MedicineOrderForm.Instance.currView["roleName"] = user["roleName"];
                    if (radioGroupType.SelectedIndex == 0)
                    {
                        MedicineOrderForm.Instance.currView["wardID"] = null;
                        MedicineOrderForm.Instance.currView["warehouseName"] = warehouse["name"];
                        MedicineOrderForm.Instance.currView["warehouseDesc"] = warehouse["description"];
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {
                        MedicineOrderForm.Instance.currView["warehouseID"] = null;
                        MedicineOrderForm.Instance.currView["warehouseName"] = ward["name"];
                        MedicineOrderForm.Instance.currView["warehouseDesc"] = ward["description"];
                    }

                    if (radioGroupOrderType.SelectedIndex == 0)
                    {
                        MedicineOrderForm.Instance.currView["expUserName"] = expUser["fullName"];
                        MedicineOrderForm.Instance.currView["expRoleName"] = expUser["roleName"];

                        if (radioGroupExpType.SelectedIndex == 0)
                        {
                            MedicineOrderForm.Instance.currView["expWardID"] = null;
                            MedicineOrderForm.Instance.currView["expWarehouseName"] = expWarehouse["name"];
                            MedicineOrderForm.Instance.currView["expWarehouseDesc"] = expWarehouse["description"];
                        }
                        else if (radioGroupExpType.SelectedIndex == 1)
                        {
                            MedicineOrderForm.Instance.currView["expWarehouseID"] = null;
                            MedicineOrderForm.Instance.currView["expWarehouseName"] = expWard["name"];
                            MedicineOrderForm.Instance.currView["expWarehouseDesc"] = expWard["description"];
                        }
                    }
                    else if (radioGroupOrderType.SelectedIndex == 1)
                    {
                        MedicineOrderForm.Instance.currView["expUserID"] = null;
                        MedicineOrderForm.Instance.currView["expUserName"] = gridLookUpEditProducer.Text;
                    }

                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        DataRow row = gridViewDetail.GetDataRow(i);
                        row["date"] = dateEditOrderDate.EditValue;
                        row["description"] = memoEditDescription.EditValue;
                        row["orderType"] = radioGroupOrderType.EditValue;
                        row["userID"] = gridLookUpEditUser.EditValue;
                        if (radioGroupType.SelectedIndex == 0)
                        {
                            row["wardID"] = null;
                            row["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                            row["warehouseName"] = warehouse["name"];
                            row["warehouseDesc"] = warehouse["description"];
                        }
                        else if (radioGroupType.SelectedIndex == 1)
                        {
                            row["wardID"] = treeListLookUpEditWard.EditValue;
                            row["warehouseID"] = null;
                            row["warehouseName"] = ward["name"];
                            row["warehouseDesc"] = ward["description"];
                        }

                        if (radioGroupOrderType.SelectedIndex == 0)
                        {
                            row["expUserID"] = gridLookUpEditExpUser.EditValue;
                            row["expUserName"] = expUser["fullName"];
                            row["expRoleName"] = expUser["roleName"];
                            row["producerID"] = null;
                            if (radioGroupExpType.SelectedIndex == 0)
                            {
                                row["expWarehouseID"] = treeListLookUpEditExpWarehouse.EditValue;
                                row["expWardID"] = null;
                                row["expWarehouseName"] = expWarehouse["name"];
                                row["expWarehouseDesc"] = expWarehouse["description"];
                            }
                            else if (radioGroupExpType.SelectedIndex == 1)
                            {
                                row["expWarehouseID"] = null;
                                row["expWardID"] = treeListLookUpEditExpWard.EditValue;
                                row["expWarehouseName"] = expWard["name"];
                                row["expWarehouseDesc"] = expWard["description"];
                            }
                        }
                        else if (radioGroupOrderType.SelectedIndex == 1)
                        {
                            row["expUserID"] = null;
                            row["expUserName"] = gridLookUpEditProducer.Text;
                            row["expRoleName"] = null;
                            row["producerID"] = gridLookUpEditProducer.EditValue;
                            row["expWarehouseID"] = null;
                            row["expWardID"] = null;
                            row["expWarehouseName"] = null;
                            row["expWarehouseDesc"] = null;
                        }
                    }
                    JournalDataSet.OrderGoodsBindingSource.EndEdit();
                    JournalDataSet.OrderGoodsTableAdapter.Update(JournalDataSet.Instance.OrderGoods);

                    JournalDataSet.OrderGoodsDetailBindingSource.EndEdit();
                    JournalDataSet.OrderGoodsDetailTableAdapter.Update(JournalDataSet.Instance.OrderGoodsDetail);

                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }


            // Сонгосон эм зүйчийг буцаана
            public string getWarehouseID()
            {
                return treeListLookUpEditWarehouse.EditValue.ToString();
            }

            private void showDownloadForm()
            {
                if (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.EditValue.ToString().Equals(""))
                {
                    string text = "Агуулахыг сонгоно уу";
                    treeListLookUpEditWarehouse.ErrorText = text;
                    treeListLookUpEditWarehouse.Focus();
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                    return;
                }
                
                MedicineBalDownForm.Instance.showForm(this);
            }

            // Татсан эмийг авч байна
            public void download(DataRow[] rows)
            {
                if (rows.Length == 0)
                    return;
                
                //if (gridViewDetail.RowCount > 0 && (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.Text.Equals("")))
                    //JournalDataSet.MedicineOrderDetailBindingSource.RemoveCurrent();

                bool exist = false;
                DataRow row = null;
                for (int i = 0; i < rows.Length; i++)
                {   
                    exist = false;
                    for (int j = 0; j < gridViewDetail.RowCount; j++)
                    {
                        row = gridViewDetail.GetDataRow(j);
                        if (row["medicineID"].ToString().Equals(rows[i]["medicineID"].ToString()))
                        {
                            row["count"] = rows[i]["count"];
                            row.EndEdit();
                            exist = true;
                            break;
                        }
                    }
                    if (!exist)
                    {
                        //JournalDataSet.Instance.MedicineOrderDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id();
                        //DataRowView view = (DataRowView)JournalDataSet.MedicineOrderDetailBindingSource.AddNew();
                        //view["medicineID"] = rows[i]["medicineID"];
                        //view["medicineName"] = rows[i]["medicineName"];
                        //view["latinName"] = rows[i]["latinName"];
                        //view["unitType"] = rows[i]["unitType"];
                        //view["shape"] = rows[i]["shape"];
                        //view["validDate"] = rows[i]["validDate"];
                        //view["medicineTypeName"] = rows[i]["medicineTypeName"];
                        //view["count"] = rows[i]["count"];
                        //view.EndEdit();
                        
                        gridViewDetail.FocusedRowHandle = gridViewDetail.RowCount - 1;
                    }
                }
            }

        #endregion

    }
}