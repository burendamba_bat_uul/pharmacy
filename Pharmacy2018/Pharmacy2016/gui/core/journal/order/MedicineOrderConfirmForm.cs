﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;



namespace Pharmacy2016.gui.core.journal.order
{
    /*
     * Эмийн захиалга нэмэх, засах цонх.
     * 
     */

    public partial class MedicineOrderConfirmForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static MedicineOrderConfirmForm INSTANCE = new MedicineOrderConfirmForm();

            public static MedicineOrderConfirmForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int stageNo = 0;

            private int lastStage = 0;

            private int orderType;

            private MedicineOrderConfirmForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                dateEditOrderDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditOrderDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                gridControlDetail.DataSource = JournalDataSet.OrderGoodsDetailBindingSource;

                initBinding();
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }
                    JournalDataSet.SystemUserJournalTableAdapter.Fill(JournalDataSet.Instance.SystemUserJournal, HospitalUtil.getHospitalId(), SystemUtil.ALL_USER);
                    int x = labelControl13.Location.X;
                    if (Convert.ToInt32(((DataRowView)JournalDataSet.OrderGoodsBindingSource.Current)["type"]) == 0)
                    {
                        labelControlWarehouse.Location = new Point(x - 20, 104);
                        labelControlWarehouse.Text = "Эмийн сан*:";
                    }
                    else if (Convert.ToInt32(((DataRowView)JournalDataSet.OrderGoodsBindingSource.Current)["type"]) == 1)
                    {
                        labelControlWarehouse.Location = new Point(x + 3, 104);
                        labelControlWarehouse.Text = "Тасаг*:";
                    }

                    orderType = Convert.ToInt32(((DataRowView)JournalDataSet.OrderGoodsBindingSource.Current)["orderType"]);
                    if (orderType == 0 || orderType == 3)
                    {
                        labelControlExpUser.Text = "Захиалах нярав*:";
                        labelControlExpUser.Location = new Point(x - 50, 130);
                    }
                    else if (orderType == 1)
                    {
                        labelControlExpUser.Text = "Тендер*:";
                        labelControlExpUser.Location = new Point(x - 6, 130);
                    }
                    else if (orderType == 2)
                    {
                        labelControlExpUser.Text = "Гадаад*:";
                        labelControlExpUser.Location = new Point(x - 6, 130);
                    }

                    if (orderType == 0 || orderType == 3)
                    {
                        gridColumnConfirmCount.Visible = false;
                        checkEditChooseAll.Visible = false;
                        labelControl2.Visible = false;
                        gridColumnConf1.Caption = "Шатлал №1";
                        gridColumnConf2.Caption = "Шатлал №2";
                        gridColumnConf3.Caption = "Шатлал №3";
                        gridColumnConf4.Caption = "Шатлал №4";
                        gridColumnConf5.Caption = "Шатлал №5";
                        gridColumnConf1.VisibleIndex = 4;
                        gridColumnConf2.VisibleIndex = 5;
                        gridColumnConf3.VisibleIndex = 6;
                        gridColumnConf4.VisibleIndex = 7;
                        gridColumnConf5.VisibleIndex = 8;
                        for (int i = 0; i < gridViewDetail.RowCount; i++)
                        {
                            if (gridViewDetail.GetRowCellValue(i, gridColumnConf5) != DBNull.Value)
                            {
                                gridColumnConf1.OptionsColumn.ReadOnly = true;
                                gridColumnConf1.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf2.OptionsColumn.ReadOnly = true;
                                gridColumnConf2.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf3.OptionsColumn.ReadOnly = true;
                                gridColumnConf3.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf4.OptionsColumn.ReadOnly = true;
                                gridColumnConf4.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf5.OptionsColumn.ReadOnly = true;
                                gridColumnConf5.AppearanceCell.BackColor = Color.Transparent;
                                break;
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf4) != DBNull.Value)
                            {
                                gridColumnConf1.OptionsColumn.ReadOnly = true;
                                gridColumnConf1.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf2.OptionsColumn.ReadOnly = true;
                                gridColumnConf2.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf3.OptionsColumn.ReadOnly = true;
                                gridColumnConf3.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf4.OptionsColumn.ReadOnly = true;
                                gridColumnConf4.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf5.OptionsColumn.ReadOnly = false;
                                gridColumnConf5.AppearanceCell.BackColor = Color.FromArgb(255, 255, 192);
                                gridColumnConf1.Visible = true;
                                gridColumnConf2.Visible = true;
                                gridColumnConf3.Visible = true;
                                gridColumnConf4.Visible = true;
                                gridColumnConf5.Visible = true;
                                lastStage = 5;
                                gridViewDetail.SetRowCellValue(i, gridColumnConf5, gridViewDetail.GetRowCellValue(i, gridColumnConf4));
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf3) != DBNull.Value)
                            {
                                gridColumnConf1.OptionsColumn.ReadOnly = true;
                                gridColumnConf1.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf2.OptionsColumn.ReadOnly = true;
                                gridColumnConf2.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf3.OptionsColumn.ReadOnly = true;
                                gridColumnConf3.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf4.OptionsColumn.ReadOnly = false;
                                gridColumnConf4.AppearanceCell.BackColor = Color.FromArgb(255, 255, 192);
                                gridColumnConf5.OptionsColumn.ReadOnly = true;
                                gridColumnConf5.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf1.Visible = true;
                                gridColumnConf2.Visible = true;
                                gridColumnConf3.Visible = true;
                                gridColumnConf4.Visible = true;
                                gridColumnConf5.Visible = false;
                                lastStage = 4;
                                gridViewDetail.SetRowCellValue(i, gridColumnConf4, gridViewDetail.GetRowCellValue(i, gridColumnConf3));
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf2) != DBNull.Value)
                            {
                                gridColumnConf1.OptionsColumn.ReadOnly = true;
                                gridColumnConf1.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf2.OptionsColumn.ReadOnly = true;
                                gridColumnConf2.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf3.OptionsColumn.ReadOnly = false;
                                gridColumnConf3.AppearanceCell.BackColor = Color.FromArgb(255, 255, 192);
                                gridColumnConf4.OptionsColumn.ReadOnly = true;
                                gridColumnConf4.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf5.OptionsColumn.ReadOnly = true;
                                gridColumnConf5.AppearanceCell.BackColor = Color.Transparent;         
                                gridColumnConf1.Visible = true;
                                gridColumnConf2.Visible = true;
                                gridColumnConf3.Visible = true;
                                gridColumnConf4.Visible = false;
                                gridColumnConf5.Visible = false;
                                lastStage = 3;
                                gridViewDetail.SetRowCellValue(i, gridColumnConf3, gridViewDetail.GetRowCellValue(i, gridColumnConf2));
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf1) != DBNull.Value)
                            {
                                gridColumnConf1.OptionsColumn.ReadOnly = true;
                                gridColumnConf1.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf2.OptionsColumn.ReadOnly = false;
                                gridColumnConf2.AppearanceCell.BackColor = Color.FromArgb(255, 255, 192);
                                gridColumnConf3.OptionsColumn.ReadOnly = true;
                                gridColumnConf3.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf4.OptionsColumn.ReadOnly = true;
                                gridColumnConf4.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf5.OptionsColumn.ReadOnly = true;
                                gridColumnConf5.AppearanceCell.BackColor = Color.Transparent;                   
                                gridColumnConf1.Visible = true;
                                gridColumnConf2.Visible = true;
                                gridColumnConf3.Visible = false;
                                gridColumnConf4.Visible = false;
                                gridColumnConf5.Visible = false;
                                lastStage = 2;
                                gridViewDetail.SetRowCellValue(i, gridColumnConf2, gridViewDetail.GetRowCellValue(i, gridColumnConf1));
                            }
                            else
                            {
                                gridColumnConf1.OptionsColumn.ReadOnly = false;
                                gridColumnConf1.AppearanceCell.BackColor = Color.FromArgb(255, 255, 192);
                                gridColumnConf2.OptionsColumn.ReadOnly = true;
                                gridColumnConf2.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf3.OptionsColumn.ReadOnly = true;
                                gridColumnConf3.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf4.OptionsColumn.ReadOnly = true;
                                gridColumnConf4.AppearanceCell.BackColor = Color.Transparent;
                                gridColumnConf5.OptionsColumn.ReadOnly = true;
                                gridColumnConf5.AppearanceCell.BackColor = Color.Transparent;                               
                                gridColumnConf1.Visible = true;
                                gridColumnConf2.Visible = false;
                                gridColumnConf3.Visible = false;
                                gridColumnConf4.Visible = false;
                                gridColumnConf5.Visible = false;
                                lastStage = 1;
                                gridViewDetail.SetRowCellValue(i, gridColumnConf1, gridViewDetail.GetRowCellValue(i, gridColumnCount));
                            }
                            DataRow[] user = JournalDataSet.Instance.OrderStage.Select("orderGoodsDetialID = '" + gridViewDetail.GetRowCellValue(i, "id") + "'");
                            for (int j = 0; j < user.Length; j++)
                            {
                                if (gridViewDetail.GetRowCellValue(i, gridColumnConf1) != DBNull.Value && Convert.ToInt32(user[j]["confStage"]) == 1)
                                    gridColumnConf1.Caption = "Баталсан: " + GetUserName(user[j]["userID"].ToString());
                                if (gridViewDetail.GetRowCellValue(i, gridColumnConf2) != DBNull.Value && Convert.ToInt32(user[j]["confStage"]) == 2)
                                    gridColumnConf2.Caption = "Баталсан: " + GetUserName(user[j]["userID"].ToString());
                                if (gridViewDetail.GetRowCellValue(i, gridColumnConf3) != DBNull.Value && Convert.ToInt32(user[j]["confStage"]) == 3)
                                    gridColumnConf3.Caption = "Баталсан: " + GetUserName(user[j]["userID"].ToString());
                                if (gridViewDetail.GetRowCellValue(i, gridColumnConf4) != DBNull.Value && Convert.ToInt32(user[j]["confStage"]) == 4)
                                    gridColumnConf4.Caption = "Баталсан: " + GetUserName(user[j]["userID"].ToString());
                                if (gridViewDetail.GetRowCellValue(i, gridColumnConf5) != DBNull.Value && Convert.ToInt32(user[j]["confStage"]) == 5)
                                    gridColumnConf5.Caption = "Баталсан: " + GetUserName(user[j]["userID"].ToString());
                            }
                        }
                    }
                    else if (orderType == 1 || orderType == 2)
                    {
                        gridColumnConfirmCount.Visible = true;
                        checkEditChooseAll.Visible = true;
                        labelControl2.Visible = true;
                        gridColumnConf1.Visible = false;
                        gridColumnConf2.Visible = false;
                        gridColumnConf3.Visible = false;
                        gridColumnConf4.Visible = false;
                        gridColumnConf5.Visible = false;
                    }

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void initBinding()
            {
                textEditID.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "id", true));
                dateEditOrderDate.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "date", true));
                textEditUserName.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "userName", true));
                textEditWarehouseName.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "warehouseName", true));
                textEditExpUserName.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "expUserName", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "description", true));
                checkEditIsConfirm.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "isConfirm", true));
            }


            private void clearData()
            {
                checkEditChooseAll.Checked = false;

                JournalDataSet.OrderGoodsBindingSource.CancelEdit();
                JournalDataSet.OrderGoodsDetailBindingSource.CancelEdit();
                JournalDataSet.Instance.OrderGoods.RejectChanges();
                JournalDataSet.Instance.OrderGoodsDetail.RejectChanges();
            }

            private void clearBinding()
            {
                textEditID.DataBindings.Clear();
                dateEditOrderDate.DataBindings.Clear();
                textEditUserName.DataBindings.Clear();
                textEditWarehouseName.DataBindings.Clear();
                textEditExpUserName.DataBindings.Clear();
                checkEditIsConfirm.DataBindings.Clear();
                memoEditDescription.DataBindings.Clear();
            }

            private void MedicineOrderInUpForm_Shown(object sender, EventArgs e)
            {
                checkEditIsConfirm.Focus();
            }

        #endregion

        #region Формын event

            private void checkEditChooseAll_CheckedChanged(object sender, EventArgs e)
            {
                chooseAll();
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private void reload()
            {
                
                if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                {

                    JournalDataSet.OrderGoodsTableAdapter.Fill(JournalDataSet.Instance.OrderGoods, HospitalUtil.getHospitalId(), UserUtil.getUserId(), MedicineOrderForm.Instance.getStartDate().ToString(), MedicineOrderForm.Instance.getEndDate().ToString());
                    JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), MedicineOrderForm.Instance.getStartDate().ToString(), MedicineOrderForm.Instance.getEndDate().ToString());
                }
                else
                {
                    JournalDataSet.OrderGoodsTableAdapter.Fill(JournalDataSet.Instance.OrderGoods, HospitalUtil.getHospitalId(), "", MedicineOrderForm.Instance.getStartDate().ToString(), MedicineOrderForm.Instance.getEndDate().ToString());
                    JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(), "", MedicineOrderForm.Instance.getStartDate().ToString(), MedicineOrderForm.Instance.getEndDate().ToString());
                }
                JournalDataSet.OrderStageTableAdapter.Fill(JournalDataSet.Instance.OrderStage);
            }

            private void chooseAll()
            {
                if (checkEditChooseAll.Checked)
                {
                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        if (orderType == 1 || orderType == 2)
                        {
                            gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDetail.GetRowCellValue(i, gridColumnCount));
                        }
                        else if (orderType == 0 || orderType == 3)
                        {
                            if (gridViewDetail.GetRowCellValue(i, gridColumnConf5) != DBNull.Value)
                            {
                                gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDetail.GetRowCellValue(i, gridColumnConf5));
                                stageNo = 5;
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf4) != DBNull.Value)
                            {
                                gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDetail.GetRowCellValue(i, gridColumnConf4));
                                stageNo = 4;
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf3) != DBNull.Value)
                            {
                                gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDetail.GetRowCellValue(i, gridColumnConf3));
                                stageNo = 3;
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf2) != DBNull.Value)
                            {
                                gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDetail.GetRowCellValue(i, gridColumnConf2));
                                stageNo = 2;
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf1) != DBNull.Value)
                            {
                                gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDetail.GetRowCellValue(i, gridColumnConf1));
                                stageNo = 1;
                            }
                        }
                        
                    }
                    //for (int i = 0; i < JournalDataSet.OrderGoodsOtherDetailBindingSource.Count; i++)
                    //{
                    //    DataRowView view = (DataRowView)JournalDataSet.OrderGoodsOtherDetailBindingSource[i];
                    //    if (Convert.ToDouble(view["confirmCount"]) == 0)
                    //        view["confirmCount"] = view["count"];
                    //    view.EndEdit();
                    //}
                }
                else
                {
                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, 0);
                    }

                    //for (int i = 0; i < JournalDataSet.OrderGoodsOtherDetailBindingSource.Count; i++)
                    //{
                    //    DataRowView view = (DataRowView)JournalDataSet.OrderGoodsOtherDetailBindingSource[i];
                    //    view["confirmCount"] = 0;
                    //    view.EndEdit();
                    //}
                }
            }

            private bool check()
            {
                //Instance.Validate();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (isRight && (orderType == 0 || orderType == 3) && checkEditIsConfirm.Checked)
                {
                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        DataRow detial = gridViewDetail.GetDataRow(i);
                        if (JournalDataSet.Instance.OrderStage.Select("orderGoodsDetialID = '" + detial["id"] + "' AND userID = '" + UserUtil.getUserId() + "'").Count() > 0)
                        {
                            errorText = "Хэрэглэгч дахин батлах боломжгүй!";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            isRight = false;
                            gridViewDetail.Focus();
                            break;
                        }
                        if(detial[""+ lastStage +""] == DBNull.Value)
                        {
                            errorText = "Батлах тоог оруулна уу!";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            isRight = false;
                            gridViewDetail.Focus();
                            break;
                        }
                        //if (Convert.ToDouble(gridViewDetail.GetRowCellValue(i, gridColumnConfirmCount)) == 0)
                        //{
                        //    errorText = "Батлах тоог оруулна уу!";
                        //    gridViewDetail.SetColumnError(gridColumnConfirmCount, errorText);
                        //    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        //    isRight = false;
                        //    break;
                        //}
                        if (gridViewDetail.GetRowCellValue(i, gridColumnCount) != null && gridViewDetail.GetRowCellValue(i, gridColumnCount) != DBNull.Value && Convert.ToDouble(gridViewDetail.GetRowCellValue(i, gridColumnCount)) > 0 &&
                                Convert.ToDouble(gridViewDetail.GetRowCellValue(i, gridColumnCount)) < Convert.ToDouble(detial["" + lastStage + ""]))
                        {
                            errorText = "Батлах тоо захиалсан тооноос их байж болохгүй!";
                            gridViewDetail.SetColumnError(gridColumnCount, errorText);
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            isRight = false;
                            break;
                        }
                        if (Convert.ToDouble(detial["" + lastStage + ""]) < Convert.ToDouble(gridViewDetail.GetRowCellValue(i, gridColumnConfirmCount)))
                        {
                            errorText = "Батлах тоо захиалсан тооноос их байж болохгүй!";
                            gridViewDetail.SetColumnError(gridColumnConfirmCount, errorText);
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            isRight = false;
                            break;
                        }
                    }
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void AddRows()
            {
                if (stageNo != 0)
                {
                    for (int i = 0; i < gridViewDetail.RowCount; i++)
                    {
                        DataRow row = gridViewDetail.GetDataRow(i);
                        DataRow newRow = JournalDataSet.Instance.OrderStage.NewRow();
                        newRow["id"] = getNotExistId();
                        newRow["confCount"] = row["" + stageNo + ""];
                        newRow["orderGoodsDetialID"] = row["id"];
                        newRow["confStage"] = stageNo;
                        newRow["confDate"] = DateTime.Now;
                        newRow["userID"] = UserUtil.getUserId();
                        if (JournalDataSet.Instance.OrderStage.Select("orderGoodsDetialID = '" + newRow["orderGoodsDetialID"] + "' AND confStage = '" + newRow["confStage"] + "'").Count() == 0)
                        {
                            JournalDataSet.Instance.OrderStage.Rows.Add(newRow);
                        }
                    }
                }
            }

            private string getNotExistId()
            {
                string newID = null;
                DataRow[] rows = null;
                do
                {
                    newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    rows = JournalDataSet.Instance.OrderStage.Select("id = '" + newID + "'");
                } while (rows != null && rows.Length > 0);

                return newID;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    if ((orderType == 0 || orderType == 3) && checkEditIsConfirm.Checked)
                    {
                        for (int i = 0; i < gridViewDetail.RowCount; i++)
                        {
                        
                            if (gridViewDetail.GetRowCellValue(i, gridColumnConf5) != DBNull.Value)
                            {
                                gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDetail.GetRowCellValue(i, gridColumnConf5));
                                stageNo = 5;
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf4) != DBNull.Value)
                            {
                                gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDetail.GetRowCellValue(i, gridColumnConf4));
                                stageNo = 4;
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf3) != DBNull.Value)
                            {
                                gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDetail.GetRowCellValue(i, gridColumnConf3));
                                stageNo = 3;
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf2) != DBNull.Value)
                            {
                                gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDetail.GetRowCellValue(i, gridColumnConf2));
                                stageNo = 2;
                            }
                            else if (gridViewDetail.GetRowCellValue(i, gridColumnConf1) != DBNull.Value)
                            {
                                gridViewDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDetail.GetRowCellValue(i, gridColumnConf1));
                                stageNo = 1;
                            }
                        }
                        AddRows(); 
                    }
                    JournalDataSet.OrderGoodsBindingSource.EndEdit();
                    JournalDataSet.OrderGoodsTableAdapter.Update(JournalDataSet.Instance.OrderGoods);
                    JournalDataSet.OrderGoodsDetailBindingSource.EndEdit();
                    JournalDataSet.OrderGoodsDetailTableAdapter.Update(JournalDataSet.Instance.OrderGoodsDetail);
                    JournalDataSet.OrderStageTableAdapter.Update(JournalDataSet.Instance.OrderStage);
                    ProgramUtil.closeWaitDialog();
                    reload();
                    Close();
                }
            }

            private string GetUserName(string userID)
            {  
                string userName = "";
                for (int i = 0; i < JournalDataSet.Instance.SystemUserJournal.Count; i++)
                {
                    DataRow user = JournalDataSet.Instance.SystemUserJournal.Rows[i];
                    if(user["id"].ToString().Equals(userID)){
                        userName = user["fullName"].ToString();
                    }
                }
                if (!string.IsNullOrEmpty(userName))
                    return userName;
                else
                    return "Admin";
            }

        #endregion

    }
}