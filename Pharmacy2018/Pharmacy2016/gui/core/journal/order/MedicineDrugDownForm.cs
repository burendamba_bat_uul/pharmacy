﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.plan;


namespace Pharmacy2016.gui.core.journal.order
{
    /*
     * Эмийн түүврийг захиалгын хуудас руу татах цонх.
     * 
     */

    public partial class MedicineDrugDownForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static MedicineDrugDownForm INSTANCE = new MedicineDrugDownForm();

        public static MedicineDrugDownForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private DataRowView docView;

        private int TenderOrDrug;

        private MedicineDrugDownForm()
        {

        }


        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            dateEditOrderDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditOrderDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;
            dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
            dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

            initDataSource();
            initError();
            initBinding();
        }

        private void initDataSource()
        {
            repositoryItemGridLookUpEditMed.DataSource = JournalDataSet.MedicineBalBindingSource;
            gridLookUpEditUser.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
            treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
            treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
            gridLookUpEditExpUser.Properties.DataSource = InformationDataSet.SystemUserOtherBindingSource;
            gridLookUpEditTender.Properties.DataSource = JournalDataSet.Instance.Tender;
        }

        private void initError()
        {
            dateEditOrderDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditExpUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            dateEditEnd.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm(int type, XtraForm form)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }
                TenderOrDrug = type;
                string id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                JournalDataSet.Instance.OrderGoods.idColumn.DefaultValue = id;
                JournalDataSet.Instance.OrderGoodsDetail.orderGoodsIDColumn.DefaultValue = id;
                docView = (DataRowView)JournalDataSet.OrderGoodsBindingSource.AddNew();
                DateTime now = DateTime.Now;
                docView["id"] = id;
                docView["date"] = now;
                dateEditOrderDate.DateTime = now;
                textEditID.Text = id;
                radioGroupType.SelectedIndex = 1;
                dateEditStart.DateTime = now;
                dateEditEnd.DateTime = now;
                int x = labelControlWarehouse.Location.X;
                if (type == 1)
                {
                    gridControlDrug.DataSource = JournalDataSet.Instance.DrugDetailSum;
                    Instance.Text = "Эмийн түүвэр татах";
                    labelControlTender.Location = new Point(123, 173);
                    gridLookUpEditTender.Location = new Point(x + 105, 144);
                    gridLookUpEditExpUser.Location = new Point(220, 170);

                    gridLookUpEditTender.Visible = false;
                    gridLookUpEditExpUser.Visible = true;   
                    labelControlTender.Text = "Захиалах нярав*:";
                    dateEditStart.Visible = true;
                    dateEditEnd.Visible = true;
                    simpleButtonReload.Visible = true;
                    bandedGridColumnPackage.Visible = false;
                    bandedGridColumnMedicineName.Visible = false;
                    bandedGridColumnMedName.Visible = true;
                    bandedGridColumnWard.Visible = true;
                    bandedGridColumnCount.OptionsColumn.ReadOnly = true;
                    bandedGridColumnCount.AppearanceCell.BackColor = Color.Transparent;
                }
                else if (type == 2)
                {
                    JournalDataSet.Instance.TenderMedicine.Clear();
                    gridControlDrug.DataSource = JournalDataSet.Instance.TenderMedicine;
                    Instance.Text = "Тендер татах";
                    labelControlTender.Location = new Point(167, 173);
                    gridLookUpEditTender.Location = new Point(220, 170);
                    gridLookUpEditExpUser.Location = new Point(x + 105, 144);

                    gridLookUpEditTender.Visible = true;
                    gridLookUpEditExpUser.Visible = false;
                    labelControlTender.Text = "Тендер*:";
                    dateEditStart.Visible = false;
                    dateEditEnd.Visible = false;
                    simpleButtonReload.Visible = false;
                    bandedGridColumnPackage.Visible = true;
                    bandedGridColumnMedicineName.Visible = true;
                    bandedGridColumnMedName.Visible = false;
                    bandedGridColumnWard.Visible = false;
                    bandedGridColumnCount.OptionsColumn.ReadOnly = false;
                    bandedGridColumnCount.AppearanceCell.BackColor = Color.FromArgb(255, 255, 192);
                    reloadTender(true);
                }
                reload(false);
                ShowDialog(form);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Цонх ачааллахад алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clearData();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void initBinding()
        {
            textEditID.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "id", true));
            dateEditOrderDate.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "date", true));
            gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "userID", true));
            radioGroupType.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "type", true));
            treeListLookUpEditWarehouse.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "warehouseID", true));
            treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "wardID", true));
            gridLookUpEditExpUser.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "expUserID", true));
            memoEditDescription.DataBindings.Add(new Binding("EditValue", JournalDataSet.OrderGoodsBindingSource, "description", true));
        }

        private void clearData()
        {
            clearErrorText();
            gridLookUpEditTender.EditValue = null;

            InformationDataSet.SystemUserBindingSource.Filter = "";
            JournalDataSet.WarehouseOtherBindingSource.Filter = "";
            JournalDataSet.WardOtherBindingSource.Filter = "";

            JournalDataSet.Instance.DrugDetailSum.Clear();
            JournalDataSet.Instance.Tender.Clear();
            JournalDataSet.Instance.TenderMedicine.Clear();
            JournalDataSet.Instance.TenderOther.Clear();
            JournalDataSet.OrderGoodsBindingSource.CancelEdit();
            JournalDataSet.OrderGoodsDetailBindingSource.CancelEdit();
            JournalDataSet.Instance.OrderGoods.RejectChanges();
            JournalDataSet.Instance.OrderGoodsDetail.RejectChanges();
        }

        private void clearErrorText()
        {
            dateEditOrderDate.ErrorText = "";
            gridLookUpEditUser.ErrorText = "";
            treeListLookUpEditWarehouse.ErrorText = "";
            treeListLookUpEditWard.ErrorText = "";
            dateEditStart.ErrorText = "";
            dateEditEnd.ErrorText = "";
        }

        private void MedicineDrugDownForm_Shown(object sender, EventArgs e)
        {
            if (UserUtil.getUserRoleId() != RoleUtil.ADMIN_ID)
                InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";

            if (UserUtil.getUserRoleId() != RoleUtil.ADMIN_ID)
            {
                gridLookUpEditUser.EditValue = UserUtil.getUserId();
                docView["userID"] = UserUtil.getUserId();
                docView.EndEdit();
                treeListLookUpEditWarehouse.Focus();
            }
            else
            {
                gridLookUpEditUser.Focus();
            }
        }

        #endregion

        #region Формын event

        private void dateEditOrderDate_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    dateEditOrderDate.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditUser.Focus();
                }
            }
        }

        private void gridLookUpEditUser_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditUser.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    radioGroupType.Focus();
                }
            }
        }

        private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible && gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null)
            {
                JournalDataSet.WarehouseOtherBindingSource.Filter = UserUtil.getUserWarehouse(gridLookUpEditUser.EditValue.ToString());
                JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(gridLookUpEditUser.EditValue.ToString());
            }
        }

        private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeType();
        }

        private void treeListLookUpEditWarehouse_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    ((TreeListLookUpEdit)sender).ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditExpUser.Focus();
                }
            }
        }

        private void gridLookUpEditExpUser_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditExpUser.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    memoEditDescription.Focus();
                }
            }
        }


        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            reloadDrug();
        }

        private void simpleButtonLoad_Click(object sender, EventArgs e)
        {
            reload(true);
        }

        private void simpleButtonSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void gridLookUpEditTender_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible && gridLookUpEditTender.EditValue != DBNull.Value && gridLookUpEditTender.EditValue != null && !gridLookUpEditTender.Equals(""))
            {
                changeTender();
            }
        }

        private void dateEditOrderDate_EditValueChanged(object sender, EventArgs e)
        {
            reloadTender(true);
        }

        #endregion

        #region Формын функц

        private void reload(bool isReload)
        {
            InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.PHARMACIST);
            InformationDataSet.SystemUserOtherTableAdapter.Fill(InformationDataSet.Instance.SystemUserOther, HospitalUtil.getHospitalId(), SystemUtil.STOREMAN_AND_PHARMACIST);
            if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
            if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
        }

        private void changeType()
        {
            int x = labelControl14.Location.X;
            if (radioGroupType.SelectedIndex == 0)
            {
                labelControlWarehouse.Location = new Point(x + 38, 147);
                treeListLookUpEditWard.Location = new Point(157, 12);
                treeListLookUpEditWarehouse.Location = new Point(x + 105, 144);

                treeListLookUpEditWard.Visible = false;
                treeListLookUpEditWarehouse.Visible = true;
                labelControlWarehouse.Text = "Эмийн сан*:";
            }
            else if (radioGroupType.SelectedIndex == 1)
            {
                labelControlWarehouse.Location = new Point(x + 61, 147);
                treeListLookUpEditWard.Location = new Point(x + 105, 144);
                treeListLookUpEditWarehouse.Location = new Point(157, 12);

                treeListLookUpEditWard.Visible = true;
                treeListLookUpEditWarehouse.Visible = false;
                labelControlWarehouse.Text = "Тасаг*:";
            }
        }

        private bool checkDrug()
        {
            bool isRight = true;
            string text = "", errorText;

            if (dateEditStart.Text == "")
            {
                errorText = "Эхлэх огноог оруулна уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }
            if (dateEditEnd.Text == "")
            {
                errorText = "Дуусах огноог оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditEnd.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    dateEditEnd.Focus();
                }
            }
            if (gridLookUpEditUser.EditValue == null || gridLookUpEditUser.EditValue == DBNull.Value)
            {
                errorText = "Эм зүйчийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditUser.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
            }
            if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.EditValue == DBNull.Value))
            {
                errorText = "Тасаг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWard.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWard.Focus();
                }
            }
            if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
            {
                errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }
            if (!isRight)
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        private void reloadDrug()
        {
            if (checkDrug())
            {
                if(radioGroupType.SelectedIndex == 0){
                JournalDataSet.DrugJournalSumTableAdapter.Fill(JournalDataSet.Instance.DrugJournalSum, HospitalUtil.getHospitalId(), gridLookUpEditUser.EditValue.ToString(), dateEditStart.Text, dateEditEnd.Text, "");
                JournalDataSet.DrugDetailSumTableAdapter.Fill(JournalDataSet.Instance.DrugDetailSum, HospitalUtil.getHospitalId(), gridLookUpEditUser.EditValue.ToString(), dateEditStart.Text, dateEditEnd.Text, "");
                }
                else
                {
                    JournalDataSet.DrugJournalSumTableAdapter.Fill(JournalDataSet.Instance.DrugJournalSum, HospitalUtil.getHospitalId(), gridLookUpEditUser.EditValue.ToString(), dateEditStart.Text, dateEditEnd.Text, getWardID());
                    JournalDataSet.DrugDetailSumTableAdapter.Fill(JournalDataSet.Instance.DrugDetailSum, HospitalUtil.getHospitalId(), gridLookUpEditUser.EditValue.ToString(), dateEditStart.Text, dateEditEnd.Text, getWardID());
                }
            }
        }

        private string getWardID()
        {
            string journalID = "";
            for (int i = 0; i < JournalDataSet.WardOtherBindingSource.Count; i++)
            {
                DataRowView ward = (DataRowView)JournalDataSet.WardOtherBindingSource[i];
                if (treeListLookUpEditWard.EditValue.ToString().Equals(ward["pid"].ToString()))
                    journalID += ward["id"] + "~";
            }    
            if (!journalID.Equals(""))
                journalID = journalID.Substring(0, journalID.Length - 1);

            if (!string.IsNullOrEmpty(journalID))
                return journalID;
            else
                return treeListLookUpEditWard.EditValue.ToString();
        }

        private bool check()
        {
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();

            if (dateEditOrderDate.EditValue == DBNull.Value || dateEditOrderDate.EditValue == null || dateEditOrderDate.EditValue.ToString().Equals(""))
            {
                errorText = "Огноог оруулна уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditOrderDate.ErrorText = errorText;
                isRight = false;
                dateEditOrderDate.Focus();
            }
            if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.Text.Equals(""))
            {
                errorText = "Няравыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditUser.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
            }
            if (radioGroupType.SelectedIndex == 0 && (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.Text.Equals("")))
            {
                errorText = "Агуулахыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWarehouse.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWarehouse.Focus();
                }
            }
            if (radioGroupType.SelectedIndex == 1 && (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals("")))
            {
                errorText = "Тасгийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWard.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWard.Focus();
                }
            }
            if (TenderOrDrug == 1 && (gridLookUpEditExpUser.EditValue == DBNull.Value || gridLookUpEditExpUser.EditValue == null || gridLookUpEditExpUser.Text.Equals("")))
            {
                errorText = "Захиалга өгөх няравыг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditExpUser.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditExpUser.Focus();
                }
            }
            if (TenderOrDrug == 2 && (gridLookUpEditTender.EditValue == DBNull.Value || gridLookUpEditTender.EditValue == null || gridLookUpEditTender.Text.Equals("")))
            {
                errorText = "Тендер сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditTender.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditTender.Focus();
                }
            }
            if (isRight && gridViewDrug.RowCount == 0)
            {
                isRight = false;
                errorText = "Эмийн жагсаалт хоосон байна";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
            }

            if (!isRight && !text.Equals(""))
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        private void save()
        {
            ProgramUtil.IsShowPopup = true;
            if (check())
            {
                if (ProgramUtil.checkLockMedicine(dateEditOrderDate.EditValue.ToString()))
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);

                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                    DataRowView expUser = (DataRowView)gridLookUpEditExpUser.GetSelectedDataRow();
                    DataRowView tender = (DataRowView)gridLookUpEditTender.GetSelectedDataRow();

                    if (radioGroupType.SelectedIndex == 0)
                    {
                        docView["wardID"] = null;
                        docView["warehouseName"] = warehouse["name"];
                        docView["warehouseDesc"] = warehouse["description"];
                    }
                    else if (radioGroupType.SelectedIndex == 1)
                    {
                        docView["warehouseID"] = null;
                        docView["warehouseName"] = ward["name"];
                        docView["warehouseDesc"] = ward["description"];
                    }

                    docView["userName"] = user["fullName"];
                    docView["roleName"] = user["roleName"];
                    if (TenderOrDrug == 1)
                    {
                        docView["orderType"] = 3;
                        docView["expUserName"] = expUser["fullName"];

                        docView["tenderID"] = null;
                        docView["tenderNumber"] = null;
                        docView["supplierID"] = null;
                    }

                    else if (TenderOrDrug == 2)
                    {
                        docView["expUserID"] = null;
                        docView["tenderID"] = tender["tenderID"];
                        docView["orderType"] = 1;
                        docView["supplierID"] = tender["supplierID"];
                        docView["tenderNumber"] = tender["tenderNumber"];
                        docView["expUserName"] = tender["supplierName"];
                    }
                    docView.EndEdit();

                    long newID = Convert.ToInt64(JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId()));
                    for (int i = 0; i < gridViewDrug.RowCount; i++, newID++)
                    {
                        DataRow row = gridViewDrug.GetDataRow(i);
                        JournalDataSet.Instance.OrderGoodsDetail.idColumn.DefaultValue = newID;
                        DataRowView view = (DataRowView)JournalDataSet.OrderGoodsDetailBindingSource.AddNew();

                        view["date"] = dateEditOrderDate.EditValue;
                        view["description"] = memoEditDescription.EditValue;
                        view["userID"] = gridLookUpEditUser.EditValue;
                        view["type"] = radioGroupType.SelectedIndex;
                        view["userName"] = user["fullName"];
                        view["roleName"] = user["roleName"];
                        if (TenderOrDrug == 1)
                        {
                            view["tenderID"] = null;
                            view["tenderNumber"] = null;
                            view["supplierID"] = null;
                            view["orderType"] = 3;
                            view["expUserName"] = expUser["fullName"];
                            view["medicineID"] = row["medicineID"];
                            view["medicineName"] = row["medicineName"];
                        }
                        else
                        {
                            view["orderType"] = 1;
                            view["supplierID"] = tender["supplierID"];
                            view["expUserName"] = tender["supplierName"];
                            view["medicineID"] = row["id"];
                            view["medicineName"] = row["name"];
                        }

                        if (radioGroupType.SelectedIndex == 0)
                        {
                            view["wardID"] = null;
                            view["warehouseID"] = treeListLookUpEditWarehouse.EditValue;
                            view["warehouseName"] = warehouse["name"];
                            view["warehouseDesc"] = warehouse["description"];
                        }
                        else if (radioGroupType.SelectedIndex == 1)
                        {
                            view["warehouseID"] = null;
                            view["wardID"] = treeListLookUpEditWard.EditValue;
                            view["warehouseName"] = ward["name"];
                            view["warehouseDesc"] = ward["description"];
                        }


                        view["medicineTypeName"] = row["medicineTypeName"];
                        view["latinName"] = row["latinName"];
                        view["unitType"] = row["unitType"];
                        view["shape"] = row["shape"];
                        view["quantity"] = row["quantity"];
                        view["validDate"] = row["validDate"];
                        view["count"] = row["count"];
                        view.EndEdit();
                    }
                    JournalDataSet.OrderGoodsTableAdapter.Update(JournalDataSet.Instance.OrderGoods);
                    JournalDataSet.OrderGoodsDetailTableAdapter.Update(JournalDataSet.Instance.OrderGoodsDetail);

                    if (TenderOrDrug == 1)
                    {
                        string journalID = "";
                        for (int i = 0; i < JournalDataSet.Instance.DrugJournalSum.Rows.Count; i++)
                            journalID += JournalDataSet.Instance.DrugJournalSum.Rows[i]["journalID"] + "~";

                        if (!journalID.Equals(""))
                            journalID = journalID.Substring(0, journalID.Length - 1);

                        JournalDataSet.QueriesTableAdapter.update_drug_journal_order(HospitalUtil.getHospitalId(), journalID, docView["id"].ToString(), true);
                    }
                    ProgramUtil.closeWaitDialog();
                    Close();
                }
                else
                {
                    XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
                } 
            }
        }

        private void reloadTender(bool isReload)
        {
            if (isReload && JournalDataSet.Instance.TenderOther.Rows.Count == 0 && dateEditOrderDate.Text != "")
            {
                JournalDataSet.TenderOtherTableAdapter.Fill(JournalDataSet.Instance.TenderOther, HospitalUtil.getHospitalId(), dateEditOrderDate.Text);

                List<string> list = new List<string>();
                JournalDataSet.Instance.Tender.Clear();
                for (int i = 0; i < JournalDataSet.Instance.TenderOther.Rows.Count; i++)
                {
                    DataRow row = JournalDataSet.Instance.TenderOther.Rows[i];
                    if (list.IndexOf(row["tenderID"].ToString()) == -1)
                    {
                        list.Add(row["tenderID"].ToString());

                        DataRow newRow = JournalDataSet.Instance.Tender.NewRow();
                        newRow["tenderID"] = row["tenderID"];
                        newRow["date"] = row["date"];
                        newRow["tenderNumber"] = row["tenderNumber"];
                        newRow["supplierID"] = row["supplierID"];
                        newRow["supplierName"] = row["supplierName"];
                        newRow.EndEdit();
                        JournalDataSet.Instance.Tender.Rows.Add(newRow);
                    }
                }
            }
        }

        private void changeTender()
        {
            PlanDataSet.Instance.TenderPlan.Clear();
            PlanDataSet.Instance.TenderPlanDetail.Clear();
            PlanDataSet.TenderPlanTableAdapter.Fill(PlanDataSet.Instance.TenderPlan, HospitalUtil.getHospitalId(), dateEditOrderDate.DateTime.Year.ToString()+"-01-01", dateEditOrderDate.Text);
            PlanDataSet.TenderPlanDetailTableAdapter.Fill(PlanDataSet.Instance.TenderPlanDetail, HospitalUtil.getHospitalId(), dateEditOrderDate.DateTime.Year.ToString()+"-01-01", dateEditOrderDate.Text);
            JournalDataSet.Instance.TenderMedicine.Clear();
            DataRow[] rows = JournalDataSet.Instance.TenderOther.Select("tenderID = '" + gridLookUpEditTender.EditValue + "'");
            for (int i = 0; i < rows.Length; i++)
            {
                DataRow newRow = JournalDataSet.Instance.TenderMedicine.NewRow();
                newRow["packageID"] = rows[i]["packageID"];
                newRow["packageCount"] = rows[i]["packageCount"];
                newRow["supplierName"] = rows[i]["supplierName"];
                newRow["packageName"] = rows[i]["packageName"];
                newRow["id"] = rows[i]["id"];
                newRow["price"] = rows[i]["price"];
                DataRow[] row_plan = PlanDataSet.Instance.TenderPlan.Select("packageID = '" + rows[i]["packageID"]+ "'");
                if (row_plan.Length > 0)
                {                    
                    DataRow[] row_plandetail = PlanDataSet.Instance.TenderPlanDetail.Select("tenderPlanID = '" + row_plan[0]["id"] +
                        "' AND medicineID='" + rows[i]["id"] + "' AND hospitalID='" + HospitalUtil.getHospitalId() + "'");
                    if(row_plandetail.Length>0)
                    {
                        switch(dateEditOrderDate.DateTime.Month)
                        {
                            case(1) : newRow["count"] = row_plandetail[0]["jan"]; break;
                            case(2) : newRow["count"] = row_plandetail[0]["feb"]; break;
                            case(3) : newRow["count"] = row_plandetail[0]["mar"]; break;
                            case(4) : newRow["count"] = row_plandetail[0]["apr"]; break;
                            case(5) : newRow["count"] = row_plandetail[0]["may"]; break;
                            case(6) : newRow["count"] = row_plandetail[0]["jun"]; break;
                            case(7) : newRow["count"] = row_plandetail[0]["jul"]; break;
                            case(8) : newRow["count"] = row_plandetail[0]["aug"]; break;
                            case(9) : newRow["count"] = row_plandetail[0]["sep"]; break;
                            case(10) : newRow["count"] = row_plandetail[0]["oct"]; break;
                            case(11) : newRow["count"] = row_plandetail[0]["nov"]; break;
                            case(12) : newRow["count"] = row_plandetail[0]["dec"]; break;
                        }
                    }
                    else
                    {
                        newRow["count"] = 0;
                    }
                }
                else
                {
                    newRow["count"] = 0;
                }                
                newRow["name"] = rows[i]["name"];
                newRow["latinName"] = rows[i]["latinName"];
                newRow["medicineTypeName"] = rows[i]["medicineTypeName"];
                newRow["unitType"] = rows[i]["unitType"];
                newRow["shape"] = rows[i]["shape"];
                newRow["quantity"] = rows[i]["quantity"];
                newRow["validDate"] = rows[i]["validDate"];
                newRow["barcode"] = rows[i]["barcode"];
                newRow.EndEdit();
                JournalDataSet.Instance.TenderMedicine.Rows.Add(newRow);

            }
            PlanDataSet.Instance.TenderPlan.Clear();
            PlanDataSet.Instance.TenderPlanDetail.Clear();
        }

        #endregion

        
    }
}