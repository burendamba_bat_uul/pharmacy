﻿namespace Pharmacy2016.gui.core.journal.order
{
    partial class MedicineDrugDownForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicineDrugDownForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridLookUpEditTender = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlTender = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditExpUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButtonLoad = new DevExpress.XtraEditors.SimpleButton();
            this.treeListLookUpEditWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList2 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn5 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupType = new DevExpress.XtraEditors.RadioGroup();
            this.treeListLookUpEditWarehouse = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControlWarehouse = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEditID = new DevExpress.XtraEditors.TextEdit();
            this.dateEditOrderDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlDrug = new DevExpress.XtraGrid.GridControl();
            this.gridViewDrug = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnWard = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnMedName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn6 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnPackage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnMedicineName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn16 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn13 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn7 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumn17 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemGridLookUpEditMed = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditExpUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDrug)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDrug)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditMed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridLookUpEditTender);
            this.panelControl1.Controls.Add(this.labelControlTender);
            this.panelControl1.Controls.Add(this.gridLookUpEditExpUser);
            this.panelControl1.Controls.Add(this.simpleButtonLoad);
            this.panelControl1.Controls.Add(this.treeListLookUpEditWard);
            this.panelControl1.Controls.Add(this.labelControl14);
            this.panelControl1.Controls.Add(this.radioGroupType);
            this.panelControl1.Controls.Add(this.treeListLookUpEditWarehouse);
            this.panelControl1.Controls.Add(this.labelControlWarehouse);
            this.panelControl1.Controls.Add(this.gridLookUpEditUser);
            this.panelControl1.Controls.Add(this.simpleButtonCancel);
            this.panelControl1.Controls.Add(this.simpleButtonSave);
            this.panelControl1.Controls.Add(this.labelControl13);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.memoEditDescription);
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.textEditID);
            this.panelControl1.Controls.Add(this.dateEditOrderDate);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.gridControlDrug);
            this.panelControl1.Controls.Add(this.simpleButtonReload);
            this.panelControl1.Controls.Add(this.dateEditStart);
            this.panelControl1.Controls.Add(this.dateEditEnd);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(584, 561);
            this.panelControl1.TabIndex = 0;
            // 
            // gridLookUpEditTender
            // 
            this.gridLookUpEditTender.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditTender.Location = new System.Drawing.Point(14, 243);
            this.gridLookUpEditTender.Name = "gridLookUpEditTender";
            this.gridLookUpEditTender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditTender.Properties.DisplayMember = "tenderNumber";
            this.gridLookUpEditTender.Properties.NullText = "";
            this.gridLookUpEditTender.Properties.ValueMember = "tenderID";
            this.gridLookUpEditTender.Properties.View = this.gridView6;
            this.gridLookUpEditTender.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditTender.TabIndex = 76;
            this.gridLookUpEditTender.EditValueChanged += new System.EventHandler(this.gridLookUpEditTender_EditValueChanged);
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ShowAutoFilterRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Дугаар";
            this.gridColumn15.CustomizationCaption = "Тендерийн дугаар";
            this.gridColumn15.FieldName = "tenderNumber";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 133;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Огноо";
            this.gridColumn16.CustomizationCaption = "Тендерийн огноо";
            this.gridColumn16.FieldName = "date";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 1;
            this.gridColumn16.Width = 147;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Бэлтгэн нийлүүлэгч";
            this.gridColumn17.CustomizationCaption = "Бэлтгэн нийлүүлэгч";
            this.gridColumn17.FieldName = "supplierName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 2;
            this.gridColumn17.Width = 651;
            // 
            // labelControlTender
            // 
            this.labelControlTender.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlTender.Location = new System.Drawing.Point(123, 173);
            this.labelControlTender.Name = "labelControlTender";
            this.labelControlTender.Size = new System.Drawing.Size(91, 13);
            this.labelControlTender.TabIndex = 75;
            this.labelControlTender.Text = "Захиалах нярав*:";
            // 
            // gridLookUpEditExpUser
            // 
            this.gridLookUpEditExpUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditExpUser.EditValue = "";
            this.gridLookUpEditExpUser.Location = new System.Drawing.Point(220, 170);
            this.gridLookUpEditExpUser.Name = "gridLookUpEditExpUser";
            this.gridLookUpEditExpUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditExpUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditExpUser.Properties.NullText = "";
            this.gridLookUpEditExpUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditExpUser.Properties.ValueMember = "id";
            this.gridLookUpEditExpUser.Properties.View = this.gridView1;
            this.gridLookUpEditExpUser.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditExpUser.TabIndex = 6;
            this.gridLookUpEditExpUser.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditExpUser_KeyUp);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn28,
            this.gridColumn14,
            this.gridColumn22,
            this.gridColumn24,
            this.gridColumn25});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Дугаар";
            this.gridColumn28.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn28.FieldName = "id";
            this.gridColumn28.MinWidth = 75;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 0;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Овог";
            this.gridColumn14.CustomizationCaption = "Овог";
            this.gridColumn14.FieldName = "lastName";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 2;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Нэр";
            this.gridColumn22.CustomizationCaption = "Нэр";
            this.gridColumn22.FieldName = "firstName";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 1;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Эрх";
            this.gridColumn24.CustomizationCaption = "Эрх";
            this.gridColumn24.FieldName = "roleName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 3;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Тасаг";
            this.gridColumn25.CustomizationCaption = "Тасаг";
            this.gridColumn25.FieldName = "wardName";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 4;
            // 
            // simpleButtonLoad
            // 
            this.simpleButtonLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonLoad.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLoad.Image")));
            this.simpleButtonLoad.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonLoad.Location = new System.Drawing.Point(12, 526);
            this.simpleButtonLoad.Name = "simpleButtonLoad";
            this.simpleButtonLoad.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLoad.TabIndex = 33;
            this.simpleButtonLoad.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonLoad.Click += new System.EventHandler(this.simpleButtonLoad_Click);
            // 
            // treeListLookUpEditWard
            // 
            this.treeListLookUpEditWard.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWard.Location = new System.Drawing.Point(220, 12);
            this.treeListLookUpEditWard.Name = "treeListLookUpEditWard";
            this.treeListLookUpEditWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditWard.Properties.NullText = "";
            this.treeListLookUpEditWard.Properties.TreeList = this.treeList2;
            this.treeListLookUpEditWard.Properties.ValueMember = "id";
            this.treeListLookUpEditWard.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWard.TabIndex = 5;
            this.treeListLookUpEditWard.Visible = false;
            this.treeListLookUpEditWard.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWarehouse_KeyUp);
            // 
            // treeList2
            // 
            this.treeList2.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn5});
            this.treeList2.KeyFieldName = "id";
            this.treeList2.Location = new System.Drawing.Point(92, 264);
            this.treeList2.Name = "treeList2";
            this.treeList2.OptionsBehavior.EnableFiltering = true;
            this.treeList2.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList2.ParentFieldName = "pid";
            this.treeList2.Size = new System.Drawing.Size(400, 200);
            this.treeList2.TabIndex = 0;
            // 
            // treeListColumn5
            // 
            this.treeListColumn5.Caption = "Нэр";
            this.treeListColumn5.FieldName = "name";
            this.treeListColumn5.Name = "treeListColumn5";
            this.treeListColumn5.Visible = true;
            this.treeListColumn5.VisibleIndex = 0;
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl14.Location = new System.Drawing.Point(115, 120);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(99, 13);
            this.labelControl14.TabIndex = 73;
            this.labelControl14.Text = "Агуулахын төрөл*:";
            // 
            // radioGroupType
            // 
            this.radioGroupType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupType.EditValue = 0;
            this.radioGroupType.EnterMoveNextControl = true;
            this.radioGroupType.Location = new System.Drawing.Point(220, 116);
            this.radioGroupType.Name = "radioGroupType";
            this.radioGroupType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Эмийн сан"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Тасаг")});
            this.radioGroupType.Size = new System.Drawing.Size(200, 22);
            this.radioGroupType.TabIndex = 4;
            this.radioGroupType.SelectedIndexChanged += new System.EventHandler(this.radioGroupType_SelectedIndexChanged);
            // 
            // treeListLookUpEditWarehouse
            // 
            this.treeListLookUpEditWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWarehouse.EditValue = "0";
            this.treeListLookUpEditWarehouse.Location = new System.Drawing.Point(220, 144);
            this.treeListLookUpEditWarehouse.Name = "treeListLookUpEditWarehouse";
            this.treeListLookUpEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWarehouse.Properties.DisplayMember = "name";
            this.treeListLookUpEditWarehouse.Properties.NullText = "";
            this.treeListLookUpEditWarehouse.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEditWarehouse.Properties.ValueMember = "id";
            this.treeListLookUpEditWarehouse.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWarehouse.TabIndex = 5;
            this.treeListLookUpEditWarehouse.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWarehouse_KeyUp);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1,
            this.treeListColumn3});
            this.treeListLookUpEdit1TreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListLookUpEdit1TreeList.KeyFieldName = "id";
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(142, 175);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsBehavior.PopulateServiceColumns = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.ParentFieldName = "pid";
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Нэр";
            this.treeListColumn1.CustomizationCaption = "Агуулахын нэр";
            this.treeListColumn1.FieldName = "name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraTreeList.FilterPopupMode.List;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 400;
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "Дугаар";
            this.treeListColumn3.CustomizationCaption = "Агуулахын дугаар";
            this.treeListColumn3.FieldName = "number";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 1;
            this.treeListColumn3.Width = 96;
            // 
            // labelControlWarehouse
            // 
            this.labelControlWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlWarehouse.Location = new System.Drawing.Point(153, 147);
            this.labelControlWarehouse.Name = "labelControlWarehouse";
            this.labelControlWarehouse.Size = new System.Drawing.Size(61, 13);
            this.labelControlWarehouse.TabIndex = 70;
            this.labelControlWarehouse.Text = "Эмийн сан*:";
            // 
            // gridLookUpEditUser
            // 
            this.gridLookUpEditUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditUser.EditValue = "";
            this.gridLookUpEditUser.Location = new System.Drawing.Point(220, 90);
            this.gridLookUpEditUser.Name = "gridLookUpEditUser";
            this.gridLookUpEditUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditUser.Properties.NullText = "";
            this.gridLookUpEditUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditUser.Properties.ValueMember = "id";
            this.gridLookUpEditUser.Properties.View = this.gridView2;
            this.gridLookUpEditUser.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditUser.TabIndex = 3;
            this.gridLookUpEditUser.EditValueChanged += new System.EventHandler(this.gridLookUpEditUser_EditValueChanged);
            this.gridLookUpEditUser.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditUser_KeyUp);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Дугаар";
            this.gridColumn9.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn9.FieldName = "id";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 0;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Овог";
            this.gridColumn10.CustomizationCaption = "Овог";
            this.gridColumn10.FieldName = "lastName";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Нэр";
            this.gridColumn11.CustomizationCaption = "Нэр";
            this.gridColumn11.FieldName = "firstName";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Эрх";
            this.gridColumn12.CustomizationCaption = "Эрх";
            this.gridColumn12.FieldName = "roleName";
            this.gridColumn12.MinWidth = 75;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 3;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Тасаг";
            this.gridColumn13.CustomizationCaption = "Тасаг";
            this.gridColumn13.FieldName = "wardName";
            this.gridColumn13.MinWidth = 75;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 4;
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(497, 526);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 32;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(416, 526);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 31;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl13.Location = new System.Drawing.Point(166, 93);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(48, 13);
            this.labelControl13.TabIndex = 65;
            this.labelControl13.Text = "Эм зүйч*:";
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(165, 198);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(49, 13);
            this.labelControl8.TabIndex = 64;
            this.labelControl8.Text = "Тайлбар :";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.memoEditDescription.Location = new System.Drawing.Point(220, 196);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(352, 41);
            this.memoEditDescription.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl7.Location = new System.Drawing.Point(115, 41);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(99, 13);
            this.labelControl7.TabIndex = 63;
            this.labelControl7.Text = "Баримтын дугаар*:";
            // 
            // textEditID
            // 
            this.textEditID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditID.Location = new System.Drawing.Point(220, 38);
            this.textEditID.Name = "textEditID";
            this.textEditID.Properties.ReadOnly = true;
            this.textEditID.Size = new System.Drawing.Size(200, 20);
            this.textEditID.TabIndex = 1;
            // 
            // dateEditOrderDate
            // 
            this.dateEditOrderDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditOrderDate.EditValue = null;
            this.dateEditOrderDate.Location = new System.Drawing.Point(220, 64);
            this.dateEditOrderDate.Name = "dateEditOrderDate";
            this.dateEditOrderDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOrderDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOrderDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditOrderDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditOrderDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditOrderDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditOrderDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditOrderDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditOrderDate.TabIndex = 2;
            this.dateEditOrderDate.EditValueChanged += new System.EventHandler(this.dateEditOrderDate_EditValueChanged);
            this.dateEditOrderDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditOrderDate_KeyUp);
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(173, 67);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(41, 13);
            this.labelControl3.TabIndex = 59;
            this.labelControl3.Text = "Огноо*:";
            // 
            // gridControlDrug
            // 
            this.gridControlDrug.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlDrug.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlDrug.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlDrug.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlDrug.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlDrug.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlDrug.Location = new System.Drawing.Point(12, 271);
            this.gridControlDrug.MainView = this.gridViewDrug;
            this.gridControlDrug.Name = "gridControlDrug";
            this.gridControlDrug.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemGridLookUpEditMed});
            this.gridControlDrug.Size = new System.Drawing.Size(560, 249);
            this.gridControlDrug.TabIndex = 20;
            this.gridControlDrug.UseEmbeddedNavigator = true;
            this.gridControlDrug.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDrug});
            // 
            // gridViewDrug
            // 
            this.gridViewDrug.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2});
            this.gridViewDrug.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumnMedName,
            this.bandedGridColumn6,
            this.bandedGridColumnWard,
            this.bandedGridColumn16,
            this.bandedGridColumn13,
            this.bandedGridColumn7,
            this.bandedGridColumn17,
            this.bandedGridColumnCount,
            this.bandedGridColumnMedicineName,
            this.bandedGridColumnPackage});
            this.gridViewDrug.GridControl = this.gridControlDrug;
            this.gridViewDrug.Name = "gridViewDrug";
            this.gridViewDrug.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewDrug.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDrug.OptionsView.ShowFooter = true;
            this.gridViewDrug.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Захиалсан эм";
            this.gridBand2.Columns.Add(this.bandedGridColumnWard);
            this.gridBand2.Columns.Add(this.bandedGridColumnMedName);
            this.gridBand2.Columns.Add(this.bandedGridColumn6);
            this.gridBand2.Columns.Add(this.bandedGridColumnPackage);
            this.gridBand2.Columns.Add(this.bandedGridColumnMedicineName);
            this.gridBand2.Columns.Add(this.bandedGridColumn16);
            this.gridBand2.Columns.Add(this.bandedGridColumn13);
            this.gridBand2.Columns.Add(this.bandedGridColumn7);
            this.gridBand2.Columns.Add(this.bandedGridColumn17);
            this.gridBand2.Columns.Add(this.bandedGridColumnCount);
            this.gridBand2.CustomizationCaption = "Захиалсан эм";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 625;
            // 
            // bandedGridColumnWard
            // 
            this.bandedGridColumnWard.Caption = "Тасаг";
            this.bandedGridColumnWard.FieldName = "wardName";
            this.bandedGridColumnWard.MinWidth = 100;
            this.bandedGridColumnWard.Name = "bandedGridColumnWard";
            this.bandedGridColumnWard.Visible = true;
            this.bandedGridColumnWard.Width = 100;
            // 
            // bandedGridColumnMedName
            // 
            this.bandedGridColumnMedName.Caption = "Нэр";
            this.bandedGridColumnMedName.CustomizationCaption = "Нэр";
            this.bandedGridColumnMedName.FieldName = "medicineName";
            this.bandedGridColumnMedName.MinWidth = 75;
            this.bandedGridColumnMedName.Name = "bandedGridColumnMedName";
            this.bandedGridColumnMedName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumnMedName.OptionsColumn.ReadOnly = true;
            this.bandedGridColumnMedName.Visible = true;
            // 
            // bandedGridColumn6
            // 
            this.bandedGridColumn6.Caption = "Латин нэр";
            this.bandedGridColumn6.CustomizationCaption = "Латин нэр";
            this.bandedGridColumn6.FieldName = "latinName";
            this.bandedGridColumn6.MinWidth = 75;
            this.bandedGridColumn6.Name = "bandedGridColumn6";
            this.bandedGridColumn6.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // bandedGridColumnPackage
            // 
            this.bandedGridColumnPackage.Caption = "Багц";
            this.bandedGridColumnPackage.FieldName = "packageName";
            this.bandedGridColumnPackage.MinWidth = 75;
            this.bandedGridColumnPackage.Name = "bandedGridColumnPackage";
            this.bandedGridColumnPackage.Visible = true;
            // 
            // bandedGridColumnMedicineName
            // 
            this.bandedGridColumnMedicineName.Caption = "Нэр";
            this.bandedGridColumnMedicineName.FieldName = "name";
            this.bandedGridColumnMedicineName.MinWidth = 75;
            this.bandedGridColumnMedicineName.Name = "bandedGridColumnMedicineName";
            this.bandedGridColumnMedicineName.Visible = true;
            // 
            // bandedGridColumn16
            // 
            this.bandedGridColumn16.Caption = "Ангилал";
            this.bandedGridColumn16.CustomizationCaption = "Ангилал";
            this.bandedGridColumn16.FieldName = "medicineTypeName";
            this.bandedGridColumn16.MinWidth = 75;
            this.bandedGridColumn16.Name = "bandedGridColumn16";
            this.bandedGridColumn16.OptionsColumn.ReadOnly = true;
            this.bandedGridColumn16.Visible = true;
            // 
            // bandedGridColumn13
            // 
            this.bandedGridColumn13.Caption = "Хэлбэр";
            this.bandedGridColumn13.CustomizationCaption = "Хэлбэр";
            this.bandedGridColumn13.FieldName = "shape";
            this.bandedGridColumn13.MinWidth = 75;
            this.bandedGridColumn13.Name = "bandedGridColumn13";
            this.bandedGridColumn13.OptionsColumn.ReadOnly = true;
            // 
            // bandedGridColumn7
            // 
            this.bandedGridColumn7.Caption = "Тун хэмжээ";
            this.bandedGridColumn7.CustomizationCaption = "Тун хэмжээ";
            this.bandedGridColumn7.FieldName = "unitType";
            this.bandedGridColumn7.MinWidth = 75;
            this.bandedGridColumn7.Name = "bandedGridColumn7";
            this.bandedGridColumn7.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumn7.OptionsColumn.ReadOnly = true;
            this.bandedGridColumn7.Visible = true;
            // 
            // bandedGridColumn17
            // 
            this.bandedGridColumn17.Caption = "Хэмжих нэгж";
            this.bandedGridColumn17.CustomizationCaption = "Хэмжих нэгж";
            this.bandedGridColumn17.FieldName = "quantity";
            this.bandedGridColumn17.MinWidth = 75;
            this.bandedGridColumn17.Name = "bandedGridColumn17";
            this.bandedGridColumn17.OptionsColumn.ReadOnly = true;
            this.bandedGridColumn17.Visible = true;
            // 
            // bandedGridColumnCount
            // 
            this.bandedGridColumnCount.Caption = "Тоо хэмжээ";
            this.bandedGridColumnCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.bandedGridColumnCount.CustomizationCaption = "Тоо хэмжээ";
            this.bandedGridColumnCount.FieldName = "count";
            this.bandedGridColumnCount.MinWidth = 75;
            this.bandedGridColumnCount.Name = "bandedGridColumnCount";
            this.bandedGridColumnCount.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.bandedGridColumnCount.OptionsColumn.ReadOnly = true;
            this.bandedGridColumnCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.bandedGridColumnCount.Visible = true;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemGridLookUpEditMed
            // 
            this.repositoryItemGridLookUpEditMed.AutoHeight = false;
            this.repositoryItemGridLookUpEditMed.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditMed.DisplayMember = "medicineName";
            this.repositoryItemGridLookUpEditMed.Name = "repositoryItemGridLookUpEditMed";
            this.repositoryItemGridLookUpEditMed.NullText = "";
            this.repositoryItemGridLookUpEditMed.ValueMember = "medicinePrice";
            this.repositoryItemGridLookUpEditMed.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Нэр";
            this.gridColumn1.CustomizationCaption = "Нэр";
            this.gridColumn1.FieldName = "name";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Ангилал";
            this.gridColumn2.CustomizationCaption = "Ангилал";
            this.gridColumn2.FieldName = "medicineTypeName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ОУ нэр";
            this.gridColumn3.CustomizationCaption = "ОУ нэр";
            this.gridColumn3.FieldName = "latinName";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Хэлбэр";
            this.gridColumn4.CustomizationCaption = "Хэлбэр";
            this.gridColumn4.FieldName = "shape";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Тун хэмжээ";
            this.gridColumn5.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn5.FieldName = "unitType";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Хэмжих нэгж";
            this.gridColumn6.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn6.FieldName = "quantity";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Үнэ";
            this.gridColumn7.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumn7.FieldName = "price";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Орлогын төрөл";
            this.gridColumn8.CustomizationCaption = "Орлогын төрөл";
            this.gridColumn8.FieldName = "incTypeName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(426, 240);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 25);
            this.simpleButtonReload.TabIndex = 10;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Үлдэгдэл шүүх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(220, 243);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(95, 20);
            this.dateEditStart.TabIndex = 8;
            this.dateEditStart.ToolTip = "Эхлэх огноо";
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(325, 243);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(95, 20);
            this.dateEditEnd.TabIndex = 9;
            this.dateEditEnd.ToolTip = "Дуусах огноо";
            // 
            // MedicineDrugDownForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.panelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MedicineDrugDownForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Эмийн түүвэр татах";
            this.Shown += new System.EventHandler(this.MedicineDrugDownForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditExpUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDrug)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDrug)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditMed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraGrid.GridControl gridControlDrug;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridViewDrug;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnMedName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnCount;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn16;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn17;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditMed;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEditID;
        private DevExpress.XtraEditors.DateEdit dateEditOrderDate;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.RadioGroup radioGroupType;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWarehouse;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private DevExpress.XtraEditors.LabelControl labelControlWarehouse;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWard;
        private DevExpress.XtraTreeList.TreeList treeList2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLoad;
        private DevExpress.XtraEditors.LabelControl labelControlTender;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditExpUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnWard;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditTender;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnMedicineName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnPackage;
    }
}