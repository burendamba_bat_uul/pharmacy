﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;

namespace Pharmacy2016.gui.core.journal.order
{
    /*
     * Эмийн үлдэгдлийг харан захиалганд нэмэх цонх.
     * 
     */

    public partial class MedicineBalDownForm : DevExpress.XtraEditors.XtraForm
    {
       
        #region Форм анх ачааллах функц

            private static MedicineBalDownForm INSTANCE = new MedicineBalDownForm();

            public static MedicineBalDownForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private string warehouseID;

            private MedicineBalDownForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                initError();

                gridControlBal.DataSource = JournalDataSet.OrderBalBindingSource;
            }

            private void initError()
            {
                dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditEnd.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion
        
        #region Форм нээх, хаах функц

            public void showForm(XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    warehouseID = MedicineOrderInUpForm.Instance.getWarehouseID();
                    dateEditStart.DateTime = MedicineOrderForm.Instance.getStartDate();
                    dateEditEnd.DateTime = MedicineOrderForm.Instance.getEndDate();

                    checkEditExp.Checked = false;
                    checkEditGive.Checked = false;

                    reload();
                    ShowDialog(form);
                }
                catch(Exception ex)
                {
                    XtraMessageBox.Show("Цонх ачааллахад алдаа гарлаа "+ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void MedicineBalForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                clearData();
            }

            private void clearData()
            {   
                JournalDataSet.Instance.OrderBal.Clear();
            }

        #endregion

        #region Формын event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void gridViewBal_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
            {
                if (e.Column == bandedGridColumn1 || e.Column == bandedGridColumnGive || e.Column == bandedGridColumnExp || e.Column == bandedGridColumn12)
                {
                    if (Convert.ToDouble(e.CellValue) < 0)
                    {
                        e.Appearance.BackColor = Color.Red;
                    }
                }
            }

            private void checkEditExp_CheckedChanged(object sender, EventArgs e)
            {
                if (checkEditExp.Checked)
                    setValue(1);
            }

            private void checkEditGive_CheckedChanged(object sender, EventArgs e)
            {
                if (checkEditGive.Checked)
                    setValue(2);
            }

            private void simpleButtonClear_Click(object sender, EventArgs e)
            {
                setValue(3);
            }

            private void simpleButtonDownload_Click(object sender, EventArgs e)
            {
                download();
            }

        #endregion

        #region Формын функц

            private void setValue(int type)
            {
                if (type == 1)
                {
                    double count, exp;
                    for (int i = 0; i < gridViewBal.RowCount; i++)
                    {
                        count = Convert.ToDouble(gridViewBal.GetRowCellValue(i, bandedGridColumnCount));
                        exp = Convert.ToDouble(gridViewBal.GetRowCellValue(i, bandedGridColumnExp));
                        if (count == 0 && exp > 0)
                            gridViewBal.SetRowCellValue(i, bandedGridColumnCount, exp);
                    }
                }
                else if (type == 2)
                {
                    double count, give;
                    for (int i = 0; i < gridViewBal.RowCount; i++)
                    {
                        count = Convert.ToDouble(gridViewBal.GetRowCellValue(i, bandedGridColumnCount));
                        give = Convert.ToDouble(gridViewBal.GetRowCellValue(i, bandedGridColumnGive));
                        if (count == 0 && give > 0)
                            gridViewBal.SetRowCellValue(i, bandedGridColumnCount, give);
                    }
                }
                else if (type == 3)
                {
                    checkEditExp.Checked = false;
                    checkEditGive.Checked = false;
                    for (int i = 0; i < gridViewBal.RowCount; i++)
                    {   
                        gridViewBal.SetRowCellValue(i, bandedGridColumnCount, 0);
                    }
                }
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void reload()
            {
                if (check())
                {
                    //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    JournalDataSet.OrderBalTableAdapter.Fill(JournalDataSet.Instance.OrderBal, HospitalUtil.getHospitalId(), warehouseID, dateEditStart.Text, dateEditEnd.Text);

                    checkEditExp.Checked = false;
                    checkEditGive.Checked = false;
                    //ProgramUtil.closeWaitDialog();
                }
            }

            private void download()
            {
                JournalDataSet.OrderBalBindingSource.EndEdit();
                MedicineOrderInUpForm.Instance.download(JournalDataSet.Instance.OrderBal.Select("count > 0"));
                Close();
            }

        #endregion

    }
}