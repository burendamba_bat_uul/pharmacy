﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.report.journal.order;
using DevExpress.XtraGrid.Views.Grid;
using Pharmacy2016.report.journal.order;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.report.ds;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid;

namespace Pharmacy2016.gui.core.journal.order
{
    /*
     * Эмийн захиалга харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class MedicineOrderForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static MedicineOrderForm INSTANCE = new MedicineOrderForm();

        public static MedicineOrderForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private readonly int FORM_ID = 2007;

        private bool isInit = false;

        public DataRowView currView;

        private BaseEdit inplaceEditor;

        private System.IO.Stream layout_order = null;
        private System.IO.Stream layout_detail = null;

        private MedicineOrderForm()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            this.MdiParent = MainForm.Instance;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            initLayout();
            RoleUtil.addForm(FORM_ID, this);

            gridControlOrder.DataSource = JournalDataSet.OrderGoodsBindingSource;

            dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
            dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

            DateTime date = DateTime.Now;
            dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1);
            dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }

        private void initLayout()
        {
            layout_order = new System.IO.MemoryStream();
            gridViewOrder.SaveLayoutToStream(layout_order);
            layout_order.Seek(0, System.IO.SeekOrigin.Begin);

            layout_detail = new System.IO.MemoryStream();
            gridViewDetail.SaveLayoutToStream(layout_detail);
            layout_detail.Seek(0, System.IO.SeekOrigin.Begin);
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm()
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }

                if (Visible)
                {
                    Select();
                }
                else
                {
                    checkRole();
                    radioGroupJournal.SelectedIndex = 0;

                    JournalDataSet.Instance.OrderGoods.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    JournalDataSet.Instance.OrderGoods.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                    JournalDataSet.Instance.OrderGoodsDetail.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    JournalDataSet.Instance.OrderGoodsDetail.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                    reload();
                    gridColumn3.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                    Show();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                clearData();
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void checkRole()
        {
            bool[] roles = UserUtil.getWindowRole(FORM_ID);

            gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
            gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
            gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
            dropDownButtonPrint.Visible = roles[4];
            simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;

            if (UserUtil.getUserRoleId() == RoleUtil.TREATMENT_DIRECTOR_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID)
            {
                //gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = true;
                simpleButtonConfirm.Visible = true;
                // 2016-10-17 захайлгын хуудасны засах эрхийг шинэчлэв.
                //gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
            }
            else if (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                simpleButtonConfirm.Visible = !SystemUtil.SELECTED_DB_READONLY;
            //gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = true;
            else
                simpleButtonConfirm.Visible = false;
            //gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = false;

            if ((UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID || UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID) && RoleUtil.getIsUseStoreDrug())
            {
                dropDownButton1.Visible = !SystemUtil.SELECTED_DB_READONLY;
                simpleButtonConfirm.Location = new Point(376, 11);
            }
            else
            {
                simpleButtonConfirm.Location = new Point(293, 11);
                dropDownButton1.Visible = false;
            }


            checkLayout();
        }

        private void checkLayout()
        {
            string layout = UserUtil.getLayout("orderGoods");
            if (layout != null)
            {
                gridViewOrder.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewOrder.RestoreLayoutFromStream(layout_order);
                layout_order.Seek(0, System.IO.SeekOrigin.Begin);
            }

            layout = UserUtil.getLayout("orderGoodsDetail");
            if (layout != null)
            {
                gridViewDetail.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
            }
            else
            {
                gridViewDetail.RestoreLayoutFromStream(layout_detail);
                layout_detail.Seek(0, System.IO.SeekOrigin.Begin);
            }
        }

        private void MedicineOrderForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            clearData();
            Hide();
        }

        private void clearData()
        {
            JournalDataSet.Instance.OrderGoodsDetail.Clear();
            JournalDataSet.Instance.OrderGoods.Clear();
        }

        #endregion

        #region Формын event

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            try
            {
                reload();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        private void simpleButtonConfirm_Click(object sender, EventArgs e)
        {
            if (gridViewOrder.GetFocusedDataRow() != null)
                confirm();
        }

        private void simpleButtonLayout_Click(object sender, EventArgs e)
        {
            saveLayout();
        }

        private void gridControlOrder_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    add();
                }
                else if (String.Equals(e.Button.Tag, "delete") && gridViewOrder.GetFocusedDataRow() != null)
                {
                    try
                    {
                        delete();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
                else if (String.Equals(e.Button.Tag, "update") && gridViewOrder.GetFocusedDataRow() != null)
                {
                    update();
                }
                //else if (String.Equals(e.Button.Tag, "confirm") && gridViewOrder.GetFocusedDataRow() != null)
                //{
                //    confirm();
                //}
                //else if (String.Equals(e.Button.Tag, "download"))
                //{
                //    download();
                //}
            }
        }

        private void gridViewOrder_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "expUserID")
            {
                int orderType = Convert.ToInt32(gridViewOrder.GetRowCellValue(e.ListSourceRowIndex, "orderType"));
                if ((orderType == 0 || orderType == 3) && gridViewOrder.GetRowCellValue(e.ListSourceRowIndex, "expUserID") != DBNull.Value && gridViewOrder.GetRowCellValue(e.ListSourceRowIndex, "expUserID") != null)
                {
                    e.DisplayText = gridViewOrder.GetRowCellValue(e.ListSourceRowIndex, "expUserID").ToString();
                }
                else if (orderType == 1 && gridViewOrder.GetRowCellValue(e.ListSourceRowIndex, "tenderNumber") != DBNull.Value && gridViewOrder.GetRowCellValue(e.ListSourceRowIndex, "tenderNumber") != null)
                {
                    e.DisplayText = gridViewOrder.GetRowCellValue(e.ListSourceRowIndex, "tenderNumber").ToString();
                }
            }
        }

        private void gridViewOrderDetail_RowClick(object sender, RowClickEventArgs e)
        {
            GridView detailView = sender as GridView;
            gridViewOrder.FocusedRowHandle = detailView.SourceRowHandle;
        }

        private void radioGroupJournal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioGroupJournal.SelectedIndex == 0)
            {
                JournalDataSet.OrderGoodsDetailBindingSource.DataSource = JournalDataSet.OrderGoodsBindingSource;
                JournalDataSet.OrderGoodsDetailBindingSource.DataMember = "OrderGoods_OrderGoodsDetail";

                gridControlOrder.DataSource = JournalDataSet.OrderGoodsBindingSource;
                gridControlOrder.MainView = gridViewOrder;

                gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;
                checkRole();
                barButtonItemPrint.Enabled = true;
            }
            else if (radioGroupJournal.SelectedIndex == 1)
            {
                JournalDataSet.OrderGoodsDetailBindingSource.DataSource = JournalDataSet.Instance;
                JournalDataSet.OrderGoodsDetailBindingSource.DataMember = "OrderGoodsDetail";

                gridControlOrder.DataSource = JournalDataSet.OrderGoodsDetailBindingSource;
                gridControlOrder.MainView = gridViewDetail;

                gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
                barButtonItemPrint.Enabled = false;
            }
        }

        private void gridViewDetail_DoubleClick(object sender, EventArgs e)
        {
            DataRowView view = (DataRowView)gridViewDetail.GetFocusedRow();
            if (view == null)
                return;


            radioGroupJournal.SelectedIndex = 0;
            for (int i = 0; i < gridViewOrder.RowCount; i++)
            {
                DataRow row = gridViewOrder.GetDataRow(i);
                if (view["orderGoodsID"].ToString().Equals(row["id"].ToString()))
                {
                    gridViewOrder.SelectRow(i);
                    if (!gridViewOrder.GetMasterRowExpanded(i))
                        gridViewOrder.SetMasterRowExpanded(i, true);
                }
            }
        }

        private void barButtonItemDrug_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MedicineDrugDownForm.Instance.showForm(1, Instance);
        }

        private void barButtonItemTender_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MedicineDrugDownForm.Instance.showForm(2, Instance);
        }

        private void gridViewOrder_ShownEditor(object sender, EventArgs e)
        {
            inplaceEditor = ((GridView)sender).ActiveEditor;
            inplaceEditor.DoubleClick += new EventHandler(ActiveEditor_DoubleClick);
        }

        private void gridViewOrderDetail_ShownEditor(object sender, EventArgs e)
        {
            inplaceEditor = ((GridView)sender).ActiveEditor;
            inplaceEditor.DoubleClick += new EventHandler(ActiveEditor_DoubleClick);
        }

        private void ActiveEditor_DoubleClick(object sender, System.EventArgs e)
        {
            BaseEdit editor = (BaseEdit)sender;
            GridControl grid = (GridControl)editor.Parent;
            Point pt = grid.PointToClient(Control.MousePosition);
            GridView view = (GridView)grid.FocusedView;
            DoRowDoubleClick(view, pt);
        }

        #endregion

        #region Формын функц

        private bool check()
        {
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();

            if (dateEditStart.Text == "")
            {
                errorText = "Эхлэх огноог оруулна уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }
            if (dateEditEnd.Text == "")
            {
                errorText = "Дуусах огноог оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditEnd.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    dateEditEnd.Focus();
                }
            }
            if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
            {
                errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditStart.ErrorText = errorText;
                isRight = false;
                dateEditStart.Focus();
            }
            if (!isRight)
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        private void reload()
        {
            if (check())
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                {

                    JournalDataSet.OrderGoodsTableAdapter.Fill(JournalDataSet.Instance.OrderGoods, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                    JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                }
                else
                {
                    JournalDataSet.OrderGoodsTableAdapter.Fill(JournalDataSet.Instance.OrderGoods, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                    JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);

                }
                ProgramUtil.closeWaitDialog();
                JournalDataSet.OrderStageTableAdapter.Fill(JournalDataSet.Instance.OrderStage);
            }
        }

        private void add()
        {
            MedicineOrderUpForm.Instance.showForm(1, this);
        }

        private void update()
        {
            if (Convert.ToBoolean(gridViewOrder.GetFocusedRowCellValue("isConfirm")))
            {
                ProgramUtil.showAlertDialog(Instance, Instance.Text, "Захиалгын хуудсыг баталсан тул өөрчилж болохгүй");
                return;
            }
            if (Convert.ToBoolean(gridViewOrder.GetFocusedRowCellValue("isGive")))
            {
                ProgramUtil.showAlertDialog(Instance, Instance.Text, "Захиалгын хуудсыг олгосон тул өөрчилж болохгүй");
                return;
            }
            if (Convert.ToInt32(gridViewOrder.GetFocusedRowCellValue("orderType")) == 3)
            {
                ProgramUtil.showAlertDialog(Instance, Instance.Text, "Эмийн түүврээс ирсэн хуудсыг өөрчилж болохгүй");
                return;
            }

            if (!gridViewOrder.GetMasterRowExpanded(gridViewOrder.FocusedRowHandle))
                gridViewOrder.SetMasterRowExpanded(gridViewOrder.FocusedRowHandle, true);

            MedicineOrderUpForm.Instance.showForm(2, this);
        }

        private void delete()
        {
            if (Convert.ToBoolean(gridViewOrder.GetFocusedRowCellValue("isConfirm")))
            {
                ProgramUtil.showAlertDialog(Instance, Instance.Text, "Захиалгын хуудсыг баталсан тул устгаж болохгүй");
                return;
            }
            if (Convert.ToBoolean(gridViewOrder.GetFocusedRowCellValue("isGive")))
            {
                ProgramUtil.showAlertDialog(Instance, Instance.Text, "Захиалгын хуудсыг олгосон тул устгаж болохгүй");
                return;
            }
            string date = gridViewOrder.GetRowCellValue(gridViewOrder.FocusedRowHandle, gridViewOrder.Columns["date"]).ToString();
            if (ProgramUtil.checkLockMedicine(date)) {
                bool isDeleteDoc = false;
                GridView detail = null;

                if (gridViewOrder.IsFocusedView)
                {
                    isDeleteDoc = true;
                }
                else
                {
                    detail = gridViewOrder.GetDetailView(gridViewOrder.FocusedRowHandle, 0) as GridView;
                    if (detail.IsFocusedView)
                    {
                        isDeleteDoc = detail.RowCount == 1;
                    }
                }
                if (isDeleteDoc)
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Захиалгын хуудас устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        JournalDataSet.QueriesTableAdapter.delete_drug_journal_order(HospitalUtil.getHospitalId(), gridViewOrder.GetFocusedDataRow()["id"].ToString(), false);
                        DataRow[] detialView = JournalDataSet.Instance.OrderGoodsDetail.Select("orderGoodsId = '" + gridViewOrder.GetFocusedDataRow()["id"] + "'");
                        for (int i = 0; i < detialView.Length; i++)
                        {
                            DataRow[] rows = JournalDataSet.Instance.OrderStage.Select("orderGoodsDetialID = '" + detialView[i]["id"] + "'");
                            for (int j = 0; j < rows.Count(); j++)
                                rows[j].Delete();
                        }
                        gridViewOrder.DeleteSelectedRows();
                        JournalDataSet.OrderStageTableAdapter.Update(JournalDataSet.Instance.OrderStage);
                        JournalDataSet.OrderGoodsBindingSource.EndEdit();
                        JournalDataSet.OrderGoodsTableAdapter.Update(JournalDataSet.Instance.OrderGoods);
                        ProgramUtil.closeWaitDialog();
                    }
                }
                else
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Захиалгын гүйлгээ устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        DataRow[] rows = JournalDataSet.Instance.OrderStage.Select("orderGoodsDetialID = '" + detail.GetFocusedDataRow()["id"] + "'");
                        for (int j = 0; j < rows.Count(); j++)
                            rows[j].Delete();
                        detail.DeleteSelectedRows();
                        JournalDataSet.OrderStageTableAdapter.Update(JournalDataSet.Instance.OrderStage);
                        JournalDataSet.OrderGoodsDetailBindingSource.EndEdit();
                        JournalDataSet.OrderGoodsDetailTableAdapter.Update(JournalDataSet.Instance.OrderGoodsDetail);
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }
            else
            {
                XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
            }           
        }

        private void confirm()
        {
            if (Convert.ToBoolean(gridViewOrder.GetFocusedRowCellValue("isGive")))
            {
                ProgramUtil.showAlertDialog(Instance, Instance.Text, "Захиалгын хуудсыг олгосон тул өөрчилж болохгүй");
                return;
            }
            if (!RoleUtil.getAccessList(UserUtil.getUserRoleId().ToString()))
            {
                //    if ((Convert.ToInt32(gridViewOrder.GetFocusedRowCellValue("orderType")) == 0 || Convert.ToInt32(gridViewOrder.GetFocusedRowCellValue("orderType")) == 3) && UserUtil.getUserId() != gridViewOrder.GetFocusedRowCellValue("expUserID").ToString())
                //    {
                //        ProgramUtil.showAlertDialog(Instance, Instance.Text, "Захиалсан хэрэглэгч биш тул баталж болохгүй");
                //        return;
                //    }
                //    else if ((Convert.ToInt32(gridViewOrder.GetFocusedRowCellValue("orderType")) == 1 || Convert.ToInt32(gridViewOrder.GetFocusedRowCellValue("orderType")) == 2) && UserUtil.getUserId() != gridViewOrder.GetFocusedRowCellValue("userID").ToString())
                //    {
                ProgramUtil.showAlertDialog(Instance, Instance.Text, "Захиалсан хэрэглэгч биш тул баталж болохгүй");
                return;
                // }
            }
            MedicineOrderConfirmForm.Instance.showForm(Instance);
        }

        private void saveLayout()
        {
            try
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                System.IO.Stream str = new System.IO.MemoryStream();
                System.IO.StreamReader reader = null;
                if (radioGroupJournal.SelectedIndex == 0)
                {
                    gridViewOrder.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("orderGoods", reader.ReadToEnd());
                }
                else
                {
                    gridViewDetail.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("orderGoodsDetail", reader.ReadToEnd());
                }
                reader.Close();
                str.Close();

                ProgramUtil.closeWaitDialog();
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
            }
            finally
            {
                ProgramUtil.closeWaitDialog();
            }
        }

        public DataRowView getSelectedRow()
        {
            GridView detail = gridViewOrder.GetDetailView(gridViewOrder.FocusedRowHandle, 0) as GridView;
            string id = detail.GetFocusedDataRow()["id"].ToString();
            for (int i = 0; i < JournalDataSet.OrderGoodsDetailBindingSource.Count; i++)
            {
                DataRowView view = (DataRowView)JournalDataSet.OrderGoodsDetailBindingSource[i];
                if (view["id"].ToString().Equals(id))
                    return view;
            }
            return null;
        }

        public DateTime getStartDate()
        {
            return dateEditStart.DateTime;
        }

        public DateTime getEndDate()
        {
            return dateEditEnd.DateTime;
        }

        public void reloadOuter(string start, string end)
        {
            if (UserUtil.getUserRoleId() == RoleUtil.STOREMAN_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                JournalDataSet.OrderGoodsTableAdapter.Fill(JournalDataSet.Instance.OrderGoods, HospitalUtil.getHospitalId(), UserUtil.getUserId(), start, end);
                JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), start, end);
                //ProgramUtil.closeWaitDialog();
            }
            else
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                JournalDataSet.OrderGoodsTableAdapter.Fill(JournalDataSet.Instance.OrderGoods, HospitalUtil.getHospitalId(), "", start, end);
                JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(), "", start, end);
                //ProgramUtil.closeWaitDialog();
            }
        }

        private void DoRowDoubleClick(GridView view, Point pt)
        {
            GridHitInfo info = view.CalcHitInfo(pt);
            if (info.InRow || info.InRowCell)
            {
                if (view.GetFocusedDataRow() != null)
                    update();
            }
        }

        #endregion

        #region Хэвлэх, хөрвүүлэх функц

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItemPrint.Caption;
            dropDownButtonPrint.Image = barButtonItemPrint.Glyph;
            ProgramUtil.closeAlertDialog();
            if (gridViewOrder.GetFocusedRow() != null)
            {
                DataRow row = gridViewOrder.GetFocusedDataRow();
                OrderDocReport.Instance.initAndShow(row["id"].ToString(), row["userName"].ToString(), row["roleName"].ToString(), Convert.ToBoolean(row["isConfirm"]));
                //double count = 0;
                //DataRow[] rows = JournalDataSet.Instance.OrderGoodsDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND orderGoodsID = '" + gridViewOrder.GetFocusedDataRow()["id"] + "'");
                //foreach (DataRow row in rows)
                //    count += Convert.ToDouble(row["count"]);

                //OrderReport.Instance.initAndShow(gridViewOrder.GetFocusedDataRow()["id"].ToString(), gridViewOrder.GetFocusedDataRow()["motJournalid"].ToString(), String.Format("{0:n}", count));
            }
            else
            {
                //XtraMessageBox.Show("Хэвлэх мөр алга байна");
                string errorText = "Хэвлэх мөр алга байна";
                string text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
            }
        }

        private void barButtonItemCustomize_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItemCustomize.Caption;
            dropDownButtonPrint.Image = barButtonItemCustomize.Glyph;
            ProgramUtil.closeAlertDialog();
            if (gridViewOrder.GetFocusedRow() != null)
            {
                int maxStage = 6;
                DataRow row = gridViewOrder.GetFocusedDataRow();
                DataRow[] detial = JournalDataSet.Instance.OrderGoodsDetail.Select("orderGoodsID = '" + row["id"] + "' AND hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                if (detial.Count() > 0)
                {
                    for (int i = 5; i > 0; i--)
                    {
                        maxStage = maxStage - 1;
                        if (detial[0]["" + i + ""] != DBNull.Value)
                        {
                            break;
                        }
                    }
                }
                string confUserID = "";
                DataRow[] confUser = JournalDataSet.Instance.OrderStage.Select("orderGoodsDetialID = '" + detial[0]["id"] + "'AND confStage = '" + maxStage + "'");
                if (confUser.Count() > 0)
                    confUserID = confUser[0]["userID"].ToString();

                List<string[]> colList = new List<string[]>();
                foreach (DevExpress.XtraGrid.Columns.GridColumn col in gridViewOrder.Columns.Where(x => (x.FieldName != "userID" && x.FieldName != "warehouseDesc" && x.FieldName != "isGiveText" && x.FieldName != "isConfirmText" && x.FieldName != "orderTypeText" && x.FieldName != "id" && x.FieldName != "roleName" && x.FieldName != "date" && x.FieldName != "warehouseName" && x.FieldName != "expUserID")))
                {
                    colList.Add(new string[] { col.FieldName, col.CustomizationCaption });
                }
                foreach (DevExpress.XtraGrid.Columns.GridColumn cols in gridViewOrderDetail.Columns.Where(x => (x.FieldName != "count" && x.FieldName != "confirmCount" && x.FieldName != "giveCount")))
                {
                    colList.Add(new string[] { cols.FieldName, cols.Caption });
                }

                ReportUpForm.Instance.showForm(colList, MedicineOrderForm.Instance, row, dateEditStart.DateTime, dateEditEnd.DateTime, confUserID);
            }
            else
            {
                string errorText = "Хэвлэх мөр алга байна";
                string text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
            }
        }

        private void barButtonItemEndBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItemEndBal.Caption;
            dropDownButtonPrint.Image = barButtonItemEndBal.Glyph;
        }

        private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            dropDownButtonPrint.Text = barButtonItemConvert.Caption;
            dropDownButtonPrint.Image = barButtonItemConvert.Glyph;
            if (radioGroupJournal.SelectedIndex == 0)
                ProgramUtil.convertGrid(gridViewOrder, "Захиалгын хуудас");
            else
                ProgramUtil.convertGrid(gridViewDetail, "Захиалгын гүйлгээ");
        }

        #endregion

        private void dropDownButton1_Click(object sender, EventArgs e)
        {

        }

    }
}