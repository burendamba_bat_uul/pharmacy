﻿namespace Pharmacy2016.gui.core.journal.order
{
    partial class MedicineOrderConfirmForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControlWarehouse = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditOrderDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlExpUser = new DevExpress.XtraEditors.LabelControl();
            this.textEditUserName = new DevExpress.XtraEditors.TextEdit();
            this.textEditWarehouseName = new DevExpress.XtraEditors.TextEdit();
            this.textEditExpUserName = new DevExpress.XtraEditors.TextEdit();
            this.checkEditIsConfirm = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlDetail = new DevExpress.XtraGrid.GridControl();
            this.gridViewDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnConf1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnConf2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnConf3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnConf4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnConf5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnConfirmCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditChooseAll = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWarehouseName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditExpUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsConfirm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChooseAll.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControlWarehouse
            // 
            this.labelControlWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlWarehouse.Location = new System.Drawing.Point(178, 104);
            this.labelControlWarehouse.Name = "labelControlWarehouse";
            this.labelControlWarehouse.Size = new System.Drawing.Size(38, 13);
            this.labelControlWarehouse.TabIndex = 2;
            this.labelControlWarehouse.Text = "Тасаг*:";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(175, 52);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(41, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Огноо*:";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(416, 476);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 21;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(497, 476);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 22;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // dateEditOrderDate
            // 
            this.dateEditOrderDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditOrderDate.EditValue = null;
            this.dateEditOrderDate.Location = new System.Drawing.Point(222, 49);
            this.dateEditOrderDate.Name = "dateEditOrderDate";
            this.dateEditOrderDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOrderDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOrderDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditOrderDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditOrderDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditOrderDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditOrderDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditOrderDate.Properties.ReadOnly = true;
            this.dateEditOrderDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditOrderDate.TabIndex = 2;
            // 
            // textEditID
            // 
            this.textEditID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditID.Location = new System.Drawing.Point(222, 23);
            this.textEditID.Name = "textEditID";
            this.textEditID.Properties.ReadOnly = true;
            this.textEditID.Size = new System.Drawing.Size(200, 20);
            this.textEditID.TabIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl7.Location = new System.Drawing.Point(117, 26);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(99, 13);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "Баримтын дугаар*:";
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.memoEditDescription.Location = new System.Drawing.Point(222, 153);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Properties.ReadOnly = true;
            this.memoEditDescription.Size = new System.Drawing.Size(315, 41);
            this.memoEditDescription.TabIndex = 7;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(167, 155);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(49, 13);
            this.labelControl8.TabIndex = 18;
            this.labelControl8.Text = "Тайлбар :";
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl13.Location = new System.Drawing.Point(175, 78);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(41, 13);
            this.labelControl13.TabIndex = 45;
            this.labelControl13.Text = "Нярав*:";
            // 
            // labelControlExpUser
            // 
            this.labelControlExpUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlExpUser.Location = new System.Drawing.Point(169, 130);
            this.labelControlExpUser.Name = "labelControlExpUser";
            this.labelControlExpUser.Size = new System.Drawing.Size(47, 13);
            this.labelControlExpUser.TabIndex = 54;
            this.labelControlExpUser.Text = "Тендер*:";
            // 
            // textEditUserName
            // 
            this.textEditUserName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditUserName.Location = new System.Drawing.Point(222, 75);
            this.textEditUserName.Name = "textEditUserName";
            this.textEditUserName.Properties.ReadOnly = true;
            this.textEditUserName.Size = new System.Drawing.Size(200, 20);
            this.textEditUserName.TabIndex = 3;
            // 
            // textEditWarehouseName
            // 
            this.textEditWarehouseName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditWarehouseName.Location = new System.Drawing.Point(222, 101);
            this.textEditWarehouseName.Name = "textEditWarehouseName";
            this.textEditWarehouseName.Properties.ReadOnly = true;
            this.textEditWarehouseName.Size = new System.Drawing.Size(200, 20);
            this.textEditWarehouseName.TabIndex = 5;
            // 
            // textEditExpUserName
            // 
            this.textEditExpUserName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditExpUserName.Location = new System.Drawing.Point(222, 127);
            this.textEditExpUserName.Name = "textEditExpUserName";
            this.textEditExpUserName.Properties.ReadOnly = true;
            this.textEditExpUserName.Size = new System.Drawing.Size(200, 20);
            this.textEditExpUserName.TabIndex = 6;
            // 
            // checkEditIsConfirm
            // 
            this.checkEditIsConfirm.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkEditIsConfirm.Location = new System.Drawing.Point(222, 200);
            this.checkEditIsConfirm.Name = "checkEditIsConfirm";
            this.checkEditIsConfirm.Properties.Caption = "";
            this.checkEditIsConfirm.Size = new System.Drawing.Size(20, 19);
            this.checkEditIsConfirm.TabIndex = 55;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(121, 203);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(95, 13);
            this.labelControl1.TabIndex = 56;
            this.labelControl1.Text = "Баталгаажуулах*:";
            // 
            // gridControlDetail
            // 
            this.gridControlDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlDetail.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlDetail.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlDetail.Location = new System.Drawing.Point(12, 225);
            this.gridControlDetail.MainView = this.gridViewDetail;
            this.gridControlDetail.Name = "gridControlDetail";
            this.gridControlDetail.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1});
            this.gridControlDetail.Size = new System.Drawing.Size(560, 245);
            this.gridControlDetail.TabIndex = 57;
            this.gridControlDetail.UseEmbeddedNavigator = true;
            this.gridControlDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDetail});
            // 
            // gridViewDetail
            // 
            this.gridViewDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumnCount,
            this.gridColumnConf1,
            this.gridColumnConf2,
            this.gridColumnConf3,
            this.gridColumnConf4,
            this.gridColumnConf5,
            this.gridColumnConfirmCount});
            this.gridViewDetail.GridControl = this.gridControlDetail;
            this.gridViewDetail.Name = "gridViewDetail";
            this.gridViewDetail.OptionsView.ShowFooter = true;
            this.gridViewDetail.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Эмийн нэр";
            this.gridColumn2.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn2.FieldName = "medicineName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 111;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Олон улсын нэр";
            this.gridColumn3.CustomizationCaption = "Олон улсын нэр";
            this.gridColumn3.FieldName = "latinName";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 110;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Тун хэмжээ";
            this.gridColumn4.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn4.FieldName = "unitType";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumnCount
            // 
            this.gridColumnCount.Caption = "Захиалсан тоо";
            this.gridColumnCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnCount.CustomizationCaption = "Захиалсан тоо";
            this.gridColumnCount.DisplayFormat.FormatString = "n2";
            this.gridColumnCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnCount.FieldName = "count";
            this.gridColumnCount.MinWidth = 75;
            this.gridColumnCount.Name = "gridColumnCount";
            this.gridColumnCount.OptionsColumn.ReadOnly = true;
            this.gridColumnCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "count", "{0:n2}")});
            this.gridColumnCount.Visible = true;
            this.gridColumnCount.VisibleIndex = 3;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnConf1
            // 
            this.gridColumnConf1.Caption = "Шатлал №1";
            this.gridColumnConf1.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnConf1.FieldName = "1";
            this.gridColumnConf1.MinWidth = 120;
            this.gridColumnConf1.Name = "gridColumnConf1";
            this.gridColumnConf1.Visible = true;
            this.gridColumnConf1.VisibleIndex = 4;
            this.gridColumnConf1.Width = 120;
            // 
            // gridColumnConf2
            // 
            this.gridColumnConf2.Caption = "Шатлал №2";
            this.gridColumnConf2.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnConf2.FieldName = "2";
            this.gridColumnConf2.MinWidth = 120;
            this.gridColumnConf2.Name = "gridColumnConf2";
            this.gridColumnConf2.Visible = true;
            this.gridColumnConf2.VisibleIndex = 5;
            this.gridColumnConf2.Width = 120;
            // 
            // gridColumnConf3
            // 
            this.gridColumnConf3.Caption = "Шатлал №3";
            this.gridColumnConf3.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnConf3.FieldName = "3";
            this.gridColumnConf3.MinWidth = 120;
            this.gridColumnConf3.Name = "gridColumnConf3";
            this.gridColumnConf3.Visible = true;
            this.gridColumnConf3.VisibleIndex = 6;
            this.gridColumnConf3.Width = 120;
            // 
            // gridColumnConf4
            // 
            this.gridColumnConf4.Caption = "Шатлал №4";
            this.gridColumnConf4.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnConf4.FieldName = "4";
            this.gridColumnConf4.MinWidth = 120;
            this.gridColumnConf4.Name = "gridColumnConf4";
            this.gridColumnConf4.Visible = true;
            this.gridColumnConf4.VisibleIndex = 7;
            this.gridColumnConf4.Width = 120;
            // 
            // gridColumnConf5
            // 
            this.gridColumnConf5.Caption = "Шатлал №5";
            this.gridColumnConf5.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnConf5.FieldName = "5";
            this.gridColumnConf5.MinWidth = 120;
            this.gridColumnConf5.Name = "gridColumnConf5";
            this.gridColumnConf5.Visible = true;
            this.gridColumnConf5.VisibleIndex = 8;
            this.gridColumnConf5.Width = 120;
            // 
            // gridColumnConfirmCount
            // 
            this.gridColumnConfirmCount.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnConfirmCount.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnConfirmCount.Caption = "Баталсан тоо";
            this.gridColumnConfirmCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnConfirmCount.CustomizationCaption = "Баталсан тоо";
            this.gridColumnConfirmCount.FieldName = "confirmCount";
            this.gridColumnConfirmCount.MinWidth = 75;
            this.gridColumnConfirmCount.Name = "gridColumnConfirmCount";
            this.gridColumnConfirmCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "confirmCount", "{0:n2}")});
            this.gridColumnConfirmCount.Visible = true;
            this.gridColumnConfirmCount.VisibleIndex = 9;
            this.gridColumnConfirmCount.Width = 92;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(467, 203);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(79, 13);
            this.labelControl2.TabIndex = 59;
            this.labelControl2.Text = "Бүгдийг олгох :";
            // 
            // checkEditChooseAll
            // 
            this.checkEditChooseAll.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkEditChooseAll.Location = new System.Drawing.Point(552, 200);
            this.checkEditChooseAll.Name = "checkEditChooseAll";
            this.checkEditChooseAll.Properties.Caption = "";
            this.checkEditChooseAll.Size = new System.Drawing.Size(20, 19);
            this.checkEditChooseAll.TabIndex = 58;
            this.checkEditChooseAll.CheckedChanged += new System.EventHandler(this.checkEditChooseAll_CheckedChanged);
            // 
            // MedicineOrderConfirmForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(584, 511);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.checkEditChooseAll);
            this.Controls.Add(this.gridControlDetail);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.checkEditIsConfirm);
            this.Controls.Add(this.textEditExpUserName);
            this.Controls.Add(this.textEditWarehouseName);
            this.Controls.Add(this.textEditUserName);
            this.Controls.Add(this.labelControlExpUser);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.memoEditDescription);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.textEditID);
            this.Controls.Add(this.dateEditOrderDate);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonSave);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControlWarehouse);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MedicineOrderConfirmForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Захиалгын хуудас баталгаажуулах";
            this.Shown += new System.EventHandler(this.MedicineOrderInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOrderDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWarehouseName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditExpUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsConfirm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChooseAll.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControlWarehouse;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.DateEdit dateEditOrderDate;
        private DevExpress.XtraEditors.TextEdit textEditID;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControlExpUser;
        private DevExpress.XtraEditors.TextEdit textEditUserName;
        private DevExpress.XtraEditors.TextEdit textEditWarehouseName;
        private DevExpress.XtraEditors.TextEdit textEditExpUserName;
        private DevExpress.XtraEditors.CheckEdit checkEditIsConfirm;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gridControlDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConfirmCount;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit checkEditChooseAll;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConf1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConf2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConf3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConf4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConf5;
    }
}