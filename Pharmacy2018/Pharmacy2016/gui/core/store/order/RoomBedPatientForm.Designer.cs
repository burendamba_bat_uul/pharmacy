﻿namespace Pharmacy2016.gui.core.store.order
{
    partial class RoomBedPatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoomBedPatientForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonExport = new DevExpress.XtraEditors.SimpleButton();
            this.treeListLookUpEditWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.dateEditDate = new DevExpress.XtraEditors.DateEdit();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageBed = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlRoom = new DevExpress.XtraGrid.GridControl();
            this.gridViewRoom = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnRoom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnBed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnWard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageWard = new DevExpress.XtraTab.XtraTabPage();
            this.treeListWard = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnFullName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnStart = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumnEnd = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageBed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRoom)).BeginInit();
            this.xtraTabPageWard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonLayout);
            this.panelControl1.Controls.Add(this.simpleButtonExport);
            this.panelControl1.Controls.Add(this.treeListLookUpEditWard);
            this.panelControl1.Controls.Add(this.dateEditDate);
            this.panelControl1.Controls.Add(this.simpleButtonReload);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 50);
            this.panelControl1.TabIndex = 4;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(749, 12);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 15;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // simpleButtonExport
            // 
            this.simpleButtonExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonExport.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonExport.Image")));
            this.simpleButtonExport.Location = new System.Drawing.Point(653, 12);
            this.simpleButtonExport.Name = "simpleButtonExport";
            this.simpleButtonExport.Size = new System.Drawing.Size(90, 23);
            this.simpleButtonExport.TabIndex = 14;
            this.simpleButtonExport.Text = "Хөрвүүлэх";
            this.simpleButtonExport.ToolTip = "Тасаг, өрөөний эмчлүүлэгч хөрвүүлэх";
            this.simpleButtonExport.Click += new System.EventHandler(this.simpleButtonExport_Click);
            // 
            // treeListLookUpEditWard
            // 
            this.treeListLookUpEditWard.Location = new System.Drawing.Point(12, 13);
            this.treeListLookUpEditWard.Name = "treeListLookUpEditWard";
            this.treeListLookUpEditWard.Properties.ActionButtonIndex = 1;
            this.treeListLookUpEditWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("treeListLookUpEditWard.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "reload", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditWard.Properties.NullText = "Тасаг сонгох";
            this.treeListLookUpEditWard.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditWard.Properties.ValueMember = "id";
            this.treeListLookUpEditWard.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.treeListLookUpEditWard_Properties_ButtonClick);
            this.treeListLookUpEditWard.Size = new System.Drawing.Size(200, 22);
            this.treeListLookUpEditWard.TabIndex = 13;
            this.treeListLookUpEditWard.ToolTip = "Тасаг сонгох";
            this.treeListLookUpEditWard.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.treeListLookUpEditWard_CustomDisplayText);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(192, 41);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsSelection.MultiSelect = true;
            this.treeList1.OptionsView.ShowCheckBoxes = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.OptionsView.ShowIndicator = false;
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Нэр";
            this.treeListColumn2.FieldName = "name";
            this.treeListColumn2.MinWidth = 32;
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // dateEditDate
            // 
            this.dateEditDate.EditValue = null;
            this.dateEditDate.Location = new System.Drawing.Point(218, 14);
            this.dateEditDate.Name = "dateEditDate";
            this.dateEditDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditDate.TabIndex = 8;
            this.dateEditDate.ToolTip = "Огноо";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(324, 12);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonReload.TabIndex = 6;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Тасаг, өрөөний эмчлүүлэгч шүүх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl.Location = new System.Drawing.Point(0, 50);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageBed;
            this.xtraTabControl.Size = new System.Drawing.Size(784, 411);
            this.xtraTabControl.TabIndex = 5;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageBed,
            this.xtraTabPageWard});
            // 
            // xtraTabPageBed
            // 
            this.xtraTabPageBed.Controls.Add(this.gridControlRoom);
            this.xtraTabPageBed.Name = "xtraTabPageBed";
            this.xtraTabPageBed.Size = new System.Drawing.Size(778, 383);
            this.xtraTabPageBed.Text = "Өрөө, ор";
            // 
            // gridControlRoom
            // 
            this.gridControlRoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRoom.Location = new System.Drawing.Point(0, 0);
            this.gridControlRoom.MainView = this.gridViewRoom;
            this.gridControlRoom.Name = "gridControlRoom";
            this.gridControlRoom.Size = new System.Drawing.Size(778, 383);
            this.gridControlRoom.TabIndex = 0;
            this.gridControlRoom.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRoom});
            // 
            // gridViewRoom
            // 
            this.gridViewRoom.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnRoom,
            this.gridColumnBed,
            this.gridColumnWard,
            this.gridColumnFullName,
            this.gridColumnStart,
            this.gridColumnEnd});
            this.gridViewRoom.GridControl = this.gridControlRoom;
            this.gridViewRoom.Name = "gridViewRoom";
            this.gridViewRoom.OptionsBehavior.ReadOnly = true;
            this.gridViewRoom.OptionsCustomization.AllowGroup = false;
            this.gridViewRoom.OptionsView.ShowAutoFilterRow = true;
            this.gridViewRoom.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnRoom
            // 
            this.gridColumnRoom.Caption = "Өрөө";
            this.gridColumnRoom.CustomizationCaption = "Өрөө";
            this.gridColumnRoom.FieldName = "roomName";
            this.gridColumnRoom.Name = "gridColumnRoom";
            this.gridColumnRoom.Visible = true;
            this.gridColumnRoom.VisibleIndex = 1;
            this.gridColumnRoom.Width = 127;
            // 
            // gridColumnBed
            // 
            this.gridColumnBed.Caption = "Ор";
            this.gridColumnBed.CustomizationCaption = "Ор";
            this.gridColumnBed.FieldName = "bedName";
            this.gridColumnBed.Name = "gridColumnBed";
            this.gridColumnBed.Visible = true;
            this.gridColumnBed.VisibleIndex = 2;
            this.gridColumnBed.Width = 125;
            // 
            // gridColumnWard
            // 
            this.gridColumnWard.Caption = "Тасаг";
            this.gridColumnWard.CustomizationCaption = "Тасаг";
            this.gridColumnWard.FieldName = "wardName";
            this.gridColumnWard.Name = "gridColumnWard";
            this.gridColumnWard.Visible = true;
            this.gridColumnWard.VisibleIndex = 0;
            this.gridColumnWard.Width = 284;
            // 
            // gridColumnFullName
            // 
            this.gridColumnFullName.Caption = "Эмчлүүлэгч";
            this.gridColumnFullName.CustomizationCaption = "Эмчлүүлэгчийн нэр";
            this.gridColumnFullName.FieldName = "fullName";
            this.gridColumnFullName.Name = "gridColumnFullName";
            this.gridColumnFullName.Visible = true;
            this.gridColumnFullName.VisibleIndex = 3;
            this.gridColumnFullName.Width = 224;
            // 
            // gridColumnStart
            // 
            this.gridColumnStart.Caption = "Нээсэн огноо";
            this.gridColumnStart.CustomizationCaption = "Өвчний түүх нээсэн огноо";
            this.gridColumnStart.FieldName = "startDate";
            this.gridColumnStart.MinWidth = 75;
            this.gridColumnStart.Name = "gridColumnStart";
            this.gridColumnStart.Visible = true;
            this.gridColumnStart.VisibleIndex = 4;
            // 
            // gridColumnEnd
            // 
            this.gridColumnEnd.Caption = "Хаасан огноо";
            this.gridColumnEnd.CustomizationCaption = "Өвчний түүх хаасан огноо";
            this.gridColumnEnd.FieldName = "endDate";
            this.gridColumnEnd.MinWidth = 75;
            this.gridColumnEnd.Name = "gridColumnEnd";
            this.gridColumnEnd.Visible = true;
            this.gridColumnEnd.VisibleIndex = 5;
            // 
            // xtraTabPageWard
            // 
            this.xtraTabPageWard.Controls.Add(this.treeListWard);
            this.xtraTabPageWard.Name = "xtraTabPageWard";
            this.xtraTabPageWard.Size = new System.Drawing.Size(778, 383);
            this.xtraTabPageWard.Text = "Тасаг, өрөө";
            // 
            // treeListWard
            // 
            this.treeListWard.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnName,
            this.treeListColumnFullName,
            this.treeListColumnStart,
            this.treeListColumnEnd});
            this.treeListWard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListWard.KeyFieldName = "id";
            this.treeListWard.Location = new System.Drawing.Point(0, 0);
            this.treeListWard.Name = "treeListWard";
            this.treeListWard.OptionsBehavior.ReadOnly = true;
            this.treeListWard.ParentFieldName = "pid";
            this.treeListWard.Size = new System.Drawing.Size(778, 383);
            this.treeListWard.TabIndex = 0;
            // 
            // treeListColumnName
            // 
            this.treeListColumnName.Caption = "Нэр";
            this.treeListColumnName.CustomizationCaption = "Нэр";
            this.treeListColumnName.FieldName = "name";
            this.treeListColumnName.Name = "treeListColumnName";
            this.treeListColumnName.Visible = true;
            this.treeListColumnName.VisibleIndex = 0;
            this.treeListColumnName.Width = 189;
            // 
            // treeListColumnFullName
            // 
            this.treeListColumnFullName.Caption = "Эмчлүүлэгч";
            this.treeListColumnFullName.CustomizationCaption = "Эмчлүүлэгчийн нэр";
            this.treeListColumnFullName.FieldName = "fullName";
            this.treeListColumnFullName.Name = "treeListColumnFullName";
            this.treeListColumnFullName.Visible = true;
            this.treeListColumnFullName.VisibleIndex = 1;
            this.treeListColumnFullName.Width = 271;
            // 
            // treeListColumnStart
            // 
            this.treeListColumnStart.Caption = "Нээсэн огноо";
            this.treeListColumnStart.CustomizationCaption = "Өвчний түүх нээсэн огноо";
            this.treeListColumnStart.FieldName = "startDate";
            this.treeListColumnStart.Name = "treeListColumnStart";
            this.treeListColumnStart.Visible = true;
            this.treeListColumnStart.VisibleIndex = 2;
            this.treeListColumnStart.Width = 150;
            // 
            // treeListColumnEnd
            // 
            this.treeListColumnEnd.Caption = "Хаасан огноо";
            this.treeListColumnEnd.CustomizationCaption = "Өвчний түүх хаасан огноо";
            this.treeListColumnEnd.FieldName = "endDate";
            this.treeListColumnEnd.Name = "treeListColumnEnd";
            this.treeListColumnEnd.Visible = true;
            this.treeListColumnEnd.VisibleIndex = 3;
            this.treeListColumnEnd.Width = 150;
            // 
            // RoomBedPatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.xtraTabControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "RoomBedPatientForm";
            this.ShowInTaskbar = false;
            this.Text = "Тасаг, өрөөний эмчлүүлэгч";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RoomBedPatientForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageBed.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRoom)).EndInit();
            this.xtraTabPageWard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageBed;
        private DevExpress.XtraGrid.GridControl gridControlRoom;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRoom;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRoom;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBed;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnWard;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWard;
        private DevExpress.XtraTreeList.TreeList treeListWard;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnName;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.DateEdit dateEditDate;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWard;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnFullName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnFullName;
        private DevExpress.XtraEditors.SimpleButton simpleButtonExport;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEnd;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnStart;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnEnd;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;
    }
}