﻿namespace Pharmacy2016.gui.core.store.order
{
    partial class OrderInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderInUpForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.simpleButtonCloseRegisterOrderForm = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSaveRegisterOrder = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonCheck = new DevExpress.XtraEditors.SimpleButton();
            this.textEditProvince = new DevExpress.XtraEditors.TextEdit();
            this.gridLookUpEditPatient = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.radioGroupGender = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditAddress = new DevExpress.XtraEditors.MemoEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.textEditPhone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.textEditNumber = new DevExpress.XtraEditors.TextEdit();
            this.textEditFirstName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupOrderState = new DevExpress.XtraEditors.RadioGroup();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.textEditRegister = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEditSirName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditBirthday = new DevExpress.XtraEditors.DateEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditGetOrderDate = new DevExpress.XtraEditors.DateEdit();
            this.textEditLastName = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProvince.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupOrderState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegister.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSirName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditGetOrderDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditGetOrderDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonCloseRegisterOrderForm
            // 
            this.simpleButtonCloseRegisterOrderForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCloseRegisterOrderForm.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCloseRegisterOrderForm.Location = new System.Drawing.Point(372, 526);
            this.simpleButtonCloseRegisterOrderForm.Name = "simpleButtonCloseRegisterOrderForm";
            this.simpleButtonCloseRegisterOrderForm.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCloseRegisterOrderForm.TabIndex = 22;
            this.simpleButtonCloseRegisterOrderForm.Text = "Болих";
            this.simpleButtonCloseRegisterOrderForm.ToolTip = "Болих";
            // 
            // simpleButtonSaveRegisterOrder
            // 
            this.simpleButtonSaveRegisterOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSaveRegisterOrder.Location = new System.Drawing.Point(291, 526);
            this.simpleButtonSaveRegisterOrder.Name = "simpleButtonSaveRegisterOrder";
            this.simpleButtonSaveRegisterOrder.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSaveRegisterOrder.TabIndex = 21;
            this.simpleButtonSaveRegisterOrder.Text = "Хадгалах";
            this.simpleButtonSaveRegisterOrder.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSaveRegisterOrder.Click += new System.EventHandler(this.simpleButtonSaveOrder_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButtonCheck);
            this.panelControl2.Controls.Add(this.textEditProvince);
            this.panelControl2.Controls.Add(this.gridLookUpEditPatient);
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Controls.Add(this.simpleButtonCloseRegisterOrderForm);
            this.panelControl2.Controls.Add(this.simpleButtonSaveRegisterOrder);
            this.panelControl2.Controls.Add(this.radioGroupGender);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.memoEditAddress);
            this.panelControl2.Controls.Add(this.label2);
            this.panelControl2.Controls.Add(this.textEditPhone);
            this.panelControl2.Controls.Add(this.labelControl10);
            this.panelControl2.Controls.Add(this.textEditNumber);
            this.panelControl2.Controls.Add(this.textEditFirstName);
            this.panelControl2.Controls.Add(this.labelControl9);
            this.panelControl2.Controls.Add(this.radioGroupOrderState);
            this.panelControl2.Controls.Add(this.memoEditDescription);
            this.panelControl2.Controls.Add(this.label1);
            this.panelControl2.Controls.Add(this.textEditRegister);
            this.panelControl2.Controls.Add(this.labelControl8);
            this.panelControl2.Controls.Add(this.labelControl7);
            this.panelControl2.Controls.Add(this.textEditSirName);
            this.panelControl2.Controls.Add(this.labelControl6);
            this.panelControl2.Controls.Add(this.dateEditBirthday);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.dateEditGetOrderDate);
            this.panelControl2.Controls.Add(this.textEditLastName);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(459, 561);
            this.panelControl2.TabIndex = 1;
            // 
            // simpleButtonCheck
            // 
            this.simpleButtonCheck.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonCheck.Image")));
            this.simpleButtonCheck.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonCheck.Location = new System.Drawing.Point(364, 80);
            this.simpleButtonCheck.Name = "simpleButtonCheck";
            this.simpleButtonCheck.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonCheck.TabIndex = 24;
            this.simpleButtonCheck.ToolTip = "Эмчлүүлэгч дугаар авсан эсэхийг шалгах";
            this.simpleButtonCheck.Click += new System.EventHandler(this.simpleButtonCheck_Click);
            // 
            // textEditProvince
            // 
            this.textEditProvince.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditProvince.Location = new System.Drawing.Point(158, 296);
            this.textEditProvince.Name = "textEditProvince";
            this.textEditProvince.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEditProvince.Properties.Appearance.Options.UseBackColor = true;
            this.textEditProvince.Properties.MaxLength = 50;
            this.textEditProvince.Properties.ReadOnly = true;
            this.textEditProvince.Size = new System.Drawing.Size(200, 20);
            this.textEditProvince.TabIndex = 11;
            // 
            // gridLookUpEditPatient
            // 
            this.gridLookUpEditPatient.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditPatient.EditValue = "";
            this.gridLookUpEditPatient.Location = new System.Drawing.Point(158, 82);
            this.gridLookUpEditPatient.Name = "gridLookUpEditPatient";
            this.gridLookUpEditPatient.Properties.ActionButtonIndex = 1;
            serializableAppearanceObject1.Options.UseImage = true;
            this.gridLookUpEditPatient.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.Default, ((System.Drawing.Image)(resources.GetObject("gridLookUpEditPatient.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "add_patient", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPatient.Properties.DisplayMember = "fullName";
            this.gridLookUpEditPatient.Properties.NullText = "";
            this.gridLookUpEditPatient.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditPatient.Properties.ValueMember = "id";
            this.gridLookUpEditPatient.Properties.View = this.gridView1;
            this.gridLookUpEditPatient.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditPatient.TabIndex = 3;
            this.gridLookUpEditPatient.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditPatient_ButtonClick);
            this.gridLookUpEditPatient.EditValueChanged += new System.EventHandler(this.gridLookUpEditPatient_EditValueChanged);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn3,
            this.gridColumn7,
            this.gridColumn8});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Нэр";
            this.gridColumn2.CustomizationCaption = "Нэр";
            this.gridColumn2.FieldName = "firstName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Регистер";
            this.gridColumn4.CustomizationCaption = "Регистер";
            this.gridColumn4.FieldName = "id";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Төрсөн өдөр";
            this.gridColumn5.CustomizationCaption = "Төрсөн өдөр";
            this.gridColumn5.FieldName = "birthday";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Утас";
            this.gridColumn3.CustomizationCaption = "Утас";
            this.gridColumn3.FieldName = "phone";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Ажлын газар";
            this.gridColumn7.CustomizationCaption = "Ажлын газар";
            this.gridColumn7.FieldName = "organizationName";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Албан тушаал";
            this.gridColumn8.CustomizationCaption = "Албан тушаал";
            this.gridColumn8.FieldName = "positionName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 6;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 526);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 23;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // radioGroupGender
            // 
            this.radioGroupGender.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupGender.EditValue = false;
            this.radioGroupGender.Location = new System.Drawing.Point(158, 213);
            this.radioGroupGender.Name = "radioGroupGender";
            this.radioGroupGender.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.radioGroupGender.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroupGender.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Эр", true, "Эр"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эм", true, "Эм")});
            this.radioGroupGender.Properties.ReadOnly = true;
            this.radioGroupGender.Size = new System.Drawing.Size(100, 25);
            this.radioGroupGender.TabIndex = 8;
            // 
            // labelControl12
            // 
            this.labelControl12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl12.Location = new System.Drawing.Point(123, 219);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(29, 13);
            this.labelControl12.TabIndex = 59;
            this.labelControl12.Text = "Хүйс :";
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl11.Location = new System.Drawing.Point(101, 299);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(51, 13);
            this.labelControl11.TabIndex = 56;
            this.labelControl11.Text = "Байршил :";
            // 
            // memoEditAddress
            // 
            this.memoEditAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditAddress.Location = new System.Drawing.Point(158, 322);
            this.memoEditAddress.Name = "memoEditAddress";
            this.memoEditAddress.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.memoEditAddress.Properties.Appearance.Options.UseBackColor = true;
            this.memoEditAddress.Properties.MaxLength = 200;
            this.memoEditAddress.Properties.ReadOnly = true;
            this.memoEditAddress.Size = new System.Drawing.Size(200, 50);
            this.memoEditAddress.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(115, 325);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 54;
            this.label2.Text = "Хаяг :";
            // 
            // textEditPhone
            // 
            this.textEditPhone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditPhone.Location = new System.Drawing.Point(158, 270);
            this.textEditPhone.Name = "textEditPhone";
            this.textEditPhone.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEditPhone.Properties.Appearance.Options.UseBackColor = true;
            this.textEditPhone.Properties.MaxLength = 50;
            this.textEditPhone.Properties.ReadOnly = true;
            this.textEditPhone.Size = new System.Drawing.Size(200, 20);
            this.textEditPhone.TabIndex = 10;
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl10.Location = new System.Drawing.Point(121, 273);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(31, 13);
            this.labelControl10.TabIndex = 47;
            this.labelControl10.Text = "Утас :";
            // 
            // textEditNumber
            // 
            this.textEditNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditNumber.Location = new System.Drawing.Point(158, 30);
            this.textEditNumber.Name = "textEditNumber";
            this.textEditNumber.Properties.Mask.EditMask = "d";
            this.textEditNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditNumber.Properties.MaxLength = 50;
            this.textEditNumber.Size = new System.Drawing.Size(200, 20);
            this.textEditNumber.TabIndex = 1;
            // 
            // textEditFirstName
            // 
            this.textEditFirstName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditFirstName.Location = new System.Drawing.Point(158, 162);
            this.textEditFirstName.Name = "textEditFirstName";
            this.textEditFirstName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEditFirstName.Properties.Appearance.Options.UseBackColor = true;
            this.textEditFirstName.Properties.MaxLength = 50;
            this.textEditFirstName.Properties.ReadOnly = true;
            this.textEditFirstName.Size = new System.Drawing.Size(200, 20);
            this.textEditFirstName.TabIndex = 6;
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl9.Location = new System.Drawing.Point(112, 461);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(40, 13);
            this.labelControl9.TabIndex = 44;
            this.labelControl9.Text = "Төлөв*:";
            // 
            // radioGroupOrderState
            // 
            this.radioGroupOrderState.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupOrderState.EditValue = ((byte)(0));
            this.radioGroupOrderState.Location = new System.Drawing.Point(158, 454);
            this.radioGroupOrderState.Name = "radioGroupOrderState";
            this.radioGroupOrderState.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(0)), "Хүлээлгэх", true, ((short)(0))),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(1)), "Хэвтүүлэх", true, ((short)(1)))});
            this.radioGroupOrderState.Size = new System.Drawing.Size(200, 45);
            this.radioGroupOrderState.TabIndex = 14;
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditDescription.Location = new System.Drawing.Point(158, 378);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(200, 70);
            this.memoEditDescription.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(99, 381);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "Тайлбар:";
            // 
            // textEditRegister
            // 
            this.textEditRegister.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditRegister.Location = new System.Drawing.Point(158, 188);
            this.textEditRegister.Name = "textEditRegister";
            this.textEditRegister.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEditRegister.Properties.Appearance.Options.UseBackColor = true;
            this.textEditRegister.Properties.MaxLength = 50;
            this.textEditRegister.Properties.ReadOnly = true;
            this.textEditRegister.Size = new System.Drawing.Size(200, 20);
            this.textEditRegister.TabIndex = 7;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(99, 191);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(53, 13);
            this.labelControl8.TabIndex = 39;
            this.labelControl8.Text = "Регистер :";
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl7.Location = new System.Drawing.Point(85, 85);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(67, 13);
            this.labelControl7.TabIndex = 38;
            this.labelControl7.Text = "Эмчлүүлэгч*:";
            // 
            // textEditSirName
            // 
            this.textEditSirName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditSirName.Location = new System.Drawing.Point(158, 110);
            this.textEditSirName.Name = "textEditSirName";
            this.textEditSirName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEditSirName.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            this.textEditSirName.Properties.Appearance.Options.UseBackColor = true;
            this.textEditSirName.Properties.Appearance.Options.UseForeColor = true;
            this.textEditSirName.Properties.MaxLength = 50;
            this.textEditSirName.Properties.ReadOnly = true;
            this.textEditSirName.Size = new System.Drawing.Size(200, 20);
            this.textEditSirName.TabIndex = 4;
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl6.Location = new System.Drawing.Point(105, 33);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(47, 13);
            this.labelControl6.TabIndex = 34;
            this.labelControl6.Text = "Дугаар*:";
            // 
            // dateEditBirthday
            // 
            this.dateEditBirthday.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditBirthday.EditValue = null;
            this.dateEditBirthday.Location = new System.Drawing.Point(158, 244);
            this.dateEditBirthday.Name = "dateEditBirthday";
            this.dateEditBirthday.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.dateEditBirthday.Properties.Appearance.Options.UseBackColor = true;
            this.dateEditBirthday.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBirthday.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBirthday.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBirthday.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBirthday.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBirthday.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBirthday.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditBirthday.Properties.ReadOnly = true;
            this.dateEditBirthday.Size = new System.Drawing.Size(100, 20);
            this.dateEditBirthday.TabIndex = 9;
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(41, 59);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(111, 13);
            this.labelControl5.TabIndex = 30;
            this.labelControl5.Text = "Дугаар авсан огноо*:";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(78, 247);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(74, 13);
            this.labelControl4.TabIndex = 29;
            this.labelControl4.Text = "Төрсөн огноо :";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(83, 113);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(69, 13);
            this.labelControl3.TabIndex = 28;
            this.labelControl3.Text = "Ургийн овог :";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(120, 139);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(32, 13);
            this.labelControl2.TabIndex = 27;
            this.labelControl2.Text = "Овог :";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(127, 165);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(25, 13);
            this.labelControl1.TabIndex = 26;
            this.labelControl1.Text = "Нэр :";
            // 
            // dateEditGetOrderDate
            // 
            this.dateEditGetOrderDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditGetOrderDate.EditValue = null;
            this.dateEditGetOrderDate.Location = new System.Drawing.Point(158, 56);
            this.dateEditGetOrderDate.Name = "dateEditGetOrderDate";
            this.dateEditGetOrderDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditGetOrderDate.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditGetOrderDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditGetOrderDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditGetOrderDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditGetOrderDate.Properties.EditFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditGetOrderDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditGetOrderDate.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.dateEditGetOrderDate.Size = new System.Drawing.Size(140, 20);
            this.dateEditGetOrderDate.TabIndex = 2;
            // 
            // textEditLastName
            // 
            this.textEditLastName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditLastName.Location = new System.Drawing.Point(158, 136);
            this.textEditLastName.Name = "textEditLastName";
            this.textEditLastName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEditLastName.Properties.Appearance.Options.UseBackColor = true;
            this.textEditLastName.Properties.MaxLength = 50;
            this.textEditLastName.Properties.ReadOnly = true;
            this.textEditLastName.Size = new System.Drawing.Size(200, 20);
            this.textEditLastName.TabIndex = 5;
            // 
            // OrderInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCloseRegisterOrderForm;
            this.ClientSize = new System.Drawing.Size(459, 561);
            this.Controls.Add(this.panelControl2);
            this.Name = "OrderInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Дугаар олгох";
            this.Shown += new System.EventHandler(this.OrderInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProvince.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupOrderState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegister.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSirName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthday.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditGetOrderDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditGetOrderDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonCloseRegisterOrderForm;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSaveRegisterOrder;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.RadioGroup radioGroupOrderState;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEditRegister;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEditSirName;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.DateEdit dateEditBirthday;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditGetOrderDate;
        private DevExpress.XtraEditors.TextEdit textEditLastName;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit textEditFirstName;
        private DevExpress.XtraEditors.TextEdit textEditNumber;
        private DevExpress.XtraEditors.TextEdit textEditPhone;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.MemoEdit memoEditAddress;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.RadioGroup radioGroupGender;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPatient;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.TextEdit textEditProvince;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCheck;

    }
}