﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.patient;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.store.store;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.store.order
{
    /*
     * Хүлээлгийн жагсаалт нэмэх, засах цонх.
     * 
     */

    public partial class OrderInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static OrderInUpForm INSTANCE = new OrderInUpForm();

            public static OrderInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            public DataRowView currentPatientView;

            private int actionType = 0;

            public string currID;

            private OrderInUpForm()
            {

            }

            
            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                dateEditGetOrderDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditGetOrderDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                
                initError();
                initDataSource();
                initBinding();
                reload();
            }

            private void initDataSource()
            {
                gridLookUpEditPatient.Properties.DataSource = StoreDataSet.PatientOrderBindingSource;
            }

            private void initBinding()
            {
                gridLookUpEditPatient.DataBindings.Add(new Binding("EditValue", StoreDataSet.OrderBindingSource, "userID", true));
                textEditRegister.DataBindings.Add(new Binding("EditValue", StoreDataSet.OrderBindingSource, "userID", true));
                textEditProvince.DataBindings.Add(new Binding("EditValue", StoreDataSet.OrderBindingSource, "provinceName", true));
                textEditNumber.DataBindings.Add(new Binding("EditValue", StoreDataSet.OrderBindingSource, "number", true));
                dateEditGetOrderDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.OrderBindingSource, "date", true));
                dateEditBirthday.DataBindings.Add(new Binding("EditValue", StoreDataSet.OrderBindingSource, "birthday", true));
                radioGroupGender.DataBindings.Add(new Binding("EditValue", StoreDataSet.OrderBindingSource, "gender", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", StoreDataSet.OrderBindingSource, "description", true));
                //radioGroupOrderState.DataBindings.Add(new Binding("EditValue", StoreDataSet.OrderBindingSource, "orderState", true));
            }

            private void initError()
            {
                gridLookUpEditPatient.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                memoEditAddress.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditNumber.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditGetOrderDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insertOrder();
                    }
                    else if (actionType == 2)
                    {
                        editOrder();
                    }

                    ShowDialog(OrderForm.Instance);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insertOrder()
            {
                gridLookUpEditPatient.Properties.Buttons[0].Enabled = true;
                gridLookUpEditPatient.Properties.Buttons[0].Visible = true;
                DateTime now = DateTime.Now;
                OrderForm.Instance.currentOrderView["date"] = now;
                dateEditGetOrderDate.DateTime = now;


                textEditSirName.EditValue = null;
                textEditLastName.EditValue = null;
                textEditFirstName.EditValue = null;
                textEditRegister.EditValue = null;
                textEditPhone.EditValue = null;
                radioGroupGender.EditValue = null;
                dateEditBirthday.EditValue = null;
                memoEditAddress.EditValue = null;
                textEditProvince.EditValue = null;
            }

            private void editOrder()
            {
                gridLookUpEditPatient.Properties.Buttons[0].Visible = false;
            }


            private void clearData()
            {
                clearErrorText();

                StoreDataSet.OrderBindingSource.CancelEdit();
                StoreDataSet.Instance.Order.RejectChanges();
            }

            private void clearErrorText()
            {
                gridLookUpEditPatient.ErrorText = "";
                textEditNumber.ErrorText = "";
                dateEditGetOrderDate.ErrorText = "";
            }

            private void OrderInUpForm_Shown(object sender, EventArgs e)
            {
                textEditNumber.Focus();
            }

        #endregion

        #region Формын event

            private void gridLookUpEditPatient_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add_patient"))
                {
                    PatientInUpForm.Instance.showForm(3, OrderForm.Instance);
                }
            }

            private void gridLookUpEditPatient_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditPatient.EditValue != null && gridLookUpEditPatient.EditValue != DBNull.Value && !gridLookUpEditPatient.Text.Equals(""))
                {
                    currentPatientView = (DataRowView)gridLookUpEditPatient.GetSelectedDataRow();
                    textEditSirName.EditValue = currentPatientView["sirName"];
                    textEditLastName.EditValue = currentPatientView["lastName"];
                    textEditFirstName.EditValue = currentPatientView["firstName"];
                    textEditRegister.EditValue = currentPatientView["id"];
                    textEditPhone.EditValue = currentPatientView["phone"];
                    radioGroupGender.EditValue = currentPatientView["gender"];
                    dateEditBirthday.EditValue = currentPatientView["birthday"];
                    memoEditAddress.EditValue = currentPatientView["address"];
                    textEditProvince.EditValue = currentPatientView["provinceName"];
                }
            }

            
            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonSaveOrder_Click(object sender, EventArgs e)
            {
                saveOrder();
            }

            private void simpleButtonCheck_Click(object sender, EventArgs e)
            {
                checkPatient();
            }

        #endregion

        #region Формын функцууд

            private void reload()
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                StoreDataSet.PatientOrderTableAdapter.Fill(StoreDataSet.Instance.PatientOrder, HospitalUtil.getHospitalId());
                //ProgramUtil.closeWaitDialog();
            }

            private bool checkOrder()
            {
                bool isRight = true;
                Instance.Validate();

                if (textEditNumber.EditValue == DBNull.Value || (textEditNumber.EditValue = textEditNumber.EditValue.ToString().Trim()).Equals(""))
                {
                    textEditNumber.ErrorText = "Дугаарыг оруулна уу";
                    isRight = false;
                    textEditNumber.Focus();
                }
                if (dateEditGetOrderDate.EditValue == DBNull.Value || dateEditBirthday.EditValue == null || dateEditBirthday.Text == "")
                {
                    dateEditGetOrderDate.ErrorText = "Огноог оруулна уу";
                    if (isRight)
                    {
                        isRight = false;
                        dateEditGetOrderDate.Focus();
                    }
                }
                if (gridLookUpEditPatient.Properties.Buttons[0].Enabled)
                {
                    if (gridLookUpEditPatient.EditValue == DBNull.Value || gridLookUpEditPatient.EditValue == null || gridLookUpEditPatient.Text.Equals(""))
                    {
                        gridLookUpEditPatient.ErrorText = "Эмчлүүлэгчийг сонгоно уу";
                        if (isRight)
                        {
                            isRight = false;
                            gridLookUpEditPatient.Focus();
                        }
                    }
                    if (isRight)
                        isRight = checkPatient();
                }

                return isRight;
            }

            private bool checkPatient()
            {
                bool isRight = true;

                object ret = UserDataSet.QueriesTableAdapter.exist_order_patient(gridLookUpEditPatient.EditValue.ToString());
                if (ret != DBNull.Value && ret != null && actionType != 2)
                {
                    isRight = false;
                    gridLookUpEditPatient.ErrorText = ret.ToString();
                }

                return isRight;
            }

            private void saveOrder()
            {
                if (checkOrder())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    if (actionType == 1)
                    {
                        OrderForm.Instance.currentOrderView["id"] = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    }

                    OrderForm.Instance.currentOrderView["sirName"] = textEditSirName.Text;
                    OrderForm.Instance.currentOrderView["lastName"] = textEditLastName.Text;
                    OrderForm.Instance.currentOrderView["firstName"] = textEditFirstName.Text;
                    OrderForm.Instance.currentOrderView["birthday"] = dateEditBirthday.Text;
                    OrderForm.Instance.currentOrderView["address"] = memoEditAddress.Text;
                    OrderForm.Instance.currentOrderView["gender"] = radioGroupGender.EditValue;
                    OrderForm.Instance.currentOrderView["phone"] = textEditPhone.Text;
                    OrderForm.Instance.currentOrderView["provinceName"] = textEditProvince.Text;

                    OrderForm.Instance.currentOrderView["isMarried"] = currentPatientView["isMarried"];
                    OrderForm.Instance.currentOrderView["emdNumber"] = currentPatientView["emdNumber"];
                    OrderForm.Instance.currentOrderView["education"] = currentPatientView["education"];
                    OrderForm.Instance.currentOrderView["positionName"] = currentPatientView["positionName"];
                    OrderForm.Instance.currentOrderView["organizationName"] = currentPatientView["organizationName"];
                    OrderForm.Instance.currentOrderView["specialtyName"] = currentPatientView["specialtyName"];
                    OrderForm.Instance.currentOrderView["orderState"] = 0;

                    StoreDataSet.OrderBindingSource.EndEdit();
                    StoreDataSet.OrderTableAdapter.Update(StoreDataSet.Instance.Order);

                    OrderForm.Instance.refreshData();
                    ProgramUtil.closeWaitDialog();
                    Close();

                    if (radioGroupOrderState.SelectedIndex == 1)
                    {
                        currID = OrderForm.Instance.currentOrderView["id"].ToString();
                        StoreForm.Instance.showForm(actionType);
                    }
                }
            }

            // Шинээр нэмсэн эмчлүүлэгчийг авч байна
            public void addPatient(DataRowView view)
            {
                DataRowView row = (DataRowView)StoreDataSet.PatientOrderBindingSource.AddNew();
                row["id"] = view["id"];
                row["sirName"] = view["sirName"];
                row["lastName"] = view["lastName"];
                row["firstName"] = view["firstName"];
                row["birthday"] = view["birthday"];
                row["gender"] = view["gender"];
                row["isMarried"] = view["isMarried"];
                row["phone"] = view["phone"];
                row["address"] = view["address"];
                row["emdNumber"] = view["emdNumber"];
                row["education"] = view["education"];
                row["organizationName"] = view["organizationName"];
                row["positionName"] = view["positionName"];
                row["provinceName"] = view["provinceName"];
                row["specialtyName"] = view["specialtyName"];
                row.EndEdit();

                gridLookUpEditPatient.EditValue = view["id"];
            }

        #endregion

    }
}