﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.store.store;
using Pharmacy2016.gui.report.store.order;

namespace Pharmacy2016.gui.core.store.order
{
    /*
     * Хүлээлгийн жагсаалтыг харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class OrderForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static OrderForm INSTANCE = new OrderForm();

            public static OrderForm Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 1001;

            private bool isInit = false;

            public DataRowView currentOrderView;

            private System.IO.Stream layout_order = null;

            private OrderForm()
            {
            
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initLayout();
                RoleUtil.addForm(FORM_ID, this);

                treeListWard.DataSource = StoreDataSet.OrderWardRoomBedBindingSource;
                gridControlRoom.DataSource = StoreDataSet.OrderRoomBedWardBindingSource;
                gridControlOrder.DataSource = StoreDataSet.OrderBindingSource;

                dateEditFilterStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditFilterStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditFilterEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditFilterEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                dateEditFilterStart.ErrorIconAlignment = ErrorIconAlignment.MiddleLeft;
                DateTime date = DateTime.Now;
                dateEditFilterStart.DateTime = new DateTime(date.Year, date.Month, 1, 00, 00, 01);
                dateEditFilterEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month), 23, 59, 59);
            }

            private void initLayout()
            {
                layout_order = new System.IO.MemoryStream();
                gridViewOrder.SaveLayoutToStream(layout_order);
                layout_order.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        comboBoxEditOrderState.SelectedIndex = 3;
                        checkRole();
                      
                        StoreDataSet.Instance.Order.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.Order.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        reload();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[4].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                //gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[5].Visible = roles[1];
                dropDownButtonPrint.Visible = roles[4];
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;

                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("order");
                if (layout != null)
                {
                    gridViewOrder.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewOrder.RestoreLayoutFromStream(layout_order);
                    layout_order.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }
            
            private void OrderForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                StoreDataSet.Instance.Order.Clear();
            }

        #endregion

        #region Формын event

            private void simpleButtonReloadOrder_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                saveLayout();
            }

            private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addOrder();
                    }
                    else if (String.Equals(e.Button.Tag, "edit") && gridViewOrder.GetFocusedRow() != null)
                    {
                        editOrder();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewOrder.GetFocusedRow() != null)
                    {
                        try
                        {
                            deleteOrder();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "add_store") && gridViewOrder.GetFocusedRow() != null)
                    {
                        addStore();
                    }
                    else if (String.Equals(e.Button.Tag, "cancel_order") && gridViewOrder.GetFocusedRow() != null)
                    {
                        try
                        {
                            cancelOrder();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Дугаар цуцлахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "not_cancel_order") && gridViewOrder.GetFocusedRow() != null)
                    {
                        try
                        {
                            notCancelOrder();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Дугаар цуцлахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                }
            }

            private void comboBoxEditOrderState_SelectedIndexChanged(object sender, EventArgs e)
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                if (comboBoxEditOrderState.SelectedIndex == 0)
                {
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = roles[1];
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = roles[3];
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = roles[2];
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = roles[1];
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[4].Visible = roles[1];
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[5].Visible = false;

                    gridViewOrder.ActiveFilterString = "orderStateText = '" + comboBoxEditOrderState.Text + "'";
                }
                else if (comboBoxEditOrderState.SelectedIndex == 1)
                {
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[4].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[5].Visible = false;

                    gridViewOrder.ActiveFilterString = "orderStateText = '" + comboBoxEditOrderState.Text + "'";
                }
                else if (comboBoxEditOrderState.SelectedIndex == 2)
                {
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = roles[3];
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[4].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[5].Visible = roles[1];

                    gridViewOrder.ActiveFilterString = "orderStateText = '" + comboBoxEditOrderState.Text + "'";
                }
                else if (comboBoxEditOrderState.SelectedIndex == 3)
                {
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[4].Visible = false;
                    gridControlOrder.EmbeddedNavigator.Buttons.CustomButtons[5].Visible = false;
                    gridViewOrder.ActiveFilterString = "";
                }
            }

        #endregion
          
        #region Формын функц

            private void reload()
            {
                if (dateEditFilterStart.DateTime > dateEditFilterEnd.DateTime)
                {
                    dateEditFilterStart.ErrorText = "Эхлэх огноо дуусах огнооноос хойно байж болохгүй";
                    return;
                }

                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                if (UserUtil.getUserRoleId() == RoleUtil.RECEPTION_ID)
                {
                    StoreDataSet.OrderTableAdapter.Fill(StoreDataSet.Instance.Order, HospitalUtil.getHospitalId(), dateEditFilterStart.Text, dateEditFilterEnd.Text);
                    comboBoxEditOrderState.SelectedIndex = 3;
                    //gridViewOrder.ActiveFilterString = "orderState = '" + comboBoxEditOrderState.SelectedIndex + "'";
                }
                else
                {
                    StoreDataSet.OrderTableAdapter.Fill(StoreDataSet.Instance.Order, HospitalUtil.getHospitalId(), dateEditFilterStart.Text, dateEditFilterEnd.Text);
                    comboBoxEditOrderState.SelectedIndex = 3;
                    //gridViewOrder.ActiveFilterString = "orderState = '" + comboBoxEditOrderState.SelectedIndex + "'";
                }
                StoreDataSet.OrderWardRoomBedTableAdapter.Fill(StoreDataSet.Instance.OrderWardRoomBed, HospitalUtil.getHospitalId());
                StoreDataSet.OrderRoomBedWardTableAdapter.Fill(StoreDataSet.Instance.OrderRoomBedWard, HospitalUtil.getHospitalId());
                treeListWard.CollapseAll();
                ProgramUtil.closeWaitDialog();
            }

            private void addOrder() 
            {
                currentOrderView = (DataRowView)StoreDataSet.OrderBindingSource.AddNew();
                OrderInUpForm.Instance.showForm(1);
            }

            private void editOrder()
            {
                currentOrderView = (DataRowView)StoreDataSet.OrderBindingSource.Current;
                OrderInUpForm.Instance.showForm(2);
            }

            private void deleteOrder()
            {
                object ret = UserDataSet.QueriesTableAdapter.can_delete_order(HospitalUtil.getHospitalId(), gridViewOrder.GetFocusedDataRow()["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Дугаар олголтыг устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    gridViewOrder.DeleteSelectedRows();
                    StoreDataSet.OrderBindingSource.EndEdit();
                    StoreDataSet.OrderTableAdapter.Update(StoreDataSet.Instance.Order);
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void cancelOrder() 
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ дугаарыг цуцлах уу", "Дугаар олголтыг цуцлах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    currentOrderView = (DataRowView)StoreDataSet.OrderBindingSource.Current;
                    currentOrderView["orderState"] = 2;
                    currentOrderView.EndEdit();
                    StoreDataSet.OrderTableAdapter.Update(StoreDataSet.Instance.Order);
                    gridViewOrder.RefreshData();
                }
            }
        
            private void notCancelOrder()
            {
                currentOrderView = (DataRowView)StoreDataSet.OrderBindingSource.Current;
                object ret = UserDataSet.QueriesTableAdapter.exist_order_patient(currentOrderView["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    XtraMessageBox.Show(ret.ToString());
                    return;
                }

                DialogResult dialogResult = XtraMessageBox.Show("Энэ дугаарын сэргээх үү", "Дугаар олголтыг сэргээх", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    currentOrderView["orderState"] = 0;
                    currentOrderView.EndEdit();
                    StoreDataSet.OrderTableAdapter.Update(StoreDataSet.Instance.Order);
                    gridViewOrder.RefreshData();
                }
            }

            private void addStore() 
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ дугаарын хэвтүүлэх үү", "Дугаар олголтыг хэвтүүлэх", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    currentOrderView = (DataRowView)StoreDataSet.OrderBindingSource.Current;
                    OrderInUpForm.Instance.currID = currentOrderView["id"].ToString();
                    StoreForm.Instance.showForm(3);
                }
            }

            private void saveLayout()
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    gridViewOrder.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    System.IO.StreamReader reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("order", reader.ReadToEnd());
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            
            public void refreshData()
            {
                int i = gridViewOrder.FocusedRowHandle;
                gridViewOrder.RefreshData();
                gridViewOrder.FocusedRowHandle = i;
            }

            public DateTime getStartDate()
            {
                return dateEditFilterStart.DateTime;
            }

            public DateTime getEndDate()
            {
                return dateEditFilterEnd.DateTime;
            }

        #endregion
        
        #region Хэвлэх, хөрвүүлэх функц

            private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemPrint.Caption;
                dropDownButtonPrint.Image = barButtonItemPrint.Glyph;
                
                OrderReport.Instance.initAndShow(dateEditFilterStart.Text, dateEditFilterEnd.Text,gridViewOrder.ActiveFilterString);
            }

            private void barButtonItemBegBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemAll.Caption;
                dropDownButtonPrint.Image = barButtonItemAll.Glyph;
            }

            private void barButtonItemEndBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemEndBal.Caption;
                dropDownButtonPrint.Image = barButtonItemEndBal.Glyph;
            }

            private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemConvert.Caption;
                dropDownButtonPrint.Image = barButtonItemConvert.Glyph;
                ProgramUtil.convertGrid(gridViewOrder, "Хүлээлгийн жагсаалт");
            }

        #endregion

    }
}