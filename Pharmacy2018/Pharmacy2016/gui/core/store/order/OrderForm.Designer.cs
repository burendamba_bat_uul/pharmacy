﻿namespace Pharmacy2016.gui.core.store.order
{
    partial class OrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderForm));
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageBed = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlRoom = new DevExpress.XtraGrid.GridControl();
            this.gridViewRoom = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnRoom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnBed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnWard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageWard = new DevExpress.XtraTab.XtraTabPage();
            this.treeListWard = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPatientPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPatientAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPatientBirthday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumnPatientRegister = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPatientFirstName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPatientLastName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOrderDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewOrder = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnOrderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnProvince = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPatientGender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOrderState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlOrder = new DevExpress.XtraGrid.GridControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.dateEditFilterEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFilterStart = new DevExpress.XtraEditors.DateEdit();
            this.simpleButtonReloadOrder = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAll = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEndBal = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.comboBoxEditOrderState = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPageBed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRoom)).BeginInit();
            this.xtraTabPageWard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFilterEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFilterEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFilterStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFilterStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditOrderState.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanel
            // 
            this.dockPanel.Controls.Add(this.dockPanel1_Container);
            this.dockPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel.ID = new System.Guid("d3dcdec7-947d-4d70-8625-132bdfbdea1b");
            this.dockPanel.Location = new System.Drawing.Point(0, 0);
            this.dockPanel.Name = "dockPanel";
            this.dockPanel.Options.ShowCloseButton = false;
            this.dockPanel.Options.ShowMaximizeButton = false;
            this.dockPanel.OriginalSize = new System.Drawing.Size(250, 200);
            this.dockPanel.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel.SavedIndex = 0;
            this.dockPanel.Size = new System.Drawing.Size(250, 461);
            this.dockPanel.Text = "Сул ор";
            this.dockPanel.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.xtraTabControl);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(242, 434);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPageBed;
            this.xtraTabControl.Size = new System.Drawing.Size(242, 434);
            this.xtraTabControl.TabIndex = 0;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageBed,
            this.xtraTabPageWard});
            // 
            // xtraTabPageBed
            // 
            this.xtraTabPageBed.Controls.Add(this.gridControlRoom);
            this.xtraTabPageBed.Name = "xtraTabPageBed";
            this.xtraTabPageBed.Size = new System.Drawing.Size(236, 406);
            this.xtraTabPageBed.Text = "Өрөө, ор";
            // 
            // gridControlRoom
            // 
            this.gridControlRoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRoom.Location = new System.Drawing.Point(0, 0);
            this.gridControlRoom.MainView = this.gridViewRoom;
            this.gridControlRoom.Name = "gridControlRoom";
            this.gridControlRoom.Size = new System.Drawing.Size(236, 406);
            this.gridControlRoom.TabIndex = 0;
            this.gridControlRoom.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRoom});
            // 
            // gridViewRoom
            // 
            this.gridViewRoom.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnRoom,
            this.gridColumnBed,
            this.gridColumnWard});
            this.gridViewRoom.GridControl = this.gridControlRoom;
            this.gridViewRoom.Name = "gridViewRoom";
            this.gridViewRoom.OptionsBehavior.Editable = false;
            this.gridViewRoom.OptionsBehavior.ReadOnly = true;
            this.gridViewRoom.OptionsCustomization.AllowGroup = false;
            this.gridViewRoom.OptionsView.ShowAutoFilterRow = true;
            this.gridViewRoom.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnRoom
            // 
            this.gridColumnRoom.Caption = "Өрөө";
            this.gridColumnRoom.CustomizationCaption = "Өрөө";
            this.gridColumnRoom.FieldName = "roomName";
            this.gridColumnRoom.Name = "gridColumnRoom";
            this.gridColumnRoom.Visible = true;
            this.gridColumnRoom.VisibleIndex = 0;
            this.gridColumnRoom.Width = 65;
            // 
            // gridColumnBed
            // 
            this.gridColumnBed.Caption = "Ор";
            this.gridColumnBed.CustomizationCaption = "Ор";
            this.gridColumnBed.FieldName = "bedName";
            this.gridColumnBed.Name = "gridColumnBed";
            this.gridColumnBed.Visible = true;
            this.gridColumnBed.VisibleIndex = 1;
            this.gridColumnBed.Width = 60;
            // 
            // gridColumnWard
            // 
            this.gridColumnWard.Caption = "Тасаг";
            this.gridColumnWard.CustomizationCaption = "Тасаг";
            this.gridColumnWard.FieldName = "wardName";
            this.gridColumnWard.Name = "gridColumnWard";
            this.gridColumnWard.Visible = true;
            this.gridColumnWard.VisibleIndex = 2;
            this.gridColumnWard.Width = 93;
            // 
            // xtraTabPageWard
            // 
            this.xtraTabPageWard.Controls.Add(this.treeListWard);
            this.xtraTabPageWard.Name = "xtraTabPageWard";
            this.xtraTabPageWard.Size = new System.Drawing.Size(236, 406);
            this.xtraTabPageWard.Text = "Тасаг";
            // 
            // treeListWard
            // 
            this.treeListWard.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnName});
            this.treeListWard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListWard.KeyFieldName = "id";
            this.treeListWard.Location = new System.Drawing.Point(0, 0);
            this.treeListWard.Name = "treeListWard";
            this.treeListWard.OptionsBehavior.ReadOnly = true;
            this.treeListWard.ParentFieldName = "pid";
            this.treeListWard.Size = new System.Drawing.Size(236, 406);
            this.treeListWard.TabIndex = 0;
            // 
            // treeListColumnName
            // 
            this.treeListColumnName.Caption = "Нэр";
            this.treeListColumnName.FieldName = "name";
            this.treeListColumnName.Name = "treeListColumnName";
            this.treeListColumnName.Visible = true;
            this.treeListColumnName.VisibleIndex = 0;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Үйлдэл";
            this.gridColumn9.Name = "gridColumn9";
            // 
            // gridColumnPatientPhone
            // 
            this.gridColumnPatientPhone.Caption = "Утас";
            this.gridColumnPatientPhone.FieldName = "phone";
            this.gridColumnPatientPhone.Name = "gridColumnPatientPhone";
            this.gridColumnPatientPhone.OptionsColumn.ReadOnly = true;
            this.gridColumnPatientPhone.Visible = true;
            this.gridColumnPatientPhone.VisibleIndex = 10;
            this.gridColumnPatientPhone.Width = 55;
            // 
            // gridColumnPatientAddress
            // 
            this.gridColumnPatientAddress.Caption = "Хаяг";
            this.gridColumnPatientAddress.FieldName = "address";
            this.gridColumnPatientAddress.Name = "gridColumnPatientAddress";
            this.gridColumnPatientAddress.OptionsColumn.ReadOnly = true;
            this.gridColumnPatientAddress.Visible = true;
            this.gridColumnPatientAddress.VisibleIndex = 9;
            this.gridColumnPatientAddress.Width = 55;
            // 
            // gridColumnPatientBirthday
            // 
            this.gridColumnPatientBirthday.Caption = "Төрсөн огноо";
            this.gridColumnPatientBirthday.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.gridColumnPatientBirthday.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumnPatientBirthday.FieldName = "birthday";
            this.gridColumnPatientBirthday.Name = "gridColumnPatientBirthday";
            this.gridColumnPatientBirthday.OptionsColumn.ReadOnly = true;
            this.gridColumnPatientBirthday.Visible = true;
            this.gridColumnPatientBirthday.VisibleIndex = 7;
            this.gridColumnPatientBirthday.Width = 55;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridColumnPatientRegister
            // 
            this.gridColumnPatientRegister.Caption = "Регистер";
            this.gridColumnPatientRegister.FieldName = "userID";
            this.gridColumnPatientRegister.Name = "gridColumnPatientRegister";
            this.gridColumnPatientRegister.OptionsColumn.ReadOnly = true;
            this.gridColumnPatientRegister.Visible = true;
            this.gridColumnPatientRegister.VisibleIndex = 5;
            this.gridColumnPatientRegister.Width = 54;
            // 
            // gridColumnPatientFirstName
            // 
            this.gridColumnPatientFirstName.Caption = "Нэр";
            this.gridColumnPatientFirstName.FieldName = "firstName";
            this.gridColumnPatientFirstName.Name = "gridColumnPatientFirstName";
            this.gridColumnPatientFirstName.OptionsColumn.ReadOnly = true;
            this.gridColumnPatientFirstName.Visible = true;
            this.gridColumnPatientFirstName.VisibleIndex = 4;
            this.gridColumnPatientFirstName.Width = 54;
            // 
            // gridColumnPatientLastName
            // 
            this.gridColumnPatientLastName.Caption = "Овог";
            this.gridColumnPatientLastName.FieldName = "lastName";
            this.gridColumnPatientLastName.Name = "gridColumnPatientLastName";
            this.gridColumnPatientLastName.OptionsColumn.ReadOnly = true;
            this.gridColumnPatientLastName.Visible = true;
            this.gridColumnPatientLastName.VisibleIndex = 3;
            this.gridColumnPatientLastName.Width = 54;
            // 
            // gridColumnOrderDate
            // 
            this.gridColumnOrderDate.Caption = "Огноо";
            this.gridColumnOrderDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnOrderDate.FieldName = "date";
            this.gridColumnOrderDate.MinWidth = 80;
            this.gridColumnOrderDate.Name = "gridColumnOrderDate";
            this.gridColumnOrderDate.OptionsColumn.ReadOnly = true;
            this.gridColumnOrderDate.Visible = true;
            this.gridColumnOrderDate.VisibleIndex = 1;
            this.gridColumnOrderDate.Width = 80;
            // 
            // gridColumnOrderNumber
            // 
            this.gridColumnOrderNumber.Caption = "Дугаар";
            this.gridColumnOrderNumber.FieldName = "number";
            this.gridColumnOrderNumber.Name = "gridColumnOrderNumber";
            this.gridColumnOrderNumber.OptionsColumn.ReadOnly = true;
            this.gridColumnOrderNumber.Visible = true;
            this.gridColumnOrderNumber.VisibleIndex = 0;
            this.gridColumnOrderNumber.Width = 54;
            // 
            // gridViewOrder
            // 
            this.gridViewOrder.ActiveFilterEnabled = false;
            this.gridViewOrder.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnOrderNumber,
            this.gridColumnOrderDate,
            this.gridColumnPatientLastName,
            this.gridColumnPatientFirstName,
            this.gridColumnPatientRegister,
            this.gridColumnPatientBirthday,
            this.gridColumnPatientAddress,
            this.gridColumnPatientPhone,
            this.gridColumnOrderDescription,
            this.gridColumnProvince,
            this.gridColumnPatientGender,
            this.gridColumnOrderState});
            this.gridViewOrder.GridControl = this.gridControlOrder;
            this.gridViewOrder.Name = "gridViewOrder";
            this.gridViewOrder.OptionsBehavior.ReadOnly = true;
            this.gridViewOrder.OptionsCustomization.AllowGroup = false;
            this.gridViewOrder.OptionsFind.AllowFindPanel = false;
            this.gridViewOrder.OptionsView.ShowAutoFilterRow = true;
            this.gridViewOrder.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnOrderDescription
            // 
            this.gridColumnOrderDescription.Caption = "Тайлбар";
            this.gridColumnOrderDescription.FieldName = "description";
            this.gridColumnOrderDescription.Name = "gridColumnOrderDescription";
            this.gridColumnOrderDescription.OptionsColumn.ReadOnly = true;
            this.gridColumnOrderDescription.Visible = true;
            this.gridColumnOrderDescription.VisibleIndex = 11;
            this.gridColumnOrderDescription.Width = 60;
            // 
            // gridColumnProvince
            // 
            this.gridColumnProvince.Caption = "Байршил";
            this.gridColumnProvince.FieldName = "provinceName";
            this.gridColumnProvince.Name = "gridColumnProvince";
            this.gridColumnProvince.OptionsColumn.ReadOnly = true;
            this.gridColumnProvince.Visible = true;
            this.gridColumnProvince.VisibleIndex = 8;
            this.gridColumnProvince.Width = 55;
            // 
            // gridColumnPatientGender
            // 
            this.gridColumnPatientGender.Caption = "Хүйс";
            this.gridColumnPatientGender.FieldName = "genderText";
            this.gridColumnPatientGender.Name = "gridColumnPatientGender";
            this.gridColumnPatientGender.Visible = true;
            this.gridColumnPatientGender.VisibleIndex = 6;
            this.gridColumnPatientGender.Width = 45;
            // 
            // gridColumnOrderState
            // 
            this.gridColumnOrderState.Caption = "Төлөв";
            this.gridColumnOrderState.CustomizationCaption = "Төлөв";
            this.gridColumnOrderState.FieldName = "orderStateText";
            this.gridColumnOrderState.Name = "gridColumnOrderState";
            this.gridColumnOrderState.Visible = true;
            this.gridColumnOrderState.VisibleIndex = 2;
            // 
            // gridControlOrder
            // 
            this.gridControlOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlOrder.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlOrder.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlOrder.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlOrder.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlOrder.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlOrder.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 15, true, true, "Засах", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 11, true, true, "Хэвтүүлэх", "add_store"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Цуцлах", "cancel_order"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "", "not_cancel_order")});
            this.gridControlOrder.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControlOrder.Location = new System.Drawing.Point(2, 2);
            this.gridControlOrder.MainView = this.gridViewOrder;
            this.gridControlOrder.Name = "gridControlOrder";
            this.gridControlOrder.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridControlOrder.Size = new System.Drawing.Size(880, 409);
            this.gridControlOrder.TabIndex = 1;
            this.gridControlOrder.UseEmbeddedNavigator = true;
            this.gridControlOrder.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOrder});
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControlOrder);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 48);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(884, 413);
            this.panelControl2.TabIndex = 6;
            // 
            // dateEditFilterEnd
            // 
            this.dateEditFilterEnd.EditValue = null;
            this.dateEditFilterEnd.Location = new System.Drawing.Point(144, 14);
            this.dateEditFilterEnd.Name = "dateEditFilterEnd";
            this.dateEditFilterEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFilterEnd.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditFilterEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFilterEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditFilterEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditFilterEnd.Properties.EditFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditFilterEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditFilterEnd.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.dateEditFilterEnd.Size = new System.Drawing.Size(130, 20);
            this.dateEditFilterEnd.TabIndex = 7;
            this.dateEditFilterEnd.ToolTip = "Дуусах огноо";
            // 
            // dateEditFilterStart
            // 
            this.dateEditFilterStart.EditValue = null;
            this.dateEditFilterStart.Location = new System.Drawing.Point(8, 14);
            this.dateEditFilterStart.Name = "dateEditFilterStart";
            this.dateEditFilterStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFilterStart.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditFilterStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFilterStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditFilterStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditFilterStart.Properties.EditFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditFilterStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditFilterStart.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.dateEditFilterStart.Size = new System.Drawing.Size(130, 20);
            this.dateEditFilterStart.TabIndex = 6;
            this.dateEditFilterStart.ToolTip = "Эхлэх огноо";
            // 
            // simpleButtonReloadOrder
            // 
            this.simpleButtonReloadOrder.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReloadOrder.Image")));
            this.simpleButtonReloadOrder.Location = new System.Drawing.Point(280, 12);
            this.simpleButtonReloadOrder.Name = "simpleButtonReloadOrder";
            this.simpleButtonReloadOrder.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonReloadOrder.TabIndex = 5;
            this.simpleButtonReloadOrder.Text = "Шүүх";
            this.simpleButtonReloadOrder.ToolTip = "Хүлээлгийн жагсаалт шүүх";
            this.simpleButtonReloadOrder.Click += new System.EventHandler(this.simpleButtonReloadOrder_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonLayout);
            this.panelControl1.Controls.Add(this.dropDownButtonPrint);
            this.panelControl1.Controls.Add(this.comboBoxEditOrderState);
            this.panelControl1.Controls.Add(this.dateEditFilterEnd);
            this.panelControl1.Controls.Add(this.dateEditFilterStart);
            this.panelControl1.Controls.Add(this.simpleButtonReloadOrder);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(884, 48);
            this.panelControl1.TabIndex = 5;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(849, 12);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 10;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.DropDownControl = this.popupMenu;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(713, 12);
            this.dropDownButtonPrint.MenuManager = this.barManager;
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.barManager.SetPopupContextMenu(this.dropDownButtonPrint, this.popupMenu);
            this.dropDownButtonPrint.Size = new System.Drawing.Size(130, 23);
            this.dropDownButtonPrint.TabIndex = 9;
            this.dropDownButtonPrint.Text = "Хэвлэх";
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAll),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemEndBal),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "Хэвлэх";
            this.barButtonItemPrint.Description = "Хүлээлгийн жагсаалт хэвлэх";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 0;
            this.barButtonItemPrint.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.LargeGlyph")));
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemAll
            // 
            this.barButtonItemAll.Caption = "Жагсаалт хэвлэх";
            this.barButtonItemAll.Description = "Жагсаалт хэвлэх";
            this.barButtonItemAll.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAll.Glyph")));
            this.barButtonItemAll.Id = 1;
            this.barButtonItemAll.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAll.LargeGlyph")));
            this.barButtonItemAll.Name = "barButtonItemAll";
            this.barButtonItemAll.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemBegBal_ItemClick);
            // 
            // barButtonItemEndBal
            // 
            this.barButtonItemEndBal.Caption = "Эцсийн үлдэгдэл";
            this.barButtonItemEndBal.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.Glyph")));
            this.barButtonItemEndBal.Id = 2;
            this.barButtonItemEndBal.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.LargeGlyph")));
            this.barButtonItemEndBal.Name = "barButtonItemEndBal";
            this.barButtonItemEndBal.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemEndBal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEndBal_ItemClick);
            // 
            // barButtonItemConvert
            // 
            this.barButtonItemConvert.Caption = "Хөрвүүлэх";
            this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
            this.barButtonItemConvert.Id = 3;
            this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
            this.barButtonItemConvert.Name = "barButtonItemConvert";
            this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemPrint,
            this.barButtonItemAll,
            this.barButtonItemEndBal,
            this.barButtonItemConvert});
            this.barManager.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(884, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 461);
            this.barDockControlBottom.Size = new System.Drawing.Size(884, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 461);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(884, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 461);
            // 
            // comboBoxEditOrderState
            // 
            this.comboBoxEditOrderState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxEditOrderState.EditValue = "Бүгдийг сонгох";
            this.comboBoxEditOrderState.Location = new System.Drawing.Point(587, 14);
            this.comboBoxEditOrderState.Name = "comboBoxEditOrderState";
            this.comboBoxEditOrderState.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditOrderState.Properties.Items.AddRange(new object[] {
            "Хүлээлгэсэн",
            "Хэвтүүлсэн",
            "Цуцалсан",
            "Бүгдийг сонгох"});
            this.comboBoxEditOrderState.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditOrderState.Size = new System.Drawing.Size(120, 20);
            this.comboBoxEditOrderState.TabIndex = 8;
            this.comboBoxEditOrderState.ToolTip = "Хүлээлгийн төрөл";
            this.comboBoxEditOrderState.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditOrderState_SelectedIndexChanged);
            // 
            // OrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "OrderForm";
            this.ShowInTaskbar = false;
            this.Text = "Хүлээлгийн жагсаалт";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OrderForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPageBed.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRoom)).EndInit();
            this.xtraTabPageWard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListWard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFilterEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFilterEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFilterStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFilterStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditOrderState.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewOrder;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOrderDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPatientLastName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPatientFirstName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPatientRegister;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPatientBirthday;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPatientAddress;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPatientPhone;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOrderDescription;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFilterEnd;
        private DevExpress.XtraEditors.DateEdit dateEditFilterStart;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReloadOrder;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraTreeList.TreeList treeListWard;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        public DevExpress.XtraGrid.GridControl gridControlOrder;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnProvince;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPatientGender;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnName;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditOrderState;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOrderState;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageBed;
        private DevExpress.XtraGrid.GridControl gridControlRoom;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRoom;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWard;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRoom;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBed;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnWard;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAll;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEndBal;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;


    }
}