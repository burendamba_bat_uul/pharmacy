﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.info.ds;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList;

namespace Pharmacy2016.gui.core.store.order
{
    /*
     * Тасаг, өрөөнд хэвтэн эмчлүүлж байгаа эмчлүүлэгчийг харах цонх.
     * 
     */

    public partial class RoomBedPatientForm : DevExpress.XtraEditors.XtraForm
    {
        
        #region Форм анх ачааллах функц

            private static RoomBedPatientForm INSTANCE = new RoomBedPatientForm();

            public static RoomBedPatientForm Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 1004;

            private bool isInit = false;

            public RoomBedPatientForm currentOrderView;

            private System.IO.Stream layout_room_bed = null;
            private System.IO.Stream layout_ward_room = null;

            private RoomBedPatientForm()
            {
            
            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                treeListLookUpEditWard.Properties.TreeList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(treeListWard_AfterCheckNode);

                initLayout();
                RoleUtil.addForm(FORM_ID, this);

                treeListLookUpEditWard.Properties.DataSource = UserDataSet.WardBindingSource;
                treeListWard.DataSource = UserDataSet.PatientWardRoomBedBindingSource;
                gridControlRoom.DataSource = UserDataSet.PatientRoomBedWardBindingSource;

                dateEditDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditDate.DateTime = DateTime.Now;
                reloadWard();
            }

            private void initLayout()
            {
                layout_room_bed = new System.IO.MemoryStream();
                gridViewRoom.SaveLayoutToStream(layout_room_bed);
                layout_room_bed.Seek(0, System.IO.SeekOrigin.Begin);

                layout_ward_room = new System.IO.MemoryStream();
                treeListWard.SaveLayoutToStream(layout_ward_room);
                layout_ward_room.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion
        
        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        chechRole();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void chechRole()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.NURSE_ID)
                {
                    UserDataSet.WardBindingSource.Filter = "id = '" + UserUtil.getUserWardID() + "'";
                }

                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("roomBedWard");
                if (layout != null)
                {
                    gridViewRoom.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewRoom.RestoreLayoutFromStream(layout_room_bed);
                    layout_room_bed.Seek(0, System.IO.SeekOrigin.Begin);
                }

                layout = UserUtil.getLayout("wardRoomBed");
                if (layout != null)
                {
                    treeListWard.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    treeListWard.RestoreLayoutFromStream(layout_ward_room);
                    layout_ward_room.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }
            
            private void RoomBedPatientForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                UserDataSet.WardBindingSource.Filter = "";
                UserDataSet.Instance.PatientWardRoomBed.Clear();
                UserDataSet.Instance.PatientRoomBedWard.Clear();
            }

        #endregion
        
        #region Формын event

            private void treeListLookUpEditWard_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
            {
                e.DisplayText = "Тасаг сонгох";
            }

            private void treeListLookUpEditWard_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "reload"))
                {
                    try
                    {
                        reloadWard();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }
            }

            private void treeListWard_AfterCheckNode(object sender, NodeEventArgs e)
            {
                if (e.Node.HasChildren)
                {
                    foreach (TreeListNode node in e.Node.Nodes)
                        node.Checked = e.Node.Checked;
                }
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void simpleButtonExport_Click(object sender, EventArgs e)
            {
                if (xtraTabControl.SelectedTabPage == xtraTabPageBed)
                {
                    ProgramUtil.convertGrid(gridViewRoom, "Өрөө, ор, тасгийн эмчлүүлэгч");
                }
                else if (xtraTabControl.SelectedTabPage == xtraTabPageWard)
                {
                    ProgramUtil.convertGrid(treeListWard, "Тасаг, өрөө, орны эмчлүүлэгч");
                }
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {

            }

        #endregion
          
        #region Формын функц

            private bool check() 
            {
                bool isRight = true;

                if (dateEditDate.EditValue == DBNull.Value || dateEditDate.EditValue == null || dateEditDate.Text.Trim().Equals(""))
                {
                    dateEditDate.ErrorText = "Огноог сонгоно уу";
                    isRight = false;
                    dateEditDate.Focus();
                }
                if (treeListLookUpEditWard.Properties.TreeList.GetAllCheckedNodes().Count == 0)
                {
                    treeListLookUpEditWard.ErrorText = "Тасгийг сонгоно уу";
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }

                return isRight;
            }

            private void reload()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    string wardID = "";
                    foreach(TreeListNode node in treeListLookUpEditWard.Properties.TreeList.GetAllCheckedNodes())
                        wardID += node.GetValue("id") + "~";
                    if (wardID.Length > 0)
                        wardID = wardID.Substring(0, wardID.Length - 1);

                    UserDataSet.PatientWardRoomBedTableAdapter.Fill(UserDataSet.Instance.PatientWardRoomBed, HospitalUtil.getHospitalId(), wardID, dateEditDate.Text);
                    UserDataSet.PatientRoomBedWardTableAdapter.Fill(UserDataSet.Instance.PatientRoomBedWard, HospitalUtil.getHospitalId(), wardID, dateEditDate.Text);
                    treeListWard.ExpandAll();

                    ProgramUtil.closeWaitDialog();
                }
            }

            private void reloadWard()
            {
                UserDataSet.WardTableAdapter.Fill(UserDataSet.Instance.Ward, HospitalUtil.getHospitalId());
                treeListLookUpEditWard.Properties.TreeList.ExpandAll();
            }

            private void saveLayout()
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    System.IO.StreamReader reader = null;
                    if (xtraTabControl.SelectedTabPage == xtraTabPageBed)
                    {
                        gridViewRoom.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        reader = new System.IO.StreamReader(str);
                        UserUtil.saveUserLayout("roomBedWard", reader.ReadToEnd());
                    }
                    else
                    {
                        treeListWard.SaveLayoutToStream(str);
                        str.Seek(0, System.IO.SeekOrigin.Begin);
                        reader = new System.IO.StreamReader(str);
                        UserUtil.saveUserLayout("wardRoomBed", reader.ReadToEnd());
                    }
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

        #endregion

    }
}