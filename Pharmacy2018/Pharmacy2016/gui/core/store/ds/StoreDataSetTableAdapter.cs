﻿using Pharmacy2016.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacy2016.gui.core.store.ds.StoreDataSetTableAdapters
{

    public partial class OrderTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreInspectionTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreTreatmentTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreDiagnoseTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreLeftTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreHistoryTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreGeneralinspectionTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreInternalinspectionTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreHeartTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreDigestiveTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreUrineTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreWardRoomTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class PharmacistConfUserTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class PatientStoreTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class PatientOrderTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class OrderWardRoomBedTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class OrderRoomBedWardTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class RoomBedTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreValidityTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

    public partial class StoreLeftDiagnoseTableAdapter
    {
        public int SelectCommandTimeout
        {
            get
            {
                if (this._commandCollection == null || this._commandCollection[0] == null)
                {
                    return 0;
                }
                else
                {
                    return (this._commandCollection[0].CommandTimeout);
                }
            }

            set
            {
                if (this._commandCollection != null)
                {
                    for (int i = 0; i < this._commandCollection.Length; i++)
                    {
                        if ((this._commandCollection[i] != null))
                        {
                            ((System.Data.SqlClient.SqlCommand)
                             (this._commandCollection[i])).CommandTimeout = value;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            InitCommandCollection();
            SelectCommandTimeout = SystemUtil.connectTimeOut;
            if (Adapter != null && Adapter.InsertCommand != null)
            {
                Adapter.InsertCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.UpdateCommand != null)
            {
                Adapter.UpdateCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            if (Adapter != null && Adapter.DeleteCommand != null)
            {
                Adapter.DeleteCommand.CommandTimeout = SystemUtil.connectTimeOut;
            }
            Connection.ConnectionString = global::Pharmacy2016.Properties.Settings.Default.conMedicine;
            ClearBeforeFill = true;
        }
    }

}
