﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
namespace Pharmacy2016.gui.core.store.ds
{


    public partial class StoreDataSet
    {

        #region DataSet-н обьект

            private static StoreDataSet INSTANCE = new StoreDataSet();

            public static StoreDataSet Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            public static readonly string[] paymentType = {
                                   "Төр хариуцна",
                                   "15%",
                                   "10%",
                                   "Өвчтөн хариуцна"
                                  };

            public static readonly string[] alergyType = {
                                    "Эмийн бодис",
                                    "Хоол хүнс",
                                    "Бусад"
                                  };

            public static readonly string[] diagnoseType = {
                                    "Үндсэн онош",
                                    "Дагалдах онош",
                                    "Үйлдлийн онош",
                                    "Уламжлалтын онош",
                                    "Хүндрэл",
                                    "Ялган оношлох эмгэгүүд ба хам шинжүүд",
                                    "Хийгдэх шинжилгээ",
                                    "Яаралтай хийгдэх эмчилгээ",
                                    "Эд эсийн шинжилгээний дүгнэлт"
                                  };
            public static readonly string[] leftDiagnose = {
                                    "Үндсэн онош",
                                    "Дагалдах онош",
                                    "Хүндрэл",
                                    "Үйлдлийн онош",
                                    "Уламжлалтын онош"
                                  };

            public static readonly string[] endHurt = {
                                    "Эдгэрсэн",
                                    "Сайжирсан",
                                    "Хэвэндээ",
                                    "Дордсон",
                                    "Нас барсан"
                                  };

            public static readonly string[] leaveHospital = {
                                    "Гарсан",
                                    "Шилжсэн",
                                    "Нас барсан"
                                  };

            public static readonly string[] inCome = {
                                    "Хөнгөн",
                                    "Дунд",
                                    "Хүндэвтэр",
                                    "Хүнд",
                                    "Маш хүнд"
                                    };
            public static readonly string[] inBack = {
                                    "Хөнгөн",
                                    "Дунд",
                                    "Хүндэвтэр",
                                    "Хүнд",
                                    "Маш хүнд"
                                    };

            public static readonly string[] surgery = {
                                    "Эмийн",
                                    "Эмийн бус",
                                    "Мэс заслын",
                                    "Бусад"                                
                                    };

            public static readonly string[] analysisTreat = {
                                    "Биохими",
                                    "Нян судлал",
                                    "Вирус дуран",
                                    "Эд, эсийн",
                                    "Иммунологи",
                                    "Бусад"
                                    };

            public static readonly string[] analysisLight = {
                                    "ЭКГ",
                                    "ЭХО",
                                    "Уян",
                                    "Компьютер томографи",
                                    "Соронзон томографи",
                                    "Хийгдээгүй"
                                };
            public static readonly string[] lifeTerm = {
                                    "Орон сууцанд",
                                    "Гэрт",
                                    "Бусад"
                                };
            public static readonly string[] workTerm = {
                                    "Ердийн",
                                    "Хүнд",
                                    "Хортой",
                                    "Бусад"
                                };
            public static readonly string[] infection = {
                                    "Улаан бурхан",
                                    "Салхин цэцэг",
                                    "Вируст хепатит A",
                                    "Вируст хепатит B",
                                    "Вируст хепатит C",
                                    "Сүрьеэ",
                                    "Гахайн хавдар",
                                    "Бусад"
                                };
            public static readonly string[] diet = {
                                    "Ердийн",
                                    "Цагаан",                                 
                                    "Бусад"
                                };
            public static readonly string[] mind = {
                                    "Саруул",
                                    "Бүдгэрсэн",                                 
                                    "Ступор",
                                    "Солор",
                                    "Кома",
                                    "Бусад"
                                };
            public static readonly string[] surround = {
                                    "Харьцаатай",
                                    "Харьцаагүй",                                 
                                    "Сул"
                                };
            public static readonly string[] position = {
                                    "Идэвхтэй",
                                    "Идэвхгүй",                                 
                                    "Албадмал",
                                    "Хагас суугаа",
                                    "Хэвтрийн"
                                };
            public static readonly string[] skin = {
                                    "Хэвийн",
                                    "Ихэссэн",                                 
                                    "Алдагдсан",
                                    "Багассан"
                                };
            public static readonly string[] generalPart = {
                                    "Нүүрэнд",
                                    "Зовхинд",                                 
                                    "Хэвлийд",
                                    "Шилбээр"
                                };
            public static readonly string[] size = {
                                    "Хэвийн",
                                    "Харахад томорсон",                                 
                                    "Тэмтрэлтээр томорсон"
                                };
            public static readonly string[] place = {
                                    "Хүзүүний",
                                    "Суганы",                                 
                                    "Цавины",
                                    "Бусад"
                                };
            public static readonly string[] move = {
                                    "Идэвхтэй",
                                    "Идэвхгүй",                                 
                                    "Хязгаарлагдмал"
                                };
            public static readonly string[] breathShape = {
                                    "Цээжний",
                                    "Хэвлийн",                                 
                                    "Холимог"
                                };
            public static readonly string[] soundVib = {
                                    "Хэвийн Суларсан",
                                    "Тодорхойлогдохгүй",                                 
                                    "Хүчтэй болсон"
                                };
            public static readonly string[] partPercussion = {
                                    "Бүдгэрсэн",
                                    "Тодорсон",                                 
                                    "Дүлий болсон"
                                };
            public static readonly string[] ifDesease = {
                                    "Цулцангийн суларсан",
                                    "Цулцангийн ширүүссэн",                                 
                                    "Гуурсан хоолойн эмгэг",
                                    "Стенозын",
                                    "Амьсгал сонсогдохгүй"
                                };
            public static readonly string[] ifNoisy = {
                                    "Нойтон хэржигнүүр",
                                    "Шажигнуур",                                 
                                    "Хуурай хэржигнүүр",
                                    "Гялтангийн шүрэлгэлцэх чимээ"
                                };
            public static readonly string[] bronkhofoni = {
                                    "Хэвийн",
                                    "Тодорсон",                                 
                                    "Суларсан"
                                };
            public static readonly string[] heartRisk = {
                                    "Зохисгүй хооллолт",
                                    "Хөдөлгөөний хомсдол",                                 
                                    "Стресс",
                                    "Таргалалт",
                                    "Тамхидалт",
                                    "Архины зохисгүй хэрэглээ",
                                    "Удамшил",
                                    "Артерийн гипертензи",
                                    "Гиперхолестеринеми",
                                    "Чихрийн шижин"
                                };
            public static readonly string[] strong = {
                                    "Дунд зэрэг",
                                    "Хүчтэй",                                 
                                    "Сул"
                                };
            public static readonly string[] voltage = {
                                    "Дунд зэрэг",
                                    "Их",                                 
                                    "Бага"
                                };
            public static readonly string[] direction = {
                                    "Дээд хил",
                                    "Баруун хил",                                 
                                    "Зүүн хил"
                                };
            public static readonly string[] firstSound = {
                                    "Тод",
                                    "Бүдгэвтэр(I, IV)",                                 
                                    "Бүдэг(I, IV)",
                                    "Чангарсан(I, IV)"
                                };
            public static readonly string[] secondSound = {
                                    "Тод",
                                    "Бүдэг(II, III, V)",                                 
                                    "Өргөгдсөн(II, III)"
                                };
            public static readonly string[] thirdSound = {
                                    "Сонсогдоно",
                                    "Сонсогдохгүй"
                                };
            public static readonly string[] romb = {
                                    "I цэг",
                                    "II цэг",
                                    "III цэг",
                                    "IV цэг",
                                    "V цэг"
                                };
            public static readonly string[] isPercussion = {
                                    "Бүдгэрсэн",
                                    "Тодорсон",
                                    "Дүлий болсон"
                                };

            public static readonly string[] isMoving = {
                                    "Хэвийн",
                                    "Ихэссэн",
                                    "Дүлий"
                                };
            public static readonly string[] liver = {
                                    "Мэдрэл сульдал",
                                    "Биж хам шинж",
                                    "Иммуни-үрэвслийн шинж",
                                    "Өвдөх хам шинж",
                                    "Шархлах хам шинж",
                                    "Загатналт",
                                    "Цусархаг хам шинж",
                                    "Элэгний их шинж",
                                    "Элэгний бага шинж"
                                };

            public static readonly string[] urineOutput = {
                                    "Хэвийн",
                                    "Ихэссэн",
                                    "Буурсан"
                                };

            public static readonly string[] urineColor = {
                                    "Сүрлэн шар",
                                    "Улаан шар",
                                    "Өнгөгүй",
                                    "Тундастай",
                                    "Тундасгүй"
                                };

            public static readonly string[] validity = {
                                    "Зовиураас",
                                    "Асуумжаас",
                                    "Бодит үзлэгээс",
                                    "Лабораторийн шинжилгээ",
                                    "Үйл оношийн шинжилгээ",
                                    "Үндсэн онош",
                                    "Дагалдах онош",
                                    "Хүндрэл"
                                };

            public static readonly string[] code = {
                                    "ӨОУА-10",
                                    "Код",
                                    "ҮОУА-9",
                                    "Хагалгааны код",
                                    "Морфологи код"
                                };

        #endregion

        #region TableAdapter, BindingSource-ууд

            // QueriesTableAdapter
            //private static readonly StoreDataSetTableAdapters.QueriesTableAdapter INSTANCE_QUERY_TABLE_ADAPTER = new StoreDataSetTableAdapters.QueriesTableAdapter();
            //public static StoreDataSetTableAdapters.QueriesTableAdapter QueriesTableAdapter
            //{
            //    get
            //    {
            //        return INSTANCE_QUERY_TABLE_ADAPTER;
            //    }
            //}
        

            //Дараалал
            private static readonly StoreDataSetTableAdapters.OrderTableAdapter INSTANCE_ORDER_TABLE_ADAPTER = new StoreDataSetTableAdapters.OrderTableAdapter();
            public static StoreDataSetTableAdapters.OrderTableAdapter OrderTableAdapter
            {
                get
                {
                    return INSTANCE_ORDER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ORDER_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrderBindingSource
            {
                get
                {
                    return INSTANCE_ORDER_BINDING_SOURCE;
                }
            }

            private static readonly BindingSource INSTANCE_ORDER_STORE_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrderStoreBindingSource
            {
                get
                {
                    return INSTANCE_ORDER_STORE_BINDING_SOURCE;
                }
            }    


            // Эмчлүүлэгчийн түүх
            private static readonly StoreDataSetTableAdapters.StoreTableAdapter INSTANCE_STORE_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreTableAdapter();
            public static StoreDataSetTableAdapters.StoreTableAdapter StoreTableAdapter
            {
                get
                {
                    return INSTANCE_STORE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STORE_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreBindingSource
            {
                get
                {
                    return INSTANCE_STORE_BINDING_SOURCE;
                }
            }


            // Эмчлүүлэгчийн үзлэг
            private static readonly StoreDataSetTableAdapters.StoreInspectionTableAdapter INSTANCE_STOREINSPECTION_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreInspectionTableAdapter();
            public static StoreDataSetTableAdapters.StoreInspectionTableAdapter StoreInspectionTableAdapter
            {
                get
                {
                    return INSTANCE_STOREINSPECTION_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STOREINSPECTION_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreInspectionBindingSource
            {
                get
                {
                    return INSTANCE_STOREINSPECTION_BINDING_SOURCE;
                }
            }


            // Эмчилгээ
            private static readonly StoreDataSetTableAdapters.StoreTreatmentTableAdapter INSTANCE_STORETREATMENT_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreTreatmentTableAdapter();
            public static StoreDataSetTableAdapters.StoreTreatmentTableAdapter StoreTreatmentTableAdapter
            {
                get
                {
                    return INSTANCE_STORETREATMENT_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STORETREATMENT_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreTreatmentBindingSource
            {
                get
                {
                    return INSTANCE_STORETREATMENT_BINDING_SOURCE;
                }
            }


            // Онош
            private static readonly StoreDataSetTableAdapters.StoreDiagnoseTableAdapter INSTANCE_STOREDIAGNOSE_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreDiagnoseTableAdapter();
            public static StoreDataSetTableAdapters.StoreDiagnoseTableAdapter StoreDiagnoseTableAdapter
            {
                get
                {
                    return INSTANCE_STOREDIAGNOSE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STOREDIAGNOSE_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreDiagnoseBindingSource
            {
                get
                {
                    return INSTANCE_STOREDIAGNOSE_BINDING_SOURCE;
                }
            }

            // Гарах үеийн онош
            private static readonly StoreDataSetTableAdapters.StoreLeftDiagnoseTableAdapter INSTANCE_STORE_LEFT_DIAGNOSE_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreLeftDiagnoseTableAdapter();
            public static StoreDataSetTableAdapters.StoreLeftDiagnoseTableAdapter StoreLeftDiagnoseTableAdapter
            {
                get
                {
                    return INSTANCE_STORE_LEFT_DIAGNOSE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STORE_LEFT_DIAGNOSE_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreLeftDiagnoseBindingSource
            {
                get
                {
                    return INSTANCE_STORE_LEFT_DIAGNOSE_BINDING_SOURCE;
                }
            }

            // Клиникийн оношийн үндэслэл
            private static readonly StoreDataSetTableAdapters.StoreValidityTableAdapter INSTANCE_STORE_VALIDITY_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreValidityTableAdapter();
            public static StoreDataSetTableAdapters.StoreValidityTableAdapter StoreValidityTableAdapter
            {
                get
                {
                    return INSTANCE_STORE_VALIDITY_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STORE_VALIDITY_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreValidityBindingSource
            {
                get
                {
                    return INSTANCE_STORE_VALIDITY_BINDING_SOURCE;
                }
            }


            //Гарах үеийн онош
            private static readonly StoreDataSetTableAdapters.StoreLeftTableAdapter INSTANCE_STORELEFT_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreLeftTableAdapter();
            public static StoreDataSetTableAdapters.StoreLeftTableAdapter StoreLeftTableAdapter
            {
                get
                {
                    return INSTANCE_STORELEFT_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STORELEFT_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreLeftBindingSource
            {
                get
                {
                    return INSTANCE_STORELEFT_BINDING_SOURCE;
                }
            }

            //Эмчлүүлэгчийн анамнез
            private static readonly StoreDataSetTableAdapters.StoreHistoryTableAdapter INSTANCE_STORE_HISTORY_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreHistoryTableAdapter();
            public static StoreDataSetTableAdapters.StoreHistoryTableAdapter StoreHistoryTableAdapter
            {
                get
                {
                    return INSTANCE_STORE_HISTORY_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STORE_HISTORY_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreHistoryBindingSource
            {
                get
                {
                    return INSTANCE_STORE_HISTORY_BINDING_SOURCE;
                }
            }

            //Ерөнхий үзлэг
            private static readonly StoreDataSetTableAdapters.StoreGeneralinspectionTableAdapter INSTANCE_STORE_GENERAL_INSPECTION_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreGeneralinspectionTableAdapter();
            public static StoreDataSetTableAdapters.StoreGeneralinspectionTableAdapter StoreGeneralinspectionTableAdapter
            {
                get
                {
                    return INSTANCE_STORE_GENERAL_INSPECTION_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STORE_GENERAL_INSPECTION_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreGeneralInspectionBindingSource
            {
                get
                {
                    return INSTANCE_STORE_GENERAL_INSPECTION_BINDING_SOURCE;
                }
            }

            //Дотрын эмчийн үзлэг
            private static readonly StoreDataSetTableAdapters.StoreInternalinspectionTableAdapter INSTANCE_STORE_INTERNAL_INSPECTION_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreInternalinspectionTableAdapter();
            public static StoreDataSetTableAdapters.StoreInternalinspectionTableAdapter StoreInternalinspectionTableAdapter
            {
                get
                {
                    return INSTANCE_STORE_INTERNAL_INSPECTION_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STORE_INTERNAL_INSPECTION_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreInternalinspectionBindingSource
            {
                get
                {
                    return INSTANCE_STORE_INTERNAL_INSPECTION_BINDING_SOURCE;
                }
            }

            //Зүрх судасны тогтолцоо.
            private static readonly StoreDataSetTableAdapters.StoreHeartTableAdapter INSTANCE_STORE_HEART_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreHeartTableAdapter();
            public static StoreDataSetTableAdapters.StoreHeartTableAdapter StoreHeartTableAdapter
            {
                get
                {
                    return INSTANCE_STORE_HEART_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STORE_HEART_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreHeartBindingSource
            {
                get
                {
                    return INSTANCE_STORE_HEART_BINDING_SOURCE;
                }
            }

            //Хоол боловсруулах тогтолцоо.
            private static readonly StoreDataSetTableAdapters.StoreDigestiveTableAdapter INSTANCE_STORE_DIGESTIVE_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreDigestiveTableAdapter();
            public static StoreDataSetTableAdapters.StoreDigestiveTableAdapter StoreDigestiveTableAdapter
            {
                get
                {
                    return INSTANCE_STORE_DIGESTIVE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STORE_DIGESTIVE_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreDigestiveBindingSource
            {
                get
                {
                    return INSTANCE_STORE_DIGESTIVE_BINDING_SOURCE;
                }
            }

            //Шээс бэлгийн тогтолцоо.
            private static readonly StoreDataSetTableAdapters.StoreUrineTableAdapter INSTANCE_STORE_URINE_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreUrineTableAdapter();
            public static StoreDataSetTableAdapters.StoreUrineTableAdapter StoreUrineTableAdapter
            {
                get
                {
                    return INSTANCE_STORE_URINE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STORE_URINE_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreUrineBindingSource
            {
                get
                {
                    return INSTANCE_STORE_URINE_BINDING_SOURCE;
                }
            }

            // Эмчлүүлэгчийн нэмэлт түүх
            private static readonly StoreDataSetTableAdapters.StoreWardRoomTableAdapter INSTANCE_STOREWARDROOM_TABLE_ADAPTER = new StoreDataSetTableAdapters.StoreWardRoomTableAdapter();
            public static StoreDataSetTableAdapters.StoreWardRoomTableAdapter StoreWardRoomTableAdapter
            {
                get
                {
                    return INSTANCE_STOREWARDROOM_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_STOREWARDROOM_BINDING_SOURCE = new BindingSource();
            public static BindingSource StoreWardRoomBindingSource
            {
                get
                {
                    return INSTANCE_STOREWARDROOM_BINDING_SOURCE;
                }
            }


            // Эм зүйчийн тохиргоо
            private static readonly StoreDataSetTableAdapters.PharmacistConfUserTableAdapter INSTANCE_PHARMACIST_CONF_USER_TABLE_ADAPTER = new StoreDataSetTableAdapters.PharmacistConfUserTableAdapter();
            public static StoreDataSetTableAdapters.PharmacistConfUserTableAdapter PharmacistConfUserTableAdapter
            {
                get
                {
                    return INSTANCE_PHARMACIST_CONF_USER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PHARMACIST_CONF_USER_BINDING_SOURCE = new BindingSource();
            public static BindingSource PharmacistConfUserBindingSource
            {
                get
                {
                    return INSTANCE_PHARMACIST_CONF_USER_BINDING_SOURCE;
                }
            }


            // Эмчлүүлэгчийн түүхийн дугаар олголт
            private static readonly StoreDataSetTableAdapters.PatientStoreTableAdapter INSTANCE_PATIENT_STORE_TABLE_ADAPTER = new StoreDataSetTableAdapters.PatientStoreTableAdapter();
            public static StoreDataSetTableAdapters.PatientStoreTableAdapter PatientStoreTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_STORE_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_STORE_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientStoreBindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_STORE_BINDING_SOURCE;
                }
            }


            // Эмчлүүлэгч дугаар олголт
            private static readonly StoreDataSetTableAdapters.PatientOrderTableAdapter INSTANCE_PATIENT_ORDER_TABLE_ADAPTER = new StoreDataSetTableAdapters.PatientOrderTableAdapter();
            public static StoreDataSetTableAdapters.PatientOrderTableAdapter PatientOrderTableAdapter
            {
                get
                {
                    return INSTANCE_PATIENT_ORDER_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_PATIENT_ORDER_BINDING_SOURCE = new BindingSource();
            public static BindingSource PatientOrderBindingSource
            {
                get
                {
                    return INSTANCE_PATIENT_ORDER_BINDING_SOURCE;
                }
            }


            // Хүнгүй байгаа орны мэдээллийг харуулна
            private static readonly StoreDataSetTableAdapters.OrderWardRoomBedTableAdapter INSTANCE_ORDER_WARD_ROOM_BED_TABLE_ADAPTER = new StoreDataSetTableAdapters.OrderWardRoomBedTableAdapter();
            public static StoreDataSetTableAdapters.OrderWardRoomBedTableAdapter OrderWardRoomBedTableAdapter
            {
                get
                {
                    return INSTANCE_ORDER_WARD_ROOM_BED_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ORDER_WARD_ROOM_BED_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrderWardRoomBedBindingSource
            {
                get
                {
                    return INSTANCE_ORDER_WARD_ROOM_BED_BINDING_SOURCE;
                }
            }


            // Хүнгүй байгаа орны мэдээллийг харуулна
            private static readonly StoreDataSetTableAdapters.OrderRoomBedWardTableAdapter INSTANCE_ORDER_ROOM_BED_WARD_TABLE_ADAPTER = new StoreDataSetTableAdapters.OrderRoomBedWardTableAdapter();
            public static StoreDataSetTableAdapters.OrderRoomBedWardTableAdapter OrderRoomBedWardTableAdapter
            {
                get
                {
                    return INSTANCE_ORDER_ROOM_BED_WARD_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ORDER_ROOM_BED_WARD_BINDING_SOURCE = new BindingSource();
            public static BindingSource OrderRoomBedWardBindingSource
            {
                get
                {
                    return INSTANCE_ORDER_ROOM_BED_WARD_BINDING_SOURCE;
                }
            }


            // Хүнгүй байгаа орны мэдээллийг харуулна
            private static readonly StoreDataSetTableAdapters.RoomBedTableAdapter INSTANCE_ROOM_BED_TABLE_ADAPTER = new StoreDataSetTableAdapters.RoomBedTableAdapter();
            public static StoreDataSetTableAdapters.RoomBedTableAdapter RoomBedTableAdapter
            {
                get
                {
                    return INSTANCE_ROOM_BED_TABLE_ADAPTER;
                }
            }

            private static readonly BindingSource INSTANCE_ROOM_BED_BINDING_SOURCE = new BindingSource();
            public static BindingSource RoomBedBindingSource
            {
                get
                {
                    return INSTANCE_ROOM_BED_BINDING_SOURCE;
                }
            }


        #endregion

        // DataSet-ийг ачаалах функц.
        public static void initialize()
        {
            ((ISupportInitialize)(Instance)).BeginInit();
            
            Instance.DataSetName = "UserDataSet";
            Instance.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;


            // Хэрвээ доорх нэг мөр кодчлол нь алдаатай болсон тохиолдолд тухайн
            // DataSet-ийн designer-ийн код руу нь орж InitCommandCollection() функцийн 
            // 'private' -> 'public' болгон өөрчилнө.
            //QueriesTableAdapter.InitCommandCollection();

            //Дараалал
            OrderTableAdapter.Initialize();
            ((ISupportInitialize)(OrderBindingSource)).BeginInit();
            OrderBindingSource.DataMember = "Order";
            OrderBindingSource.DataSource = Instance;
            ((ISupportInitialize)(OrderBindingSource)).EndInit();

            ((ISupportInitialize)(OrderStoreBindingSource)).BeginInit();
            OrderStoreBindingSource.DataMember = "Order";
            OrderStoreBindingSource.DataSource = Instance;
            ((ISupportInitialize)(OrderStoreBindingSource)).EndInit();


            //Түүх
            StoreTableAdapter.Initialize();
            ((ISupportInitialize)(StoreBindingSource)).BeginInit();
            StoreBindingSource.DataMember = "Store";
            StoreBindingSource.DataSource = Instance;
            ((ISupportInitialize)(StoreBindingSource)).EndInit();

            //Эмчлүүлэгчийн түүхээс нэмэлт
            StoreWardRoomTableAdapter.Initialize();
            ((ISupportInitialize)(StoreWardRoomBindingSource)).BeginInit();
            StoreWardRoomBindingSource.DataMember = "Store_StoreWardRoom";
            StoreWardRoomBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreWardRoomBindingSource)).EndInit();

            //Үзлэг
            StoreInspectionTableAdapter.Initialize();
            ((ISupportInitialize)(StoreInspectionBindingSource)).BeginInit();
            StoreInspectionBindingSource.DataMember = "Store_StoreInspection";
            StoreInspectionBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreInspectionBindingSource)).EndInit();

            //Эмчилгээ
            StoreTreatmentTableAdapter.Initialize();
            ((ISupportInitialize)(StoreTreatmentBindingSource)).BeginInit();
            StoreTreatmentBindingSource.DataMember = "StoreInspection_StoreTreatment";
            StoreTreatmentBindingSource.DataSource = StoreInspectionBindingSource;
            ((ISupportInitialize)(StoreTreatmentBindingSource)).EndInit();

            //Онош
            StoreDiagnoseTableAdapter.Initialize();
            ((ISupportInitialize)(StoreDiagnoseBindingSource)).BeginInit();
            StoreDiagnoseBindingSource.DataMember = "Store_StoreDiagnose";
            StoreDiagnoseBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreDiagnoseBindingSource)).EndInit();

            //Гарах үеийн онош
            StoreLeftDiagnoseTableAdapter.Initialize();
            ((ISupportInitialize)(StoreLeftDiagnoseBindingSource)).BeginInit();
            StoreLeftDiagnoseBindingSource.DataMember = "Store_StoreLeftDiagnose";
            StoreLeftDiagnoseBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreLeftDiagnoseBindingSource)).EndInit();

            //Килиникийн оношийн үндэслэл
            StoreValidityTableAdapter.Initialize();
            ((ISupportInitialize)(StoreValidityBindingSource)).BeginInit();
            StoreValidityBindingSource.DataMember = "Store_StoreValidity";
            StoreValidityBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreValidityBindingSource)).EndInit();

            //Гарах үеийн эпикриз
            StoreLeftTableAdapter.Initialize();
            ((ISupportInitialize)(StoreLeftBindingSource)).BeginInit();
            StoreLeftBindingSource.DataMember = "Store_StoreLeft";
            StoreLeftBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreLeftBindingSource)).EndInit();

            //Анамнез
            StoreHistoryTableAdapter.Initialize();
            ((ISupportInitialize)(StoreHistoryBindingSource)).BeginInit();
            StoreHistoryBindingSource.DataMember = "Store_StoreHistory";
            StoreHistoryBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreHistoryBindingSource)).EndInit();

            //Ерөнхий үзлэг
            StoreGeneralinspectionTableAdapter.Initialize();
            ((ISupportInitialize)(StoreGeneralInspectionBindingSource)).BeginInit();
            StoreGeneralInspectionBindingSource.DataMember = "Store_StoreGeneralinspection";
            StoreGeneralInspectionBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreGeneralInspectionBindingSource)).EndInit();

            //Дотрын эмчийн үзлэг.
            OrderTableAdapter.Initialize();
            ((ISupportInitialize)(StoreInternalinspectionBindingSource)).BeginInit();
            StoreInternalinspectionBindingSource.DataMember = "Store_StoreInternalinspection";
            StoreInternalinspectionBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreInternalinspectionBindingSource)).EndInit();

            //Зүрх судасны тогтолцоо
            StoreHeartTableAdapter.Initialize();
            ((ISupportInitialize)(StoreHeartBindingSource)).BeginInit();
            StoreHeartBindingSource.DataMember = "Store_StoreHeart";
            StoreHeartBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreHeartBindingSource)).EndInit();

            //Хоол боловсруулах тогтолцоо
            StoreDigestiveTableAdapter.Initialize();
            ((ISupportInitialize)(StoreDigestiveBindingSource)).BeginInit();
            StoreDigestiveBindingSource.DataMember = "Store_StoreDigestive";
            StoreDigestiveBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreDigestiveBindingSource)).EndInit();

            //Шээс бэлгийн тогтолцоо
            OrderTableAdapter.Initialize();
            ((ISupportInitialize)(StoreUrineBindingSource)).BeginInit();
            StoreUrineBindingSource.DataMember = "Store_StoreUrine";
            StoreUrineBindingSource.DataSource = StoreBindingSource;
            ((ISupportInitialize)(StoreUrineBindingSource)).EndInit();


            
            //Эм зүйчийн тохиргоо
            PharmacistConfUserTableAdapter.Initialize();
            ((ISupportInitialize)(PharmacistConfUserBindingSource)).BeginInit();
            PharmacistConfUserBindingSource.DataMember = "PharmacistConfUser";
            PharmacistConfUserBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PharmacistConfUserBindingSource)).EndInit();



            //Эмчлүүлэгчийн түүхийн дугаар олголт
            PatientStoreTableAdapter.Initialize();
            ((ISupportInitialize)(PatientStoreBindingSource)).BeginInit();
            PatientStoreBindingSource.DataMember = "PatientStore";
            PatientStoreBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientStoreBindingSource)).EndInit();



            // Эмчлүүлэгч дугаар олголт
            PatientOrderTableAdapter.Initialize();
            ((ISupportInitialize)(PatientOrderBindingSource)).BeginInit();
            PatientOrderBindingSource.DataMember = "PatientOrder";
            PatientOrderBindingSource.DataSource = Instance;
            ((ISupportInitialize)(PatientOrderBindingSource)).EndInit();



            // Хүнгүй байгаа орны мэдээллийг харуулна
            OrderWardRoomBedTableAdapter.Initialize();
            ((ISupportInitialize)(OrderWardRoomBedBindingSource)).BeginInit();
            OrderWardRoomBedBindingSource.DataMember = "OrderWardRoomBed";
            OrderWardRoomBedBindingSource.DataSource = Instance;
            ((ISupportInitialize)(OrderWardRoomBedBindingSource)).EndInit();



            // Хүнгүй байгаа орны мэдээллийг харуулна
            OrderRoomBedWardTableAdapter.Initialize();
            ((ISupportInitialize)(OrderRoomBedWardBindingSource)).BeginInit();
            OrderRoomBedWardBindingSource.DataMember = "OrderRoomBedWard";
            OrderRoomBedWardBindingSource.DataSource = Instance;
            ((ISupportInitialize)(OrderRoomBedWardBindingSource)).EndInit();



            // Хүнгүй байгаа орны мэдээллийг харуулна
            RoomBedTableAdapter.Initialize();
            ((ISupportInitialize)(RoomBedBindingSource)).BeginInit();
            RoomBedBindingSource.DataMember = "RoomBed";
            RoomBedBindingSource.DataSource = Instance;
            ((ISupportInitialize)(RoomBedBindingSource)).EndInit();


            if (Instance.PaymentType.Rows.Count == 0)
            {
                for (int i = 0; i < paymentType.Length; i++)
                {
                    DataRow row = Instance.PaymentType.NewRow();
                    row["id"] = i + 1;
                    row["name"] = paymentType[i];
                    row.EndEdit();
                    Instance.PaymentType.Rows.Add(row);
                }
            }
            

            if (Instance.AlergyType.Rows.Count == 0)
            {
                for (int i = 0; i < alergyType.Length; i++)
                {
                    DataRow row = Instance.AlergyType.NewRow();
                    row["id"] = i + 1;
                    row["name"] = alergyType[i];
                    row.EndEdit();
                    Instance.AlergyType.Rows.Add(row);
                }
            }
           

            if (Instance.DiagnoseType.Rows.Count == 0)
            {
                for (int i = 0; i < diagnoseType.Length; i++)
                {
                    DataRow row = Instance.DiagnoseType.NewRow();
                    row["id"] = i + 1;
                    row["name"] = diagnoseType[i];
                    row.EndEdit();
                    Instance.DiagnoseType.Rows.Add(row);
                }
            }
            

            if (Instance.LeftDiagnose.Rows.Count == 0)
            {
                for (int i = 0; i < leftDiagnose.Length; i++)
                {
                    DataRow row = Instance.LeftDiagnose.NewRow();
                    row["id"] = i + 1;
                    row["name"] = leftDiagnose[i];
                    row.EndEdit();
                    Instance.LeftDiagnose.Rows.Add(row);
                }
            }
            

            if (Instance.EndHurt.Rows.Count == 0)
            {
                for (int i = 0; i < endHurt.Length; i++)
                {
                    DataRow row = Instance.EndHurt.NewRow();
                    row["id"] = i + 1;
                    row["name"] = endHurt[i];
                    row.EndEdit();
                    Instance.EndHurt.Rows.Add(row);
                }
            }
            

            if (Instance.LeaveHospital.Rows.Count == 0)
            {
                for (int i = 0; i < leaveHospital.Length; i++)
                {
                    DataRow row = Instance.LeaveHospital.NewRow();
                    row["id"] = i + 1;
                    row["name"] = leaveHospital[i];
                    row.EndEdit();
                    Instance.LeaveHospital.Rows.Add(row);
                }
            }
            

            if (Instance.InCome.Rows.Count == 0)
            {
                for (int i = 0; i < inCome.Length; i++)
                {
                    DataRow row = Instance.InCome.NewRow();
                    row["id"] = i + 1;
                    row["name"] = inCome[i];
                    row.EndEdit();
                    Instance.InCome.Rows.Add(row);
                }
            }
            

            if (Instance.InBack.Rows.Count == 0)
            {
                for (int i = 0; i < inBack.Length; i++)
                {
                    DataRow row = Instance.InBack.NewRow();
                    row["id"] = i + 1;
                    row["name"] = inBack[i];
                    row.EndEdit();
                    Instance.InBack.Rows.Add(row);
                }
            }
            

            if (Instance.Surgery.Rows.Count == 0)
            {
                for (int i = 0; i < surgery.Length; i++)
                {
                    DataRow row = Instance.Surgery.NewRow();
                    row["id"] = i + 1;
                    row["name"] = surgery[i];
                    row.EndEdit();
                    Instance.Surgery.Rows.Add(row);
                }
            }
            

            if (Instance.AnalysisTreat.Rows.Count == 0)
            {
                for (int i = 0; i < analysisTreat.Length; i++)
                {
                    DataRow row = Instance.AnalysisTreat.NewRow();
                    row["id"] = i + 1;
                    row["name"] = analysisTreat[i];
                    row.EndEdit();
                    Instance.AnalysisTreat.Rows.Add(row);
                }
            }
           

            if (Instance.AnalysisLight.Rows.Count == 0)
            {
                for (int i = 0; i < analysisLight.Length; i++)
                {
                    DataRow row = Instance.AnalysisLight.NewRow();
                    row["id"] = i + 1;
                    row["name"] = analysisLight[i];
                    row.EndEdit();
                    Instance.AnalysisLight.Rows.Add(row);
                }
            }
            

            if (Instance.LifeTerm.Rows.Count == 0)
            {
                for (int i = 0; i < lifeTerm.Length; i++)
                {
                    DataRow row = Instance.LifeTerm.NewRow();
                    row["id"] = i + 1;
                    row["name"] = lifeTerm[i];
                    row.EndEdit();
                    Instance.LifeTerm.Rows.Add(row);
                }
            }
            

            if (Instance.WorkTerm.Rows.Count == 0)
            {
                for (int i = 0; i < workTerm.Length; i++)
                {
                    DataRow row = Instance.WorkTerm.NewRow();
                    row["id"] = i + 1;
                    row["name"] = workTerm[i];
                    row.EndEdit();
                    Instance.WorkTerm.Rows.Add(row);
                }
            }
            

            if (Instance.Infection.Rows.Count == 0)
            {
                for (int i = 0; i < infection.Length; i++)
                {
                    DataRow row = Instance.Infection.NewRow();
                    row["id"] = i + 1;
                    row["name"] = infection[i];
                    row.EndEdit();
                    Instance.Infection.Rows.Add(row);
                }
            }
            

            if (Instance.Diet.Rows.Count == 0)
            {
                for (int i = 0; i < diet.Length; i++)
                {
                    DataRow row = Instance.Diet.NewRow();
                    row["id"] = i + 1;
                    row["name"] = diet[i];
                    row.EndEdit();
                    Instance.Diet.Rows.Add(row);
                }
            }
            

            if (Instance.Mind.Rows.Count == 0)
            {
                for (int i = 0; i < mind.Length; i++)
                {
                    DataRow row = Instance.Mind.NewRow();
                    row["id"] = i + 1;
                    row["name"] = mind[i];
                    row.EndEdit();
                    Instance.Mind.Rows.Add(row);
                }
            }
            

            if (Instance.Surround.Rows.Count == 0)
            {
                for (int i = 0; i < surround.Length; i++)
                {
                    DataRow row = Instance.Surround.NewRow();
                    row["id"] = i + 1;
                    row["name"] = surround[i];
                    row.EndEdit();
                    Instance.Surround.Rows.Add(row);
                }
            }
            

            if (Instance.Position.Rows.Count == 0)
            {
                for (int i = 0; i < position.Length; i++)
                {
                    DataRow row = Instance.Position.NewRow();
                    row["id"] = i + 1;
                    row["name"] = position[i];
                    row.EndEdit();
                    Instance.Position.Rows.Add(row);
                }
            }
            

            if (Instance.Skin.Rows.Count == 0)
            {
                for (int i = 0; i < skin.Length; i++)
                {
                    DataRow row = Instance.Skin.NewRow();
                    row["id"] = i + 1;
                    row["name"] = skin[i];
                    row.EndEdit();
                    Instance.Skin.Rows.Add(row);
                }
            }
            

            if (Instance.GeneralPart.Rows.Count == 0)
            {
                for (int i = 0; i < generalPart.Length; i++)
                {
                    DataRow row = Instance.GeneralPart.NewRow();
                    row["id"] = i + 1;
                    row["name"] = generalPart[i];
                    row.EndEdit();
                    Instance.GeneralPart.Rows.Add(row);
                }
            }
           

            if (Instance.Size.Rows.Count == 0)
            {
                for (int i = 0; i < size.Length; i++)
                {
                    DataRow row = Instance.Size.NewRow();
                    row["id"] = i + 1;
                    row["name"] = size[i];
                    row.EndEdit();
                    Instance.Size.Rows.Add(row);
                }
            }
            

            if (Instance.Place.Rows.Count == 0)
            {
                for (int i = 0; i < place.Length; i++)
                {
                    DataRow row = Instance.Place.NewRow();
                    row["id"] = i + 1;
                    row["name"] = place[i];
                    row.EndEdit();
                    Instance.Place.Rows.Add(row);
                }
            }
            

            if (Instance.Move.Rows.Count == 0)
            {
                for (int i = 0; i < move.Length; i++)
                {
                    DataRow row = Instance.Move.NewRow();
                    row["id"] = i + 1;
                    row["name"] = move[i];
                    row.EndEdit();
                    Instance.Move.Rows.Add(row);
                }
            }
            

            if (Instance.BreathShape.Rows.Count == 0)
            {
                for (int i = 0; i < breathShape.Length; i++)
                {
                    DataRow row = Instance.BreathShape.NewRow();
                    row["id"] = i + 1;
                    row["name"] = breathShape[i];
                    row.EndEdit();
                    Instance.BreathShape.Rows.Add(row);
                }
            }
            

            if (Instance.SoundVib.Rows.Count == 0)
            {
                for (int i = 0; i < soundVib.Length; i++)
                {
                    DataRow row = Instance.SoundVib.NewRow();
                    row["id"] = i + 1;
                    row["name"] = soundVib[i];
                    row.EndEdit();
                    Instance.SoundVib.Rows.Add(row);
                }
            }
            

            if (Instance.PartPercussion.Rows.Count == 0)
            {
                for (int i = 0; i < partPercussion.Length; i++)
                {
                    DataRow row = Instance.PartPercussion.NewRow();
                    row["id"] = i + 1;
                    row["name"] = partPercussion[i];
                    row.EndEdit();
                    Instance.PartPercussion.Rows.Add(row);
                }
            }
            

            if (Instance.IfDesease.Rows.Count == 0)
            {
                for (int i = 0; i < ifDesease.Length; i++)
                {
                    DataRow row = Instance.IfDesease.NewRow();
                    row["id"] = i + 1;
                    row["name"] = ifDesease[i];
                    row.EndEdit();
                    Instance.IfDesease.Rows.Add(row);
                }
            }
            

            if (Instance.IfNoisy.Rows.Count == 0)
            {
                for (int i = 0; i < ifNoisy.Length; i++)
                {
                    DataRow row = Instance.IfNoisy.NewRow();
                    row["id"] = i + 1;
                    row["name"] = ifNoisy[i];
                    row.EndEdit();
                    Instance.IfNoisy.Rows.Add(row);
                }
            }
            

            if (Instance.Bronkhofoni.Rows.Count == 0)
            {
                for (int i = 0; i < bronkhofoni.Length; i++)
                {
                    DataRow row = Instance.Bronkhofoni.NewRow();
                    row["id"] = i + 1;
                    row["name"] = bronkhofoni[i];
                    row.EndEdit();
                    Instance.Bronkhofoni.Rows.Add(row);
                }
            }
            

            if (Instance.HeartRisk.Rows.Count == 0)
            {
                for (int i = 0; i < heartRisk.Length; i++)
                {
                    DataRow row = Instance.HeartRisk.NewRow();
                    row["id"] = i + 1;
                    row["name"] = heartRisk[i];
                    row.EndEdit();
                    Instance.HeartRisk.Rows.Add(row);
                }
            }
            

            if (Instance.Strong.Rows.Count == 0)
            {
                for (int i = 0; i < strong.Length; i++)
                {
                    DataRow row = Instance.Strong.NewRow();
                    row["id"] = i + 1;
                    row["name"] = strong[i];
                    row.EndEdit();
                    Instance.Strong.Rows.Add(row);
                }
            }
            

            if (Instance.Voltage.Rows.Count == 0)
            {
                for (int i = 0; i < voltage.Length; i++)
                {
                    DataRow row = Instance.Voltage.NewRow();
                    row["id"] = i + 1;
                    row["name"] = voltage[i];
                    row.EndEdit();
                    Instance.Voltage.Rows.Add(row);
                }
            }
            

            if (Instance.Direction.Rows.Count == 0)
            {
                for (int i = 0; i < direction.Length; i++)
                {
                    DataRow row = Instance.Direction.NewRow();
                    row["id"] = i + 1;
                    row["name"] = direction[i];
                    row.EndEdit();
                    Instance.Direction.Rows.Add(row);
                }
            }
            

            if (Instance.FirstSound.Rows.Count == 0)
            {
                for (int i = 0; i < firstSound.Length; i++)
                {
                    DataRow row = Instance.FirstSound.NewRow();
                    row["id"] = i + 1;
                    row["name"] = firstSound[i];
                    row.EndEdit();
                    Instance.FirstSound.Rows.Add(row);
                }
            }
            

            if (Instance.SecondSound.Rows.Count == 0)
            {
                for (int i = 0; i < secondSound.Length; i++)
                {
                    DataRow row = Instance.SecondSound.NewRow();
                    row["id"] = i + 1;
                    row["name"] = secondSound[i];
                    row.EndEdit();
                    Instance.SecondSound.Rows.Add(row);
                }
            }
            

            if (Instance.ThirdSound.Rows.Count == 0)
            {
                for (int i = 0; i < thirdSound.Length; i++)
                {
                    DataRow row = Instance.ThirdSound.NewRow();
                    row["id"] = i + 1;
                    row["name"] = thirdSound[i];
                    row.EndEdit();
                    Instance.ThirdSound.Rows.Add(row);
                }
            }
            

            if (Instance.Romb.Rows.Count == 0)
            {
                for (int i = 0; i < romb.Length; i++)
                {
                    DataRow row = Instance.Romb.NewRow();
                    row["id"] = i + 1;
                    row["name"] = romb[i];
                    row.EndEdit();
                    Instance.Romb.Rows.Add(row);
                }
            }
            

            if (Instance.IsPercussion.Rows.Count == 0)
            {
                for (int i = 0; i < isPercussion.Length; i++)
                {
                    DataRow row = Instance.IsPercussion.NewRow();
                    row["id"] = i + 1;
                    row["name"] = isPercussion[i];
                    row.EndEdit();
                    Instance.IsPercussion.Rows.Add(row);
                }
            }
           

            if (Instance.IsMoving.Rows.Count == 0)
            {
                for (int i = 0; i < isMoving.Length; i++)
                {
                    DataRow row = Instance.IsMoving.NewRow();
                    row["id"] = i + 1;
                    row["name"] = isMoving[i];
                    row.EndEdit();
                    Instance.IsMoving.Rows.Add(row);
                }
            }
            

            if (Instance.Liver.Rows.Count == 0)
            {
                for (int i = 0; i < liver.Length; i++)
                {
                    DataRow row = Instance.Liver.NewRow();
                    row["id"] = i + 1;
                    row["name"] = liver[i];
                    row.EndEdit();
                    Instance.Liver.Rows.Add(row);
                }
            }
            

            if (Instance.UrineOutput.Rows.Count == 0)
            {
                for (int i = 0; i < urineOutput.Length; i++)
                {
                    DataRow row = Instance.UrineOutput.NewRow();
                    row["id"] = i + 1;
                    row["name"] = urineOutput[i];
                    row.EndEdit();
                    Instance.UrineOutput.Rows.Add(row);
                }
            }
            

            if (Instance.UrineColor.Rows.Count == 0)
            {
                for (int i = 0; i < urineColor.Length; i++)
                {
                    DataRow row = Instance.UrineColor.NewRow();
                    row["id"] = i + 1;
                    row["name"] = urineColor[i];
                    row.EndEdit();
                    Instance.UrineColor.Rows.Add(row);
                }
            }
            
            if (Instance.Validity.Rows.Count == 0)
            {
                for (int i = 0; i < validity.Length; i++)
                {
                    DataRow row = Instance.Validity.NewRow();
                    row["id"] = i + 1;
                    row["name"] = validity[i];
                    row.EndEdit();
                    Instance.Validity.Rows.Add(row);
                }
            }

            if (Instance.Code.Rows.Count == 0)
            {
                for (int i = 0; i < code.Length; i++)
                {
                    DataRow row = Instance.Code.NewRow();
                    row["id"] = i + 1;
                    row["name"] = code[i];
                    row.EndEdit();
                    Instance.Code.Rows.Add(row);
                }
            }
            

            ((ISupportInitialize)(Instance)).EndInit();
        }

        public static void changeConnectionString()
        {
            OrderTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreInspectionTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreTreatmentTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreDiagnoseTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreLeftDiagnoseTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreHistoryTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreGeneralinspectionTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreInternalinspectionTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreHeartTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreDigestiveTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreUrineTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreWardRoomTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PharmacistConfUserTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PatientStoreTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            PatientOrderTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            OrderWardRoomBedTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            OrderRoomBedWardTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            RoomBedTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreValidityTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
            StoreLeftTableAdapter.Connection.ConnectionString = Pharmacy2016.Properties.Settings.Default.conMedicine;
        }

    }
}

