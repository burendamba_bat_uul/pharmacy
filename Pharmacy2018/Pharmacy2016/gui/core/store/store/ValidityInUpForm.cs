﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.util;

namespace Pharmacy2016.gui.core.store.store
{
    /*
     * Эмчлүүлэгчийн оношийн үндэслэл нэмэх, засах цонх.
     * 
     */

    public partial class ValidityInUpForm : DevExpress.XtraEditors.XtraForm
    {
        #region Форм анх ачааллах функц.

            private static ValidityInUpForm INSTANCE = new ValidityInUpForm();

            public static ValidityInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private ValidityInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initBinding();
                initDataSource();
                reload();
            }

            private void initDataSource()
            {
                gridLookUpEditDiagnoseType.Properties.DataSource = StoreDataSet.Instance.Validity;
                gridLookUpEditDoctor.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
            }

            private void initBinding()
            {
                dateEditDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreValidityBindingSource, "date", true));
                gridLookUpEditDiagnoseType.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreValidityBindingSource, "diagnoseType", true));
                memoEditDesc.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreValidityBindingSource, "description", true));
                gridLookUpEditDoctor.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreValidityBindingSource, "userID", true));
            }

            private void initError()
            {
                dateEditDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditDoctor.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insertValidity();
                    }
                    else if (actionType == 2)
                    {
                        updateValidity();
                    }

                    ShowDialog();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insertValidity()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    StoreInUpForm.Instance.currValidityView["userID"] = UserUtil.getUserId();
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                    gridLookUpEditDoctor.EditValue = UserUtil.getUserId();
                }
                DateTime now = DateTime.Now;
                StoreInUpForm.Instance.currValidityView["date"] = now;
                dateEditDate.DateTime = now;
            }

            private void updateValidity()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                }
            }



            private void clearData()
            {
                clearErrorText();

                InformationDataSet.SystemUserBindingSource.Filter = "";
                StoreDataSet.StoreValidityBindingSource.CancelEdit();
            }

            private void clearErrorText()
            {
                dateEditDate.ErrorText = "";
                gridLookUpEditDoctor.ErrorText = "";
            }

            private void ValidityInUpForm_Shown(object sender, EventArgs e)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    dateEditDate.Focus();
                }
                else
                {
                    gridLookUpEditDoctor.Focus();               
                }
            }

        #endregion           

        #region Формын event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                saveValidity();
            }

            private void ValidityInUpForm_FormClosing(object sender, FormClosingEventArgs e)
            {

            }

        #endregion   
 
        #region Хадгалах, ачааллах функц

            private void reload()
            {
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.DOCTOR);
            }

            private bool checkValidity()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (gridLookUpEditDoctor.EditValue == DBNull.Value || gridLookUpEditDoctor.EditValue == null || gridLookUpEditDoctor.EditValue.ToString().Equals(""))
                {
                    errorText = "Үзлэг хийх эмчийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditDoctor.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditDoctor.Focus();
                }
                if (dateEditDate.EditValue == DBNull.Value || dateEditDate.EditValue == null || dateEditDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditDate.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            public void saveValidity()
            {
                if (checkValidity())
                {
                    StoreInUpForm.Instance.currValidityView["fullName"] = gridLookUpEditDoctor.Text;

                    StoreInUpForm.Instance.currValidityView.EndEdit();
                    StoreDataSet.StoreValidityBindingSource.EndEdit();

                    Close();
                }
            }

        #endregion
        
    }
}