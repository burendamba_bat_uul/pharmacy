﻿namespace Pharmacy2016.gui.core.store.store
{
    partial class StoreInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StoreInUpForm));
            this.gridViewInspectionTreatment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnInspectionTreatmentStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumnInspectionTreatmentEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnInspectionDescriptionMedicine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnInspectionDescriptionDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnInspectionTreatmentEachDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnInspectionTreatmentDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlStoreInspection = new DevExpress.XtraGrid.GridControl();
            this.gridViewStoreInspection = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnStoreInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreInspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreInspectionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreInspectionDoctorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.treeListLookUpEditWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnWard = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditAlergyType = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnAlergyTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditOrder = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOrderName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOrderRegister = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.textEditFirstName = new DevExpress.XtraEditors.TextEdit();
            this.gridLookUpEditPaymentType = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnPaymentTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEditLastName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupLoopState = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditBirthdate = new DevExpress.XtraEditors.DateEdit();
            this.radioGroupGender = new DevExpress.XtraEditors.RadioGroup();
            this.checkEditIsUnder = new DevExpress.XtraEditors.CheckEdit();
            this.radioGroupIsMarried = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditBeginDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEditNumber = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditRegister = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.textEditEmd = new DevExpress.XtraEditors.TextEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancelStore = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSaveStore = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.textEditEducation = new DevExpress.XtraEditors.TextEdit();
            this.textEditProvince = new DevExpress.XtraEditors.TextEdit();
            this.textEditSpecialty = new DevExpress.XtraEditors.TextEdit();
            this.textEditPosition = new DevExpress.XtraEditors.TextEdit();
            this.textEditOrganization = new DevExpress.XtraEditors.TextEdit();
            this.memoEditBeginDiagnose = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditAddress = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.textEditEmergencyPhone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.textEditEmergencyName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlWardRoom = new DevExpress.XtraGrid.GridControl();
            this.gridViewWardRoom = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlStoreDiagnose = new DevExpress.XtraGrid.GridControl();
            this.gridViewStoreDiagnose = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnStoreDiagnoseDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumnStoreDiagnose = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreDiagnoseDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreDiagnoseTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreDiagnoseDoctorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControlAnamnez = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageAnamnez1 = new DevExpress.XtraTab.XtraTabPage();
            this.checkEditStore = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl66 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl65 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl64 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl63 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl62 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl61 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl60 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl59 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl52 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl51 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl50 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl49 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditPain = new DevExpress.XtraEditors.MemoEdit();
            this.memoEditHistory = new DevExpress.XtraEditors.MemoEdit();
            this.memoEditLifeStore = new DevExpress.XtraEditors.MemoEdit();
            this.spinEditHigh = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditWeight = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditBodyIndex = new DevExpress.XtraEditors.SpinEdit();
            this.radioGroupBloodPressure = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditLifeTerm = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditWorkTerm = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditInfection = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEditDoesSurgery = new DevExpress.XtraEditors.CheckEdit();
            this.memoEditAccident = new DevExpress.XtraEditors.MemoEdit();
            this.gridLookUpEditAllergy = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupIsVodka = new DevExpress.XtraEditors.RadioGroup();
            this.RadioGroupIsSmoke = new DevExpress.XtraEditors.RadioGroup();
            this.memoEditHowLong = new DevExpress.XtraEditors.MemoEdit();
            this.gridLookUpEditDiet = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView16 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.memoEditInheritance = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabPageAnamnez2 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl83 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl82 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl81 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl80 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl79 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl78 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl77 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl76 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl75 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl74 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl73 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl72 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl71 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl70 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl69 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl68 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl67 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditGeneralCondition = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView17 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditMind = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView18 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditSurround = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView19 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditPos = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView20 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupSkinSalst = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditSkinFlexible = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView21 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupSkinRash = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupEdema = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditGeneralPart = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView22 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridlookUpEditSize = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView23 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditPlace = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView24 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupConcern = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupShape = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditMove = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView25 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditSkinWet = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView26 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEditGeneral = new DevExpress.XtraEditors.CheckEdit();
            this.xtraTabPageAnamnez3 = new DevExpress.XtraTab.XtraTabPage();
            this.checkEditInternal = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl108 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl107 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl106 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl105 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl104 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl103 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl102 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl101 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl100 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl99 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl98 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl97 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl96 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl95 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl94 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl93 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl92 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl91 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl90 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl89 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl88 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl87 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl86 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl85 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl84 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupIsBreathing = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsCyanosis = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIfCyanosis = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsBreathMuscles = new DevExpress.XtraEditors.RadioGroup();
            this.spinEditBreathCount = new DevExpress.XtraEditors.SpinEdit();
            this.radioGroupChestShape = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupBreathChest = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsCount = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIconcern = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditSoundVib = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView27 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupPercussion = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditPartPercussion = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView28 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupBreathDouble = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsDesease = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditIfDesease = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView29 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupIsFlexible = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsNoisy = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditIfNoisy = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView30 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditBronkhofoni = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView31 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupRightLeft = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditBreathShape = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView32 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageAnamnez4 = new DevExpress.XtraTab.XtraTabPage();
            this.checkEditHeart = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl138 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl137 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl136 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl135 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl134 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl133 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl132 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl131 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl130 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl129 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl128 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl127 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl126 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl125 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl124 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl123 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl122 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl121 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl120 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl119 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl118 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl117 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl116 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl115 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl114 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl113 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl112 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl111 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl110 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl109 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupSkinBlue = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsEdem = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupHeartPush = new DevExpress.XtraEditors.RadioGroup();
            this.spinEditFrequency = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditHeartFrequency = new DevExpress.XtraEditors.SpinEdit();
            this.radioGroupHeartSound = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupRate = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditFirstSound = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView33 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditSecondSound = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView34 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditThirdSound = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView35 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupSameDouble = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupTransmission = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditTransStrong = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView36 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupHeartLimit = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditSezi = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView37 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditSis = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView38 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditPost = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView39 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditDis = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView40 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupIsTouch = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditHeartRisk = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView41 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupVenous = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditVoltage = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView42 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupFilled = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupHeartPlace = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupThisNoisy = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditStrong = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView43 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageAnamnez5 = new DevExpress.XtraTab.XtraTabPage();
            this.radioGroupTideConcern = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsTuck = new DevExpress.XtraEditors.RadioGroup();
            this.checkEditDigestive = new DevExpress.XtraEditors.CheckEdit();
            this.radioGroupSpleen = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl171 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl170 = new DevExpress.XtraEditors.LabelControl();
            this.textEditDuringDay = new DevExpress.XtraEditors.TextEdit();
            this.labelControl169 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditLiver = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView46 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl168 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl167 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupIsTidePos = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsTideStable = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsCrossPos = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsCrossStable = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsPos = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsPosStable = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupCrossConcern = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupPosConcern = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl166 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl165 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl164 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl163 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl162 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl161 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl160 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl159 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl158 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl157 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl156 = new DevExpress.XtraEditors.LabelControl();
            this.textEditTongueShape = new DevExpress.XtraEditors.TextEdit();
            this.textEditColor = new DevExpress.XtraEditors.TextEdit();
            this.labelControl155 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl154 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl153 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl152 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl151 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl150 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl149 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl148 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl147 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl146 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl145 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl144 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl143 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl142 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl141 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl140 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl139 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupIsTongue = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsSkinWet = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupTuckConcern = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsTympanic = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsTough = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditIsPercussion = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView44 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditIsMoving = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView45 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupBellyConcern = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsBellyStable = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupIsBellyPos = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupLiverSize = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupLiverDirection = new DevExpress.XtraEditors.RadioGroup();
            this.xtraTabPageAnamnez6 = new DevExpress.XtraTab.XtraTabPage();
            this.checkEditUrine = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl193 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl192 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl191 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl190 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl189 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl188 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl187 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl186 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl185 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl184 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl183 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl182 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl181 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl180 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl179 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl178 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl177 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupKidneyConcern = new DevExpress.XtraEditors.LabelControl();
            this.labelControl175 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl174 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl173 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl172 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditOther = new DevExpress.XtraEditors.MemoEdit();
            this.memoEditMentalStatus = new DevExpress.XtraEditors.MemoEdit();
            this.gridLookUpEditShallow = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView47 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpeditDeep = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView48 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditQuiteDeep = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView49 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupReflex = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupFaceDouble = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupSmell = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupHear = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditUrineOutPut = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView50 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditUrineColor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView51 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroupIsUrineNight = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupPushUrine = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupHurtUrine = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupKidney = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupPastyernatskii = new DevExpress.XtraEditors.RadioGroup();
            this.spinEditIsUrineRight = new DevExpress.XtraEditors.SpinEdit();
            this.radioGroupCutUrine = new DevExpress.XtraEditors.RadioGroup();
            this.radioGroupMissUrine = new DevExpress.XtraEditors.RadioGroup();
            this.xtraTabPageAnamnez7 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlValidity = new DevExpress.XtraGrid.GridControl();
            this.gridViewValidity = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControlStoreLeft = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageStoreLeftFirst = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl176 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditDoctor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEditIsClose = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.radionGroupLeaveHospital = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditDiagnose = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnDiagnoseType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditEndDate = new DevExpress.XtraEditors.DateEdit();
            this.gridLookUpEditEndHurt = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.memoEditOtherTreat = new DevExpress.XtraEditors.MemoEdit();
            this.checkEditBlood = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditIsSurgery = new DevExpress.XtraEditors.CheckEdit();
            this.memoEditSurgery = new DevExpress.XtraEditors.MemoEdit();
            this.checkEditIsAfterSurgery = new DevExpress.XtraEditors.CheckEdit();
            this.memoEditAfterSurgery = new DevExpress.XtraEditors.MemoEdit();
            this.memoEditBillOfMedic = new DevExpress.XtraEditors.MemoEdit();
            this.gridLookUpEditTreat = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spinEditDuration = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditBill = new DevExpress.XtraEditors.SpinEdit();
            this.xtraTabPageStoreLeftSecond = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditAnalysisVirus = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl46 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditAnalysisChanged = new DevExpress.XtraEditors.MemoEdit();
            this.memoEditAnalysisBlood = new DevExpress.XtraEditors.MemoEdit();
            this.radioGroupIsHeal = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEditInCome = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditInBack = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditAsTreat = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditAsLight = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.memoEditAnalysisGeneral = new DevExpress.XtraEditors.MemoEdit();
            this.xtraTabPageStoreLeftThird = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlStoreLeftDiagnose = new DevExpress.XtraGrid.GridControl();
            this.gridViewStoreLeftDiagnose = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnLeftDiagnoseDoctor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLeftDiagnoseType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLeftDiagnoseDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLeftDiagnoseCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.alertControlPharmacy2016 = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInspectionTreatment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStoreInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStoreInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditAlergyType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupLoopState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthdate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthdate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsUnder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsMarried.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBeginDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBeginDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegister.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).BeginInit();
            this.xtraTabControl.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEducation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProvince.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSpecialty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOrganization.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditBeginDiagnose.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmergencyPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmergencyName.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWardRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWardRoom)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStoreDiagnose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStoreDiagnose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlAnamnez)).BeginInit();
            this.xtraTabControlAnamnez.SuspendLayout();
            this.xtraTabPageAnamnez1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditStore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditPain.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditHistory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditLifeStore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHigh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBodyIndex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupBloodPressure.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditLifeTerm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditWorkTerm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditInfection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDoesSurgery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAccident.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditAllergy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsVodka.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadioGroupIsSmoke.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditHowLong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDiet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditInheritance.Properties)).BeginInit();
            this.xtraTabPageAnamnez2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditGeneralCondition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSurround.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSkinSalst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSkinFlexible.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSkinRash.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupEdema.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditGeneralPart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridlookUpEditSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPlace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupConcern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupShape.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMove.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSkinWet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditGeneral.Properties)).BeginInit();
            this.xtraTabPageAnamnez3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditInternal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsBreathing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsCyanosis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIfCyanosis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsBreathMuscles.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBreathCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupChestShape.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupBreathChest.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIconcern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSoundVib.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPercussion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPartPercussion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupBreathDouble.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsDesease.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIfDesease.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsFlexible.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsNoisy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIfNoisy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditBronkhofoni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupRightLeft.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditBreathShape.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView32)).BeginInit();
            this.xtraTabPageAnamnez4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditHeart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSkinBlue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsEdem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHeartPush.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditFrequency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHeartFrequency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHeartSound.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditFirstSound.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSecondSound.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditThirdSound.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSameDouble.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupTransmission.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTransStrong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHeartLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSezi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDis.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTouch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditHeartRisk.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupVenous.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditVoltage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupFilled.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHeartPlace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupThisNoisy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditStrong.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView43)).BeginInit();
            this.xtraTabPageAnamnez5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupTideConcern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTuck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDigestive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSpleen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDuringDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditLiver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTidePos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTideStable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsCrossPos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsCrossStable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsPos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsPosStable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupCrossConcern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPosConcern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTongueShape.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditColor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTongue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsSkinWet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupTuckConcern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTympanic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTough.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIsPercussion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIsMoving.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupBellyConcern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsBellyStable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsBellyPos.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupLiverSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupLiverDirection.Properties)).BeginInit();
            this.xtraTabPageAnamnez6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditUrine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditOther.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditMentalStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditShallow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpeditDeep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditQuiteDeep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupReflex.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupFaceDouble.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSmell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUrineOutPut.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUrineColor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsUrineNight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPushUrine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHurtUrine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupKidney.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPastyernatskii.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditIsUrineRight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupCutUrine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupMissUrine.Properties)).BeginInit();
            this.xtraTabPageAnamnez7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlValidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewValidity)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlStoreLeft)).BeginInit();
            this.xtraTabControlStoreLeft.SuspendLayout();
            this.xtraTabPageStoreLeftFirst.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsClose.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radionGroupLeaveHospital.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDiagnose.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditEndHurt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditOtherTreat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlood.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsSurgery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditSurgery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsAfterSurgery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAfterSurgery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditBillOfMedic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTreat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDuration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBill.Properties)).BeginInit();
            this.xtraTabPageStoreLeftSecond.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAnalysisVirus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAnalysisChanged.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAnalysisBlood.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsHeal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditInCome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditInBack.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditAsTreat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditAsLight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAnalysisGeneral.Properties)).BeginInit();
            this.xtraTabPageStoreLeftThird.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStoreLeftDiagnose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStoreLeftDiagnose)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewInspectionTreatment
            // 
            this.gridViewInspectionTreatment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnInspectionTreatmentStartDate,
            this.gridColumnInspectionTreatmentEndDate,
            this.gridColumnInspectionDescriptionMedicine,
            this.gridColumnInspectionDescriptionDuration,
            this.gridColumnInspectionTreatmentEachDay,
            this.gridColumnInspectionTreatmentDescription});
            this.gridViewInspectionTreatment.GridControl = this.gridControlStoreInspection;
            this.gridViewInspectionTreatment.Name = "gridViewInspectionTreatment";
            this.gridViewInspectionTreatment.OptionsBehavior.ReadOnly = true;
            this.gridViewInspectionTreatment.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewInspectionTreatment.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnInspectionTreatmentStartDate
            // 
            this.gridColumnInspectionTreatmentStartDate.Caption = "Эхлэх огноо";
            this.gridColumnInspectionTreatmentStartDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnInspectionTreatmentStartDate.FieldName = "startDate";
            this.gridColumnInspectionTreatmentStartDate.Name = "gridColumnInspectionTreatmentStartDate";
            this.gridColumnInspectionTreatmentStartDate.Visible = true;
            this.gridColumnInspectionTreatmentStartDate.VisibleIndex = 0;
            this.gridColumnInspectionTreatmentStartDate.Width = 92;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridColumnInspectionTreatmentEndDate
            // 
            this.gridColumnInspectionTreatmentEndDate.Caption = "Дуусах огноо";
            this.gridColumnInspectionTreatmentEndDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnInspectionTreatmentEndDate.FieldName = "endDate";
            this.gridColumnInspectionTreatmentEndDate.Name = "gridColumnInspectionTreatmentEndDate";
            this.gridColumnInspectionTreatmentEndDate.Visible = true;
            this.gridColumnInspectionTreatmentEndDate.VisibleIndex = 1;
            this.gridColumnInspectionTreatmentEndDate.Width = 87;
            // 
            // gridColumnInspectionDescriptionMedicine
            // 
            this.gridColumnInspectionDescriptionMedicine.Caption = "Эм";
            this.gridColumnInspectionDescriptionMedicine.FieldName = "name";
            this.gridColumnInspectionDescriptionMedicine.Name = "gridColumnInspectionDescriptionMedicine";
            this.gridColumnInspectionDescriptionMedicine.Visible = true;
            this.gridColumnInspectionDescriptionMedicine.VisibleIndex = 3;
            this.gridColumnInspectionDescriptionMedicine.Width = 189;
            // 
            // gridColumnInspectionDescriptionDuration
            // 
            this.gridColumnInspectionDescriptionDuration.Caption = "Үргэлжлэх хугацаа";
            this.gridColumnInspectionDescriptionDuration.FieldName = "duration";
            this.gridColumnInspectionDescriptionDuration.Name = "gridColumnInspectionDescriptionDuration";
            this.gridColumnInspectionDescriptionDuration.Visible = true;
            this.gridColumnInspectionDescriptionDuration.VisibleIndex = 2;
            this.gridColumnInspectionDescriptionDuration.Width = 80;
            // 
            // gridColumnInspectionTreatmentEachDay
            // 
            this.gridColumnInspectionTreatmentEachDay.Caption = "Өдөрт уух хэмжээ";
            this.gridColumnInspectionTreatmentEachDay.FieldName = "eachDay";
            this.gridColumnInspectionTreatmentEachDay.Name = "gridColumnInspectionTreatmentEachDay";
            this.gridColumnInspectionTreatmentEachDay.Visible = true;
            this.gridColumnInspectionTreatmentEachDay.VisibleIndex = 4;
            this.gridColumnInspectionTreatmentEachDay.Width = 77;
            // 
            // gridColumnInspectionTreatmentDescription
            // 
            this.gridColumnInspectionTreatmentDescription.Caption = "Тайлбар";
            this.gridColumnInspectionTreatmentDescription.FieldName = "description";
            this.gridColumnInspectionTreatmentDescription.Name = "gridColumnInspectionTreatmentDescription";
            this.gridColumnInspectionTreatmentDescription.Visible = true;
            this.gridColumnInspectionTreatmentDescription.VisibleIndex = 5;
            this.gridColumnInspectionTreatmentDescription.Width = 235;
            // 
            // gridControlStoreInspection
            // 
            this.gridControlStoreInspection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlStoreInspection.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlStoreInspection.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlStoreInspection.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlStoreInspection.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlStoreInspection.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlStoreInspection.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Үзлэг нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Үзлэг устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Үзлэг засах", "edit")});
            this.gridControlStoreInspection.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlStoreInspection_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.LevelTemplate = this.gridViewInspectionTreatment;
            gridLevelNode1.RelationName = "StoreInspection_StoreTreatment";
            this.gridControlStoreInspection.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlStoreInspection.Location = new System.Drawing.Point(0, 0);
            this.gridControlStoreInspection.MainView = this.gridViewStoreInspection;
            this.gridControlStoreInspection.Name = "gridControlStoreInspection";
            this.gridControlStoreInspection.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridControlStoreInspection.Size = new System.Drawing.Size(778, 331);
            this.gridControlStoreInspection.TabIndex = 41;
            this.gridControlStoreInspection.UseEmbeddedNavigator = true;
            this.gridControlStoreInspection.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStoreInspection,
            this.gridViewInspectionTreatment});
            // 
            // gridViewStoreInspection
            // 
            this.gridViewStoreInspection.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnStoreInspectionDate,
            this.gridColumnStoreInspection,
            this.gridColumnStoreInspectionDescription,
            this.gridColumnStoreInspectionDoctorName});
            this.gridViewStoreInspection.GridControl = this.gridControlStoreInspection;
            this.gridViewStoreInspection.Name = "gridViewStoreInspection";
            this.gridViewStoreInspection.OptionsBehavior.ReadOnly = true;
            this.gridViewStoreInspection.OptionsDetail.ShowDetailTabs = false;
            this.gridViewStoreInspection.OptionsNavigation.AutoFocusNewRow = true;
            this.gridViewStoreInspection.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnStoreInspectionDate
            // 
            this.gridColumnStoreInspectionDate.Caption = "Үзлэг хийсэн огноо";
            this.gridColumnStoreInspectionDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnStoreInspectionDate.FieldName = "date";
            this.gridColumnStoreInspectionDate.Name = "gridColumnStoreInspectionDate";
            this.gridColumnStoreInspectionDate.Visible = true;
            this.gridColumnStoreInspectionDate.VisibleIndex = 1;
            // 
            // gridColumnStoreInspection
            // 
            this.gridColumnStoreInspection.Caption = "Үзлэгийн тэмдэглэл";
            this.gridColumnStoreInspection.FieldName = "inspection";
            this.gridColumnStoreInspection.Name = "gridColumnStoreInspection";
            this.gridColumnStoreInspection.Visible = true;
            this.gridColumnStoreInspection.VisibleIndex = 2;
            // 
            // gridColumnStoreInspectionDescription
            // 
            this.gridColumnStoreInspectionDescription.Caption = "Тайлбар";
            this.gridColumnStoreInspectionDescription.FieldName = "description";
            this.gridColumnStoreInspectionDescription.Name = "gridColumnStoreInspectionDescription";
            this.gridColumnStoreInspectionDescription.Visible = true;
            this.gridColumnStoreInspectionDescription.VisibleIndex = 3;
            // 
            // gridColumnStoreInspectionDoctorName
            // 
            this.gridColumnStoreInspectionDoctorName.Caption = "Эмчийн нэр";
            this.gridColumnStoreInspectionDoctorName.CustomizationCaption = "Эмчийн нэр";
            this.gridColumnStoreInspectionDoctorName.FieldName = "fullName";
            this.gridColumnStoreInspectionDoctorName.Name = "gridColumnStoreInspectionDoctorName";
            this.gridColumnStoreInspectionDoctorName.Visible = true;
            this.gridColumnStoreInspectionDoctorName.VisibleIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.treeListLookUpEditWard);
            this.panelControl2.Controls.Add(this.labelControl15);
            this.panelControl2.Controls.Add(this.gridLookUpEditAlergyType);
            this.panelControl2.Controls.Add(this.gridLookUpEditOrder);
            this.panelControl2.Controls.Add(this.labelControl23);
            this.panelControl2.Controls.Add(this.textEditFirstName);
            this.panelControl2.Controls.Add(this.gridLookUpEditPaymentType);
            this.panelControl2.Controls.Add(this.textEditLastName);
            this.panelControl2.Controls.Add(this.labelControl22);
            this.panelControl2.Controls.Add(this.radioGroupLoopState);
            this.panelControl2.Controls.Add(this.labelControl13);
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Controls.Add(this.labelControl21);
            this.panelControl2.Controls.Add(this.labelControl14);
            this.panelControl2.Controls.Add(this.dateEditBirthdate);
            this.panelControl2.Controls.Add(this.radioGroupGender);
            this.panelControl2.Controls.Add(this.checkEditIsUnder);
            this.panelControl2.Controls.Add(this.radioGroupIsMarried);
            this.panelControl2.Controls.Add(this.labelControl17);
            this.panelControl2.Controls.Add(this.labelControl7);
            this.panelControl2.Controls.Add(this.labelControl6);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Controls.Add(this.dateEditBeginDate);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.textEditNumber);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.textEditRegister);
            this.panelControl2.Controls.Add(this.labelControl8);
            this.panelControl2.Controls.Add(this.labelControl9);
            this.panelControl2.Controls.Add(this.textEditEmd);
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(784, 259);
            this.panelControl2.TabIndex = 1;
            // 
            // treeListLookUpEditWard
            // 
            this.treeListLookUpEditWard.Location = new System.Drawing.Point(181, 100);
            this.treeListLookUpEditWard.Name = "treeListLookUpEditWard";
            this.treeListLookUpEditWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditWard.Properties.NullText = "";
            this.treeListLookUpEditWard.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditWard.Properties.ValueMember = "id";
            this.treeListLookUpEditWard.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWard.TabIndex = 4;
            this.treeListLookUpEditWard.EditValueChanged += new System.EventHandler(this.treeListLookUpEditWard_EditValueChanged);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnWard});
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(180, 30);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumnWard
            // 
            this.treeListColumnWard.Caption = "Тасгийн нэрс";
            this.treeListColumnWard.FieldName = "name";
            this.treeListColumnWard.Name = "treeListColumnWard";
            this.treeListColumnWard.Visible = true;
            this.treeListColumnWard.VisibleIndex = 0;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(10, 155);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(165, 13);
            this.labelControl15.TabIndex = 228;
            this.labelControl15.Text = "Доод шатлалаас илгээсэн эсэх*:";
            // 
            // gridLookUpEditAlergyType
            // 
            this.gridLookUpEditAlergyType.EditValue = "";
            this.gridLookUpEditAlergyType.Location = new System.Drawing.Point(181, 177);
            this.gridLookUpEditAlergyType.Name = "gridLookUpEditAlergyType";
            this.gridLookUpEditAlergyType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditAlergyType.Properties.DisplayMember = "name";
            this.gridLookUpEditAlergyType.Properties.NullText = "";
            this.gridLookUpEditAlergyType.Properties.ValueMember = "id";
            this.gridLookUpEditAlergyType.Properties.View = this.gridView5;
            this.gridLookUpEditAlergyType.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditAlergyType.TabIndex = 7;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnAlergyTypeName});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnAlergyTypeName
            // 
            this.gridColumnAlergyTypeName.Caption = "Нэр";
            this.gridColumnAlergyTypeName.CustomizationCaption = "Нэр";
            this.gridColumnAlergyTypeName.FieldName = "name";
            this.gridColumnAlergyTypeName.Name = "gridColumnAlergyTypeName";
            this.gridColumnAlergyTypeName.OptionsColumn.ShowCaption = false;
            this.gridColumnAlergyTypeName.Visible = true;
            this.gridColumnAlergyTypeName.VisibleIndex = 0;
            // 
            // gridLookUpEditOrder
            // 
            this.gridLookUpEditOrder.EditValue = "";
            this.gridLookUpEditOrder.Location = new System.Drawing.Point(181, 48);
            this.gridLookUpEditOrder.Name = "gridLookUpEditOrder";
            this.gridLookUpEditOrder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditOrder.Properties.DisplayMember = "number";
            this.gridLookUpEditOrder.Properties.NullText = "";
            this.gridLookUpEditOrder.Properties.ValueMember = "id";
            this.gridLookUpEditOrder.Properties.View = this.gridView4;
            this.gridLookUpEditOrder.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditOrder.TabIndex = 2;
            this.gridLookUpEditOrder.EditValueChanged += new System.EventHandler(this.gridLookUpEditOrder_EditValueChanged);
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn23,
            this.gridColumnOrderName,
            this.gridColumnOrderRegister,
            this.gridColumn16,
            this.gridColumn17});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Хүлээлгиийн дугаар";
            this.gridColumn23.CustomizationCaption = "Хүлээлгиийн дугаар";
            this.gridColumn23.FieldName = "number";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 0;
            this.gridColumn23.Width = 123;
            // 
            // gridColumnOrderName
            // 
            this.gridColumnOrderName.Caption = "Нэр";
            this.gridColumnOrderName.CustomizationCaption = "Нэр";
            this.gridColumnOrderName.FieldName = "firstName";
            this.gridColumnOrderName.Name = "gridColumnOrderName";
            this.gridColumnOrderName.Visible = true;
            this.gridColumnOrderName.VisibleIndex = 2;
            this.gridColumnOrderName.Width = 142;
            // 
            // gridColumnOrderRegister
            // 
            this.gridColumnOrderRegister.Caption = "Регистер";
            this.gridColumnOrderRegister.CustomizationCaption = "Регистер";
            this.gridColumnOrderRegister.FieldName = "userID";
            this.gridColumnOrderRegister.Name = "gridColumnOrderRegister";
            this.gridColumnOrderRegister.Visible = true;
            this.gridColumnOrderRegister.VisibleIndex = 3;
            this.gridColumnOrderRegister.Width = 142;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Овог";
            this.gridColumn16.CustomizationCaption = "Овог";
            this.gridColumn16.FieldName = "lastName";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 1;
            this.gridColumn16.Width = 142;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Огноо";
            this.gridColumn17.CustomizationCaption = "Огноо";
            this.gridColumn17.FieldName = "date";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 4;
            this.gridColumn17.Width = 147;
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(78, 129);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(97, 13);
            this.labelControl23.TabIndex = 141;
            this.labelControl23.Text = "Төлбөрийн төрөл*:";
            // 
            // textEditFirstName
            // 
            this.textEditFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditFirstName.Location = new System.Drawing.Point(543, 74);
            this.textEditFirstName.Name = "textEditFirstName";
            this.textEditFirstName.Properties.ReadOnly = true;
            this.textEditFirstName.Size = new System.Drawing.Size(200, 20);
            this.textEditFirstName.TabIndex = 12;
            // 
            // gridLookUpEditPaymentType
            // 
            this.gridLookUpEditPaymentType.EditValue = "";
            this.gridLookUpEditPaymentType.Location = new System.Drawing.Point(181, 126);
            this.gridLookUpEditPaymentType.Name = "gridLookUpEditPaymentType";
            this.gridLookUpEditPaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPaymentType.Properties.DisplayMember = "name";
            this.gridLookUpEditPaymentType.Properties.NullText = "";
            this.gridLookUpEditPaymentType.Properties.ValueMember = "id";
            this.gridLookUpEditPaymentType.Properties.View = this.gridView1;
            this.gridLookUpEditPaymentType.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditPaymentType.TabIndex = 5;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnPaymentTypeName});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnPaymentTypeName
            // 
            this.gridColumnPaymentTypeName.Caption = "Нэр";
            this.gridColumnPaymentTypeName.CustomizationCaption = "Нэр";
            this.gridColumnPaymentTypeName.FieldName = "name";
            this.gridColumnPaymentTypeName.Name = "gridColumnPaymentTypeName";
            this.gridColumnPaymentTypeName.OptionsColumn.ShowCaption = false;
            this.gridColumnPaymentTypeName.Visible = true;
            this.gridColumnPaymentTypeName.VisibleIndex = 0;
            // 
            // textEditLastName
            // 
            this.textEditLastName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditLastName.Location = new System.Drawing.Point(543, 48);
            this.textEditLastName.Name = "textEditLastName";
            this.textEditLastName.Properties.ReadOnly = true;
            this.textEditLastName.Size = new System.Drawing.Size(200, 20);
            this.textEditLastName.TabIndex = 11;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(125, 208);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(50, 13);
            this.labelControl22.TabIndex = 139;
            this.labelControl22.Text = "Хэвтэлт*:";
            // 
            // radioGroupLoopState
            // 
            this.radioGroupLoopState.EditValue = false;
            this.radioGroupLoopState.Location = new System.Drawing.Point(181, 203);
            this.radioGroupLoopState.Name = "radioGroupLoopState";
            this.radioGroupLoopState.Properties.Columns = 2;
            this.radioGroupLoopState.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Давтан", true, true),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Анх удаа", true, false)});
            this.radioGroupLoopState.Size = new System.Drawing.Size(200, 24);
            this.radioGroupLoopState.TabIndex = 8;
            // 
            // labelControl13
            // 
            this.labelControl13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl13.Location = new System.Drawing.Point(511, 183);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(26, 13);
            this.labelControl13.TabIndex = 227;
            this.labelControl13.Text = "Хүйс:";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Location = new System.Drawing.Point(466, 103);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(71, 13);
            this.labelControl4.TabIndex = 209;
            this.labelControl4.Text = "Төрсөн огноо:";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(75, 180);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(100, 13);
            this.labelControl21.TabIndex = 137;
            this.labelControl21.Text = "Харшлын анамнез*:";
            // 
            // labelControl14
            // 
            this.labelControl14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl14.Location = new System.Drawing.Point(462, 212);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(75, 13);
            this.labelControl14.TabIndex = 221;
            this.labelControl14.Text = "Гэрлэсэн эсэх :";
            // 
            // dateEditBirthdate
            // 
            this.dateEditBirthdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditBirthdate.EditValue = null;
            this.dateEditBirthdate.Location = new System.Drawing.Point(543, 100);
            this.dateEditBirthdate.Name = "dateEditBirthdate";
            this.dateEditBirthdate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBirthdate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBirthdate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBirthdate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBirthdate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBirthdate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBirthdate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditBirthdate.Properties.ReadOnly = true;
            this.dateEditBirthdate.Size = new System.Drawing.Size(100, 20);
            this.dateEditBirthdate.TabIndex = 13;
            // 
            // radioGroupGender
            // 
            this.radioGroupGender.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupGender.Enabled = false;
            this.radioGroupGender.Location = new System.Drawing.Point(543, 178);
            this.radioGroupGender.Name = "radioGroupGender";
            this.radioGroupGender.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Эр", true, "Эр"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эм", true, "Эм")});
            this.radioGroupGender.Size = new System.Drawing.Size(100, 24);
            this.radioGroupGender.TabIndex = 16;
            // 
            // checkEditIsUnder
            // 
            this.checkEditIsUnder.Location = new System.Drawing.Point(184, 152);
            this.checkEditIsUnder.Name = "checkEditIsUnder";
            this.checkEditIsUnder.Properties.Caption = "";
            this.checkEditIsUnder.Size = new System.Drawing.Size(32, 19);
            this.checkEditIsUnder.TabIndex = 6;
            // 
            // radioGroupIsMarried
            // 
            this.radioGroupIsMarried.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupIsMarried.Enabled = false;
            this.radioGroupIsMarried.Location = new System.Drawing.Point(543, 208);
            this.radioGroupIsMarried.Name = "radioGroupIsMarried";
            this.radioGroupIsMarried.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм", true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй", true, " Үгүй")});
            this.radioGroupIsMarried.Size = new System.Drawing.Size(100, 24);
            this.radioGroupIsMarried.TabIndex = 17;
            // 
            // labelControl17
            // 
            this.labelControl17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl17.Location = new System.Drawing.Point(254, 15);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(109, 16);
            this.labelControl17.TabIndex = 126;
            this.labelControl17.Text = "ӨВЧНИЙ ТҮҮХ №";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(137, 103);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(38, 13);
            this.labelControl7.TabIndex = 112;
            this.labelControl7.Text = "Тасаг*:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(100, 77);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(75, 13);
            this.labelControl6.TabIndex = 111;
            this.labelControl6.Text = "Нээсэн огноо*:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(64, 51);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(117, 13);
            this.labelControl5.TabIndex = 110;
            this.labelControl5.Text = "Хүлээлгийн жагсаалт*:";
            // 
            // dateEditBeginDate
            // 
            this.dateEditBeginDate.EditValue = null;
            this.dateEditBeginDate.Location = new System.Drawing.Point(181, 74);
            this.dateEditBeginDate.Name = "dateEditBeginDate";
            this.dateEditBeginDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBeginDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBeginDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBeginDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBeginDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBeginDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBeginDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditBeginDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditBeginDate.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Location = new System.Drawing.Point(508, 51);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(29, 13);
            this.labelControl2.TabIndex = 207;
            this.labelControl2.Text = "Овог:";
            // 
            // textEditNumber
            // 
            this.textEditNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditNumber.Location = new System.Drawing.Point(369, 14);
            this.textEditNumber.Name = "textEditNumber";
            this.textEditNumber.Properties.Mask.EditMask = "d";
            this.textEditNumber.Properties.MaxLength = 18;
            this.textEditNumber.Size = new System.Drawing.Size(120, 20);
            this.textEditNumber.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Location = new System.Drawing.Point(515, 77);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(22, 13);
            this.labelControl1.TabIndex = 206;
            this.labelControl1.Text = "Нэр:";
            // 
            // textEditRegister
            // 
            this.textEditRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditRegister.Location = new System.Drawing.Point(543, 126);
            this.textEditRegister.Name = "textEditRegister";
            this.textEditRegister.Properties.ReadOnly = true;
            this.textEditRegister.Size = new System.Drawing.Size(200, 20);
            this.textEditRegister.TabIndex = 14;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl8.Location = new System.Drawing.Point(487, 129);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(50, 13);
            this.labelControl8.TabIndex = 210;
            this.labelControl8.Text = "Регистер:";
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl9.Location = new System.Drawing.Point(507, 155);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(30, 13);
            this.labelControl9.TabIndex = 217;
            this.labelControl9.Text = "ЭМД :";
            // 
            // textEditEmd
            // 
            this.textEditEmd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditEmd.Location = new System.Drawing.Point(543, 152);
            this.textEditEmd.Name = "textEditEmd";
            this.textEditEmd.Properties.ReadOnly = true;
            this.textEditEmd.Size = new System.Drawing.Size(200, 20);
            this.textEditEmd.TabIndex = 15;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonReload);
            this.panelControl1.Controls.Add(this.simpleButtonCancelStore);
            this.panelControl1.Controls.Add(this.simpleButtonSaveStore);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 618);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 43);
            this.panelControl1.TabIndex = 170;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(11, 10);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 93;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // simpleButtonCancelStore
            // 
            this.simpleButtonCancelStore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancelStore.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancelStore.Location = new System.Drawing.Point(692, 10);
            this.simpleButtonCancelStore.Name = "simpleButtonCancelStore";
            this.simpleButtonCancelStore.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancelStore.TabIndex = 92;
            this.simpleButtonCancelStore.Text = "Болих";
            this.simpleButtonCancelStore.ToolTip = "Болих";
            // 
            // simpleButtonSaveStore
            // 
            this.simpleButtonSaveStore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSaveStore.Location = new System.Drawing.Point(601, 10);
            this.simpleButtonSaveStore.Name = "simpleButtonSaveStore";
            this.simpleButtonSaveStore.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSaveStore.TabIndex = 91;
            this.simpleButtonSaveStore.Text = "Хадгалах";
            this.simpleButtonSaveStore.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSaveStore.Click += new System.EventHandler(this.simpleButtonSaveStore_Click);
            // 
            // xtraTabControl
            // 
            this.xtraTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl.Location = new System.Drawing.Point(0, 259);
            this.xtraTabControl.Name = "xtraTabControl";
            this.xtraTabControl.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl.Size = new System.Drawing.Size(784, 359);
            this.xtraTabControl.TabIndex = 169;
            this.xtraTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage4,
            this.xtraTabPage3,
            this.xtraTabPage6,
            this.xtraTabPage5});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.textEditEducation);
            this.xtraTabPage1.Controls.Add(this.textEditProvince);
            this.xtraTabPage1.Controls.Add(this.textEditSpecialty);
            this.xtraTabPage1.Controls.Add(this.textEditPosition);
            this.xtraTabPage1.Controls.Add(this.textEditOrganization);
            this.xtraTabPage1.Controls.Add(this.memoEditBeginDiagnose);
            this.xtraTabPage1.Controls.Add(this.labelControl3);
            this.xtraTabPage1.Controls.Add(this.labelControl10);
            this.xtraTabPage1.Controls.Add(this.memoEditAddress);
            this.xtraTabPage1.Controls.Add(this.labelControl28);
            this.xtraTabPage1.Controls.Add(this.labelControl27);
            this.xtraTabPage1.Controls.Add(this.labelControl24);
            this.xtraTabPage1.Controls.Add(this.labelControl19);
            this.xtraTabPage1.Controls.Add(this.labelControl18);
            this.xtraTabPage1.Controls.Add(this.textEditEmergencyPhone);
            this.xtraTabPage1.Controls.Add(this.labelControl12);
            this.xtraTabPage1.Controls.Add(this.labelControl20);
            this.xtraTabPage1.Controls.Add(this.textEditEmergencyName);
            this.xtraTabPage1.Controls.Add(this.labelControl16);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(778, 331);
            this.xtraTabPage1.Text = "Хэвтэх үеийн онош";
            // 
            // textEditEducation
            // 
            this.textEditEducation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditEducation.Location = new System.Drawing.Point(542, 113);
            this.textEditEducation.Name = "textEditEducation";
            this.textEditEducation.Properties.ReadOnly = true;
            this.textEditEducation.Size = new System.Drawing.Size(200, 20);
            this.textEditEducation.TabIndex = 27;
            // 
            // textEditProvince
            // 
            this.textEditProvince.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditProvince.Location = new System.Drawing.Point(542, 139);
            this.textEditProvince.Name = "textEditProvince";
            this.textEditProvince.Properties.ReadOnly = true;
            this.textEditProvince.Size = new System.Drawing.Size(200, 20);
            this.textEditProvince.TabIndex = 28;
            // 
            // textEditSpecialty
            // 
            this.textEditSpecialty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditSpecialty.Location = new System.Drawing.Point(542, 87);
            this.textEditSpecialty.Name = "textEditSpecialty";
            this.textEditSpecialty.Properties.ReadOnly = true;
            this.textEditSpecialty.Size = new System.Drawing.Size(200, 20);
            this.textEditSpecialty.TabIndex = 26;
            // 
            // textEditPosition
            // 
            this.textEditPosition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditPosition.Location = new System.Drawing.Point(542, 61);
            this.textEditPosition.Name = "textEditPosition";
            this.textEditPosition.Properties.ReadOnly = true;
            this.textEditPosition.Size = new System.Drawing.Size(200, 20);
            this.textEditPosition.TabIndex = 25;
            // 
            // textEditOrganization
            // 
            this.textEditOrganization.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditOrganization.Location = new System.Drawing.Point(542, 35);
            this.textEditOrganization.Name = "textEditOrganization";
            this.textEditOrganization.Properties.ReadOnly = true;
            this.textEditOrganization.Size = new System.Drawing.Size(200, 20);
            this.textEditOrganization.TabIndex = 24;
            // 
            // memoEditBeginDiagnose
            // 
            this.memoEditBeginDiagnose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.memoEditBeginDiagnose.Location = new System.Drawing.Point(27, 116);
            this.memoEditBeginDiagnose.Name = "memoEditBeginDiagnose";
            this.memoEditBeginDiagnose.Properties.MaxLength = 200;
            this.memoEditBeginDiagnose.Size = new System.Drawing.Size(353, 203);
            this.memoEditBeginDiagnose.TabIndex = 23;
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Location = new System.Drawing.Point(476, 116);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(60, 13);
            this.labelControl3.TabIndex = 228;
            this.labelControl3.Text = "Боловсрол :";
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl10.Location = new System.Drawing.Point(480, 90);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(56, 13);
            this.labelControl10.TabIndex = 240;
            this.labelControl10.Text = "Мэргэжил :";
            // 
            // memoEditAddress
            // 
            this.memoEditAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditAddress.Location = new System.Drawing.Point(455, 165);
            this.memoEditAddress.Name = "memoEditAddress";
            this.memoEditAddress.Properties.ReadOnly = true;
            this.memoEditAddress.Size = new System.Drawing.Size(287, 154);
            this.memoEditAddress.TabIndex = 29;
            // 
            // labelControl28
            // 
            this.labelControl28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl28.Location = new System.Drawing.Point(457, 64);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(79, 13);
            this.labelControl28.TabIndex = 232;
            this.labelControl28.Text = "Албан тушаал :";
            // 
            // labelControl27
            // 
            this.labelControl27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl27.Location = new System.Drawing.Point(463, 38);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(73, 13);
            this.labelControl27.TabIndex = 231;
            this.labelControl27.Text = "Ажлын газар :";
            // 
            // labelControl24
            // 
            this.labelControl24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl24.Location = new System.Drawing.Point(455, 142);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(81, 13);
            this.labelControl24.TabIndex = 228;
            this.labelControl24.Text = "Хаяг, байршил :";
            // 
            // labelControl19
            // 
            this.labelControl19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl19.Location = new System.Drawing.Point(455, 11);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(160, 13);
            this.labelControl19.TabIndex = 225;
            this.labelControl19.Text = "Эмчлүүлэгчийн нэмэлт мэдээлэл";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(27, 11);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(142, 13);
            this.labelControl18.TabIndex = 224;
            this.labelControl18.Text = "Яаралтай үед холбоо барих";
            // 
            // textEditEmergencyPhone
            // 
            this.textEditEmergencyPhone.Location = new System.Drawing.Point(181, 61);
            this.textEditEmergencyPhone.Name = "textEditEmergencyPhone";
            this.textEditEmergencyPhone.Properties.MaxLength = 50;
            this.textEditEmergencyPhone.Size = new System.Drawing.Size(200, 20);
            this.textEditEmergencyPhone.TabIndex = 22;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(141, 64);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(34, 13);
            this.labelControl12.TabIndex = 220;
            this.labelControl12.Text = "Утас*:";
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(27, 90);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(105, 13);
            this.labelControl20.TabIndex = 134;
            this.labelControl20.Text = "Хэвтэх үеийн онош*:";
            // 
            // textEditEmergencyName
            // 
            this.textEditEmergencyName.Location = new System.Drawing.Point(182, 35);
            this.textEditEmergencyName.Name = "textEditEmergencyName";
            this.textEditEmergencyName.Properties.MaxLength = 50;
            this.textEditEmergencyName.Size = new System.Drawing.Size(200, 20);
            this.textEditEmergencyName.TabIndex = 21;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(114, 38);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(62, 13);
            this.labelControl16.TabIndex = 223;
            this.labelControl16.Text = "Хүний нэр *:";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControlWardRoom);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(778, 331);
            this.xtraTabPage2.Text = "Эмчлүүлэгчийн тасаг өрөө";
            // 
            // gridControlWardRoom
            // 
            this.gridControlWardRoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlWardRoom.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlWardRoom.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlWardRoom.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlWardRoom.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlWardRoom.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlWardRoom.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "edit")});
            this.gridControlWardRoom.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlPatientInform_EmbeddedNavigator_ButtonClick);
            this.gridControlWardRoom.Location = new System.Drawing.Point(0, 0);
            this.gridControlWardRoom.MainView = this.gridViewWardRoom;
            this.gridControlWardRoom.Name = "gridControlWardRoom";
            this.gridControlWardRoom.Size = new System.Drawing.Size(778, 331);
            this.gridControlWardRoom.TabIndex = 52;
            this.gridControlWardRoom.UseEmbeddedNavigator = true;
            this.gridControlWardRoom.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWardRoom});
            // 
            // gridViewWardRoom
            // 
            this.gridViewWardRoom.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridViewWardRoom.GridControl = this.gridControlWardRoom;
            this.gridViewWardRoom.Name = "gridViewWardRoom";
            this.gridViewWardRoom.OptionsBehavior.ReadOnly = true;
            this.gridViewWardRoom.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Эхэлсэн хугацаа";
            this.gridColumn4.FieldName = "startDate";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Дууссан хугацаа";
            this.gridColumn5.FieldName = "endDate";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тасгийн нэр";
            this.gridColumn6.FieldName = "wardName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Өрөө, ор";
            this.gridColumn7.FieldName = "bedName";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Тайлбар";
            this.gridColumn8.FieldName = "description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 4;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Одоо байгаа эсэх";
            this.gridColumn9.FieldName = "isCur";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridControlStoreDiagnose);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(778, 331);
            this.xtraTabPage4.Text = "Клиникийн урьдчилсан онош";
            // 
            // gridControlStoreDiagnose
            // 
            this.gridControlStoreDiagnose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlStoreDiagnose.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlStoreDiagnose.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlStoreDiagnose.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlStoreDiagnose.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlStoreDiagnose.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "edit")});
            this.gridControlStoreDiagnose.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlStoreDiagnose_EmbeddedNavigator_ButtonClick);
            this.gridControlStoreDiagnose.Location = new System.Drawing.Point(0, 0);
            this.gridControlStoreDiagnose.MainView = this.gridViewStoreDiagnose;
            this.gridControlStoreDiagnose.Name = "gridControlStoreDiagnose";
            this.gridControlStoreDiagnose.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit2});
            this.gridControlStoreDiagnose.Size = new System.Drawing.Size(778, 331);
            this.gridControlStoreDiagnose.TabIndex = 32;
            this.gridControlStoreDiagnose.UseEmbeddedNavigator = true;
            this.gridControlStoreDiagnose.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStoreDiagnose,
            this.gridView6});
            // 
            // gridViewStoreDiagnose
            // 
            this.gridViewStoreDiagnose.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnStoreDiagnoseDate,
            this.gridColumnStoreDiagnose,
            this.gridColumnStoreDiagnoseDescription,
            this.gridColumnStoreDiagnoseTypeName,
            this.gridColumnStoreDiagnoseDoctorName});
            this.gridViewStoreDiagnose.GridControl = this.gridControlStoreDiagnose;
            this.gridViewStoreDiagnose.Name = "gridViewStoreDiagnose";
            this.gridViewStoreDiagnose.OptionsBehavior.ReadOnly = true;
            this.gridViewStoreDiagnose.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnStoreDiagnoseDate
            // 
            this.gridColumnStoreDiagnoseDate.Caption = "Огноо";
            this.gridColumnStoreDiagnoseDate.ColumnEdit = this.repositoryItemDateEdit2;
            this.gridColumnStoreDiagnoseDate.FieldName = "date";
            this.gridColumnStoreDiagnoseDate.Name = "gridColumnStoreDiagnoseDate";
            this.gridColumnStoreDiagnoseDate.Visible = true;
            this.gridColumnStoreDiagnoseDate.VisibleIndex = 1;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit2.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // gridColumnStoreDiagnose
            // 
            this.gridColumnStoreDiagnose.Caption = "Онош";
            this.gridColumnStoreDiagnose.FieldName = "diagnose";
            this.gridColumnStoreDiagnose.Name = "gridColumnStoreDiagnose";
            this.gridColumnStoreDiagnose.Visible = true;
            this.gridColumnStoreDiagnose.VisibleIndex = 3;
            // 
            // gridColumnStoreDiagnoseDescription
            // 
            this.gridColumnStoreDiagnoseDescription.Caption = "Тайлбар";
            this.gridColumnStoreDiagnoseDescription.FieldName = "description";
            this.gridColumnStoreDiagnoseDescription.Name = "gridColumnStoreDiagnoseDescription";
            this.gridColumnStoreDiagnoseDescription.Visible = true;
            this.gridColumnStoreDiagnoseDescription.VisibleIndex = 4;
            // 
            // gridColumnStoreDiagnoseTypeName
            // 
            this.gridColumnStoreDiagnoseTypeName.Caption = "Оношийн ангилал";
            this.gridColumnStoreDiagnoseTypeName.FieldName = "diagnoseTypeName";
            this.gridColumnStoreDiagnoseTypeName.Name = "gridColumnStoreDiagnoseTypeName";
            this.gridColumnStoreDiagnoseTypeName.Visible = true;
            this.gridColumnStoreDiagnoseTypeName.VisibleIndex = 2;
            // 
            // gridColumnStoreDiagnoseDoctorName
            // 
            this.gridColumnStoreDiagnoseDoctorName.Caption = "Эмчийн нэр";
            this.gridColumnStoreDiagnoseDoctorName.CustomizationCaption = "Эмчийн нэр";
            this.gridColumnStoreDiagnoseDoctorName.FieldName = "fullName";
            this.gridColumnStoreDiagnoseDoctorName.Name = "gridColumnStoreDiagnoseDoctorName";
            this.gridColumnStoreDiagnoseDoctorName.Visible = true;
            this.gridColumnStoreDiagnoseDoctorName.VisibleIndex = 0;
            // 
            // gridView6
            // 
            this.gridView6.GridControl = this.gridControlStoreDiagnose;
            this.gridView6.Name = "gridView6";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControlStoreInspection);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(778, 331);
            this.xtraTabPage3.Text = "Эмчийн үзлэг, эмчилгээ";
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.xtraTabControlAnamnez);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(778, 331);
            this.xtraTabPage6.Text = "Эмчлүүлэгчийн анамнез";
            // 
            // xtraTabControlAnamnez
            // 
            this.xtraTabControlAnamnez.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlAnamnez.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControlAnamnez.Name = "xtraTabControlAnamnez";
            this.xtraTabControlAnamnez.SelectedTabPage = this.xtraTabPageAnamnez1;
            this.xtraTabControlAnamnez.Size = new System.Drawing.Size(778, 331);
            this.xtraTabControlAnamnez.TabIndex = 0;
            this.xtraTabControlAnamnez.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageAnamnez1,
            this.xtraTabPageAnamnez2,
            this.xtraTabPageAnamnez3,
            this.xtraTabPageAnamnez4,
            this.xtraTabPageAnamnez5,
            this.xtraTabPageAnamnez6,
            this.xtraTabPageAnamnez7});
            // 
            // xtraTabPageAnamnez1
            // 
            this.xtraTabPageAnamnez1.Controls.Add(this.checkEditStore);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl66);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl65);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl64);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl63);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl62);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl61);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl60);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl59);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl58);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl57);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl56);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl55);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl54);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl53);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl52);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl51);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl50);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl49);
            this.xtraTabPageAnamnez1.Controls.Add(this.labelControl48);
            this.xtraTabPageAnamnez1.Controls.Add(this.memoEditPain);
            this.xtraTabPageAnamnez1.Controls.Add(this.memoEditHistory);
            this.xtraTabPageAnamnez1.Controls.Add(this.memoEditLifeStore);
            this.xtraTabPageAnamnez1.Controls.Add(this.spinEditHigh);
            this.xtraTabPageAnamnez1.Controls.Add(this.spinEditWeight);
            this.xtraTabPageAnamnez1.Controls.Add(this.spinEditBodyIndex);
            this.xtraTabPageAnamnez1.Controls.Add(this.radioGroupBloodPressure);
            this.xtraTabPageAnamnez1.Controls.Add(this.gridLookUpEditLifeTerm);
            this.xtraTabPageAnamnez1.Controls.Add(this.gridLookUpEditWorkTerm);
            this.xtraTabPageAnamnez1.Controls.Add(this.gridLookUpEditInfection);
            this.xtraTabPageAnamnez1.Controls.Add(this.checkEditDoesSurgery);
            this.xtraTabPageAnamnez1.Controls.Add(this.memoEditAccident);
            this.xtraTabPageAnamnez1.Controls.Add(this.gridLookUpEditAllergy);
            this.xtraTabPageAnamnez1.Controls.Add(this.radioGroupIsVodka);
            this.xtraTabPageAnamnez1.Controls.Add(this.RadioGroupIsSmoke);
            this.xtraTabPageAnamnez1.Controls.Add(this.memoEditHowLong);
            this.xtraTabPageAnamnez1.Controls.Add(this.gridLookUpEditDiet);
            this.xtraTabPageAnamnez1.Controls.Add(this.memoEditInheritance);
            this.xtraTabPageAnamnez1.Name = "xtraTabPageAnamnez1";
            this.xtraTabPageAnamnez1.Size = new System.Drawing.Size(772, 303);
            this.xtraTabPageAnamnez1.Text = "Анамнез";
            // 
            // checkEditStore
            // 
            this.checkEditStore.Location = new System.Drawing.Point(8, 8);
            this.checkEditStore.Name = "checkEditStore";
            this.checkEditStore.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.checkEditStore.Properties.Caption = "Бөглөх";
            this.checkEditStore.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditStore.Size = new System.Drawing.Size(61, 21);
            this.checkEditStore.TabIndex = 37;
            this.checkEditStore.CheckedChanged += new System.EventHandler(this.checkEditStore_CheckedChanged);
            // 
            // labelControl66
            // 
            this.labelControl66.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl66.Location = new System.Drawing.Point(342, 236);
            this.labelControl66.Name = "labelControl66";
            this.labelControl66.Size = new System.Drawing.Size(111, 13);
            this.labelControl66.TabIndex = 36;
            this.labelControl66.Text = "Архи хэрэглэдэг эсэх:";
            // 
            // labelControl65
            // 
            this.labelControl65.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl65.Location = new System.Drawing.Point(585, 236);
            this.labelControl65.Name = "labelControl65";
            this.labelControl65.Size = new System.Drawing.Size(147, 26);
            this.labelControl65.TabIndex = 35;
            this.labelControl65.Text = "Хэдэн наснаас эхэлж татсан/ \r\nХэдэн жил татаж байгаа";
            // 
            // labelControl64
            // 
            this.labelControl64.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl64.Location = new System.Drawing.Point(467, 236);
            this.labelControl64.Name = "labelControl64";
            this.labelControl64.Size = new System.Drawing.Size(97, 13);
            this.labelControl64.TabIndex = 34;
            this.labelControl64.Text = "Тамхи татдаг эсэх:";
            // 
            // labelControl63
            // 
            this.labelControl63.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl63.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelControl63.Location = new System.Drawing.Point(464, 171);
            this.labelControl63.Name = "labelControl63";
            this.labelControl63.Size = new System.Drawing.Size(106, 13);
            this.labelControl63.TabIndex = 24;
            this.labelControl63.Text = "Хооллолтын байдал:";
            // 
            // labelControl62
            // 
            this.labelControl62.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl62.Location = new System.Drawing.Point(585, 171);
            this.labelControl62.Name = "labelControl62";
            this.labelControl62.Size = new System.Drawing.Size(102, 13);
            this.labelControl62.TabIndex = 23;
            this.labelControl62.Text = "Удамшлын анамнез:";
            // 
            // labelControl61
            // 
            this.labelControl61.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl61.Location = new System.Drawing.Point(342, 171);
            this.labelControl61.Name = "labelControl61";
            this.labelControl61.Size = new System.Drawing.Size(94, 13);
            this.labelControl61.TabIndex = 22;
            this.labelControl61.Text = "Харшлын анамнез:";
            // 
            // labelControl60
            // 
            this.labelControl60.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl60.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelControl60.Location = new System.Drawing.Point(585, 93);
            this.labelControl60.Name = "labelControl60";
            this.labelControl60.Size = new System.Drawing.Size(120, 26);
            this.labelControl60.TabIndex = 21;
            this.labelControl60.Text = "Осол гэмтэл, хордлого, \r\nшалтгаан";
            // 
            // labelControl59
            // 
            this.labelControl59.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl59.Location = new System.Drawing.Point(440, 106);
            this.labelControl59.Name = "labelControl59";
            this.labelControl59.Size = new System.Drawing.Size(130, 13);
            this.labelControl59.TabIndex = 20;
            this.labelControl59.Text = "Мэс ажилбар хийсэн эсэх:";
            // 
            // labelControl58
            // 
            this.labelControl58.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl58.Location = new System.Drawing.Point(342, 106);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(53, 13);
            this.labelControl58.TabIndex = 19;
            this.labelControl58.Text = "Халдварт:";
            // 
            // labelControl57
            // 
            this.labelControl57.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl57.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelControl57.Location = new System.Drawing.Point(342, 87);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(204, 13);
            this.labelControl57.TabIndex = 18;
            this.labelControl57.Text = "Урьд өвчилсөн өвчин, эмгэгийн байдал:";
            // 
            // labelControl56
            // 
            this.labelControl56.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl56.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelControl56.Location = new System.Drawing.Point(440, 39);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(140, 13);
            this.labelControl56.TabIndex = 17;
            this.labelControl56.Text = "Ажил хөдөлмөрийн нөхцөл:";
            // 
            // labelControl55
            // 
            this.labelControl55.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl55.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.labelControl55.Location = new System.Drawing.Point(342, 39);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(74, 13);
            this.labelControl55.TabIndex = 14;
            this.labelControl55.Text = "Ахуйн нөхцөл:";
            // 
            // labelControl54
            // 
            this.labelControl54.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl54.Location = new System.Drawing.Point(632, 23);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(109, 13);
            this.labelControl54.TabIndex = 12;
            this.labelControl54.Text = "Цусны даралт(ммуб):";
            // 
            // labelControl53
            // 
            this.labelControl53.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl53.Location = new System.Drawing.Point(375, 16);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(160, 13);
            this.labelControl53.TabIndex = 11;
            this.labelControl53.Text = "Биеийн жингийн индекс(кг/м2):\r\n";
            // 
            // labelControl52
            // 
            this.labelControl52.Location = new System.Drawing.Point(234, 16);
            this.labelControl52.Name = "labelControl52";
            this.labelControl52.Size = new System.Drawing.Size(45, 13);
            this.labelControl52.TabIndex = 10;
            this.labelControl52.Text = "Жин(кг):";
            // 
            // labelControl51
            // 
            this.labelControl51.Location = new System.Drawing.Point(87, 16);
            this.labelControl51.Name = "labelControl51";
            this.labelControl51.Size = new System.Drawing.Size(56, 13);
            this.labelControl51.TabIndex = 9;
            this.labelControl51.Text = "Өндөр(см):";
            // 
            // labelControl50
            // 
            this.labelControl50.Location = new System.Drawing.Point(22, 214);
            this.labelControl50.Name = "labelControl50";
            this.labelControl50.Size = new System.Drawing.Size(59, 26);
            this.labelControl50.TabIndex = 2;
            this.labelControl50.Text = "Амьдралын \r\n           түүх:";
            // 
            // labelControl49
            // 
            this.labelControl49.Location = new System.Drawing.Point(14, 128);
            this.labelControl49.Name = "labelControl49";
            this.labelControl49.Size = new System.Drawing.Size(67, 13);
            this.labelControl49.TabIndex = 1;
            this.labelControl49.Text = "Өвчний түүх:";
            // 
            // labelControl48
            // 
            this.labelControl48.Location = new System.Drawing.Point(15, 42);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(66, 26);
            this.labelControl48.TabIndex = 0;
            this.labelControl48.Text = "Хэвтэх үеийн \r\nзовиур:";
            // 
            // memoEditPain
            // 
            this.memoEditPain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditPain.Location = new System.Drawing.Point(87, 39);
            this.memoEditPain.Name = "memoEditPain";
            this.memoEditPain.Properties.MaxLength = 200;
            this.memoEditPain.Size = new System.Drawing.Size(222, 80);
            this.memoEditPain.TabIndex = 3;
            // 
            // memoEditHistory
            // 
            this.memoEditHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditHistory.Location = new System.Drawing.Point(87, 125);
            this.memoEditHistory.Name = "memoEditHistory";
            this.memoEditHistory.Properties.MaxLength = 200;
            this.memoEditHistory.Size = new System.Drawing.Size(222, 80);
            this.memoEditHistory.TabIndex = 4;
            // 
            // memoEditLifeStore
            // 
            this.memoEditLifeStore.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditLifeStore.Location = new System.Drawing.Point(87, 211);
            this.memoEditLifeStore.Name = "memoEditLifeStore";
            this.memoEditLifeStore.Properties.MaxLength = 200;
            this.memoEditLifeStore.Size = new System.Drawing.Size(223, 80);
            this.memoEditLifeStore.TabIndex = 5;
            // 
            // spinEditHigh
            // 
            this.spinEditHigh.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditHigh.Location = new System.Drawing.Point(144, 13);
            this.spinEditHigh.Name = "spinEditHigh";
            this.spinEditHigh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditHigh.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditHigh.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spinEditHigh.Size = new System.Drawing.Size(80, 20);
            this.spinEditHigh.TabIndex = 6;
            // 
            // spinEditWeight
            // 
            this.spinEditWeight.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditWeight.Location = new System.Drawing.Point(281, 13);
            this.spinEditWeight.Name = "spinEditWeight";
            this.spinEditWeight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditWeight.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditWeight.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spinEditWeight.Size = new System.Drawing.Size(80, 20);
            this.spinEditWeight.TabIndex = 7;
            // 
            // spinEditBodyIndex
            // 
            this.spinEditBodyIndex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spinEditBodyIndex.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditBodyIndex.Location = new System.Drawing.Point(537, 13);
            this.spinEditBodyIndex.Name = "spinEditBodyIndex";
            this.spinEditBodyIndex.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditBodyIndex.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditBodyIndex.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spinEditBodyIndex.Size = new System.Drawing.Size(80, 20);
            this.spinEditBodyIndex.TabIndex = 8;
            // 
            // radioGroupBloodPressure
            // 
            this.radioGroupBloodPressure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupBloodPressure.Location = new System.Drawing.Point(611, 42);
            this.radioGroupBloodPressure.Name = "radioGroupBloodPressure";
            this.radioGroupBloodPressure.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Систолын даралт"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Диастолын даралт")});
            this.radioGroupBloodPressure.Size = new System.Drawing.Size(130, 42);
            this.radioGroupBloodPressure.TabIndex = 13;
            // 
            // gridLookUpEditLifeTerm
            // 
            this.gridLookUpEditLifeTerm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditLifeTerm.Location = new System.Drawing.Point(342, 61);
            this.gridLookUpEditLifeTerm.Name = "gridLookUpEditLifeTerm";
            this.gridLookUpEditLifeTerm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditLifeTerm.Properties.DisplayMember = "name";
            this.gridLookUpEditLifeTerm.Properties.NullText = "";
            this.gridLookUpEditLifeTerm.Properties.ValueMember = "id";
            this.gridLookUpEditLifeTerm.Properties.View = this.gridLookUpEdit1View;
            this.gridLookUpEditLifeTerm.Size = new System.Drawing.Size(100, 20);
            this.gridLookUpEditLifeTerm.TabIndex = 15;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnName});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnName
            // 
            this.gridColumnName.Caption = "Нэр";
            this.gridColumnName.FieldName = "name";
            this.gridColumnName.Name = "gridColumnName";
            this.gridColumnName.Visible = true;
            this.gridColumnName.VisibleIndex = 0;
            // 
            // gridLookUpEditWorkTerm
            // 
            this.gridLookUpEditWorkTerm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditWorkTerm.Location = new System.Drawing.Point(464, 61);
            this.gridLookUpEditWorkTerm.Name = "gridLookUpEditWorkTerm";
            this.gridLookUpEditWorkTerm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditWorkTerm.Properties.DisplayMember = "name";
            this.gridLookUpEditWorkTerm.Properties.NullText = "";
            this.gridLookUpEditWorkTerm.Properties.ValueMember = "id";
            this.gridLookUpEditWorkTerm.Properties.View = this.gridView3;
            this.gridLookUpEditWorkTerm.Size = new System.Drawing.Size(100, 20);
            this.gridLookUpEditWorkTerm.TabIndex = 16;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Нэрс";
            this.gridColumn15.FieldName = "name";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            // 
            // gridLookUpEditInfection
            // 
            this.gridLookUpEditInfection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditInfection.Location = new System.Drawing.Point(342, 125);
            this.gridLookUpEditInfection.Name = "gridLookUpEditInfection";
            this.gridLookUpEditInfection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditInfection.Properties.DisplayMember = "name";
            this.gridLookUpEditInfection.Properties.NullText = "";
            this.gridLookUpEditInfection.Properties.ValueMember = "id";
            this.gridLookUpEditInfection.Properties.View = this.gridView7;
            this.gridLookUpEditInfection.Size = new System.Drawing.Size(154, 20);
            this.gridLookUpEditInfection.TabIndex = 25;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn18});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Нэрс";
            this.gridColumn18.FieldName = "name";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 0;
            // 
            // checkEditDoesSurgery
            // 
            this.checkEditDoesSurgery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditDoesSurgery.EditValue = null;
            this.checkEditDoesSurgery.Location = new System.Drawing.Point(541, 126);
            this.checkEditDoesSurgery.Name = "checkEditDoesSurgery";
            this.checkEditDoesSurgery.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.checkEditDoesSurgery.Properties.Caption = "";
            this.checkEditDoesSurgery.Size = new System.Drawing.Size(23, 19);
            this.checkEditDoesSurgery.TabIndex = 26;
            // 
            // memoEditAccident
            // 
            this.memoEditAccident.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditAccident.Location = new System.Drawing.Point(587, 125);
            this.memoEditAccident.Name = "memoEditAccident";
            this.memoEditAccident.Properties.MaxLength = 50;
            this.memoEditAccident.Size = new System.Drawing.Size(154, 40);
            this.memoEditAccident.TabIndex = 29;
            // 
            // gridLookUpEditAllergy
            // 
            this.gridLookUpEditAllergy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditAllergy.Location = new System.Drawing.Point(342, 189);
            this.gridLookUpEditAllergy.Name = "gridLookUpEditAllergy";
            this.gridLookUpEditAllergy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditAllergy.Properties.DisplayMember = "name";
            this.gridLookUpEditAllergy.Properties.NullText = "";
            this.gridLookUpEditAllergy.Properties.ValueMember = "id";
            this.gridLookUpEditAllergy.Properties.View = this.gridView8;
            this.gridLookUpEditAllergy.Size = new System.Drawing.Size(100, 20);
            this.gridLookUpEditAllergy.TabIndex = 27;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn19});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Нэрс";
            this.gridColumn19.FieldName = "name";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 0;
            // 
            // radioGroupIsVodka
            // 
            this.radioGroupIsVodka.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupIsVodka.Location = new System.Drawing.Point(342, 255);
            this.radioGroupIsVodka.Name = "radioGroupIsVodka";
            this.radioGroupIsVodka.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм", true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй", true, "Үгүй")});
            this.radioGroupIsVodka.Size = new System.Drawing.Size(100, 20);
            this.radioGroupIsVodka.TabIndex = 30;
            // 
            // RadioGroupIsSmoke
            // 
            this.RadioGroupIsSmoke.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadioGroupIsSmoke.Location = new System.Drawing.Point(464, 255);
            this.RadioGroupIsSmoke.Name = "RadioGroupIsSmoke";
            this.RadioGroupIsSmoke.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм", true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй", true, "Үгүй")});
            this.RadioGroupIsSmoke.Size = new System.Drawing.Size(100, 20);
            this.RadioGroupIsSmoke.TabIndex = 31;
            // 
            // memoEditHowLong
            // 
            this.memoEditHowLong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditHowLong.Location = new System.Drawing.Point(587, 271);
            this.memoEditHowLong.Name = "memoEditHowLong";
            this.memoEditHowLong.Properties.MaxLength = 50;
            this.memoEditHowLong.Size = new System.Drawing.Size(154, 20);
            this.memoEditHowLong.TabIndex = 32;
            // 
            // gridLookUpEditDiet
            // 
            this.gridLookUpEditDiet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditDiet.Location = new System.Drawing.Point(464, 189);
            this.gridLookUpEditDiet.Name = "gridLookUpEditDiet";
            this.gridLookUpEditDiet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDiet.Properties.DisplayMember = "name";
            this.gridLookUpEditDiet.Properties.NullText = "";
            this.gridLookUpEditDiet.Properties.ValueMember = "id";
            this.gridLookUpEditDiet.Properties.View = this.gridView16;
            this.gridLookUpEditDiet.Size = new System.Drawing.Size(100, 20);
            this.gridLookUpEditDiet.TabIndex = 33;
            // 
            // gridView16
            // 
            this.gridView16.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn20});
            this.gridView16.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView16.Name = "gridView16";
            this.gridView16.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView16.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Нэрс";
            this.gridColumn20.FieldName = "name";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            // 
            // memoEditInheritance
            // 
            this.memoEditInheritance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditInheritance.Location = new System.Drawing.Point(587, 190);
            this.memoEditInheritance.Name = "memoEditInheritance";
            this.memoEditInheritance.Properties.MaxLength = 50;
            this.memoEditInheritance.Size = new System.Drawing.Size(154, 40);
            this.memoEditInheritance.TabIndex = 28;
            // 
            // xtraTabPageAnamnez2
            // 
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl83);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl82);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl81);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl80);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl79);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl78);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl77);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl76);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl75);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl74);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl73);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl72);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl71);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl70);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl69);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl68);
            this.xtraTabPageAnamnez2.Controls.Add(this.labelControl67);
            this.xtraTabPageAnamnez2.Controls.Add(this.gridLookUpEditGeneralCondition);
            this.xtraTabPageAnamnez2.Controls.Add(this.gridLookUpEditMind);
            this.xtraTabPageAnamnez2.Controls.Add(this.gridLookUpEditSurround);
            this.xtraTabPageAnamnez2.Controls.Add(this.gridLookUpEditPos);
            this.xtraTabPageAnamnez2.Controls.Add(this.radioGroupSkinSalst);
            this.xtraTabPageAnamnez2.Controls.Add(this.gridLookUpEditSkinFlexible);
            this.xtraTabPageAnamnez2.Controls.Add(this.radioGroupSkinRash);
            this.xtraTabPageAnamnez2.Controls.Add(this.radioGroupEdema);
            this.xtraTabPageAnamnez2.Controls.Add(this.gridLookUpEditGeneralPart);
            this.xtraTabPageAnamnez2.Controls.Add(this.gridlookUpEditSize);
            this.xtraTabPageAnamnez2.Controls.Add(this.gridLookUpEditPlace);
            this.xtraTabPageAnamnez2.Controls.Add(this.radioGroupConcern);
            this.xtraTabPageAnamnez2.Controls.Add(this.radioGroupShape);
            this.xtraTabPageAnamnez2.Controls.Add(this.gridLookUpEditMove);
            this.xtraTabPageAnamnez2.Controls.Add(this.gridLookUpEditSkinWet);
            this.xtraTabPageAnamnez2.Controls.Add(this.checkEditGeneral);
            this.xtraTabPageAnamnez2.Name = "xtraTabPageAnamnez2";
            this.xtraTabPageAnamnez2.Size = new System.Drawing.Size(772, 303);
            this.xtraTabPageAnamnez2.Text = "Ерөнхий үзлэг";
            // 
            // labelControl83
            // 
            this.labelControl83.Location = new System.Drawing.Point(19, 244);
            this.labelControl83.Name = "labelControl83";
            this.labelControl83.Size = new System.Drawing.Size(58, 13);
            this.labelControl83.TabIndex = 23;
            this.labelControl83.Text = "в. Эмзэглэл";
            // 
            // labelControl82
            // 
            this.labelControl82.Location = new System.Drawing.Point(210, 244);
            this.labelControl82.Name = "labelControl82";
            this.labelControl82.Size = new System.Drawing.Size(92, 13);
            this.labelControl82.TabIndex = 22;
            this.labelControl82.Text = "Үе мөчний хэлбэр:";
            // 
            // labelControl81
            // 
            this.labelControl81.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl81.Location = new System.Drawing.Point(401, 244);
            this.labelControl81.Name = "labelControl81";
            this.labelControl81.Size = new System.Drawing.Size(91, 13);
            this.labelControl81.TabIndex = 21;
            this.labelControl81.Text = "Үений хөдөлгөөн:";
            // 
            // labelControl80
            // 
            this.labelControl80.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl80.Location = new System.Drawing.Point(591, 176);
            this.labelControl80.Name = "labelControl80";
            this.labelControl80.Size = new System.Drawing.Size(55, 13);
            this.labelControl80.TabIndex = 20;
            this.labelControl80.Text = "б. Байрлал";
            // 
            // labelControl79
            // 
            this.labelControl79.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl79.Location = new System.Drawing.Point(401, 176);
            this.labelControl79.Name = "labelControl79";
            this.labelControl79.Size = new System.Drawing.Size(150, 26);
            this.labelControl79.TabIndex = 19;
            this.labelControl79.Text = "Захын тунглагын булчирхай:\r\nа. Хэмжээ";
            // 
            // labelControl78
            // 
            this.labelControl78.Location = new System.Drawing.Point(210, 176);
            this.labelControl78.Name = "labelControl78";
            this.labelControl78.Size = new System.Drawing.Size(56, 26);
            this.labelControl78.TabIndex = 18;
            this.labelControl78.Text = "а. Ерөнхий \r\nб. Хэсгийн:";
            // 
            // labelControl77
            // 
            this.labelControl77.Location = new System.Drawing.Point(19, 176);
            this.labelControl77.Name = "labelControl77";
            this.labelControl77.Size = new System.Drawing.Size(34, 13);
            this.labelControl77.TabIndex = 17;
            this.labelControl77.Text = "Хаван:";
            // 
            // labelControl76
            // 
            this.labelControl76.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl76.Location = new System.Drawing.Point(591, 121);
            this.labelControl76.Name = "labelControl76";
            this.labelControl76.Size = new System.Drawing.Size(129, 13);
            this.labelControl76.TabIndex = 9;
            this.labelControl76.Text = "г. Арьсан дээрх тууралт:";
            // 
            // labelControl75
            // 
            this.labelControl75.Appearance.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelControl75.Location = new System.Drawing.Point(19, 103);
            this.labelControl75.Name = "labelControl75";
            this.labelControl75.Size = new System.Drawing.Size(115, 12);
            this.labelControl75.TabIndex = 8;
            this.labelControl75.Text = "Арьс салстын байдал:";
            // 
            // labelControl74
            // 
            this.labelControl74.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl74.Location = new System.Drawing.Point(401, 121);
            this.labelControl74.Name = "labelControl74";
            this.labelControl74.Size = new System.Drawing.Size(137, 13);
            this.labelControl74.TabIndex = 7;
            this.labelControl74.Text = "в. Арьсны чийглэг байдал:";
            // 
            // labelControl73
            // 
            this.labelControl73.Location = new System.Drawing.Point(210, 121);
            this.labelControl73.Name = "labelControl73";
            this.labelControl73.Size = new System.Drawing.Size(109, 13);
            this.labelControl73.TabIndex = 6;
            this.labelControl73.Text = "б. Арьсны уян чанар:";
            // 
            // labelControl72
            // 
            this.labelControl72.Location = new System.Drawing.Point(19, 121);
            this.labelControl72.Name = "labelControl72";
            this.labelControl72.Size = new System.Drawing.Size(112, 13);
            this.labelControl72.TabIndex = 5;
            this.labelControl72.Text = "а. Арьс салстын өнгө:";
            // 
            // labelControl71
            // 
            this.labelControl71.Location = new System.Drawing.Point(19, 49);
            this.labelControl71.Name = "labelControl71";
            this.labelControl71.Size = new System.Drawing.Size(125, 13);
            this.labelControl71.TabIndex = 4;
            this.labelControl71.Text = "Биеийн ерөнхий байдал:";
            // 
            // labelControl70
            // 
            this.labelControl70.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl70.Location = new System.Drawing.Point(591, 49);
            this.labelControl70.Name = "labelControl70";
            this.labelControl70.Size = new System.Drawing.Size(46, 13);
            this.labelControl70.TabIndex = 3;
            this.labelControl70.Text = "Байрлал:";
            // 
            // labelControl69
            // 
            this.labelControl69.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl69.Location = new System.Drawing.Point(401, 49);
            this.labelControl69.Name = "labelControl69";
            this.labelControl69.Size = new System.Drawing.Size(55, 13);
            this.labelControl69.TabIndex = 2;
            this.labelControl69.Text = "Орчиндоо:";
            // 
            // labelControl68
            // 
            this.labelControl68.Location = new System.Drawing.Point(210, 49);
            this.labelControl68.Name = "labelControl68";
            this.labelControl68.Size = new System.Drawing.Size(67, 13);
            this.labelControl68.TabIndex = 1;
            this.labelControl68.Text = "Ухаан санаа:";
            // 
            // labelControl67
            // 
            this.labelControl67.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl67.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl67.Location = new System.Drawing.Point(329, 13);
            this.labelControl67.Name = "labelControl67";
            this.labelControl67.Size = new System.Drawing.Size(90, 13);
            this.labelControl67.TabIndex = 0;
            this.labelControl67.Text = "ЕРӨНХИЙ ҮЗЛЭГ";
            // 
            // gridLookUpEditGeneralCondition
            // 
            this.gridLookUpEditGeneralCondition.Location = new System.Drawing.Point(19, 68);
            this.gridLookUpEditGeneralCondition.Name = "gridLookUpEditGeneralCondition";
            this.gridLookUpEditGeneralCondition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditGeneralCondition.Properties.DisplayMember = "name";
            this.gridLookUpEditGeneralCondition.Properties.NullText = "";
            this.gridLookUpEditGeneralCondition.Properties.ValueMember = "id";
            this.gridLookUpEditGeneralCondition.Properties.View = this.gridView17;
            this.gridLookUpEditGeneralCondition.Size = new System.Drawing.Size(150, 20);
            this.gridLookUpEditGeneralCondition.TabIndex = 10;
            // 
            // gridView17
            // 
            this.gridView17.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn27});
            this.gridView17.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView17.Name = "gridView17";
            this.gridView17.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView17.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Нэрс";
            this.gridColumn27.FieldName = "name";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 0;
            // 
            // gridLookUpEditMind
            // 
            this.gridLookUpEditMind.Location = new System.Drawing.Point(210, 68);
            this.gridLookUpEditMind.Name = "gridLookUpEditMind";
            this.gridLookUpEditMind.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMind.Properties.DisplayMember = "name";
            this.gridLookUpEditMind.Properties.NullText = "";
            this.gridLookUpEditMind.Properties.ValueMember = "id";
            this.gridLookUpEditMind.Properties.View = this.gridView18;
            this.gridLookUpEditMind.Size = new System.Drawing.Size(150, 20);
            this.gridLookUpEditMind.TabIndex = 11;
            // 
            // gridView18
            // 
            this.gridView18.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn28});
            this.gridView18.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView18.Name = "gridView18";
            this.gridView18.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView18.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Нэрс";
            this.gridColumn28.FieldName = "name";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 0;
            // 
            // gridLookUpEditSurround
            // 
            this.gridLookUpEditSurround.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditSurround.Location = new System.Drawing.Point(401, 68);
            this.gridLookUpEditSurround.Name = "gridLookUpEditSurround";
            this.gridLookUpEditSurround.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSurround.Properties.DisplayMember = "name";
            this.gridLookUpEditSurround.Properties.NullText = "";
            this.gridLookUpEditSurround.Properties.ValueMember = "id";
            this.gridLookUpEditSurround.Properties.View = this.gridView19;
            this.gridLookUpEditSurround.Size = new System.Drawing.Size(150, 20);
            this.gridLookUpEditSurround.TabIndex = 12;
            // 
            // gridView19
            // 
            this.gridView19.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn29});
            this.gridView19.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView19.Name = "gridView19";
            this.gridView19.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView19.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Нэрс";
            this.gridColumn29.FieldName = "name";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 0;
            // 
            // gridLookUpEditPos
            // 
            this.gridLookUpEditPos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditPos.Location = new System.Drawing.Point(591, 68);
            this.gridLookUpEditPos.Name = "gridLookUpEditPos";
            this.gridLookUpEditPos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPos.Properties.DisplayMember = "name";
            this.gridLookUpEditPos.Properties.NullText = "";
            this.gridLookUpEditPos.Properties.ValueMember = "id";
            this.gridLookUpEditPos.Properties.View = this.gridView20;
            this.gridLookUpEditPos.Size = new System.Drawing.Size(150, 20);
            this.gridLookUpEditPos.TabIndex = 13;
            // 
            // gridView20
            // 
            this.gridView20.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn30});
            this.gridView20.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView20.Name = "gridView20";
            this.gridView20.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView20.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Нэрс";
            this.gridColumn30.FieldName = "name";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 0;
            // 
            // radioGroupSkinSalst
            // 
            this.radioGroupSkinSalst.Location = new System.Drawing.Point(19, 140);
            this.radioGroupSkinSalst.Name = "radioGroupSkinSalst";
            this.radioGroupSkinSalst.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хэвийн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Хэвийн бус")});
            this.radioGroupSkinSalst.Size = new System.Drawing.Size(154, 20);
            this.radioGroupSkinSalst.TabIndex = 14;
            // 
            // gridLookUpEditSkinFlexible
            // 
            this.gridLookUpEditSkinFlexible.Location = new System.Drawing.Point(210, 140);
            this.gridLookUpEditSkinFlexible.Name = "gridLookUpEditSkinFlexible";
            this.gridLookUpEditSkinFlexible.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSkinFlexible.Properties.DisplayMember = "name";
            this.gridLookUpEditSkinFlexible.Properties.NullText = "";
            this.gridLookUpEditSkinFlexible.Properties.ValueMember = "id";
            this.gridLookUpEditSkinFlexible.Properties.View = this.gridView21;
            this.gridLookUpEditSkinFlexible.Size = new System.Drawing.Size(150, 20);
            this.gridLookUpEditSkinFlexible.TabIndex = 15;
            // 
            // gridView21
            // 
            this.gridView21.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn31});
            this.gridView21.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView21.Name = "gridView21";
            this.gridView21.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView21.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Нэрс";
            this.gridColumn31.FieldName = "name";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 0;
            // 
            // radioGroupSkinRash
            // 
            this.radioGroupSkinRash.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupSkinRash.Location = new System.Drawing.Point(591, 140);
            this.radioGroupSkinRash.Name = "radioGroupSkinRash";
            this.radioGroupSkinRash.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тууралтгүй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Тууралттай")});
            this.radioGroupSkinRash.Size = new System.Drawing.Size(174, 20);
            this.radioGroupSkinRash.TabIndex = 24;
            // 
            // radioGroupEdema
            // 
            this.radioGroupEdema.Location = new System.Drawing.Point(19, 208);
            this.radioGroupEdema.Name = "radioGroupEdema";
            this.radioGroupEdema.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хавангүй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Хавантай")});
            this.radioGroupEdema.Size = new System.Drawing.Size(154, 20);
            this.radioGroupEdema.TabIndex = 25;
            // 
            // gridLookUpEditGeneralPart
            // 
            this.gridLookUpEditGeneralPart.Location = new System.Drawing.Point(210, 208);
            this.gridLookUpEditGeneralPart.Name = "gridLookUpEditGeneralPart";
            this.gridLookUpEditGeneralPart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditGeneralPart.Properties.DisplayMember = "name";
            this.gridLookUpEditGeneralPart.Properties.NullText = "";
            this.gridLookUpEditGeneralPart.Properties.ValueMember = "id";
            this.gridLookUpEditGeneralPart.Properties.View = this.gridView22;
            this.gridLookUpEditGeneralPart.Size = new System.Drawing.Size(150, 20);
            this.gridLookUpEditGeneralPart.TabIndex = 26;
            // 
            // gridView22
            // 
            this.gridView22.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn33});
            this.gridView22.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView22.Name = "gridView22";
            this.gridView22.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView22.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Нэрс";
            this.gridColumn33.FieldName = "name";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 0;
            // 
            // gridlookUpEditSize
            // 
            this.gridlookUpEditSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridlookUpEditSize.Location = new System.Drawing.Point(401, 208);
            this.gridlookUpEditSize.Name = "gridlookUpEditSize";
            this.gridlookUpEditSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridlookUpEditSize.Properties.DisplayMember = "name";
            this.gridlookUpEditSize.Properties.NullText = "";
            this.gridlookUpEditSize.Properties.ValueMember = "id";
            this.gridlookUpEditSize.Properties.View = this.gridView23;
            this.gridlookUpEditSize.Size = new System.Drawing.Size(150, 20);
            this.gridlookUpEditSize.TabIndex = 27;
            // 
            // gridView23
            // 
            this.gridView23.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn34});
            this.gridView23.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView23.Name = "gridView23";
            this.gridView23.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView23.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Нэрс";
            this.gridColumn34.FieldName = "name";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 0;
            // 
            // gridLookUpEditPlace
            // 
            this.gridLookUpEditPlace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditPlace.Location = new System.Drawing.Point(591, 208);
            this.gridLookUpEditPlace.Name = "gridLookUpEditPlace";
            this.gridLookUpEditPlace.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPlace.Properties.DisplayMember = "name";
            this.gridLookUpEditPlace.Properties.NullText = "";
            this.gridLookUpEditPlace.Properties.ValueMember = "id";
            this.gridLookUpEditPlace.Properties.View = this.gridView24;
            this.gridLookUpEditPlace.Size = new System.Drawing.Size(150, 20);
            this.gridLookUpEditPlace.TabIndex = 28;
            // 
            // gridView24
            // 
            this.gridView24.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn35});
            this.gridView24.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView24.Name = "gridView24";
            this.gridView24.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView24.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Нэрс";
            this.gridColumn35.FieldName = "name";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 0;
            // 
            // radioGroupConcern
            // 
            this.radioGroupConcern.Location = new System.Drawing.Point(19, 263);
            this.radioGroupConcern.Name = "radioGroupConcern";
            this.radioGroupConcern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Эмзэглэлтэй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эмзэглэлгүй")});
            this.radioGroupConcern.Size = new System.Drawing.Size(175, 20);
            this.radioGroupConcern.TabIndex = 29;
            // 
            // radioGroupShape
            // 
            this.radioGroupShape.Location = new System.Drawing.Point(210, 263);
            this.radioGroupShape.Name = "radioGroupShape";
            this.radioGroupShape.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хэвийн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Өөрчлөгдсөн")});
            this.radioGroupShape.Size = new System.Drawing.Size(179, 20);
            this.radioGroupShape.TabIndex = 31;
            // 
            // gridLookUpEditMove
            // 
            this.gridLookUpEditMove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditMove.Location = new System.Drawing.Point(401, 263);
            this.gridLookUpEditMove.Name = "gridLookUpEditMove";
            this.gridLookUpEditMove.Properties.AutoHeight = false;
            this.gridLookUpEditMove.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMove.Properties.DisplayMember = "name";
            this.gridLookUpEditMove.Properties.NullText = "";
            this.gridLookUpEditMove.Properties.ValueMember = "id";
            this.gridLookUpEditMove.Properties.View = this.gridView25;
            this.gridLookUpEditMove.Size = new System.Drawing.Size(150, 20);
            this.gridLookUpEditMove.TabIndex = 30;
            // 
            // gridView25
            // 
            this.gridView25.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn36});
            this.gridView25.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView25.Name = "gridView25";
            this.gridView25.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView25.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Нэрс";
            this.gridColumn36.FieldName = "name";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 0;
            // 
            // gridLookUpEditSkinWet
            // 
            this.gridLookUpEditSkinWet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditSkinWet.Location = new System.Drawing.Point(401, 140);
            this.gridLookUpEditSkinWet.Name = "gridLookUpEditSkinWet";
            this.gridLookUpEditSkinWet.Properties.AutoHeight = false;
            this.gridLookUpEditSkinWet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSkinWet.Properties.DisplayMember = "name";
            this.gridLookUpEditSkinWet.Properties.NullText = "";
            this.gridLookUpEditSkinWet.Properties.ValueMember = "id";
            this.gridLookUpEditSkinWet.Properties.View = this.gridView26;
            this.gridLookUpEditSkinWet.Size = new System.Drawing.Size(150, 20);
            this.gridLookUpEditSkinWet.TabIndex = 16;
            // 
            // gridView26
            // 
            this.gridView26.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn32});
            this.gridView26.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView26.Name = "gridView26";
            this.gridView26.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView26.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Нэрс";
            this.gridColumn32.FieldName = "name";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 0;
            // 
            // checkEditGeneral
            // 
            this.checkEditGeneral.Location = new System.Drawing.Point(19, 10);
            this.checkEditGeneral.Name = "checkEditGeneral";
            this.checkEditGeneral.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.checkEditGeneral.Properties.Caption = "Бөглөх";
            this.checkEditGeneral.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditGeneral.Size = new System.Drawing.Size(58, 21);
            this.checkEditGeneral.TabIndex = 32;
            this.checkEditGeneral.CheckedChanged += new System.EventHandler(this.checkEditGeneral_CheckedChanged);
            // 
            // xtraTabPageAnamnez3
            // 
            this.xtraTabPageAnamnez3.AutoScroll = true;
            this.xtraTabPageAnamnez3.Controls.Add(this.checkEditInternal);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl108);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl107);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl106);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl105);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl104);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl103);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl102);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl101);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl100);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl99);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl98);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl97);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl96);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl95);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl94);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl93);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl92);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl91);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl90);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl89);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl88);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl87);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl86);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl85);
            this.xtraTabPageAnamnez3.Controls.Add(this.labelControl84);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupIsBreathing);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupIsCyanosis);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupIfCyanosis);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupIsBreathMuscles);
            this.xtraTabPageAnamnez3.Controls.Add(this.spinEditBreathCount);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupChestShape);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupBreathChest);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupIsCount);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupIconcern);
            this.xtraTabPageAnamnez3.Controls.Add(this.gridLookUpEditSoundVib);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupPercussion);
            this.xtraTabPageAnamnez3.Controls.Add(this.gridLookUpEditPartPercussion);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupBreathDouble);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupIsDesease);
            this.xtraTabPageAnamnez3.Controls.Add(this.gridLookUpEditIfDesease);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupIsFlexible);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupIsNoisy);
            this.xtraTabPageAnamnez3.Controls.Add(this.gridLookUpEditIfNoisy);
            this.xtraTabPageAnamnez3.Controls.Add(this.gridLookUpEditBronkhofoni);
            this.xtraTabPageAnamnez3.Controls.Add(this.radioGroupRightLeft);
            this.xtraTabPageAnamnez3.Controls.Add(this.gridLookUpEditBreathShape);
            this.xtraTabPageAnamnez3.Name = "xtraTabPageAnamnez3";
            this.xtraTabPageAnamnez3.Size = new System.Drawing.Size(772, 303);
            this.xtraTabPageAnamnez3.Text = "Дотрын үзлэг";
            // 
            // checkEditInternal
            // 
            this.checkEditInternal.Location = new System.Drawing.Point(22, 7);
            this.checkEditInternal.Name = "checkEditInternal";
            this.checkEditInternal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.checkEditInternal.Properties.Caption = "Бөглөх";
            this.checkEditInternal.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditInternal.Size = new System.Drawing.Size(62, 21);
            this.checkEditInternal.TabIndex = 48;
            this.checkEditInternal.CheckedChanged += new System.EventHandler(this.checkEditInternal_CheckedChanged);
            // 
            // labelControl108
            // 
            this.labelControl108.Location = new System.Drawing.Point(290, 247);
            this.labelControl108.Name = "labelControl108";
            this.labelControl108.Size = new System.Drawing.Size(66, 13);
            this.labelControl108.TabIndex = 45;
            this.labelControl108.Text = "Бронхофони:";
            // 
            // labelControl107
            // 
            this.labelControl107.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl107.Location = new System.Drawing.Point(554, 263);
            this.labelControl107.Name = "labelControl107";
            this.labelControl107.Size = new System.Drawing.Size(49, 13);
            this.labelControl107.TabIndex = 44;
            this.labelControl107.Text = "Тийм бол:";
            // 
            // labelControl106
            // 
            this.labelControl106.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl106.Location = new System.Drawing.Point(476, 236);
            this.labelControl106.Name = "labelControl106";
            this.labelControl106.Size = new System.Drawing.Size(127, 13);
            this.labelControl106.TabIndex = 43;
            this.labelControl106.Text = "Нэмэлт шуугаантай эсэх:";
            // 
            // labelControl105
            // 
            this.labelControl105.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl105.Location = new System.Drawing.Point(22, 218);
            this.labelControl105.Name = "labelControl105";
            this.labelControl105.Size = new System.Drawing.Size(43, 12);
            this.labelControl105.TabIndex = 37;
            this.labelControl105.Text = "Чагналт";
            // 
            // labelControl104
            // 
            this.labelControl104.Location = new System.Drawing.Point(290, 202);
            this.labelControl104.Name = "labelControl104";
            this.labelControl104.Size = new System.Drawing.Size(49, 13);
            this.labelControl104.TabIndex = 36;
            this.labelControl104.Text = "Тийм бол:";
            // 
            // labelControl103
            // 
            this.labelControl103.Location = new System.Drawing.Point(28, 236);
            this.labelControl103.Name = "labelControl103";
            this.labelControl103.Size = new System.Drawing.Size(75, 13);
            this.labelControl103.TabIndex = 35;
            this.labelControl103.Text = "Амьсгал 2 тал:";
            // 
            // labelControl102
            // 
            this.labelControl102.Location = new System.Drawing.Point(10, 263);
            this.labelControl102.Name = "labelControl102";
            this.labelControl102.Size = new System.Drawing.Size(93, 13);
            this.labelControl102.TabIndex = 34;
            this.labelControl102.Text = "Эмгэг амьсгалтай:";
            // 
            // labelControl101
            // 
            this.labelControl101.Location = new System.Drawing.Point(47, 199);
            this.labelControl101.Name = "labelControl101";
            this.labelControl101.Size = new System.Drawing.Size(56, 13);
            this.labelControl101.TabIndex = 32;
            this.labelControl101.Text = "Уян чанар:";
            // 
            // labelControl100
            // 
            this.labelControl100.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl100.Location = new System.Drawing.Point(485, 199);
            this.labelControl100.Name = "labelControl100";
            this.labelControl100.Size = new System.Drawing.Size(118, 13);
            this.labelControl100.TabIndex = 26;
            this.labelControl100.Text = "Хэсэгт тогшилтын дуу:";
            // 
            // labelControl99
            // 
            this.labelControl99.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl99.Location = new System.Drawing.Point(520, 167);
            this.labelControl99.Name = "labelControl99";
            this.labelControl99.Size = new System.Drawing.Size(83, 13);
            this.labelControl99.TabIndex = 25;
            this.labelControl99.Text = "Тогшилтын дуу:";
            // 
            // labelControl98
            // 
            this.labelControl98.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl98.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl98.Location = new System.Drawing.Point(476, 148);
            this.labelControl98.Name = "labelControl98";
            this.labelControl98.Size = new System.Drawing.Size(45, 12);
            this.labelControl98.TabIndex = 24;
            this.labelControl98.Text = "Тогшилт";
            // 
            // labelControl97
            // 
            this.labelControl97.Location = new System.Drawing.Point(13, 167);
            this.labelControl97.Name = "labelControl97";
            this.labelControl97.Size = new System.Drawing.Size(90, 13);
            this.labelControl97.TabIndex = 23;
            this.labelControl97.Text = "Эмзэглэлтэй эсэх:";
            // 
            // labelControl96
            // 
            this.labelControl96.Location = new System.Drawing.Point(290, 157);
            this.labelControl96.Name = "labelControl96";
            this.labelControl96.Size = new System.Drawing.Size(83, 13);
            this.labelControl96.TabIndex = 22;
            this.labelControl96.Text = "Дууны долгион:";
            // 
            // labelControl95
            // 
            this.labelControl95.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl95.Location = new System.Drawing.Point(22, 148);
            this.labelControl95.Name = "labelControl95";
            this.labelControl95.Size = new System.Drawing.Size(51, 12);
            this.labelControl95.TabIndex = 21;
            this.labelControl95.Text = "Тэмтрэлт:";
            // 
            // labelControl94
            // 
            this.labelControl94.Appearance.Font = new System.Drawing.Font("Tahoma", 7.5F, System.Drawing.FontStyle.Bold);
            this.labelControl94.Location = new System.Drawing.Point(109, 14);
            this.labelControl94.Name = "labelControl94";
            this.labelControl94.Size = new System.Drawing.Size(164, 12);
            this.labelControl94.TabIndex = 10;
            this.labelControl94.Text = "Амьсгалын эрхтэний тогтолцоо";
            // 
            // labelControl93
            // 
            this.labelControl93.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl93.Location = new System.Drawing.Point(22, 32);
            this.labelControl93.Name = "labelControl93";
            this.labelControl93.Size = new System.Drawing.Size(83, 12);
            this.labelControl93.TabIndex = 9;
            this.labelControl93.Text = "Харж ажиглах:";
            // 
            // labelControl92
            // 
            this.labelControl92.Location = new System.Drawing.Point(290, 84);
            this.labelControl92.Name = "labelControl92";
            this.labelControl92.Size = new System.Drawing.Size(141, 13);
            this.labelControl92.TabIndex = 8;
            this.labelControl92.Text = "Амьсгалын тоо 1 минутанд:";
            // 
            // labelControl91
            // 
            this.labelControl91.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl91.Location = new System.Drawing.Point(464, 43);
            this.labelControl91.Name = "labelControl91";
            this.labelControl91.Size = new System.Drawing.Size(85, 13);
            this.labelControl91.TabIndex = 7;
            this.labelControl91.Text = "Цээжний хэлбэр:";
            // 
            // labelControl90
            // 
            this.labelControl90.Location = new System.Drawing.Point(22, 79);
            this.labelControl90.Name = "labelControl90";
            this.labelControl90.Size = new System.Drawing.Size(80, 26);
            this.labelControl90.TabIndex = 6;
            this.labelControl90.Text = "Хөхрөлт байгаа \r\nэсэх:";
            // 
            // labelControl89
            // 
            this.labelControl89.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl89.Location = new System.Drawing.Point(609, 31);
            this.labelControl89.Name = "labelControl89";
            this.labelControl89.Size = new System.Drawing.Size(131, 26);
            this.labelControl89.TabIndex = 5;
            this.labelControl89.Text = "Цээжний 2 талд амьсгалд \r\nжигд оролцох байдал:";
            // 
            // labelControl88
            // 
            this.labelControl88.Location = new System.Drawing.Point(53, 112);
            this.labelControl88.Name = "labelControl88";
            this.labelControl88.Size = new System.Drawing.Size(49, 13);
            this.labelControl88.TabIndex = 4;
            this.labelControl88.Text = "Тийм бол:";
            // 
            // labelControl87
            // 
            this.labelControl87.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl87.Location = new System.Drawing.Point(465, 86);
            this.labelControl87.Name = "labelControl87";
            this.labelControl87.Size = new System.Drawing.Size(96, 13);
            this.labelControl87.TabIndex = 3;
            this.labelControl87.Text = "Амьсгалын хэлбэр:";
            // 
            // labelControl86
            // 
            this.labelControl86.Location = new System.Drawing.Point(290, 30);
            this.labelControl86.Name = "labelControl86";
            this.labelControl86.Size = new System.Drawing.Size(149, 26);
            this.labelControl86.TabIndex = 2;
            this.labelControl86.Text = "Амьсгалд туслах булчингууд \r\nоролцож байгаа эсэх:";
            // 
            // labelControl85
            // 
            this.labelControl85.Location = new System.Drawing.Point(22, 51);
            this.labelControl85.Name = "labelControl85";
            this.labelControl85.Size = new System.Drawing.Size(81, 26);
            this.labelControl85.TabIndex = 1;
            this.labelControl85.Text = "Хамрын амьсгал \r\nчөлөөтэй эсэх:";
            // 
            // labelControl84
            // 
            this.labelControl84.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl84.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl84.Location = new System.Drawing.Point(327, 11);
            this.labelControl84.Name = "labelControl84";
            this.labelControl84.Size = new System.Drawing.Size(140, 13);
            this.labelControl84.TabIndex = 0;
            this.labelControl84.Text = "ДОТРЫН ЭМЧИЙН ҮЗЛЭГ";
            // 
            // radioGroupIsBreathing
            // 
            this.radioGroupIsBreathing.Location = new System.Drawing.Point(108, 56);
            this.radioGroupIsBreathing.Name = "radioGroupIsBreathing";
            this.radioGroupIsBreathing.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsBreathing.Size = new System.Drawing.Size(140, 20);
            this.radioGroupIsBreathing.TabIndex = 11;
            // 
            // radioGroupIsCyanosis
            // 
            this.radioGroupIsCyanosis.Location = new System.Drawing.Point(108, 82);
            this.radioGroupIsCyanosis.Name = "radioGroupIsCyanosis";
            this.radioGroupIsCyanosis.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsCyanosis.Size = new System.Drawing.Size(140, 20);
            this.radioGroupIsCyanosis.TabIndex = 16;
            // 
            // radioGroupIfCyanosis
            // 
            this.radioGroupIfCyanosis.Location = new System.Drawing.Point(108, 108);
            this.radioGroupIfCyanosis.Name = "radioGroupIfCyanosis";
            this.radioGroupIfCyanosis.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Төвийн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Захын")});
            this.radioGroupIfCyanosis.Size = new System.Drawing.Size(140, 20);
            this.radioGroupIfCyanosis.TabIndex = 20;
            // 
            // radioGroupIsBreathMuscles
            // 
            this.radioGroupIsBreathMuscles.Location = new System.Drawing.Point(290, 57);
            this.radioGroupIsBreathMuscles.Name = "radioGroupIsBreathMuscles";
            this.radioGroupIsBreathMuscles.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsBreathMuscles.Size = new System.Drawing.Size(145, 20);
            this.radioGroupIsBreathMuscles.TabIndex = 12;
            // 
            // spinEditBreathCount
            // 
            this.spinEditBreathCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditBreathCount.Location = new System.Drawing.Point(290, 103);
            this.spinEditBreathCount.Name = "spinEditBreathCount";
            this.spinEditBreathCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditBreathCount.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditBreathCount.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spinEditBreathCount.Size = new System.Drawing.Size(145, 20);
            this.spinEditBreathCount.TabIndex = 18;
            // 
            // radioGroupChestShape
            // 
            this.radioGroupChestShape.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupChestShape.Location = new System.Drawing.Point(464, 57);
            this.radioGroupChestShape.Name = "radioGroupChestShape";
            this.radioGroupChestShape.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Зөв"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эмгэг")});
            this.radioGroupChestShape.Size = new System.Drawing.Size(120, 20);
            this.radioGroupChestShape.TabIndex = 13;
            // 
            // radioGroupBreathChest
            // 
            this.radioGroupBreathChest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupBreathChest.Location = new System.Drawing.Point(609, 57);
            this.radioGroupBreathChest.Name = "radioGroupBreathChest";
            this.radioGroupBreathChest.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Жигд(баруун\\зүүн тал)"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Хоцорно")});
            this.radioGroupBreathChest.Size = new System.Drawing.Size(145, 40);
            this.radioGroupBreathChest.TabIndex = 33;
            // 
            // radioGroupIsCount
            // 
            this.radioGroupIsCount.Location = new System.Drawing.Point(290, 129);
            this.radioGroupIsCount.Name = "radioGroupIsCount";
            this.radioGroupIsCount.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хэм жигд"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Жигд бус")});
            this.radioGroupIsCount.Size = new System.Drawing.Size(145, 20);
            this.radioGroupIsCount.TabIndex = 15;
            // 
            // radioGroupIconcern
            // 
            this.radioGroupIconcern.Location = new System.Drawing.Point(108, 148);
            this.radioGroupIconcern.Name = "radioGroupIconcern";
            this.radioGroupIconcern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Эмзэглэлгүй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эмзэглэлтэй")});
            this.radioGroupIconcern.Size = new System.Drawing.Size(140, 40);
            this.radioGroupIconcern.TabIndex = 27;
            // 
            // gridLookUpEditSoundVib
            // 
            this.gridLookUpEditSoundVib.Location = new System.Drawing.Point(290, 176);
            this.gridLookUpEditSoundVib.Name = "gridLookUpEditSoundVib";
            this.gridLookUpEditSoundVib.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSoundVib.Properties.DisplayMember = "name";
            this.gridLookUpEditSoundVib.Properties.NullText = "";
            this.gridLookUpEditSoundVib.Properties.ValueMember = "id";
            this.gridLookUpEditSoundVib.Properties.View = this.gridView27;
            this.gridLookUpEditSoundVib.Size = new System.Drawing.Size(145, 20);
            this.gridLookUpEditSoundVib.TabIndex = 28;
            // 
            // gridView27
            // 
            this.gridView27.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn37});
            this.gridView27.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView27.Name = "gridView27";
            this.gridView27.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView27.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Нэрс";
            this.gridColumn37.FieldName = "name";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 0;
            // 
            // radioGroupPercussion
            // 
            this.radioGroupPercussion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupPercussion.Location = new System.Drawing.Point(609, 148);
            this.radioGroupPercussion.Name = "radioGroupPercussion";
            this.radioGroupPercussion.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "2 талд ижил"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Ижил бус")});
            this.radioGroupPercussion.Size = new System.Drawing.Size(145, 40);
            this.radioGroupPercussion.TabIndex = 29;
            // 
            // gridLookUpEditPartPercussion
            // 
            this.gridLookUpEditPartPercussion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditPartPercussion.Location = new System.Drawing.Point(609, 196);
            this.gridLookUpEditPartPercussion.Name = "gridLookUpEditPartPercussion";
            this.gridLookUpEditPartPercussion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPartPercussion.Properties.DisplayMember = "name";
            this.gridLookUpEditPartPercussion.Properties.NullText = "";
            this.gridLookUpEditPartPercussion.Properties.ValueMember = "id";
            this.gridLookUpEditPartPercussion.Properties.View = this.gridView28;
            this.gridLookUpEditPartPercussion.Size = new System.Drawing.Size(145, 20);
            this.gridLookUpEditPartPercussion.TabIndex = 30;
            // 
            // gridView28
            // 
            this.gridView28.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn38});
            this.gridView28.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView28.Name = "gridView28";
            this.gridView28.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView28.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Нэрс";
            this.gridColumn38.FieldName = "name";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 0;
            // 
            // radioGroupBreathDouble
            // 
            this.radioGroupBreathDouble.Location = new System.Drawing.Point(109, 233);
            this.radioGroupBreathDouble.Name = "radioGroupBreathDouble";
            this.radioGroupBreathDouble.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Ижил"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Ижил бус")});
            this.radioGroupBreathDouble.Size = new System.Drawing.Size(140, 20);
            this.radioGroupBreathDouble.TabIndex = 39;
            // 
            // radioGroupIsDesease
            // 
            this.radioGroupIsDesease.Location = new System.Drawing.Point(109, 259);
            this.radioGroupIsDesease.Name = "radioGroupIsDesease";
            this.radioGroupIsDesease.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsDesease.Size = new System.Drawing.Size(140, 20);
            this.radioGroupIsDesease.TabIndex = 38;
            // 
            // gridLookUpEditIfDesease
            // 
            this.gridLookUpEditIfDesease.Location = new System.Drawing.Point(290, 221);
            this.gridLookUpEditIfDesease.Name = "gridLookUpEditIfDesease";
            this.gridLookUpEditIfDesease.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditIfDesease.Properties.DisplayMember = "name";
            this.gridLookUpEditIfDesease.Properties.NullText = "";
            this.gridLookUpEditIfDesease.Properties.ValueMember = "id";
            this.gridLookUpEditIfDesease.Properties.View = this.gridView29;
            this.gridLookUpEditIfDesease.Size = new System.Drawing.Size(145, 20);
            this.gridLookUpEditIfDesease.TabIndex = 40;
            // 
            // gridView29
            // 
            this.gridView29.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn39});
            this.gridView29.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView29.Name = "gridView29";
            this.gridView29.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView29.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Нэрс";
            this.gridColumn39.FieldName = "name";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 0;
            // 
            // radioGroupIsFlexible
            // 
            this.radioGroupIsFlexible.Location = new System.Drawing.Point(108, 196);
            this.radioGroupIsFlexible.Name = "radioGroupIsFlexible";
            this.radioGroupIsFlexible.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хэвийн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Буурсан")});
            this.radioGroupIsFlexible.Size = new System.Drawing.Size(140, 20);
            this.radioGroupIsFlexible.TabIndex = 31;
            // 
            // radioGroupIsNoisy
            // 
            this.radioGroupIsNoisy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupIsNoisy.Location = new System.Drawing.Point(609, 232);
            this.radioGroupIsNoisy.Name = "radioGroupIsNoisy";
            this.radioGroupIsNoisy.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsNoisy.Size = new System.Drawing.Size(145, 20);
            this.radioGroupIsNoisy.TabIndex = 41;
            // 
            // gridLookUpEditIfNoisy
            // 
            this.gridLookUpEditIfNoisy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditIfNoisy.Location = new System.Drawing.Point(609, 260);
            this.gridLookUpEditIfNoisy.Name = "gridLookUpEditIfNoisy";
            this.gridLookUpEditIfNoisy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditIfNoisy.Properties.DisplayMember = "name";
            this.gridLookUpEditIfNoisy.Properties.NullText = "";
            this.gridLookUpEditIfNoisy.Properties.ValueMember = "id";
            this.gridLookUpEditIfNoisy.Properties.View = this.gridView30;
            this.gridLookUpEditIfNoisy.Size = new System.Drawing.Size(145, 20);
            this.gridLookUpEditIfNoisy.TabIndex = 42;
            // 
            // gridView30
            // 
            this.gridView30.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn40});
            this.gridView30.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView30.Name = "gridView30";
            this.gridView30.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView30.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Нэрс";
            this.gridColumn40.FieldName = "name";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 0;
            // 
            // gridLookUpEditBronkhofoni
            // 
            this.gridLookUpEditBronkhofoni.Location = new System.Drawing.Point(290, 260);
            this.gridLookUpEditBronkhofoni.Name = "gridLookUpEditBronkhofoni";
            this.gridLookUpEditBronkhofoni.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditBronkhofoni.Properties.DisplayMember = "name";
            this.gridLookUpEditBronkhofoni.Properties.NullText = "";
            this.gridLookUpEditBronkhofoni.Properties.ValueMember = "id";
            this.gridLookUpEditBronkhofoni.Properties.View = this.gridView31;
            this.gridLookUpEditBronkhofoni.Size = new System.Drawing.Size(145, 20);
            this.gridLookUpEditBronkhofoni.TabIndex = 46;
            // 
            // gridView31
            // 
            this.gridView31.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn41});
            this.gridView31.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView31.Name = "gridView31";
            this.gridView31.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView31.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Нэрс";
            this.gridColumn41.FieldName = "name";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 0;
            // 
            // radioGroupRightLeft
            // 
            this.radioGroupRightLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupRightLeft.Location = new System.Drawing.Point(609, 105);
            this.radioGroupRightLeft.Name = "radioGroupRightLeft";
            this.radioGroupRightLeft.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Баруун"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Зүүн")});
            this.radioGroupRightLeft.Size = new System.Drawing.Size(145, 20);
            this.radioGroupRightLeft.TabIndex = 47;
            // 
            // gridLookUpEditBreathShape
            // 
            this.gridLookUpEditBreathShape.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditBreathShape.Location = new System.Drawing.Point(464, 103);
            this.gridLookUpEditBreathShape.Name = "gridLookUpEditBreathShape";
            this.gridLookUpEditBreathShape.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditBreathShape.Properties.DisplayMember = "name";
            this.gridLookUpEditBreathShape.Properties.NullText = "";
            this.gridLookUpEditBreathShape.Properties.PopupSizeable = false;
            this.gridLookUpEditBreathShape.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.gridLookUpEditBreathShape.Properties.ValueMember = "id";
            this.gridLookUpEditBreathShape.Properties.View = this.gridView32;
            this.gridLookUpEditBreathShape.Size = new System.Drawing.Size(120, 20);
            this.gridLookUpEditBreathShape.TabIndex = 14;
            // 
            // gridView32
            // 
            this.gridView32.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn42});
            this.gridView32.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView32.Name = "gridView32";
            this.gridView32.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView32.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Нэрс";
            this.gridColumn42.FieldName = "name";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 0;
            // 
            // xtraTabPageAnamnez4
            // 
            this.xtraTabPageAnamnez4.Controls.Add(this.checkEditHeart);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl138);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl137);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl136);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl135);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl134);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl133);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl132);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl131);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl130);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl129);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl128);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl127);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl126);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl125);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl124);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl123);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl122);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl121);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl120);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl119);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl118);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl117);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl116);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl115);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl114);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl113);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl112);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl111);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl110);
            this.xtraTabPageAnamnez4.Controls.Add(this.labelControl109);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupSkinBlue);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupIsEdem);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupHeartPush);
            this.xtraTabPageAnamnez4.Controls.Add(this.spinEditFrequency);
            this.xtraTabPageAnamnez4.Controls.Add(this.spinEditHeartFrequency);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupHeartSound);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupRate);
            this.xtraTabPageAnamnez4.Controls.Add(this.gridLookUpEditFirstSound);
            this.xtraTabPageAnamnez4.Controls.Add(this.gridLookUpEditSecondSound);
            this.xtraTabPageAnamnez4.Controls.Add(this.gridLookUpEditThirdSound);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupSameDouble);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupTransmission);
            this.xtraTabPageAnamnez4.Controls.Add(this.gridLookUpEditTransStrong);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupHeartLimit);
            this.xtraTabPageAnamnez4.Controls.Add(this.gridLookUpEditSezi);
            this.xtraTabPageAnamnez4.Controls.Add(this.gridLookUpEditSis);
            this.xtraTabPageAnamnez4.Controls.Add(this.gridLookUpEditPost);
            this.xtraTabPageAnamnez4.Controls.Add(this.gridLookUpEditDis);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupIsTouch);
            this.xtraTabPageAnamnez4.Controls.Add(this.gridLookUpEditHeartRisk);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupVenous);
            this.xtraTabPageAnamnez4.Controls.Add(this.gridLookUpEditVoltage);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupFilled);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupHeartPlace);
            this.xtraTabPageAnamnez4.Controls.Add(this.radioGroupThisNoisy);
            this.xtraTabPageAnamnez4.Controls.Add(this.gridLookUpEditStrong);
            this.xtraTabPageAnamnez4.Name = "xtraTabPageAnamnez4";
            this.xtraTabPageAnamnez4.Size = new System.Drawing.Size(772, 303);
            this.xtraTabPageAnamnez4.Text = "Зүрхны тогтолцоо";
            // 
            // checkEditHeart
            // 
            this.checkEditHeart.Location = new System.Drawing.Point(8, 3);
            this.checkEditHeart.Name = "checkEditHeart";
            this.checkEditHeart.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.checkEditHeart.Properties.Caption = "Бөглөх";
            this.checkEditHeart.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditHeart.Size = new System.Drawing.Size(64, 21);
            this.checkEditHeart.TabIndex = 56;
            this.checkEditHeart.CheckedChanged += new System.EventHandler(this.checkEditHeart_CheckedChanged);
            // 
            // labelControl138
            // 
            this.labelControl138.Location = new System.Drawing.Point(401, 247);
            this.labelControl138.Name = "labelControl138";
            this.labelControl138.Size = new System.Drawing.Size(21, 13);
            this.labelControl138.TabIndex = 54;
            this.labelControl138.Text = "Хүч:";
            // 
            // labelControl137
            // 
            this.labelControl137.Location = new System.Drawing.Point(544, 225);
            this.labelControl137.Name = "labelControl137";
            this.labelControl137.Size = new System.Drawing.Size(61, 13);
            this.labelControl137.TabIndex = 47;
            this.labelControl137.Text = "Диастолын:";
            // 
            // labelControl136
            // 
            this.labelControl136.Location = new System.Drawing.Point(569, 249);
            this.labelControl136.Name = "labelControl136";
            this.labelControl136.Size = new System.Drawing.Size(187, 13);
            this.labelControl136.TabIndex = 46;
            this.labelControl136.Text = "Пикардын шүргэлцэх чимээ бий эсэх:";
            // 
            // labelControl135
            // 
            this.labelControl135.Location = new System.Drawing.Point(3, 272);
            this.labelControl135.Name = "labelControl135";
            this.labelControl135.Size = new System.Drawing.Size(50, 13);
            this.labelControl135.TabIndex = 41;
            this.labelControl135.Text = "Давтамж:";
            // 
            // labelControl134
            // 
            this.labelControl134.Location = new System.Drawing.Point(559, 173);
            this.labelControl134.Name = "labelControl134";
            this.labelControl134.Size = new System.Drawing.Size(46, 13);
            this.labelControl134.TabIndex = 40;
            this.labelControl134.Text = "Байрлал:";
            // 
            // labelControl133
            // 
            this.labelControl133.Location = new System.Drawing.Point(551, 199);
            this.labelControl133.Name = "labelControl133";
            this.labelControl133.Size = new System.Drawing.Size(54, 13);
            this.labelControl133.TabIndex = 39;
            this.labelControl133.Text = "Систолын:";
            // 
            // labelControl132
            // 
            this.labelControl132.Location = new System.Drawing.Point(216, 220);
            this.labelControl132.Name = "labelControl132";
            this.labelControl132.Size = new System.Drawing.Size(35, 13);
            this.labelControl132.TabIndex = 38;
            this.labelControl132.Text = "I авиа:";
            // 
            // labelControl131
            // 
            this.labelControl131.Location = new System.Drawing.Point(208, 272);
            this.labelControl131.Name = "labelControl131";
            this.labelControl131.Size = new System.Drawing.Size(43, 13);
            this.labelControl131.TabIndex = 37;
            this.labelControl131.Text = "III авиа:";
            // 
            // labelControl130
            // 
            this.labelControl130.Location = new System.Drawing.Point(212, 247);
            this.labelControl130.Name = "labelControl130";
            this.labelControl130.Size = new System.Drawing.Size(39, 13);
            this.labelControl130.TabIndex = 36;
            this.labelControl130.Text = "II авиа:";
            // 
            // labelControl129
            // 
            this.labelControl129.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl129.Location = new System.Drawing.Point(8, 204);
            this.labelControl129.Name = "labelControl129";
            this.labelControl129.Size = new System.Drawing.Size(61, 12);
            this.labelControl129.TabIndex = 32;
            this.labelControl129.Text = "Чагналтаар";
            // 
            // labelControl128
            // 
            this.labelControl128.Location = new System.Drawing.Point(245, 178);
            this.labelControl128.Name = "labelControl128";
            this.labelControl128.Size = new System.Drawing.Size(91, 13);
            this.labelControl128.TabIndex = 31;
            this.labelControl128.Text = "2 талд ижил эсэх:";
            // 
            // labelControl127
            // 
            this.labelControl127.Location = new System.Drawing.Point(14, 247);
            this.labelControl127.Name = "labelControl127";
            this.labelControl127.Size = new System.Drawing.Size(38, 13);
            this.labelControl127.TabIndex = 30;
            this.labelControl127.Text = "Хэмнэл:";
            // 
            // labelControl126
            // 
            this.labelControl126.Location = new System.Drawing.Point(8, 220);
            this.labelControl126.Name = "labelControl126";
            this.labelControl126.Size = new System.Drawing.Size(72, 13);
            this.labelControl126.TabIndex = 29;
            this.labelControl126.Text = "Зүрхний авиа:";
            // 
            // labelControl125
            // 
            this.labelControl125.Location = new System.Drawing.Point(291, 152);
            this.labelControl125.Name = "labelControl125";
            this.labelControl125.Size = new System.Drawing.Size(45, 13);
            this.labelControl125.TabIndex = 24;
            this.labelControl125.Text = "Дүүрэлт:";
            // 
            // labelControl124
            // 
            this.labelControl124.Location = new System.Drawing.Point(14, 182);
            this.labelControl124.Name = "labelControl124";
            this.labelControl124.Size = new System.Drawing.Size(38, 13);
            this.labelControl124.TabIndex = 23;
            this.labelControl124.Text = "Хэмнэл:";
            // 
            // labelControl123
            // 
            this.labelControl123.Location = new System.Drawing.Point(12, 160);
            this.labelControl123.Name = "labelControl123";
            this.labelControl123.Size = new System.Drawing.Size(133, 13);
            this.labelControl123.TabIndex = 22;
            this.labelControl123.Text = "Шууны артерийн лугшилт";
            // 
            // labelControl122
            // 
            this.labelControl122.Location = new System.Drawing.Point(31, 137);
            this.labelControl122.Name = "labelControl122";
            this.labelControl122.Size = new System.Drawing.Size(21, 13);
            this.labelControl122.TabIndex = 21;
            this.labelControl122.Text = "Хүч:";
            // 
            // labelControl121
            // 
            this.labelControl121.Location = new System.Drawing.Point(297, 126);
            this.labelControl121.Name = "labelControl121";
            this.labelControl121.Size = new System.Drawing.Size(39, 13);
            this.labelControl121.TabIndex = 20;
            this.labelControl121.Text = "Хүчдэл:";
            // 
            // labelControl120
            // 
            this.labelControl120.Location = new System.Drawing.Point(12, 89);
            this.labelControl120.Name = "labelControl120";
            this.labelControl120.Size = new System.Drawing.Size(166, 13);
            this.labelControl120.TabIndex = 11;
            this.labelControl120.Text = "Зүрхний оройн түлхэлт Байрлал:";
            // 
            // labelControl119
            // 
            this.labelControl119.Location = new System.Drawing.Point(261, 100);
            this.labelControl119.Name = "labelControl119";
            this.labelControl119.Size = new System.Drawing.Size(75, 13);
            this.labelControl119.TabIndex = 10;
            this.labelControl119.Text = "Давтамж /мин:";
            // 
            // labelControl118
            // 
            this.labelControl118.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl118.Location = new System.Drawing.Point(513, 71);
            this.labelControl118.Name = "labelControl118";
            this.labelControl118.Size = new System.Drawing.Size(63, 12);
            this.labelControl118.TabIndex = 9;
            this.labelControl118.Text = "Тогшилтоор";
            // 
            // labelControl117
            // 
            this.labelControl117.Location = new System.Drawing.Point(494, 26);
            this.labelControl117.Name = "labelControl117";
            this.labelControl117.Size = new System.Drawing.Size(111, 13);
            this.labelControl117.TabIndex = 8;
            this.labelControl117.Text = "Захын хавантай эсэх:";
            // 
            // labelControl116
            // 
            this.labelControl116.Location = new System.Drawing.Point(208, 26);
            this.labelControl116.Name = "labelControl116";
            this.labelControl116.Size = new System.Drawing.Size(128, 13);
            this.labelControl116.TabIndex = 7;
            this.labelControl116.Text = "Арьсны хөхрөлттэй эсэх:";
            // 
            // labelControl115
            // 
            this.labelControl115.Location = new System.Drawing.Point(252, 48);
            this.labelControl115.Name = "labelControl115";
            this.labelControl115.Size = new System.Drawing.Size(84, 26);
            this.labelControl115.TabIndex = 6;
            this.labelControl115.Text = "Гүрээний венийн \r\nлугшилт:";
            // 
            // labelControl114
            // 
            this.labelControl114.Location = new System.Drawing.Point(578, 99);
            this.labelControl114.Name = "labelControl114";
            this.labelControl114.Size = new System.Drawing.Size(178, 13);
            this.labelControl114.TabIndex = 5;
            this.labelControl114.Text = "Зүрхний (Харьцангүй) хил хизгаар:";
            // 
            // labelControl113
            // 
            this.labelControl113.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl113.Location = new System.Drawing.Point(353, 4);
            this.labelControl113.Name = "labelControl113";
            this.labelControl113.Size = new System.Drawing.Size(55, 12);
            this.labelControl113.TabIndex = 4;
            this.labelControl113.Text = "Ажиглалт";
            // 
            // labelControl112
            // 
            this.labelControl112.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl112.Location = new System.Drawing.Point(12, 71);
            this.labelControl112.Name = "labelControl112";
            this.labelControl112.Size = new System.Drawing.Size(64, 12);
            this.labelControl112.TabIndex = 3;
            this.labelControl112.Text = "Тэмтрэлтээр";
            // 
            // labelControl111
            // 
            this.labelControl111.Location = new System.Drawing.Point(502, 52);
            this.labelControl111.Name = "labelControl111";
            this.labelControl111.Size = new System.Drawing.Size(103, 13);
            this.labelControl111.TabIndex = 2;
            this.labelControl111.Text = "Зүрх оройн түлхэлт:";
            // 
            // labelControl110
            // 
            this.labelControl110.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl110.Location = new System.Drawing.Point(10, 22);
            this.labelControl110.Name = "labelControl110";
            this.labelControl110.Size = new System.Drawing.Size(182, 12);
            this.labelControl110.TabIndex = 1;
            this.labelControl110.Text = "Зүрх судасны эрсдэлт хүчин зүйлс:";
            // 
            // labelControl109
            // 
            this.labelControl109.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl109.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl109.Location = new System.Drawing.Point(98, 3);
            this.labelControl109.Name = "labelControl109";
            this.labelControl109.Size = new System.Drawing.Size(148, 13);
            this.labelControl109.TabIndex = 0;
            this.labelControl109.Text = "Зүрх судасны тогтолцоо";
            // 
            // radioGroupSkinBlue
            // 
            this.radioGroupSkinBlue.Location = new System.Drawing.Point(342, 22);
            this.radioGroupSkinBlue.Name = "radioGroupSkinBlue";
            this.radioGroupSkinBlue.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupSkinBlue.Size = new System.Drawing.Size(145, 20);
            this.radioGroupSkinBlue.TabIndex = 12;
            // 
            // radioGroupIsEdem
            // 
            this.radioGroupIsEdem.Location = new System.Drawing.Point(611, 19);
            this.radioGroupIsEdem.Name = "radioGroupIsEdem";
            this.radioGroupIsEdem.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsEdem.Size = new System.Drawing.Size(145, 20);
            this.radioGroupIsEdem.TabIndex = 13;
            // 
            // radioGroupHeartPush
            // 
            this.radioGroupHeartPush.Location = new System.Drawing.Point(611, 45);
            this.radioGroupHeartPush.Name = "radioGroupHeartPush";
            this.radioGroupHeartPush.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Ажиглагдана"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Ажиглагдахгүй")});
            this.radioGroupHeartPush.Size = new System.Drawing.Size(145, 45);
            this.radioGroupHeartPush.TabIndex = 16;
            // 
            // spinEditFrequency
            // 
            this.spinEditFrequency.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditFrequency.Location = new System.Drawing.Point(342, 99);
            this.spinEditFrequency.Name = "spinEditFrequency";
            this.spinEditFrequency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditFrequency.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditFrequency.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spinEditFrequency.Size = new System.Drawing.Size(145, 20);
            this.spinEditFrequency.TabIndex = 17;
            // 
            // spinEditHeartFrequency
            // 
            this.spinEditHeartFrequency.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditHeartFrequency.Location = new System.Drawing.Point(56, 269);
            this.spinEditHeartFrequency.Name = "spinEditHeartFrequency";
            this.spinEditHeartFrequency.Properties.AutoHeight = false;
            this.spinEditHeartFrequency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditHeartFrequency.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditHeartFrequency.Size = new System.Drawing.Size(144, 20);
            this.spinEditHeartFrequency.TabIndex = 42;
            // 
            // radioGroupHeartSound
            // 
            this.radioGroupHeartSound.Location = new System.Drawing.Point(56, 243);
            this.radioGroupHeartSound.Name = "radioGroupHeartSound";
            this.radioGroupHeartSound.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Жигд"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Жигд бус")});
            this.radioGroupHeartSound.Size = new System.Drawing.Size(144, 20);
            this.radioGroupHeartSound.TabIndex = 34;
            // 
            // radioGroupRate
            // 
            this.radioGroupRate.Location = new System.Drawing.Point(56, 178);
            this.radioGroupRate.Name = "radioGroupRate";
            this.radioGroupRate.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Жигд"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Жигд бус")});
            this.radioGroupRate.Size = new System.Drawing.Size(144, 20);
            this.radioGroupRate.TabIndex = 25;
            // 
            // gridLookUpEditFirstSound
            // 
            this.gridLookUpEditFirstSound.Location = new System.Drawing.Point(257, 217);
            this.gridLookUpEditFirstSound.Name = "gridLookUpEditFirstSound";
            this.gridLookUpEditFirstSound.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditFirstSound.Properties.DisplayMember = "name";
            this.gridLookUpEditFirstSound.Properties.NullText = "";
            this.gridLookUpEditFirstSound.Properties.ValueMember = "id";
            this.gridLookUpEditFirstSound.Properties.View = this.gridView33;
            this.gridLookUpEditFirstSound.Size = new System.Drawing.Size(138, 20);
            this.gridLookUpEditFirstSound.TabIndex = 44;
            // 
            // gridView33
            // 
            this.gridView33.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn51});
            this.gridView33.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView33.Name = "gridView33";
            this.gridView33.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView33.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Нэрс";
            this.gridColumn51.FieldName = "name";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 0;
            // 
            // gridLookUpEditSecondSound
            // 
            this.gridLookUpEditSecondSound.Location = new System.Drawing.Point(257, 243);
            this.gridLookUpEditSecondSound.Name = "gridLookUpEditSecondSound";
            this.gridLookUpEditSecondSound.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSecondSound.Properties.DisplayMember = "name";
            this.gridLookUpEditSecondSound.Properties.NullText = "";
            this.gridLookUpEditSecondSound.Properties.ValueMember = "id";
            this.gridLookUpEditSecondSound.Properties.View = this.gridView34;
            this.gridLookUpEditSecondSound.Size = new System.Drawing.Size(138, 20);
            this.gridLookUpEditSecondSound.TabIndex = 43;
            // 
            // gridView34
            // 
            this.gridView34.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn52});
            this.gridView34.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView34.Name = "gridView34";
            this.gridView34.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView34.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Нэрс";
            this.gridColumn52.FieldName = "name";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 0;
            // 
            // gridLookUpEditThirdSound
            // 
            this.gridLookUpEditThirdSound.Location = new System.Drawing.Point(257, 269);
            this.gridLookUpEditThirdSound.Name = "gridLookUpEditThirdSound";
            this.gridLookUpEditThirdSound.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditThirdSound.Properties.DisplayMember = "name";
            this.gridLookUpEditThirdSound.Properties.NullText = "";
            this.gridLookUpEditThirdSound.Properties.ValueMember = "id";
            this.gridLookUpEditThirdSound.Properties.View = this.gridView35;
            this.gridLookUpEditThirdSound.Size = new System.Drawing.Size(138, 20);
            this.gridLookUpEditThirdSound.TabIndex = 45;
            // 
            // gridView35
            // 
            this.gridView35.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn53});
            this.gridView35.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView35.Name = "gridView35";
            this.gridView35.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView35.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Нэрс";
            this.gridColumn53.FieldName = "name";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 0;
            // 
            // radioGroupSameDouble
            // 
            this.radioGroupSameDouble.Location = new System.Drawing.Point(342, 174);
            this.radioGroupSameDouble.Name = "radioGroupSameDouble";
            this.radioGroupSameDouble.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Ижил"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Ижил бус")});
            this.radioGroupSameDouble.Size = new System.Drawing.Size(157, 20);
            this.radioGroupSameDouble.TabIndex = 35;
            // 
            // radioGroupTransmission
            // 
            this.radioGroupTransmission.Location = new System.Drawing.Point(428, 268);
            this.radioGroupTransmission.Name = "radioGroupTransmission";
            this.radioGroupTransmission.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Үл дамжина"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Дамжина")});
            this.radioGroupTransmission.Size = new System.Drawing.Size(165, 20);
            this.radioGroupTransmission.TabIndex = 53;
            // 
            // gridLookUpEditTransStrong
            // 
            this.gridLookUpEditTransStrong.Location = new System.Drawing.Point(428, 242);
            this.gridLookUpEditTransStrong.Name = "gridLookUpEditTransStrong";
            this.gridLookUpEditTransStrong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditTransStrong.Properties.DisplayMember = "name";
            this.gridLookUpEditTransStrong.Properties.NullText = "";
            this.gridLookUpEditTransStrong.Properties.ValueMember = "id";
            this.gridLookUpEditTransStrong.Properties.View = this.gridView36;
            this.gridLookUpEditTransStrong.Size = new System.Drawing.Size(100, 20);
            this.gridLookUpEditTransStrong.TabIndex = 52;
            // 
            // gridView36
            // 
            this.gridView36.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn50});
            this.gridView36.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView36.Name = "gridView36";
            this.gridView36.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView36.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Нэрс";
            this.gridColumn50.FieldName = "name";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 0;
            // 
            // radioGroupHeartLimit
            // 
            this.radioGroupHeartLimit.Location = new System.Drawing.Point(611, 118);
            this.radioGroupHeartLimit.Name = "radioGroupHeartLimit";
            this.radioGroupHeartLimit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хэвийн", true, "Хэвийн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Томорсон", true, "Томорсон")});
            this.radioGroupHeartLimit.Size = new System.Drawing.Size(145, 20);
            this.radioGroupHeartLimit.TabIndex = 19;
            // 
            // gridLookUpEditSezi
            // 
            this.gridLookUpEditSezi.Location = new System.Drawing.Point(611, 144);
            this.gridLookUpEditSezi.Name = "gridLookUpEditSezi";
            this.gridLookUpEditSezi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSezi.Properties.DisplayMember = "name";
            this.gridLookUpEditSezi.Properties.NullText = "";
            this.gridLookUpEditSezi.Properties.ValueMember = "id";
            this.gridLookUpEditSezi.Properties.View = this.gridView37;
            this.gridLookUpEditSezi.Size = new System.Drawing.Size(145, 20);
            this.gridLookUpEditSezi.TabIndex = 33;
            // 
            // gridView37
            // 
            this.gridView37.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn46});
            this.gridView37.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView37.Name = "gridView37";
            this.gridView37.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView37.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Нэрс";
            this.gridColumn46.FieldName = "name";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 0;
            // 
            // gridLookUpEditSis
            // 
            this.gridLookUpEditSis.Location = new System.Drawing.Point(611, 196);
            this.gridLookUpEditSis.Name = "gridLookUpEditSis";
            this.gridLookUpEditSis.Properties.AutoHeight = false;
            this.gridLookUpEditSis.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditSis.Properties.DisplayMember = "name";
            this.gridLookUpEditSis.Properties.NullText = "";
            this.gridLookUpEditSis.Properties.ValueMember = "id";
            this.gridLookUpEditSis.Properties.View = this.gridView38;
            this.gridLookUpEditSis.Size = new System.Drawing.Size(145, 20);
            this.gridLookUpEditSis.TabIndex = 49;
            // 
            // gridView38
            // 
            this.gridView38.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn48});
            this.gridView38.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView38.Name = "gridView38";
            this.gridView38.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView38.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Нэрс";
            this.gridColumn48.FieldName = "name";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 0;
            // 
            // gridLookUpEditPost
            // 
            this.gridLookUpEditPost.Location = new System.Drawing.Point(611, 170);
            this.gridLookUpEditPost.Name = "gridLookUpEditPost";
            this.gridLookUpEditPost.Properties.AutoHeight = false;
            this.gridLookUpEditPost.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPost.Properties.DisplayMember = "name";
            this.gridLookUpEditPost.Properties.NullText = "";
            this.gridLookUpEditPost.Properties.ValueMember = "id";
            this.gridLookUpEditPost.Properties.View = this.gridView39;
            this.gridLookUpEditPost.Size = new System.Drawing.Size(145, 20);
            this.gridLookUpEditPost.TabIndex = 48;
            // 
            // gridView39
            // 
            this.gridView39.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn47});
            this.gridView39.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView39.Name = "gridView39";
            this.gridView39.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView39.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Нэрс";
            this.gridColumn47.FieldName = "name";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.Visible = true;
            this.gridColumn47.VisibleIndex = 0;
            // 
            // gridLookUpEditDis
            // 
            this.gridLookUpEditDis.Location = new System.Drawing.Point(611, 222);
            this.gridLookUpEditDis.Name = "gridLookUpEditDis";
            this.gridLookUpEditDis.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDis.Properties.DisplayMember = "name";
            this.gridLookUpEditDis.Properties.NullText = "";
            this.gridLookUpEditDis.Properties.ValueMember = "id";
            this.gridLookUpEditDis.Properties.View = this.gridView40;
            this.gridLookUpEditDis.Size = new System.Drawing.Size(145, 20);
            this.gridLookUpEditDis.TabIndex = 50;
            // 
            // gridView40
            // 
            this.gridView40.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn49});
            this.gridView40.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView40.Name = "gridView40";
            this.gridView40.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView40.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Нэрс";
            this.gridColumn49.FieldName = "name";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 0;
            // 
            // radioGroupIsTouch
            // 
            this.radioGroupIsTouch.Location = new System.Drawing.Point(611, 268);
            this.radioGroupIsTouch.Name = "radioGroupIsTouch";
            this.radioGroupIsTouch.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsTouch.Size = new System.Drawing.Size(145, 20);
            this.radioGroupIsTouch.TabIndex = 51;
            // 
            // gridLookUpEditHeartRisk
            // 
            this.gridLookUpEditHeartRisk.Location = new System.Drawing.Point(20, 40);
            this.gridLookUpEditHeartRisk.Name = "gridLookUpEditHeartRisk";
            this.gridLookUpEditHeartRisk.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditHeartRisk.Properties.DisplayMember = "name";
            this.gridLookUpEditHeartRisk.Properties.NullText = "";
            this.gridLookUpEditHeartRisk.Properties.ValueMember = "id";
            this.gridLookUpEditHeartRisk.Properties.View = this.gridView41;
            this.gridLookUpEditHeartRisk.Size = new System.Drawing.Size(180, 20);
            this.gridLookUpEditHeartRisk.TabIndex = 14;
            // 
            // gridView41
            // 
            this.gridView41.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn43});
            this.gridView41.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView41.Name = "gridView41";
            this.gridView41.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView41.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Нэрс";
            this.gridColumn43.FieldName = "name";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 0;
            // 
            // radioGroupVenous
            // 
            this.radioGroupVenous.Location = new System.Drawing.Point(342, 48);
            this.radioGroupVenous.Name = "radioGroupVenous";
            this.radioGroupVenous.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Ажиглагдахгүй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Ажиглагдана")});
            this.radioGroupVenous.Size = new System.Drawing.Size(145, 45);
            this.radioGroupVenous.TabIndex = 15;
            // 
            // gridLookUpEditVoltage
            // 
            this.gridLookUpEditVoltage.Location = new System.Drawing.Point(342, 123);
            this.gridLookUpEditVoltage.Name = "gridLookUpEditVoltage";
            this.gridLookUpEditVoltage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditVoltage.Properties.DisplayMember = "name";
            this.gridLookUpEditVoltage.Properties.NullText = "";
            this.gridLookUpEditVoltage.Properties.ValueMember = "id";
            this.gridLookUpEditVoltage.Properties.View = this.gridView42;
            this.gridLookUpEditVoltage.Size = new System.Drawing.Size(145, 20);
            this.gridLookUpEditVoltage.TabIndex = 28;
            // 
            // gridView42
            // 
            this.gridView42.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn45});
            this.gridView42.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView42.Name = "gridView42";
            this.gridView42.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView42.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Нэрс";
            this.gridColumn45.FieldName = "name";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 0;
            // 
            // radioGroupFilled
            // 
            this.radioGroupFilled.Location = new System.Drawing.Point(342, 148);
            this.radioGroupFilled.Name = "radioGroupFilled";
            this.radioGroupFilled.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Дунд зэрэг"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Сул")});
            this.radioGroupFilled.Size = new System.Drawing.Size(157, 20);
            this.radioGroupFilled.TabIndex = 26;
            // 
            // radioGroupHeartPlace
            // 
            this.radioGroupHeartPlace.Location = new System.Drawing.Point(43, 108);
            this.radioGroupHeartPlace.Name = "radioGroupHeartPlace";
            this.radioGroupHeartPlace.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хэвийн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Хэвийн бус")});
            this.radioGroupHeartPlace.Size = new System.Drawing.Size(157, 20);
            this.radioGroupHeartPlace.TabIndex = 18;
            // 
            // radioGroupThisNoisy
            // 
            this.radioGroupThisNoisy.Location = new System.Drawing.Point(428, 199);
            this.radioGroupThisNoisy.Name = "radioGroupThisNoisy";
            this.radioGroupThisNoisy.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Шуугиангүй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Шуугиантай")});
            this.radioGroupThisNoisy.Size = new System.Drawing.Size(100, 40);
            this.radioGroupThisNoisy.TabIndex = 55;
            // 
            // gridLookUpEditStrong
            // 
            this.gridLookUpEditStrong.Location = new System.Drawing.Point(56, 134);
            this.gridLookUpEditStrong.Name = "gridLookUpEditStrong";
            this.gridLookUpEditStrong.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditStrong.Properties.DisplayMember = "name";
            this.gridLookUpEditStrong.Properties.NullText = "";
            this.gridLookUpEditStrong.Properties.ValueMember = "id";
            this.gridLookUpEditStrong.Properties.View = this.gridView43;
            this.gridLookUpEditStrong.Size = new System.Drawing.Size(144, 20);
            this.gridLookUpEditStrong.TabIndex = 27;
            // 
            // gridView43
            // 
            this.gridView43.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn44});
            this.gridView43.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView43.Name = "gridView43";
            this.gridView43.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView43.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Нэрс";
            this.gridColumn44.FieldName = "name";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 0;
            // 
            // xtraTabPageAnamnez5
            // 
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupTideConcern);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsTuck);
            this.xtraTabPageAnamnez5.Controls.Add(this.checkEditDigestive);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupSpleen);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl171);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl170);
            this.xtraTabPageAnamnez5.Controls.Add(this.textEditDuringDay);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl169);
            this.xtraTabPageAnamnez5.Controls.Add(this.gridLookUpEditLiver);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl168);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl167);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsTidePos);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsTideStable);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsCrossPos);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsCrossStable);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsPos);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsPosStable);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupCrossConcern);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupPosConcern);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl166);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl165);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl164);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl163);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl162);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl161);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl160);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl159);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl158);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl157);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl156);
            this.xtraTabPageAnamnez5.Controls.Add(this.textEditTongueShape);
            this.xtraTabPageAnamnez5.Controls.Add(this.textEditColor);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl155);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl154);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl153);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl152);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl151);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl150);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl149);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl148);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl147);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl146);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl145);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl144);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl143);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl142);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl141);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl140);
            this.xtraTabPageAnamnez5.Controls.Add(this.labelControl139);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsTongue);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsSkinWet);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupTuckConcern);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsTympanic);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsTough);
            this.xtraTabPageAnamnez5.Controls.Add(this.gridLookUpEditIsPercussion);
            this.xtraTabPageAnamnez5.Controls.Add(this.gridLookUpEditIsMoving);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupBellyConcern);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsBellyStable);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupIsBellyPos);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupLiverSize);
            this.xtraTabPageAnamnez5.Controls.Add(this.radioGroupLiverDirection);
            this.xtraTabPageAnamnez5.Name = "xtraTabPageAnamnez5";
            this.xtraTabPageAnamnez5.Size = new System.Drawing.Size(772, 303);
            this.xtraTabPageAnamnez5.Text = "Хоол боловсруулах тогтолцоо";
            // 
            // radioGroupTideConcern
            // 
            this.radioGroupTideConcern.Location = new System.Drawing.Point(392, 167);
            this.radioGroupTideConcern.Name = "radioGroupTideConcern";
            this.radioGroupTideConcern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Эмзэглэлгүй", true, true),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эмзэглэлтэй", true, false)});
            this.radioGroupTideConcern.Size = new System.Drawing.Size(100, 40);
            this.radioGroupTideConcern.TabIndex = 71;
            // 
            // radioGroupIsTuck
            // 
            this.radioGroupIsTuck.Location = new System.Drawing.Point(135, 104);
            this.radioGroupIsTuck.Name = "radioGroupIsTuck";
            this.radioGroupIsTuck.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Жигд", true, true),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Жигд бус", true, false)});
            this.radioGroupIsTuck.Size = new System.Drawing.Size(100, 40);
            this.radioGroupIsTuck.TabIndex = 70;
            // 
            // checkEditDigestive
            // 
            this.checkEditDigestive.Location = new System.Drawing.Point(10, 3);
            this.checkEditDigestive.Name = "checkEditDigestive";
            this.checkEditDigestive.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.checkEditDigestive.Properties.Caption = "Бөглөх";
            this.checkEditDigestive.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditDigestive.Size = new System.Drawing.Size(65, 21);
            this.checkEditDigestive.TabIndex = 69;
            this.checkEditDigestive.CheckedChanged += new System.EventHandler(this.checkEditDigestive_CheckedChanged);
            // 
            // radioGroupSpleen
            // 
            this.radioGroupSpleen.Location = new System.Drawing.Point(517, 246);
            this.radioGroupSpleen.Name = "radioGroupSpleen";
            this.radioGroupSpleen.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хэвийн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Томорсон")});
            this.radioGroupSpleen.Size = new System.Drawing.Size(100, 40);
            this.radioGroupSpleen.TabIndex = 67;
            // 
            // labelControl171
            // 
            this.labelControl171.Location = new System.Drawing.Point(517, 214);
            this.labelControl171.Name = "labelControl171";
            this.labelControl171.Size = new System.Drawing.Size(85, 26);
            this.labelControl171.TabIndex = 66;
            this.labelControl171.Text = "Дэлүүний хэмжээ \r\nтэмтрэлтээр:";
            // 
            // labelControl170
            // 
            this.labelControl170.Location = new System.Drawing.Point(517, 135);
            this.labelControl170.Name = "labelControl170";
            this.labelControl170.Size = new System.Drawing.Size(79, 26);
            this.labelControl170.TabIndex = 65;
            this.labelControl170.Text = "Элэгний хэмжээ\r\n тэмтрэлтээр:";
            // 
            // textEditDuringDay
            // 
            this.textEditDuringDay.Location = new System.Drawing.Point(628, 220);
            this.textEditDuringDay.Name = "textEditDuringDay";
            this.textEditDuringDay.Properties.MaxLength = 50;
            this.textEditDuringDay.Size = new System.Drawing.Size(130, 20);
            this.textEditDuringDay.TabIndex = 63;
            // 
            // labelControl169
            // 
            this.labelControl169.Location = new System.Drawing.Point(628, 201);
            this.labelControl169.Name = "labelControl169";
            this.labelControl169.Size = new System.Drawing.Size(66, 13);
            this.labelControl169.TabIndex = 62;
            this.labelControl169.Text = "Хүч/хугацаа:";
            // 
            // gridLookUpEditLiver
            // 
            this.gridLookUpEditLiver.Location = new System.Drawing.Point(628, 175);
            this.gridLookUpEditLiver.Name = "gridLookUpEditLiver";
            this.gridLookUpEditLiver.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditLiver.Properties.DisplayMember = "name";
            this.gridLookUpEditLiver.Properties.NullText = "";
            this.gridLookUpEditLiver.Properties.ValueMember = "id";
            this.gridLookUpEditLiver.Properties.View = this.gridView46;
            this.gridLookUpEditLiver.Size = new System.Drawing.Size(130, 20);
            this.gridLookUpEditLiver.TabIndex = 61;
            // 
            // gridView46
            // 
            this.gridView46.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn56});
            this.gridView46.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView46.Name = "gridView46";
            this.gridView46.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView46.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Нэрс";
            this.gridColumn56.FieldName = "name";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.Visible = true;
            this.gridColumn56.VisibleIndex = 0;
            // 
            // labelControl168
            // 
            this.labelControl168.Location = new System.Drawing.Point(628, 161);
            this.labelControl168.Name = "labelControl168";
            this.labelControl168.Size = new System.Drawing.Size(119, 13);
            this.labelControl168.TabIndex = 60;
            this.labelControl168.Text = "Элэгний шиж тэмдгүүд:";
            // 
            // labelControl167
            // 
            this.labelControl167.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl167.Location = new System.Drawing.Point(628, 143);
            this.labelControl167.Name = "labelControl167";
            this.labelControl167.Size = new System.Drawing.Size(82, 12);
            this.labelControl167.TabIndex = 59;
            this.labelControl167.Text = "Элэг цөс дэлүү:";
            // 
            // radioGroupIsTidePos
            // 
            this.radioGroupIsTidePos.Location = new System.Drawing.Point(392, 274);
            this.radioGroupIsTidePos.Name = "radioGroupIsTidePos";
            this.radioGroupIsTidePos.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsTidePos.Size = new System.Drawing.Size(100, 20);
            this.radioGroupIsTidePos.TabIndex = 58;
            // 
            // radioGroupIsTideStable
            // 
            this.radioGroupIsTideStable.Location = new System.Drawing.Point(392, 232);
            this.radioGroupIsTideStable.Name = "radioGroupIsTideStable";
            this.radioGroupIsTideStable.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хатуу"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Зөөлөн")});
            this.radioGroupIsTideStable.Size = new System.Drawing.Size(116, 20);
            this.radioGroupIsTideStable.TabIndex = 57;
            // 
            // radioGroupIsCrossPos
            // 
            this.radioGroupIsCrossPos.Location = new System.Drawing.Point(262, 274);
            this.radioGroupIsCrossPos.Name = "radioGroupIsCrossPos";
            this.radioGroupIsCrossPos.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsCrossPos.Size = new System.Drawing.Size(100, 20);
            this.radioGroupIsCrossPos.TabIndex = 56;
            // 
            // radioGroupIsCrossStable
            // 
            this.radioGroupIsCrossStable.Location = new System.Drawing.Point(262, 232);
            this.radioGroupIsCrossStable.Name = "radioGroupIsCrossStable";
            this.radioGroupIsCrossStable.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хатуу"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Зөөлөн")});
            this.radioGroupIsCrossStable.Size = new System.Drawing.Size(116, 20);
            this.radioGroupIsCrossStable.TabIndex = 55;
            // 
            // radioGroupIsPos
            // 
            this.radioGroupIsPos.Location = new System.Drawing.Point(135, 274);
            this.radioGroupIsPos.Name = "radioGroupIsPos";
            this.radioGroupIsPos.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsPos.Size = new System.Drawing.Size(100, 20);
            this.radioGroupIsPos.TabIndex = 54;
            // 
            // radioGroupIsPosStable
            // 
            this.radioGroupIsPosStable.Location = new System.Drawing.Point(135, 232);
            this.radioGroupIsPosStable.Name = "radioGroupIsPosStable";
            this.radioGroupIsPosStable.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хатуу"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Зөөлөн")});
            this.radioGroupIsPosStable.Size = new System.Drawing.Size(116, 20);
            this.radioGroupIsPosStable.TabIndex = 53;
            // 
            // radioGroupCrossConcern
            // 
            this.radioGroupCrossConcern.Location = new System.Drawing.Point(262, 167);
            this.radioGroupCrossConcern.Name = "radioGroupCrossConcern";
            this.radioGroupCrossConcern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Эмзэглэлгүй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эмзэглэлтэй")});
            this.radioGroupCrossConcern.Size = new System.Drawing.Size(100, 40);
            this.radioGroupCrossConcern.TabIndex = 51;
            // 
            // radioGroupPosConcern
            // 
            this.radioGroupPosConcern.Location = new System.Drawing.Point(135, 167);
            this.radioGroupPosConcern.Name = "radioGroupPosConcern";
            this.radioGroupPosConcern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Эмзэглэлгүй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эмзэглэлтэй")});
            this.radioGroupPosConcern.Size = new System.Drawing.Size(100, 40);
            this.radioGroupPosConcern.TabIndex = 50;
            // 
            // labelControl166
            // 
            this.labelControl166.Location = new System.Drawing.Point(392, 258);
            this.labelControl166.Name = "labelControl166";
            this.labelControl166.Size = new System.Drawing.Size(58, 13);
            this.labelControl166.TabIndex = 45;
            this.labelControl166.Text = "Хөдөлгөөн:";
            // 
            // labelControl165
            // 
            this.labelControl165.Location = new System.Drawing.Point(262, 258);
            this.labelControl165.Name = "labelControl165";
            this.labelControl165.Size = new System.Drawing.Size(58, 13);
            this.labelControl165.TabIndex = 44;
            this.labelControl165.Text = "Хөдөлгөөн:";
            // 
            // labelControl164
            // 
            this.labelControl164.Location = new System.Drawing.Point(135, 258);
            this.labelControl164.Name = "labelControl164";
            this.labelControl164.Size = new System.Drawing.Size(75, 13);
            this.labelControl164.TabIndex = 43;
            this.labelControl164.Text = "Хөдөлгөөнтэй:";
            // 
            // labelControl163
            // 
            this.labelControl163.Location = new System.Drawing.Point(8, 258);
            this.labelControl163.Name = "labelControl163";
            this.labelControl163.Size = new System.Drawing.Size(58, 13);
            this.labelControl163.TabIndex = 42;
            this.labelControl163.Text = "Хөдөлгөөн:";
            // 
            // labelControl162
            // 
            this.labelControl162.Location = new System.Drawing.Point(392, 213);
            this.labelControl162.Name = "labelControl162";
            this.labelControl162.Size = new System.Drawing.Size(35, 13);
            this.labelControl162.TabIndex = 38;
            this.labelControl162.Text = "Тогтоц";
            // 
            // labelControl161
            // 
            this.labelControl161.Location = new System.Drawing.Point(262, 213);
            this.labelControl161.Name = "labelControl161";
            this.labelControl161.Size = new System.Drawing.Size(35, 13);
            this.labelControl161.TabIndex = 37;
            this.labelControl161.Text = "Тогтоц";
            // 
            // labelControl160
            // 
            this.labelControl160.Location = new System.Drawing.Point(135, 213);
            this.labelControl160.Name = "labelControl160";
            this.labelControl160.Size = new System.Drawing.Size(35, 13);
            this.labelControl160.TabIndex = 36;
            this.labelControl160.Text = "Тогтоц";
            // 
            // labelControl159
            // 
            this.labelControl159.Location = new System.Drawing.Point(385, 148);
            this.labelControl159.Name = "labelControl159";
            this.labelControl159.Size = new System.Drawing.Size(128, 13);
            this.labelControl159.TabIndex = 31;
            this.labelControl159.Text = "Цутгалан гэдэс: Байрлал";
            // 
            // labelControl158
            // 
            this.labelControl158.Location = new System.Drawing.Point(252, 148);
            this.labelControl158.Name = "labelControl158";
            this.labelControl158.Size = new System.Drawing.Size(122, 13);
            this.labelControl158.TabIndex = 30;
            this.labelControl158.Text = "Хөндлөн гэдэс: Байрлал";
            // 
            // labelControl157
            // 
            this.labelControl157.Location = new System.Drawing.Point(10, 213);
            this.labelControl157.Name = "labelControl157";
            this.labelControl157.Size = new System.Drawing.Size(35, 13);
            this.labelControl157.TabIndex = 29;
            this.labelControl157.Text = "Тогтоц";
            // 
            // labelControl156
            // 
            this.labelControl156.Location = new System.Drawing.Point(628, 98);
            this.labelControl156.Name = "labelControl156";
            this.labelControl156.Size = new System.Drawing.Size(38, 13);
            this.labelControl156.TabIndex = 28;
            this.labelControl156.Text = "Хэлбэр:";
            // 
            // textEditTongueShape
            // 
            this.textEditTongueShape.Location = new System.Drawing.Point(628, 117);
            this.textEditTongueShape.Name = "textEditTongueShape";
            this.textEditTongueShape.Properties.MaxLength = 50;
            this.textEditTongueShape.Size = new System.Drawing.Size(130, 20);
            this.textEditTongueShape.TabIndex = 27;
            // 
            // textEditColor
            // 
            this.textEditColor.Location = new System.Drawing.Point(135, 59);
            this.textEditColor.Name = "textEditColor";
            this.textEditColor.Properties.MaxLength = 50;
            this.textEditColor.Size = new System.Drawing.Size(100, 20);
            this.textEditColor.TabIndex = 25;
            // 
            // labelControl155
            // 
            this.labelControl155.Location = new System.Drawing.Point(135, 85);
            this.labelControl155.Name = "labelControl155";
            this.labelControl155.Size = new System.Drawing.Size(62, 13);
            this.labelControl155.TabIndex = 22;
            this.labelControl155.Text = "Хэвлийн-хэм";
            // 
            // labelControl154
            // 
            this.labelControl154.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl154.Location = new System.Drawing.Point(8, 130);
            this.labelControl154.Name = "labelControl154";
            this.labelControl154.Size = new System.Drawing.Size(108, 12);
            this.labelControl154.TabIndex = 21;
            this.labelControl154.Text = "Гүнзгий тэмтрэлтээр:";
            // 
            // labelControl153
            // 
            this.labelControl153.Location = new System.Drawing.Point(262, 85);
            this.labelControl153.Name = "labelControl153";
            this.labelControl153.Size = new System.Drawing.Size(169, 13);
            this.labelControl153.TabIndex = 20;
            this.labelControl153.Text = "Бучингийн чангарал байгаа эсэх:";
            // 
            // labelControl152
            // 
            this.labelControl152.Location = new System.Drawing.Point(135, 40);
            this.labelControl152.Name = "labelControl152";
            this.labelControl152.Size = new System.Drawing.Size(29, 13);
            this.labelControl152.TabIndex = 19;
            this.labelControl152.Text = "Өнгө:";
            // 
            // labelControl151
            // 
            this.labelControl151.Appearance.Font = new System.Drawing.Font("Tahoma", 7F);
            this.labelControl151.Location = new System.Drawing.Point(135, 146);
            this.labelControl151.Name = "labelControl151";
            this.labelControl151.Size = new System.Drawing.Size(92, 22);
            this.labelControl151.TabIndex = 18;
            this.labelControl151.Text = "Өсгөх болон уруудах \r\nгэдэс: байрлал";
            // 
            // labelControl150
            // 
            this.labelControl150.Location = new System.Drawing.Point(8, 148);
            this.labelControl150.Name = "labelControl150";
            this.labelControl150.Size = new System.Drawing.Size(109, 13);
            this.labelControl150.TabIndex = 17;
            this.labelControl150.Text = "Тахир гэдэс- байрлал";
            // 
            // labelControl149
            // 
            this.labelControl149.Location = new System.Drawing.Point(457, 85);
            this.labelControl149.Name = "labelControl149";
            this.labelControl149.Size = new System.Drawing.Size(160, 13);
            this.labelControl149.TabIndex = 16;
            this.labelControl149.Text = "Ихэссэн хэсэгт тогшилтын дуу:";
            // 
            // labelControl148
            // 
            this.labelControl148.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl148.Location = new System.Drawing.Point(98, 6);
            this.labelControl148.Name = "labelControl148";
            this.labelControl148.Size = new System.Drawing.Size(183, 13);
            this.labelControl148.TabIndex = 9;
            this.labelControl148.Text = "Хоол боловсруулах тогтолцоо:";
            // 
            // labelControl147
            // 
            this.labelControl147.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl147.Location = new System.Drawing.Point(262, 22);
            this.labelControl147.Name = "labelControl147";
            this.labelControl147.Size = new System.Drawing.Size(100, 12);
            this.labelControl147.TabIndex = 8;
            this.labelControl147.Text = "Өнгөц тэмтрэлтээр:";
            // 
            // labelControl146
            // 
            this.labelControl146.Location = new System.Drawing.Point(630, 40);
            this.labelControl146.Name = "labelControl146";
            this.labelControl146.Size = new System.Drawing.Size(98, 26);
            this.labelControl146.TabIndex = 7;
            this.labelControl146.Text = "Гэдэсний гүрвэлзэх \r\nхөдөлгөөн:";
            // 
            // labelControl145
            // 
            this.labelControl145.Location = new System.Drawing.Point(262, 40);
            this.labelControl145.Name = "labelControl145";
            this.labelControl145.Size = new System.Drawing.Size(132, 13);
            this.labelControl145.TabIndex = 6;
            this.labelControl145.Text = "Хэвлийн эмзэглэлтэй эсэх:";
            // 
            // labelControl144
            // 
            this.labelControl144.Location = new System.Drawing.Point(10, 85);
            this.labelControl144.Name = "labelControl144";
            this.labelControl144.Size = new System.Drawing.Size(102, 13);
            this.labelControl144.TabIndex = 5;
            this.labelControl144.Text = "Арьс, салст-чийглэг";
            // 
            // labelControl143
            // 
            this.labelControl143.Location = new System.Drawing.Point(10, 40);
            this.labelControl143.Name = "labelControl143";
            this.labelControl143.Size = new System.Drawing.Size(94, 13);
            this.labelControl143.TabIndex = 4;
            this.labelControl143.Text = "Хэл өнгөртэй эсэх:";
            // 
            // labelControl142
            // 
            this.labelControl142.Location = new System.Drawing.Point(457, 40);
            this.labelControl142.Name = "labelControl142";
            this.labelControl142.Size = new System.Drawing.Size(128, 13);
            this.labelControl142.TabIndex = 3;
            this.labelControl142.Text = "Хэвлийн хэнгрэгэн чимээ:";
            // 
            // labelControl141
            // 
            this.labelControl141.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl141.Location = new System.Drawing.Point(628, 22);
            this.labelControl141.Name = "labelControl141";
            this.labelControl141.Size = new System.Drawing.Size(61, 12);
            this.labelControl141.TabIndex = 2;
            this.labelControl141.Text = "Чагналтаар";
            // 
            // labelControl140
            // 
            this.labelControl140.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl140.Location = new System.Drawing.Point(457, 22);
            this.labelControl140.Name = "labelControl140";
            this.labelControl140.Size = new System.Drawing.Size(56, 12);
            this.labelControl140.TabIndex = 1;
            this.labelControl140.Text = "Тогшитоор";
            // 
            // labelControl139
            // 
            this.labelControl139.Appearance.Font = new System.Drawing.Font("Tahoma", 7.25F, System.Drawing.FontStyle.Bold);
            this.labelControl139.Location = new System.Drawing.Point(10, 22);
            this.labelControl139.Name = "labelControl139";
            this.labelControl139.Size = new System.Drawing.Size(83, 12);
            this.labelControl139.TabIndex = 0;
            this.labelControl139.Text = "Харж ажиглах:";
            // 
            // radioGroupIsTongue
            // 
            this.radioGroupIsTongue.Location = new System.Drawing.Point(9, 59);
            this.radioGroupIsTongue.Name = "radioGroupIsTongue";
            this.radioGroupIsTongue.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм", true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй", true, "Үгүй")});
            this.radioGroupIsTongue.Size = new System.Drawing.Size(100, 20);
            this.radioGroupIsTongue.TabIndex = 10;
            // 
            // radioGroupIsSkinWet
            // 
            this.radioGroupIsSkinWet.Location = new System.Drawing.Point(9, 104);
            this.radioGroupIsSkinWet.Name = "radioGroupIsSkinWet";
            this.radioGroupIsSkinWet.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsSkinWet.Size = new System.Drawing.Size(100, 20);
            this.radioGroupIsSkinWet.TabIndex = 15;
            // 
            // radioGroupTuckConcern
            // 
            this.radioGroupTuckConcern.Location = new System.Drawing.Point(262, 58);
            this.radioGroupTuckConcern.Name = "radioGroupTuckConcern";
            this.radioGroupTuckConcern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Эмзэглэлгүй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эмзэглэлтэй")});
            this.radioGroupTuckConcern.Size = new System.Drawing.Size(169, 20);
            this.radioGroupTuckConcern.TabIndex = 11;
            // 
            // radioGroupIsTympanic
            // 
            this.radioGroupIsTympanic.Location = new System.Drawing.Point(457, 58);
            this.radioGroupIsTympanic.Name = "radioGroupIsTympanic";
            this.radioGroupIsTympanic.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хэвийн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Ихэссэн")});
            this.radioGroupIsTympanic.Size = new System.Drawing.Size(128, 20);
            this.radioGroupIsTympanic.TabIndex = 12;
            // 
            // radioGroupIsTough
            // 
            this.radioGroupIsTough.Location = new System.Drawing.Point(262, 104);
            this.radioGroupIsTough.Name = "radioGroupIsTough";
            this.radioGroupIsTough.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsTough.Size = new System.Drawing.Size(100, 20);
            this.radioGroupIsTough.TabIndex = 14;
            // 
            // gridLookUpEditIsPercussion
            // 
            this.gridLookUpEditIsPercussion.Location = new System.Drawing.Point(457, 104);
            this.gridLookUpEditIsPercussion.Name = "gridLookUpEditIsPercussion";
            this.gridLookUpEditIsPercussion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditIsPercussion.Properties.DisplayMember = "name";
            this.gridLookUpEditIsPercussion.Properties.NullText = "";
            this.gridLookUpEditIsPercussion.Properties.ValueMember = "id";
            this.gridLookUpEditIsPercussion.Properties.View = this.gridView44;
            this.gridLookUpEditIsPercussion.Size = new System.Drawing.Size(128, 20);
            this.gridLookUpEditIsPercussion.TabIndex = 23;
            // 
            // gridView44
            // 
            this.gridView44.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn54});
            this.gridView44.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView44.Name = "gridView44";
            this.gridView44.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView44.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Нэрс";
            this.gridColumn54.FieldName = "name";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.Visible = true;
            this.gridColumn54.VisibleIndex = 0;
            // 
            // gridLookUpEditIsMoving
            // 
            this.gridLookUpEditIsMoving.Location = new System.Drawing.Point(628, 72);
            this.gridLookUpEditIsMoving.Name = "gridLookUpEditIsMoving";
            this.gridLookUpEditIsMoving.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditIsMoving.Properties.DisplayMember = "name";
            this.gridLookUpEditIsMoving.Properties.NullText = "";
            this.gridLookUpEditIsMoving.Properties.ValueMember = "id";
            this.gridLookUpEditIsMoving.Properties.View = this.gridView45;
            this.gridLookUpEditIsMoving.Size = new System.Drawing.Size(130, 20);
            this.gridLookUpEditIsMoving.TabIndex = 13;
            // 
            // gridView45
            // 
            this.gridView45.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn55});
            this.gridView45.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView45.Name = "gridView45";
            this.gridView45.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView45.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Нэрс";
            this.gridColumn55.FieldName = "name";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 0;
            // 
            // radioGroupBellyConcern
            // 
            this.radioGroupBellyConcern.Location = new System.Drawing.Point(9, 167);
            this.radioGroupBellyConcern.Name = "radioGroupBellyConcern";
            this.radioGroupBellyConcern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Эмзэглэлгүй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эмзэглэлтэй")});
            this.radioGroupBellyConcern.Size = new System.Drawing.Size(100, 40);
            this.radioGroupBellyConcern.TabIndex = 32;
            // 
            // radioGroupIsBellyStable
            // 
            this.radioGroupIsBellyStable.Location = new System.Drawing.Point(9, 232);
            this.radioGroupIsBellyStable.Name = "radioGroupIsBellyStable";
            this.radioGroupIsBellyStable.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хатуу"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Зөөлөн")});
            this.radioGroupIsBellyStable.Size = new System.Drawing.Size(116, 20);
            this.radioGroupIsBellyStable.TabIndex = 24;
            // 
            // radioGroupIsBellyPos
            // 
            this.radioGroupIsBellyPos.Location = new System.Drawing.Point(8, 274);
            this.radioGroupIsBellyPos.Name = "radioGroupIsBellyPos";
            this.radioGroupIsBellyPos.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsBellyPos.Size = new System.Drawing.Size(100, 20);
            this.radioGroupIsBellyPos.TabIndex = 46;
            // 
            // radioGroupLiverSize
            // 
            this.radioGroupLiverSize.Location = new System.Drawing.Point(517, 167);
            this.radioGroupLiverSize.Name = "radioGroupLiverSize";
            this.radioGroupLiverSize.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хэвийн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Томорсон")});
            this.radioGroupLiverSize.Size = new System.Drawing.Size(100, 40);
            this.radioGroupLiverSize.TabIndex = 64;
            // 
            // radioGroupLiverDirection
            // 
            this.radioGroupLiverDirection.Location = new System.Drawing.Point(628, 246);
            this.radioGroupLiverDirection.Name = "radioGroupLiverDirection";
            this.radioGroupLiverDirection.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Баруун дэлбэн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Зүүн дэлбэн")});
            this.radioGroupLiverDirection.Size = new System.Drawing.Size(130, 40);
            this.radioGroupLiverDirection.TabIndex = 68;
            // 
            // xtraTabPageAnamnez6
            // 
            this.xtraTabPageAnamnez6.Controls.Add(this.checkEditUrine);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl193);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl192);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl191);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl190);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl189);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl188);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl187);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl186);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl185);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl184);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl183);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl182);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl181);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl180);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl179);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl178);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl177);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupKidneyConcern);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl175);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl174);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl173);
            this.xtraTabPageAnamnez6.Controls.Add(this.labelControl172);
            this.xtraTabPageAnamnez6.Controls.Add(this.memoEditOther);
            this.xtraTabPageAnamnez6.Controls.Add(this.memoEditMentalStatus);
            this.xtraTabPageAnamnez6.Controls.Add(this.gridLookUpEditShallow);
            this.xtraTabPageAnamnez6.Controls.Add(this.gridLookUpeditDeep);
            this.xtraTabPageAnamnez6.Controls.Add(this.gridLookUpEditQuiteDeep);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupReflex);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupFaceDouble);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupSmell);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupHear);
            this.xtraTabPageAnamnez6.Controls.Add(this.gridLookUpEditUrineOutPut);
            this.xtraTabPageAnamnez6.Controls.Add(this.gridLookUpEditUrineColor);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupIsUrineNight);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupPushUrine);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupHurtUrine);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupKidney);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupPastyernatskii);
            this.xtraTabPageAnamnez6.Controls.Add(this.spinEditIsUrineRight);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupCutUrine);
            this.xtraTabPageAnamnez6.Controls.Add(this.radioGroupMissUrine);
            this.xtraTabPageAnamnez6.Name = "xtraTabPageAnamnez6";
            this.xtraTabPageAnamnez6.Size = new System.Drawing.Size(772, 303);
            this.xtraTabPageAnamnez6.Text = "Шээс бэлгийн тогтолцоо";
            // 
            // checkEditUrine
            // 
            this.checkEditUrine.Location = new System.Drawing.Point(10, 3);
            this.checkEditUrine.Name = "checkEditUrine";
            this.checkEditUrine.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.checkEditUrine.Properties.Caption = "Бөглөх";
            this.checkEditUrine.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditUrine.Size = new System.Drawing.Size(64, 21);
            this.checkEditUrine.TabIndex = 41;
            this.checkEditUrine.CheckedChanged += new System.EventHandler(this.checkEditUrine_CheckedChanged);
            // 
            // labelControl193
            // 
            this.labelControl193.Location = new System.Drawing.Point(332, 67);
            this.labelControl193.Name = "labelControl193";
            this.labelControl193.Size = new System.Drawing.Size(70, 13);
            this.labelControl193.TabIndex = 40;
            this.labelControl193.Text = "Тийм бол тоо:";
            // 
            // labelControl192
            // 
            this.labelControl192.Location = new System.Drawing.Point(524, 231);
            this.labelControl192.Name = "labelControl192";
            this.labelControl192.Size = new System.Drawing.Size(236, 13);
            this.labelControl192.TabIndex = 20;
            this.labelControl192.Text = "Бусад:(Арьс, үе, мөч, тунгалагийн тогтолцоо)";
            // 
            // labelControl191
            // 
            this.labelControl191.Location = new System.Drawing.Point(8, 231);
            this.labelControl191.Name = "labelControl191";
            this.labelControl191.Size = new System.Drawing.Size(96, 13);
            this.labelControl191.TabIndex = 19;
            this.labelControl191.Text = "Сэтгэцийн байдал:";
            // 
            // labelControl190
            // 
            this.labelControl190.Location = new System.Drawing.Point(298, 177);
            this.labelControl190.Name = "labelControl190";
            this.labelControl190.Size = new System.Drawing.Size(64, 13);
            this.labelControl190.TabIndex = 18;
            this.labelControl190.Text = "Рефлексүүд:";
            // 
            // labelControl189
            // 
            this.labelControl189.Location = new System.Drawing.Point(660, 153);
            this.labelControl189.Name = "labelControl189";
            this.labelControl189.Size = new System.Drawing.Size(78, 13);
            this.labelControl189.TabIndex = 17;
            this.labelControl189.Text = "Хэт мэдрэгшил:";
            // 
            // labelControl188
            // 
            this.labelControl188.Location = new System.Drawing.Point(554, 151);
            this.labelControl188.Name = "labelControl188";
            this.labelControl188.Size = new System.Drawing.Size(21, 13);
            this.labelControl188.TabIndex = 16;
            this.labelControl188.Text = "Гүн:";
            // 
            // labelControl187
            // 
            this.labelControl187.Location = new System.Drawing.Point(448, 151);
            this.labelControl187.Name = "labelControl187";
            this.labelControl187.Size = new System.Drawing.Size(35, 13);
            this.labelControl187.TabIndex = 15;
            this.labelControl187.Text = "Өнгөц:";
            // 
            // labelControl186
            // 
            this.labelControl186.Location = new System.Drawing.Point(448, 132);
            this.labelControl186.Name = "labelControl186";
            this.labelControl186.Size = new System.Drawing.Size(48, 13);
            this.labelControl186.TabIndex = 14;
            this.labelControl186.Text = "Мэдрэхүй";
            // 
            // labelControl185
            // 
            this.labelControl185.Location = new System.Drawing.Point(298, 132);
            this.labelControl185.Name = "labelControl185";
            this.labelControl185.Size = new System.Drawing.Size(75, 13);
            this.labelControl185.TabIndex = 13;
            this.labelControl185.Text = "Нүүрний 2 тал:";
            // 
            // labelControl184
            // 
            this.labelControl184.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl184.Location = new System.Drawing.Point(10, 113);
            this.labelControl184.Name = "labelControl184";
            this.labelControl184.Size = new System.Drawing.Size(130, 13);
            this.labelControl184.TabIndex = 12;
            this.labelControl184.Text = "Мэдрэлийн тогтолцоо";
            // 
            // labelControl183
            // 
            this.labelControl183.Location = new System.Drawing.Point(172, 132);
            this.labelControl183.Name = "labelControl183";
            this.labelControl183.Size = new System.Drawing.Size(105, 13);
            this.labelControl183.TabIndex = 11;
            this.labelControl183.Text = "Сонсголын мэдрэмж:";
            // 
            // labelControl182
            // 
            this.labelControl182.Location = new System.Drawing.Point(10, 132);
            this.labelControl182.Name = "labelControl182";
            this.labelControl182.Size = new System.Drawing.Size(90, 13);
            this.labelControl182.TabIndex = 10;
            this.labelControl182.Text = "Үнэрлэх мэдрэмж:";
            // 
            // labelControl181
            // 
            this.labelControl181.Location = new System.Drawing.Point(634, 87);
            this.labelControl181.Name = "labelControl181";
            this.labelControl181.Size = new System.Drawing.Size(72, 13);
            this.labelControl181.TabIndex = 9;
            this.labelControl181.Text = "Пастернацкий";
            // 
            // labelControl180
            // 
            this.labelControl180.Location = new System.Drawing.Point(489, 74);
            this.labelControl180.Name = "labelControl180";
            this.labelControl180.Size = new System.Drawing.Size(88, 26);
            this.labelControl180.TabIndex = 8;
            this.labelControl180.Text = "Шээхэд давсгаар\r\n өвддөг эсэх:";
            // 
            // labelControl179
            // 
            this.labelControl179.Location = new System.Drawing.Point(10, 67);
            this.labelControl179.Name = "labelControl179";
            this.labelControl179.Size = new System.Drawing.Size(103, 13);
            this.labelControl179.TabIndex = 7;
            this.labelControl179.Text = "Шээс тасалдаг эсэх:";
            // 
            // labelControl178
            // 
            this.labelControl178.Location = new System.Drawing.Point(172, 67);
            this.labelControl178.Name = "labelControl178";
            this.labelControl178.Size = new System.Drawing.Size(98, 13);
            this.labelControl178.TabIndex = 6;
            this.labelControl178.Text = "Дутуу шээдэг эсэх:";
            // 
            // labelControl177
            // 
            this.labelControl177.Location = new System.Drawing.Point(489, 22);
            this.labelControl177.Name = "labelControl177";
            this.labelControl177.Size = new System.Drawing.Size(93, 13);
            this.labelControl177.TabIndex = 5;
            this.labelControl177.Text = "Дүлж шээдэг эсэх:";
            // 
            // radioGroupKidneyConcern
            // 
            this.radioGroupKidneyConcern.Location = new System.Drawing.Point(634, 22);
            this.radioGroupKidneyConcern.Name = "radioGroupKidneyConcern";
            this.radioGroupKidneyConcern.Size = new System.Drawing.Size(93, 13);
            this.radioGroupKidneyConcern.TabIndex = 4;
            this.radioGroupKidneyConcern.Text = "Бөөр тэмтрэлтээр:";
            // 
            // labelControl175
            // 
            this.labelControl175.Location = new System.Drawing.Point(10, 22);
            this.labelControl175.Name = "labelControl175";
            this.labelControl175.Size = new System.Drawing.Size(121, 13);
            this.labelControl175.TabIndex = 3;
            this.labelControl175.Text = "Хоногийн шээсний гарц:";
            // 
            // labelControl174
            // 
            this.labelControl174.Location = new System.Drawing.Point(172, 22);
            this.labelControl174.Name = "labelControl174";
            this.labelControl174.Size = new System.Drawing.Size(73, 13);
            this.labelControl174.TabIndex = 2;
            this.labelControl174.Text = "Шээсний өнгө:";
            // 
            // labelControl173
            // 
            this.labelControl173.Location = new System.Drawing.Point(332, 22);
            this.labelControl173.Name = "labelControl173";
            this.labelControl173.Size = new System.Drawing.Size(94, 13);
            this.labelControl173.TabIndex = 1;
            this.labelControl173.Text = "Шөнө шээдэг эсэх:";
            // 
            // labelControl172
            // 
            this.labelControl172.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl172.Location = new System.Drawing.Point(98, 3);
            this.labelControl172.Name = "labelControl172";
            this.labelControl172.Size = new System.Drawing.Size(143, 13);
            this.labelControl172.TabIndex = 0;
            this.labelControl172.Text = "Шээс бэлгийн тогтолцоо";
            // 
            // memoEditOther
            // 
            this.memoEditOther.Location = new System.Drawing.Point(380, 250);
            this.memoEditOther.Name = "memoEditOther";
            this.memoEditOther.Properties.MaxLength = 200;
            this.memoEditOther.Size = new System.Drawing.Size(380, 40);
            this.memoEditOther.TabIndex = 38;
            // 
            // memoEditMentalStatus
            // 
            this.memoEditMentalStatus.Location = new System.Drawing.Point(8, 250);
            this.memoEditMentalStatus.Name = "memoEditMentalStatus";
            this.memoEditMentalStatus.Properties.MaxLength = 200;
            this.memoEditMentalStatus.Size = new System.Drawing.Size(360, 40);
            this.memoEditMentalStatus.TabIndex = 37;
            // 
            // gridLookUpEditShallow
            // 
            this.gridLookUpEditShallow.Location = new System.Drawing.Point(448, 170);
            this.gridLookUpEditShallow.Name = "gridLookUpEditShallow";
            this.gridLookUpEditShallow.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditShallow.Properties.DisplayMember = "name";
            this.gridLookUpEditShallow.Properties.NullText = "";
            this.gridLookUpEditShallow.Properties.ValueMember = "id";
            this.gridLookUpEditShallow.Properties.View = this.gridView47;
            this.gridLookUpEditShallow.Size = new System.Drawing.Size(100, 20);
            this.gridLookUpEditShallow.TabIndex = 34;
            // 
            // gridView47
            // 
            this.gridView47.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn59});
            this.gridView47.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView47.Name = "gridView47";
            this.gridView47.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView47.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Нэрс";
            this.gridColumn59.FieldName = "name";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 0;
            // 
            // gridLookUpeditDeep
            // 
            this.gridLookUpeditDeep.Location = new System.Drawing.Point(554, 170);
            this.gridLookUpeditDeep.Name = "gridLookUpeditDeep";
            this.gridLookUpeditDeep.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpeditDeep.Properties.DisplayMember = "name";
            this.gridLookUpeditDeep.Properties.NullText = "";
            this.gridLookUpeditDeep.Properties.ValueMember = "id";
            this.gridLookUpeditDeep.Properties.View = this.gridView48;
            this.gridLookUpeditDeep.Size = new System.Drawing.Size(100, 20);
            this.gridLookUpeditDeep.TabIndex = 35;
            // 
            // gridView48
            // 
            this.gridView48.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn60});
            this.gridView48.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView48.Name = "gridView48";
            this.gridView48.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView48.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Нэрс";
            this.gridColumn60.FieldName = "name";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 0;
            // 
            // gridLookUpEditQuiteDeep
            // 
            this.gridLookUpEditQuiteDeep.Location = new System.Drawing.Point(660, 170);
            this.gridLookUpEditQuiteDeep.Name = "gridLookUpEditQuiteDeep";
            this.gridLookUpEditQuiteDeep.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditQuiteDeep.Properties.DisplayMember = "name";
            this.gridLookUpEditQuiteDeep.Properties.NullText = "";
            this.gridLookUpEditQuiteDeep.Properties.ValueMember = "id";
            this.gridLookUpEditQuiteDeep.Properties.View = this.gridView49;
            this.gridLookUpEditQuiteDeep.Size = new System.Drawing.Size(100, 20);
            this.gridLookUpEditQuiteDeep.TabIndex = 36;
            // 
            // gridView49
            // 
            this.gridView49.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn61});
            this.gridView49.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView49.Name = "gridView49";
            this.gridView49.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView49.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Нэрс";
            this.gridColumn61.FieldName = "name";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.Visible = true;
            this.gridColumn61.VisibleIndex = 0;
            // 
            // radioGroupReflex
            // 
            this.radioGroupReflex.Location = new System.Drawing.Point(298, 195);
            this.radioGroupReflex.Name = "radioGroupReflex";
            this.radioGroupReflex.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Хадгалагдана"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Хадгалагдаагүй")});
            this.radioGroupReflex.Size = new System.Drawing.Size(142, 40);
            this.radioGroupReflex.TabIndex = 33;
            // 
            // radioGroupFaceDouble
            // 
            this.radioGroupFaceDouble.Location = new System.Drawing.Point(298, 153);
            this.radioGroupFaceDouble.Name = "radioGroupFaceDouble";
            this.radioGroupFaceDouble.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Ижил"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Ижил бус")});
            this.radioGroupFaceDouble.Size = new System.Drawing.Size(142, 20);
            this.radioGroupFaceDouble.TabIndex = 32;
            // 
            // radioGroupSmell
            // 
            this.radioGroupSmell.Location = new System.Drawing.Point(10, 151);
            this.radioGroupSmell.Name = "radioGroupSmell";
            this.radioGroupSmell.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(1)), "Хэвийн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(2)), "Буурсан"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(3)), "Ялгахгүй")});
            this.radioGroupSmell.Size = new System.Drawing.Size(120, 60);
            this.radioGroupSmell.TabIndex = 30;
            // 
            // radioGroupHear
            // 
            this.radioGroupHear.Location = new System.Drawing.Point(172, 151);
            this.radioGroupHear.Name = "radioGroupHear";
            this.radioGroupHear.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(1)), "Хэвийн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(2)), "Ихэссэн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(3)), "Буурсан")});
            this.radioGroupHear.Size = new System.Drawing.Size(120, 60);
            this.radioGroupHear.TabIndex = 31;
            // 
            // gridLookUpEditUrineOutPut
            // 
            this.gridLookUpEditUrineOutPut.Location = new System.Drawing.Point(10, 41);
            this.gridLookUpEditUrineOutPut.Name = "gridLookUpEditUrineOutPut";
            this.gridLookUpEditUrineOutPut.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUrineOutPut.Properties.DisplayMember = "name";
            this.gridLookUpEditUrineOutPut.Properties.NullText = "";
            this.gridLookUpEditUrineOutPut.Properties.ValueMember = "id";
            this.gridLookUpEditUrineOutPut.Properties.View = this.gridView50;
            this.gridLookUpEditUrineOutPut.Size = new System.Drawing.Size(120, 20);
            this.gridLookUpEditUrineOutPut.TabIndex = 21;
            // 
            // gridView50
            // 
            this.gridView50.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn57});
            this.gridView50.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView50.Name = "gridView50";
            this.gridView50.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView50.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Нэрс";
            this.gridColumn57.FieldName = "name";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 0;
            // 
            // gridLookUpEditUrineColor
            // 
            this.gridLookUpEditUrineColor.Location = new System.Drawing.Point(172, 41);
            this.gridLookUpEditUrineColor.Name = "gridLookUpEditUrineColor";
            this.gridLookUpEditUrineColor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUrineColor.Properties.DisplayMember = "name";
            this.gridLookUpEditUrineColor.Properties.NullText = "";
            this.gridLookUpEditUrineColor.Properties.ValueMember = "id";
            this.gridLookUpEditUrineColor.Properties.View = this.gridView51;
            this.gridLookUpEditUrineColor.Size = new System.Drawing.Size(120, 20);
            this.gridLookUpEditUrineColor.TabIndex = 22;
            // 
            // gridView51
            // 
            this.gridView51.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn58});
            this.gridView51.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView51.Name = "gridView51";
            this.gridView51.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView51.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Нэрс";
            this.gridColumn58.FieldName = "name";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 0;
            // 
            // radioGroupIsUrineNight
            // 
            this.radioGroupIsUrineNight.Location = new System.Drawing.Point(332, 40);
            this.radioGroupIsUrineNight.Name = "radioGroupIsUrineNight";
            this.radioGroupIsUrineNight.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupIsUrineNight.Size = new System.Drawing.Size(120, 20);
            this.radioGroupIsUrineNight.TabIndex = 23;
            // 
            // radioGroupPushUrine
            // 
            this.radioGroupPushUrine.Location = new System.Drawing.Point(489, 41);
            this.radioGroupPushUrine.Name = "radioGroupPushUrine";
            this.radioGroupPushUrine.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupPushUrine.Size = new System.Drawing.Size(120, 20);
            this.radioGroupPushUrine.TabIndex = 24;
            // 
            // radioGroupHurtUrine
            // 
            this.radioGroupHurtUrine.Location = new System.Drawing.Point(489, 106);
            this.radioGroupHurtUrine.Name = "radioGroupHurtUrine";
            this.radioGroupHurtUrine.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupHurtUrine.Size = new System.Drawing.Size(120, 20);
            this.radioGroupHurtUrine.TabIndex = 28;
            // 
            // radioGroupKidney
            // 
            this.radioGroupKidney.Location = new System.Drawing.Point(634, 38);
            this.radioGroupKidney.Name = "radioGroupKidney";
            this.radioGroupKidney.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Эмзэглэлгүй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Эмзэглэлтэй")});
            this.radioGroupKidney.Size = new System.Drawing.Size(126, 40);
            this.radioGroupKidney.TabIndex = 25;
            // 
            // radioGroupPastyernatskii
            // 
            this.radioGroupPastyernatskii.Location = new System.Drawing.Point(634, 106);
            this.radioGroupPastyernatskii.Name = "radioGroupPastyernatskii";
            this.radioGroupPastyernatskii.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Баруун"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Зүүн")});
            this.radioGroupPastyernatskii.Size = new System.Drawing.Size(126, 20);
            this.radioGroupPastyernatskii.TabIndex = 29;
            // 
            // spinEditIsUrineRight
            // 
            this.spinEditIsUrineRight.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditIsUrineRight.Location = new System.Drawing.Point(332, 83);
            this.spinEditIsUrineRight.Name = "spinEditIsUrineRight";
            this.spinEditIsUrineRight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditIsUrineRight.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditIsUrineRight.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.spinEditIsUrineRight.Size = new System.Drawing.Size(120, 20);
            this.spinEditIsUrineRight.TabIndex = 39;
            // 
            // radioGroupCutUrine
            // 
            this.radioGroupCutUrine.Location = new System.Drawing.Point(10, 83);
            this.radioGroupCutUrine.Name = "radioGroupCutUrine";
            this.radioGroupCutUrine.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupCutUrine.Size = new System.Drawing.Size(120, 20);
            this.radioGroupCutUrine.TabIndex = 26;
            // 
            // radioGroupMissUrine
            // 
            this.radioGroupMissUrine.Location = new System.Drawing.Point(172, 83);
            this.radioGroupMissUrine.Name = "radioGroupMissUrine";
            this.radioGroupMissUrine.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Тийм"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Үгүй")});
            this.radioGroupMissUrine.Size = new System.Drawing.Size(120, 20);
            this.radioGroupMissUrine.TabIndex = 27;
            // 
            // xtraTabPageAnamnez7
            // 
            this.xtraTabPageAnamnez7.Controls.Add(this.gridControlValidity);
            this.xtraTabPageAnamnez7.Name = "xtraTabPageAnamnez7";
            this.xtraTabPageAnamnez7.Size = new System.Drawing.Size(772, 303);
            this.xtraTabPageAnamnez7.Text = "Оношийн үндэслэл";
            // 
            // gridControlValidity
            // 
            this.gridControlValidity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlValidity.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlValidity.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlValidity.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlValidity.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlValidity.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlValidity.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "edit")});
            this.gridControlValidity.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlValidity_EmbeddedNavigator_ButtonClick);
            this.gridControlValidity.Location = new System.Drawing.Point(0, 0);
            this.gridControlValidity.MainView = this.gridViewValidity;
            this.gridControlValidity.Name = "gridControlValidity";
            this.gridControlValidity.Size = new System.Drawing.Size(772, 303);
            this.gridControlValidity.TabIndex = 0;
            this.gridControlValidity.UseEmbeddedNavigator = true;
            this.gridControlValidity.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewValidity});
            // 
            // gridViewValidity
            // 
            this.gridViewValidity.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65});
            this.gridViewValidity.GridControl = this.gridControlValidity;
            this.gridViewValidity.Name = "gridViewValidity";
            this.gridViewValidity.OptionsBehavior.ReadOnly = true;
            this.gridViewValidity.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Огноо";
            this.gridColumn62.FieldName = "date";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.Visible = true;
            this.gridColumn62.VisibleIndex = 0;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Оношийн ангилал";
            this.gridColumn63.FieldName = "diagnoseTypeText";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.Visible = true;
            this.gridColumn63.VisibleIndex = 1;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Оношийн үндэслэл";
            this.gridColumn64.FieldName = "description";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.Visible = true;
            this.gridColumn64.VisibleIndex = 2;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Эмчийн нэр";
            this.gridColumn65.FieldName = "fullName";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.Visible = true;
            this.gridColumn65.VisibleIndex = 3;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.AllowTouchScroll = true;
            this.xtraTabPage5.AutoScroll = true;
            this.xtraTabPage5.Controls.Add(this.xtraTabControlStoreLeft);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(778, 331);
            this.xtraTabPage5.Text = "Эмнэлгээс гарах үеийн эпикриз";
            // 
            // xtraTabControlStoreLeft
            // 
            this.xtraTabControlStoreLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlStoreLeft.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControlStoreLeft.Name = "xtraTabControlStoreLeft";
            this.xtraTabControlStoreLeft.SelectedTabPage = this.xtraTabPageStoreLeftFirst;
            this.xtraTabControlStoreLeft.Size = new System.Drawing.Size(778, 331);
            this.xtraTabControlStoreLeft.TabIndex = 0;
            this.xtraTabControlStoreLeft.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageStoreLeftFirst,
            this.xtraTabPageStoreLeftSecond,
            this.xtraTabPageStoreLeftThird});
            // 
            // xtraTabPageStoreLeftFirst
            // 
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl176);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.gridLookUpEditDoctor);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.checkEditIsClose);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl11);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl25);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl26);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl29);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl30);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl31);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl32);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl33);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl34);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl35);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.labelControl36);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.radionGroupLeaveHospital);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.gridLookUpEditDiagnose);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.dateEditEndDate);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.gridLookUpEditEndHurt);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.memoEditOtherTreat);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.checkEditBlood);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.checkEditIsSurgery);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.memoEditSurgery);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.checkEditIsAfterSurgery);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.memoEditAfterSurgery);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.memoEditBillOfMedic);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.gridLookUpEditTreat);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.spinEditDuration);
            this.xtraTabPageStoreLeftFirst.Controls.Add(this.spinEditBill);
            this.xtraTabPageStoreLeftFirst.Name = "xtraTabPageStoreLeftFirst";
            this.xtraTabPageStoreLeftFirst.Size = new System.Drawing.Size(772, 303);
            this.xtraTabPageStoreLeftFirst.Text = "Эпикриз";
            // 
            // labelControl176
            // 
            this.labelControl176.Location = new System.Drawing.Point(158, 12);
            this.labelControl176.Name = "labelControl176";
            this.labelControl176.Size = new System.Drawing.Size(61, 13);
            this.labelControl176.TabIndex = 77;
            this.labelControl176.Text = "Эмчийн нэр:";
            // 
            // gridLookUpEditDoctor
            // 
            this.gridLookUpEditDoctor.EditValue = "";
            this.gridLookUpEditDoctor.Location = new System.Drawing.Point(225, 9);
            this.gridLookUpEditDoctor.Name = "gridLookUpEditDoctor";
            this.gridLookUpEditDoctor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDoctor.Properties.DisplayMember = "fullName";
            this.gridLookUpEditDoctor.Properties.NullText = "";
            this.gridLookUpEditDoctor.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditDoctor.Properties.ValueMember = "id";
            this.gridLookUpEditDoctor.Properties.View = this.gridView2;
            this.gridLookUpEditDoctor.Size = new System.Drawing.Size(153, 20);
            this.gridLookUpEditDoctor.TabIndex = 76;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn3,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Овог";
            this.gridColumn2.CustomizationCaption = "Овог";
            this.gridColumn2.FieldName = "lastName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Нэр";
            this.gridColumn21.CustomizationCaption = "Нэр";
            this.gridColumn21.FieldName = "firstName";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Регистер";
            this.gridColumn22.CustomizationCaption = "Регистер";
            this.gridColumn22.FieldName = "id";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Эрх";
            this.gridColumn3.CustomizationCaption = "Эрх";
            this.gridColumn3.FieldName = "roleName";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Тасаг";
            this.gridColumn24.CustomizationCaption = "Тасаг";
            this.gridColumn24.FieldName = "wardName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Ажлын газар";
            this.gridColumn25.CustomizationCaption = "Ажлын газар";
            this.gridColumn25.FieldName = "organizationName";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Албан тушаал";
            this.gridColumn26.CustomizationCaption = "Албан тушаал";
            this.gridColumn26.FieldName = "positionName";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            // 
            // checkEditIsClose
            // 
            this.checkEditIsClose.Location = new System.Drawing.Point(10, 8);
            this.checkEditIsClose.Name = "checkEditIsClose";
            this.checkEditIsClose.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.checkEditIsClose.Properties.Appearance.Options.UseBackColor = true;
            this.checkEditIsClose.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.checkEditIsClose.Properties.Caption = "Өвчний түүх хаах";
            this.checkEditIsClose.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditIsClose.Size = new System.Drawing.Size(115, 21);
            this.checkEditIsClose.TabIndex = 61;
            this.checkEditIsClose.CheckedChanged += new System.EventHandler(this.checkEditIsClose_CheckedChanged);
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(25, 221);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(226, 13);
            this.labelControl11.TabIndex = 75;
            this.labelControl11.Text = "Нийт хэрэглэсэн эмийн зардал(₮): Нийт дүн  ";
            // 
            // labelControl25
            // 
            this.labelControl25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl25.Location = new System.Drawing.Point(452, 242);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(83, 13);
            this.labelControl25.TabIndex = 74;
            this.labelControl25.Text = "Хүндрэлийн нэр:";
            // 
            // labelControl26
            // 
            this.labelControl26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl26.Location = new System.Drawing.Point(451, 161);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(84, 13);
            this.labelControl26.TabIndex = 73;
            this.labelControl26.Text = "Мэс заслын нэр :";
            // 
            // labelControl29
            // 
            this.labelControl29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl29.Location = new System.Drawing.Point(454, 55);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(81, 13);
            this.labelControl29.TabIndex = 72;
            this.labelControl29.Text = "Бусад эмчилгээ:";
            // 
            // labelControl30
            // 
            this.labelControl30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl30.Location = new System.Drawing.Point(438, 30);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(97, 13);
            this.labelControl30.TabIndex = 71;
            this.labelControl30.Text = "Хийгдсэн эмчилгээ:";
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(59, 142);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(113, 13);
            this.labelControl31.TabIndex = 70;
            this.labelControl31.Text = "Эмчилгээний зардал*:";
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(61, 116);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(112, 13);
            this.labelControl32.TabIndex = 64;
            this.labelControl32.Text = "Үргэлжилсэн хугацаа:";
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(86, 90);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(87, 13);
            this.labelControl33.TabIndex = 63;
            this.labelControl33.Text = "Өвчний  төгсгөл:";
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(102, 64);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(71, 13);
            this.labelControl34.TabIndex = 60;
            this.labelControl34.Text = "Гарсан огноо:";
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(113, 165);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(60, 13);
            this.labelControl35.TabIndex = 58;
            this.labelControl35.Text = "Эмнэлгээс*:";
            // 
            // labelControl36
            // 
            this.labelControl36.Location = new System.Drawing.Point(73, 37);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(101, 13);
            this.labelControl36.TabIndex = 57;
            this.labelControl36.Text = "Гарах үеийн онош*:";
            // 
            // radionGroupLeaveHospital
            // 
            this.radionGroupLeaveHospital.Location = new System.Drawing.Point(179, 165);
            this.radionGroupLeaveHospital.Name = "radionGroupLeaveHospital";
            this.radionGroupLeaveHospital.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(0)), "Гарсан", true, "Гарсан"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(1)), "Шилжсэн", true, "Шилжсэн"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((byte)(2)), "Нас барсан", true, "Нас барсан")});
            this.radionGroupLeaveHospital.Size = new System.Drawing.Size(200, 50);
            this.radionGroupLeaveHospital.TabIndex = 67;
            // 
            // gridLookUpEditDiagnose
            // 
            this.gridLookUpEditDiagnose.Location = new System.Drawing.Point(178, 35);
            this.gridLookUpEditDiagnose.Name = "gridLookUpEditDiagnose";
            this.gridLookUpEditDiagnose.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDiagnose.Properties.DisplayMember = "name";
            this.gridLookUpEditDiagnose.Properties.NullText = "";
            this.gridLookUpEditDiagnose.Properties.ValueMember = "id";
            this.gridLookUpEditDiagnose.Properties.View = this.gridView9;
            this.gridLookUpEditDiagnose.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditDiagnose.TabIndex = 62;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnDiagnoseType});
            this.gridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnDiagnoseType
            // 
            this.gridColumnDiagnoseType.Caption = "Оношууд";
            this.gridColumnDiagnoseType.FieldName = "name";
            this.gridColumnDiagnoseType.Name = "gridColumnDiagnoseType";
            this.gridColumnDiagnoseType.Visible = true;
            this.gridColumnDiagnoseType.VisibleIndex = 0;
            // 
            // dateEditEndDate
            // 
            this.dateEditEndDate.EditValue = null;
            this.dateEditEndDate.Location = new System.Drawing.Point(178, 61);
            this.dateEditEndDate.Name = "dateEditEndDate";
            this.dateEditEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEndDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEndDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEndDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEndDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEndDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEndDate.Size = new System.Drawing.Size(101, 20);
            this.dateEditEndDate.TabIndex = 63;
            this.dateEditEndDate.EditValueChanged += new System.EventHandler(this.dateEditEndDate_EditValueChanged);
            // 
            // gridLookUpEditEndHurt
            // 
            this.gridLookUpEditEndHurt.Location = new System.Drawing.Point(178, 87);
            this.gridLookUpEditEndHurt.Name = "gridLookUpEditEndHurt";
            this.gridLookUpEditEndHurt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditEndHurt.Properties.DisplayMember = "name";
            this.gridLookUpEditEndHurt.Properties.NullText = "";
            this.gridLookUpEditEndHurt.Properties.ValueMember = "id";
            this.gridLookUpEditEndHurt.Properties.View = this.gridView10;
            this.gridLookUpEditEndHurt.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditEndHurt.TabIndex = 64;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gridView10.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Өвчний төлөв";
            this.gridColumn1.FieldName = "name";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // memoEditOtherTreat
            // 
            this.memoEditOtherTreat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditOtherTreat.Location = new System.Drawing.Point(541, 53);
            this.memoEditOtherTreat.Name = "memoEditOtherTreat";
            this.memoEditOtherTreat.Properties.MaxLength = 200;
            this.memoEditOtherTreat.Size = new System.Drawing.Size(200, 50);
            this.memoEditOtherTreat.TabIndex = 70;
            // 
            // checkEditBlood
            // 
            this.checkEditBlood.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditBlood.EditValue = null;
            this.checkEditBlood.Location = new System.Drawing.Point(516, 109);
            this.checkEditBlood.Name = "checkEditBlood";
            this.checkEditBlood.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.checkEditBlood.Properties.Caption = "Цус, цусан  бүтээгдэхүүн сэлбэсэн эсэх:";
            this.checkEditBlood.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditBlood.Size = new System.Drawing.Size(227, 19);
            this.checkEditBlood.TabIndex = 71;
            // 
            // checkEditIsSurgery
            // 
            this.checkEditIsSurgery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditIsSurgery.EditValue = null;
            this.checkEditIsSurgery.Location = new System.Drawing.Point(551, 134);
            this.checkEditIsSurgery.Name = "checkEditIsSurgery";
            this.checkEditIsSurgery.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.checkEditIsSurgery.Properties.Caption = "Мэс заслын эмчилгээ хийсэн эсэх:";
            this.checkEditIsSurgery.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditIsSurgery.Size = new System.Drawing.Size(192, 19);
            this.checkEditIsSurgery.TabIndex = 72;
            this.checkEditIsSurgery.CheckedChanged += new System.EventHandler(this.checkEditIsSurgery_CheckedChanged);
            // 
            // memoEditSurgery
            // 
            this.memoEditSurgery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditSurgery.Location = new System.Drawing.Point(541, 159);
            this.memoEditSurgery.Name = "memoEditSurgery";
            this.memoEditSurgery.Properties.MaxLength = 200;
            this.memoEditSurgery.Size = new System.Drawing.Size(200, 50);
            this.memoEditSurgery.TabIndex = 73;
            // 
            // checkEditIsAfterSurgery
            // 
            this.checkEditIsAfterSurgery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditIsAfterSurgery.EditValue = null;
            this.checkEditIsAfterSurgery.Location = new System.Drawing.Point(516, 215);
            this.checkEditIsAfterSurgery.Name = "checkEditIsAfterSurgery";
            this.checkEditIsAfterSurgery.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.checkEditIsAfterSurgery.Properties.Caption = "Мэс заслын дараах хүндрэл байсан эсэх:";
            this.checkEditIsAfterSurgery.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditIsAfterSurgery.Size = new System.Drawing.Size(227, 19);
            this.checkEditIsAfterSurgery.TabIndex = 74;
            this.checkEditIsAfterSurgery.CheckedChanged += new System.EventHandler(this.checkEditIsAfterSurgery_CheckedChanged);
            // 
            // memoEditAfterSurgery
            // 
            this.memoEditAfterSurgery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditAfterSurgery.Location = new System.Drawing.Point(541, 240);
            this.memoEditAfterSurgery.Name = "memoEditAfterSurgery";
            this.memoEditAfterSurgery.Properties.MaxLength = 200;
            this.memoEditAfterSurgery.Size = new System.Drawing.Size(200, 50);
            this.memoEditAfterSurgery.TabIndex = 75;
            // 
            // memoEditBillOfMedic
            // 
            this.memoEditBillOfMedic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.memoEditBillOfMedic.EditValue = "Тасагт/ Хагалгаанд/ Мэдээ асуултанд";
            this.memoEditBillOfMedic.Location = new System.Drawing.Point(25, 240);
            this.memoEditBillOfMedic.Name = "memoEditBillOfMedic";
            this.memoEditBillOfMedic.Properties.MaxLength = 200;
            this.memoEditBillOfMedic.Size = new System.Drawing.Size(353, 50);
            this.memoEditBillOfMedic.TabIndex = 68;
            // 
            // gridLookUpEditTreat
            // 
            this.gridLookUpEditTreat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditTreat.Location = new System.Drawing.Point(541, 27);
            this.gridLookUpEditTreat.Name = "gridLookUpEditTreat";
            this.gridLookUpEditTreat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditTreat.Properties.DisplayMember = "name";
            this.gridLookUpEditTreat.Properties.NullText = "";
            this.gridLookUpEditTreat.Properties.ValueMember = "id";
            this.gridLookUpEditTreat.Properties.View = this.gridView11;
            this.gridLookUpEditTreat.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditTreat.TabIndex = 69;
            this.gridLookUpEditTreat.EditValueChanged += new System.EventHandler(this.gridLookUpEditTreat_EditValueChanged);
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10});
            this.gridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Эмчилгээ";
            this.gridColumn10.FieldName = "name";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            // 
            // spinEditDuration
            // 
            this.spinEditDuration.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditDuration.Location = new System.Drawing.Point(178, 113);
            this.spinEditDuration.Name = "spinEditDuration";
            this.spinEditDuration.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditDuration.Properties.DisplayFormat.FormatString = "n0";
            this.spinEditDuration.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditDuration.Properties.EditFormat.FormatString = "n0";
            this.spinEditDuration.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditDuration.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditDuration.Properties.Mask.EditMask = "n0";
            this.spinEditDuration.Size = new System.Drawing.Size(200, 20);
            this.spinEditDuration.TabIndex = 65;
            // 
            // spinEditBill
            // 
            this.spinEditBill.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditBill.Location = new System.Drawing.Point(178, 139);
            this.spinEditBill.Name = "spinEditBill";
            this.spinEditBill.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditBill.Properties.DisplayFormat.FormatString = "n0";
            this.spinEditBill.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditBill.Properties.EditFormat.FormatString = "n0";
            this.spinEditBill.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditBill.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditBill.Properties.Mask.EditMask = "n0";
            this.spinEditBill.Size = new System.Drawing.Size(200, 20);
            this.spinEditBill.TabIndex = 66;
            // 
            // xtraTabPageStoreLeftSecond
            // 
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.labelControl37);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.labelControl38);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.memoEditAnalysisVirus);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.labelControl39);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.labelControl40);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.labelControl41);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.labelControl42);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.labelControl43);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.labelControl44);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.labelControl45);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.labelControl46);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.labelControl47);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.memoEditAnalysisChanged);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.memoEditAnalysisBlood);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.radioGroupIsHeal);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.gridLookUpEditInCome);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.gridLookUpEditInBack);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.gridLookUpEditAsTreat);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.gridLookUpEditAsLight);
            this.xtraTabPageStoreLeftSecond.Controls.Add(this.memoEditAnalysisGeneral);
            this.xtraTabPageStoreLeftSecond.Name = "xtraTabPageStoreLeftSecond";
            this.xtraTabPageStoreLeftSecond.Size = new System.Drawing.Size(772, 303);
            this.xtraTabPageStoreLeftSecond.Text = "Шинжилгээ";
            // 
            // labelControl37
            // 
            this.labelControl37.Location = new System.Drawing.Point(95, 157);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(78, 26);
            this.labelControl37.TabIndex = 72;
            this.labelControl37.Text = "Цусны ерөнхий \r\nшинжилгээ:";
            // 
            // labelControl38
            // 
            this.labelControl38.Location = new System.Drawing.Point(93, 221);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(80, 13);
            this.labelControl38.TabIndex = 71;
            this.labelControl38.Text = "Шарх эдгэрэлт:";
            // 
            // memoEditAnalysisVirus
            // 
            this.memoEditAnalysisVirus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditAnalysisVirus.Location = new System.Drawing.Point(541, 158);
            this.memoEditAnalysisVirus.Name = "memoEditAnalysisVirus";
            this.memoEditAnalysisVirus.Properties.MaxLength = 200;
            this.memoEditAnalysisVirus.Size = new System.Drawing.Size(200, 55);
            this.memoEditAnalysisVirus.TabIndex = 84;
            // 
            // labelControl39
            // 
            this.labelControl39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl39.Location = new System.Drawing.Point(445, 161);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(90, 26);
            this.labelControl39.TabIndex = 70;
            this.labelControl39.Text = "Вирус, маркерийн \r\nшинжилгээ:";
            // 
            // labelControl40
            // 
            this.labelControl40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl40.Location = new System.Drawing.Point(445, 99);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(90, 26);
            this.labelControl40.TabIndex = 69;
            this.labelControl40.Text = "Цусны биохимийн \r\nшинжилгээ:";
            // 
            // labelControl41
            // 
            this.labelControl41.Location = new System.Drawing.Point(98, 96);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(75, 26);
            this.labelControl41.TabIndex = 68;
            this.labelControl41.Text = "Шинжилгээний \r\nөөрчлөлтөнд:";
            // 
            // labelControl42
            // 
            this.labelControl42.Location = new System.Drawing.Point(118, 70);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(55, 13);
            this.labelControl42.TabIndex = 67;
            this.labelControl42.Text = "Гарах үед:";
            // 
            // labelControl43
            // 
            this.labelControl43.Location = new System.Drawing.Point(124, 44);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(49, 13);
            this.labelControl43.TabIndex = 66;
            this.labelControl43.Text = "Ирэх үед:";
            // 
            // labelControl44
            // 
            this.labelControl44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl44.Location = new System.Drawing.Point(490, 74);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(45, 13);
            this.labelControl44.TabIndex = 65;
            this.labelControl44.Text = "Гэрлийн:";
            // 
            // labelControl45
            // 
            this.labelControl45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl45.Location = new System.Drawing.Point(476, 48);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(59, 13);
            this.labelControl45.TabIndex = 64;
            this.labelControl45.Text = "Эмнэл зүйн:";
            // 
            // labelControl46
            // 
            this.labelControl46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl46.Location = new System.Drawing.Point(513, 22);
            this.labelControl46.Name = "labelControl46";
            this.labelControl46.Size = new System.Drawing.Size(104, 13);
            this.labelControl46.TabIndex = 62;
            this.labelControl46.Text = "Хийгдсэн шинжилгээ";
            // 
            // labelControl47
            // 
            this.labelControl47.Location = new System.Drawing.Point(76, 22);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(76, 13);
            this.labelControl47.TabIndex = 55;
            this.labelControl47.Text = "Биеийн байдал";
            // 
            // memoEditAnalysisChanged
            // 
            this.memoEditAnalysisChanged.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditAnalysisChanged.EditValue = "/зөвхөн өөрчлөлт бүхий шинжилгээний үзүүлэлтүүдийг бичнэ/";
            this.memoEditAnalysisChanged.Location = new System.Drawing.Point(179, 93);
            this.memoEditAnalysisChanged.Name = "memoEditAnalysisChanged";
            this.memoEditAnalysisChanged.Properties.MaxLength = 200;
            this.memoEditAnalysisChanged.Size = new System.Drawing.Size(200, 55);
            this.memoEditAnalysisChanged.TabIndex = 78;
            // 
            // memoEditAnalysisBlood
            // 
            this.memoEditAnalysisBlood.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditAnalysisBlood.Location = new System.Drawing.Point(541, 97);
            this.memoEditAnalysisBlood.Name = "memoEditAnalysisBlood";
            this.memoEditAnalysisBlood.Properties.MaxLength = 200;
            this.memoEditAnalysisBlood.Size = new System.Drawing.Size(200, 55);
            this.memoEditAnalysisBlood.TabIndex = 83;
            // 
            // radioGroupIsHeal
            // 
            this.radioGroupIsHeal.Location = new System.Drawing.Point(179, 215);
            this.radioGroupIsHeal.Name = "radioGroupIsHeal";
            this.radioGroupIsHeal.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Анхдагчаар"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Хоёрдогчоор")});
            this.radioGroupIsHeal.Size = new System.Drawing.Size(200, 40);
            this.radioGroupIsHeal.TabIndex = 80;
            // 
            // gridLookUpEditInCome
            // 
            this.gridLookUpEditInCome.Location = new System.Drawing.Point(179, 41);
            this.gridLookUpEditInCome.Name = "gridLookUpEditInCome";
            this.gridLookUpEditInCome.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditInCome.Properties.DisplayMember = "name";
            this.gridLookUpEditInCome.Properties.NullText = "";
            this.gridLookUpEditInCome.Properties.ValueMember = "id";
            this.gridLookUpEditInCome.Properties.View = this.gridView12;
            this.gridLookUpEditInCome.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditInCome.TabIndex = 76;
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11});
            this.gridView12.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Ирэх үеийн төлөв";
            this.gridColumn11.FieldName = "name";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            // 
            // gridLookUpEditInBack
            // 
            this.gridLookUpEditInBack.Location = new System.Drawing.Point(179, 67);
            this.gridLookUpEditInBack.Name = "gridLookUpEditInBack";
            this.gridLookUpEditInBack.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditInBack.Properties.DisplayMember = "name";
            this.gridLookUpEditInBack.Properties.NullText = "";
            this.gridLookUpEditInBack.Properties.ValueMember = "id";
            this.gridLookUpEditInBack.Properties.View = this.gridView13;
            this.gridLookUpEditInBack.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditInBack.TabIndex = 77;
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12});
            this.gridView13.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Гарах үеийн төлөв";
            this.gridColumn12.FieldName = "name";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 0;
            // 
            // gridLookUpEditAsTreat
            // 
            this.gridLookUpEditAsTreat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditAsTreat.Location = new System.Drawing.Point(541, 45);
            this.gridLookUpEditAsTreat.Name = "gridLookUpEditAsTreat";
            this.gridLookUpEditAsTreat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditAsTreat.Properties.DisplayMember = "name";
            this.gridLookUpEditAsTreat.Properties.NullText = "";
            this.gridLookUpEditAsTreat.Properties.ValueMember = "id";
            this.gridLookUpEditAsTreat.Properties.View = this.gridView14;
            this.gridLookUpEditAsTreat.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditAsTreat.TabIndex = 81;
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13});
            this.gridView14.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Эмнэл зүйн";
            this.gridColumn13.FieldName = "name";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 0;
            // 
            // gridLookUpEditAsLight
            // 
            this.gridLookUpEditAsLight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEditAsLight.Location = new System.Drawing.Point(541, 71);
            this.gridLookUpEditAsLight.Name = "gridLookUpEditAsLight";
            this.gridLookUpEditAsLight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditAsLight.Properties.DisplayMember = "name";
            this.gridLookUpEditAsLight.Properties.NullText = "";
            this.gridLookUpEditAsLight.Properties.ValueMember = "id";
            this.gridLookUpEditAsLight.Properties.View = this.gridView15;
            this.gridLookUpEditAsLight.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditAsLight.TabIndex = 82;
            // 
            // gridView15
            // 
            this.gridView15.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn14});
            this.gridView15.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView15.Name = "gridView15";
            this.gridView15.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView15.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Гэрлийн";
            this.gridColumn14.FieldName = "name";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            // 
            // memoEditAnalysisGeneral
            // 
            this.memoEditAnalysisGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditAnalysisGeneral.Location = new System.Drawing.Point(179, 154);
            this.memoEditAnalysisGeneral.Name = "memoEditAnalysisGeneral";
            this.memoEditAnalysisGeneral.Properties.MaxLength = 200;
            this.memoEditAnalysisGeneral.Size = new System.Drawing.Size(200, 55);
            this.memoEditAnalysisGeneral.TabIndex = 79;
            // 
            // xtraTabPageStoreLeftThird
            // 
            this.xtraTabPageStoreLeftThird.Controls.Add(this.gridControlStoreLeftDiagnose);
            this.xtraTabPageStoreLeftThird.Name = "xtraTabPageStoreLeftThird";
            this.xtraTabPageStoreLeftThird.Size = new System.Drawing.Size(772, 303);
            this.xtraTabPageStoreLeftThird.Text = "Гарах үеийн онош";
            // 
            // gridControlStoreLeftDiagnose
            // 
            this.gridControlStoreLeftDiagnose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlStoreLeftDiagnose.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlStoreLeftDiagnose.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlStoreLeftDiagnose.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlStoreLeftDiagnose.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlStoreLeftDiagnose.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlStoreLeftDiagnose.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "edit")});
            this.gridControlStoreLeftDiagnose.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlStoreLeftDiagnose_EmbeddedNavigator_ButtonClick);
            this.gridControlStoreLeftDiagnose.Location = new System.Drawing.Point(0, 0);
            this.gridControlStoreLeftDiagnose.MainView = this.gridViewStoreLeftDiagnose;
            this.gridControlStoreLeftDiagnose.Name = "gridControlStoreLeftDiagnose";
            this.gridControlStoreLeftDiagnose.Size = new System.Drawing.Size(772, 303);
            this.gridControlStoreLeftDiagnose.TabIndex = 0;
            this.gridControlStoreLeftDiagnose.UseEmbeddedNavigator = true;
            this.gridControlStoreLeftDiagnose.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewStoreLeftDiagnose});
            // 
            // gridViewStoreLeftDiagnose
            // 
            this.gridViewStoreLeftDiagnose.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnLeftDiagnoseDoctor,
            this.gridColumnLeftDiagnoseType,
            this.gridColumnLeftDiagnoseDesc,
            this.gridColumnLeftDiagnoseCode});
            this.gridViewStoreLeftDiagnose.GridControl = this.gridControlStoreLeftDiagnose;
            this.gridViewStoreLeftDiagnose.Name = "gridViewStoreLeftDiagnose";
            this.gridViewStoreLeftDiagnose.OptionsBehavior.ReadOnly = true;
            this.gridViewStoreLeftDiagnose.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnLeftDiagnoseDoctor
            // 
            this.gridColumnLeftDiagnoseDoctor.Caption = "Эмчийн нэр";
            this.gridColumnLeftDiagnoseDoctor.FieldName = "fullName";
            this.gridColumnLeftDiagnoseDoctor.Name = "gridColumnLeftDiagnoseDoctor";
            this.gridColumnLeftDiagnoseDoctor.Visible = true;
            this.gridColumnLeftDiagnoseDoctor.VisibleIndex = 0;
            // 
            // gridColumnLeftDiagnoseType
            // 
            this.gridColumnLeftDiagnoseType.Caption = "Оношийн ангилал";
            this.gridColumnLeftDiagnoseType.FieldName = "diagnoseTypeName";
            this.gridColumnLeftDiagnoseType.Name = "gridColumnLeftDiagnoseType";
            this.gridColumnLeftDiagnoseType.Visible = true;
            this.gridColumnLeftDiagnoseType.VisibleIndex = 1;
            // 
            // gridColumnLeftDiagnoseDesc
            // 
            this.gridColumnLeftDiagnoseDesc.Caption = "Тайлбар";
            this.gridColumnLeftDiagnoseDesc.FieldName = "description";
            this.gridColumnLeftDiagnoseDesc.Name = "gridColumnLeftDiagnoseDesc";
            this.gridColumnLeftDiagnoseDesc.Visible = true;
            this.gridColumnLeftDiagnoseDesc.VisibleIndex = 2;
            // 
            // gridColumnLeftDiagnoseCode
            // 
            this.gridColumnLeftDiagnoseCode.Caption = "Оношийн код";
            this.gridColumnLeftDiagnoseCode.FieldName = "codeText";
            this.gridColumnLeftDiagnoseCode.Name = "gridColumnLeftDiagnoseCode";
            this.gridColumnLeftDiagnoseCode.Visible = true;
            this.gridColumnLeftDiagnoseCode.VisibleIndex = 3;
            // 
            // alertControlPharmacy2016
            // 
            this.alertControlPharmacy2016.AutoFormDelay = 5000;
            // 
            // StoreInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancelStore;
            this.ClientSize = new System.Drawing.Size(784, 661);
            this.Controls.Add(this.xtraTabControl);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StoreInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Өвчний түүх нээх";
            this.Shown += new System.EventHandler(this.StoreInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInspectionTreatment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStoreInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStoreInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditAlergyType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupLoopState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthdate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBirthdate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsUnder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsMarried.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBeginDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBeginDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRegister.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl)).EndInit();
            this.xtraTabControl.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEducation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProvince.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditSpecialty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditOrganization.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditBeginDiagnose.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmergencyPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEmergencyName.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWardRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWardRoom)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStoreDiagnose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStoreDiagnose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlAnamnez)).EndInit();
            this.xtraTabControlAnamnez.ResumeLayout(false);
            this.xtraTabPageAnamnez1.ResumeLayout(false);
            this.xtraTabPageAnamnez1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditStore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditPain.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditHistory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditLifeStore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHigh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBodyIndex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupBloodPressure.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditLifeTerm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditWorkTerm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditInfection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDoesSurgery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAccident.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditAllergy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsVodka.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadioGroupIsSmoke.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditHowLong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDiet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditInheritance.Properties)).EndInit();
            this.xtraTabPageAnamnez2.ResumeLayout(false);
            this.xtraTabPageAnamnez2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditGeneralCondition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSurround.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSkinSalst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSkinFlexible.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSkinRash.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupEdema.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditGeneralPart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridlookUpEditSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPlace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupConcern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupShape.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMove.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSkinWet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditGeneral.Properties)).EndInit();
            this.xtraTabPageAnamnez3.ResumeLayout(false);
            this.xtraTabPageAnamnez3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditInternal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsBreathing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsCyanosis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIfCyanosis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsBreathMuscles.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBreathCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupChestShape.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupBreathChest.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIconcern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSoundVib.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPercussion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPartPercussion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupBreathDouble.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsDesease.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIfDesease.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsFlexible.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsNoisy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIfNoisy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditBronkhofoni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupRightLeft.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditBreathShape.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView32)).EndInit();
            this.xtraTabPageAnamnez4.ResumeLayout(false);
            this.xtraTabPageAnamnez4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditHeart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSkinBlue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsEdem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHeartPush.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditFrequency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHeartFrequency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHeartSound.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditFirstSound.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSecondSound.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditThirdSound.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSameDouble.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupTransmission.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTransStrong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHeartLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSezi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditSis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDis.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTouch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditHeartRisk.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupVenous.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditVoltage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupFilled.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHeartPlace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupThisNoisy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditStrong.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView43)).EndInit();
            this.xtraTabPageAnamnez5.ResumeLayout(false);
            this.xtraTabPageAnamnez5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupTideConcern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTuck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditDigestive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSpleen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDuringDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditLiver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTidePos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTideStable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsCrossPos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsCrossStable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsPos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsPosStable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupCrossConcern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPosConcern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTongueShape.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditColor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTongue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsSkinWet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupTuckConcern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTympanic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsTough.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIsPercussion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditIsMoving.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupBellyConcern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsBellyStable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsBellyPos.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupLiverSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupLiverDirection.Properties)).EndInit();
            this.xtraTabPageAnamnez6.ResumeLayout(false);
            this.xtraTabPageAnamnez6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditUrine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditOther.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditMentalStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditShallow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpeditDeep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditQuiteDeep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupReflex.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupFaceDouble.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSmell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUrineOutPut.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUrineColor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsUrineNight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPushUrine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupHurtUrine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupKidney.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPastyernatskii.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditIsUrineRight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupCutUrine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupMissUrine.Properties)).EndInit();
            this.xtraTabPageAnamnez7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlValidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewValidity)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlStoreLeft)).EndInit();
            this.xtraTabControlStoreLeft.ResumeLayout(false);
            this.xtraTabPageStoreLeftFirst.ResumeLayout(false);
            this.xtraTabPageStoreLeftFirst.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsClose.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radionGroupLeaveHospital.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDiagnose.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditEndHurt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditOtherTreat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditBlood.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsSurgery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditSurgery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsAfterSurgery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAfterSurgery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditBillOfMedic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditTreat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDuration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditBill.Properties)).EndInit();
            this.xtraTabPageStoreLeftSecond.ResumeLayout(false);
            this.xtraTabPageStoreLeftSecond.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAnalysisVirus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAnalysisChanged.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAnalysisBlood.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsHeal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditInCome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditInBack.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditAsTreat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditAsLight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditAnalysisGeneral.Properties)).EndInit();
            this.xtraTabPageStoreLeftThird.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStoreLeftDiagnose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStoreLeftDiagnose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPaymentType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.RadioGroup radioGroupLoopState;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.CheckEdit checkEditIsUnder;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.DateEdit dateEditBeginDate;
        private DevExpress.XtraEditors.TextEdit textEditNumber;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditOrder;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOrderName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOrderRegister;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditAlergyType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnAlergyTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPaymentTypeName;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.TextEdit textEditFirstName;
        private DevExpress.XtraEditors.TextEdit textEditLastName;
        private DevExpress.XtraEditors.MemoEdit memoEditAddress;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.RadioGroup radioGroupGender;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit textEditEmd;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsMarried;
        private DevExpress.XtraEditors.TextEdit textEditEmergencyName;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEditRegister;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancelStore;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSaveStore;
        private DevExpress.XtraGrid.GridControl gridControlStoreInspection;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInspectionTreatment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInspectionTreatmentStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInspectionTreatmentEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInspectionDescriptionMedicine;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInspectionDescriptionDuration;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInspectionTreatmentEachDay;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInspectionTreatmentDescription;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStoreInspection;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreInspection;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreInspectionDescription;
        private DevExpress.XtraEditors.DateEdit dateEditBirthdate;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.TextEdit textEditEmergencyPhone;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.MemoEdit memoEditBeginDiagnose;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlStoreLeft;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStoreLeftFirst;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStoreLeftSecond;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.RadioGroup radionGroupLeaveHospital;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDiagnose;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDiagnoseType;
        private DevExpress.XtraEditors.DateEdit dateEditEndDate;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditEndHurt;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.MemoEdit memoEditOtherTreat;
        private DevExpress.XtraEditors.CheckEdit checkEditBlood;
        private DevExpress.XtraEditors.CheckEdit checkEditIsSurgery;
        private DevExpress.XtraEditors.MemoEdit memoEditSurgery;
        private DevExpress.XtraEditors.CheckEdit checkEditIsAfterSurgery;
        private DevExpress.XtraEditors.MemoEdit memoEditAfterSurgery;
        private DevExpress.XtraEditors.MemoEdit memoEditBillOfMedic;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditTreat;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.MemoEdit memoEditAnalysisVirus;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.LabelControl labelControl46;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.MemoEdit memoEditAnalysisChanged;
        private DevExpress.XtraEditors.MemoEdit memoEditAnalysisBlood;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsHeal;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditInCome;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditInBack;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditAsTreat;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditAsLight;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraEditors.MemoEdit memoEditAnalysisGeneral;
        private DevExpress.XtraEditors.CheckEdit checkEditIsClose;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.TextEdit textEditEducation;
        private DevExpress.XtraEditors.TextEdit textEditProvince;
        private DevExpress.XtraEditors.TextEdit textEditSpecialty;
        private DevExpress.XtraEditors.TextEdit textEditPosition;
        private DevExpress.XtraEditors.TextEdit textEditOrganization;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreInspectionDoctorName;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWard;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnWard;
        private DevExpress.XtraGrid.GridControl gridControlWardRoom;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWardRoom;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.GridControl gridControlStoreDiagnose;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStoreDiagnose;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreDiagnoseDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreDiagnose;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreDiagnoseDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreDiagnoseTypeName;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreDiagnoseDoctorName;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDoctor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.SpinEdit spinEditDuration;
        private DevExpress.XtraEditors.SpinEdit spinEditBill;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlAnamnez;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAnamnez1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAnamnez2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAnamnez3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAnamnez4;
        private DevExpress.XtraEditors.LabelControl labelControl63;
        private DevExpress.XtraEditors.LabelControl labelControl62;
        private DevExpress.XtraEditors.LabelControl labelControl61;
        private DevExpress.XtraEditors.LabelControl labelControl60;
        private DevExpress.XtraEditors.LabelControl labelControl59;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.LabelControl labelControl52;
        private DevExpress.XtraEditors.LabelControl labelControl51;
        private DevExpress.XtraEditors.LabelControl labelControl50;
        private DevExpress.XtraEditors.LabelControl labelControl49;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.MemoEdit memoEditPain;
        private DevExpress.XtraEditors.MemoEdit memoEditHistory;
        private DevExpress.XtraEditors.MemoEdit memoEditLifeStore;
        private DevExpress.XtraEditors.LabelControl labelControl66;
        private DevExpress.XtraEditors.LabelControl labelControl65;
        private DevExpress.XtraEditors.LabelControl labelControl64;
        private DevExpress.XtraEditors.SpinEdit spinEditHigh;
        private DevExpress.XtraEditors.SpinEdit spinEditWeight;
        private DevExpress.XtraEditors.SpinEdit spinEditBodyIndex;
        private DevExpress.XtraEditors.RadioGroup radioGroupBloodPressure;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditLifeTerm;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditWorkTerm;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditInfection;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.CheckEdit checkEditDoesSurgery;
        private DevExpress.XtraEditors.MemoEdit memoEditAccident;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditAllergy;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsVodka;
        private DevExpress.XtraEditors.RadioGroup RadioGroupIsSmoke;
        private DevExpress.XtraEditors.MemoEdit memoEditHowLong;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDiet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView16;
        private DevExpress.XtraEditors.MemoEdit memoEditInheritance;
        private DevExpress.XtraEditors.CheckEdit checkEditStore;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.LabelControl labelControl67;
        private DevExpress.XtraEditors.LabelControl labelControl83;
        private DevExpress.XtraEditors.LabelControl labelControl82;
        private DevExpress.XtraEditors.LabelControl labelControl81;
        private DevExpress.XtraEditors.LabelControl labelControl80;
        private DevExpress.XtraEditors.LabelControl labelControl79;
        private DevExpress.XtraEditors.LabelControl labelControl78;
        private DevExpress.XtraEditors.LabelControl labelControl77;
        private DevExpress.XtraEditors.LabelControl labelControl76;
        private DevExpress.XtraEditors.LabelControl labelControl75;
        private DevExpress.XtraEditors.LabelControl labelControl74;
        private DevExpress.XtraEditors.LabelControl labelControl73;
        private DevExpress.XtraEditors.LabelControl labelControl72;
        private DevExpress.XtraEditors.LabelControl labelControl71;
        private DevExpress.XtraEditors.LabelControl labelControl70;
        private DevExpress.XtraEditors.LabelControl labelControl69;
        private DevExpress.XtraEditors.LabelControl labelControl68;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditGeneralCondition;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView17;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMind;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView18;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSurround;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView19;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPos;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView20;
        private DevExpress.XtraEditors.RadioGroup radioGroupSkinSalst;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSkinFlexible;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView21;
        private DevExpress.XtraEditors.RadioGroup radioGroupSkinRash;
        private DevExpress.XtraEditors.RadioGroup radioGroupEdema;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditGeneralPart;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView22;
        private DevExpress.XtraEditors.GridLookUpEdit gridlookUpEditSize;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView23;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPlace;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView24;
        private DevExpress.XtraEditors.RadioGroup radioGroupConcern;
        private DevExpress.XtraEditors.RadioGroup radioGroupShape;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMove;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView25;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSkinWet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraEditors.LabelControl labelControl94;
        private DevExpress.XtraEditors.LabelControl labelControl93;
        private DevExpress.XtraEditors.LabelControl labelControl92;
        private DevExpress.XtraEditors.LabelControl labelControl91;
        private DevExpress.XtraEditors.LabelControl labelControl90;
        private DevExpress.XtraEditors.LabelControl labelControl89;
        private DevExpress.XtraEditors.LabelControl labelControl88;
        private DevExpress.XtraEditors.LabelControl labelControl87;
        private DevExpress.XtraEditors.LabelControl labelControl86;
        private DevExpress.XtraEditors.LabelControl labelControl85;
        private DevExpress.XtraEditors.LabelControl labelControl84;
        private DevExpress.XtraEditors.LabelControl labelControl101;
        private DevExpress.XtraEditors.LabelControl labelControl100;
        private DevExpress.XtraEditors.LabelControl labelControl99;
        private DevExpress.XtraEditors.LabelControl labelControl98;
        private DevExpress.XtraEditors.LabelControl labelControl97;
        private DevExpress.XtraEditors.LabelControl labelControl96;
        private DevExpress.XtraEditors.LabelControl labelControl95;
        private DevExpress.XtraEditors.LabelControl labelControl108;
        private DevExpress.XtraEditors.LabelControl labelControl107;
        private DevExpress.XtraEditors.LabelControl labelControl106;
        private DevExpress.XtraEditors.LabelControl labelControl105;
        private DevExpress.XtraEditors.LabelControl labelControl104;
        private DevExpress.XtraEditors.LabelControl labelControl103;
        private DevExpress.XtraEditors.LabelControl labelControl102;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsBreathing;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsCyanosis;
        private DevExpress.XtraEditors.RadioGroup radioGroupIfCyanosis;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsBreathMuscles;
        private DevExpress.XtraEditors.SpinEdit spinEditBreathCount;
        private DevExpress.XtraEditors.RadioGroup radioGroupChestShape;
        private DevExpress.XtraEditors.RadioGroup radioGroupBreathChest;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsCount;
        private DevExpress.XtraEditors.RadioGroup radioGroupIconcern;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSoundVib;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView27;
        private DevExpress.XtraEditors.RadioGroup radioGroupPercussion;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPartPercussion;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView28;
        private DevExpress.XtraEditors.RadioGroup radioGroupBreathDouble;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsDesease;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditIfDesease;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView29;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsFlexible;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsNoisy;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditIfNoisy;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView30;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditBronkhofoni;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView31;
        private DevExpress.XtraEditors.RadioGroup radioGroupRightLeft;
        private DevExpress.XtraEditors.CheckEdit checkEditGeneral;
        private DevExpress.XtraEditors.CheckEdit checkEditInternal;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditBreathShape;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraEditors.LabelControl labelControl120;
        private DevExpress.XtraEditors.LabelControl labelControl119;
        private DevExpress.XtraEditors.LabelControl labelControl118;
        private DevExpress.XtraEditors.LabelControl labelControl117;
        private DevExpress.XtraEditors.LabelControl labelControl116;
        private DevExpress.XtraEditors.LabelControl labelControl115;
        private DevExpress.XtraEditors.LabelControl labelControl114;
        private DevExpress.XtraEditors.LabelControl labelControl113;
        private DevExpress.XtraEditors.LabelControl labelControl112;
        private DevExpress.XtraEditors.LabelControl labelControl111;
        private DevExpress.XtraEditors.LabelControl labelControl110;
        private DevExpress.XtraEditors.LabelControl labelControl109;
        private DevExpress.XtraEditors.LabelControl labelControl138;
        private DevExpress.XtraEditors.LabelControl labelControl137;
        private DevExpress.XtraEditors.LabelControl labelControl136;
        private DevExpress.XtraEditors.LabelControl labelControl135;
        private DevExpress.XtraEditors.LabelControl labelControl134;
        private DevExpress.XtraEditors.LabelControl labelControl133;
        private DevExpress.XtraEditors.LabelControl labelControl132;
        private DevExpress.XtraEditors.LabelControl labelControl131;
        private DevExpress.XtraEditors.LabelControl labelControl130;
        private DevExpress.XtraEditors.LabelControl labelControl129;
        private DevExpress.XtraEditors.LabelControl labelControl128;
        private DevExpress.XtraEditors.LabelControl labelControl127;
        private DevExpress.XtraEditors.LabelControl labelControl126;
        private DevExpress.XtraEditors.LabelControl labelControl125;
        private DevExpress.XtraEditors.LabelControl labelControl124;
        private DevExpress.XtraEditors.LabelControl labelControl123;
        private DevExpress.XtraEditors.LabelControl labelControl122;
        private DevExpress.XtraEditors.LabelControl labelControl121;
        private DevExpress.XtraEditors.RadioGroup radioGroupSkinBlue;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsEdem;
        private DevExpress.XtraEditors.RadioGroup radioGroupHeartPush;
        private DevExpress.XtraEditors.SpinEdit spinEditFrequency;
        private DevExpress.XtraEditors.SpinEdit spinEditHeartFrequency;
        private DevExpress.XtraEditors.RadioGroup radioGroupHeartSound;
        private DevExpress.XtraEditors.RadioGroup radioGroupRate;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditFirstSound;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView33;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSecondSound;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView34;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditThirdSound;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView35;
        private DevExpress.XtraEditors.RadioGroup radioGroupSameDouble;
        private DevExpress.XtraEditors.RadioGroup radioGroupTransmission;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditTransStrong;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView36;
        private DevExpress.XtraEditors.RadioGroup radioGroupHeartLimit;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSezi;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView37;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditSis;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView38;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPost;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView39;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDis;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView40;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsTouch;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditHeartRisk;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView41;
        private DevExpress.XtraEditors.RadioGroup radioGroupVenous;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditVoltage;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView42;
        private DevExpress.XtraEditors.RadioGroup radioGroupFilled;
        private DevExpress.XtraEditors.RadioGroup radioGroupHeartPlace;
        private DevExpress.XtraEditors.RadioGroup radioGroupThisNoisy;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditStrong;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView43;
        private DevExpress.XtraEditors.CheckEdit checkEditHeart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAnamnez5;
        private DevExpress.XtraEditors.LabelControl labelControl148;
        private DevExpress.XtraEditors.LabelControl labelControl147;
        private DevExpress.XtraEditors.LabelControl labelControl146;
        private DevExpress.XtraEditors.LabelControl labelControl145;
        private DevExpress.XtraEditors.LabelControl labelControl144;
        private DevExpress.XtraEditors.LabelControl labelControl143;
        private DevExpress.XtraEditors.LabelControl labelControl142;
        private DevExpress.XtraEditors.LabelControl labelControl141;
        private DevExpress.XtraEditors.LabelControl labelControl140;
        private DevExpress.XtraEditors.LabelControl labelControl139;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAnamnez6;
        private DevExpress.XtraEditors.LabelControl labelControl164;
        private DevExpress.XtraEditors.LabelControl labelControl163;
        private DevExpress.XtraEditors.LabelControl labelControl162;
        private DevExpress.XtraEditors.LabelControl labelControl161;
        private DevExpress.XtraEditors.LabelControl labelControl160;
        private DevExpress.XtraEditors.LabelControl labelControl159;
        private DevExpress.XtraEditors.LabelControl labelControl158;
        private DevExpress.XtraEditors.LabelControl labelControl157;
        private DevExpress.XtraEditors.LabelControl labelControl156;
        private DevExpress.XtraEditors.TextEdit textEditTongueShape;
        private DevExpress.XtraEditors.TextEdit textEditColor;
        private DevExpress.XtraEditors.LabelControl labelControl155;
        private DevExpress.XtraEditors.LabelControl labelControl154;
        private DevExpress.XtraEditors.LabelControl labelControl153;
        private DevExpress.XtraEditors.LabelControl labelControl152;
        private DevExpress.XtraEditors.LabelControl labelControl151;
        private DevExpress.XtraEditors.LabelControl labelControl150;
        private DevExpress.XtraEditors.LabelControl labelControl149;
        private DevExpress.XtraEditors.RadioGroup radioGroupCrossConcern;
        private DevExpress.XtraEditors.RadioGroup radioGroupPosConcern;
        private DevExpress.XtraEditors.LabelControl labelControl166;
        private DevExpress.XtraEditors.LabelControl labelControl165;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsTongue;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsSkinWet;
        private DevExpress.XtraEditors.RadioGroup radioGroupTuckConcern;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsTympanic;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsTough;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditIsPercussion;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView44;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditIsMoving;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView45;
        private DevExpress.XtraEditors.RadioGroup radioGroupBellyConcern;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsBellyStable;
        private DevExpress.XtraEditors.RadioGroup radioGroupSpleen;
        private DevExpress.XtraEditors.LabelControl labelControl171;
        private DevExpress.XtraEditors.LabelControl labelControl170;
        private DevExpress.XtraEditors.TextEdit textEditDuringDay;
        private DevExpress.XtraEditors.LabelControl labelControl169;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditLiver;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView46;
        private DevExpress.XtraEditors.LabelControl labelControl168;
        private DevExpress.XtraEditors.LabelControl labelControl167;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsTidePos;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsTideStable;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsCrossPos;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsCrossStable;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsPos;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsPosStable;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsBellyPos;
        private DevExpress.XtraEditors.RadioGroup radioGroupLiverSize;
        private DevExpress.XtraEditors.RadioGroup radioGroupLiverDirection;
        private DevExpress.XtraEditors.CheckEdit checkEditDigestive;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraEditors.LabelControl labelControl193;
        private DevExpress.XtraEditors.LabelControl labelControl192;
        private DevExpress.XtraEditors.LabelControl labelControl191;
        private DevExpress.XtraEditors.LabelControl labelControl190;
        private DevExpress.XtraEditors.LabelControl labelControl189;
        private DevExpress.XtraEditors.LabelControl labelControl188;
        private DevExpress.XtraEditors.LabelControl labelControl187;
        private DevExpress.XtraEditors.LabelControl labelControl186;
        private DevExpress.XtraEditors.LabelControl labelControl185;
        private DevExpress.XtraEditors.LabelControl labelControl184;
        private DevExpress.XtraEditors.LabelControl labelControl183;
        private DevExpress.XtraEditors.LabelControl labelControl182;
        private DevExpress.XtraEditors.LabelControl labelControl181;
        private DevExpress.XtraEditors.LabelControl labelControl180;
        private DevExpress.XtraEditors.LabelControl labelControl179;
        private DevExpress.XtraEditors.LabelControl labelControl178;
        private DevExpress.XtraEditors.LabelControl labelControl177;
        private DevExpress.XtraEditors.LabelControl radioGroupKidneyConcern;
        private DevExpress.XtraEditors.LabelControl labelControl175;
        private DevExpress.XtraEditors.LabelControl labelControl174;
        private DevExpress.XtraEditors.LabelControl labelControl173;
        private DevExpress.XtraEditors.LabelControl labelControl172;
        private DevExpress.XtraEditors.MemoEdit memoEditOther;
        private DevExpress.XtraEditors.MemoEdit memoEditMentalStatus;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditShallow;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView47;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpeditDeep;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView48;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditQuiteDeep;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView49;
        private DevExpress.XtraEditors.RadioGroup radioGroupReflex;
        private DevExpress.XtraEditors.RadioGroup radioGroupFaceDouble;
        private DevExpress.XtraEditors.RadioGroup radioGroupSmell;
        private DevExpress.XtraEditors.RadioGroup radioGroupHear;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUrineOutPut;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView50;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUrineColor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView51;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsUrineNight;
        private DevExpress.XtraEditors.RadioGroup radioGroupPushUrine;
        private DevExpress.XtraEditors.RadioGroup radioGroupHurtUrine;
        private DevExpress.XtraEditors.RadioGroup radioGroupKidney;
        private DevExpress.XtraEditors.RadioGroup radioGroupPastyernatskii;
        private DevExpress.XtraEditors.SpinEdit spinEditIsUrineRight;
        private DevExpress.XtraEditors.RadioGroup radioGroupCutUrine;
        private DevExpress.XtraEditors.RadioGroup radioGroupMissUrine;
        private DevExpress.XtraEditors.CheckEdit checkEditUrine;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraEditors.LabelControl labelControl176;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAnamnez7;
        private DevExpress.XtraGrid.GridControl gridControlValidity;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewValidity;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsTuck;
        private DevExpress.XtraEditors.RadioGroup radioGroupTideConcern;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStoreLeftThird;
        private DevExpress.XtraGrid.GridControl gridControlStoreLeftDiagnose;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStoreLeftDiagnose;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLeftDiagnoseDoctor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLeftDiagnoseType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLeftDiagnoseDesc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLeftDiagnoseCode;
        private DevExpress.XtraBars.Alerter.AlertControl alertControlPharmacy2016;

    }
}