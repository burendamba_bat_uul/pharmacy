﻿namespace Pharmacy2016.gui.core.store.store
{
    partial class InspectionInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InspectionInUpForm));
            this.simpleButtonSaveInspection = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonbackInspection = new DevExpress.XtraEditors.SimpleButton();
            this.memoEditInspection = new DevExpress.XtraEditors.MemoEdit();
            this.memoEditInspectionDescription = new DevExpress.XtraEditors.MemoEdit();
            this.dateEditInspectionDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlTreatment = new DevExpress.XtraGrid.GridControl();
            this.gridViewTreatment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMedicineName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnEachDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditDoctor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlExpUser = new DevExpress.XtraEditors.LabelControl();
            this.checkEditIsBreak = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditBreakDate = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditInspection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditInspectionDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTreatment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTreatment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsBreak.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonSaveInspection
            // 
            this.simpleButtonSaveInspection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSaveInspection.Location = new System.Drawing.Point(516, 476);
            this.simpleButtonSaveInspection.Name = "simpleButtonSaveInspection";
            this.simpleButtonSaveInspection.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSaveInspection.TabIndex = 21;
            this.simpleButtonSaveInspection.Text = "Хадгалах";
            this.simpleButtonSaveInspection.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSaveInspection.Click += new System.EventHandler(this.simpleButtonSaveInspection_Click);
            // 
            // simpleButtonbackInspection
            // 
            this.simpleButtonbackInspection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonbackInspection.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonbackInspection.Location = new System.Drawing.Point(597, 476);
            this.simpleButtonbackInspection.Name = "simpleButtonbackInspection";
            this.simpleButtonbackInspection.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonbackInspection.TabIndex = 22;
            this.simpleButtonbackInspection.Text = "Болих";
            this.simpleButtonbackInspection.ToolTip = "Болих";
            // 
            // memoEditInspection
            // 
            this.memoEditInspection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditInspection.Location = new System.Drawing.Point(148, 82);
            this.memoEditInspection.Name = "memoEditInspection";
            this.memoEditInspection.Properties.MaxLength = 200;
            this.memoEditInspection.Size = new System.Drawing.Size(503, 70);
            this.memoEditInspection.TabIndex = 3;
            // 
            // memoEditInspectionDescription
            // 
            this.memoEditInspectionDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditInspectionDescription.Location = new System.Drawing.Point(148, 158);
            this.memoEditInspectionDescription.Name = "memoEditInspectionDescription";
            this.memoEditInspectionDescription.Properties.MaxLength = 200;
            this.memoEditInspectionDescription.Size = new System.Drawing.Size(503, 70);
            this.memoEditInspectionDescription.TabIndex = 4;
            // 
            // dateEditInspectionDate
            // 
            this.dateEditInspectionDate.EditValue = null;
            this.dateEditInspectionDate.Location = new System.Drawing.Point(148, 56);
            this.dateEditInspectionDate.Name = "dateEditInspectionDate";
            this.dateEditInspectionDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditInspectionDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditInspectionDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditInspectionDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditInspectionDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditInspectionDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditInspectionDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditInspectionDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditInspectionDate.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(101, 59);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(41, 13);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "Огноо*:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(33, 85);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(109, 13);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Үзлэгийн тэмдэглэл*:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(96, 161);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(46, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Тайлбар:";
            // 
            // gridControlTreatment
            // 
            this.gridControlTreatment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlTreatment.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlTreatment.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlTreatment.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlTreatment.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlTreatment.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlTreatment.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "edit")});
            this.gridControlTreatment.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlTreatment_EmbeddedNavigator_ButtonClick);
            this.gridControlTreatment.Location = new System.Drawing.Point(12, 238);
            this.gridControlTreatment.MainView = this.gridViewTreatment;
            this.gridControlTreatment.Name = "gridControlTreatment";
            this.gridControlTreatment.Size = new System.Drawing.Size(660, 232);
            this.gridControlTreatment.TabIndex = 11;
            this.gridControlTreatment.UseEmbeddedNavigator = true;
            this.gridControlTreatment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTreatment});
            // 
            // gridViewTreatment
            // 
            this.gridViewTreatment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnStartDate,
            this.gridColumnEndDate,
            this.gridColumnMedicineName,
            this.gridColumnDuration,
            this.gridColumnEachDay,
            this.gridColumnDescription});
            this.gridViewTreatment.GridControl = this.gridControlTreatment;
            this.gridViewTreatment.Name = "gridViewTreatment";
            this.gridViewTreatment.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnStartDate
            // 
            this.gridColumnStartDate.Caption = "Эхэлсэн хугацаа";
            this.gridColumnStartDate.CustomizationCaption = "Эхэлсэн хугацаа";
            this.gridColumnStartDate.FieldName = "startDate";
            this.gridColumnStartDate.Name = "gridColumnStartDate";
            this.gridColumnStartDate.OptionsColumn.AllowEdit = false;
            this.gridColumnStartDate.Visible = true;
            this.gridColumnStartDate.VisibleIndex = 0;
            // 
            // gridColumnEndDate
            // 
            this.gridColumnEndDate.Caption = "Дууссан хугацаа";
            this.gridColumnEndDate.CustomizationCaption = "Дууссан хугацаа";
            this.gridColumnEndDate.FieldName = "endDate";
            this.gridColumnEndDate.Name = "gridColumnEndDate";
            this.gridColumnEndDate.OptionsColumn.AllowEdit = false;
            this.gridColumnEndDate.Visible = true;
            this.gridColumnEndDate.VisibleIndex = 1;
            // 
            // gridColumnMedicineName
            // 
            this.gridColumnMedicineName.Caption = "Эмийн нэр";
            this.gridColumnMedicineName.CustomizationCaption = "Худалдааны нэр";
            this.gridColumnMedicineName.FieldName = "name";
            this.gridColumnMedicineName.Name = "gridColumnMedicineName";
            this.gridColumnMedicineName.OptionsColumn.AllowEdit = false;
            this.gridColumnMedicineName.Visible = true;
            this.gridColumnMedicineName.VisibleIndex = 2;
            // 
            // gridColumnDuration
            // 
            this.gridColumnDuration.Caption = "Үргэлжлэх хугацаа";
            this.gridColumnDuration.CustomizationCaption = "Үргэлжлэх хугацаа";
            this.gridColumnDuration.FieldName = "duration";
            this.gridColumnDuration.Name = "gridColumnDuration";
            this.gridColumnDuration.OptionsColumn.AllowEdit = false;
            this.gridColumnDuration.Visible = true;
            this.gridColumnDuration.VisibleIndex = 4;
            // 
            // gridColumnEachDay
            // 
            this.gridColumnEachDay.Caption = "Өдөрт уух хэмжээ";
            this.gridColumnEachDay.CustomizationCaption = "Өдөрт уух хэмжээ";
            this.gridColumnEachDay.FieldName = "eachDay";
            this.gridColumnEachDay.Name = "gridColumnEachDay";
            this.gridColumnEachDay.OptionsColumn.AllowEdit = false;
            this.gridColumnEachDay.Visible = true;
            this.gridColumnEachDay.VisibleIndex = 3;
            // 
            // gridColumnDescription
            // 
            this.gridColumnDescription.Caption = "Тайлбар";
            this.gridColumnDescription.CustomizationCaption = "Тайлбар";
            this.gridColumnDescription.FieldName = "description";
            this.gridColumnDescription.Name = "gridColumnDescription";
            this.gridColumnDescription.OptionsColumn.AllowEdit = false;
            this.gridColumnDescription.Visible = true;
            this.gridColumnDescription.VisibleIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 219);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(53, 13);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "Эмчилгээ :";
            // 
            // gridLookUpEditDoctor
            // 
            this.gridLookUpEditDoctor.EditValue = "";
            this.gridLookUpEditDoctor.Location = new System.Drawing.Point(148, 30);
            this.gridLookUpEditDoctor.Name = "gridLookUpEditDoctor";
            this.gridLookUpEditDoctor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDoctor.Properties.DisplayMember = "fullName";
            this.gridLookUpEditDoctor.Properties.NullText = "";
            this.gridLookUpEditDoctor.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditDoctor.Properties.ValueMember = "id";
            this.gridLookUpEditDoctor.Properties.View = this.gridView4;
            this.gridLookUpEditDoctor.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditDoctor.TabIndex = 1;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Нэр";
            this.gridColumn21.CustomizationCaption = "Нэр";
            this.gridColumn21.FieldName = "firstName";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Регистер";
            this.gridColumn22.CustomizationCaption = "Регистер";
            this.gridColumn22.FieldName = "id";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Эрх";
            this.gridColumn23.CustomizationCaption = "Эрх";
            this.gridColumn23.FieldName = "roleName";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Тасаг";
            this.gridColumn24.CustomizationCaption = "Тасаг";
            this.gridColumn24.FieldName = "wardName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Ажлын газар";
            this.gridColumn25.CustomizationCaption = "Ажлын газар";
            this.gridColumn25.FieldName = "organizationName";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Албан тушаал";
            this.gridColumn26.CustomizationCaption = "Албан тушаал";
            this.gridColumn26.FieldName = "positionName";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            // 
            // labelControlExpUser
            // 
            this.labelControlExpUser.Location = new System.Drawing.Point(110, 33);
            this.labelControlExpUser.Name = "labelControlExpUser";
            this.labelControlExpUser.Size = new System.Drawing.Size(32, 13);
            this.labelControlExpUser.TabIndex = 33;
            this.labelControlExpUser.Text = "Эмч*: ";
            // 
            // checkEditIsBreak
            // 
            this.checkEditIsBreak.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditIsBreak.Location = new System.Drawing.Point(551, 30);
            this.checkEditIsBreak.Name = "checkEditIsBreak";
            this.checkEditIsBreak.Properties.Caption = "";
            this.checkEditIsBreak.Size = new System.Drawing.Size(20, 19);
            this.checkEditIsBreak.TabIndex = 5;
            this.checkEditIsBreak.CheckedChanged += new System.EventHandler(this.checkEditIsBreak_CheckedChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl5.Location = new System.Drawing.Point(445, 33);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(100, 13);
            this.labelControl5.TabIndex = 35;
            this.labelControl5.Text = "Эмчилгээг зогсоох :";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 476);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 23;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl6.Location = new System.Drawing.Point(504, 59);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(41, 13);
            this.labelControl6.TabIndex = 37;
            this.labelControl6.Text = "Огноо*:";
            // 
            // dateEditBreakDate
            // 
            this.dateEditBreakDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditBreakDate.EditValue = null;
            this.dateEditBreakDate.Location = new System.Drawing.Point(551, 56);
            this.dateEditBreakDate.Name = "dateEditBreakDate";
            this.dateEditBreakDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBreakDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBreakDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBreakDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBreakDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.ReadOnly = true;
            this.dateEditBreakDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditBreakDate.TabIndex = 6;
            // 
            // InspectionInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonbackInspection;
            this.ClientSize = new System.Drawing.Size(684, 511);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.dateEditBreakDate);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.checkEditIsBreak);
            this.Controls.Add(this.gridLookUpEditDoctor);
            this.Controls.Add(this.labelControlExpUser);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.gridControlTreatment);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.dateEditInspectionDate);
            this.Controls.Add(this.memoEditInspectionDescription);
            this.Controls.Add(this.memoEditInspection);
            this.Controls.Add(this.simpleButtonbackInspection);
            this.Controls.Add(this.simpleButtonSaveInspection);
            this.Name = "InspectionInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Үзлэг нэмэх";
            this.Shown += new System.EventHandler(this.InspectionInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.memoEditInspection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditInspectionDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTreatment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTreatment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsBreak.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonSaveInspection;
        private DevExpress.XtraEditors.SimpleButton simpleButtonbackInspection;
        private DevExpress.XtraEditors.MemoEdit memoEditInspection;
        private DevExpress.XtraEditors.MemoEdit memoEditInspectionDescription;
        private DevExpress.XtraEditors.DateEdit dateEditInspectionDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGrid.GridControl gridControlTreatment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTreatment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMedicineName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEachDay;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDuration;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDescription;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDoctor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.LabelControl labelControlExpUser;
        private DevExpress.XtraEditors.CheckEdit checkEditIsBreak;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.DateEdit dateEditBreakDate;
    }
}