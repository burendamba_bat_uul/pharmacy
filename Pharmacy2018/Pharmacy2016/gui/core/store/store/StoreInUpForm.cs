﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.store.order;
using Pharmacy2016.gui.core.journal.ds;

namespace Pharmacy2016.gui.core.store.store
{
    /*
     * Өвчний түүх нэмэх, засах цонх.
     * 
     */

    public partial class StoreInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц.

            private static StoreInUpForm INSTANCE = new StoreInUpForm();

            public static StoreInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            public DataRowView currentInspectionView;      

            public DataRowView currentDiagnoseView;

            public DataRowView currPatientInformView;

            public DataRowView currLeftStoreView;

            public DataRowView currGeneralView;

            public DataRowView currAnamnezView;

            public DataRowView currInternalView;

            public DataRowView currHeartView;

            public DataRowView currDigestiveView;

            public DataRowView currUrineView;

            public DataRowView currValidityView;

            public DataRowView currLeftDiagnoseView;

            private int actionType;

            private int formType;

            private StoreInUpForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                dateEditBeginDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditBeginDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEndDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEndDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                initError();
                initDataSource();
                initDataSourceAnaminez();
                initDataBindingGeneral();
                initDataBindingInternal();
                initDataBindingHeart();
                initDataBindingDigestive();
                initDataBindingUrine();
                initBinding();
                reload();
            }

            private void initDataSource()
            {
                gridControlWardRoom.DataSource = StoreDataSet.StoreWardRoomBindingSource;
                gridControlStoreInspection.DataSource = StoreDataSet.StoreInspectionBindingSource;
                gridControlStoreDiagnose.DataSource = StoreDataSet.StoreDiagnoseBindingSource;
                gridControlValidity.DataSource = StoreDataSet.StoreValidityBindingSource;
                gridControlStoreLeftDiagnose.DataSource = StoreDataSet.StoreLeftDiagnoseBindingSource;
               
                gridLookUpEditOrder.Properties.DataSource = StoreDataSet.PatientStoreBindingSource;
                gridLookUpEditPaymentType.Properties.DataSource = StoreDataSet.Instance.PaymentType;
                gridLookUpEditAlergyType.Properties.DataSource = StoreDataSet.Instance.AlergyType;
                treeListLookUpEditWard.Properties.DataSource = UserDataSet.WardBindingSource;


                // Гарах үед
                gridLookUpEditDoctor.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
                gridLookUpEditDiagnose.Properties.DataSource = StoreDataSet.Instance.LeftDiagnose;
                gridLookUpEditEndHurt.Properties.DataSource = StoreDataSet.Instance.EndHurt;
                gridLookUpEditTreat.Properties.DataSource = StoreDataSet.Instance.Surgery;
                gridLookUpEditInCome.Properties.DataSource = StoreDataSet.Instance.InCome;
                gridLookUpEditInBack.Properties.DataSource = StoreDataSet.Instance.InBack;
                gridLookUpEditAsTreat.Properties.DataSource = StoreDataSet.Instance.AnalysisTreat;
                gridLookUpEditAsLight.Properties.DataSource = StoreDataSet.Instance.AnalysisLight;

                //Эмнэлгийн аманез
                gridLookUpEditLifeTerm.Properties.DataSource = StoreDataSet.Instance.LifeTerm;
                gridLookUpEditWorkTerm.Properties.DataSource = StoreDataSet.Instance.WorkTerm;
                gridLookUpEditInfection.Properties.DataSource = StoreDataSet.Instance.Infection;
                gridLookUpEditDiet.Properties.DataSource = StoreDataSet.Instance.Diet;
                gridLookUpEditAllergy.Properties.DataSource = StoreDataSet.Instance.AlergyType;

                //Ерөнхий үзлэгээс.
                gridLookUpEditGeneralCondition.Properties.DataSource = StoreDataSet.Instance.InCome;
                gridLookUpEditMind.Properties.DataSource = StoreDataSet.Instance.Mind;
                gridLookUpEditSurround.Properties.DataSource = StoreDataSet.Instance.Surround;
                gridLookUpEditPos.Properties.DataSource = StoreDataSet.Instance.Position;
                gridLookUpEditSkinFlexible.Properties.DataSource = StoreDataSet.Instance.Skin;
                gridLookUpEditSkinWet.Properties.DataSource = StoreDataSet.Instance.Skin;
                gridLookUpEditGeneralPart.Properties.DataSource = StoreDataSet.Instance.GeneralPart;
                gridlookUpEditSize.Properties.DataSource = StoreDataSet.Instance.Size;
                gridLookUpEditPlace.Properties.DataSource = StoreDataSet.Instance.Place;
                gridLookUpEditMove.Properties.DataSource = StoreDataSet.Instance.Move;

                //Дотрын эмчийн үзлэг
                gridLookUpEditBreathShape.Properties.DataSource = StoreDataSet.Instance.BreathShape;
                gridLookUpEditSoundVib.Properties.DataSource = StoreDataSet.Instance.SoundVib;
                gridLookUpEditPartPercussion.Properties.DataSource = StoreDataSet.Instance.PartPercussion;
                gridLookUpEditIfDesease.Properties.DataSource = StoreDataSet.Instance.IfDesease;
                gridLookUpEditIfNoisy.Properties.DataSource = StoreDataSet.Instance.IfNoisy;
                gridLookUpEditBronkhofoni.Properties.DataSource = StoreDataSet.Instance.Bronkhofoni;

                //Зүрх судасны тогтолцоо
                gridLookUpEditHeartRisk.Properties.DataSource = StoreDataSet.Instance.HeartRisk;
                gridLookUpEditStrong.Properties.DataSource = StoreDataSet.Instance.Strong;
                gridLookUpEditVoltage.Properties.DataSource = StoreDataSet.Instance.Voltage;
                gridLookUpEditSezi.Properties.DataSource = StoreDataSet.Instance.Direction;
                gridLookUpEditFirstSound.Properties.DataSource = StoreDataSet.Instance.FirstSound;
                gridLookUpEditSecondSound.Properties.DataSource = StoreDataSet.Instance.SecondSound;
                gridLookUpEditThirdSound.Properties.DataSource = StoreDataSet.Instance.ThirdSound;
                gridLookUpEditPost.Properties.DataSource = StoreDataSet.Instance.Romb;
                gridLookUpEditSis.Properties.DataSource = StoreDataSet.Instance.Romb;
                gridLookUpEditDis.Properties.DataSource = StoreDataSet.Instance.Romb;
                gridLookUpEditTransStrong.Properties.DataSource = StoreDataSet.Instance.Strong;

                //Хоол боловсруулах эрхтэн тогтолцоо
                gridLookUpEditIsPercussion.Properties.DataSource = StoreDataSet.Instance.IsPercussion;
                gridLookUpEditIsMoving.Properties.DataSource = StoreDataSet.Instance.IsMoving;
                gridLookUpEditLiver.Properties.DataSource = StoreDataSet.Instance.Liver;

                //Шээсний бэлгийн тогтолцоо
                gridLookUpEditUrineOutPut.Properties.DataSource = StoreDataSet.Instance.UrineOutput;
                gridLookUpEditUrineColor.Properties.DataSource = StoreDataSet.Instance.UrineColor;
                gridLookUpEditShallow.Properties.DataSource = StoreDataSet.Instance.UrineOutput;
                gridLookUpeditDeep.Properties.DataSource = StoreDataSet.Instance.UrineOutput;
                gridLookUpEditQuiteDeep.Properties.DataSource = StoreDataSet.Instance.UrineOutput;
            }

            private void initBinding()
            {
                textEditNumber.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "number", true));             

                // Эмчлүүлэгчийн түүхээс
                gridLookUpEditOrder.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "orderID", true));
                dateEditBeginDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "beginDate", true));
                treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "wardID", true));             
                gridLookUpEditPaymentType.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "paymentType", true));
                checkEditIsUnder.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "isUnder", true));
                gridLookUpEditAlergyType.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "alergyType", true));                          
                radioGroupLoopState.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "isLoop", true));

                
                textEditEmergencyName.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "emergencyName", true));
                textEditEmergencyPhone.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "emergencyPhone", true));
                memoEditBeginDiagnose.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "beginDiagnose", true));

                //Дугаар олголтоос
                textEditRegister.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "patientID", true));
                textEditLastName.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "lastName", true));
                textEditFirstName.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "firstName", true));
                textEditEmd.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "emdNumber", true));
                dateEditBirthdate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "birthday", true));
                radioGroupGender.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "gender", true));
                radioGroupIsMarried.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "isMarried", true));
                textEditOrganization.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "organizationName", true));
                textEditPosition.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "positionName", true));
                textEditProvince.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "provinceName", true));
                textEditSpecialty.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "specialtyName", true));
                //textEditEducation.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "education", true));
                memoEditAddress.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "address", true));


                // Гарах үеээс.
                //checkEditIsClose.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreBindingSource, "isClose", true));
                gridLookUpEditDoctor.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "userID", true));
                gridLookUpEditDiagnose.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "diagnoseType", true));
                radionGroupLeaveHospital.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "leaveHospital", true));
                dateEditEndDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "endDate", true));
                gridLookUpEditEndHurt.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "endHurt", true));
                spinEditDuration.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "duration", true));
                spinEditBill.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "bill", true));
                memoEditBillOfMedic.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "billOfMedic", true));
                gridLookUpEditInCome.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "inCome", true));
                gridLookUpEditInBack.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "inBack", true));
                gridLookUpEditAsTreat.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "asTreat", true));
                gridLookUpEditAsLight.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "asLight", true));
                memoEditAnalysisChanged.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "analysisChanged", true));
                memoEditAnalysisGeneral.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "analysisGeneral", true));
                memoEditAnalysisBlood.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "analysisBlood", true));
                memoEditAnalysisVirus.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "analysisVirus", true));
                gridLookUpEditTreat.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "treat", true));
                checkEditBlood.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "blood", true));
                memoEditOtherTreat.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "otherTreat", true));
                checkEditIsSurgery.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "isSurgery", true));
                memoEditSurgery.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "surgery", true));
                radioGroupIsHeal.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "isHeal", true));
                checkEditIsAfterSurgery.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "isAfterSurgery", true));
                memoEditAfterSurgery.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftBindingSource, "afterSurgery", true));
            }

            private void initError()
            {
                gridLookUpEditOrder.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditNumber.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditBeginDate.ErrorIconAlignment = ErrorIconAlignment.MiddleLeft;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditPaymentType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditAlergyType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditEmergencyName.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                textEditEmergencyPhone.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                memoEditBeginDiagnose.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;

                // Гарах үед
                dateEditBeginDate.ErrorIconAlignment = ErrorIconAlignment.MiddleLeft;
                gridLookUpEditDiagnose.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditEndDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditBill.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                memoEditBillOfMedic.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditDoctor.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;

                memoEditLifeStore.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

            private void initDataSourceAnaminez()
            {
                spinEditHigh.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "high", true));
                spinEditWeight.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "weight", true));
                spinEditBodyIndex.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "bodyIndex", true));
                radioGroupBloodPressure.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "bloodPressure", true));
                memoEditPain.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "pain", true));
                memoEditHistory.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "history", true));
                memoEditLifeStore.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "lifeStore", true));
                gridLookUpEditLifeTerm.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "lifeTerm", true));
                gridLookUpEditWorkTerm.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "workTerm", true));
                gridLookUpEditInfection.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "infection", true));
                checkEditDoesSurgery.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "isSurgery", true));
                memoEditAccident.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "accident", true));
                gridLookUpEditAllergy.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "allergy", true));
                memoEditInheritance.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "inheritance", true));
                gridLookUpEditDiet.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "diet", true));
                radioGroupIsVodka.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "isVodka", true));
                RadioGroupIsSmoke.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "isSmoke", true));
                memoEditHowLong.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHistoryBindingSource, "howLong", true));
            }

            private void initDataBindingGeneral()
            {
                gridLookUpEditGeneralCondition.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "generalCondition", true));
                gridLookUpEditMind.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "mind", true));
                gridLookUpEditSurround.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "surround", true));
                gridLookUpEditPos.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "position", true));
                radioGroupSkinSalst.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "skinSalst", true));
                gridLookUpEditSkinFlexible.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "skinFlexible", true));
                gridLookUpEditSkinWet.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "skinWet", true));
                radioGroupSkinRash.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "skinRash", true));
                radioGroupEdema.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "edema", true));
                gridLookUpEditGeneralPart.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "generalPart", true));
                gridlookUpEditSize.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "size", true));
                gridLookUpEditPlace.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "place", true));
                radioGroupConcern.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "concern", true));
                radioGroupShape.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "shape", true));
                gridLookUpEditMove.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreGeneralInspectionBindingSource, "move", true));
            }

            private void initDataBindingInternal()
            {
                radioGroupIsBreathing.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "isBreathing", true));
                radioGroupIsCyanosis.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "isCyanosis", true));
                radioGroupIfCyanosis.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "ifCyanosis", true));
                radioGroupIsBreathMuscles.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "isBreathMuscles", true));
                spinEditBreathCount.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "breathCount", true));
                radioGroupIsCount.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "isCount", true));
                radioGroupChestShape.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "chestShape", true));
                gridLookUpEditBreathShape.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "breathShape", true));
                radioGroupBreathChest.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "breathChest", true));
                radioGroupRightLeft.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "rightLeft", true));
                radioGroupIconcern.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "concern", true));
                radioGroupIsFlexible.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "isFlexible", true));
                gridLookUpEditSoundVib.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "soundVib", true));
                radioGroupPercussion.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "percussion", true));
                gridLookUpEditPartPercussion.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "partPercussion", true));
                radioGroupBreathDouble.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "breathDouble", true));
                radioGroupIsDesease.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "isDesease", true));
                gridLookUpEditIfDesease.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "ifDesease", true));
                radioGroupIsNoisy.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "isNoisy", true));
                gridLookUpEditIfNoisy.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "ifNoisy", true));
                gridLookUpEditBronkhofoni.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInternalinspectionBindingSource, "bronkhofoni", true));
            }

            private void initDataBindingHeart()
            {
                gridLookUpEditHeartRisk.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "heartRisk", true));
                radioGroupSkinBlue.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "skinBlue", true));
                radioGroupVenous.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "venous", true));
                radioGroupIsEdem.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "isEdema", true));
                radioGroupHeartPush.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "heartPush", true));
                radioGroupHeartPlace.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "heartPlace", true));
                gridLookUpEditStrong.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "strong", true));
                radioGroupRate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "rate", true));
                spinEditFrequency.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "frequency", true));
                gridLookUpEditVoltage.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "voltage", true));
                radioGroupFilled.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "filled", true));
                radioGroupSameDouble.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "sameDouble", true));
                radioGroupHeartLimit.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "heartLimit", true));
                gridLookUpEditSezi.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "direction", true));
                radioGroupHeartSound.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "heartSound", true));
                spinEditHeartFrequency.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "heartFrequency", true));
                gridLookUpEditFirstSound.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "firstSound", true));
                gridLookUpEditSecondSound.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "secondSound", true));
                gridLookUpEditThirdSound.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "thirdSound", true));
                radioGroupThisNoisy.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "realNoise", true));
                gridLookUpEditPost.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "pos", true));
                gridLookUpEditSis.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "sis", true));
                gridLookUpEditDis.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "dis", true));
                radioGroupTransmission.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "transmission", true));
                gridLookUpEditTransStrong.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "transStrong", true));
                radioGroupIsTouch.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreHeartBindingSource, "isTouch", true));
            }

            private void initDataBindingDigestive()
            {
                radioGroupIsTongue.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isTongue", true));
                radioGroupIsSkinWet.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isSkingWet", true));
                textEditColor.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "color", true));
                radioGroupIsTuck.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isTuck", true));
                textEditTongueShape.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "tongueShape", true));
                radioGroupTuckConcern.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "tuckConcern", true));
                radioGroupIsTough.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isTough", true));
                radioGroupIsTympanic.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isTympanic", true));
                gridLookUpEditIsPercussion.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isPercussion", true));
                gridLookUpEditIsMoving.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isMoving", true));
                radioGroupBellyConcern.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "bellyConcern", true));
                radioGroupIsBellyStable.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isBellyStable", true));
                radioGroupIsBellyPos.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isBellyPos", true));
                radioGroupPosConcern.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "posConcern", true));
                radioGroupIsPosStable.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isPosStable", true));
                radioGroupIsPos.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isPos", true));
                radioGroupCrossConcern.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "crossConcern", true));
                radioGroupIsCrossStable.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isCrossStable", true));
                radioGroupIsCrossPos.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isCrossPos", true));
                radioGroupTideConcern.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "tideConcern", true));
                radioGroupIsTideStable.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isTideStable", true));
                radioGroupIsTidePos.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "isTidePos", true));
                gridLookUpEditLiver.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "liver", true));
                textEditDuringDay.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "duringDay", true));
                radioGroupLiverSize.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "liverSize", true));
                radioGroupLiverDirection.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "liverDirection", true));
                radioGroupSpleen.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDigestiveBindingSource, "spleen", true));
            }

            private void initDataBindingUrine()
            {
                gridLookUpEditUrineOutPut.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "urineOutput", true));
                gridLookUpEditUrineColor.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "urineColor", true));
                radioGroupIsUrineNight.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "isUrineNight", true));
                spinEditIsUrineRight.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "isUrineTrue", true));
                radioGroupCutUrine.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "cutUrine", true));
                radioGroupMissUrine.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "missUrine", true));
                radioGroupPushUrine.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "pushUrine", true));
                radioGroupHurtUrine.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "hurtUrine", true));
                radioGroupKidney.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "kidneyConcern", true));
                radioGroupPastyernatskii.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "pastyernatskii", true));
                radioGroupSmell.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "smell", true));
                radioGroupHear.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "hear", true));
                radioGroupFaceDouble.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "faceDouble", true));
                radioGroupReflex.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "reflex", true));
                gridLookUpEditShallow.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "shallow", true));
                gridLookUpeditDeep.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "deep", true));
                gridLookUpEditQuiteDeep.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "quiteDeep", true));
                memoEditMentalStatus.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "mentalStatus", true));
                memoEditOther.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreUrineBindingSource, "other", true));
            }

        #endregion

        #region Форм нээх, хаах функц.

            public void showForm(int type, int form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    formType = form;
                    checkRole();
                    if (actionType == 1)
                    {
                        insertStore();
                    }
                    else if (actionType == 2)
                    {
                        updateStore();
                    }
                    else if (actionType == 3)
                    {
                        insertStoreFromOrder();
                    }

                    ShowDialog();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                //Эмчийн эрх
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    setStoreEdit(true);

                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;

                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;

                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;

                    xtraTabPage1.PageVisible = false;
                    xtraTabPage5.PageVisible = true;
                    xtraTabPage6.PageVisible = true;
                }
                //Сувилагчийн эрх
                else if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID)
                {
                    setStoreEdit(true);

                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;

                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;

                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;

                    xtraTabPage1.PageVisible = false;
                    xtraTabPage5.PageVisible = false;
                    xtraTabPage6.PageVisible = false;
                }
                //Ахлах сувилагчийн эрх
                else if (UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                {
                    setStoreEdit(true);

                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;

                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;

                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;

                    xtraTabPage1.PageVisible = false;
                    xtraTabPage5.PageVisible = false;
                    xtraTabPage6.PageVisible = false;
                }
                // Хүлээн авхын Эрх
                else if (UserUtil.getUserRoleId() == RoleUtil.RECEPTION_ID)
                {
                    setStoreEdit(false);

                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;

                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;

                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;

                    xtraTabPage1.PageVisible = true;
                    xtraTabPage5.PageVisible = false;
                    xtraTabPage6.PageVisible = true;
                }
                else
                {
                    setStoreEdit(false);

                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;

                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridControlStoreInspection.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;

                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    gridControlStoreDiagnose.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;

                    xtraTabPage1.PageVisible = true;
                    xtraTabPage5.PageVisible = true;
                    xtraTabPage6.PageVisible = true;
                }
            }

            private void setStoreEdit(bool readOnly)
            {
                textEditNumber.Properties.ReadOnly = readOnly;
                treeListLookUpEditWard.Properties.ReadOnly = readOnly;
                dateEditBeginDate.Properties.ReadOnly = readOnly;
                memoEditBeginDiagnose.Properties.ReadOnly = readOnly;
                gridLookUpEditPaymentType.Properties.ReadOnly = readOnly;
                gridLookUpEditAlergyType.Properties.ReadOnly = readOnly;
                radioGroupLoopState.Properties.ReadOnly = readOnly;
                checkEditIsUnder.Properties.ReadOnly = readOnly;
                textEditEmergencyName.Properties.ReadOnly = readOnly;
                textEditEmergencyPhone.Properties.ReadOnly = readOnly;
            }

            private void checkApply(bool editable)
            {
                gridLookUpEditDoctor.Enabled = editable;
                gridLookUpEditDiagnose.Enabled = editable;
                radionGroupLeaveHospital.Enabled = editable;
                dateEditEndDate.Enabled = editable;
                gridLookUpEditEndHurt.Enabled = editable;
                spinEditDuration.Enabled = editable;
                spinEditBill.Enabled = editable;
                memoEditBillOfMedic.Enabled = editable;
                gridLookUpEditInCome.Enabled = editable;
                gridLookUpEditInBack.Enabled = editable;
                gridLookUpEditAsTreat.Enabled = editable;
                gridLookUpEditAsLight.Enabled = editable;
                memoEditAnalysisChanged.Enabled = editable;
                memoEditAnalysisGeneral.Enabled = editable;
                memoEditAnalysisBlood.Enabled = editable;
                memoEditAnalysisVirus.Enabled = editable;
                gridLookUpEditTreat.Enabled = editable;
                checkEditBlood.Enabled = editable;
                memoEditOtherTreat.Enabled = editable;
                checkEditIsSurgery.Enabled = editable;
                memoEditSurgery.Enabled = editable;
                radioGroupIsHeal.Enabled = editable;
                checkEditIsAfterSurgery.Enabled = editable;
                memoEditAfterSurgery.Enabled = editable;
            }

            private void checkStore(bool editable)
            {
                spinEditHigh.Enabled = editable;
                spinEditWeight.Enabled = editable;
                spinEditBodyIndex.Enabled = editable;
                radioGroupBloodPressure.Enabled = editable;
                memoEditPain.Enabled = editable;
                memoEditHistory.Enabled = editable;
                memoEditLifeStore.Enabled = editable;
                gridLookUpEditLifeTerm.Enabled = editable;
                gridLookUpEditWorkTerm.Enabled = editable;
                gridLookUpEditInfection.Enabled = editable;
                checkEditDoesSurgery.Enabled = editable;
                memoEditAccident.Enabled = editable;
                gridLookUpEditAllergy.Enabled = editable;
                memoEditInheritance.Enabled = editable;
                gridLookUpEditDiet.Enabled = editable;
                radioGroupIsVodka.Enabled = editable;
                RadioGroupIsSmoke.Enabled = editable;
                memoEditHowLong.Enabled = editable;              
                
            }

            private void checkGeneral(bool editable)
            {
                gridLookUpEditGeneralCondition.Enabled = editable;
                gridLookUpEditMind.Enabled = editable;
                gridLookUpEditSurround.Enabled = editable;
                gridLookUpEditPos.Enabled = editable;
                radioGroupSkinSalst.Enabled = editable;
                gridLookUpEditSkinFlexible.Enabled = editable;
                gridLookUpEditSkinWet.Enabled = editable;
                radioGroupSkinRash.Enabled = editable;
                radioGroupEdema.Enabled = editable;
                gridLookUpEditGeneralPart.Enabled = editable;
                gridlookUpEditSize.Enabled = editable;
                gridLookUpEditPlace.Enabled = editable;
                radioGroupConcern.Enabled = editable;
                radioGroupShape.Enabled = editable;
                gridLookUpEditMove.Enabled = editable;
            }

            private void checkInternal(bool editable)
            {
                radioGroupIsBreathing.Enabled = editable;
                radioGroupIsCyanosis.Enabled = editable;
                radioGroupIfCyanosis.Enabled = editable;
                radioGroupIsBreathMuscles.Enabled = editable;
                spinEditBreathCount.Enabled = editable;
                radioGroupIsCount.Enabled = editable;
                radioGroupChestShape.Enabled = editable;
                gridLookUpEditBreathShape.Enabled = editable;
                radioGroupBreathChest.Enabled = editable;
                radioGroupRightLeft.Enabled = editable;
                radioGroupIconcern.Enabled = editable;
                radioGroupIsFlexible.Enabled = editable;
                gridLookUpEditSoundVib.Enabled = editable;
                radioGroupPercussion.Enabled = editable;
                gridLookUpEditPartPercussion.Enabled = editable;
                radioGroupBreathDouble.Enabled = editable;
                radioGroupIsDesease.Enabled = editable;
                gridLookUpEditIfDesease.Enabled = editable;
                radioGroupIsNoisy.Enabled = editable;
                gridLookUpEditIfNoisy.Enabled = editable;
                gridLookUpEditBronkhofoni.Enabled = editable;
            }

            private void checkHeart(bool editable)
            {
                gridLookUpEditHeartRisk.Enabled = editable;
                radioGroupSkinBlue.Enabled = editable;
                radioGroupVenous.Enabled = editable;
                radioGroupIsEdem.Enabled = editable;
                radioGroupHeartPush.Enabled = editable;                        
                radioGroupHeartPlace.Enabled = editable;
                gridLookUpEditStrong.Enabled = editable;
                radioGroupRate.Enabled = editable;
                spinEditFrequency.Enabled = editable;
                gridLookUpEditVoltage.Enabled = editable;
                radioGroupFilled.Enabled = editable;
                radioGroupSameDouble.Enabled = editable;
                radioGroupHeartLimit.Enabled = editable;                   
                gridLookUpEditSezi.Enabled = editable;
                radioGroupHeartSound.Enabled = editable;
                spinEditHeartFrequency.Enabled = editable;
                gridLookUpEditFirstSound.Enabled = editable;
                gridLookUpEditSecondSound.Enabled = editable;
                gridLookUpEditThirdSound.Enabled = editable;
                radioGroupThisNoisy.Enabled = editable;
                gridLookUpEditPost.Enabled = editable;
                gridLookUpEditSis.Enabled = editable;
                gridLookUpEditDis.Enabled = editable;
                radioGroupTransmission.Enabled = editable;
                gridLookUpEditTransStrong.Enabled = editable;
                radioGroupIsTouch.Enabled = editable;
            }

            private void checkDigestive(bool editable)
            {
                radioGroupIsTongue.Enabled = editable;
                radioGroupIsSkinWet.Enabled = editable;
                textEditColor.Enabled = editable;
                radioGroupIsTuck.Enabled = editable;
                textEditTongueShape.Enabled = editable;
                radioGroupTuckConcern.Enabled = editable;
                radioGroupIsTough.Enabled = editable;
                radioGroupIsTympanic.Enabled = editable;
                gridLookUpEditIsPercussion.Enabled = editable;
                gridLookUpEditIsMoving.Enabled = editable;
                radioGroupBellyConcern.Enabled = editable;
                radioGroupIsBellyStable.Enabled = editable;
                radioGroupIsBellyPos.Enabled = editable;
                radioGroupPosConcern.Enabled = editable;
                radioGroupIsPosStable.Enabled = editable;
                radioGroupIsPos.Enabled = editable;
                radioGroupCrossConcern.Enabled = editable;
                radioGroupIsCrossStable.Enabled = editable;
                radioGroupIsCrossPos.Enabled = editable;
                radioGroupTideConcern.Enabled = editable;
                radioGroupIsTideStable.Enabled = editable;
                radioGroupIsTidePos.Enabled = editable;
                gridLookUpEditLiver.Enabled = editable;
                textEditDuringDay.Enabled = editable;
                radioGroupLiverSize.Enabled = editable;
                radioGroupLiverDirection.Enabled = editable;
                radioGroupSpleen.Enabled = editable;
            }

            private void checkUrine(bool editable)
            {
                gridLookUpEditUrineOutPut.Enabled = editable;
                gridLookUpEditUrineColor.Enabled = editable;
                radioGroupIsUrineNight.Enabled = editable;
                spinEditIsUrineRight.Enabled = editable;
                radioGroupCutUrine.Enabled = editable;
                radioGroupMissUrine.Enabled = editable;
                radioGroupPushUrine.Enabled = editable;
                radioGroupHurtUrine.Enabled = editable;
                radioGroupKidney.Enabled = editable;
                radioGroupPastyernatskii.Enabled = editable;
                radioGroupSmell.Enabled = editable;
                radioGroupHear.Enabled = editable;
                radioGroupFaceDouble.Enabled = editable;
                radioGroupReflex.Enabled = editable;
                gridLookUpEditShallow.Enabled = editable;
                gridLookUpeditDeep.Enabled = editable;
                gridLookUpEditQuiteDeep.Enabled = editable;
                memoEditMentalStatus.Enabled = editable;
                memoEditOther.Enabled = editable;
            }

            private void insertStore()
            {
                StoreDataSet.PatientStoreTableAdapter.Fill(StoreDataSet.Instance.PatientStore, HospitalUtil.getHospitalId(), StoreForm.Instance.getStartDate(), StoreForm.Instance.getEndDate(), true);
                textEditNumber.Properties.ReadOnly = false;
                gridLookUpEditOrder.Properties.ReadOnly = false;
                treeListLookUpEditWard.Properties.ReadOnly = false;
                gridLookUpEditDiagnose.Properties.ReadOnly = false;
                memoEditSurgery.Properties.ReadOnly = true;
                memoEditAfterSurgery.Properties.ReadOnly = true;
                memoEditOtherTreat.Properties.ReadOnly = true;
                checkEditBlood.Properties.ReadOnly = true;

                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    StoreInUpForm.Instance.currentInspectionView["userID"] = UserUtil.getUserId();
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                    gridLookUpEditDoctor.EditValue = UserUtil.getUserId();
                }
                gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
                
                setAdditional();
            }

            private void updateStore()
            {
                StoreDataSet.PatientStoreTableAdapter.Fill(StoreDataSet.Instance.PatientStore, HospitalUtil.getHospitalId(), StoreForm.Instance.getStartDate(), StoreForm.Instance.getEndDate(), false);
                textEditNumber.Properties.ReadOnly = true;
                gridLookUpEditOrder.Properties.ReadOnly = true;
                treeListLookUpEditWard.Properties.ReadOnly = true;
                gridLookUpEditDiagnose.Properties.ReadOnly = false;
                memoEditSurgery.Properties.ReadOnly = true;
                memoEditAfterSurgery.Properties.ReadOnly = true;
                memoEditOtherTreat.Properties.ReadOnly = true;
                checkEditBlood.Properties.ReadOnly = true;

                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                }

                gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;

                setAdditional();
            }

            private void insertStoreFromOrder()
            {
                StoreDataSet.PatientStoreTableAdapter.Fill(StoreDataSet.Instance.PatientStore, HospitalUtil.getHospitalId(), StoreForm.Instance.getStartDate(), StoreForm.Instance.getEndDate(), true);
                textEditNumber.Properties.ReadOnly = false;
                gridLookUpEditOrder.Properties.ReadOnly = false;
                treeListLookUpEditWard.Properties.ReadOnly = false;
                gridLookUpEditDiagnose.Properties.ReadOnly = false;
                memoEditSurgery.Properties.ReadOnly = true;
                memoEditAfterSurgery.Properties.ReadOnly = true;
                memoEditOtherTreat.Properties.ReadOnly = true;
                checkEditBlood.Properties.ReadOnly = true;

                gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                gridControlWardRoom.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;

                setAdditional();
            }
            
            //Итэвхтэй болгох функц.
            private void setAdditional()
            {
                if (checkEditIsClose.Checked)
                {
                    checkApply(true);
                }
                else
                {
                    checkApply(false);
                }

                if (checkEditStore.Checked)
                {
                    checkStore(true);
                }
                else
                {
                    checkStore(false);
                }

                if (checkEditGeneral.Checked)
                {
                    checkGeneral(true);
                }
                else
                {
                    checkGeneral(false);
                }

                if (checkEditInternal.Checked)
                {
                    checkInternal(true);
                }
                else
                {
                    checkInternal(false);
                }

                if (checkEditHeart.Checked)
                {
                    checkHeart(true);
                }
                else
                {
                    checkHeart(false);
                }

                if (checkEditDigestive.Checked)
                {
                    checkDigestive(true);
                }
                else
                {
                    checkDigestive(false);
                }

                if (checkEditUrine.Checked)
                {
                    checkUrine(true);
                }
                else
                {
                    checkUrine(false);
                }
            }

            private void StoreInUpForm_Shown(object sender, EventArgs e)
            {
                if (actionType == 1)
                {
                    textEditNumber.Focus();
                }
                else if (actionType == 3)
                {
                    StoreForm.Instance.currentStoreView["orderID"] = OrderInUpForm.Instance.currID;
                    gridLookUpEditOrder.EditValue = OrderInUpForm.Instance.currID;
                    DataRow[] row = StoreDataSet.Instance.PatientStore.Select("id = '" + OrderInUpForm.Instance.currID + "'");
                    if (row != null && row.Length > 0)
                    {
                        textEditLastName.EditValue = row[0]["lastName"];
                        textEditFirstName.EditValue = row[0]["firstName"];
                        dateEditBirthdate.EditValue = row[0]["birthday"];
                        textEditRegister.EditValue = row[0]["userID"];
                        textEditEmd.EditValue = row[0]["emdNumber"];
                        radioGroupGender.EditValue = row[0]["gender"];
                        radioGroupIsMarried.EditValue = row[0]["isMarried"];
                        textEditOrganization.EditValue = row[0]["organizationName"];
                        textEditPosition.EditValue = row[0]["positionName"];
                        textEditSpecialty.EditValue = row[0]["specialtyName"];
                        textEditProvince.EditValue = row[0]["provinceName"];
                        if (row[0]["education"] != DBNull.Value && row[0]["education"] != null)
                            textEditEducation.EditValue = InformationDataSet.getEducationName(Convert.ToInt32(row[0]["education"]));
                        else
                            textEditEducation.EditValue = "";
                        memoEditAddress.EditValue = row[0]["address"];
                    }

                    textEditNumber.Focus();
                }
                else if (actionType == 2)
                {
                    dateEditBeginDate.Focus();
                    if (StoreForm.Instance.currentStoreView["education"] != DBNull.Value && StoreForm.Instance.currentStoreView["education"] != null)
                        textEditEducation.EditValue = InformationDataSet.getEducationName(Convert.ToInt32(StoreForm.Instance.currentStoreView["education"]));
                    else
                        textEditEducation.EditValue = "";

                }


                xtraTabControlAnamnez.SelectedTabPage = xtraTabPageAnamnez1;

                xtraTabControl.SelectedTabPage = xtraTabPage6;
                xtraTabControl.SelectedTabPage = xtraTabPage5;
                xtraTabControlStoreLeft.SelectedTabPage = xtraTabPageStoreLeftThird;
                xtraTabControlStoreLeft.SelectedTabPage = xtraTabPageStoreLeftSecond;
                xtraTabControlStoreLeft.SelectedTabPage = xtraTabPageStoreLeftFirst;
                xtraTabControl.SelectedTabPage = xtraTabPage4;
                xtraTabControl.SelectedTabPage = xtraTabPage3;
                xtraTabControl.SelectedTabPage = xtraTabPage2;
                xtraTabControl.SelectedTabPage = xtraTabPage1;

                treeListLookUpEditWard.Properties.TreeList.ExpandAll();

                checkEditIsClose.Checked = false;
                checkEditStore.Checked = false;
                checkEditGeneral.Checked = false;
                checkEditInternal.Checked = false;
                checkEditHeart.Checked = false;
                checkEditDigestive.Checked = false;
                checkEditUrine.Checked = false;
            }

            private void clearErrorTextStoreLeft()
            {
                gridLookUpEditDiagnose.ErrorText = "";
                spinEditBill.ErrorText = "";
                dateEditEndDate.ErrorText = "";
                memoEditBillOfMedic.ErrorText = "";
                gridLookUpEditDoctor.ErrorText = "";
            }
            
            private void clearData()
            {
                clearErrorText();
                clearErrorTextStoreLeft();                

                currLeftStoreView = null;
                currentDiagnoseView = null;
                currentInspectionView = null;
                currPatientInformView = null;
                currAnamnezView = null;
                currGeneralView = null;
                currInternalView = null;
                currDigestiveView = null;
                currUrineView = null;
                currValidityView = null;
                currLeftDiagnoseView = null;

                StoreDataSet.StoreBindingSource.CancelEdit();
                StoreDataSet.Instance.Store.RejectChanges();
            }

            private void clearErrorText()
            {
                gridLookUpEditOrder.ErrorText = "";
                textEditNumber.ErrorText = "";
                dateEditBeginDate.ErrorText = "";
                treeListLookUpEditWard.ErrorText = "";
                gridLookUpEditPaymentType.ErrorText = "";
                gridLookUpEditAlergyType.ErrorText = "";
                textEditEmergencyPhone.ErrorText = "";
                textEditEmergencyName.ErrorText = "";
                memoEditBeginDiagnose.ErrorText = "";
                memoEditLifeStore.ErrorText = "";
            }

        #endregion

        #region Формын эвэнтүүд.

            private void gridLookUpEditOrder_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditOrder.EditValue != null && gridLookUpEditOrder.EditValue != DBNull.Value && !gridLookUpEditOrder.EditValue.ToString().Equals(""))
                {
                    DataRowView view = (DataRowView)gridLookUpEditOrder.GetSelectedDataRow();
                    if (view == null)
                        return;

                    //Хүлээлтийн жагсаалтаас ирэх мэдээллүүд
                    textEditLastName.EditValue = view["lastName"];
                    textEditFirstName.EditValue = view["firstName"];
                    dateEditBirthdate.EditValue = view["birthday"];
                    //dateEditBirthdate.Text = DateTime.Parse(view["birthday"].ToString()).ToString("yyyy-MM-dd");
                    textEditRegister.EditValue = view["userID"];
                    textEditEmd.EditValue = view["emdNumber"];
                    radioGroupGender.EditValue = view["gender"];
                    radioGroupIsMarried.EditValue = view["isMarried"];

                    textEditOrganization.EditValue = view["organizationName"];
                    textEditPosition.EditValue = view["positionName"];
                    textEditSpecialty.EditValue = view["specialtyName"];
                    textEditProvince.EditValue = view["provinceName"];
                    if (view["education"] != DBNull.Value && view["education"] != null)
                        textEditEducation.EditValue = InformationDataSet.getEducationName(Convert.ToInt32(view["education"]));
                    else
                        textEditEducation.EditValue = "";
                    memoEditAddress.EditValue = view["address"];

                    radioGroupLoopState.EditValue = true;
                    checkEditIsUnder.EditValue = false;
                }
            }

            private void treeListLookUpEditWard_EditValueChanged(object sender, EventArgs e)
            {
                if (Visible && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && !treeListLookUpEditWard.EditValue.ToString().Equals("") && 
                    (actionType == 1 || actionType == 3) && StoreForm.Instance.currentStoreView != null)
                {
                    DataRow[] rows = StoreDataSet.Instance.StoreWardRoom.Select("hospitalID = '"+HospitalUtil.getHospitalId()+"' AND storeID = '"+StoreForm.Instance.currentStoreView["id"]+"'");
                    if(rows == null || rows.Length == 0)
                        addWardRoom();
                    else if (rows.Length == 1)
                    {
                        currPatientInformView["wardID"] = treeListLookUpEditWard.EditValue;
                        currPatientInformView["wardName"] = treeListLookUpEditWard.Text;
                        currPatientInformView.EndEdit();
                    }
                }
            }

            private void gridControlStoreDiagnose_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {   
                        addDiagnose();
                    }
                    else if (String.Equals(e.Button.Tag, "edit") && gridViewStoreDiagnose.GetFocusedRow() != null)
                    {
                        editDiagnose();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewStoreDiagnose.GetFocusedRow() != null)
                    {
                        deleteDiagnose();
                    }
                }
            }

            private void gridControlStoreInspection_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addInspection();
                    }
                    else if (String.Equals(e.Button.Tag, "edit") && gridViewStoreInspection.GetFocusedRow() != null)
                    {
                        editInspection();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewStoreInspection.GetFocusedRow() != null)
                    {
                        deleteInspection();
                    }
                }
            }

            private void gridControlPatientInform_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addPatientinform();
                    }
                    else if (String.Equals(e.Button.Tag, "edit") && gridViewWardRoom.GetFocusedRow() != null && Convert.ToBoolean(gridViewWardRoom.GetFocusedDataRow()["isCur"]))
                    {
                        updatePatientInform();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewWardRoom.GetFocusedRow() != null && Convert.ToBoolean(gridViewWardRoom.GetFocusedDataRow()["isCur"]))
                    {
                        deletePatientInform();
                    }
                }
            }

            private void gridControlValidity_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addValidity();
                    }
                    else if (String.Equals(e.Button.Tag, "edit") && gridViewValidity.GetFocusedRow() != null)
                    {
                        editValidity();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewValidity.GetFocusedRow() != null)
                    {
                        deleteValidity();
                    }
                }
            }

            private void gridControlStoreLeftDiagnose_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addLeftDiagnose();
                    }
                    else if (String.Equals(e.Button.Tag, "edit") && gridViewStoreLeftDiagnose.GetFocusedRow() != null)
                    {
                        editLeftDiagnose();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewStoreLeftDiagnose.GetFocusedRow() != null)
                    {
                        deleteLeftDiagnose();
                    }
                }
            }
            
            private void dateEditEndDate_EditValueChanged(object sender, EventArgs e)
            {
                changeDate();
            }

            // Гарах үед.
            private void checkEditIsClose_CheckedChanged(object sender, EventArgs e)
            {
                if (checkEditIsClose.Checked && StoreDataSet.Instance.StoreLeft.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND storeID = '" + StoreForm.Instance.currentStoreView["id"] + "'").Length == 0)
                {
                    StoreDataSet.Instance.StoreLeft.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                    {
                        StoreDataSet.Instance.StoreLeft.userIDColumn.DefaultValue = UserUtil.getUserId();
                    }
                    else
                    {
                        StoreDataSet.Instance.StoreLeft.userIDColumn.DefaultValue = "";
                    }
                    StoreDataSet.Instance.StoreLeft.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                    StoreDataSet.Instance.StoreLeft.endDateColumn.DefaultValue = DateTime.Now;
                    currLeftStoreView = (DataRowView)StoreDataSet.StoreLeftBindingSource.AddNew();
                    currLeftStoreView.EndEdit();
                }
                else
                {
                    clearErrorTextStoreLeft();
                    currLeftStoreView = (DataRowView)StoreDataSet.StoreLeftBindingSource.Current;
                }
                checkApply(checkEditIsClose.Checked);
            }

            //Эмчлүүлэгчийн Анамнез
            private void checkEditStore_CheckedChanged(object sender, EventArgs e)
            {
                if (checkEditStore.Checked && StoreDataSet.Instance.StoreHistory.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND storeID = '" + StoreForm.Instance.currentStoreView["id"] + "'").Length == 0)
                {
                    StoreDataSet.Instance.StoreHistory.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    StoreDataSet.Instance.StoreHistory.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                    currAnamnezView = (DataRowView)StoreDataSet.StoreHistoryBindingSource.AddNew();
                }
                else
                {
                    currAnamnezView = (DataRowView)StoreDataSet.StoreHistoryBindingSource.Current;
                }
                checkStore(checkEditStore.Checked);
            }

            //Ерөнхий үзлэг
            private void checkEditGeneral_CheckedChanged(object sender, EventArgs e)
            {
                if (checkEditGeneral.Checked && StoreDataSet.Instance.StoreGeneralinspection.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND storeID = '" + StoreForm.Instance.currentStoreView["id"] + "'").Length == 0)
                {
                    StoreDataSet.Instance.StoreGeneralinspection.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    StoreDataSet.Instance.StoreGeneralinspection.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                    currGeneralView = (DataRowView)StoreDataSet.StoreGeneralInspectionBindingSource.AddNew();
                }
                else
                {
                    currGeneralView = (DataRowView)StoreDataSet.StoreGeneralInspectionBindingSource.Current;
                }
                checkGeneral(checkEditGeneral.Checked);
            }

            //Дотрын эмчийн үзлэг
            private void checkEditInternal_CheckedChanged(object sender, EventArgs e)
            {
                if (checkEditInternal.Checked && StoreDataSet.Instance.StoreInternalinspection.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND storeID = '" + StoreForm.Instance.currentStoreView["id"] + "'").Length == 0)
                {
                    StoreDataSet.Instance.StoreInternalinspection.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    StoreDataSet.Instance.StoreInternalinspection.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                    currInternalView = (DataRowView)StoreDataSet.StoreInternalinspectionBindingSource.AddNew();
                }
                else
                {
                    currInternalView = (DataRowView)StoreDataSet.StoreInternalinspectionBindingSource.Current;
                }
                checkInternal(checkEditInternal.Checked);
            }

            //Зүрх судасны эрсдэлт хүчин зүйлс
            private void checkEditHeart_CheckedChanged(object sender, EventArgs e)
            {
                if (checkEditHeart.Checked && StoreDataSet.Instance.StoreHeart.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND storeID = '" + StoreForm.Instance.currentStoreView["id"] + "'").Length == 0)
                {
                    StoreDataSet.Instance.StoreHeart.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    StoreDataSet.Instance.StoreHeart.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                    currHeartView = (DataRowView)StoreDataSet.StoreHeartBindingSource.AddNew();
                }
                else
                {
                    currHeartView = (DataRowView)StoreDataSet.StoreHeartBindingSource.Current;
                }
                checkHeart(checkEditHeart.Checked);
            }

            //Хоол боловсруулах эрхтэн тогтолцоо
            private void checkEditDigestive_CheckedChanged(object sender, EventArgs e)
            {
                if (checkEditDigestive.Checked && StoreDataSet.Instance.StoreDigestive.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND storeID = '" + StoreForm.Instance.currentStoreView["id"] + "'").Length == 0)
                {
                    StoreDataSet.Instance.StoreDigestive.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    StoreDataSet.Instance.StoreDigestive.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                    currDigestiveView = (DataRowView)StoreDataSet.StoreDigestiveBindingSource.AddNew();
                }
                else
                {
                    currDigestiveView = (DataRowView)StoreDataSet.StoreDigestiveBindingSource.Current;
                }
                checkDigestive(checkEditDigestive.Checked);
            }

            //Шээс бэлгийн тогтолцоо
            private void checkEditUrine_CheckedChanged(object sender, EventArgs e)
            {
                if (checkEditUrine.Checked && StoreDataSet.Instance.StoreUrine.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND storeID = '" + StoreForm.Instance.currentStoreView["id"] + "'").Length == 0)
                {
                    StoreDataSet.Instance.StoreUrine.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    StoreDataSet.Instance.StoreUrine.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                    currUrineView = (DataRowView)StoreDataSet.StoreUrineBindingSource.AddNew();
                }
                else
                {
                    currUrineView = (DataRowView)StoreDataSet.StoreUrineBindingSource.Current;
                }
                checkUrine(checkEditUrine.Checked);
            }

            private void checkEditIsSurgery_CheckedChanged(object sender, EventArgs e)
            {
                memoEditSurgery.Properties.ReadOnly = !checkEditIsSurgery.Checked;
            }

            private void checkEditIsAfterSurgery_CheckedChanged(object sender, EventArgs e)
            {
                memoEditAfterSurgery.Properties.ReadOnly = !checkEditIsAfterSurgery.Checked;
            }

            private void gridLookUpEditTreat_EditValueChanged(object sender, EventArgs e)
            {
                if (gridLookUpEditTreat.Text.Equals("Бусад"))
                {
                    memoEditOtherTreat.Properties.ReadOnly = false;
                    checkEditBlood.Properties.ReadOnly = false;
                }
                else
                {
                    memoEditOtherTreat.Properties.ReadOnly = true;
                    checkEditBlood.Properties.ReadOnly = true;
                }
            }


            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonSaveStore_Click(object sender, EventArgs e)
            {
                saveStore();
            }

        #endregion

        #region Онош нэмж, хасах, засах функцууд.

            private void addDiagnose()
            {
                StoreDataSet.Instance.StoreDiagnose.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                StoreDataSet.Instance.StoreDiagnose.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                currentDiagnoseView = (DataRowView)StoreDataSet.StoreDiagnoseBindingSource.AddNew();

                DiagnoseInUpForm.Instance.showForm(1);
            }

            private void editDiagnose()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.NURSE_ID)
                {
                    return;
                }
                else if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID && !gridViewStoreDiagnose.GetFocusedDataRow()["userID"].ToString().Equals(UserUtil.getUserId()))
                {
                    return;
                }
                currentDiagnoseView = (DataRowView)StoreDataSet.StoreDiagnoseBindingSource.Current;
                DiagnoseInUpForm.Instance.showForm(2);
            }

            private void deleteDiagnose()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.NURSE_ID)
                {
                    return;
                }
                else if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID && !gridViewStoreDiagnose.GetFocusedDataRow()["userID"].ToString().Equals(UserUtil.getUserId()))
                {
                    return;
                }

                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Оношийг устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    gridViewStoreDiagnose.DeleteSelectedRows();
                    StoreDataSet.StoreDiagnoseBindingSource.EndEdit();
                }
            }

        #endregion

        #region Гарах үеийн онош нэмж, хасах, засах функцууд.

            private void addLeftDiagnose()
            {
                StoreDataSet.Instance.StoreLeftDiagnose.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                StoreDataSet.Instance.StoreLeftDiagnose.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                currLeftDiagnoseView = (DataRowView)StoreDataSet.StoreLeftDiagnoseBindingSource.AddNew();

                LeftDiagnoseInUpForm.Instance.showForm(1);
            }

            private void editLeftDiagnose()
            {
                currLeftDiagnoseView = (DataRowView)StoreDataSet.StoreLeftDiagnoseBindingSource.Current;
                LeftDiagnoseInUpForm.Instance.showForm(2);
            }

            private void deleteLeftDiagnose()
            {

                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Оношийг устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    gridViewStoreLeftDiagnose.DeleteSelectedRows();
                    StoreDataSet.StoreLeftDiagnoseBindingSource.EndEdit();
                }
            }

        #endregion

        #region Клиникийн үндэслэл нэмж, хасах, засах функцууд.

            private void addValidity()
            {
                StoreDataSet.Instance.StoreValidity.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                StoreDataSet.Instance.StoreValidity.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                currValidityView = (DataRowView)StoreDataSet.StoreValidityBindingSource.AddNew();

                ValidityInUpForm.Instance.showForm(1);
            }

            private void editValidity()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID && !gridViewStoreInspection.GetFocusedDataRow()["userID"].ToString().Equals(UserUtil.getUserId()))
                {
                    return;
                }
                currValidityView = (DataRowView)StoreDataSet.StoreValidityBindingSource.Current;
                ValidityInUpForm.Instance.showForm(2);
            }

            private void deleteValidity()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID && !gridViewStoreInspection.GetFocusedDataRow()["userID"].ToString().Equals(UserUtil.getUserId()))
                {
                    return;
                }
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Килиникийн үндэслэл устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    gridViewValidity.DeleteSelectedRows();
                    StoreDataSet.StoreValidityBindingSource.EndEdit();
                }
            }

        #endregion

        #region Үзлэг нэмж, хасах, засах функцууд.

            private void addInspection()
            {
                StoreDataSet.Instance.StoreInspection.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                StoreDataSet.Instance.StoreInspection.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                currentInspectionView = (DataRowView)StoreDataSet.StoreInspectionBindingSource.AddNew();
                InspectionInUpForm.Instance.showForm(1);
            }

            private void editInspection()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID && !gridViewStoreInspection.GetFocusedDataRow()["userID"].ToString().Equals(UserUtil.getUserId()))
                {
                    return;
                }
                currentInspectionView = (DataRowView)StoreDataSet.StoreInspectionBindingSource.Current;
                InspectionInUpForm.Instance.showForm(2);
            }

            private void deleteInspection()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID && !gridViewStoreInspection.GetFocusedDataRow()["userID"].ToString().Equals(UserUtil.getUserId()))
                {
                    return;
                }
                
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Үзлэгийг устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    gridViewStoreInspection.DeleteSelectedRows();
                    StoreDataSet.StoreInspectionBindingSource.EndEdit();
                }
            }

        #endregion

        #region Тасаг ор нэмэх, засах, устгах функцууд.

            private void addWardRoom()
            {
                StoreDataSet.Instance.StoreWardRoom.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                StoreDataSet.Instance.StoreWardRoom.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                StoreDataSet.Instance.StoreWardRoom.isCurColumn.DefaultValue = true;
                currPatientInformView = (DataRowView)StoreDataSet.StoreWardRoomBindingSource.AddNew();
                currPatientInformView["startDate"] = dateEditBeginDate.Text;
                currPatientInformView["wardID"] = treeListLookUpEditWard.EditValue;
                currPatientInformView["wardName"] = treeListLookUpEditWard.Text;
                StoreForm.Instance.currentStoreView["patientID"] = textEditRegister.EditValue;
                currPatientInformView.EndEdit();
            }

            private void addPatientinform()
            {
                if (textEditNumber.Text == "")
                {
                    textEditNumber.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                    textEditNumber.ErrorText = "Дугаар оруулна уу!";
                    return;
                }

                StoreDataSet.Instance.StoreWardRoom.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                StoreDataSet.Instance.StoreWardRoom.isCurColumn.DefaultValue = true;
                StoreDataSet.Instance.StoreWardRoom.startDateColumn.DefaultValue = DateTime.Now;
                StoreDataSet.Instance.StoreWardRoom.wardIDColumn.DefaultValue = treeListLookUpEditWard.EditValue;

                currPatientInformView = (DataRowView)StoreDataSet.StoreWardRoomBindingSource.AddNew();
                WardRoomInUpForm.Instance.showForm(1);

                if (StoreDataSet.StoreWardRoomBindingSource.Current != null)
                {
                    currPatientInformView = (DataRowView)StoreDataSet.StoreWardRoomBindingSource.Current;
                    currPatientInformView["isCur"] = 0;
                    currPatientInformView.EndEdit();
                }
            }

            private void deletePatientInform()
            {
                DataRow[] rows = StoreDataSet.Instance.StoreWardRoom.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND storeID = '" + StoreForm.Instance.currentStoreView["id"] + "'");
                if (rows.Length == 1)
                {
                    XtraMessageBox.Show("Эхний тасгийг устгаж болохгүй!");
                    return;
                }
                else
                {
                    DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмчлүүлэгчийн тасаг, орыг устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {                  
                        gridViewWardRoom.DeleteSelectedRows();
                        StoreDataSet.StoreWardRoomBindingSource.EndEdit();
                        if (StoreDataSet.StoreWardRoomBindingSource.Current != null)
                        {
                            currPatientInformView = (DataRowView)StoreDataSet.StoreWardRoomBindingSource.Current;
                            currPatientInformView["isCur"] = 1;
                            treeListLookUpEditWard.EditValue = currPatientInformView["wardID"];
                            StoreDataSet.StoreBindingSource.EndEdit();
                            currPatientInformView.EndEdit();
                        }
                    }                                      
                }
            }

            private void updatePatientInform()
            {
                currPatientInformView = (DataRowView)StoreDataSet.StoreWardRoomBindingSource.Current;
                WardRoomInUpForm.Instance.showForm(2);        
            }
       
        #endregion                   
       
        #region Түүх нээх, хаах, засах, устгах функцууд.

            private void reload()
            {
                UserDataSet.WardTableAdapter.Fill(UserDataSet.Instance.Ward, HospitalUtil.getHospitalId());
                treeListLookUpEditWard.Properties.TreeList.ExpandAll();
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.DOCTOR);
                setAdditional();
            }

            private void changeDate()
            {
                if (dateEditBeginDate.EditValue != DBNull.Value && dateEditBeginDate.EditValue != null &&
                    !dateEditBeginDate.EditValue.ToString().Equals(""))
                {
                    spinEditDuration.EditValue = (dateEditEndDate.DateTime - dateEditBeginDate.DateTime).Days;                   
                }
            }
            
            private bool checkStore()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (gridLookUpEditOrder.EditValue == DBNull.Value || gridLookUpEditOrder.EditValue == null || gridLookUpEditOrder.EditValue.ToString().Equals(""))
                {
                    errorText = "Хүлээлгийн жагсаалтаас сонгоно уу!";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditOrder.ErrorText = errorText;
                    //if (isRight)
                    //{
                    isRight = false;
                    gridLookUpEditOrder.Focus();
                    //}
                }
                if (textEditNumber.EditValue == DBNull.Value || textEditNumber.EditValue == null || (textEditNumber.EditValue = textEditNumber.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Эмчлүүлэгчийн түүхийн дугаарыг оруулна уу!";    
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditNumber.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        textEditNumber.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;         
                    }
                }
                if (dateEditBeginDate.EditValue == DBNull.Value || dateEditBeginDate.EditValue == null || dateEditBeginDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Нээсэн огноог оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditBeginDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditBeginDate.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.EditValue.ToString().Equals(""))
                {
                    errorText = "Тасгийг сонгоно уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (gridLookUpEditPaymentType.EditValue == DBNull.Value || gridLookUpEditPaymentType.EditValue == null || gridLookUpEditPaymentType.EditValue.ToString().Equals(""))
                {
                    errorText = "Төлбөрийн төрлийг сонгоно уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditPaymentType.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditPaymentType.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (gridLookUpEditAlergyType.EditValue == DBNull.Value || gridLookUpEditAlergyType.EditValue == null || gridLookUpEditAlergyType.EditValue.ToString().Equals(""))
                {
                    errorText = "Харшлын анамнезыг сонгоно уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditAlergyType.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditAlergyType.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (textEditEmergencyName.EditValue == DBNull.Value || textEditEmergencyName.EditValue == null || (textEditEmergencyName.EditValue = textEditEmergencyName.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Яаралтай үед холбоо барих хүний нэрийг оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditEmergencyName.ErrorText = errorText;
                    if (isRight)
                    {
                        xtraTabControl.SelectedTabPage = xtraTabPage1;
                        isRight = false;
                        textEditEmergencyName.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (textEditEmergencyPhone.EditValue == DBNull.Value || textEditEmergencyPhone.EditValue == null || (textEditEmergencyPhone.EditValue = textEditEmergencyPhone.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Яаралтай үед холбоо барих хүний утасны дугаарыг оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    textEditEmergencyPhone.ErrorText = errorText;
                    if (isRight)
                    {
                        xtraTabControl.SelectedTabPage = xtraTabPage1;
                        isRight = false;
                        textEditEmergencyPhone.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (memoEditBeginDiagnose.EditValue == DBNull.Value || memoEditBeginDiagnose.EditValue == null || (memoEditBeginDiagnose.EditValue = memoEditBeginDiagnose.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Хэвтэх үеийн оношийг оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    memoEditBeginDiagnose.ErrorText = errorText;
                    if (isRight)
                    {
                        xtraTabControl.SelectedTabPage = xtraTabPage1;
                        isRight = false;
                        memoEditBeginDiagnose.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }        

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
                
            }

            // Гарах үед шалгах функц.
            private bool checkLeftHospital()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (gridLookUpEditDiagnose.EditValue == DBNull.Value || gridLookUpEditDiagnose.EditValue == null || gridLookUpEditDiagnose.EditValue.ToString().Equals(""))
                {
                    errorText = "Оношийн ангилалаас сонгоно уу!";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditDiagnose.ErrorText = errorText;
                    xtraTabControl.SelectedTabPage = xtraTabPage5;
                    isRight = false;
                    gridLookUpEditDiagnose.Focus();
                    //if (isRight)
                    //{                 
                    //}                  
                }
                if (spinEditBill.Value == 0)
                {
                    errorText = "Эмчилгээний зардалыг оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditBill.ErrorText = errorText;
                    if (isRight)
                    {
                        xtraTabControl.SelectedTabPage = xtraTabPage5;
                        isRight = false;
                        spinEditBill.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }

                }
                if (dateEditEndDate.EditValue == DBNull.Value || dateEditEndDate.EditValue == null || dateEditEndDate.Text.Equals(""))
                {
                    errorText = "Гарсан огноо оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEndDate.ErrorText = errorText;
                    if (isRight)
                    {
                        xtraTabControl.SelectedTabPage = xtraTabPage5;
                        isRight = false;
                        dateEditEndDate.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }

                }
                if (memoEditBillOfMedic.EditValue == DBNull.Value || memoEditBillOfMedic.EditValue == null || (memoEditBillOfMedic.EditValue = memoEditBillOfMedic.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Нийт гарсан эмийн зардалыг оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    memoEditBillOfMedic.ErrorText = errorText;
                    if (isRight)
                    {
                        xtraTabControl.SelectedTabPage = xtraTabPage5;
                        isRight = false;
                        memoEditBillOfMedic.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }

                }
                if (gridLookUpEditDoctor.EditValue == DBNull.Value || gridLookUpEditDoctor.EditValue == null || (gridLookUpEditDoctor.EditValue = gridLookUpEditDoctor.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Эмчийг оруулна уу!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditDoctor.ErrorText = errorText;
                    if (isRight)
                    {
                        xtraTabControl.SelectedTabPage = xtraTabPage5;
                        isRight = false;
                        gridLookUpEditDoctor.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }

                }

                if (dateEditBeginDate.DateTime > dateEditEndDate.DateTime)
                {
                    errorText = "Дуусах огноо эхлэх огнооноос өмнө байж болохгүй!";
                    dateEditEndDate.ErrorText = errorText;
                    if (isRight)
                    {
                        xtraTabControl.SelectedTabPage = xtraTabPage5;
                        isRight = false;
                        dateEditEndDate.Focus();
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }
        
            private void saveStore()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.RECEPTION_ID || (UserUtil.getUserRoleId() != RoleUtil.NURSE_ID && UserUtil.getUserRoleId() != RoleUtil.HEAD_NURSE_ID && UserUtil.getUserRoleId() != RoleUtil.DOCTOR_ID))
                {
                    if (!checkStore())
                        return;

                    if (checkEditIsClose.Checked && !checkLeftHospital())
                        return;                  


                    DataRowView view = (DataRowView)gridLookUpEditOrder.GetSelectedDataRow();
                    StoreForm.Instance.currentStoreView["orderID"] = gridLookUpEditOrder.EditValue;
                    StoreForm.Instance.currentStoreView["patientID"] = textEditRegister.EditValue;
                    StoreForm.Instance.currentStoreView["emdNumber"] = textEditEmd.Text;
                    StoreForm.Instance.currentStoreView["lastName"] = textEditLastName.Text;
                    StoreForm.Instance.currentStoreView["firstName"] = textEditFirstName.Text;

                    if (dateEditEndDate.EditValue != DBNull.Value && dateEditEndDate.EditValue != null)
                        StoreForm.Instance.currentStoreView["endDate"] = dateEditEndDate.EditValue;
                    else
                        dateEditEndDate.EditValue = null;

                    StoreForm.Instance.currentStoreView["birthday"] = dateEditBirthdate.EditValue;
                    StoreForm.Instance.currentStoreView["address"] = memoEditAddress.Text;
                    StoreForm.Instance.currentStoreView["gender"] = radioGroupGender.EditValue;
                    StoreForm.Instance.currentStoreView["isMarried"] = radioGroupIsMarried.EditValue;

                    StoreForm.Instance.currentStoreView["wardName"] = treeListLookUpEditWard.Text;
                    StoreForm.Instance.currentStoreView["provinceName"] = textEditProvince.Text;
                    StoreForm.Instance.currentStoreView["education"] = view["education"];
                    StoreForm.Instance.currentStoreView["organizationName"] = textEditOrganization.Text;
                    StoreForm.Instance.currentStoreView["positionName"] = textEditPosition.Text;
                    StoreForm.Instance.currentStoreView["specialtyName"] = textEditSpecialty.Text;
                    StoreForm.Instance.currentStoreView["isClose"] = checkEditIsClose.Checked;
                }
                
                //Эмчийн эрх
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    StoreForm.Instance.currentStoreView["isClose"] = checkEditIsClose.Checked;
                    if (dateEditEndDate.EditValue != DBNull.Value && dateEditEndDate.EditValue != null)
                        StoreForm.Instance.currentStoreView["endDate"] = dateEditEndDate.EditValue;
                    else
                        dateEditEndDate.EditValue = null;


                    StoreForm.Instance.currentStoreView.EndEdit();
                    StoreDataSet.StoreTableAdapter.Update(StoreDataSet.Instance.Store);
                    

                    StoreDataSet.StoreDiagnoseBindingSource.EndEdit();
                    StoreDataSet.StoreDiagnoseTableAdapter.Update(StoreDataSet.Instance.StoreDiagnose);

                    StoreDataSet.StoreInspectionBindingSource.EndEdit();
                    StoreDataSet.StoreInspectionTableAdapter.Update(StoreDataSet.Instance.StoreInspection);

                    StoreDataSet.StoreTreatmentBindingSource.EndEdit();
                    StoreDataSet.StoreTreatmentTableAdapter.Update(StoreDataSet.Instance.StoreTreatment);

                    StoreDataSet.StoreLeftDiagnoseBindingSource.EndEdit();
                    StoreDataSet.StoreLeftDiagnoseTableAdapter.Update(StoreDataSet.Instance.StoreLeftDiagnose);

                    if (currLeftStoreView != null)
                    {
                        currLeftStoreView.EndEdit();
                        StoreDataSet.StoreLeftBindingSource.EndEdit();
                        StoreDataSet.StoreLeftTableAdapter.Update(StoreDataSet.Instance.StoreLeft);
                    }
                    else
                    {
                        StoreDataSet.StoreLeftBindingSource.EndEdit();
                        StoreDataSet.StoreLeftTableAdapter.Update(StoreDataSet.Instance.StoreLeft);
                    }
                        

                    if (currValidityView != null)
                    {
                        currValidityView.EndEdit();
                        StoreDataSet.StoreValidityBindingSource.EndEdit();
                        StoreDataSet.StoreValidityTableAdapter.Update(StoreDataSet.Instance.StoreValidity);
                    }
                    else
                    {
                        StoreDataSet.StoreValidityBindingSource.EndEdit();
                        StoreDataSet.StoreValidityTableAdapter.Update(StoreDataSet.Instance.StoreValidity);
                    }
                        

                    if (currAnamnezView != null)
                    {
                        currAnamnezView.EndEdit();
                        StoreDataSet.StoreHistoryBindingSource.EndEdit();
                        StoreDataSet.StoreHistoryTableAdapter.Update(StoreDataSet.Instance.StoreHistory);
                    }
                    else
                    {
                        StoreDataSet.StoreHistoryBindingSource.EndEdit();
                        StoreDataSet.StoreHistoryTableAdapter.Update(StoreDataSet.Instance.StoreHistory);
                    }
                        

                    if (currGeneralView != null)
                    {
                        currGeneralView.EndEdit();
                        StoreDataSet.StoreGeneralInspectionBindingSource.EndEdit();
                        StoreDataSet.StoreGeneralinspectionTableAdapter.Update(StoreDataSet.Instance.StoreGeneralinspection);
                    }
                    else
                    {
                        StoreDataSet.StoreGeneralInspectionBindingSource.EndEdit();
                        StoreDataSet.StoreGeneralinspectionTableAdapter.Update(StoreDataSet.Instance.StoreGeneralinspection);
                    }
                        

                    if (currInternalView != null)
                    {
                        currInternalView.EndEdit();
                        StoreDataSet.StoreInternalinspectionBindingSource.EndEdit();
                        StoreDataSet.StoreInternalinspectionTableAdapter.Update(StoreDataSet.Instance.StoreInternalinspection);
                    }
                    else
                    {
                        StoreDataSet.StoreInternalinspectionBindingSource.EndEdit();
                        StoreDataSet.StoreInternalinspectionTableAdapter.Update(StoreDataSet.Instance.StoreInternalinspection);
                    }

                    if (currHeartView != null)
                    {
                        currHeartView.EndEdit();
                        StoreDataSet.StoreHeartBindingSource.EndEdit();
                        StoreDataSet.StoreHeartTableAdapter.Update(StoreDataSet.Instance.StoreHeart);
                    }
                    else
                    {
                        StoreDataSet.StoreHeartBindingSource.EndEdit();
                        StoreDataSet.StoreHeartTableAdapter.Update(StoreDataSet.Instance.StoreHeart);
                    }


                    if (currDigestiveView != null)
                    {
                        currDigestiveView.EndEdit();
                        StoreDataSet.StoreDigestiveBindingSource.EndEdit();
                        StoreDataSet.StoreDigestiveTableAdapter.Update(StoreDataSet.Instance.StoreDigestive);
                    }
                    else
                    {
                        StoreDataSet.StoreDigestiveBindingSource.EndEdit();
                        StoreDataSet.StoreDigestiveTableAdapter.Update(StoreDataSet.Instance.StoreDigestive);
                    }


                    if (currUrineView != null)
                    {
                        currUrineView.EndEdit();
                        StoreDataSet.StoreUrineBindingSource.EndEdit();
                        StoreDataSet.StoreUrineTableAdapter.Update(StoreDataSet.Instance.StoreUrine);
                    }
                    else
                    {
                        StoreDataSet.StoreUrineBindingSource.EndEdit();
                        StoreDataSet.StoreUrineTableAdapter.Update(StoreDataSet.Instance.StoreUrine);
                    }
                        
                }
                //Сувилагчийн эрх
                else if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID)
                {
                    StoreDataSet.StoreInspectionBindingSource.EndEdit();
                    StoreDataSet.StoreInspectionTableAdapter.Update(StoreDataSet.Instance.StoreInspection);

                    StoreDataSet.StoreTreatmentBindingSource.EndEdit();
                    StoreDataSet.StoreTreatmentTableAdapter.Update(StoreDataSet.Instance.StoreTreatment);
                }
                //Ахлах сувилагчийн эрх
                else if (UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                {
                    StoreForm.Instance.currentStoreView.EndEdit();
                    StoreDataSet.StoreBindingSource.EndEdit();           
                    StoreDataSet.StoreTableAdapter.Update(StoreDataSet.Instance.Store);

                    StoreDataSet.StoreWardRoomBindingSource.EndEdit();
                    StoreDataSet.StoreWardRoomTableAdapter.Update(StoreDataSet.Instance.StoreWardRoom);
                        
                    StoreDataSet.StoreInspectionBindingSource.EndEdit();
                    StoreDataSet.StoreInspectionTableAdapter.Update(StoreDataSet.Instance.StoreInspection);

                    StoreDataSet.StoreTreatmentBindingSource.EndEdit();
                    StoreDataSet.StoreTreatmentTableAdapter.Update(StoreDataSet.Instance.StoreTreatment);
                }
                // Хүлээн авхын Эрх
                else if (UserUtil.getUserRoleId() == RoleUtil.RECEPTION_ID)
                {
                    StoreForm.Instance.currentStoreView.EndEdit();
                    StoreDataSet.StoreBindingSource.EndEdit();
                    StoreDataSet.StoreTableAdapter.Update(StoreDataSet.Instance.Store);

                    if (currValidityView != null)
                    {
                        currValidityView.EndEdit();
                        StoreDataSet.StoreValidityBindingSource.EndEdit();
                        StoreDataSet.StoreValidityTableAdapter.Update(StoreDataSet.Instance.StoreValidity);
                    }
                    else
                    {
                        StoreDataSet.StoreValidityBindingSource.EndEdit();
                        StoreDataSet.StoreValidityTableAdapter.Update(StoreDataSet.Instance.StoreValidity);
                    }

                    StoreDataSet.StoreWardRoomBindingSource.EndEdit();
                    StoreDataSet.StoreWardRoomTableAdapter.Update(StoreDataSet.Instance.StoreWardRoom);

                    if (currAnamnezView != null)
                    {
                        currAnamnezView.EndEdit();
                        StoreDataSet.StoreHistoryBindingSource.EndEdit();
                        StoreDataSet.StoreHistoryTableAdapter.Update(StoreDataSet.Instance.StoreHistory);
                    }
                    else
                    {
                        StoreDataSet.StoreHistoryBindingSource.EndEdit();
                        StoreDataSet.StoreHistoryTableAdapter.Update(StoreDataSet.Instance.StoreHistory);
                    }


                    if (currGeneralView != null)
                    {
                        currGeneralView.EndEdit();
                        StoreDataSet.StoreGeneralInspectionBindingSource.EndEdit();
                        StoreDataSet.StoreGeneralinspectionTableAdapter.Update(StoreDataSet.Instance.StoreGeneralinspection);
                    }
                    else
                    {
                        StoreDataSet.StoreGeneralInspectionBindingSource.EndEdit();
                        StoreDataSet.StoreGeneralinspectionTableAdapter.Update(StoreDataSet.Instance.StoreGeneralinspection);
                    }
                        

                    if (currInternalView != null)
                    {
                        currInternalView.EndEdit();
                        StoreDataSet.StoreInternalinspectionBindingSource.EndEdit();
                        StoreDataSet.StoreInternalinspectionTableAdapter.Update(StoreDataSet.Instance.StoreInternalinspection);
                    }
                    else
                    {
                        StoreDataSet.StoreInternalinspectionBindingSource.EndEdit();
                        StoreDataSet.StoreInternalinspectionTableAdapter.Update(StoreDataSet.Instance.StoreInternalinspection);
                    }

                    if (currHeartView != null)
                    {
                        currHeartView.EndEdit();
                        StoreDataSet.StoreHeartBindingSource.EndEdit();
                        StoreDataSet.StoreHeartTableAdapter.Update(StoreDataSet.Instance.StoreHeart);
                    }
                    else
                    {
                        StoreDataSet.StoreHeartBindingSource.EndEdit();
                        StoreDataSet.StoreHeartTableAdapter.Update(StoreDataSet.Instance.StoreHeart);
                    }


                    if (currDigestiveView != null)
                    {
                        currDigestiveView.EndEdit();
                        StoreDataSet.StoreDigestiveBindingSource.EndEdit();
                        StoreDataSet.StoreDigestiveTableAdapter.Update(StoreDataSet.Instance.StoreDigestive);
                    }
                    else
                    {
                        StoreDataSet.StoreDigestiveBindingSource.EndEdit();
                        StoreDataSet.StoreDigestiveTableAdapter.Update(StoreDataSet.Instance.StoreDigestive);
                    }


                    if (currUrineView != null)
                    {
                        currUrineView.EndEdit();
                        StoreDataSet.StoreUrineBindingSource.EndEdit();
                        StoreDataSet.StoreUrineTableAdapter.Update(StoreDataSet.Instance.StoreUrine);
                    }
                    else
                    {
                        StoreDataSet.StoreUrineBindingSource.EndEdit();
                        StoreDataSet.StoreUrineTableAdapter.Update(StoreDataSet.Instance.StoreUrine);
                    }

                    StoreDataSet.StoreLeftDiagnoseBindingSource.EndEdit();
                    StoreDataSet.StoreLeftDiagnoseTableAdapter.Update(StoreDataSet.Instance.StoreLeftDiagnose);
                        
                }
                else
                {
                    StoreForm.Instance.currentStoreView.EndEdit();
                    StoreDataSet.StoreBindingSource.EndEdit();
                    StoreDataSet.StoreTableAdapter.Update(StoreDataSet.Instance.Store);


                    StoreDataSet.StoreWardRoomBindingSource.EndEdit();
                    StoreDataSet.StoreWardRoomTableAdapter.Update(StoreDataSet.Instance.StoreWardRoom);
                        

                    StoreDataSet.StoreDiagnoseBindingSource.EndEdit();
                    StoreDataSet.StoreDiagnoseTableAdapter.Update(StoreDataSet.Instance.StoreDiagnose);

                    StoreDataSet.StoreInspectionBindingSource.EndEdit();
                    StoreDataSet.StoreInspectionTableAdapter.Update(StoreDataSet.Instance.StoreInspection);

                    StoreDataSet.StoreTreatmentBindingSource.EndEdit();
                    StoreDataSet.StoreTreatmentTableAdapter.Update(StoreDataSet.Instance.StoreTreatment);

                    StoreDataSet.StoreLeftDiagnoseBindingSource.EndEdit();
                    StoreDataSet.StoreLeftDiagnoseTableAdapter.Update(StoreDataSet.Instance.StoreLeftDiagnose);

                    if (currLeftStoreView != null)
                    {
                        currLeftStoreView.EndEdit();
                        StoreDataSet.StoreLeftBindingSource.EndEdit();
                        StoreDataSet.StoreLeftTableAdapter.Update(StoreDataSet.Instance.StoreLeft);
                    }
                    else
                    {
                        StoreDataSet.StoreLeftBindingSource.EndEdit();
                        StoreDataSet.StoreLeftTableAdapter.Update(StoreDataSet.Instance.StoreLeft);
                    }
                        

                    if (currValidityView != null)
                    {
                        currValidityView.EndEdit();
                        StoreDataSet.StoreValidityBindingSource.EndEdit();
                        StoreDataSet.StoreValidityTableAdapter.Update(StoreDataSet.Instance.StoreValidity);
                    }
                    else
                    {
                        StoreDataSet.StoreValidityBindingSource.EndEdit();
                        StoreDataSet.StoreValidityTableAdapter.Update(StoreDataSet.Instance.StoreValidity);
                    }
                        

                    if (currAnamnezView != null)
                    {
                        currAnamnezView.EndEdit();
                        StoreDataSet.StoreHistoryBindingSource.EndEdit();
                        StoreDataSet.StoreHistoryTableAdapter.Update(StoreDataSet.Instance.StoreHistory);
                    }
                    else
                    {
                        StoreDataSet.StoreHistoryBindingSource.EndEdit();
                        StoreDataSet.StoreHistoryTableAdapter.Update(StoreDataSet.Instance.StoreHistory);
                    }
                        

                    if (currGeneralView != null)
                    {
                        currGeneralView.EndEdit();
                        StoreDataSet.StoreGeneralInspectionBindingSource.EndEdit();
                        StoreDataSet.StoreGeneralinspectionTableAdapter.Update(StoreDataSet.Instance.StoreGeneralinspection);
                    }
                    else
                    {
                        StoreDataSet.StoreGeneralInspectionBindingSource.EndEdit();
                        StoreDataSet.StoreGeneralinspectionTableAdapter.Update(StoreDataSet.Instance.StoreGeneralinspection);
                    }
                        

                    if (currInternalView != null)
                    {
                        currInternalView.EndEdit();
                        StoreDataSet.StoreInternalinspectionBindingSource.EndEdit();
                        StoreDataSet.StoreInternalinspectionTableAdapter.Update(StoreDataSet.Instance.StoreInternalinspection);
                    }
                    else
                    {
                        StoreDataSet.StoreInternalinspectionBindingSource.EndEdit();
                        StoreDataSet.StoreInternalinspectionTableAdapter.Update(StoreDataSet.Instance.StoreInternalinspection);
                    }

                    if (currHeartView != null)
                    {
                        currHeartView.EndEdit();
                        StoreDataSet.StoreHeartBindingSource.EndEdit();
                        StoreDataSet.StoreHeartTableAdapter.Update(StoreDataSet.Instance.StoreHeart);
                    }
                    else
                    {
                        StoreDataSet.StoreHeartBindingSource.EndEdit();
                        StoreDataSet.StoreHeartTableAdapter.Update(StoreDataSet.Instance.StoreHeart);
                    }
                        

                    if (currDigestiveView != null)
                    {
                        currDigestiveView.EndEdit();
                        StoreDataSet.StoreDigestiveBindingSource.EndEdit();
                        StoreDataSet.StoreDigestiveTableAdapter.Update(StoreDataSet.Instance.StoreDigestive);
                    }
                    else
                    {
                        StoreDataSet.StoreDigestiveBindingSource.EndEdit();
                        StoreDataSet.StoreDigestiveTableAdapter.Update(StoreDataSet.Instance.StoreDigestive);
                    }


                    if (currUrineView != null)
                    {
                        currUrineView.EndEdit();
                        StoreDataSet.StoreUrineBindingSource.EndEdit();
                        StoreDataSet.StoreUrineTableAdapter.Update(StoreDataSet.Instance.StoreUrine);
                    }
                    else
                    {
                        StoreDataSet.StoreUrineBindingSource.EndEdit();
                        StoreDataSet.StoreUrineTableAdapter.Update(StoreDataSet.Instance.StoreUrine);
                    }
                       
                }

                Close();                  
            }

        #endregion                                                                       
                      
    }
}