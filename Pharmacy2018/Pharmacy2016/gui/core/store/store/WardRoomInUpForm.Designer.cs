﻿namespace Pharmacy2016.gui.core.store.store
{
    partial class WardRoomInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WardRoomInUpForm));
            this.treeListLookUpEditWardLine = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnWard = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.treeListLookUpEditRoom = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList2 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumnRoom = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditStartDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditEndDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDesc = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWardLine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditRoom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDesc.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // treeListLookUpEditWardLine
            // 
            this.treeListLookUpEditWardLine.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWardLine.Location = new System.Drawing.Point(167, 29);
            this.treeListLookUpEditWardLine.Name = "treeListLookUpEditWardLine";
            this.treeListLookUpEditWardLine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWardLine.Properties.DisplayMember = "name";
            this.treeListLookUpEditWardLine.Properties.NullText = "";
            this.treeListLookUpEditWardLine.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditWardLine.Properties.ValueMember = "id";
            this.treeListLookUpEditWardLine.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWardLine.TabIndex = 1;
            this.treeListLookUpEditWardLine.EditValueChanged += new System.EventHandler(this.treeListLookUpEditWardLine_EditValueChanged);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnWard});
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(-58, 30);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumnWard
            // 
            this.treeListColumnWard.Caption = "Тасгийн нэрс";
            this.treeListColumnWard.FieldName = "name";
            this.treeListColumnWard.Name = "treeListColumnWard";
            this.treeListColumnWard.Visible = true;
            this.treeListColumnWard.VisibleIndex = 0;
            // 
            // labelControl37
            // 
            this.labelControl37.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl37.Location = new System.Drawing.Point(45, 30);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(116, 13);
            this.labelControl37.TabIndex = 13;
            this.labelControl37.Text = "Эмчлүүлэгчийн тасаг*:";
            // 
            // treeListLookUpEditRoom
            // 
            this.treeListLookUpEditRoom.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditRoom.Location = new System.Drawing.Point(167, 55);
            this.treeListLookUpEditRoom.Name = "treeListLookUpEditRoom";
            this.treeListLookUpEditRoom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditRoom.Properties.DisplayMember = "name";
            this.treeListLookUpEditRoom.Properties.NullText = "";
            this.treeListLookUpEditRoom.Properties.TreeList = this.treeList2;
            this.treeListLookUpEditRoom.Properties.ValueMember = "id";
            this.treeListLookUpEditRoom.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditRoom.TabIndex = 2;
            // 
            // treeList2
            // 
            this.treeList2.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumnRoom});
            this.treeList2.KeyFieldName = "id";
            this.treeList2.Location = new System.Drawing.Point(-8, 80);
            this.treeList2.Name = "treeList2";
            this.treeList2.OptionsBehavior.EnableFiltering = true;
            this.treeList2.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList2.ParentFieldName = "pid";
            this.treeList2.Size = new System.Drawing.Size(357, 200);
            this.treeList2.TabIndex = 0;
            // 
            // treeListColumnRoom
            // 
            this.treeListColumnRoom.Caption = "Өрөө, орны нэрс";
            this.treeListColumnRoom.FieldName = "name";
            this.treeListColumnRoom.Name = "treeListColumnRoom";
            this.treeListColumnRoom.Visible = true;
            this.treeListColumnRoom.VisibleIndex = 0;
            // 
            // labelControl34
            // 
            this.labelControl34.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl34.Location = new System.Drawing.Point(106, 57);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(55, 13);
            this.labelControl34.TabIndex = 15;
            this.labelControl34.Text = "Өрөө, ор*:";
            // 
            // dateEditStartDate
            // 
            this.dateEditStartDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditStartDate.EditValue = null;
            this.dateEditStartDate.Location = new System.Drawing.Point(167, 81);
            this.dateEditStartDate.Name = "dateEditStartDate";
            this.dateEditStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStartDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStartDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStartDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStartDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStartDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStartDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditStartDate.TabIndex = 3;
            // 
            // labelControl35
            // 
            this.labelControl35.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl35.Location = new System.Drawing.Point(73, 83);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(88, 13);
            this.labelControl35.TabIndex = 17;
            this.labelControl35.Text = "Эхэлсэн хугацаа:";
            // 
            // dateEditEndDate
            // 
            this.dateEditEndDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditEndDate.EditValue = null;
            this.dateEditEndDate.Location = new System.Drawing.Point(167, 106);
            this.dateEditEndDate.Name = "dateEditEndDate";
            this.dateEditEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEndDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEndDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEndDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEndDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEndDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEndDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditEndDate.TabIndex = 4;
            // 
            // labelControl36
            // 
            this.labelControl36.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl36.Location = new System.Drawing.Point(71, 109);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(90, 13);
            this.labelControl36.TabIndex = 19;
            this.labelControl36.Text = "Дууссан хугацаа:";
            // 
            // memoEditDesc
            // 
            this.memoEditDesc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditDesc.Location = new System.Drawing.Point(167, 132);
            this.memoEditDesc.Name = "memoEditDesc";
            this.memoEditDesc.Properties.MaxLength = 200;
            this.memoEditDesc.Size = new System.Drawing.Size(305, 137);
            this.memoEditDesc.TabIndex = 5;
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(115, 134);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(46, 13);
            this.labelControl33.TabIndex = 21;
            this.labelControl33.Text = "Тайлбар:";
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(397, 276);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 12;
            this.simpleButtonCancel.Text = "Болих";
            this.simpleButtonCancel.ToolTip = "Болих";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSave.Location = new System.Drawing.Point(316, 276);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 11;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 276);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 33;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // WardRoomInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(484, 311);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.simpleButtonSave);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.labelControl33);
            this.Controls.Add(this.memoEditDesc);
            this.Controls.Add(this.labelControl36);
            this.Controls.Add(this.dateEditEndDate);
            this.Controls.Add(this.labelControl35);
            this.Controls.Add(this.dateEditStartDate);
            this.Controls.Add(this.labelControl34);
            this.Controls.Add(this.treeListLookUpEditRoom);
            this.Controls.Add(this.labelControl37);
            this.Controls.Add(this.treeListLookUpEditWardLine);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WardRoomInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Эмчлүүлэгчийн тасаг өрөөний мэдээлэл";
            this.Shown += new System.EventHandler(this.PateintInformInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWardLine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditRoom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDesc.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWardLine;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditRoom;
        private DevExpress.XtraTreeList.TreeList treeList2;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.DateEdit dateEditStartDate;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.DateEdit dateEditEndDate;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.MemoEdit memoEditDesc;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnWard;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumnRoom;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
    }
}