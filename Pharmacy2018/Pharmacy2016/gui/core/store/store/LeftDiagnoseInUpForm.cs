﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.util;

namespace Pharmacy2016.gui.core.store.store
{
    /*
     * Гарах үеийн онош нэмэх, засах цонх.
     * 
     */

    public partial class LeftDiagnoseInUpForm : DevExpress.XtraEditors.XtraForm
    {
        #region Форм анх ачааллах функц

            private static LeftDiagnoseInUpForm INSTANCE = new LeftDiagnoseInUpForm();

            public static LeftDiagnoseInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            public LeftDiagnoseInUpForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                initBinding();
                reload();
            }

            private void initDataSource()
            {
                gridLookUpEditDoctor.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
                gridLookUpEditType.Properties.DataSource = StoreDataSet.Instance.DiagnoseType;
                gridLookUpEditCode.Properties.DataSource = StoreDataSet.Instance.Code;
            }

            private void initBinding()
            {
                gridLookUpEditDoctor.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftDiagnoseBindingSource, "userID", true));
                gridLookUpEditType.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftDiagnoseBindingSource, "diagnoseType", true));
                memoEditDiagnoseDescription.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftDiagnoseBindingSource, "description", true));
                gridLookUpEditCode.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreLeftDiagnoseBindingSource, "code", true));
            }

            private void initError()
            {
                gridLookUpEditDoctor.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }
        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insertLeftDiagnose();
                    }
                    else if (actionType == 2)
                    {
                        updateLeftDiagnose();
                    }

                    ShowDialog();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insertLeftDiagnose()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    StoreInUpForm.Instance.currentDiagnoseView["userID"] = UserUtil.getUserId();
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                    gridLookUpEditDoctor.EditValue = UserUtil.getUserId();
                }
            }

            private void updateLeftDiagnose()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                }
            }


            private void clearData()
            {
                clearErrorText();
                //clearBinding();
                InformationDataSet.SystemUserBindingSource.Filter = "";
                StoreDataSet.StoreLeftDiagnoseBindingSource.CancelEdit();
            }

            private void clearErrorText()
            {
                gridLookUpEditDoctor.ErrorText = "";
                gridLookUpEditType.ErrorText = "";
            }

            private void LeftDiagnoseInUpForm_Shown(object sender, EventArgs e)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    gridLookUpEditType.Focus();
                }
                else
                {
                    gridLookUpEditDoctor.Focus();
                }
            }

        #endregion

        #region Формын event

            private void simpleButtonSaveDiagnose_Click(object sender, EventArgs e)
            {
                saveLeftDiagnose();
            }

        #endregion    

        #region Формын функц

            private void reload()
            {
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.DOCTOR);
            }

            private bool checkDiagnose()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (gridLookUpEditDoctor.EditValue == DBNull.Value || gridLookUpEditDoctor.EditValue == null || gridLookUpEditDoctor.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмчийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditDoctor.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditDoctor.Focus();
                }
                if (gridLookUpEditType.EditValue == DBNull.Value || gridLookUpEditType.EditValue == null || gridLookUpEditType.EditValue.ToString().Equals(""))
                {
                    errorText = "Ангилалыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditType.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditType.Focus();
                    }
                }
                memoEditDiagnoseDescription.Text = memoEditDiagnoseDescription.Text.Trim();

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void saveLeftDiagnose()
            {
                if (checkDiagnose())
                {
                    StoreInUpForm.Instance.currLeftDiagnoseView["fullName"] = gridLookUpEditDoctor.Text;

                    StoreDataSet.StoreLeftDiagnoseBindingSource.EndEdit();
                    StoreInUpForm.Instance.currLeftDiagnoseView.EndEdit();
                    Close();
                }
            }

        #endregion            
    }
}