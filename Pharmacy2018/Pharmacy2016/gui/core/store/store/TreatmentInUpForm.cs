﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.store.store
{
    /*
     * Эмчилгээ нэмэх, засах цонх.
     * 
     */

    public partial class TreatmentInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц.

            private static TreatmentInUpForm INSTANCE = new TreatmentInUpForm();

            public static TreatmentInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private TreatmentInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initBinding();
                initDataSource();
                reload();
            }

            private void initDataSource()
            {
                gridLookUpEditMedicine.Properties.DataSource = UserDataSet.PackageMedicineBindingSource;
            }

            private void initBinding()
            {
                gridLookUpEditMedicine.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreTreatmentBindingSource, "medicineID", true));
                spinEditEachDay.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreTreatmentBindingSource, "eachDay", true));
                spinEditDuration.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreTreatmentBindingSource, "duration", true));
                dateEditTreatmentStartDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreTreatmentBindingSource, "startDate", true));
                dateEditTreatmentEndDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreTreatmentBindingSource, "endDate", true));
                memoEditTreatmentDescription.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreTreatmentBindingSource, "description", true));
                checkEditIsBreak.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreTreatmentBindingSource, "isBreak", true));
                dateEditBreakDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreTreatmentBindingSource, "breakDate", true));
            }

            private void initError()
            {
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditEachDay.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditDuration.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditTreatmentStartDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditTreatmentEndDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц.

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }
                    
                    actionType = type;
                    if (actionType == 1)
                    {
                        insertTreatment();
                    }
                    else if (actionType == 2)
                    {
                        updateTreatment();
                    }

                    ShowDialog();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insertTreatment()
            {
                DateTime now = DateTime.Now;
                InspectionInUpForm.Instance.currentTreatmentView["startDate"] = now;
                dateEditTreatmentEndDate.DateTime = now;
            }

            private void updateTreatment()
            {
                dateEditBreakDate.Properties.ReadOnly = !Convert.ToBoolean(InspectionInUpForm.Instance.currentTreatmentView["isBreak"]);
            }

            
            private void clearData()
            {
                clearErrorText();
               // UserDataSet.Instance.PackageMedicine.Clear();
                dateEditBreakDate.Properties.ReadOnly = true;

                StoreDataSet.StoreTreatmentBindingSource.CancelEdit();
            }

            private void clearErrorText()
            {
                gridLookUpEditMedicine.ErrorText = ""; ;
                spinEditEachDay.ErrorText = ""; ;
                spinEditDuration.ErrorText = ""; ;
                dateEditTreatmentStartDate.ErrorText = ""; ;
                dateEditTreatmentEndDate.ErrorText = "";
            }

            private void TreatmentInUpForm_Shown(object sender, EventArgs e)
            {
                gridLookUpEditMedicine.Focus();
            }

        #endregion

        #region Формын event

            private void dateEditTreatmentStartDate_EditValueChanged(object sender, EventArgs e)
            {
                changeDate();
            }

            private void checkEditIsBreak_CheckedChanged(object sender, EventArgs e)
            {
                dateEditBreakDate.Properties.ReadOnly = !checkEditIsBreak.Checked;
            }

            private void simpleButtonSaveTreatment_Click(object sender, EventArgs e)
            {
                saveTreatment();
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

        #endregion

        #region Формын функцууд.

            private void changeDate()
            {
                if (dateEditTreatmentStartDate.EditValue != DBNull.Value && dateEditTreatmentStartDate.EditValue != null &&
                    !dateEditTreatmentStartDate.EditValue.ToString().Equals("") && spinEditDuration.Value > 0)
                {
                    dateEditTreatmentEndDate.DateTime = dateEditTreatmentStartDate.DateTime.AddDays(Convert.ToDouble(spinEditDuration.Value));                  
                }
            }

            private void reload()
            {
                UserDataSet.PackageMedicineTableAdapter.Fill(UserDataSet.Instance.PackageMedicine, HospitalUtil.getHospitalId());
            }

            private bool checkTreatment()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
                if (spinEditEachDay.Value <= 0)
                {
                    errorText = "Өдөрт уух хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditEachDay.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditEachDay.Focus();
                       // text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (dateEditTreatmentStartDate.EditValue == DBNull.Value || dateEditTreatmentStartDate.EditValue == null || dateEditTreatmentStartDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмчилгээ эхлэх огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditTreatmentStartDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditTreatmentStartDate.Focus();
                    }
                }
                if (spinEditDuration.Value <= 0)
                {
                    errorText = "Эмчилгээ үргэлжлэх хугацааг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditDuration.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditDuration.Focus();
                    }
                }
                if (dateEditTreatmentEndDate.EditValue == DBNull.Value || dateEditTreatmentEndDate.EditValue == null || dateEditTreatmentEndDate.Text == "")
                {
                    errorText = "Эмчилгээ дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditTreatmentEndDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditTreatmentEndDate.Focus();
                    }
                }
                //for (int i = 0; isRight && i < .RowCount; i++)
                //{
                //    if (gridViewDetail.FocusedRowHandle != i && gridLookUpEditMedicine.EditValue != null
                //        && gridLookUpEditMedicine.EditValue.ToString().Equals(gridViewDetail.GetRowCellValue(i, "medicineID").ToString()))
                //    {
                //        isRight = false;
                //        gridLookUpEditMedicine.ErrorText = "Эм давхацсан байна";
                //        gridLookUpEditMedicine.Focus();
                //        break;
                //    }
                //}
                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void saveTreatment()
            {
                if (checkTreatment()) 
                {
                    InspectionInUpForm.Instance.currentTreatmentView["name"] = gridLookUpEditMedicine.Text;
                    InspectionInUpForm.Instance.currentTreatmentView["endDate"] = dateEditTreatmentEndDate.EditValue; 
                    InspectionInUpForm.Instance.currentTreatmentView.EndEdit();
                    Close();
                }  
            }

        #endregion

    }
}