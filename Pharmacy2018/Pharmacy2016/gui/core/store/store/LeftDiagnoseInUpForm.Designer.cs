﻿namespace Pharmacy2016.gui.core.store.store
{
    partial class LeftDiagnoseInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridLookUpEditType = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.memoEditDiagnoseDescription = new DevExpress.XtraEditors.MemoEdit();
            this.gridLookUpEditDoctor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlExpUser = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonCancelDiagnose = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSaveDiagnose = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditCode = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDiagnoseDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridLookUpEditType
            // 
            this.gridLookUpEditType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditType.Location = new System.Drawing.Point(91, 49);
            this.gridLookUpEditType.Name = "gridLookUpEditType";
            this.gridLookUpEditType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditType.Properties.DisplayMember = "name";
            this.gridLookUpEditType.Properties.NullText = " ";
            this.gridLookUpEditType.Properties.ValueMember = "id";
            this.gridLookUpEditType.Properties.View = this.gridLookUpEdit1View;
            this.gridLookUpEditType.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditType.TabIndex = 9;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.ReadOnly = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Нэр";
            this.gridColumn2.FieldName = "name";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(33, 52);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 13);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "Ангилал*:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(39, 78);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(46, 13);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Тайлбар:";
            // 
            // memoEditDiagnoseDescription
            // 
            this.memoEditDiagnoseDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditDiagnoseDescription.Location = new System.Drawing.Point(91, 75);
            this.memoEditDiagnoseDescription.Name = "memoEditDiagnoseDescription";
            this.memoEditDiagnoseDescription.Properties.MaxLength = 200;
            this.memoEditDiagnoseDescription.Size = new System.Drawing.Size(355, 100);
            this.memoEditDiagnoseDescription.TabIndex = 12;
            // 
            // gridLookUpEditDoctor
            // 
            this.gridLookUpEditDoctor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditDoctor.EditValue = "";
            this.gridLookUpEditDoctor.Location = new System.Drawing.Point(91, 23);
            this.gridLookUpEditDoctor.Name = "gridLookUpEditDoctor";
            this.gridLookUpEditDoctor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDoctor.Properties.DisplayMember = "fullName";
            this.gridLookUpEditDoctor.Properties.NullText = "";
            this.gridLookUpEditDoctor.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditDoctor.Properties.ValueMember = "id";
            this.gridLookUpEditDoctor.Properties.View = this.gridView4;
            this.gridLookUpEditDoctor.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditDoctor.TabIndex = 32;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Нэр";
            this.gridColumn21.CustomizationCaption = "Нэр";
            this.gridColumn21.FieldName = "firstName";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Регистер";
            this.gridColumn22.CustomizationCaption = "Регистер";
            this.gridColumn22.FieldName = "id";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Эрх";
            this.gridColumn23.CustomizationCaption = "Эрх";
            this.gridColumn23.FieldName = "roleName";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Тасаг";
            this.gridColumn24.CustomizationCaption = "Тасаг";
            this.gridColumn24.FieldName = "wardName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Ажлын газар";
            this.gridColumn25.CustomizationCaption = "Ажлын газар";
            this.gridColumn25.FieldName = "organizationName";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Албан тушаал";
            this.gridColumn26.CustomizationCaption = "Албан тушаал";
            this.gridColumn26.FieldName = "positionName";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            // 
            // labelControlExpUser
            // 
            this.labelControlExpUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlExpUser.Location = new System.Drawing.Point(53, 26);
            this.labelControlExpUser.Name = "labelControlExpUser";
            this.labelControlExpUser.Size = new System.Drawing.Size(32, 13);
            this.labelControlExpUser.TabIndex = 33;
            this.labelControlExpUser.Text = "Эмч*: ";
            // 
            // simpleButtonCancelDiagnose
            // 
            this.simpleButtonCancelDiagnose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancelDiagnose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancelDiagnose.Location = new System.Drawing.Point(397, 226);
            this.simpleButtonCancelDiagnose.Name = "simpleButtonCancelDiagnose";
            this.simpleButtonCancelDiagnose.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancelDiagnose.TabIndex = 35;
            this.simpleButtonCancelDiagnose.Text = "Болих";
            this.simpleButtonCancelDiagnose.ToolTip = "Болих";
            // 
            // simpleButtonSaveDiagnose
            // 
            this.simpleButtonSaveDiagnose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSaveDiagnose.Location = new System.Drawing.Point(316, 226);
            this.simpleButtonSaveDiagnose.Name = "simpleButtonSaveDiagnose";
            this.simpleButtonSaveDiagnose.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSaveDiagnose.TabIndex = 34;
            this.simpleButtonSaveDiagnose.Text = "Хадгалах";
            this.simpleButtonSaveDiagnose.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSaveDiagnose.Click += new System.EventHandler(this.simpleButtonSaveDiagnose_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 184);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(72, 13);
            this.labelControl1.TabIndex = 36;
            this.labelControl1.Text = "Оношийн код:";
            // 
            // gridLookUpEditCode
            // 
            this.gridLookUpEditCode.Location = new System.Drawing.Point(91, 181);
            this.gridLookUpEditCode.Name = "gridLookUpEditCode";
            this.gridLookUpEditCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditCode.Properties.DisplayMember = "name";
            this.gridLookUpEditCode.Properties.NullText = "";
            this.gridLookUpEditCode.Properties.ValueMember = "id";
            this.gridLookUpEditCode.Properties.View = this.gridView1;
            this.gridLookUpEditCode.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditCode.TabIndex = 37;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Кодууд";
            this.gridColumn3.FieldName = "name";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // LeftDiagnoseInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancelDiagnose;
            this.ClientSize = new System.Drawing.Size(484, 261);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.simpleButtonCancelDiagnose);
            this.Controls.Add(this.simpleButtonSaveDiagnose);
            this.Controls.Add(this.gridLookUpEditDoctor);
            this.Controls.Add(this.labelControlExpUser);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.memoEditDiagnoseDescription);
            this.Controls.Add(this.gridLookUpEditType);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.gridLookUpEditCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LeftDiagnoseInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Эмнэлгээс гарах үеийн онош";
            this.Shown += new System.EventHandler(this.LeftDiagnoseInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDiagnoseDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.MemoEdit memoEditDiagnoseDescription;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDoctor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.LabelControl labelControlExpUser;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancelDiagnose;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSaveDiagnose;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditCode;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
    }
}