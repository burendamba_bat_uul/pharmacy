﻿namespace Pharmacy2016.gui.core.store.store
{
    partial class ValidityInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ValidityInUpForm));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditDate = new DevExpress.XtraEditors.DateEdit();
            this.memoEditDesc = new DevExpress.XtraEditors.MemoEdit();
            this.gridLookUpEditDiagnoseType = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditDoctor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDiagnoseType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(31, 41);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(94, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Оношийн ангилал:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(64, 173);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(61, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Эмчийн нэр:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(27, 67);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(98, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Оношийн үндэслэл:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(90, 15);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(35, 13);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Огноо:";
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(397, 226);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancel.TabIndex = 8;
            this.simpleButtonCancel.Text = "Болих";
            this.simpleButtonCancel.ToolTip = "Болих";
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Location = new System.Drawing.Point(316, 226);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSave.TabIndex = 9;
            this.simpleButtonSave.Text = "Хадгалах";
            this.simpleButtonSave.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // dateEditDate
            // 
            this.dateEditDate.EditValue = null;
            this.dateEditDate.Location = new System.Drawing.Point(131, 12);
            this.dateEditDate.Name = "dateEditDate";
            this.dateEditDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditDate.Size = new System.Drawing.Size(200, 20);
            this.dateEditDate.TabIndex = 4;
            // 
            // memoEditDesc
            // 
            this.memoEditDesc.Location = new System.Drawing.Point(131, 64);
            this.memoEditDesc.Name = "memoEditDesc";
            this.memoEditDesc.Properties.MaxLength = 200;
            this.memoEditDesc.Size = new System.Drawing.Size(300, 100);
            this.memoEditDesc.TabIndex = 6;
            // 
            // gridLookUpEditDiagnoseType
            // 
            this.gridLookUpEditDiagnoseType.Location = new System.Drawing.Point(131, 38);
            this.gridLookUpEditDiagnoseType.Name = "gridLookUpEditDiagnoseType";
            this.gridLookUpEditDiagnoseType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDiagnoseType.Properties.DisplayMember = "name";
            this.gridLookUpEditDiagnoseType.Properties.NullText = "";
            this.gridLookUpEditDiagnoseType.Properties.ValueMember = "id";
            this.gridLookUpEditDiagnoseType.Properties.View = this.gridView1;
            this.gridLookUpEditDiagnoseType.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditDiagnoseType.TabIndex = 5;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Ангилал";
            this.gridColumn2.CustomizationCaption = "Ангилал";
            this.gridColumn2.FieldName = "name";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // gridLookUpEditDoctor
            // 
            this.gridLookUpEditDoctor.EditValue = "";
            this.gridLookUpEditDoctor.Location = new System.Drawing.Point(131, 170);
            this.gridLookUpEditDoctor.Name = "gridLookUpEditDoctor";
            this.gridLookUpEditDoctor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDoctor.Properties.DisplayMember = "fullName";
            this.gridLookUpEditDoctor.Properties.NullText = "";
            this.gridLookUpEditDoctor.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditDoctor.Properties.ValueMember = "id";
            this.gridLookUpEditDoctor.Properties.View = this.gridView4;
            this.gridLookUpEditDoctor.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditDoctor.TabIndex = 10;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Нэр";
            this.gridColumn21.CustomizationCaption = "Нэр";
            this.gridColumn21.FieldName = "firstName";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Регистер";
            this.gridColumn22.CustomizationCaption = "Регистер";
            this.gridColumn22.FieldName = "id";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Эрх";
            this.gridColumn23.CustomizationCaption = "Эрх";
            this.gridColumn23.FieldName = "roleName";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Тасаг";
            this.gridColumn24.CustomizationCaption = "Тасаг";
            this.gridColumn24.FieldName = "wardName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Ажлын газар";
            this.gridColumn25.CustomizationCaption = "Ажлын газар";
            this.gridColumn25.FieldName = "organizationName";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Албан тушаал";
            this.gridColumn26.CustomizationCaption = "Албан тушаал";
            this.gridColumn26.FieldName = "positionName";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 226);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 34;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // ValidityInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(484, 261);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.gridLookUpEditDoctor);
            this.Controls.Add(this.simpleButtonSave);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.dateEditDate);
            this.Controls.Add(this.memoEditDesc);
            this.Controls.Add(this.gridLookUpEditDiagnoseType);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ValidityInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "КИЛИНИКИЙН ОНОШИЙН ҮНДЭСЛЭЛ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ValidityInUpForm_FormClosing);
            this.Shown += new System.EventHandler(this.ValidityInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDiagnoseType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.DateEdit dateEditDate;
        private DevExpress.XtraEditors.MemoEdit memoEditDesc;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDiagnoseType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDoctor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
    }
}