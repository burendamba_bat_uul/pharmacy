﻿namespace Pharmacy2016.gui.core.store.store
{
    partial class TreatmentInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TreatmentInUpForm));
            this.dateEditTreatmentStartDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditTreatmentEndDate = new DevExpress.XtraEditors.DateEdit();
            this.simpleButtonSaveTreatment = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonBackTreatment = new DevExpress.XtraEditors.SimpleButton();
            this.memoEditTreatmentDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditDuration = new DevExpress.XtraEditors.SpinEdit();
            this.gridLookUpEditMedicine = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spinEditEachDay = new DevExpress.XtraEditors.SpinEdit();
            this.checkEditIsBreak = new DevExpress.XtraEditors.CheckEdit();
            this.dateEditBreakDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditTreatmentDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDuration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEachDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsBreak.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // dateEditTreatmentStartDate
            // 
            this.dateEditTreatmentStartDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditTreatmentStartDate.EditValue = null;
            this.dateEditTreatmentStartDate.Location = new System.Drawing.Point(134, 82);
            this.dateEditTreatmentStartDate.Name = "dateEditTreatmentStartDate";
            this.dateEditTreatmentStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTreatmentStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTreatmentStartDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditTreatmentStartDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditTreatmentStartDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditTreatmentStartDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditTreatmentStartDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditTreatmentStartDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditTreatmentStartDate.TabIndex = 3;
            this.dateEditTreatmentStartDate.EditValueChanged += new System.EventHandler(this.dateEditTreatmentStartDate_EditValueChanged);
            // 
            // dateEditTreatmentEndDate
            // 
            this.dateEditTreatmentEndDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditTreatmentEndDate.EditValue = null;
            this.dateEditTreatmentEndDate.Location = new System.Drawing.Point(134, 134);
            this.dateEditTreatmentEndDate.Name = "dateEditTreatmentEndDate";
            this.dateEditTreatmentEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTreatmentEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTreatmentEndDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditTreatmentEndDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditTreatmentEndDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditTreatmentEndDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditTreatmentEndDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditTreatmentEndDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditTreatmentEndDate.TabIndex = 5;
            // 
            // simpleButtonSaveTreatment
            // 
            this.simpleButtonSaveTreatment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSaveTreatment.Location = new System.Drawing.Point(216, 376);
            this.simpleButtonSaveTreatment.Name = "simpleButtonSaveTreatment";
            this.simpleButtonSaveTreatment.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSaveTreatment.TabIndex = 11;
            this.simpleButtonSaveTreatment.Text = "Хадгалах";
            this.simpleButtonSaveTreatment.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSaveTreatment.Click += new System.EventHandler(this.simpleButtonSaveTreatment_Click);
            // 
            // simpleButtonBackTreatment
            // 
            this.simpleButtonBackTreatment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonBackTreatment.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonBackTreatment.Location = new System.Drawing.Point(297, 376);
            this.simpleButtonBackTreatment.Name = "simpleButtonBackTreatment";
            this.simpleButtonBackTreatment.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonBackTreatment.TabIndex = 12;
            this.simpleButtonBackTreatment.Text = "Болих";
            this.simpleButtonBackTreatment.ToolTip = "Болих";
            // 
            // memoEditTreatmentDescription
            // 
            this.memoEditTreatmentDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditTreatmentDescription.Location = new System.Drawing.Point(89, 211);
            this.memoEditTreatmentDescription.Name = "memoEditTreatmentDescription";
            this.memoEditTreatmentDescription.Properties.MaxLength = 200;
            this.memoEditTreatmentDescription.Size = new System.Drawing.Size(283, 159);
            this.memoEditTreatmentDescription.TabIndex = 8;
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(105, 33);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(23, 13);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "Эм*:";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(21, 111);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(107, 13);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "Үргэлжлэх хугацаа*:";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(26, 59);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(102, 13);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Өдөрт уух хэмжээ*:";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(46, 85);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(82, 13);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Эхлэсэн огноо*:";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl6.Location = new System.Drawing.Point(49, 137);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(79, 13);
            this.labelControl6.TabIndex = 14;
            this.labelControl6.Text = "Дуусах огноо*:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(37, 214);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(46, 13);
            this.labelControl7.TabIndex = 15;
            this.labelControl7.Text = "Тайлбар:";
            // 
            // spinEditDuration
            // 
            this.spinEditDuration.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditDuration.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditDuration.Location = new System.Drawing.Point(134, 108);
            this.spinEditDuration.Name = "spinEditDuration";
            this.spinEditDuration.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditDuration.Properties.DisplayFormat.FormatString = "n0";
            this.spinEditDuration.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditDuration.Properties.EditFormat.FormatString = "n0";
            this.spinEditDuration.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditDuration.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditDuration.Properties.Mask.EditMask = "n0";
            this.spinEditDuration.Size = new System.Drawing.Size(100, 20);
            this.spinEditDuration.TabIndex = 4;
            this.spinEditDuration.EditValueChanged += new System.EventHandler(this.dateEditTreatmentStartDate_EditValueChanged);
            // 
            // gridLookUpEditMedicine
            // 
            this.gridLookUpEditMedicine.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditMedicine.EditValue = "";
            this.gridLookUpEditMedicine.Location = new System.Drawing.Point(134, 30);
            this.gridLookUpEditMedicine.Name = "gridLookUpEditMedicine";
            this.gridLookUpEditMedicine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMedicine.Properties.DisplayMember = "name";
            this.gridLookUpEditMedicine.Properties.NullText = "";
            this.gridLookUpEditMedicine.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditMedicine.Properties.ValueMember = "id";
            this.gridLookUpEditMedicine.Properties.View = this.gridView3;
            this.gridLookUpEditMedicine.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditMedicine.TabIndex = 1;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn18,
            this.gridColumn1,
            this.gridColumn20,
            this.gridColumn17,
            this.gridColumn2});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Нэр";
            this.gridColumn15.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn15.FieldName = "name";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Ангилал";
            this.gridColumn17.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn17.FieldName = "medicineTypeName";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 1;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Тун хэмжээ";
            this.gridColumn18.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn18.FieldName = "unitName";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 1;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Хэмжих нэгж";
            this.gridColumn20.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn20.FieldName = "quantityName";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 3;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Хэлбэр";
            this.gridColumn1.CustomizationCaption = "Хэлбэр";
            this.gridColumn1.FieldName = "shapeName";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            // 
            // spinEditEachDay
            // 
            this.spinEditEachDay.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditEachDay.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditEachDay.Location = new System.Drawing.Point(134, 56);
            this.spinEditEachDay.Name = "spinEditEachDay";
            this.spinEditEachDay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditEachDay.Properties.DisplayFormat.FormatString = "n0";
            this.spinEditEachDay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditEachDay.Properties.EditFormat.FormatString = "n0";
            this.spinEditEachDay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditEachDay.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditEachDay.Properties.Mask.EditMask = "n0";
            this.spinEditEachDay.Size = new System.Drawing.Size(100, 20);
            this.spinEditEachDay.TabIndex = 2;
            // 
            // checkEditIsBreak
            // 
            this.checkEditIsBreak.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkEditIsBreak.Location = new System.Drawing.Point(134, 160);
            this.checkEditIsBreak.Name = "checkEditIsBreak";
            this.checkEditIsBreak.Properties.Caption = "";
            this.checkEditIsBreak.Size = new System.Drawing.Size(26, 19);
            this.checkEditIsBreak.TabIndex = 6;
            this.checkEditIsBreak.CheckedChanged += new System.EventHandler(this.checkEditIsBreak_CheckedChanged);
            // 
            // dateEditBreakDate
            // 
            this.dateEditBreakDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditBreakDate.EditValue = null;
            this.dateEditBreakDate.Location = new System.Drawing.Point(134, 185);
            this.dateEditBreakDate.Name = "dateEditBreakDate";
            this.dateEditBreakDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBreakDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBreakDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBreakDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBreakDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.ReadOnly = true;
            this.dateEditBreakDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditBreakDate.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(28, 163);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(100, 13);
            this.labelControl1.TabIndex = 19;
            this.labelControl1.Text = "Эмчилгээг зогсоох :";
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(53, 188);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(75, 13);
            this.labelControl8.TabIndex = 20;
            this.labelControl8.Text = "Зогсоох өдөр :";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 376);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 13;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "ОУ -н ангилал";
            this.gridColumn2.CustomizationCaption = "ОУ -н ангилал";
            this.gridColumn2.FieldName = "medicineTypeInterName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 5;
            // 
            // TreatmentInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonBackTreatment;
            this.ClientSize = new System.Drawing.Size(384, 411);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.dateEditBreakDate);
            this.Controls.Add(this.checkEditIsBreak);
            this.Controls.Add(this.spinEditEachDay);
            this.Controls.Add(this.gridLookUpEditMedicine);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.memoEditTreatmentDescription);
            this.Controls.Add(this.simpleButtonBackTreatment);
            this.Controls.Add(this.simpleButtonSaveTreatment);
            this.Controls.Add(this.dateEditTreatmentEndDate);
            this.Controls.Add(this.dateEditTreatmentStartDate);
            this.Controls.Add(this.spinEditDuration);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TreatmentInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Эмчилгээ нэмэх";
            this.Shown += new System.EventHandler(this.TreatmentInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditTreatmentDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDuration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEachDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsBreak.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.DateEdit dateEditTreatmentStartDate;
        private DevExpress.XtraEditors.DateEdit dateEditTreatmentEndDate;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSaveTreatment;
        private DevExpress.XtraEditors.SimpleButton simpleButtonBackTreatment;
        private DevExpress.XtraEditors.MemoEdit memoEditTreatmentDescription;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SpinEdit spinEditDuration;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMedicine;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.SpinEdit spinEditEachDay;
        private DevExpress.XtraEditors.CheckEdit checkEditIsBreak;
        private DevExpress.XtraEditors.DateEdit dateEditBreakDate;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
    }
}