﻿namespace Pharmacy2016.gui.core.store.store
{
    partial class StoreForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode5 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode6 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode7 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode8 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode9 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode10 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode11 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode12 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode13 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StoreForm));
            this.gridViewWardRoom = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlStore = new DevExpress.XtraGrid.GridControl();
            this.gridViewDiagnose = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnDiagnosetype = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDiagnose = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDiagnoseDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDiagnoseDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDiagnoseDoctorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewInspection = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumnInspection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnInspectionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnInspectionDoctorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewTreatment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnTreatmentMedicine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnTreatmentStarDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnTreatmentEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnTreatmentDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnTreatmentEachDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnTreatmentDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewStoreHistory = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnStoreHistoryHigh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreHistoryWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreHistoryBodyIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreHistoryPain = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreHistoryHistory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreHistorylifeStore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreHistoryLifeTerm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStoreHistoryWorkTerm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewGeneral = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnGeneralCondition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnGeneralMind = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnGeneralSurround = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnGeneralPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewInternal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnBronkhofoni = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSoundVib = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnChestShape = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnBreathShape = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewHeart = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewDigestive = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewUrine = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewStoreLeft = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewValidity = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewLeftDiagnose = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnLeftDiagnoseDoctorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLeftDiagnoseTypeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLeftDiagnoseDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLeftDiagnoseCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewStore = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnStoreNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLastName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnWardName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnFirstName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnRegister = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnBirthdate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnGender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnBeginDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonLayout = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemMot = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEndBal = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.radioGroupIsUse = new DevExpress.XtraEditors.RadioGroup();
            this.simpleButtonReloadStore = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWardRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDiagnose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTreatment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStoreHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGeneral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInternal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHeart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDigestive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUrine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStoreLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewValidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLeftDiagnose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsUse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridViewWardRoom
            // 
            this.gridViewWardRoom.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridViewWardRoom.GridControl = this.gridControlStore;
            this.gridViewWardRoom.Name = "gridViewWardRoom";
            this.gridViewWardRoom.OptionsBehavior.ReadOnly = true;
            this.gridViewWardRoom.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Эхэлсэн хугацаа";
            this.gridColumn1.FieldName = "startDate";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Дуусах огноо";
            this.gridColumn2.FieldName = "endDate";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Тасгийн нэр";
            this.gridColumn3.FieldName = "wardName";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Өрөө, ор";
            this.gridColumn4.FieldName = "bedName";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Тайлбар";
            this.gridColumn5.FieldName = "description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Одоо байгаа эсэх";
            this.gridColumn6.FieldName = "isCur";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // gridControlStore
            // 
            this.gridControlStore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlStore.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlStore.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlStore.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlStore.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlStore.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlStore.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add_store"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete_store"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "edit_store"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 11, true, true, "Нээх", "close_store")});
            this.gridControlStore.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlStore_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.LevelTemplate = this.gridViewWardRoom;
            gridLevelNode1.RelationName = "Store_StoreWardRoom";
            gridLevelNode2.LevelTemplate = this.gridViewDiagnose;
            gridLevelNode2.RelationName = "Store_StoreDiagnose";
            gridLevelNode3.LevelTemplate = this.gridViewInspection;
            gridLevelNode4.LevelTemplate = this.gridViewTreatment;
            gridLevelNode4.RelationName = "StoreInspection_StoreTreatment";
            gridLevelNode3.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode4});
            gridLevelNode3.RelationName = "Store_StoreInspection";
            gridLevelNode5.LevelTemplate = this.gridViewStoreHistory;
            gridLevelNode5.RelationName = "Store_StoreHistory";
            gridLevelNode6.LevelTemplate = this.gridViewGeneral;
            gridLevelNode6.RelationName = "Store_StoreGeneralinspection";
            gridLevelNode7.LevelTemplate = this.gridViewInternal;
            gridLevelNode7.RelationName = "Store_StoreInternalinspection";
            gridLevelNode8.LevelTemplate = this.gridViewHeart;
            gridLevelNode8.RelationName = "Store_StoreHeart";
            gridLevelNode9.LevelTemplate = this.gridViewDigestive;
            gridLevelNode9.RelationName = "Store_StoreDigestive";
            gridLevelNode10.LevelTemplate = this.gridViewUrine;
            gridLevelNode10.RelationName = "Store_StoreUrine";
            gridLevelNode11.LevelTemplate = this.gridViewStoreLeft;
            gridLevelNode11.RelationName = "Store_StoreLeft";
            gridLevelNode12.LevelTemplate = this.gridViewValidity;
            gridLevelNode12.RelationName = "Store_StoreValidity";
            gridLevelNode13.LevelTemplate = this.gridViewLeftDiagnose;
            gridLevelNode13.RelationName = "Store_StoreLeftDiagnose";
            this.gridControlStore.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2,
            gridLevelNode3,
            gridLevelNode5,
            gridLevelNode6,
            gridLevelNode7,
            gridLevelNode8,
            gridLevelNode9,
            gridLevelNode10,
            gridLevelNode11,
            gridLevelNode12,
            gridLevelNode13});
            this.gridControlStore.Location = new System.Drawing.Point(2, 2);
            this.gridControlStore.MainView = this.gridViewStore;
            this.gridControlStore.Name = "gridControlStore";
            this.gridControlStore.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridControlStore.Size = new System.Drawing.Size(780, 509);
            this.gridControlStore.TabIndex = 0;
            this.gridControlStore.UseEmbeddedNavigator = true;
            this.gridControlStore.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDiagnose,
            this.gridViewInspection,
            this.gridViewTreatment,
            this.gridViewStoreHistory,
            this.gridViewGeneral,
            this.gridViewInternal,
            this.gridViewHeart,
            this.gridViewDigestive,
            this.gridViewUrine,
            this.gridViewStoreLeft,
            this.gridViewValidity,
            this.gridViewLeftDiagnose,
            this.gridViewStore,
            this.gridViewWardRoom});            
            // 
            // gridViewDiagnose
            // 
            this.gridViewDiagnose.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnDiagnosetype,
            this.gridColumnDiagnose,
            this.gridColumnDiagnoseDescription,
            this.gridColumnDiagnoseDate,
            this.gridColumnDiagnoseDoctorName});
            this.gridViewDiagnose.GridControl = this.gridControlStore;
            this.gridViewDiagnose.Name = "gridViewDiagnose";
            this.gridViewDiagnose.OptionsBehavior.ReadOnly = true;
            this.gridViewDiagnose.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnDiagnosetype
            // 
            this.gridColumnDiagnosetype.Caption = "Оношийн ангилал";
            this.gridColumnDiagnosetype.FieldName = "diagnoseTypeName";
            this.gridColumnDiagnosetype.Name = "gridColumnDiagnosetype";
            this.gridColumnDiagnosetype.Visible = true;
            this.gridColumnDiagnosetype.VisibleIndex = 2;
            // 
            // gridColumnDiagnose
            // 
            this.gridColumnDiagnose.Caption = "Онош";
            this.gridColumnDiagnose.FieldName = "diagnose";
            this.gridColumnDiagnose.Name = "gridColumnDiagnose";
            this.gridColumnDiagnose.Visible = true;
            this.gridColumnDiagnose.VisibleIndex = 3;
            // 
            // gridColumnDiagnoseDescription
            // 
            this.gridColumnDiagnoseDescription.Caption = "Тайлбар";
            this.gridColumnDiagnoseDescription.FieldName = "description";
            this.gridColumnDiagnoseDescription.Name = "gridColumnDiagnoseDescription";
            this.gridColumnDiagnoseDescription.Visible = true;
            this.gridColumnDiagnoseDescription.VisibleIndex = 4;
            // 
            // gridColumnDiagnoseDate
            // 
            this.gridColumnDiagnoseDate.Caption = "Онош бичсэн огноо";
            this.gridColumnDiagnoseDate.FieldName = "date";
            this.gridColumnDiagnoseDate.Name = "gridColumnDiagnoseDate";
            this.gridColumnDiagnoseDate.Visible = true;
            this.gridColumnDiagnoseDate.VisibleIndex = 1;
            // 
            // gridColumnDiagnoseDoctorName
            // 
            this.gridColumnDiagnoseDoctorName.Caption = "Эмчийн нэр";
            this.gridColumnDiagnoseDoctorName.CustomizationCaption = "Эмчийн нэр";
            this.gridColumnDiagnoseDoctorName.FieldName = "fullName";
            this.gridColumnDiagnoseDoctorName.Name = "gridColumnDiagnoseDoctorName";
            this.gridColumnDiagnoseDoctorName.Visible = true;
            this.gridColumnDiagnoseDoctorName.VisibleIndex = 0;
            // 
            // gridViewInspection
            // 
            this.gridViewInspection.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnInspectionDate,
            this.gridColumnInspection,
            this.gridColumnInspectionDescription,
            this.gridColumnInspectionDoctorName});
            this.gridViewInspection.GridControl = this.gridControlStore;
            this.gridViewInspection.Name = "gridViewInspection";
            this.gridViewInspection.OptionsBehavior.ReadOnly = true;
            this.gridViewInspection.OptionsDetail.ShowDetailTabs = false;
            this.gridViewInspection.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnInspectionDate
            // 
            this.gridColumnInspectionDate.Caption = "Үзлэг хийсэн огноо";
            this.gridColumnInspectionDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnInspectionDate.FieldName = "date";
            this.gridColumnInspectionDate.Name = "gridColumnInspectionDate";
            this.gridColumnInspectionDate.Visible = true;
            this.gridColumnInspectionDate.VisibleIndex = 1;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridColumnInspection
            // 
            this.gridColumnInspection.Caption = "Үзлэгийн тэмдэглэл";
            this.gridColumnInspection.FieldName = "inspection";
            this.gridColumnInspection.Name = "gridColumnInspection";
            this.gridColumnInspection.Visible = true;
            this.gridColumnInspection.VisibleIndex = 2;
            // 
            // gridColumnInspectionDescription
            // 
            this.gridColumnInspectionDescription.Caption = "Тайлбар";
            this.gridColumnInspectionDescription.FieldName = "description";
            this.gridColumnInspectionDescription.Name = "gridColumnInspectionDescription";
            this.gridColumnInspectionDescription.Visible = true;
            this.gridColumnInspectionDescription.VisibleIndex = 3;
            // 
            // gridColumnInspectionDoctorName
            // 
            this.gridColumnInspectionDoctorName.Caption = "Эмчийн нэр";
            this.gridColumnInspectionDoctorName.CustomizationCaption = "Эмчийн нэр";
            this.gridColumnInspectionDoctorName.FieldName = "fullName";
            this.gridColumnInspectionDoctorName.Name = "gridColumnInspectionDoctorName";
            this.gridColumnInspectionDoctorName.Visible = true;
            this.gridColumnInspectionDoctorName.VisibleIndex = 0;
            // 
            // gridViewTreatment
            // 
            this.gridViewTreatment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnTreatmentMedicine,
            this.gridColumnTreatmentStarDate,
            this.gridColumnTreatmentEndDate,
            this.gridColumnTreatmentDuration,
            this.gridColumnTreatmentEachDay,
            this.gridColumnTreatmentDescription});
            this.gridViewTreatment.GridControl = this.gridControlStore;
            this.gridViewTreatment.Name = "gridViewTreatment";
            this.gridViewTreatment.OptionsBehavior.ReadOnly = true;
            this.gridViewTreatment.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnTreatmentMedicine
            // 
            this.gridColumnTreatmentMedicine.Caption = "Эм";
            this.gridColumnTreatmentMedicine.FieldName = "name";
            this.gridColumnTreatmentMedicine.Name = "gridColumnTreatmentMedicine";
            this.gridColumnTreatmentMedicine.Visible = true;
            this.gridColumnTreatmentMedicine.VisibleIndex = 2;
            // 
            // gridColumnTreatmentStarDate
            // 
            this.gridColumnTreatmentStarDate.Caption = "Эхлэх огноо";
            this.gridColumnTreatmentStarDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnTreatmentStarDate.FieldName = "startDate";
            this.gridColumnTreatmentStarDate.Name = "gridColumnTreatmentStarDate";
            this.gridColumnTreatmentStarDate.Visible = true;
            this.gridColumnTreatmentStarDate.VisibleIndex = 0;
            // 
            // gridColumnTreatmentEndDate
            // 
            this.gridColumnTreatmentEndDate.Caption = "Дуусах огноо";
            this.gridColumnTreatmentEndDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnTreatmentEndDate.FieldName = "endDate";
            this.gridColumnTreatmentEndDate.Name = "gridColumnTreatmentEndDate";
            this.gridColumnTreatmentEndDate.Visible = true;
            this.gridColumnTreatmentEndDate.VisibleIndex = 1;
            // 
            // gridColumnTreatmentDuration
            // 
            this.gridColumnTreatmentDuration.Caption = "Үргэлжлэх хугацаа";
            this.gridColumnTreatmentDuration.FieldName = "duration";
            this.gridColumnTreatmentDuration.Name = "gridColumnTreatmentDuration";
            this.gridColumnTreatmentDuration.Visible = true;
            this.gridColumnTreatmentDuration.VisibleIndex = 3;
            // 
            // gridColumnTreatmentEachDay
            // 
            this.gridColumnTreatmentEachDay.Caption = "Өдөрт уух тоо";
            this.gridColumnTreatmentEachDay.FieldName = "eachDay";
            this.gridColumnTreatmentEachDay.Name = "gridColumnTreatmentEachDay";
            this.gridColumnTreatmentEachDay.Visible = true;
            this.gridColumnTreatmentEachDay.VisibleIndex = 4;
            // 
            // gridColumnTreatmentDescription
            // 
            this.gridColumnTreatmentDescription.Caption = "Тайлбар";
            this.gridColumnTreatmentDescription.FieldName = "description";
            this.gridColumnTreatmentDescription.Name = "gridColumnTreatmentDescription";
            this.gridColumnTreatmentDescription.Visible = true;
            this.gridColumnTreatmentDescription.VisibleIndex = 5;
            // 
            // gridViewStoreHistory
            // 
            this.gridViewStoreHistory.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnStoreHistoryHigh,
            this.gridColumnStoreHistoryWeight,
            this.gridColumnStoreHistoryBodyIndex,
            this.gridColumnStoreHistoryPain,
            this.gridColumnStoreHistoryHistory,
            this.gridColumnStoreHistorylifeStore,
            this.gridColumnStoreHistoryLifeTerm,
            this.gridColumnStoreHistoryWorkTerm});
            this.gridViewStoreHistory.GridControl = this.gridControlStore;
            this.gridViewStoreHistory.Name = "gridViewStoreHistory";
            this.gridViewStoreHistory.OptionsBehavior.ReadOnly = true;
            this.gridViewStoreHistory.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnStoreHistoryHigh
            // 
            this.gridColumnStoreHistoryHigh.Caption = "Өндөр";
            this.gridColumnStoreHistoryHigh.FieldName = "high";
            this.gridColumnStoreHistoryHigh.Name = "gridColumnStoreHistoryHigh";
            this.gridColumnStoreHistoryHigh.Visible = true;
            this.gridColumnStoreHistoryHigh.VisibleIndex = 0;
            // 
            // gridColumnStoreHistoryWeight
            // 
            this.gridColumnStoreHistoryWeight.Caption = "Жин";
            this.gridColumnStoreHistoryWeight.FieldName = "weight";
            this.gridColumnStoreHistoryWeight.Name = "gridColumnStoreHistoryWeight";
            this.gridColumnStoreHistoryWeight.Visible = true;
            this.gridColumnStoreHistoryWeight.VisibleIndex = 1;
            // 
            // gridColumnStoreHistoryBodyIndex
            // 
            this.gridColumnStoreHistoryBodyIndex.Caption = "Биеийн индекс";
            this.gridColumnStoreHistoryBodyIndex.FieldName = "bodyIndex";
            this.gridColumnStoreHistoryBodyIndex.Name = "gridColumnStoreHistoryBodyIndex";
            this.gridColumnStoreHistoryBodyIndex.Visible = true;
            this.gridColumnStoreHistoryBodyIndex.VisibleIndex = 2;
            // 
            // gridColumnStoreHistoryPain
            // 
            this.gridColumnStoreHistoryPain.Caption = "Хэвтэх үеийн зовиур";
            this.gridColumnStoreHistoryPain.FieldName = "pain";
            this.gridColumnStoreHistoryPain.Name = "gridColumnStoreHistoryPain";
            this.gridColumnStoreHistoryPain.Visible = true;
            this.gridColumnStoreHistoryPain.VisibleIndex = 3;
            // 
            // gridColumnStoreHistoryHistory
            // 
            this.gridColumnStoreHistoryHistory.Caption = "Өвчний түүх";
            this.gridColumnStoreHistoryHistory.FieldName = "history";
            this.gridColumnStoreHistoryHistory.Name = "gridColumnStoreHistoryHistory";
            this.gridColumnStoreHistoryHistory.Visible = true;
            this.gridColumnStoreHistoryHistory.VisibleIndex = 5;
            // 
            // gridColumnStoreHistorylifeStore
            // 
            this.gridColumnStoreHistorylifeStore.Caption = "Амьдралын түүх";
            this.gridColumnStoreHistorylifeStore.FieldName = "lifeStore";
            this.gridColumnStoreHistorylifeStore.Name = "gridColumnStoreHistorylifeStore";
            this.gridColumnStoreHistorylifeStore.Visible = true;
            this.gridColumnStoreHistorylifeStore.VisibleIndex = 4;
            // 
            // gridColumnStoreHistoryLifeTerm
            // 
            this.gridColumnStoreHistoryLifeTerm.Caption = "Ахуйн нөхцөл";
            this.gridColumnStoreHistoryLifeTerm.FieldName = "lifeTermText";
            this.gridColumnStoreHistoryLifeTerm.Name = "gridColumnStoreHistoryLifeTerm";
            this.gridColumnStoreHistoryLifeTerm.Visible = true;
            this.gridColumnStoreHistoryLifeTerm.VisibleIndex = 6;
            // 
            // gridColumnStoreHistoryWorkTerm
            // 
            this.gridColumnStoreHistoryWorkTerm.Caption = "Ажил хөдөлмөрийн нөхцөл";
            this.gridColumnStoreHistoryWorkTerm.FieldName = "workTermText";
            this.gridColumnStoreHistoryWorkTerm.Name = "gridColumnStoreHistoryWorkTerm";
            this.gridColumnStoreHistoryWorkTerm.Visible = true;
            this.gridColumnStoreHistoryWorkTerm.VisibleIndex = 7;
            // 
            // gridViewGeneral
            // 
            this.gridViewGeneral.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnGeneralCondition,
            this.gridColumnGeneralMind,
            this.gridColumnGeneralSurround,
            this.gridColumnGeneralPosition});
            this.gridViewGeneral.GridControl = this.gridControlStore;
            this.gridViewGeneral.Name = "gridViewGeneral";
            this.gridViewGeneral.OptionsBehavior.ReadOnly = true;
            this.gridViewGeneral.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnGeneralCondition
            // 
            this.gridColumnGeneralCondition.Caption = "Ерөнхий байдал";
            this.gridColumnGeneralCondition.FieldName = "generalConditionText";
            this.gridColumnGeneralCondition.Name = "gridColumnGeneralCondition";
            this.gridColumnGeneralCondition.Visible = true;
            this.gridColumnGeneralCondition.VisibleIndex = 0;
            // 
            // gridColumnGeneralMind
            // 
            this.gridColumnGeneralMind.Caption = "Ухаан санаа";
            this.gridColumnGeneralMind.FieldName = "mindText";
            this.gridColumnGeneralMind.Name = "gridColumnGeneralMind";
            this.gridColumnGeneralMind.Visible = true;
            this.gridColumnGeneralMind.VisibleIndex = 1;
            // 
            // gridColumnGeneralSurround
            // 
            this.gridColumnGeneralSurround.Caption = "Орчиндоо";
            this.gridColumnGeneralSurround.FieldName = "surroundText";
            this.gridColumnGeneralSurround.Name = "gridColumnGeneralSurround";
            this.gridColumnGeneralSurround.Visible = true;
            this.gridColumnGeneralSurround.VisibleIndex = 2;
            // 
            // gridColumnGeneralPosition
            // 
            this.gridColumnGeneralPosition.Caption = "Байрлал";
            this.gridColumnGeneralPosition.FieldName = "positionText";
            this.gridColumnGeneralPosition.Name = "gridColumnGeneralPosition";
            this.gridColumnGeneralPosition.Visible = true;
            this.gridColumnGeneralPosition.VisibleIndex = 3;
            // 
            // gridViewInternal
            // 
            this.gridViewInternal.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnBronkhofoni,
            this.gridColumnSoundVib,
            this.gridColumnChestShape,
            this.gridColumnBreathShape});
            this.gridViewInternal.GridControl = this.gridControlStore;
            this.gridViewInternal.Name = "gridViewInternal";
            this.gridViewInternal.OptionsBehavior.ReadOnly = true;
            this.gridViewInternal.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnBronkhofoni
            // 
            this.gridColumnBronkhofoni.Caption = "Бронхофони";
            this.gridColumnBronkhofoni.FieldName = "bronkhofoniText";
            this.gridColumnBronkhofoni.Name = "gridColumnBronkhofoni";
            this.gridColumnBronkhofoni.Visible = true;
            this.gridColumnBronkhofoni.VisibleIndex = 0;
            // 
            // gridColumnSoundVib
            // 
            this.gridColumnSoundVib.Caption = "Дууны доргион";
            this.gridColumnSoundVib.FieldName = "soundVibText";
            this.gridColumnSoundVib.Name = "gridColumnSoundVib";
            this.gridColumnSoundVib.Visible = true;
            this.gridColumnSoundVib.VisibleIndex = 1;
            // 
            // gridColumnChestShape
            // 
            this.gridColumnChestShape.Caption = "Амьсгалын тоо";
            this.gridColumnChestShape.FieldName = "breathCount";
            this.gridColumnChestShape.Name = "gridColumnChestShape";
            this.gridColumnChestShape.Visible = true;
            this.gridColumnChestShape.VisibleIndex = 2;
            // 
            // gridColumnBreathShape
            // 
            this.gridColumnBreathShape.Caption = "Амьсгалын хэлбэр";
            this.gridColumnBreathShape.FieldName = "breathShapeText";
            this.gridColumnBreathShape.Name = "gridColumnBreathShape";
            this.gridColumnBreathShape.Visible = true;
            this.gridColumnBreathShape.VisibleIndex = 3;
            // 
            // gridViewHeart
            // 
            this.gridViewHeart.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21});
            this.gridViewHeart.GridControl = this.gridControlStore;
            this.gridViewHeart.Name = "gridViewHeart";
            this.gridViewHeart.OptionsBehavior.ReadOnly = true;
            this.gridViewHeart.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Зүрх судасны эрсдэлт хүчин зүйлс";
            this.gridColumn19.FieldName = "heartRiskText";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 0;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Хүч";
            this.gridColumn20.FieldName = "strongText";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 1;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Хүчдэл";
            this.gridColumn21.FieldName = "voltageText";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 2;
            // 
            // gridViewDigestive
            // 
            this.gridViewDigestive.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26});
            this.gridViewDigestive.GridControl = this.gridControlStore;
            this.gridViewDigestive.Name = "gridViewDigestive";
            this.gridViewDigestive.OptionsBehavior.ReadOnly = true;
            this.gridViewDigestive.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Элэгний шинж тэмдгүүд";
            this.gridColumn23.FieldName = "liverText";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 0;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Хэлбэр";
            this.gridColumn24.FieldName = "tongueShape";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 1;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Ихэссэн хэсэгт тогшилтын дуу";
            this.gridColumn25.FieldName = "isPercussionText";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 2;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Гэдэсний гүрвэлзэх хөдөлгөөн";
            this.gridColumn26.FieldName = "isMovingText";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 3;
            // 
            // gridViewUrine
            // 
            this.gridViewUrine.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30});
            this.gridViewUrine.GridControl = this.gridControlStore;
            this.gridViewUrine.Name = "gridViewUrine";
            this.gridViewUrine.OptionsBehavior.ReadOnly = true;
            this.gridViewUrine.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Хонгийн шээсний гарц";
            this.gridColumn27.FieldName = "urineOutputText";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 0;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Шээсний өнгө";
            this.gridColumn28.FieldName = "urineColorText";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 1;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Үнэрлэх мэдрэмж";
            this.gridColumn29.FieldName = "smellText";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 2;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Сонсголын мэдрэмж";
            this.gridColumn30.FieldName = "hearText";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 3;
            // 
            // gridViewStoreLeft
            // 
            this.gridViewStoreLeft.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn10,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn11,
            this.gridColumn13,
            this.gridColumn12,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17});
            this.gridViewStoreLeft.GridControl = this.gridControlStore;
            this.gridViewStoreLeft.Name = "gridViewStoreLeft";
            this.gridViewStoreLeft.OptionsBehavior.ReadOnly = true;
            this.gridViewStoreLeft.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Хаагдсан огноо";
            this.gridColumn7.FieldName = "endDate";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Үргэлжилсэн хугацаа";
            this.gridColumn10.FieldName = "duration";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 1;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Гарах шилжих үеийн онош";
            this.gridColumn8.FieldName = "diagnoseTypeName";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Өвчний төгсгөл";
            this.gridColumn9.FieldName = "endHurtText";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 3;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Эмчилгээний зардал";
            this.gridColumn11.FieldName = "bill";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 4;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Эмнэлгээс";
            this.gridColumn13.FieldName = "leaveHospitalText";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 5;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Эмчилгээ";
            this.gridColumn12.FieldName = "treatText";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 6;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Ирэх үед";
            this.gridColumn14.FieldName = "inComeText";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 7;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Гарах үед";
            this.gridColumn15.FieldName = "inBackText";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 8;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Эмнэл зүйн";
            this.gridColumn16.FieldName = "analysisTreatText";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 9;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Гэрлийн";
            this.gridColumn17.FieldName = "analysisLightText";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 10;
            // 
            // gridViewValidity
            // 
            this.gridViewValidity.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34});
            this.gridViewValidity.GridControl = this.gridControlStore;
            this.gridViewValidity.Name = "gridViewValidity";
            this.gridViewValidity.OptionsBehavior.ReadOnly = true;
            this.gridViewValidity.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Огноо";
            this.gridColumn31.FieldName = "date";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 0;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Оношийн ангилал";
            this.gridColumn32.FieldName = "diagnoseTypeText";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 1;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Оношийн үндэслэл";
            this.gridColumn33.FieldName = "description";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 2;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Эмчийн нэр";
            this.gridColumn34.FieldName = "fullName";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 3;
            // 
            // gridViewLeftDiagnose
            // 
            this.gridViewLeftDiagnose.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnLeftDiagnoseDoctorName,
            this.gridColumnLeftDiagnoseTypeName,
            this.gridColumnLeftDiagnoseDesc,
            this.gridColumnLeftDiagnoseCode});
            this.gridViewLeftDiagnose.GridControl = this.gridControlStore;
            this.gridViewLeftDiagnose.Name = "gridViewLeftDiagnose";
            this.gridViewLeftDiagnose.OptionsBehavior.ReadOnly = true;
            this.gridViewLeftDiagnose.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnLeftDiagnoseDoctorName
            // 
            this.gridColumnLeftDiagnoseDoctorName.Caption = "Эмчийн нэр";
            this.gridColumnLeftDiagnoseDoctorName.CustomizationCaption = "Эмчийн нэр";
            this.gridColumnLeftDiagnoseDoctorName.FieldName = "fullName";
            this.gridColumnLeftDiagnoseDoctorName.Name = "gridColumnLeftDiagnoseDoctorName";
            this.gridColumnLeftDiagnoseDoctorName.OptionsColumn.ReadOnly = true;
            this.gridColumnLeftDiagnoseDoctorName.Visible = true;
            this.gridColumnLeftDiagnoseDoctorName.VisibleIndex = 0;
            // 
            // gridColumnLeftDiagnoseTypeName
            // 
            this.gridColumnLeftDiagnoseTypeName.Caption = "Оношийн ангилал";
            this.gridColumnLeftDiagnoseTypeName.CustomizationCaption = "Оношийн ангилал";
            this.gridColumnLeftDiagnoseTypeName.FieldName = "diagnoseTypeName";
            this.gridColumnLeftDiagnoseTypeName.Name = "gridColumnLeftDiagnoseTypeName";
            this.gridColumnLeftDiagnoseTypeName.OptionsColumn.ReadOnly = true;
            this.gridColumnLeftDiagnoseTypeName.Visible = true;
            this.gridColumnLeftDiagnoseTypeName.VisibleIndex = 1;
            // 
            // gridColumnLeftDiagnoseDesc
            // 
            this.gridColumnLeftDiagnoseDesc.Caption = "Тайлбар";
            this.gridColumnLeftDiagnoseDesc.FieldName = "description";
            this.gridColumnLeftDiagnoseDesc.Name = "gridColumnLeftDiagnoseDesc";
            this.gridColumnLeftDiagnoseDesc.OptionsColumn.ReadOnly = true;
            this.gridColumnLeftDiagnoseDesc.Visible = true;
            this.gridColumnLeftDiagnoseDesc.VisibleIndex = 2;
            // 
            // gridColumnLeftDiagnoseCode
            // 
            this.gridColumnLeftDiagnoseCode.Caption = "Оношийн код";
            this.gridColumnLeftDiagnoseCode.FieldName = "codeText";
            this.gridColumnLeftDiagnoseCode.Name = "gridColumnLeftDiagnoseCode";
            this.gridColumnLeftDiagnoseCode.OptionsColumn.ReadOnly = true;
            this.gridColumnLeftDiagnoseCode.Visible = true;
            this.gridColumnLeftDiagnoseCode.VisibleIndex = 3;
            // 
            // gridViewStore
            // 
            this.gridViewStore.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnStoreNumber,
            this.gridColumnLastName,
            this.gridColumnWardName,
            this.gridColumnFirstName,
            this.gridColumnRegister,
            this.gridColumnBirthdate,
            this.gridColumnGender,
            this.gridColumnBeginDate,
            this.gridColumnEndDate,
            this.gridColumn18,
            this.gridColumnActive});
            this.gridViewStore.GridControl = this.gridControlStore;
            this.gridViewStore.Name = "gridViewStore";
            this.gridViewStore.OptionsBehavior.ReadOnly = true;
            this.gridViewStore.OptionsView.ShowAutoFilterRow = true;
            this.gridViewStore.OptionsView.ShowGroupPanel = false;
            this.gridViewStore.OptionsView.ShowPreview = true;
            this.gridViewStore.PreviewFieldName = "emergencyText";
            this.gridViewStore.MasterRowGetRelationDisplayCaption += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridViewStore_MasterRowGetRelationDisplayCaption);
            // 
            // gridColumnStoreNumber
            // 
            this.gridColumnStoreNumber.Caption = "Дугаар";
            this.gridColumnStoreNumber.FieldName = "number";
            this.gridColumnStoreNumber.Name = "gridColumnStoreNumber";
            this.gridColumnStoreNumber.OptionsColumn.ReadOnly = true;
            this.gridColumnStoreNumber.Visible = true;
            this.gridColumnStoreNumber.VisibleIndex = 0;
            this.gridColumnStoreNumber.Width = 51;
            // 
            // gridColumnLastName
            // 
            this.gridColumnLastName.Caption = "Овог";
            this.gridColumnLastName.FieldName = "lastName";
            this.gridColumnLastName.Name = "gridColumnLastName";
            this.gridColumnLastName.OptionsColumn.ReadOnly = true;
            this.gridColumnLastName.Visible = true;
            this.gridColumnLastName.VisibleIndex = 4;
            this.gridColumnLastName.Width = 73;
            // 
            // gridColumnWardName
            // 
            this.gridColumnWardName.Caption = "Тасгийн нэр";
            this.gridColumnWardName.FieldName = "wardName";
            this.gridColumnWardName.Name = "gridColumnWardName";
            this.gridColumnWardName.OptionsColumn.ReadOnly = true;
            this.gridColumnWardName.Visible = true;
            this.gridColumnWardName.VisibleIndex = 3;
            this.gridColumnWardName.Width = 124;
            // 
            // gridColumnFirstName
            // 
            this.gridColumnFirstName.Caption = "Нэр";
            this.gridColumnFirstName.FieldName = "firstName";
            this.gridColumnFirstName.Name = "gridColumnFirstName";
            this.gridColumnFirstName.OptionsColumn.ReadOnly = true;
            this.gridColumnFirstName.Visible = true;
            this.gridColumnFirstName.VisibleIndex = 5;
            this.gridColumnFirstName.Width = 69;
            // 
            // gridColumnRegister
            // 
            this.gridColumnRegister.Caption = "Регистер";
            this.gridColumnRegister.FieldName = "patientID";
            this.gridColumnRegister.Name = "gridColumnRegister";
            this.gridColumnRegister.OptionsColumn.ReadOnly = true;
            this.gridColumnRegister.Visible = true;
            this.gridColumnRegister.VisibleIndex = 6;
            this.gridColumnRegister.Width = 66;
            // 
            // gridColumnBirthdate
            // 
            this.gridColumnBirthdate.Caption = "Төрсөн огноо";
            this.gridColumnBirthdate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnBirthdate.FieldName = "birthday";
            this.gridColumnBirthdate.Name = "gridColumnBirthdate";
            this.gridColumnBirthdate.OptionsColumn.ReadOnly = true;
            this.gridColumnBirthdate.Visible = true;
            this.gridColumnBirthdate.VisibleIndex = 7;
            this.gridColumnBirthdate.Width = 56;
            // 
            // gridColumnGender
            // 
            this.gridColumnGender.Caption = "Хүйс";
            this.gridColumnGender.FieldName = "genderText";
            this.gridColumnGender.Name = "gridColumnGender";
            this.gridColumnGender.OptionsColumn.ReadOnly = true;
            this.gridColumnGender.Visible = true;
            this.gridColumnGender.VisibleIndex = 8;
            this.gridColumnGender.Width = 42;
            // 
            // gridColumnBeginDate
            // 
            this.gridColumnBeginDate.Caption = "Нээсэн огноо";
            this.gridColumnBeginDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnBeginDate.FieldName = "beginDate";
            this.gridColumnBeginDate.Name = "gridColumnBeginDate";
            this.gridColumnBeginDate.OptionsColumn.ReadOnly = true;
            this.gridColumnBeginDate.Visible = true;
            this.gridColumnBeginDate.VisibleIndex = 1;
            this.gridColumnBeginDate.Width = 56;
            // 
            // gridColumnEndDate
            // 
            this.gridColumnEndDate.Caption = "Хаасан огноо";
            this.gridColumnEndDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnEndDate.FieldName = "endDate";
            this.gridColumnEndDate.Name = "gridColumnEndDate";
            this.gridColumnEndDate.OptionsColumn.ReadOnly = true;
            this.gridColumnEndDate.Visible = true;
            this.gridColumnEndDate.VisibleIndex = 2;
            this.gridColumnEndDate.Width = 54;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Хаяг";
            this.gridColumn18.FieldName = "provinceName";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 9;
            this.gridColumn18.Width = 97;
            // 
            // gridColumnActive
            // 
            this.gridColumnActive.Caption = "Төлөв";
            this.gridColumnActive.FieldName = "isCloseText";
            this.gridColumnActive.Name = "gridColumnActive";
            // 
            // gridColumnDescription
            // 
            this.gridColumnDescription.Name = "gridColumnDescription";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonLayout);
            this.panelControl1.Controls.Add(this.dropDownButtonPrint);
            this.panelControl1.Controls.Add(this.dateEditEnd);
            this.panelControl1.Controls.Add(this.dateEditStart);
            this.panelControl1.Controls.Add(this.radioGroupIsUse);
            this.panelControl1.Controls.Add(this.simpleButtonReloadStore);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 48);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButtonLayout
            // 
            this.simpleButtonLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonLayout.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonLayout.Image")));
            this.simpleButtonLayout.Location = new System.Drawing.Point(749, 12);
            this.simpleButtonLayout.Name = "simpleButtonLayout";
            this.simpleButtonLayout.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonLayout.TabIndex = 11;
            this.simpleButtonLayout.ToolTip = "Хүснэгтийн харагдах байдлыг хадгалах";
            this.simpleButtonLayout.Click += new System.EventHandler(this.simpleButtonLayout_Click);
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.DropDownControl = this.popupMenu;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(613, 12);
            this.dropDownButtonPrint.MenuManager = this.barManager;
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.dropDownButtonPrint.Size = new System.Drawing.Size(130, 23);
            this.dropDownButtonPrint.TabIndex = 10;
            this.dropDownButtonPrint.Text = "Хэвлэх";
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemMot),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemEndBal),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "Хэвлэх";
            this.barButtonItemPrint.Description = "Шилжүүлгийн тайлан хэвлэх";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 0;
            this.barButtonItemPrint.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.LargeGlyph")));
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemMot
            // 
            this.barButtonItemMot.Caption = "Гүйлгээ хэвлэх";
            this.barButtonItemMot.Description = "Шилжүүлгийн гүйлгээ хэвлэх";
            this.barButtonItemMot.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemMot.Glyph")));
            this.barButtonItemMot.Id = 1;
            this.barButtonItemMot.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemMot.LargeGlyph")));
            this.barButtonItemMot.Name = "barButtonItemMot";
            this.barButtonItemMot.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemEndBal
            // 
            this.barButtonItemEndBal.Caption = "Эцсийн үлдэгдэл";
            this.barButtonItemEndBal.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.Glyph")));
            this.barButtonItemEndBal.Id = 2;
            this.barButtonItemEndBal.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEndBal.LargeGlyph")));
            this.barButtonItemEndBal.Name = "barButtonItemEndBal";
            this.barButtonItemEndBal.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemEndBal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEndBal_ItemClick);
            // 
            // barButtonItemConvert
            // 
            this.barButtonItemConvert.Caption = "Хөрвүүлэх";
            this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
            this.barButtonItemConvert.Id = 3;
            this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
            this.barButtonItemConvert.Name = "barButtonItemConvert";
            this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemPrint,
            this.barButtonItemMot,
            this.barButtonItemEndBal,
            this.barButtonItemConvert});
            this.barManager.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 561);
            this.barDockControlBottom.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 561);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(784, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 561);
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(118, 14);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditEnd.Size = new System.Drawing.Size(100, 20);
            this.dateEditEnd.TabIndex = 9;
            this.dateEditEnd.ToolTip = "Дуусах огноо";
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(12, 14);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditStart.Size = new System.Drawing.Size(100, 20);
            this.dateEditStart.TabIndex = 8;
            this.dateEditStart.ToolTip = "Эхлэх огноо";
            // 
            // radioGroupIsUse
            // 
            this.radioGroupIsUse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroupIsUse.Location = new System.Drawing.Point(322, 12);
            this.radioGroupIsUse.Name = "radioGroupIsUse";
            this.radioGroupIsUse.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Хаагдаагүй", "Хаагдаагүй", true, "Хаагдаагүй"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("Хаагдсан", "Хаагдсан", true, "Хаагдсан"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("all", "Бүгдийг харах", true, "all")});
            this.radioGroupIsUse.Size = new System.Drawing.Size(285, 23);
            this.radioGroupIsUse.TabIndex = 6;
            this.radioGroupIsUse.SelectedIndexChanged += new System.EventHandler(this.radioGroupIsUse_SelectedIndexChanged);
            // 
            // simpleButtonReloadStore
            // 
            this.simpleButtonReloadStore.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReloadStore.Image")));
            this.simpleButtonReloadStore.Location = new System.Drawing.Point(224, 12);
            this.simpleButtonReloadStore.Name = "simpleButtonReloadStore";
            this.simpleButtonReloadStore.Size = new System.Drawing.Size(65, 23);
            this.simpleButtonReloadStore.TabIndex = 5;
            this.simpleButtonReloadStore.Text = "Шүүх";
            this.simpleButtonReloadStore.ToolTip = "Өвчний түүх шүүх";
            this.simpleButtonReloadStore.Click += new System.EventHandler(this.simpleButtonReloadStore_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControlStore);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 48);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(784, 513);
            this.panelControl2.TabIndex = 1;
            // 
            // StoreForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "StoreForm";
            this.ShowInTaskbar = false;
            this.Text = "Өвчний түүх";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StoreForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWardRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDiagnose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTreatment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStoreHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewGeneral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInternal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHeart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDigestive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUrine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStoreLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewValidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLeftDiagnose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupIsUse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReloadStore;
        private DevExpress.XtraEditors.RadioGroup radioGroupIsUse;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDescription;
        private DevExpress.XtraGrid.GridControl gridControlStore;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInspection;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInspectionDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInspection;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInspectionDescription;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStore;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreNumber;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLastName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnFirstName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRegister;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBirthdate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGender;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnWardName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBeginDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEndDate;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTreatment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTreatmentMedicine;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTreatmentStarDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTreatmentEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTreatmentDuration;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTreatmentEachDay;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTreatmentDescription;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDiagnose;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDiagnosetype;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDiagnose;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDiagnoseDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDiagnoseDate;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStoreLeft;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWardRoom;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnInspectionDoctorName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDiagnoseDoctorName;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewStoreHistory;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreHistoryHigh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreHistoryWeight;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreHistoryBodyIndex;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreHistoryPain;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreHistorylifeStore;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreHistoryHistory;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreHistoryWorkTerm;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStoreHistoryLifeTerm;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewGeneral;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGeneralCondition;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGeneralMind;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGeneralSurround;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGeneralPosition;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInternal;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBronkhofoni;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSoundVib;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnChestShape;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBreathShape;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHeart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDigestive;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewUrine;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;


        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemMot;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEndBal;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewValidity;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnActive;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLeftDiagnose;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLeftDiagnoseDoctorName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLeftDiagnoseTypeName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLeftDiagnoseDesc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLeftDiagnoseCode;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLayout;

    }
}