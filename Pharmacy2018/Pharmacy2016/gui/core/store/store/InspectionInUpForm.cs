﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;

namespace Pharmacy2016.gui.core.store.store
{
    /*
     * Үзлэг нэмэх, засах цонх.
     * 
     */

    public partial class InspectionInUpForm : XtraForm
    {

        #region Форм анх ачааллах функц

            private static InspectionInUpForm INSTANCE = new InspectionInUpForm();

            public static InspectionInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            public DataRowView currentTreatmentView;

            private InspectionInUpForm()
            {

            }
            
            
            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                initBinding();
                reload();
            }

            private void initDataSource()
            {
                gridControlTreatment.DataSource = StoreDataSet.StoreTreatmentBindingSource;
                gridLookUpEditDoctor.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
            }

            private void initBinding()
            {
                dateEditInspectionDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInspectionBindingSource, "date", true));
                gridLookUpEditDoctor.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInspectionBindingSource, "userID", true));
                memoEditInspection.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInspectionBindingSource, "inspection", true));
                memoEditInspectionDescription.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInspectionBindingSource, "description", true));
                checkEditIsBreak.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInspectionBindingSource, "isBreak", true));
                dateEditBreakDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreInspectionBindingSource, "breakDate", true));
            }

            private void initError()
            {
                gridLookUpEditDoctor.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                memoEditInspection.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditInspectionDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insertInspection();
                    }
                    else if (actionType == 2)
                    {
                        updateInspection();
                    }

                    ShowDialog();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insertInspection()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    StoreInUpForm.Instance.currentInspectionView["userID"] = UserUtil.getUserId();
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                    gridLookUpEditDoctor.EditValue = UserUtil.getUserId();
                }
                DateTime now = DateTime.Now;
                StoreInUpForm.Instance.currentInspectionView["date"] = now;
                dateEditInspectionDate.DateTime = now;
                setGridEdit(true);
            }

            private void updateInspection()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                }
                setGridEdit(!Convert.ToBoolean(StoreInUpForm.Instance.currentInspectionView["isBreak"]));
            }

            private void setGridEdit(bool visible)
            {
                gridControlTreatment.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = visible;
                gridControlTreatment.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = visible;
                gridControlTreatment.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = visible;
            }

            
            private void clearData()
            {
                clearErrorText();
                dateEditBreakDate.Properties.ReadOnly = true;
                
                InformationDataSet.SystemUserBindingSource.Filter = "";
                StoreDataSet.StoreInspectionBindingSource.CancelEdit();
            }

            private void clearErrorText()
            {
                memoEditInspection.ErrorText = "";
                dateEditInspectionDate.ErrorText = "";
                gridLookUpEditDoctor.ErrorText = "";
            }

            private void InspectionInUpForm_Shown(object sender, EventArgs e)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    dateEditInspectionDate.Focus();
                }
                else
                {
                    gridLookUpEditDoctor.Focus();
                }
            }

        #endregion

        #region Формын event

            private void gridControlTreatment_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addTreatment();
                    }    
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewTreatment.GetFocusedRow() != null)
                    {
                        deleteTreatment();
                    }
                    else if (String.Equals(e.Button.Tag, "edit") && gridViewTreatment.GetFocusedRow() != null)
                    {
                        editTreatment();
                    }
                }
            }

            private void checkEditIsBreak_CheckedChanged(object sender, EventArgs e)
            {
                dateEditBreakDate.Properties.ReadOnly = !checkEditIsBreak.Checked;
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void simpleButtonSaveInspection_Click(object sender, EventArgs e)
            {
                saveInspection();
            }

        #endregion

        #region Эмчилгээ нэмж, засах, устгах функцууд.

            private void addTreatment()
            {
                StoreDataSet.Instance.StoreTreatment.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                StoreDataSet.Instance.StoreTreatment.storeIDColumn.DefaultValue = StoreForm.Instance.currentStoreView["id"];
                currentTreatmentView = (DataRowView)StoreDataSet.StoreTreatmentBindingSource.AddNew();
                TreatmentInUpForm.Instance.showForm(1);

                //gridViewTreatment.RefreshData();
            }

            private void editTreatment()
            {
                currentTreatmentView = (DataRowView)StoreDataSet.StoreTreatmentBindingSource.Current;
                TreatmentInUpForm.Instance.showForm(2);
                //gridViewTreatment.RefreshData();
            }

            private void deleteTreatment()
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Үзлэгийг устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    gridViewTreatment.DeleteSelectedRows();
                }
            }

        #endregion     
        
        #region Хадгалах, ачааллах функц

            private void reload()
            {
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.DOCTOR);
            }

            private bool checkInspection()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (gridLookUpEditDoctor.EditValue == DBNull.Value || gridLookUpEditDoctor.EditValue == null || gridLookUpEditDoctor.EditValue.ToString().Equals(""))
                {
                    errorText = "Үзлэг хийх эмчийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditDoctor.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditDoctor.Focus();
                }
                if (dateEditInspectionDate.EditValue == DBNull.Value || dateEditInspectionDate.EditValue == null || dateEditInspectionDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditInspectionDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditInspectionDate.Focus();
                    }
                }
                if (memoEditInspection.EditValue == DBNull.Value || memoEditInspection.EditValue == null || (memoEditInspection.EditValue = memoEditInspection.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Үзлэгийн тэмдэглэл оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    memoEditInspection.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        memoEditInspection.Focus();
                    }
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            public void saveInspection() 
            {
                if (checkInspection()) 
                {
                    StoreInUpForm.Instance.currentInspectionView["fullName"] = gridLookUpEditDoctor.Text;

                    StoreDataSet.StoreTreatmentBindingSource.EndEdit();
                    StoreInUpForm.Instance.currentInspectionView.EndEdit();
                    StoreDataSet.StoreInspectionBindingSource.EndEdit();
                    
                    Close();
                }         
            }

        #endregion

    }
}