﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.store.ds;

namespace Pharmacy2016.gui.core.store.store
{
    /*
     * Онош нэмэх, засах цонх.
     * 
     */

    public partial class DiagnoseInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static DiagnoseInUpForm INSTANCE = new DiagnoseInUpForm();

            public static DiagnoseInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private DiagnoseInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                initBinding();
                reload();
            }

            private void initDataSource()
            {
                gridLookUpEditDoctor.Properties.DataSource = InformationDataSet.SystemUserBindingSource;
                gridLookUpEditType.Properties.DataSource = StoreDataSet.Instance.DiagnoseType;
            }

            private void initBinding()
            {
                gridLookUpEditDoctor.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDiagnoseBindingSource, "userID", true));
                gridLookUpEditType.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDiagnoseBindingSource, "diagnoseType", true));
                memoEditDiagnose.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDiagnoseBindingSource, "diagnose", true));
                memoEditDiagnoseDescription.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDiagnoseBindingSource, "description", true));
                dateEditDiagnoseDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreDiagnoseBindingSource, "date", true));
            }

            private void initError()
            {
                gridLookUpEditDoctor.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                memoEditDiagnose.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditDiagnoseDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insertDiagnose();
                    }
                    else if (actionType == 2)
                    {
                        updateDiagnose();
                    }

                    ShowDialog();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insertDiagnose()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    StoreInUpForm.Instance.currentDiagnoseView["userID"] = UserUtil.getUserId();
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                    gridLookUpEditDoctor.EditValue = UserUtil.getUserId();
                }
                DateTime now = DateTime.Now;
                StoreInUpForm.Instance.currentDiagnoseView["date"] = now;
                dateEditDiagnoseDate.DateTime = now;
            }

            private void updateDiagnose()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                }
            }


            private void clearData()
            {
                clearErrorText();
                //clearBinding();
                InformationDataSet.SystemUserBindingSource.Filter = "";
                StoreDataSet.StoreDiagnoseBindingSource.CancelEdit();
            }

            private void clearErrorText()
            {
                gridLookUpEditDoctor.ErrorText = "";
                gridLookUpEditType.ErrorText = "";
                memoEditDiagnose.ErrorText = "";
                dateEditDiagnoseDate.ErrorText = "";
            }

            private void clearBinding()
            {
                //gridLookUpEditDiagnoseType.DataBindings.Clear();
                memoEditDiagnose.DataBindings.Clear();
                dateEditDiagnoseDate.DataBindings.Clear();
                memoEditDiagnoseDescription.DataBindings.Clear();
            }

            private void DiagnoseInUpForm_Shown(object sender, EventArgs e)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    gridLookUpEditType.Focus();
                }
                else
                {
                    gridLookUpEditDoctor.Focus();
                }
            }
       
        #endregion

        #region Формын event

            private void simpleButtonSaveDiagnose_Click(object sender, EventArgs e)
            {
                saveDiagnose();
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

        #endregion

        #region Формын функц

            private void reload()
            {
                InformationDataSet.SystemUserTableAdapter.Fill(InformationDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.DOCTOR);
            }

            private bool checkDiagnose()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (gridLookUpEditDoctor.EditValue == DBNull.Value || gridLookUpEditDoctor.EditValue == null || gridLookUpEditDoctor.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмчийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditDoctor.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditDoctor.Focus();
                }
                if (gridLookUpEditType.EditValue == DBNull.Value || gridLookUpEditType.EditValue == null || gridLookUpEditType.EditValue.ToString().Equals(""))
                {
                    errorText = "Ангилалыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditType.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditType.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (dateEditDiagnoseDate.EditValue == DBNull.Value || dateEditDiagnoseDate.EditValue == null || dateEditDiagnoseDate.Text == "")
                {
                    errorText = "Огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditDiagnoseDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditDiagnoseDate.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (memoEditDiagnose.EditValue == DBNull.Value || memoEditDiagnose.EditValue == null || (memoEditDiagnose.EditValue = memoEditDiagnose.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Онош оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    memoEditDiagnose.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        memoEditDiagnose.Focus();
                        //text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                memoEditDiagnoseDescription.Text = memoEditDiagnoseDescription.Text.Trim();

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void saveDiagnose() 
            {
                if (checkDiagnose()) 
                {
                    StoreInUpForm.Instance.currentDiagnoseView["fullName"] = gridLookUpEditDoctor.Text;

                    StoreDataSet.StoreDiagnoseBindingSource.EndEdit();
                    StoreInUpForm.Instance.currentDiagnoseView.EndEdit();
                    Close();
                }
            }

        #endregion

    }
}