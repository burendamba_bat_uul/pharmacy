﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using DevExpress.XtraGrid.Columns;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.store.order;
using Pharmacy2016.gui.report.store.store;

namespace Pharmacy2016.gui.core.store.store
{
    /*
     * Эмчлүүлэгчийн өвчний түүх харах, нэмэх, засах, устгах цонх.
     * 
     */

    public partial class StoreForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм үүсгэх функцууд.

            private static StoreForm INSTANCE = new StoreForm();

            public static StoreForm Instance
            {
                get 
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 1002;

            private bool isInit = false;

            public DataRowView currentStoreView;

            private int actionType;

            private System.IO.Stream layout_store = null;

            private StoreForm()
            {
            
            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initLayout();
                RoleUtil.addForm(FORM_ID, this);

                gridControlStore.DataSource = StoreDataSet.StoreBindingSource;

                dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleLeft;
                DateTime date = DateTime.Now;
                dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1);
                dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            }

            private void initLayout()
            {
                layout_store = new System.IO.MemoryStream();
                gridViewStore.SaveLayoutToStream(layout_store);
                layout_store.Seek(0, System.IO.SeekOrigin.Begin);
            }

        #endregion

        #region Форм нээх, хаах функц.

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        radioGroupIsUse.SelectedIndex = 0;
                        checkRole();

                        StoreDataSet.Instance.Store.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.Store.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreDiagnose.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreDiagnose.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreTreatment.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreTreatment.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreInspection.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreInspection.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreWardRoom.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreWardRoom.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreLeft.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreLeft.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreHistory.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreHistory.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreGeneralinspection.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreGeneralinspection.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreInternalinspection.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreInternalinspection.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreHeart.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreHeart.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreDigestive.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreDigestive.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreUrine.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreUrine.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreValidity.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreValidity.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();

                        StoreDataSet.Instance.StoreLeftDiagnose.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        StoreDataSet.Instance.StoreLeftDiagnose.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        reload();

                        Show();
                    }
                    if (type > 0)
                    {
                        dateEditStart.DateTime = OrderForm.Instance.getStartDate();
                        dateEditEnd.DateTime = OrderForm.Instance.getEndDate();
                        addStore();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                if (radioGroupIsUse.SelectedIndex == 0)
                {
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = !SystemUtil.SELECTED_DB_READONLY && false;

                    dropDownButtonPrint.Visible = roles[4];
                }
                else if (radioGroupIsUse.SelectedIndex == 1)
                {
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];

                    dropDownButtonPrint.Visible = roles[4];
                }

                dropDownButtonPrint.Visible = roles[4];
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;

                checkLayout();
            }

            private void checkLayout()
            {
                string layout = UserUtil.getLayout("store");
                if (layout != null)
                {
                    gridViewStore.RestoreLayoutFromStream(new System.IO.MemoryStream(Encoding.UTF8.GetBytes(layout)));
                }
                else
                {
                    gridViewStore.RestoreLayoutFromStream(layout_store);
                    layout_store.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }
            
            private void StoreForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                StoreDataSet.Instance.Store.Clear();
                StoreDataSet.Instance.Order.Clear();
                StoreDataSet.Instance.StoreDiagnose.Clear();
                StoreDataSet.Instance.StoreInspection.Clear();
                StoreDataSet.Instance.StoreTreatment.Clear();
                StoreDataSet.Instance.StoreWardRoom.Clear();
                StoreDataSet.Instance.StoreLeft.Clear();
                StoreDataSet.Instance.StoreValidity.Clear();
                StoreDataSet.Instance.StoreLeftDiagnose.Clear();
                StoreDataSet.Instance.StoreHistory.Clear();
                StoreDataSet.Instance.StoreGeneralinspection.Clear();
                StoreDataSet.Instance.StoreInternalinspection.Clear();
                StoreDataSet.Instance.StoreHeart.Clear();
                StoreDataSet.Instance.StoreDigestive.Clear();
                StoreDataSet.Instance.StoreUrine.Clear();
            }

        #endregion

        #region Формын event.

            private void simpleButtonReloadStore_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void gridControlStore_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add_store"))
                    {
                        addStore();
                    }
                    else if (String.Equals(e.Button.Tag, "edit_store") && gridViewStore.GetFocusedRow() != null)
                    {
                        editStore();
                    }
                    else if (String.Equals(e.Button.Tag, "delete_store") && gridViewStore.GetFocusedRow() != null)
                    {
                        try
                        {
                            deleteStore();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "close_store") && gridViewStore.GetFocusedRow() != null)
                    {
                        try
                        {
                            closeStore();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Түүх нээхэд алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                }
            }

            private void gridViewStore_MasterRowGetRelationDisplayCaption(object sender, DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs e)
            {
                if (e.RelationIndex == 0)
                    e.RelationName = "Тасаг өрөө";
                else if (e.RelationIndex == 1)
                    e.RelationName = "Үзлэг эмчилгээ";
                else if (e.RelationIndex == 2)
                    e.RelationName = "Клиникийн урьдчилсан онош";
                else if (e.RelationIndex == 3)
                    e.RelationName = "Анамнез";
                else if (e.RelationIndex == 4)
                    e.RelationName = "Ерөнхий үзлэг";
                else if (e.RelationIndex == 5)
                    e.RelationName = "Дотрын үзлэг";
                else if (e.RelationIndex == 6)
                    e.RelationName = "Зүрх судасны тогтолцоо";
                else if (e.RelationIndex == 7)
                    e.RelationName = "Хоол боловсруулах тогтолцоо";
                else if (e.RelationIndex == 8)
                    e.RelationName = "Шээсний тогтолцоо";
                else if (e.RelationIndex == 9)
                    e.RelationName = "Оношийн үндэслэл";
                else if (e.RelationIndex == 10)
                    e.RelationName = "Эмнэлэгээс гарах";
                else if (e.RelationIndex == 11)
                    e.RelationName = "Гарах үеийн онош";
            }

            private void radioGroupIsUse_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (radioGroupIsUse.SelectedIndex != 2)
                {
                    bool[] roles = UserUtil.getWindowRole(FORM_ID);
                    string filterValue;
                    if (radioGroupIsUse.SelectedIndex == 0 && (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID || UserUtil.getUserRoleId() == RoleUtil.RECEPTION_ID || UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.NURSE_ID))
                    {
                        filterValue = "'Хаагдаагүй'";

                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = roles[1];
                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = roles[3];
                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = roles[2];
                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = false;
                        gridViewStore.ActiveFilterString = "isCloseText = " + filterValue + "";
                    }
                    else if (radioGroupIsUse.SelectedIndex == 1 && (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID))
                    {
                        filterValue = "'Хаагдсан'";

                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = roles[3];
                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = true;
                        gridViewStore.ActiveFilterString = "isCloseText = " + filterValue + "";
                    }
                    else
                    {
                        filterValue = "'Хаагдсан'";

                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = roles[3];
                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
                        gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = false;
                        gridViewStore.ActiveFilterString = "isCloseText = " + filterValue + "";
                    }
                }
                else
                {
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
                    gridControlStore.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = false;
                    gridViewStore.ActiveFilterString = "";
                }
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                saveLayout();
            }

        #endregion

        #region Формын функцууд.

            private void reload()
            {
                string text = "", errorText;
                if (dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    dateEditStart.Focus();
                    errorText = "Эхлэх огноо дуусах огнооноос хойно байж болохгүй";                 
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    ProgramUtil.showAlertDialog(Instance , Instance.Text, text);
                    return;
                }

                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                string wardID = "";
                if (UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                {
                    wardID = UserUtil.getUserWardID();
                }
                else if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                {
                    StoreDataSet.PharmacistConfUserTableAdapter.Fill(StoreDataSet.Instance.PharmacistConfUser, HospitalUtil.getHospitalId(), UserUtil.getUserId());
                    for (int i = 0; i < StoreDataSet.Instance.PharmacistConfUser.Rows.Count; i++)
                        wardID += StoreDataSet.Instance.PharmacistConfUser.Rows[i]["wardID"] + "~";

                    if (wardID.Equals(""))
                        wardID = "-1";
                    else
                        wardID = wardID.Substring(0, wardID.Length - 1);
                }
                StoreDataSet.StoreTableAdapter.Fill(StoreDataSet.Instance.Store, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreWardRoomTableAdapter.Fill(StoreDataSet.Instance.StoreWardRoom, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreDiagnoseTableAdapter.Fill(StoreDataSet.Instance.StoreDiagnose, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreLeftDiagnoseTableAdapter.Fill(StoreDataSet.Instance.StoreLeftDiagnose, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreInspectionTableAdapter.Fill(StoreDataSet.Instance.StoreInspection, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreTreatmentTableAdapter.Fill(StoreDataSet.Instance.StoreTreatment, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);             
                StoreDataSet.StoreLeftTableAdapter.Fill(StoreDataSet.Instance.StoreLeft, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreValidityTableAdapter.Fill(StoreDataSet.Instance.StoreValidity, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreHistoryTableAdapter.Fill(StoreDataSet.Instance.StoreHistory, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreGeneralinspectionTableAdapter.Fill(StoreDataSet.Instance.StoreGeneralinspection, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreInternalinspectionTableAdapter.Fill(StoreDataSet.Instance.StoreInternalinspection, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreHeartTableAdapter.Fill(StoreDataSet.Instance.StoreHeart, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreDigestiveTableAdapter.Fill(StoreDataSet.Instance.StoreDigestive, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.StoreUrineTableAdapter.Fill(StoreDataSet.Instance.StoreUrine, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text, wardID);
                StoreDataSet.OrderTableAdapter.Fill(StoreDataSet.Instance.Order, HospitalUtil.getHospitalId(), dateEditStart.Text, dateEditEnd.Text);

                
                radioGroupIsUse.SelectedIndex = 0;
                gridViewStore.ActiveFilterString = "isCloseText = 'Хаагдаагүй'";
                gridViewStore.RefreshData();
                ProgramUtil.closeWaitDialog();
            }

            private void addStore() 
            {
                StoreDataSet.Instance.Store.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                StoreDataSet.Instance.Store.beginDateColumn.DefaultValue = DateTime.Now;
                currentStoreView = (DataRowView)StoreDataSet.StoreBindingSource.AddNew();
                if (actionType == 0)
                {
                    StoreInUpForm.Instance.showForm(1, 0);
                }
                else
                {
                    StoreInUpForm.Instance.showForm(3, actionType);
                }
            }

            private void editStore() 
            {
                // StoreDataSet.Instance.Store.educationColumn.DefaultValue = "";
                currentStoreView = (DataRowView)StoreDataSet.StoreBindingSource.Current;
                StoreInUpForm.Instance.showForm(2, 0);
                //gridViewStore.RefreshData();
            }

            private void deleteStore() 
            {
                if(Convert.ToBoolean(gridViewStore.GetFocusedDataRow()["isClose"]))
                {
                    XtraMessageBox.Show("Энэ түүхийг хаасан тул устгаж болохгүй");
                    return;
                }
                object ret = UserDataSet.QueriesTableAdapter.can_delete_store(HospitalUtil.getHospitalId(), gridViewStore.GetFocusedDataRow()["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    return;
                }
                DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмчлүүлэгчийн түүх устгах", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    gridViewStore.DeleteSelectedRows();
                    StoreDataSet.StoreBindingSource.EndEdit();
                    StoreDataSet.StoreTableAdapter.Update(StoreDataSet.Instance.Store);
                }
            }

            private void closeStore() 
            {
                DialogResult dialogResult = XtraMessageBox.Show("Энэ түүхийг нээх үү", "Эмчлүүлэгчийн түүх буцаан нээх", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    gridViewStore.GetFocusedDataRow();
                    currentStoreView = (DataRowView)StoreDataSet.StoreBindingSource.Current;
                    currentStoreView["isClose"] = false;
                    StoreDataSet.StoreBindingSource.EndEdit();
                    StoreDataSet.StoreTableAdapter.Update(StoreDataSet.Instance.Store);
                }
            }

            private void saveLayout()
            {
                try
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);

                    System.IO.Stream str = new System.IO.MemoryStream();
                    gridViewStore.SaveLayoutToStream(str);
                    str.Seek(0, System.IO.SeekOrigin.Begin);
                    System.IO.StreamReader reader = new System.IO.StreamReader(str);
                    UserUtil.saveUserLayout("store", reader.ReadToEnd());
                    reader.Close();
                    str.Close();

                    ProgramUtil.closeWaitDialog();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Layout хадгалахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            
            public string getStartDate()
            {
                return dateEditStart.Text;
            }

            public string getEndDate()
            {
                return dateEditEnd.Text;
            }

            public void refreshData()
            {
                gridViewStore.RefreshData();
            }

        #endregion

        #region Хэвлэх, хөрвүүлэх функц

            private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemPrint.Caption;
                dropDownButtonPrint.Image = barButtonItemPrint.Glyph;

                if (gridViewStore.GetFocusedRow() != null)
                    StoreReport.Instance.initAndShow(1, gridViewStore.GetFocusedDataRow()["id"].ToString(), (DataRowView)StoreDataSet.StoreBindingSource.Current);
                else
                    XtraMessageBox.Show("Мөр сонгоно уу");
            }

            private void barButtonItemEndBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemEndBal.Caption;
                dropDownButtonPrint.Image = barButtonItemEndBal.Glyph;
            }

            private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemConvert.Caption;
                dropDownButtonPrint.Image = barButtonItemConvert.Glyph;
                ProgramUtil.convertGrid(gridViewStore, "Өвчний түүх");
            }

        #endregion

            

    }
}