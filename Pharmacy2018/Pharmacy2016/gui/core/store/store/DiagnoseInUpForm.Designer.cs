﻿namespace Pharmacy2016.gui.core.store.store
{
    partial class DiagnoseInUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiagnoseInUpForm));
            this.dateEditDiagnoseDate = new DevExpress.XtraEditors.DateEdit();
            this.memoEditDiagnose = new DevExpress.XtraEditors.MemoEdit();
            this.memoEditDiagnoseDescription = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButtonSaveDiagnose = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancelDiagnose = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditDoctor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlExpUser = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.gridLookUpEditType = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDiagnoseDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDiagnoseDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDiagnose.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDiagnoseDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            this.SuspendLayout();
            // 
            // dateEditDiagnoseDate
            // 
            this.dateEditDiagnoseDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditDiagnoseDate.EditValue = null;
            this.dateEditDiagnoseDate.Location = new System.Drawing.Point(117, 81);
            this.dateEditDiagnoseDate.Name = "dateEditDiagnoseDate";
            this.dateEditDiagnoseDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDiagnoseDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDiagnoseDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditDiagnoseDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDiagnoseDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditDiagnoseDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDiagnoseDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditDiagnoseDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditDiagnoseDate.TabIndex = 3;
            // 
            // memoEditDiagnose
            // 
            this.memoEditDiagnose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditDiagnose.Location = new System.Drawing.Point(117, 107);
            this.memoEditDiagnose.Name = "memoEditDiagnose";
            this.memoEditDiagnose.Properties.MaxLength = 200;
            this.memoEditDiagnose.Size = new System.Drawing.Size(355, 125);
            this.memoEditDiagnose.TabIndex = 4;
            // 
            // memoEditDiagnoseDescription
            // 
            this.memoEditDiagnoseDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditDiagnoseDescription.Location = new System.Drawing.Point(117, 238);
            this.memoEditDiagnoseDescription.Name = "memoEditDiagnoseDescription";
            this.memoEditDiagnoseDescription.Properties.MaxLength = 200;
            this.memoEditDiagnoseDescription.Size = new System.Drawing.Size(355, 110);
            this.memoEditDiagnoseDescription.TabIndex = 5;
            // 
            // simpleButtonSaveDiagnose
            // 
            this.simpleButtonSaveDiagnose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSaveDiagnose.Location = new System.Drawing.Point(316, 376);
            this.simpleButtonSaveDiagnose.Name = "simpleButtonSaveDiagnose";
            this.simpleButtonSaveDiagnose.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSaveDiagnose.TabIndex = 11;
            this.simpleButtonSaveDiagnose.Text = "Хадгалах";
            this.simpleButtonSaveDiagnose.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSaveDiagnose.Click += new System.EventHandler(this.simpleButtonSaveDiagnose_Click);
            // 
            // simpleButtonCancelDiagnose
            // 
            this.simpleButtonCancelDiagnose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancelDiagnose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancelDiagnose.Location = new System.Drawing.Point(397, 376);
            this.simpleButtonCancelDiagnose.Name = "simpleButtonCancelDiagnose";
            this.simpleButtonCancelDiagnose.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonCancelDiagnose.TabIndex = 12;
            this.simpleButtonCancelDiagnose.Text = "Болих";
            this.simpleButtonCancelDiagnose.ToolTip = "Болих";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(59, 59);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(52, 13);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = "Ангилал*:";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(70, 84);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(41, 13);
            this.labelControl3.TabIndex = 9;
            this.labelControl3.Text = "Огноо*:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(73, 110);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(38, 13);
            this.labelControl4.TabIndex = 10;
            this.labelControl4.Text = "Онош*:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(65, 241);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(46, 13);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "Тайлбар:";
            // 
            // gridLookUpEditDoctor
            // 
            this.gridLookUpEditDoctor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditDoctor.EditValue = "";
            this.gridLookUpEditDoctor.Location = new System.Drawing.Point(117, 30);
            this.gridLookUpEditDoctor.Name = "gridLookUpEditDoctor";
            this.gridLookUpEditDoctor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDoctor.Properties.DisplayMember = "fullName";
            this.gridLookUpEditDoctor.Properties.NullText = "";
            this.gridLookUpEditDoctor.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditDoctor.Properties.ValueMember = "id";
            this.gridLookUpEditDoctor.Properties.View = this.gridView4;
            this.gridLookUpEditDoctor.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditDoctor.TabIndex = 1;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Нэр";
            this.gridColumn21.CustomizationCaption = "Нэр";
            this.gridColumn21.FieldName = "firstName";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Регистер";
            this.gridColumn22.CustomizationCaption = "Регистер";
            this.gridColumn22.FieldName = "id";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Эрх";
            this.gridColumn23.CustomizationCaption = "Эрх";
            this.gridColumn23.FieldName = "roleName";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Тасаг";
            this.gridColumn24.CustomizationCaption = "Тасаг";
            this.gridColumn24.FieldName = "wardName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Ажлын газар";
            this.gridColumn25.CustomizationCaption = "Ажлын газар";
            this.gridColumn25.FieldName = "organizationName";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Албан тушаал";
            this.gridColumn26.CustomizationCaption = "Албан тушаал";
            this.gridColumn26.FieldName = "positionName";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            // 
            // labelControlExpUser
            // 
            this.labelControlExpUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlExpUser.Location = new System.Drawing.Point(79, 31);
            this.labelControlExpUser.Name = "labelControlExpUser";
            this.labelControlExpUser.Size = new System.Drawing.Size(32, 13);
            this.labelControlExpUser.TabIndex = 31;
            this.labelControlExpUser.Text = "Эмч*: ";
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 376);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 32;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // gridLookUpEditType
            // 
            this.gridLookUpEditType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditType.Location = new System.Drawing.Point(117, 56);
            this.gridLookUpEditType.Name = "gridLookUpEditType";
            this.gridLookUpEditType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditType.Properties.DisplayMember = "name";
            this.gridLookUpEditType.Properties.NullText = " ";
            this.gridLookUpEditType.Properties.ValueMember = "id";
            this.gridLookUpEditType.Properties.View = this.gridLookUpEdit1View;
            this.gridLookUpEditType.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditType.TabIndex = 2;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.ReadOnly = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Нэр";
            this.gridColumn2.FieldName = "name";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ShowCaption = false;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // DiagnoseInUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancelDiagnose;
            this.ClientSize = new System.Drawing.Size(484, 411);
            this.Controls.Add(this.gridLookUpEditType);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.gridLookUpEditDoctor);
            this.Controls.Add(this.labelControlExpUser);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.simpleButtonCancelDiagnose);
            this.Controls.Add(this.simpleButtonSaveDiagnose);
            this.Controls.Add(this.memoEditDiagnoseDescription);
            this.Controls.Add(this.memoEditDiagnose);
            this.Controls.Add(this.dateEditDiagnoseDate);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DiagnoseInUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Онош нэмэх";
            this.Shown += new System.EventHandler(this.DiagnoseInUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDiagnoseDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDiagnoseDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDiagnose.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDiagnoseDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.DateEdit dateEditDiagnoseDate;
        private DevExpress.XtraEditors.MemoEdit memoEditDiagnose;
        private DevExpress.XtraEditors.MemoEdit memoEditDiagnoseDescription;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSaveDiagnose;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancelDiagnose;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDoctor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.LabelControl labelControlExpUser;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
    }
}