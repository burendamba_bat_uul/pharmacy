﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.store.ds;

namespace Pharmacy2016.gui.core.store.store
{
    /*
     * Эмчлүүлэгчийн тасаг, өрөө нэмэх, засах цонх.
     * 
     */

    public partial class WardRoomInUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц
            
            private static WardRoomInUpForm INSTANCE = new WardRoomInUpForm();

            public static WardRoomInUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            public DataRowView currBed;

            private WardRoomInUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initError();
                initDataSource();
                initBinding();
                reload();
            }

            private void initDataSource()
            {
                treeListLookUpEditWardLine.Properties.DataSource = UserDataSet.WardOtherBindingSource;
                treeListLookUpEditRoom.Properties.DataSource = StoreDataSet.RoomBedBindingSource;
            }

            private void initError()
            {
                treeListLookUpEditWardLine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditStartDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditRoom.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

            private void initBinding()
            {
                treeListLookUpEditWardLine.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreWardRoomBindingSource, "wardID", true));
                treeListLookUpEditRoom.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreWardRoomBindingSource, "bedID", true));
                dateEditStartDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreWardRoomBindingSource, "startDate", true));
                dateEditEndDate.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreWardRoomBindingSource, "endDate", true));
                memoEditDesc.DataBindings.Add(new Binding("EditValue", StoreDataSet.StoreWardRoomBindingSource, "description", true));
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    checkRole();
                    if (actionType == 1)
                    {
                        insertPatientInform();
                    }
                    else if (actionType == 2)
                    {
                        updatePatientInform();
                    }

                    ShowDialog();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                if (UserUtil.getUserRoleId() == RoleUtil.RECEPTION_ID)
                {
                    treeListLookUpEditRoom.Properties.ReadOnly = true;
                }
                else if (UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                {
                    treeListLookUpEditRoom.Properties.ReadOnly = false;
                }
                else 
                {
                    treeListLookUpEditRoom.Properties.ReadOnly = false;
                }
            }

            private void insertPatientInform()
            {
                dateEditStartDate.DateTime = DateTime.Now;
            }

            private void updatePatientInform()
            {
                addCurrBed();
                if (currBed != null)
                {
                    treeListLookUpEditRoom.EditValue = currBed["id"];
                    //treeListLookUpEditRoom.Text = currBed["name"].ToString()             
                }
            }

            private void addCurrBed()
            {
                //DataRowView currView = (DataRowView)StoreDataSet.StoreWardRoomBindingSource.Current;
                if (StoreInUpForm.Instance.currPatientInformView["bedID"] != null && StoreInUpForm.Instance.currPatientInformView["bedID"] != DBNull.Value)
                {
                    bool exist = false;
                    for (int i = 0; i < StoreDataSet.RoomBedBindingSource.Count; i++)
                    {
                        DataRowView view = (DataRowView)StoreDataSet.RoomBedBindingSource[i];
                        if (view["id"].ToString().Equals(StoreInUpForm.Instance.currPatientInformView["bedID"].ToString()))
                        {
                            exist = true;
                            break;
                        }
                    }
                    if (!exist)
                    {
                        currBed = (DataRowView)StoreDataSet.RoomBedBindingSource.AddNew();
                        currBed["id"] = StoreInUpForm.Instance.currPatientInformView["bedID"];
                        currBed["pid"] = StoreInUpForm.Instance.currPatientInformView["roomID"];
                        currBed["wardID"] = StoreInUpForm.Instance.currPatientInformView["wardID"];
                        currBed["name"] = StoreInUpForm.Instance.currPatientInformView["bedName"];
                        //currBed.EndEdit();
                        //StoreInUpForm.Instance.currPatientInformView.EndEdit();
                    }
                }
            }


            private void clearData()
            {
                clearErrorText();

                if (actionType == 2 && currBed != null && StoreInUpForm.Instance.currPatientInformView != null)
                {
                    StoreDataSet.RoomBedBindingSource.Remove(currBed);
                    //currBed = null;
                }               
                StoreDataSet.StoreWardRoomBindingSource.CancelEdit();
            }

            private void clearErrorText()
            {
                treeListLookUpEditWardLine.ErrorText = "";
                treeListLookUpEditRoom.ErrorText = "";
                dateEditStartDate.ErrorText = "";
                dateEditEndDate.ErrorText = "";
                memoEditDesc.ErrorText = "";
            }

            private void PateintInformInUpForm_Shown(object sender, EventArgs e)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                {
                    treeListLookUpEditRoom.Focus();
                    treeListLookUpEditWardLine.Properties.ReadOnly = true;
                }               
                else
                {
                    treeListLookUpEditWardLine.Focus();
                    treeListLookUpEditWardLine.Properties.ReadOnly = false;
                }      

                if (StoreInUpForm.Instance.currPatientInformView != null)
                {
                    treeListLookUpEditRoom.Text = StoreInUpForm.Instance.currPatientInformView["bedID"].ToString();
                    treeListLookUpEditRoom.Focus(); 
                }
                
            }

        #endregion

        #region Формын event.

            private void treeListLookUpEditWardLine_EditValueChanged(object sender, EventArgs e)
            {
                if (Visible && treeListLookUpEditWardLine.EditValue != null)
                {
                    StoreDataSet.RoomBedBindingSource.Filter = "wardID = '" + treeListLookUpEditWardLine.EditValue.ToString() + "'";
                    treeListLookUpEditRoom.Properties.TreeList.ExpandAll();                   
                }
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                savePatientInform();
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

        #endregion

        #region Формын функцууд.

            private void reload()
            {             
                UserDataSet.WardTableAdapter.Fill(UserDataSet.Instance.Ward, HospitalUtil.getHospitalId());
                StoreDataSet.RoomBedTableAdapter.Fill(StoreDataSet.Instance.RoomBed, HospitalUtil.getHospitalId());
                //addCurrBed();
            }

            private bool checkPatientInform()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if (treeListLookUpEditWardLine.EditValue == DBNull.Value || (treeListLookUpEditWardLine.EditValue = treeListLookUpEditWardLine.EditValue.ToString().Trim()).Equals(""))
                {
                    errorText = "Тасаг сонгоно уу!";
                    treeListLookUpEditWardLine.ErrorText = errorText;
                    isRight = false;
                    treeListLookUpEditWardLine.Focus();
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }
            
            private void savePatientInform()
            {
                if (checkPatientInform())
                {                  
                    StoreInUpForm.Instance.currPatientInformView["wardName"] = treeListLookUpEditWardLine.Text;
                    StoreInUpForm.Instance.currPatientInformView["bedName"] = treeListLookUpEditRoom.Text;
                    //StoreInUpForm.Instance.currPatientInformView["bedName"] = treeListLookUpEditRoom.EditValue;

                    StoreForm.Instance.currentStoreView["wardID"] = treeListLookUpEditWardLine.EditValue;
                    StoreForm.Instance.currentStoreView.EndEdit();

                    if (!treeListLookUpEditRoom.Text.Equals(""))
                    {
                        currBed = (DataRowView)treeListLookUpEditRoom.GetSelectedDataRow();
                        StoreInUpForm.Instance.currPatientInformView["roomID"] = currBed["pid"];
                        //DataRow[] rows = StoreDataSet.Instance.RoomBed.Select("id = '" + currentBed["pid"] + "'");                       
                        if (currBed["pid"].Equals("0"))
                        {
                            //XtraMessageBox.Show("Өрөөний орыг сонгоно уу!");
                            treeListLookUpEditRoom.ErrorText = "Өрөөний орыг сонгоно уу!";
                            treeListLookUpEditRoom.Focus();
                            return;
                        }
                    }

                    StoreInUpForm.Instance.currPatientInformView.EndEdit();
                    StoreDataSet.StoreWardRoomBindingSource.EndEdit();
                    Close();
                }
            }

        #endregion                     

    }
}