﻿namespace Pharmacy2016.gui.core.store.drug
{
    partial class DrugNurseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DrugNurseForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pivotGridControl = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.pivotGridField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldPatient = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMedicine = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldConfirmPrice = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldConfirmCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldGiveCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldSumPrice = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridFieldIncTypeName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.checkedComboBoxEdit = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.dropDownButtonPrint = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDrugPatient = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDrugMedicine = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConvert = new DevExpress.XtraBars.BarButtonItem();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlAll = new DevExpress.XtraEditors.LabelControl();
            this.checkEditAll = new DevExpress.XtraEditors.CheckEdit();
            this.gridLookUpEditPatient = new DevExpress.XtraEditors.GridLookUpEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.controlNavigator = new DevExpress.XtraEditors.ControlNavigator();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pivotGridControl);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(784, 461);
            this.panelControl1.TabIndex = 2;
            // 
            // pivotGridControl
            // 
            this.pivotGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotGridField2,
            this.pivotGridFieldPatient,
            this.pivotGridFieldMedicine,
            this.pivotGridField3,
            this.pivotGridFieldConfirmPrice,
            this.pivotGridFieldConfirmCount,
            this.pivotGridFieldGiveCount,
            this.pivotGridFieldSumPrice,
            this.pivotGridField1,
            this.pivotGridFieldDate,
            this.pivotGridFieldMonth,
            this.pivotGridFieldID,
            this.pivotGridFieldIncTypeName});
            this.pivotGridControl.Location = new System.Drawing.Point(2, 73);
            this.pivotGridControl.Name = "pivotGridControl";
            this.pivotGridControl.OptionsFilterPopup.IsRadioMode = true;
            this.pivotGridControl.OptionsSelection.CellSelection = false;
            this.pivotGridControl.OptionsSelection.MultiSelect = false;
            this.pivotGridControl.Size = new System.Drawing.Size(780, 360);
            this.pivotGridControl.TabIndex = 13;
            this.pivotGridControl.CustomCellValue += new System.EventHandler<DevExpress.XtraPivotGrid.PivotCellValueEventArgs>(this.pivotGridControl_CustomCellValue);
            this.pivotGridControl.CustomDrawCell += new DevExpress.XtraPivotGrid.PivotCustomDrawCellEventHandler(this.pivotGridControl_CustomDrawCell);
            this.pivotGridControl.CustomAppearance += new DevExpress.XtraPivotGrid.PivotCustomAppearanceEventHandler(this.pivotGridControl_CustomAppearance);
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField2.AreaIndex = 1;
            this.pivotGridField2.Caption = "Эмчлүүлэгч";
            this.pivotGridField2.FieldName = "patientName";
            this.pivotGridField2.MinWidth = 100;
            this.pivotGridField2.Name = "pivotGridField2";
            this.pivotGridField2.Options.ShowTotals = false;
            this.pivotGridField2.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            this.pivotGridField2.Width = 150;
            // 
            // pivotGridFieldPatient
            // 
            this.pivotGridFieldPatient.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldPatient.AreaIndex = 0;
            this.pivotGridFieldPatient.Caption = "Сувилагч";
            this.pivotGridFieldPatient.FieldName = "userName";
            this.pivotGridFieldPatient.MinWidth = 0;
            this.pivotGridFieldPatient.Name = "pivotGridFieldPatient";
            this.pivotGridFieldPatient.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending;
            this.pivotGridFieldPatient.Width = 150;
            // 
            // pivotGridFieldMedicine
            // 
            this.pivotGridFieldMedicine.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldMedicine.AreaIndex = 2;
            this.pivotGridFieldMedicine.Caption = "Эм";
            this.pivotGridFieldMedicine.FieldName = "medicineName";
            this.pivotGridFieldMedicine.Name = "pivotGridFieldMedicine";
            this.pivotGridFieldMedicine.Width = 180;
            // 
            // pivotGridField3
            // 
            this.pivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField3.AreaIndex = 3;
            this.pivotGridField3.Caption = "Тун хэмжээ";
            this.pivotGridField3.FieldName = "unitType";
            this.pivotGridField3.Name = "pivotGridField3";
            // 
            // pivotGridFieldConfirmPrice
            // 
            this.pivotGridFieldConfirmPrice.AreaIndex = 3;
            this.pivotGridFieldConfirmPrice.Caption = "Үнэ";
            this.pivotGridFieldConfirmPrice.FieldName = "confirmPrice";
            this.pivotGridFieldConfirmPrice.Name = "pivotGridFieldConfirmPrice";
            // 
            // pivotGridFieldConfirmCount
            // 
            this.pivotGridFieldConfirmCount.AreaIndex = 1;
            this.pivotGridFieldConfirmCount.Caption = "Тоо";
            this.pivotGridFieldConfirmCount.FieldName = "confirmCount";
            this.pivotGridFieldConfirmCount.Name = "pivotGridFieldConfirmCount";
            // 
            // pivotGridFieldGiveCount
            // 
            this.pivotGridFieldGiveCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldGiveCount.AreaIndex = 0;
            this.pivotGridFieldGiveCount.Caption = "Өгсөн тоо";
            this.pivotGridFieldGiveCount.FieldName = "giveCount";
            this.pivotGridFieldGiveCount.Name = "pivotGridFieldGiveCount";
            this.pivotGridFieldGiveCount.Width = 150;
            // 
            // pivotGridFieldSumPrice
            // 
            this.pivotGridFieldSumPrice.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridFieldSumPrice.AreaIndex = 1;
            this.pivotGridFieldSumPrice.Caption = "Нийт үнэ";
            this.pivotGridFieldSumPrice.CellFormat.FormatString = "n0";
            this.pivotGridFieldSumPrice.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.FieldName = "sumPrice";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.Name = "pivotGridFieldSumPrice";
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatString = "n2";
            this.pivotGridFieldSumPrice.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pivotGridFieldSumPrice.ValueFormat.FormatString = "n";
            this.pivotGridFieldSumPrice.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridField1.AreaIndex = 1;
            this.pivotGridField1.Caption = "Цаг";
            this.pivotGridField1.CellFormat.FormatString = "dd-н HH:mm";
            this.pivotGridField1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.pivotGridField1.FieldName = "date";
            this.pivotGridField1.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateHourMinuteSecond;
            this.pivotGridField1.Name = "pivotGridField1";
            this.pivotGridField1.UnboundFieldName = "pivotGridField1";
            this.pivotGridField1.ValueFormat.FormatString = "dd-н HH:mm";
            this.pivotGridField1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // pivotGridFieldDate
            // 
            this.pivotGridFieldDate.AreaIndex = 2;
            this.pivotGridFieldDate.Caption = "Өдөр";
            this.pivotGridFieldDate.FieldName = "date";
            this.pivotGridFieldDate.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateDay;
            this.pivotGridFieldDate.Name = "pivotGridFieldDate";
            this.pivotGridFieldDate.UnboundFieldName = "pivotGridFieldDate";
            this.pivotGridFieldDate.Width = 30;
            // 
            // pivotGridFieldMonth
            // 
            this.pivotGridFieldMonth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.pivotGridFieldMonth.AreaIndex = 0;
            this.pivotGridFieldMonth.Caption = "Сар";
            this.pivotGridFieldMonth.FieldName = "date";
            this.pivotGridFieldMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.pivotGridFieldMonth.MinWidth = 80;
            this.pivotGridFieldMonth.Name = "pivotGridFieldMonth";
            this.pivotGridFieldMonth.UnboundFieldName = "pivotGridFieldMonth";
            // 
            // pivotGridFieldID
            // 
            this.pivotGridFieldID.AreaIndex = 0;
            this.pivotGridFieldID.Caption = "Дугаар";
            this.pivotGridFieldID.FieldName = "id";
            this.pivotGridFieldID.MinWidth = 0;
            this.pivotGridFieldID.Name = "pivotGridFieldID";
            this.pivotGridFieldID.Options.ReadOnly = true;
            // 
            // pivotGridFieldIncTypeName
            // 
            this.pivotGridFieldIncTypeName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridFieldIncTypeName.AreaIndex = 3;
            this.pivotGridFieldIncTypeName.Caption = "Төрөл";
            this.pivotGridFieldIncTypeName.FieldName = "incTypeName";
            this.pivotGridFieldIncTypeName.Name = "pivotGridFieldIncTypeName";
            this.pivotGridFieldIncTypeName.Visible = false;
            this.pivotGridFieldIncTypeName.Width = 80;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.dateEditEnd);
            this.panelControl2.Controls.Add(this.dateEditStart);
            this.panelControl2.Controls.Add(this.checkedComboBoxEdit);
            this.panelControl2.Controls.Add(this.dropDownButtonPrint);
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Controls.Add(this.labelControlAll);
            this.panelControl2.Controls.Add(this.checkEditAll);
            this.panelControl2.Controls.Add(this.gridLookUpEditPatient);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(780, 71);
            this.panelControl2.TabIndex = 12;
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(146, 13);
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.EditFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditEnd.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.dateEditEnd.Size = new System.Drawing.Size(130, 20);
            this.dateEditEnd.TabIndex = 20;
            // 
            // dateEditStart
            // 
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(10, 13);
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.EditFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditStart.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.dateEditStart.Size = new System.Drawing.Size(130, 20);
            this.dateEditStart.TabIndex = 19;
            // 
            // checkedComboBoxEdit
            // 
            this.checkedComboBoxEdit.EditValue = "count";
            this.checkedComboBoxEdit.Location = new System.Drawing.Point(282, 13);
            this.checkedComboBoxEdit.Name = "checkedComboBoxEdit";
            this.checkedComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.checkedComboBoxEdit.Properties.DropDownRows = 4;
            this.checkedComboBoxEdit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("count", "Өгсөн тоо", System.Windows.Forms.CheckState.Checked),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("sumPrice", "Нийт үнэ")});
            this.checkedComboBoxEdit.Size = new System.Drawing.Size(150, 20);
            this.checkedComboBoxEdit.TabIndex = 18;
            this.checkedComboBoxEdit.ToolTip = "Харагдах багана сонгох";
            this.checkedComboBoxEdit.EditValueChanged += new System.EventHandler(this.checkedComboBoxEdit_EditValueChanged);
            // 
            // dropDownButtonPrint
            // 
            this.dropDownButtonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropDownButtonPrint.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dropDownButtonPrint.DropDownControl = this.popupMenu;
            this.dropDownButtonPrint.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonPrint.Image")));
            this.dropDownButtonPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.dropDownButtonPrint.Location = new System.Drawing.Point(640, 10);
            this.dropDownButtonPrint.MenuManager = this.barManager;
            this.dropDownButtonPrint.Name = "dropDownButtonPrint";
            this.barManager.SetPopupContextMenu(this.dropDownButtonPrint, this.popupMenu);
            this.dropDownButtonPrint.Size = new System.Drawing.Size(130, 25);
            this.dropDownButtonPrint.TabIndex = 17;
            this.dropDownButtonPrint.Text = "Хэвлэх";
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemDrugPatient),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemDrugMedicine),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConvert)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "Хэвлэх";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 0;
            this.barButtonItemPrint.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.LargeGlyph")));
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemDrugPatient
            // 
            this.barButtonItemDrugPatient.Caption = "Эмийн түүвэр сувилагчаар";
            this.barButtonItemDrugPatient.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDrugPatient.Glyph")));
            this.barButtonItemDrugPatient.Id = 1;
            this.barButtonItemDrugPatient.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDrugPatient.LargeGlyph")));
            this.barButtonItemDrugPatient.Name = "barButtonItemDrugPatient";
            this.barButtonItemDrugPatient.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemDrugMedicine
            // 
            this.barButtonItemDrugMedicine.Caption = "Эмийн түүвэр эмчээр";
            this.barButtonItemDrugMedicine.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDrugMedicine.Glyph")));
            this.barButtonItemDrugMedicine.Id = 2;
            this.barButtonItemDrugMedicine.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDrugMedicine.LargeGlyph")));
            this.barButtonItemDrugMedicine.Name = "barButtonItemDrugMedicine";
            this.barButtonItemDrugMedicine.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemConvert
            // 
            this.barButtonItemConvert.Caption = "Хөрвүүлэх";
            this.barButtonItemConvert.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.Glyph")));
            this.barButtonItemConvert.Id = 3;
            this.barButtonItemConvert.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConvert.LargeGlyph")));
            this.barButtonItemConvert.Name = "barButtonItemConvert";
            this.barButtonItemConvert.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConvert_ItemClick);
            // 
            // barManager
            // 
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControl1);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemPrint,
            this.barButtonItemDrugPatient,
            this.barButtonItemDrugMedicine,
            this.barButtonItemConvert});
            this.barManager.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 461);
            this.barDockControlBottom.Size = new System.Drawing.Size(784, 0);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(0, 461);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(784, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 461);
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.Location = new System.Drawing.Point(282, 37);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(65, 25);
            this.simpleButtonReload.TabIndex = 14;
            this.simpleButtonReload.Text = "Шүүх";
            this.simpleButtonReload.ToolTip = "Эмчлүүлэгчийн эм сонгох";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // labelControlAll
            // 
            this.labelControlAll.Location = new System.Drawing.Point(10, 43);
            this.labelControlAll.Name = "labelControlAll";
            this.labelControlAll.Size = new System.Drawing.Size(30, 13);
            this.labelControlAll.TabIndex = 13;
            this.labelControlAll.Text = "Бүгд :";
            // 
            // checkEditAll
            // 
            this.checkEditAll.Location = new System.Drawing.Point(46, 40);
            this.checkEditAll.Name = "checkEditAll";
            this.checkEditAll.Properties.Caption = "";
            this.checkEditAll.Size = new System.Drawing.Size(19, 19);
            this.checkEditAll.TabIndex = 12;
            this.checkEditAll.ToolTip = "Бүгдийг сонгох";
            this.checkEditAll.CheckedChanged += new System.EventHandler(this.checkEditAll_CheckedChanged);
            // 
            // gridLookUpEditPatient
            // 
            this.gridLookUpEditPatient.EditValue = "Эмчлүүлэгч сонгох";
            this.gridLookUpEditPatient.Location = new System.Drawing.Point(76, 39);
            this.gridLookUpEditPatient.Name = "gridLookUpEditPatient";
            this.gridLookUpEditPatient.Properties.ActionButtonIndex = 1;
            serializableAppearanceObject1.Options.UseImage = true;
            this.gridLookUpEditPatient.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleRight, ((System.Drawing.Image)(resources.GetObject("gridLookUpEditPatient.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "reload", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPatient.Properties.DisplayMember = "nurseName";
            this.gridLookUpEditPatient.Properties.NullText = "Эмийн түүвэр сонгох";
            this.gridLookUpEditPatient.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditPatient.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridLookUpEditPatient.Properties.ValueMember = "id";
            this.gridLookUpEditPatient.Properties.View = this.gridView1;
            this.gridLookUpEditPatient.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditPatient_ButtonClick);
            this.gridLookUpEditPatient.Size = new System.Drawing.Size(200, 22);
            this.gridLookUpEditPatient.TabIndex = 11;
            this.gridLookUpEditPatient.ToolTip = "Эмчлүүлэгч сонгох";
            this.gridLookUpEditPatient.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.gridLookUpEditPatient_CustomDisplayText);
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn4});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Огноо";
            this.gridColumn3.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn3.CustomizationCaption = "Огноо";
            this.gridColumn3.FieldName = "date";
            this.gridColumn3.MinWidth = 100;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 100;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Сувилагч";
            this.gridColumn1.CustomizationCaption = "Сувилагч";
            this.gridColumn1.FieldName = "nurseName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Тасаг";
            this.gridColumn2.CustomizationCaption = "Тасаг";
            this.gridColumn2.FieldName = "nurseWardName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 3;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Эмчлүүлэгч";
            this.gridColumn4.CustomizationCaption = "Эмчлүүлэгч";
            this.gridColumn4.FieldName = "patientName";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 4;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.controlNavigator);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(2, 433);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(780, 26);
            this.panelControl3.TabIndex = 4;
            // 
            // controlNavigator
            // 
            this.controlNavigator.Buttons.Append.Visible = false;
            this.controlNavigator.Buttons.CancelEdit.Visible = false;
            this.controlNavigator.Buttons.Edit.Visible = false;
            this.controlNavigator.Buttons.EndEdit.Visible = false;
            this.controlNavigator.Buttons.First.Visible = false;
            this.controlNavigator.Buttons.Last.Visible = false;
            this.controlNavigator.Buttons.Next.Visible = false;
            this.controlNavigator.Buttons.NextPage.Visible = false;
            this.controlNavigator.Buttons.Prev.Visible = false;
            this.controlNavigator.Buttons.PrevPage.Visible = false;
            this.controlNavigator.Buttons.Remove.Visible = false;
            this.controlNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "update"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Бүгдийг устгах", "deleteAll")});
            this.controlNavigator.Dock = System.Windows.Forms.DockStyle.Left;
            this.controlNavigator.Location = new System.Drawing.Point(2, 2);
            this.controlNavigator.Name = "controlNavigator";
            this.controlNavigator.ShowToolTips = true;
            this.controlNavigator.Size = new System.Drawing.Size(90, 22);
            this.controlNavigator.TabIndex = 3;
            this.controlNavigator.Text = "controlNavigator1";
            this.controlNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.controlNavigator_ButtonClick);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 461);
            // 
            // DrugNurseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DrugNurseForm";
            this.Text = "Эмчлүүлэгчид эм олгох";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DrugNurseForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkedComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.ControlNavigator controlNavigator;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldDate;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldPatient;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMedicine;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldSumPrice;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldMonth;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldID;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldIncTypeName;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonPrint;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.LabelControl labelControlAll;
        private DevExpress.XtraEditors.CheckEdit checkEditAll;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPatient;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldConfirmPrice;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldConfirmCount;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridFieldGiveCount;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField3;
        private DevExpress.XtraEditors.CheckedComboBoxEdit checkedComboBoxEdit;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDrugPatient;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDrugMedicine;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConvert;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
    }
}