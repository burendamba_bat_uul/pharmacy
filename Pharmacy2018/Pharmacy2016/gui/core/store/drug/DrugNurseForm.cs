﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.main;
using DevExpress.XtraPivotGrid;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.store.drug
{
    /*
     * Эмчлүүлэгчид эм олгох жагсаалт, нэмэх, засах, устгах цонх.
     * 
     */
    public partial class DrugNurseForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static DrugNurseForm INSTANCE = new DrugNurseForm();

            public static DrugNurseForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private readonly int FORM_ID = 1006;

            private bool[] roles;

            public string wardID = "";

            private DrugNurseForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                RoleUtil.addForm(FORM_ID, this);
                initError();

                //pivotGridFieldPatient.Width = 0;
                pivotGridControl.OptionsView.ShowDataHeaders = false;
                pivotGridControl.OptionsView.ShowFilterHeaders = false;
                
                //comboBoxEditPatientType.SelectedIndex = 0;
                dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                pivotGridControl.DataSource = PlanDataSet.DrugNurseDetailBindingSource;
                gridLookUpEditPatient.Properties.DataSource = PlanDataSet.DrugNurseBindingSource;
                
                DateTime date = DateTime.Now;
                dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1, 00, 00, 01);
                dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month), 23, 59, 59);
            }

            private void initError()
            {
                gridLookUpEditPatient.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditEnd.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {
                        pivotGridFieldGiveCount.Visible = true;
                        pivotGridFieldSumPrice.Visible = false;
                        checkRole();           

                        PlanDataSet.Instance.DrugNurse.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        PlanDataSet.Instance.DrugNurse.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        PlanDataSet.Instance.DrugNurseDetail.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        PlanDataSet.Instance.DrugNurseDetail.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        reloadPatient();
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                roles = UserUtil.getWindowRole(FORM_ID);

                controlNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                controlNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                controlNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                controlNavigator.Buttons.CustomButtons[3].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                dropDownButtonPrint.Visible = roles[4];

                if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                {
                    //wardID = UserUtil.getUserWardID();
                    //controlNavigator.Buttons.CustomButtons[1].Visible = false;
                    checkedComboBoxEdit.Visible = false;
                }
                else if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                {
                    checkedComboBoxEdit.Visible = true;
                    //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    //InformationDataSet.PharmacistWardTableAdapter.Fill(InformationDataSet.Instance.PharmacistWard, HospitalUtil.getHospitalId(), UserUtil.getUserId());
                    //for (int i = 0; i < InformationDataSet.Instance.PharmacistWard.Count; i++)
                    //    wardID += InformationDataSet.Instance.PharmacistWard[i]["wardID"].ToString() + "~";
                    //if (!wardID.Equals(""))
                    //    wardID = wardID.Substring(0, wardID.Length - 1);
                    ////textEdit.Text = wardID;
                    //ProgramUtil.closeWaitDialog();
                }
                else
                {
                    checkedComboBoxEdit.Visible = true;
                    //wardID = "-1";
                }
            }

            private void DrugNurseForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                //wardID = "";
                PlanDataSet.Instance.DrugNurse.Clear();
                PlanDataSet.Instance.DrugNurseDetail.Clear();
            }

        #endregion

        #region Формын event

            private void checkedComboBoxEdit_EditValueChanged(object sender, EventArgs e)
            {
                showDataField();
            }

            private void checkEditAll_CheckedChanged(object sender, EventArgs e)
            {
                gridLookUpEditPatient.Enabled = !checkEditAll.Checked;
                //if (checkEditAll.Checked)
                //    reload();
            }

            private void gridLookUpEditPatient_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
            {
                e.DisplayText = "Эмийн түүвэр сонгох";
            }

            private void gridLookUpEditPatient_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "reload"))
                {
                    try
                    {
                        reloadPatient();
                    }
                    catch (System.Exception ex)
                    {
                        XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                    }
                    finally
                    {
                        ProgramUtil.closeWaitDialog();
                    }
                }    
            }


            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void controlNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        addRow();
                    }
                    else if (String.Equals(e.Button.Tag, "delete"))
                    {
                        try
                        {
                            deleteRow();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "update"))
                    {
                        updateRow();
                    }
                    else if (String.Equals(e.Button.Tag, "deleteAll"))
                    {
                        try
                        {
                            deleteSelectedRow();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                }
            }
    

            private void pivotGridControl_CustomDrawCell(object sender, PivotCustomDrawCellEventArgs e)
            {
                // checkBox-г дахин утгыг харьцуулан зурж байна
                //if (e.DataField == pivotGridFieldIsGive && e.Value != null)
                //{
                //    if (e.RowValueType == PivotGridValueType.Value)
                //    {
                //        int value;
                //        if (Int32.TryParse(e.Value.ToString(), out value))
                //        {
                //            e.Appearance.ForeColor = Color.White;
                //            // checkBox-г дахин утгыг харьцуулан зурж байна
                //            DrawCheckBox(e.Graphics, e.Appearance, e.Bounds, value == 1);
                //            e.Handled = true;
                //        }
                //    }
                //    else
                //    {
                //        e.Appearance.ForeColor = Color.White;
                //        e.Handled = true;
                //    }
                //}
            }

            protected void DrawCheckBox(Graphics g, DevExpress.Utils.AppearanceObject a, Rectangle r, bool Checked)
            {
                //DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo info;
                //DevExpress.XtraEditors.Drawing.CheckEditPainter painter;
                //DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs args;
                //info = repositoryItemCheckEdit1.CreateViewInfo() as DevExpress.XtraEditors.ViewInfo.CheckEditViewInfo;
                //painter = repositoryItemCheckEdit1.CreatePainter() as DevExpress.XtraEditors.Drawing.CheckEditPainter;

                //info.EditValue = Checked;
                //DataRow[] rows = JournalDataSet.Instance.PatientMedicine.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND id = '" + id + "'");
                //if(rows != null && rows.Length > 0)
                //    info.EditValue = Convert.ToBoolean(rows[0]["isGive"]);
                //r.Height -= 2;
                //r.Width -= 2;

                //info.Bounds = r;
                //Manually set the forecolor and font
                //info.PaintAppearance.ForeColor = a.ForeColor;
                //info.PaintAppearance.Font = a.Font;
                //info.CalcViewInfo(g);
                //args = new DevExpress.XtraEditors.Drawing.ControlGraphicsInfoArgs(info, new DevExpress.Utils.Drawing.GraphicsCache(g), r);
                //painter.Draw(args);
                //args.Cache.Dispose();
            }

            private void pivotGridControl_CellDoubleClick(object sender, PivotCellEventArgs e)
            {
                if (roles[2] && e.ColumnValueType == PivotGridValueType.Value && e.RowValueType == PivotGridValueType.Value)
                {   
                    updateRow();
                }   
            }

            private void pivotGridControl_CustomCellValue(object sender, PivotCellValueEventArgs e)
            {
                //if (e.DataField == pivotGridFieldCount)
                //{
                //    PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
                //    double sum = 0;
                //    for (int i = 0; i < ds.RowCount; i++)
                //    {
                //        PivotDrillDownDataRow row = ds[i];
                //        if (Convert.ToBoolean(row["isGive"]))
                //        {
                //            sum += Convert.ToDouble(row["totalCount"]);
                //        }
                //    }
                //    if (ds.RowCount > 0)
                //    {
                //        e.Value = sum;
                //    }
                //}
                //else if (e.DataField == pivotGridFieldSumPrice)
                //{
                //    PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
                //    double sum = 0;
                //    for (int i = 0; i < ds.RowCount; i++)
                //    {
                //        PivotDrillDownDataRow row = ds[i];
                //        if (Convert.ToBoolean(row["isGive"]))
                //        {
                //            sum += Convert.ToDouble(row["totalSumPrice"]);
                //        }
                //    }
                //    if (ds.RowCount > 0)
                //    {
                //        e.Value = sum;
                //    }
                //}
            }

            private void pivotGridControl_CustomAppearance(object sender, PivotCustomAppearanceEventArgs e)
            {
                if (e.DataField == pivotGridFieldGiveCount || e.DataField == pivotGridFieldSumPrice)
                {
                    PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
                    for (int i = 0; i < ds.RowCount; i++)
                    {
                        e.Appearance.BackColor = Color.Yellow;
                    }
                }
            }

        #endregion

        #region Формын функц

            private void showDataField()
            {
                for(var i = 0; i < checkedComboBoxEdit.Properties.Items.Count; i++)
                {
                    if(checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("count"))
                    {
                        pivotGridFieldGiveCount.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                    else if(checkedComboBoxEdit.Properties.Items[i].Value.ToString().Equals("sumPrice"))
                    {
                        pivotGridFieldSumPrice.Visible = checkedComboBoxEdit.Properties.Items[i].CheckState == CheckState.Checked;
                    }
                }
            }

            private bool checkDate()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private bool checkUser()
            {
                bool isRight = true;

                 //(gridLookUpEditPatient.EditValue == DBNull.Value || gridLookUpEditPatient.EditValue == null || gridLookUpEditPatient.EditValue.ToString().Equals(""))
                if (!checkEditAll.Checked && gridLookUpEditPatient.Properties.View.GetSelectedRows().Length == 0)
                {
                    string errorText = "Эмийн түүвэр сонгоно уу";
                    string text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditPatient.ErrorText = errorText;
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                    isRight = false;
                    gridLookUpEditPatient.Focus();
                }

                return isRight;
            }

            private void reload()
            {
                if (checkDate() && checkUser())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                    string userID = "";
                    if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                    {
                        userID = UserUtil.getUserId();
                    }
                    else
                    {
                        userID = "";
                    }

                    var drugNurseID = "";  
                    if (checkEditAll.Checked)
                    {
                        pivotGridFieldPatient.Width = 200;
                        if (UserUtil.getUserRoleId() != RoleUtil.ADMIN_ID)
                        {
                            DataRow[] drugNurses = PlanDataSet.Instance.DrugNurse.Select("giveNurseID = '" + UserUtil.getUserId() + "' AND hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                            for (int j = 0; j < drugNurses.Length; j++)
                            {
                                drugNurseID += drugNurses[j]["id"] + "~";
                            }
                        }
                        //for (var i = 0; i < PlanDataSet.Instance.DrugNurseDetail.Rows.Count; i++)
                        //    drugNurseID += PlanDataSet.Instance.DrugNurseDetail.Rows[i] + "~";
                    }
                    else
                    {
                        int[] rowID = gridLookUpEditPatient.Properties.View.GetSelectedRows();
                        for (int i = 0; i < rowID.Length; i++)
                            drugNurseID += gridLookUpEditPatient.Properties.View.GetDataRow(rowID[i])["id"] + "~";
                    }
                    if (!drugNurseID.Equals(""))
                        drugNurseID = drugNurseID.Substring(0, drugNurseID.Length - 1);

                    PlanDataSet.DrugNurseDetailTableAdapter.Fill(PlanDataSet.Instance.DrugNurseDetail, HospitalUtil.getHospitalId(), userID, drugNurseID, dateEditStart.Text, dateEditEnd.Text);
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void reloadLast()
            {
                string userID = "";
                if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                {
                    userID = UserUtil.getUserId();
                }
                else
                {
                    userID = "";
                }

                var drugNurseID = "";
                if (checkEditAll.Checked)
                {
                    pivotGridFieldPatient.Width = 200;
                    //for (var i = 0; i < PlanDataSet.Instance.DrugNurseDetail.Rows.Count; i++)
                    //    drugNurseID += PlanDataSet.Instance.DrugNurseDetail.Rows[i] + "~";
                }
                else
                {
                    int[] rowID = gridLookUpEditPatient.Properties.View.GetSelectedRows();
                    for (int i = 0; i < rowID.Length; i++)
                        drugNurseID += gridLookUpEditPatient.Properties.View.GetDataRow(rowID[i])["id"] + "~";
                }
                if (!drugNurseID.Equals(""))
                    drugNurseID = drugNurseID.Substring(0, drugNurseID.Length - 1);

                PlanDataSet.DrugNurseDetailTableAdapter.Fill(PlanDataSet.Instance.DrugNurseDetail, HospitalUtil.getHospitalId(), userID, drugNurseID, dateEditStart.Text, dateEditEnd.Text);
            }
        
            private void reloadPatient()
            {
                ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                string userID = "";
                if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                {
                    userID = UserUtil.getUserId();
                }
                else
                {
                    userID = "";
                }
                PlanDataSet.DrugNurseTableAdapter.Fill(PlanDataSet.Instance.DrugNurse, HospitalUtil.getHospitalId(), userID, dateEditStart.Text, dateEditEnd.Text);
                ProgramUtil.closeWaitDialog();
            }

            private void addRow()
            {
                NurseUpForm.Instance.showForm(1, Instance);
            }

            private void updateRow()
            {
                if (PlanDataSet.Instance.DrugNurseDetail.Rows.Count > 0 && pivotGridControl.Cells.GetFocusedCellInfo() != null && pivotGridControl.Cells.GetFocusedCellInfo().Value != null)
                {   
                    NurseUpForm.Instance.showForm(2, Instance);
                }
            }

            private void deleteRow()
            {
                reloadLast();
                if (PlanDataSet.Instance.DrugNurseDetail.Rows.Count > 0 && pivotGridControl.Cells.GetFocusedCellInfo() != null && pivotGridControl.Cells.GetFocusedCellInfo().Value != null)
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    string[] values = getSelectedValues();
                    DialogResult dialogResult = XtraMessageBox.Show(values[3] + " сувилагчийн " + values[0] + " өдрийн " + "'" + values[9] + "'" +" эмийг устгах уу", "Эмчлүүлэгчийн эм устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        double confirmCount = Convert.ToDouble(values[6]) + Convert.ToDouble(values[7]);
                        UserDataSet.QueriesTableAdapter.update_drug_nurse_journal(HospitalUtil.getHospitalId(), values[8], values[4], confirmCount); 

                        DataRow[] rows = PlanDataSet.Instance.DrugNurseDetail.Select("id = '" + values[1] + "' AND hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                        for (var i = 0; i < rows.Length; i++)
                        {
                            DataRow[] detail = PlanDataSet.Instance.DrugNurseDetail.Select("drugNurseID = '" + values[5] + "' AND hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                            if (detail.Length == 1)
                            {
                                DataRow[] journal = PlanDataSet.Instance.DrugNurse.Select("id = '" + values[5] + "' AND hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                                if (journal.Length > 0)
                                    journal[0].Delete();
                            }
                            rows[i].Delete();
                        }
                        PlanDataSet.DrugNurseDetailBindingSource.EndEdit();
                        PlanDataSet.DrugNurseDetailTableAdapter.Update(PlanDataSet.Instance.DrugNurseDetail);
                        PlanDataSet.DrugNurseTableAdapter.Update(PlanDataSet.Instance.DrugNurse);
                        refreshData();     
                    }
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void deleteSelectedRow()
            {
                reloadLast();
                if (PlanDataSet.Instance.DrugNurseDetail.Rows.Count > 0 && pivotGridControl.Cells.GetFocusedCellInfo() != null && pivotGridControl.Cells.GetFocusedCellInfo().Value != null)
                {
                    string[] values = getSelectedValues();
                    DialogResult dialogResult = XtraMessageBox.Show("Сувилагч " + "'" + values[3] + "'" + "-н " + values[0] + " өдрийн эмийн олголтыг усгах уу", "Эмчлүүлэгчид эм олголт устгах", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                        checkedPatient();
                        DataRow[] detail = PlanDataSet.Instance.DrugNurseDetail.Select("drugNurseID = '" + values[5] + "' AND hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                        for (var i = 0; i < detail.Length; i++)
                        {
                            detail[i].Delete();
                        }
                        DataRow[] journal = PlanDataSet.Instance.DrugNurse.Select("id = '" + values[5] + "' AND hospitalID = '" + HospitalUtil.getHospitalId() + "'");
                        if (journal.Length > 0)
                            journal[0].Delete();
                        PlanDataSet.DrugNurseDetailTableAdapter.Update(PlanDataSet.Instance.DrugNurseDetail);
                        PlanDataSet.DrugNurseBindingSource.EndEdit();
                        PlanDataSet.DrugNurseTableAdapter.Update(PlanDataSet.Instance.DrugNurse);
                        refreshData();
                        ProgramUtil.closeWaitDialog();
                    }
                }                      
            }
            
            public void reloadPatient(DateTime start, DateTime end, string patientID)
            {
                dateEditStart.DateTime = start;
                dateEditEnd.DateTime = end;
                checkEditAll.Checked = false;
                int[] rowID = gridLookUpEditPatient.Properties.View.GetSelectedRows();
                for (int i = 0; i < rowID.Length; i++)
                    gridLookUpEditPatient.Properties.View.UnselectRow(rowID[i]);

                if (gridLookUpEditPatient.Properties.View.RowCount == 0)
                {
                    gridLookUpEditPatient.ShowPopup();
                    gridLookUpEditPatient.ClosePopup();
                }
                for(int i = 0; i < gridLookUpEditPatient.Properties.View.RowCount; i++){
                    if(gridLookUpEditPatient.Properties.View.GetDataRow(i)["id"].ToString().Equals(patientID)){
                        gridLookUpEditPatient.Properties.View.SelectRow(i);
                        break;
                    }
                }

                reload();
            }

            public string[] getSelectedValues()
            {
                string[] ret = { null, null, null, null, null, null, null, null, null, null};

                PivotCellEventArgs focus = pivotGridControl.Cells.GetFocusedCellInfo();
                PivotDrillDownDataSource source = focus.CreateDrillDownDataSource();

                ret[0] = Convert.ToDateTime(source[0]["date"]).ToString("yyyy-MM-dd HH:mm:ss");
                ret[1] = source[0]["id"].ToString();
                ret[2] = source[0]["giveNurseID"].ToString();
                ret[3] = source[0]["userName"].ToString();
                ret[4] = source[0]["drugJournalID"].ToString();
                ret[5] = source[0]["drugNurseID"].ToString();
                ret[6] = source[0]["confirmCount"].ToString();
                ret[7] = source[0]["giveCount"].ToString();
                ret[8] = source[0]["drugDetailID"].ToString();
                ret[9] = source[0]["medicineName"].ToString();

                return ret;
            }

            public string getSelectedPatient()
            {
                if (gridLookUpEditPatient.EditValue != null)
                    return gridLookUpEditPatient.EditValue.ToString();

                return null;
            }

            public DateTime getStartDate()
            {
                return dateEditStart.DateTime;
            }

            public DateTime getEndDate()
            {
                return dateEditEnd.DateTime;
            }

            public void refreshData()
            {
                pivotGridControl.RefreshData();
            }

            private void checkedPatient()
            {
                double confirmCount;
                string[] values = getSelectedValues();
                DataRow[] detail = PlanDataSet.Instance.DrugNurseDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND drugJournalID = '" + values[4] + "' AND drugNurseID = '" + values[5] + "'");
                for (int i = 0; i < detail.Length; i++)
                {
                    confirmCount = Convert.ToDouble(detail[i]["confirmCount"]) + Convert.ToDouble(detail[i]["giveCount"]);
                    UserDataSet.QueriesTableAdapter.update_drug_nurse_journal(HospitalUtil.getHospitalId(), detail[i]["drugDetailID"].ToString(), values[4].ToString(), confirmCount);
                }           
            }

        #endregion
        
        #region Хэвлэх, хөрвүүлэх функц

            private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemPrint.Caption;
                dropDownButtonPrint.Image = barButtonItemPrint.Glyph;

                //PatientChooseForm.Instance.showForm();
            }

            private void barButtonItemBegBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
            //    dropDownButtonPrint.Text = barButtonItemAllPatient.Caption;
            //    dropDownButtonPrint.Image = barButtonItemAllPatient.Glyph;

            //    if (!checkDate())
            //        return;

            //    PatientMedicineReport.Instance.initAndShow(dateEditStart.Text, dateEditEnd.Text);
            }

            private void barButtonItemEndBal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                //dropDownButtonPrint.Text = barButtonItemEndBal.Caption;
                //dropDownButtonPrint.Image = barButtonItemEndBal.Glyph;
            }

            private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemConvert.Caption;
                dropDownButtonPrint.Image = barButtonItemConvert.Glyph;

                if (PlanDataSet.Instance.DrugNurseDetail.Rows.Count > 0)
                    ProgramUtil.convertGrid(pivotGridControl, "Эмчлүүлэгчид эм олгох");
            }

        #endregion

            private void controlNavigator_Click(object sender, EventArgs e)
            {

            }

    }
}