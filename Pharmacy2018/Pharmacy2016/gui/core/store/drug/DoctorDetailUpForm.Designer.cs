﻿namespace Pharmacy2016.gui.core.store.drug
{
    partial class DoctorDetailUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DoctorDetailUpForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditBreakDate = new DevExpress.XtraEditors.DateEdit();
            this.checkEditIsBreak = new DevExpress.XtraEditors.CheckEdit();
            this.spinEditEachDay = new DevExpress.XtraEditors.SpinEdit();
            this.gridLookUpEditMedicine = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonBackTreatment = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSaveTreatment = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditTreatmentEndDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditTreatmentStartDate = new DevExpress.XtraEditors.DateEdit();
            this.spinEditDuration = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditCount = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.popupContainerControlMed = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControlMed = new DevExpress.XtraGrid.GridControl();
            this.gridViewMed = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDupCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLatinName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnShape = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSerial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPrice = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.popupContainerEditMedicineBal = new DevExpress.XtraEditors.PopupContainerEdit();
            this.checkEditMultiSelection = new DevExpress.XtraEditors.CheckEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsBreak.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEachDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDuration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMed)).BeginInit();
            this.popupContainerControlMed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditMedicineBal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMultiSelection.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 244);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 12;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(51, 205);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(75, 13);
            this.labelControl8.TabIndex = 39;
            this.labelControl8.Text = "Зогсоох өдөр :";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl1.Location = new System.Drawing.Point(26, 180);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(100, 13);
            this.labelControl1.TabIndex = 38;
            this.labelControl1.Text = "Эмчилгээг зогсоох :";
            // 
            // dateEditBreakDate
            // 
            this.dateEditBreakDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditBreakDate.EditValue = null;
            this.dateEditBreakDate.Location = new System.Drawing.Point(132, 202);
            this.dateEditBreakDate.Name = "dateEditBreakDate";
            this.dateEditBreakDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBreakDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBreakDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBreakDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBreakDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.ReadOnly = true;
            this.dateEditBreakDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditBreakDate.TabIndex = 51;
            this.dateEditBreakDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditBreakDate_KeyUp);
            // 
            // checkEditIsBreak
            // 
            this.checkEditIsBreak.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkEditIsBreak.EnterMoveNextControl = true;
            this.checkEditIsBreak.Location = new System.Drawing.Point(132, 177);
            this.checkEditIsBreak.Name = "checkEditIsBreak";
            this.checkEditIsBreak.Properties.Caption = "";
            this.checkEditIsBreak.Size = new System.Drawing.Size(26, 19);
            this.checkEditIsBreak.TabIndex = 50;
            this.checkEditIsBreak.CheckedChanged += new System.EventHandler(this.checkEditIsBreak_CheckedChanged);
            // 
            // spinEditEachDay
            // 
            this.spinEditEachDay.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditEachDay.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditEachDay.EnterMoveNextControl = true;
            this.spinEditEachDay.Location = new System.Drawing.Point(132, 125);
            this.spinEditEachDay.Name = "spinEditEachDay";
            this.spinEditEachDay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditEachDay.Properties.DisplayFormat.FormatString = "n0";
            this.spinEditEachDay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditEachDay.Properties.EditFormat.FormatString = "n0";
            this.spinEditEachDay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditEachDay.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditEachDay.Properties.Mask.EditMask = "n0";
            this.spinEditEachDay.Size = new System.Drawing.Size(100, 20);
            this.spinEditEachDay.TabIndex = 5;
            this.spinEditEachDay.EditValueChanged += new System.EventHandler(this.spinEditEachDay_EditValueChanged);
            // 
            // gridLookUpEditMedicine
            // 
            this.gridLookUpEditMedicine.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditMedicine.EditValue = "";
            this.gridLookUpEditMedicine.Location = new System.Drawing.Point(132, 99);
            this.gridLookUpEditMedicine.Name = "gridLookUpEditMedicine";
            this.gridLookUpEditMedicine.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditMedicine.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Нэмэх", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditMedicine.Properties.DisplayMember = "name";
            this.gridLookUpEditMedicine.Properties.NullText = "";
            this.gridLookUpEditMedicine.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditMedicine.Properties.ValueMember = "id";
            this.gridLookUpEditMedicine.Properties.View = this.gridView3;
            this.gridLookUpEditMedicine.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditMedicine.TabIndex = 4;
            this.gridLookUpEditMedicine.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditMedicine_ButtonClick);
            this.gridLookUpEditMedicine.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditMedicine_KeyUp);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn15,
            this.gridColumn18,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn17,
            this.gridColumn16});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Нэр";
            this.gridColumn15.CustomizationCaption = "Худалдааны нэр";
            this.gridColumn15.FieldName = "name";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 85;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "ОУ -н ангилал";
            this.gridColumn16.CustomizationCaption = "ОУ -н ангилал";
            this.gridColumn16.FieldName = "medicineTypeInterName";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 5;
            this.gridColumn16.Width = 85;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Ангилал";
            this.gridColumn17.CustomizationCaption = "Эмийн ангилал";
            this.gridColumn17.FieldName = "medicineTypeName";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 4;
            this.gridColumn17.Width = 81;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Тун хэмжээ";
            this.gridColumn18.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn18.FieldName = "unitName";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 1;
            this.gridColumn18.Width = 81;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Хэлбэр";
            this.gridColumn1.CustomizationCaption = "Хэлбэр";
            this.gridColumn1.FieldName = "shapeName";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 81;
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl6.Location = new System.Drawing.Point(47, 76);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(79, 13);
            this.labelControl6.TabIndex = 36;
            this.labelControl6.Text = "Дуусах огноо*:";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(44, 24);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(82, 13);
            this.labelControl5.TabIndex = 34;
            this.labelControl5.Text = "Эхлэсэн огноо*:";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Location = new System.Drawing.Point(15, 128);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(111, 13);
            this.labelControl4.TabIndex = 33;
            this.labelControl4.Text = "Өдөрт хэрэглэх тоо*:";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl3.Location = new System.Drawing.Point(19, 50);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(107, 13);
            this.labelControl3.TabIndex = 31;
            this.labelControl3.Text = "Үргэлжлэх хугацаа*:";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(103, 102);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(23, 13);
            this.labelControl2.TabIndex = 29;
            this.labelControl2.Text = "Эм*:";
            // 
            // simpleButtonBackTreatment
            // 
            this.simpleButtonBackTreatment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonBackTreatment.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonBackTreatment.Location = new System.Drawing.Point(297, 244);
            this.simpleButtonBackTreatment.Name = "simpleButtonBackTreatment";
            this.simpleButtonBackTreatment.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonBackTreatment.TabIndex = 11;
            this.simpleButtonBackTreatment.Text = "Гарах";
            this.simpleButtonBackTreatment.ToolTip = "Гарах";
            // 
            // simpleButtonSaveTreatment
            // 
            this.simpleButtonSaveTreatment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSaveTreatment.Location = new System.Drawing.Point(216, 244);
            this.simpleButtonSaveTreatment.Name = "simpleButtonSaveTreatment";
            this.simpleButtonSaveTreatment.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSaveTreatment.TabIndex = 10;
            this.simpleButtonSaveTreatment.Text = "Хадгалах";
            this.simpleButtonSaveTreatment.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSaveTreatment.Click += new System.EventHandler(this.simpleButtonSaveTreatment_Click);
            // 
            // dateEditTreatmentEndDate
            // 
            this.dateEditTreatmentEndDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditTreatmentEndDate.EditValue = null;
            this.dateEditTreatmentEndDate.Location = new System.Drawing.Point(132, 73);
            this.dateEditTreatmentEndDate.Name = "dateEditTreatmentEndDate";
            this.dateEditTreatmentEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTreatmentEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTreatmentEndDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditTreatmentEndDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditTreatmentEndDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditTreatmentEndDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditTreatmentEndDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditTreatmentEndDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditTreatmentEndDate.TabIndex = 3;
            this.dateEditTreatmentEndDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditTreatmentEndDate_KeyUp);
            // 
            // dateEditTreatmentStartDate
            // 
            this.dateEditTreatmentStartDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateEditTreatmentStartDate.EditValue = null;
            this.dateEditTreatmentStartDate.Location = new System.Drawing.Point(132, 21);
            this.dateEditTreatmentStartDate.Name = "dateEditTreatmentStartDate";
            this.dateEditTreatmentStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTreatmentStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTreatmentStartDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditTreatmentStartDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditTreatmentStartDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditTreatmentStartDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditTreatmentStartDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditTreatmentStartDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditTreatmentStartDate.TabIndex = 1;
            this.dateEditTreatmentStartDate.EditValueChanged += new System.EventHandler(this.dateEditTreatmentStartDate_EditValueChanged);
            this.dateEditTreatmentStartDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditTreatmentStartDate_KeyUp);
            // 
            // spinEditDuration
            // 
            this.spinEditDuration.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditDuration.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditDuration.EnterMoveNextControl = true;
            this.spinEditDuration.Location = new System.Drawing.Point(132, 47);
            this.spinEditDuration.Name = "spinEditDuration";
            this.spinEditDuration.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditDuration.Properties.DisplayFormat.FormatString = "n0";
            this.spinEditDuration.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditDuration.Properties.EditFormat.FormatString = "n0";
            this.spinEditDuration.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditDuration.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.spinEditDuration.Properties.Mask.EditMask = "n0";
            this.spinEditDuration.Size = new System.Drawing.Size(100, 20);
            this.spinEditDuration.TabIndex = 2;
            this.spinEditDuration.EditValueChanged += new System.EventHandler(this.dateEditTreatmentStartDate_EditValueChanged);
            // 
            // spinEditCount
            // 
            this.spinEditCount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditCount.EnterMoveNextControl = true;
            this.spinEditCount.Location = new System.Drawing.Point(131, 151);
            this.spinEditCount.Name = "spinEditCount";
            this.spinEditCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditCount.Properties.DisplayFormat.FormatString = "n2";
            this.spinEditCount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.EditFormat.FormatString = "n2";
            this.spinEditCount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditCount.Properties.Mask.EditMask = "n2";
            this.spinEditCount.Size = new System.Drawing.Size(101, 20);
            this.spinEditCount.TabIndex = 6;
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl7.Location = new System.Drawing.Point(60, 154);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(66, 13);
            this.labelControl7.TabIndex = 49;
            this.labelControl7.Text = "Тоо хэмжээ*:";
            // 
            // popupContainerControlMed
            // 
            this.popupContainerControlMed.Controls.Add(this.gridControlMed);
            this.popupContainerControlMed.Location = new System.Drawing.Point(12, 11);
            this.popupContainerControlMed.Name = "popupContainerControlMed";
            this.popupContainerControlMed.Size = new System.Drawing.Size(262, 78);
            this.popupContainerControlMed.TabIndex = 58;
            // 
            // gridControlMed
            // 
            this.gridControlMed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlMed.Location = new System.Drawing.Point(0, 0);
            this.gridControlMed.MainView = this.gridViewMed;
            this.gridControlMed.Name = "gridControlMed";
            this.gridControlMed.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemSpinEditPrice});
            this.gridControlMed.Size = new System.Drawing.Size(262, 78);
            this.gridControlMed.TabIndex = 56;
            this.gridControlMed.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewMed});
            // 
            // gridViewMed
            // 
            this.gridViewMed.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumName,
            this.gridColumnDupCount,
            this.gridColumnCount,
            this.gridColumnUnit,
            this.gridColumnShape,
            this.gridColumnSerial,
            this.gridColumnType,
            this.gridColumnLatinName});
            this.gridViewMed.GridControl = this.gridControlMed;
            this.gridViewMed.Name = "gridViewMed";
            this.gridViewMed.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewMed.OptionsSelection.MultiSelect = true;
            this.gridViewMed.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridViewMed.OptionsView.ShowGroupPanel = false;
            this.gridViewMed.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewMed_CellValueChanged);
            this.gridViewMed.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewMed_CellValueChanging);
            // 
            // gridColumName
            // 
            this.gridColumName.Caption = "Нэр";
            this.gridColumName.FieldName = "name";
            this.gridColumName.MinWidth = 100;
            this.gridColumName.Name = "gridColumName";
            this.gridColumName.OptionsColumn.ReadOnly = true;
            this.gridColumName.Visible = true;
            this.gridColumName.VisibleIndex = 1;
            this.gridColumName.Width = 100;
            // 
            // gridColumnDupCount
            // 
            this.gridColumnDupCount.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnDupCount.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnDupCount.Caption = "Өдөрт хэрэглэх тоо";
            this.gridColumnDupCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnDupCount.FieldName = "eachDay";
            this.gridColumnDupCount.MinWidth = 100;
            this.gridColumnDupCount.Name = "gridColumnDupCount";
            this.gridColumnDupCount.Visible = true;
            this.gridColumnDupCount.VisibleIndex = 2;
            this.gridColumnDupCount.Width = 100;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n0";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n0";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n0";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnCount
            // 
            this.gridColumnCount.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridColumnCount.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnCount.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumnCount.Caption = "Тоо хэмжээ";
            this.gridColumnCount.FieldName = "count";
            this.gridColumnCount.MinWidth = 75;
            this.gridColumnCount.Name = "gridColumnCount";
            this.gridColumnCount.OptionsColumn.ReadOnly = true;
            this.gridColumnCount.Visible = true;
            this.gridColumnCount.VisibleIndex = 3;
            // 
            // gridColumnLatinName
            // 
            this.gridColumnLatinName.Caption = "ОУ -н ангилал";
            this.gridColumnLatinName.CustomizationCaption = "ОУ -н ангилал";
            this.gridColumnLatinName.FieldName = "medicineTypeInterName";
            this.gridColumnLatinName.MinWidth = 100;
            this.gridColumnLatinName.Name = "gridColumnLatinName";
            this.gridColumnLatinName.OptionsColumn.ReadOnly = true;
            this.gridColumnLatinName.Visible = true;
            this.gridColumnLatinName.VisibleIndex = 8;
            this.gridColumnLatinName.Width = 100;
            // 
            // gridColumnType
            // 
            this.gridColumnType.Caption = "Ангилал";
            this.gridColumnType.FieldName = "medicineTypeName";
            this.gridColumnType.MinWidth = 100;
            this.gridColumnType.Name = "gridColumnType";
            this.gridColumnType.OptionsColumn.ReadOnly = true;
            this.gridColumnType.Visible = true;
            this.gridColumnType.VisibleIndex = 7;
            this.gridColumnType.Width = 100;
            // 
            // gridColumnUnit
            // 
            this.gridColumnUnit.Caption = "Тун хэмжээ";
            this.gridColumnUnit.FieldName = "unitName";
            this.gridColumnUnit.MinWidth = 100;
            this.gridColumnUnit.Name = "gridColumnUnit";
            this.gridColumnUnit.OptionsColumn.ReadOnly = true;
            this.gridColumnUnit.Visible = true;
            this.gridColumnUnit.VisibleIndex = 4;
            this.gridColumnUnit.Width = 100;
            // 
            // gridColumnShape
            // 
            this.gridColumnShape.Caption = "Хэлбэр";
            this.gridColumnShape.FieldName = "shapeName";
            this.gridColumnShape.MinWidth = 75;
            this.gridColumnShape.Name = "gridColumnShape";
            this.gridColumnShape.OptionsColumn.ReadOnly = true;
            this.gridColumnShape.Visible = true;
            this.gridColumnShape.VisibleIndex = 5;
            // 
            // gridColumnSerial
            // 
            this.gridColumnSerial.Caption = "Хэмжих нэгж";
            this.gridColumnSerial.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumnSerial.FieldName = "quantityName";
            this.gridColumnSerial.MinWidth = 75;
            this.gridColumnSerial.Name = "gridColumnSerial";
            this.gridColumnSerial.OptionsColumn.ReadOnly = true;
            this.gridColumnSerial.Visible = true;
            this.gridColumnSerial.VisibleIndex = 6;
            // 
            // repositoryItemSpinEditPrice
            // 
            this.repositoryItemSpinEditPrice.AutoHeight = false;
            this.repositoryItemSpinEditPrice.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPrice.DisplayFormat.FormatString = "c2";
            this.repositoryItemSpinEditPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditPrice.EditFormat.FormatString = "c2";
            this.repositoryItemSpinEditPrice.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditPrice.Mask.EditMask = "c2";
            this.repositoryItemSpinEditPrice.Name = "repositoryItemSpinEditPrice";
            // 
            // popupContainerEditMedicineBal
            // 
            this.popupContainerEditMedicineBal.Location = new System.Drawing.Point(133, 228);
            this.popupContainerEditMedicineBal.Name = "popupContainerEditMedicineBal";
            this.popupContainerEditMedicineBal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditMedicineBal.Properties.PopupFormMinSize = new System.Drawing.Size(500, 300);
            this.popupContainerEditMedicineBal.Size = new System.Drawing.Size(199, 20);
            this.popupContainerEditMedicineBal.TabIndex = 57;
            this.popupContainerEditMedicineBal.Visible = false;
            this.popupContainerEditMedicineBal.CloseUp += new DevExpress.XtraEditors.Controls.CloseUpEventHandler(this.popupContainerEditMedicineBal_CloseUp);
            // 
            // checkEditMultiSelection
            // 
            this.checkEditMultiSelection.Location = new System.Drawing.Point(244, 126);
            this.checkEditMultiSelection.Name = "checkEditMultiSelection";
            this.checkEditMultiSelection.Properties.Caption = "олон сонгох";
            this.checkEditMultiSelection.Size = new System.Drawing.Size(88, 19);
            this.checkEditMultiSelection.TabIndex = 59;
            this.checkEditMultiSelection.CheckedChanged += new System.EventHandler(this.checkEditMultiSelection_CheckedChanged);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Хэмжих нэгж";
            this.gridColumn2.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn2.FieldName = "quantityName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 3;
            // 
            // DoctorDetailUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonBackTreatment;
            this.ClientSize = new System.Drawing.Size(384, 279);
            this.Controls.Add(this.checkEditMultiSelection);
            this.Controls.Add(this.popupContainerEditMedicineBal);
            this.Controls.Add(this.popupContainerControlMed);
            this.Controls.Add(this.spinEditCount);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.dateEditBreakDate);
            this.Controls.Add(this.checkEditIsBreak);
            this.Controls.Add(this.spinEditEachDay);
            this.Controls.Add(this.gridLookUpEditMedicine);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.simpleButtonBackTreatment);
            this.Controls.Add(this.simpleButtonSaveTreatment);
            this.Controls.Add(this.dateEditTreatmentEndDate);
            this.Controls.Add(this.dateEditTreatmentStartDate);
            this.Controls.Add(this.spinEditDuration);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DoctorDetailUpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Эмчилгээ  нэмэх";
            this.Shown += new System.EventHandler(this.DoctorDetailUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsBreak.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEachDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditMedicine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTreatmentStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDuration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMed)).EndInit();
            this.popupContainerControlMed.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlMed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditMedicineBal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMultiSelection.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditBreakDate;
        private DevExpress.XtraEditors.CheckEdit checkEditIsBreak;
        private DevExpress.XtraEditors.SpinEdit spinEditEachDay;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditMedicine;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonBackTreatment;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSaveTreatment;
        private DevExpress.XtraEditors.DateEdit dateEditTreatmentEndDate;
        private DevExpress.XtraEditors.DateEdit dateEditTreatmentStartDate;
        private DevExpress.XtraEditors.SpinEdit spinEditDuration;
        private DevExpress.XtraEditors.SpinEdit spinEditCount;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlMed;
        private DevExpress.XtraGrid.GridControl gridControlMed;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewMed;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLatinName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnUnit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSerial;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDupCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPrice;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditMedicineBal;
        private DevExpress.XtraEditors.CheckEdit checkEditMultiSelection;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnShape;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
    }
}