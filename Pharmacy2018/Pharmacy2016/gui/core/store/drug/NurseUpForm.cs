﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.store.drug
{
    /*
     * Эмчлүүлэгчид эм олгох нэмэх, засах цонх.
     * 
     */
    public partial class NurseUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static NurseUpForm INSTANCE = new NurseUpForm();

        public static NurseUpForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private DataRowView DrugView;

        private int actionType;

        private List<string> oldRows = new List<string>();

        private DataRow[] detail;

        double confirmCount;

        private NurseUpForm()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            checkEditChooseAll.CheckState = CheckState.Unchecked;
            dateEditDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

            initError();
            initDataSource();
            reload();
        }

        private void initDataSource()
        {
            gridControlNurse.DataSource = PlanDataSet.Instance.DrugNurseDetail;
            gridLookUpEditDrug.Properties.DataSource = PlanDataSet.Instance.Drug1;
            gridLookUpEditUser.Properties.DataSource = PlanDataSet.SystemNurseBindingSource;
        }

        private void initError()
        {
            dateEditDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditDrug.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm(int type, XtraForm form)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }


                actionType = type;
                if (actionType == 1)
                {
                    insert();
                }
                if (actionType == 2)
                {
                    update();
                }
                ShowDialog(form);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clearData();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void initBinding()
        {
            //dateEditDate.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugNurseBindingSource, "date", true));
            //gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugNurseBindingSource, "giveNurseID", true));
            //gridLookUpEditDrug.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugNurseBindingSource, "drugJournalID", true));
        }

        private void insert()
        {
            gridColumnConfirmCount.FieldName = "confirmCount";
            readOnly(false);
            DateTime now = DateTime.Now;
            PlanDataSet.Instance.DrugNurse.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            DrugView = (DataRowView)PlanDataSet.DrugNurseBindingSource.AddNew();
            dateEditDate.EditValue = now;
            if (DrugView != null)
                gridViewNurse.ActiveFilterString = "drugNurseID = " + DrugView["id"];
        }

        private void update()
        {
            gridColumnConfirmCount.FieldName = "balCount";
            readOnly(true);
            string[] values = DrugNurseForm.Instance.getSelectedValues();
            if (values != null)
            {
                dateEditDate.EditValue = values[0];
                gridLookUpEditUser.EditValue = values[2];
                gridLookUpEditDrug.EditValue = values[4];
            }
            if (values[5] != null)
                gridViewNurse.ActiveFilterString = "drugNurseID = " + values[5];

            detail = PlanDataSet.Instance.DrugNurseDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND drugJournalID = '" + values[4] + "' AND drugNurseID = '" + values[5] + "'");
            oldRows = giveCount(detail);
        }

        public List<string> giveCount(DataRow[] selectedRow)
        {
            List<string> count = new List<string>();

            for (int i = 0; i < selectedRow.Length; i++)
            {
                count.Add(selectedRow[i]["giveCount"].ToString());
            }
            return count;
        }

        private void readOnly(bool isReadOnly)
        {
            dateEditDate.ReadOnly = isReadOnly;
            gridLookUpEditUser.ReadOnly = isReadOnly;
            gridLookUpEditDrug.ReadOnly = isReadOnly;
        }

        private void clearData()
        {
            dateEditDate.EditValue = null;
            gridLookUpEditDrug.EditValue = null;
            gridLookUpEditUser.EditValue = null;
            gridViewNurse.ClearColumnErrors();
            PlanDataSet.DrugBindingSource.Filter = "";
            //gridLookUpEditDrug.Properties.View.ActiveFilterString = "";

            checkEditChooseAll.CheckState = CheckState.Unchecked;
            PlanDataSet.DrugNurseDetailOtherBindingSource.CancelEdit();
            PlanDataSet.Instance.DrugNurseDetail.RejectChanges();

            PlanDataSet.DrugNurseBindingSource.CancelEdit();
            PlanDataSet.Instance.DrugNurse.RejectChanges();
        }

        private void NurseUpForm_Shown(object sender, EventArgs e)
        {
            if (actionType == 1)
            {
                //if (Visible)
                //    filterDate();

                if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                {
                    gridLookUpEditUser.EditValue = UserUtil.getUserId();
                    gridLookUpEditDrug.Focus();
                }
                else
                {
                    gridLookUpEditUser.Focus();
                }
            }
            else if (actionType == 2)
            {
                dateEditDate.Focus();
            }


            //dateEditDate.Focus();
            ProgramUtil.IsShowPopup = true;
            ProgramUtil.IsSaveEvent = false;
        }

        #endregion

        #region Формын event

        private void dateEditDate_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    dateEditDate.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditUser.Focus();
                }
            }
        }

        private void gridLookUpEditUser_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditUser.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditDrug.Focus();
                }
            }
        }

        private void gridLookUpEditDrug_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditDrug.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    checkEditChooseAll.Focus();
                }
            }
        }

        private void gridViewNurse_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            reload();
        }

        private void simpleButtonSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void checkEditChooseAll_CheckedChanged(object sender, EventArgs e)
        {
            chooseALL();
        }

        private void gridViewNurse_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //if(e.Column == gridColumnGiveCount)
            //{
            //    gridViewDrugDetail.SetRowCellValue(e.RowHandle, gridColumnSumPrice, Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(e.RowHandle, gridColumnGiveCount)) * Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(e.RowHandle, gridColumnConfirmPrice)));
            //}
            //if (e.Column == gridColumnGiveCount)
            //{
            //    if (gridViewNurse.ActiveEditor != null) oldValue = gridViewNurse.ActiveEditor.OldEditValue;
            //}
        }

        private void dateEditDate_EditValueChanged(object sender, EventArgs e)
        {
            PlanDataSet.DrugDetailOtherTableAdapter.Fill(PlanDataSet.Instance.DrugDetailOther, HospitalUtil.getHospitalId(), dateEditDate.Text);
            changeDrug();
        }

        private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
        {
            DataRowView view = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
            if (Visible && view != null && gridLookUpEditUser.EditValue != null && gridLookUpEditUser.EditValue != DBNull.Value)
                PlanDataSet.DrugBindingSource.Filter = "nurseWardName = '" + view["wardName"].ToString() + "'";
            changeDrug();
        }

        private void gridLookUpEditDrug_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible)
                gridViewNurse.Focus();
        }
        private void gridLookUpEditDrug_Validating(object sender, CancelEventArgs e)
        {
            if (Visible && actionType == 1)
                setNurseDetail();
        }

        private void gridLookUpEditDrug_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "reload"))
                reload();
        }

        #endregion

        #region Формын функц

        //private void filterDate()
        //{
        //    if (dateEditDate.EditValue != null && dateEditDate.EditValue != DBNull.Value)
        //    {
        //        PlanDataSet.DrugBindingSource.Filter = "date = '" + Convert.ToDateTime(dateEditDate.EditValue).ToShortDateString() +"'";
        //    }
        //}

        private void reload()
        {
            //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
            //PlanDataSet.DrugNurseDetailTableAdapter.Fill(PlanDataSet.Instance.DrugNurseDetail, "", HospitalUtil.getHospitalId(), DrugForm.Instance.getStartDate().ToString(), DrugForm.Instance.getEndDate().ToString());
            PlanDataSet.DrugTableAdapter.Fill(PlanDataSet.Instance.Drug, HospitalUtil.getHospitalId());
            PlanDataSet.SystemNurseTableAdapter.Fill(PlanDataSet.Instance.SystemNurse, HospitalUtil.getHospitalId(), SystemUtil.ALL_NURSE);
            PlanDataSet.DrugDetailOtherTableAdapter.Fill(PlanDataSet.Instance.DrugDetailOther, HospitalUtil.getHospitalId(), dateEditDate.Text);
            //ProgramUtil.closeWaitDialog();
        }

        private void changeDrug()
        {
            reload();
            List<string> list = new List<string>();
            PlanDataSet.Instance.Drug1.Clear();
            for (int i = 0; i < PlanDataSet.Instance.DrugDetailOther.Rows.Count; i++)
            {
                DataRow row = PlanDataSet.Instance.DrugDetailOther.Rows[i];
                DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                DataRow[] journal = PlanDataSet.Instance.Drug.Select("id = '" + row["drugJournalID"] + "'");
                if (user != null && PlanDataSet.Instance.Drug1.Select("drugJournalID = '" + row["id"] + "' AND hospitalID = '" + HospitalUtil.getHospitalId() + "'").Length == 0 && journal[0]["nurseWardName"].ToString().Equals(user["wardName"].ToString()))
                {
                    if (list.IndexOf(row["drugJournalID"].ToString()) == -1)
                    {
                        list.Add(row["drugJournalID"].ToString());

                        DataRow newRow = PlanDataSet.Instance.Drug1.NewRow();
                        newRow["date"] = row["date"];
                        newRow["id"] = journal[0]["id"];


                        newRow["date"] = journal[0]["date"];
                        newRow["patientName"] = journal[0]["patientName"];
                        newRow["nurseName"] = journal[0]["nurseName"];
                        newRow["nurseWardName"] = journal[0]["nurseWardName"];
                        newRow["pharmaName"] = journal[0]["pharmaName"];
                        newRow.EndEdit();
                        PlanDataSet.Instance.Drug1.Rows.Add(newRow);
                    }
                }
            }
        }

        private void setNurseDetail()
        {
            reload();
            if (gridLookUpEditDrug.EditValue != null && gridLookUpEditDrug.EditValue != DBNull.Value && !gridLookUpEditDrug.Text.ToString().Equals(""))
            {
                DataRowView drugDet = (DataRowView)gridLookUpEditDrug.GetSelectedDataRow();
                if (DrugView != null)
                {
                    DataRow[] oldRows = PlanDataSet.Instance.DrugNurseDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND drugNurseID = '" + DrugView["id"] + "'");
                    foreach (DataRow row in oldRows)
                    {
                        row.Delete();
                    }
                }

                DataRow[] rows = PlanDataSet.Instance.DrugDetailOther.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND drugJournalID = '" + gridLookUpEditDrug.EditValue + "'");
                for (int i = 0; i < rows.Length; i++)
                {
                    //PlanDataSet.Instance.DrugNurseDetail.idColumn.DefaultValue = getNotExistId(); 
                    if (!rows[i].IsNull("incTypeID"))
                    {
                        DataRowView row = (DataRowView)PlanDataSet.DrugNurseDetailOtherBindingSource.AddNew();
                        row["id"] = getNotExistId();
                        row["drugNurseID"] = DrugView["id"];
                        row["drugDetailID"] = rows[i]["id"];
                        row["patientName"] = drugDet["patientName"];
                        row["medicineID"] = rows[i]["medicineID"];
                        row["confirmCount"] = rows[i]["confirmCount"];
                        row["balCount"] = rows[i]["confirmCount"];
                        row["confirmPrice"] = rows[i]["confirmPrice"];
                        row["eachDay"] = rows[i]["eachDay"];
                        row["incTypeName"] = rows[i]["incTypeName"];
                        row["medicineName"] = rows[i]["medicineName"];
                        row["unitType"] = rows[i]["unitType"];
                        row["shape"] = rows[i]["shape"];
                        row["quantity"] = rows[i]["quantity"];
                        row["count"] = rows[i]["count"];
                        row["dupCount"] = rows[i]["dupCount"];
                        row.EndEdit();
                    }
                }
            }
        }

        private string getNotExistId()
        {
            string newID = null;
            DataRow[] rows = null;
            do
            {
                newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                rows = PlanDataSet.Instance.DrugNurseDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND drugNurseID = '" + DrugView["id"] + "' AND id = '" + newID + "'");
            } while (rows != null && rows.Length > 0);

            return newID;
        }

        private void chooseALL()
        {
            if (checkEditChooseAll.Checked)
            {
                for (int i = 0; i < gridViewNurse.RowCount; i++)
                {
                    gridViewNurse.SetRowCellValue(i, gridColumnGiveCount, gridViewNurse.GetRowCellValue(i, gridColumnEachday));
                    //gridViewNurse.SetRowCellValue(i, gridColumnConfirmMedicine, gridViewNurse.GetRowCellValue(i, gridColumnMedID));
                    //gridViewNurse.SetRowCellValue(i, gridColumnConfirmPrice, gridViewNurse.GetRowCellValue(i, gridColumnPrice));
                    gridViewNurse.Focus();
                }
            }
            else
            {
                for (int i = 0; i < gridViewNurse.RowCount; i++)
                {
                    gridViewNurse.SetRowCellValue(i, gridColumnGiveCount, 0);
                    //gridViewNurse.SetRowCellValue(i, gridColumnConfirmMedicine, null);
                    //gridViewNurse.SetRowCellValue(i, gridColumnConfirmPrice, 0);
                    gridViewNurse.Focus();
                }
            }
        }

        private bool check()
        {
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();
            if (dateEditDate.EditValue == DBNull.Value || dateEditDate.EditValue == null || dateEditDate.Text.Equals(""))
            {
                errorText = "Огноо сонгоно уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditDate.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    dateEditDate.Focus();
                }
            }
            if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.Text.Equals(""))
            {
                errorText = "Сувилагч сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditUser.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditUser.Focus();
                }
            }
            if (gridLookUpEditDrug.EditValue == DBNull.Value || gridLookUpEditDrug.EditValue == null || gridLookUpEditDrug.Text.Equals(""))
            {
                errorText = "Эмийн түүвэр сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditDrug.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditDrug.Focus();
                }
            }
            if (gridViewNurse.RowCount <= 0)
            {
                errorText = "Эмийн түүвэр оруулна уу!";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //dateEditStartDate.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    //dateEditStartDate.Focus();
                }
            }
            if (isRight)
            {
                bool exist = false;
                for (int i = 0; i < gridViewNurse.RowCount; i++)
                {
                    if (gridViewNurse.GetRowCellValue(i, gridColumnBal) != null &&
                        gridViewNurse.GetRowCellValue(i, gridColumnBal) != DBNull.Value &&
                        Convert.ToDouble(gridViewNurse.GetRowCellValue(i, gridColumnBal)) > 0 &&
                        Convert.ToDouble(gridViewNurse.GetRowCellValue(i, gridColumnGiveCount)) > 0 &&
                        Convert.ToDouble(gridViewNurse.GetRowCellValue(i, gridColumnConfirmCount)) > 0)
                    {
                        exist = true;
                        break;
                    }
                }
                if (exist)
                {
                    exist = false;
                    for (int i = 0; i < gridViewNurse.RowCount; i++)
                    {
                        if (gridViewNurse.GetRowCellValue(i, gridColumnConfirmCount) != null &&
                            Convert.ToDouble(gridViewNurse.GetRowCellValue(i, gridColumnConfirmCount)) > 0 &&
                            gridViewNurse.GetRowCellValue(i, gridColumnConfirmCount) != DBNull.Value &&
                            Convert.ToDouble(gridViewNurse.GetRowCellValue(i, gridColumnBal)) > 0)
                        {
                            //for (int j = i + 1; j < gridViewNurse.RowCount; j++)
                            //{
                            //Өгөх тоог өдөрт хэрэглэх тоотой харьцуулдаг байсныг болиулав.
                            if (gridViewNurse.GetRowCellValue(i, gridColumnConfirmCount) != null && gridViewNurse.GetRowCellValue(i, gridColumnConfirmCount) != DBNull.Value
                                && Convert.ToDouble(gridViewNurse.GetRowCellValue(i, gridColumnConfirmCount)) < (Convert.ToDouble(gridViewNurse.GetRowCellValue(i, gridColumnGiveCount))))
                            {
                                //gridViewDrugDetail.FocusedRowHandle = j;
                                exist = true;
                                break;
                            }
                            //}
                            if (exist)
                                break;
                        }
                    }
                    if (exist)
                    {
                        isRight = exist;
                        errorText = "Өгөх тоо үлдэгдэл тооноос их байж болохгүй!";
                        gridViewNurse.SetColumnError(gridColumnGiveCount, errorText);
                        text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        gridViewNurse.Focus();
                        isRight = false;
                    }
                }
                else
                {
                    isRight = exist;
                    errorText = "Өгөх тоог оруулаагүй эсвэл үлдэгдэл дууссан байна!";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridViewNurse.Focus();
                }
            }
            //if (isRight)
            //{
            //    for (int i = 0; i < gridViewNurse.RowCount; i++)
            //    {
            //        if(actionType == 1 && gridViewNurse.GetRowCellValue(i, gridColumnConfirmCount) != null && 
            //            gridViewNurse.GetRowCellValue(i, gridColumnConfirmCount) != DBNull.Value 
            //            && Convert.ToDouble(gridViewNurse.GetRowCellValue(i, gridColumnConfirmCount)) == 0)
            //        {
            //            errorText = "Үлдэгдэл дууссан байна!";
            //            gridViewNurse.SetColumnError(gridColumnConfirmCount, errorText);
            //            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
            //            isRight = false;
            //            break;
            //        }             
            //    }
            //}
            //if (isRight)
            //{
            //    for (int j = 0; j < gridViewNurse.RowCount; j++)
            //    {
            //        if (gridViewNurse.GetRowCellValue(j, gridColumnEachday) != null && gridViewNurse.GetRowCellValue(j, gridColumnEachday) != DBNull.Value && Convert.ToDouble(gridViewNurse.GetRowCellValue(j, gridColumnGiveCount)) != 0 
            //            && Convert.ToDouble(gridViewNurse.GetRowCellValue(j, gridColumnEachday)) < (Convert.ToDouble(gridViewNurse.GetRowCellValue(j, gridColumnGiveCount))))
            //        {
            //            errorText = "Өгөх тоо өдөрт хэрэглэх тооноос их байж болохгүй!";
            //            gridViewNurse.SetColumnError(gridColumnGiveCount, errorText);
            //            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
            //            isRight = false;
            //            break;
            //        }
            //    }
            //}
            if (!isRight && !text.Equals(""))
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        private void save()
        {
            if (check())
            {

                if (ProgramUtil.checkLockMedicine(dateEditDate.EditValue.ToString()))
                {
                    DataRowView user = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView drug = (DataRowView)gridLookUpEditDrug.GetSelectedDataRow();
                    if (actionType == 1)
                    {

                        DrugView["date"] = dateEditDate.EditValue;
                        DrugView["drugJournalID"] = drug["id"];
                        DrugView["giveNurseID"] = user["id"];
                        DrugView["nurseWardName"] = user["wardName"];
                        DrugView["nurseName"] = user["fullName"];
                        DrugView["roleName"] = user["roleName"];
                        DrugView["returnName"] = drug["nurseName"];
                        DrugView["wardName"] = drug["wardName"];
                        DrugView["patientName"] = drug["patientName"];
                        DrugView.EndEdit();
                    }
                    else if (actionType == 2)
                    {
                        //DataRowView nurse = (DataRowView)PlanDataSet.DrugNurseBindingSource.Current;
                        //nurse["date"] = dateEditDate.EditValue;
                        //nurse["drugJournalID"] = drug["id"];
                        //nurse["giveNurseID"] = user["id"];
                        //nurse["nurseWardName"] = user["wardName"];
                        //nurse["nurseName"] = user["fullName"];
                        //nurse["roleName"] = user["roleName"];
                        //nurse["returnName"] = drug["nurseName"];
                        //nurse["wardName"] = drug["wardName"];
                        //nurse["patientName"] = drug["patientName"];
                        //nurse.EndEdit();
                    }
                    PlanDataSet.DrugNurseDetailOtherBindingSource.EndEdit();
                    PlanDataSet.DrugNurseDetailTableAdapter.Update(PlanDataSet.Instance.DrugNurseDetail);

                    PlanDataSet.DrugNurseBindingSource.EndEdit();
                    PlanDataSet.DrugNurseTableAdapter.Update(PlanDataSet.Instance.DrugNurse);

                    if (actionType == 1)
                    {
                        for (int i = 0; i < gridViewNurse.RowCount; i++)
                        {
                            confirmCount = Convert.ToDouble(gridViewNurse.GetRowCellValue(i, gridColumnConfirmCount)) - Convert.ToDouble(gridViewNurse.GetRowCellValue(i, gridColumnGiveCount));
                            UserDataSet.QueriesTableAdapter.update_drug_nurse_journal(HospitalUtil.getHospitalId(), gridViewNurse.GetRowCellValue(i, gridColumnDrugDetailID).ToString(), drug["id"].ToString(), confirmCount);
                        }
                    }
                    if (actionType == 2)
                    {
                        for (int i = 0; i < oldRows.Count; i++)
                        {
                            double minus = Convert.ToDouble(oldRows[i]) - Convert.ToDouble(detail[i]["giveCount"]);
                            if (minus != 0)
                            {
                                if (minus < 0)
                                {
                                    string positive = minus.ToString().Split('-')[1];
                                    confirmCount = Convert.ToDouble(detail[i]["confirmCount"]) - Convert.ToDouble(positive);
                                }
                                else
                                {
                                    confirmCount = Convert.ToDouble(detail[i]["confirmCount"]) + minus;
                                }
                                UserDataSet.QueriesTableAdapter.update_drug_nurse_journal(HospitalUtil.getHospitalId(), gridViewNurse.GetRowCellValue(i, gridColumnDrugDetailID).ToString(), drug["id"].ToString(), confirmCount);
                            }
                        }
                    }
                    reload();
                    Close();
                }
                else
                {
                    XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
                }               
            }
        }

        #endregion
    }
}