﻿namespace Pharmacy2016.gui.core.store.drug
{
    partial class ConfirmForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmForm));
            this.gridControlDrug = new DevExpress.XtraGrid.GridControl();
            this.gridViewDrugDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnMedic = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnValidCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnConfirmMedicine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPackageName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOrderCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnConfirmCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnConfirmPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPrice = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnIncTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnIncType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDupCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditConfirm = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEditGiveCount = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDownload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.dateEditDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEditUser = new DevExpress.XtraEditors.TextEdit();
            this.textEditPatient = new DevExpress.XtraEditors.TextEdit();
            this.textEditWard = new DevExpress.XtraEditors.TextEdit();
            this.textEditNurse = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlWarehouse = new DevExpress.XtraEditors.LabelControl();
            this.treeListLookUpEditWarehouse = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.checkEditConfirm = new DevExpress.XtraEditors.CheckEdit();
            this.radioGroupType = new DevExpress.XtraEditors.RadioGroup();
            this.checkEditChooseAll = new DevExpress.XtraEditors.CheckEdit();
            this.treeListLookUpEditWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDrug)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDrugDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditGiveCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPatient.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNurse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditConfirm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChooseAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlDrug
            // 
            this.gridControlDrug.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDrug.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlDrug.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlDrug.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlDrug.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlDrug.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlDrug.Location = new System.Drawing.Point(0, 170);
            this.gridControlDrug.MainView = this.gridViewDrugDetail;
            this.gridControlDrug.Name = "gridControlDrug";
            this.gridControlDrug.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemGridLookUpEdit2,
            this.repositoryItemCheckEditConfirm,
            this.repositoryItemSpinEditPrice,
            this.repositoryItemSpinEditGiveCount});
            this.gridControlDrug.Size = new System.Drawing.Size(929, 241);
            this.gridControlDrug.TabIndex = 7;
            this.gridControlDrug.UseEmbeddedNavigator = true;
            this.gridControlDrug.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDrugDetail});
            this.gridControlDrug.Click += new System.EventHandler(this.gridControlDrug_Click);
            // 
            // gridViewDrugDetail
            // 
            this.gridViewDrugDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnMedic,
            this.gridColumn37,
            this.gridColumnCount,
            this.gridColumnValidCount,
            this.gridColumnConfirmMedicine,
            this.gridColumnOrderCount,
            this.gridColumnConfirmCount,
            this.gridColumnPrice,
            this.gridColumnConfirmPrice,
            this.gridColumnIncTypeID,
            this.gridColumnIncType,
            this.gridColumnMedID,
            this.gridColumn36,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumnDupCount,
            this.gridColumn2});
            this.gridViewDrugDetail.GridControl = this.gridControlDrug;
            this.gridViewDrugDetail.LevelIndent = 1;
            this.gridViewDrugDetail.Name = "gridViewDrugDetail";
            this.gridViewDrugDetail.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewDrugDetail.OptionsView.ShowFooter = true;
            this.gridViewDrugDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewDrugDetail.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewDrugDetail_CustomDrawCell);
            this.gridViewDrugDetail.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewDrugDetail_FocusedRowChanged);
            this.gridViewDrugDetail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewDrugDetail_CellValueChanged);
            this.gridViewDrugDetail.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewDrugDetail_InvalidRowException);
            this.gridViewDrugDetail.InvalidValueException += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(this.gridViewDrugDetail_InvalidValueException);
            // 
            // gridColumnMedic
            // 
            this.gridColumnMedic.Caption = "Эмийн багц";
            this.gridColumnMedic.CustomizationCaption = "Эмийн багц";
            this.gridColumnMedic.FieldName = "medicineName";
            this.gridColumnMedic.MinWidth = 75;
            this.gridColumnMedic.Name = "gridColumnMedic";
            this.gridColumnMedic.OptionsColumn.ReadOnly = true;
            this.gridColumnMedic.Visible = true;
            this.gridColumnMedic.VisibleIndex = 0;
            this.gridColumnMedic.Width = 106;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Тун хэмжээ";
            this.gridColumn37.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn37.FieldName = "unitType";
            this.gridColumn37.MinWidth = 75;
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 1;
            this.gridColumn37.Width = 106;
            // 
            // gridColumnCount
            // 
            this.gridColumnCount.Caption = "Захиалсан тоо";
            this.gridColumnCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnCount.CustomizationCaption = "Захиалсан тоо";
            this.gridColumnCount.FieldName = "count";
            this.gridColumnCount.MinWidth = 75;
            this.gridColumnCount.Name = "gridColumnCount";
            this.gridColumnCount.OptionsColumn.ReadOnly = true;
            this.gridColumnCount.Visible = true;
            this.gridColumnCount.VisibleIndex = 2;
            this.gridColumnCount.Width = 90;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnValidCount
            // 
            this.gridColumnValidCount.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnValidCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumnValidCount.Caption = "Баталсан тоо";
            this.gridColumnValidCount.CustomizationCaption = "Баталсан тоо";
            this.gridColumnValidCount.FieldName = "validCount";
            this.gridColumnValidCount.Name = "gridColumnValidCount";
            // 
            // gridColumnConfirmMedicine
            // 
            this.gridColumnConfirmMedicine.AppearanceCell.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.gridColumnConfirmMedicine.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnConfirmMedicine.Caption = "Олгосон эм";
            this.gridColumnConfirmMedicine.ColumnEdit = this.repositoryItemGridLookUpEdit2;
            this.gridColumnConfirmMedicine.CustomizationCaption = "Олгосон эм";
            this.gridColumnConfirmMedicine.FieldName = "medicinePrice";
            this.gridColumnConfirmMedicine.MinWidth = 75;
            this.gridColumnConfirmMedicine.Name = "gridColumnConfirmMedicine";
            this.gridColumnConfirmMedicine.Visible = true;
            this.gridColumnConfirmMedicine.VisibleIndex = 3;
            this.gridColumnConfirmMedicine.Width = 132;
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.DisplayMember = "name";
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.PopupFormMinSize = new System.Drawing.Size(600, 0);
            this.repositoryItemGridLookUpEdit2.ValueMember = "medicinePrice";
            this.repositoryItemGridLookUpEdit2.View = this.gridView1;
            this.repositoryItemGridLookUpEdit2.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEdit2_EditValueChanged);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn21,
            this.gridColumn1,
            this.gridColumn28,
            this.gridColumnPackageName,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn27,
            this.gridColumn30,
            this.gridColumn22,
            this.gridColumn26,
            this.gridColumn29});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumnPackageName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Нэр";
            this.gridColumn21.CustomizationCaption = "Нэр";
            this.gridColumn21.FieldName = "name";
            this.gridColumn21.MinWidth = 120;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 0;
            this.gridColumn21.Width = 120;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Үлдэгдэл";
            this.gridColumn1.CustomizationCaption = "Үлдэгдэл";
            this.gridColumn1.DisplayFormat.FormatString = "n2";
            this.gridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn1.FieldName = "count";
            this.gridColumn1.MinWidth = 100;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 100;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Үнэ";
            this.gridColumn28.CustomizationCaption = "Үнэ";
            this.gridColumn28.DisplayFormat.FormatString = "c2";
            this.gridColumn28.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn28.FieldName = "price";
            this.gridColumn28.MinWidth = 75;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 2;
            // 
            // gridColumnPackageName
            // 
            this.gridColumnPackageName.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.gridColumnPackageName.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnPackageName.Caption = "Эмийн багц";
            this.gridColumnPackageName.CustomizationCaption = "Эмийн багц";
            this.gridColumnPackageName.FieldName = "packageName";
            this.gridColumnPackageName.MinWidth = 100;
            this.gridColumnPackageName.Name = "gridColumnPackageName";
            this.gridColumnPackageName.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumnPackageName.Visible = true;
            this.gridColumnPackageName.VisibleIndex = 3;
            this.gridColumnPackageName.Width = 100;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Тун хэмжээ";
            this.gridColumn23.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn23.FieldName = "unitType";
            this.gridColumn23.MinWidth = 100;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 4;
            this.gridColumn23.Width = 100;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Хэлбэр";
            this.gridColumn24.CustomizationCaption = "Хэлбэр";
            this.gridColumn24.FieldName = "shape";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 6;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Хэмжих нэгж";
            this.gridColumn25.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn25.FieldName = "quantity";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 8;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Хүчинтэй хугацаа";
            this.gridColumn27.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn27.FieldName = "validDate";
            this.gridColumn27.MinWidth = 75;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 9;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Баркод";
            this.gridColumn30.CustomizationCaption = "Баркод";
            this.gridColumn30.FieldName = "barcode";
            this.gridColumn30.MinWidth = 75;
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 10;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "ОУ нэр";
            this.gridColumn22.CustomizationCaption = "ОУ нэр";
            this.gridColumn22.FieldName = "latinName";
            this.gridColumn22.MinWidth = 100;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 5;
            this.gridColumn22.Width = 100;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Ангилал";
            this.gridColumn26.CustomizationCaption = "Ангилал";
            this.gridColumn26.FieldName = "medicineTypeName";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 11;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Орлогын төрөл";
            this.gridColumn29.CustomizationCaption = "Орлогын төрөл";
            this.gridColumn29.FieldName = "incTypeName";
            this.gridColumn29.MinWidth = 75;
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 7;
            // 
            // gridColumnOrderCount
            // 
            this.gridColumnOrderCount.Caption = "Үлдэгдэл";
            this.gridColumnOrderCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnOrderCount.CustomizationCaption = "Үлдэгдэл";
            this.gridColumnOrderCount.FieldName = "orderCount";
            this.gridColumnOrderCount.MinWidth = 75;
            this.gridColumnOrderCount.Name = "gridColumnOrderCount";
            this.gridColumnOrderCount.OptionsColumn.ReadOnly = true;
            this.gridColumnOrderCount.Width = 88;
            // 
            // gridColumnConfirmCount
            // 
            this.gridColumnConfirmCount.AppearanceCell.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.gridColumnConfirmCount.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnConfirmCount.Caption = "Олгосон тоо";
            this.gridColumnConfirmCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnConfirmCount.CustomizationCaption = "Олгосон тоо";
            this.gridColumnConfirmCount.FieldName = "confirmCount";
            this.gridColumnConfirmCount.MinWidth = 75;
            this.gridColumnConfirmCount.Name = "gridColumnConfirmCount";
            this.gridColumnConfirmCount.Visible = true;
            this.gridColumnConfirmCount.VisibleIndex = 4;
            this.gridColumnConfirmCount.Width = 106;
            // 
            // gridColumnPrice
            // 
            this.gridColumnPrice.Caption = "Үнэ";
            this.gridColumnPrice.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnPrice.CustomizationCaption = "Нэгжийн үнэ";
            this.gridColumnPrice.FieldName = "price";
            this.gridColumnPrice.MinWidth = 75;
            this.gridColumnPrice.Name = "gridColumnPrice";
            this.gridColumnPrice.OptionsColumn.ReadOnly = true;
            // 
            // gridColumnConfirmPrice
            // 
            this.gridColumnConfirmPrice.AppearanceCell.BackColor = System.Drawing.Color.Transparent;
            this.gridColumnConfirmPrice.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnConfirmPrice.Caption = "Олгосон үнэ";
            this.gridColumnConfirmPrice.ColumnEdit = this.repositoryItemSpinEditPrice;
            this.gridColumnConfirmPrice.CustomizationCaption = "Олгосон үнэ";
            this.gridColumnConfirmPrice.FieldName = "confirmPrice";
            this.gridColumnConfirmPrice.MinWidth = 75;
            this.gridColumnConfirmPrice.Name = "gridColumnConfirmPrice";
            this.gridColumnConfirmPrice.OptionsColumn.ReadOnly = true;
            this.gridColumnConfirmPrice.Visible = true;
            this.gridColumnConfirmPrice.VisibleIndex = 5;
            this.gridColumnConfirmPrice.Width = 90;
            // 
            // repositoryItemSpinEditPrice
            // 
            this.repositoryItemSpinEditPrice.AutoHeight = false;
            this.repositoryItemSpinEditPrice.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPrice.DisplayFormat.FormatString = "c2";
            this.repositoryItemSpinEditPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditPrice.EditFormat.FormatString = "c2";
            this.repositoryItemSpinEditPrice.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditPrice.Mask.EditMask = "c2";
            this.repositoryItemSpinEditPrice.Name = "repositoryItemSpinEditPrice";
            // 
            // gridColumnIncTypeID
            // 
            this.gridColumnIncTypeID.Caption = "Орлого";
            this.gridColumnIncTypeID.CustomizationCaption = "Орлого";
            this.gridColumnIncTypeID.FieldName = "incTypeID";
            this.gridColumnIncTypeID.MinWidth = 75;
            this.gridColumnIncTypeID.Name = "gridColumnIncTypeID";
            this.gridColumnIncTypeID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumnIncType
            // 
            this.gridColumnIncType.Caption = "Орлогын төрөл";
            this.gridColumnIncType.CustomizationCaption = "Орлогын төрөл";
            this.gridColumnIncType.FieldName = "incTypeName";
            this.gridColumnIncType.MinWidth = 100;
            this.gridColumnIncType.Name = "gridColumnIncType";
            this.gridColumnIncType.OptionsColumn.ReadOnly = true;
            this.gridColumnIncType.Width = 100;
            // 
            // gridColumnMedID
            // 
            this.gridColumnMedID.Caption = "Эмийн дугаар";
            this.gridColumnMedID.FieldName = "medicineID";
            this.gridColumnMedID.Name = "gridColumnMedID";
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "ОУ нэр";
            this.gridColumn36.CustomizationCaption = "ОУ нэр";
            this.gridColumn36.FieldName = "latinName";
            this.gridColumn36.MinWidth = 75;
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 112;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Хэлбэр";
            this.gridColumn38.CustomizationCaption = "Хэлбэр";
            this.gridColumn38.FieldName = "shape";
            this.gridColumn38.MinWidth = 75;
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Хэмжих нэгж";
            this.gridColumn39.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn39.FieldName = "quantity";
            this.gridColumn39.MinWidth = 75;
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            // 
            // gridColumnDupCount
            // 
            this.gridColumnDupCount.Caption = "Хуулсан дугаар";
            this.gridColumnDupCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnDupCount.FieldName = "dupCount";
            this.gridColumnDupCount.MinWidth = 75;
            this.gridColumnDupCount.Name = "gridColumnDupCount";
            this.gridColumnDupCount.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Өгөх эм";
            this.gridColumn2.CustomizationCaption = "Өгөх эм";
            this.gridColumn2.FieldName = "confirmMedicineID";
            this.gridColumn2.Name = "gridColumn2";
            // 
            // repositoryItemCheckEditConfirm
            // 
            this.repositoryItemCheckEditConfirm.AutoHeight = false;
            this.repositoryItemCheckEditConfirm.Name = "repositoryItemCheckEditConfirm";
            // 
            // repositoryItemSpinEditGiveCount
            // 
            this.repositoryItemSpinEditGiveCount.AutoHeight = false;
            this.repositoryItemSpinEditGiveCount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditGiveCount.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEditGiveCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditGiveCount.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEditGiveCount.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditGiveCount.Mask.EditMask = "n2";
            this.repositoryItemSpinEditGiveCount.Name = "repositoryItemSpinEditGiveCount";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Controls.Add(this.simpleButtonDownload);
            this.panelControl2.Controls.Add(this.simpleButtonCancel);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 411);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(929, 50);
            this.panelControl2.TabIndex = 12;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 15);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 10;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // simpleButtonDownload
            // 
            this.simpleButtonDownload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonDownload.Location = new System.Drawing.Point(791, 15);
            this.simpleButtonDownload.Name = "simpleButtonDownload";
            this.simpleButtonDownload.Size = new System.Drawing.Size(60, 23);
            this.simpleButtonDownload.TabIndex = 8;
            this.simpleButtonDownload.Text = "Хадгалах";
            this.simpleButtonDownload.ToolTip = "Хадгалах";
            this.simpleButtonDownload.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(857, 15);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(60, 23);
            this.simpleButtonCancel.TabIndex = 9;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.dateEditDate);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.textEditUser);
            this.panelControl1.Controls.Add(this.textEditPatient);
            this.panelControl1.Controls.Add(this.textEditWard);
            this.panelControl1.Controls.Add(this.textEditNurse);
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.labelControl28);
            this.panelControl1.Controls.Add(this.labelControl27);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControlWarehouse);
            this.panelControl1.Controls.Add(this.treeListLookUpEditWarehouse);
            this.panelControl1.Controls.Add(this.checkEditConfirm);
            this.panelControl1.Controls.Add(this.radioGroupType);
            this.panelControl1.Controls.Add(this.checkEditChooseAll);
            this.panelControl1.Controls.Add(this.treeListLookUpEditWard);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(929, 170);
            this.panelControl1.TabIndex = 11;
            // 
            // dateEditDate
            // 
            this.dateEditDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditDate.EditValue = null;
            this.dateEditDate.Location = new System.Drawing.Point(694, 28);
            this.dateEditDate.Name = "dateEditDate";
            this.dateEditDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditDate.Properties.ReadOnly = true;
            this.dateEditDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditDate.TabIndex = 252;
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Location = new System.Drawing.Point(791, 148);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(79, 13);
            this.labelControl4.TabIndex = 100;
            this.labelControl4.Text = "Бүгдийг олгох :";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl3.Location = new System.Drawing.Point(59, 148);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(52, 13);
            this.labelControl3.TabIndex = 250;
            this.labelControl3.Text = "Эм олгох :";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Location = new System.Drawing.Point(650, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(38, 13);
            this.labelControl1.TabIndex = 249;
            this.labelControl1.Text = "Огноо :";
            // 
            // textEditUser
            // 
            this.textEditUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textEditUser.EnterMoveNextControl = true;
            this.textEditUser.Location = new System.Drawing.Point(239, 28);
            this.textEditUser.Name = "textEditUser";
            this.textEditUser.Properties.ReadOnly = true;
            this.textEditUser.Size = new System.Drawing.Size(200, 20);
            this.textEditUser.TabIndex = 1;
            // 
            // textEditPatient
            // 
            this.textEditPatient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditPatient.Location = new System.Drawing.Point(694, 106);
            this.textEditPatient.Name = "textEditPatient";
            this.textEditPatient.Properties.ReadOnly = true;
            this.textEditPatient.Size = new System.Drawing.Size(200, 20);
            this.textEditPatient.TabIndex = 243;
            // 
            // textEditWard
            // 
            this.textEditWard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditWard.Location = new System.Drawing.Point(694, 80);
            this.textEditWard.Name = "textEditWard";
            this.textEditWard.Properties.ReadOnly = true;
            this.textEditWard.Size = new System.Drawing.Size(200, 20);
            this.textEditWard.TabIndex = 242;
            // 
            // textEditNurse
            // 
            this.textEditNurse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditNurse.Location = new System.Drawing.Point(694, 54);
            this.textEditNurse.Name = "textEditNurse";
            this.textEditNurse.Properties.ReadOnly = true;
            this.textEditNurse.Size = new System.Drawing.Size(200, 20);
            this.textEditNurse.TabIndex = 241;
            // 
            // labelControl10
            // 
            this.labelControl10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl10.Location = new System.Drawing.Point(624, 109);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(64, 13);
            this.labelControl10.TabIndex = 246;
            this.labelControl10.Text = "Эмчлүүлэгч :";
            // 
            // labelControl28
            // 
            this.labelControl28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl28.Location = new System.Drawing.Point(653, 83);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(35, 13);
            this.labelControl28.TabIndex = 245;
            this.labelControl28.Text = "Тасаг :";
            // 
            // labelControl27
            // 
            this.labelControl27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl27.Location = new System.Drawing.Point(633, 57);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(55, 13);
            this.labelControl27.TabIndex = 244;
            this.labelControl27.Text = "Сувилагч :";
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl8.Location = new System.Drawing.Point(134, 57);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(99, 13);
            this.labelControl8.TabIndex = 44;
            this.labelControl8.Text = "Агуулахын төрөл*:";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(188, 31);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(45, 13);
            this.labelControl2.TabIndex = 45;
            this.labelControl2.Text = "Эм зүйч :";
            // 
            // labelControlWarehouse
            // 
            this.labelControlWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlWarehouse.Location = new System.Drawing.Point(172, 85);
            this.labelControlWarehouse.Name = "labelControlWarehouse";
            this.labelControlWarehouse.Size = new System.Drawing.Size(61, 13);
            this.labelControlWarehouse.TabIndex = 43;
            this.labelControlWarehouse.Text = "Эмийн сан*:";
            // 
            // treeListLookUpEditWarehouse
            // 
            this.treeListLookUpEditWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWarehouse.EditValue = "0";
            this.treeListLookUpEditWarehouse.Location = new System.Drawing.Point(239, 82);
            this.treeListLookUpEditWarehouse.Name = "treeListLookUpEditWarehouse";
            this.treeListLookUpEditWarehouse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWarehouse.Properties.DisplayMember = "name";
            this.treeListLookUpEditWarehouse.Properties.NullText = "";
            this.treeListLookUpEditWarehouse.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.treeListLookUpEditWarehouse.Properties.ValueMember = "id";
            this.treeListLookUpEditWarehouse.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWarehouse.TabIndex = 3;
            this.treeListLookUpEditWarehouse.EditValueChanged += new System.EventHandler(this.treeListLookUpEditWarehouse_EditValueChanged);
            this.treeListLookUpEditWarehouse.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWarehouse_KeyUp);
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1,
            this.treeListColumn3});
            this.treeListLookUpEdit1TreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListLookUpEdit1TreeList.KeyFieldName = "id";
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(72, 160);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsBehavior.EnableFiltering = true;
            this.treeListLookUpEdit1TreeList.OptionsBehavior.PopulateServiceColumns = true;
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.ParentFieldName = "pid";
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "Нэр";
            this.treeListColumn1.CustomizationCaption = "Агуулахын нэр";
            this.treeListColumn1.FieldName = "name";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowMove = false;
            this.treeListColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraTreeList.FilterPopupMode.List;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            this.treeListColumn1.Width = 400;
            // 
            // treeListColumn3
            // 
            this.treeListColumn3.Caption = "Дугаар";
            this.treeListColumn3.CustomizationCaption = "Агуулахын дугаар";
            this.treeListColumn3.FieldName = "number";
            this.treeListColumn3.Name = "treeListColumn3";
            this.treeListColumn3.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.treeListColumn3.Visible = true;
            this.treeListColumn3.VisibleIndex = 1;
            this.treeListColumn3.Width = 270;
            // 
            // checkEditConfirm
            // 
            this.checkEditConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEditConfirm.EnterMoveNextControl = true;
            this.checkEditConfirm.Location = new System.Drawing.Point(117, 145);
            this.checkEditConfirm.Name = "checkEditConfirm";
            this.checkEditConfirm.Properties.Caption = "";
            this.checkEditConfirm.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEditConfirm.Size = new System.Drawing.Size(21, 19);
            this.checkEditConfirm.TabIndex = 5;
            // 
            // radioGroupType
            // 
            this.radioGroupType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radioGroupType.Location = new System.Drawing.Point(239, 54);
            this.radioGroupType.Name = "radioGroupType";
            this.radioGroupType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Эмийн сан"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Тасаг")});
            this.radioGroupType.Size = new System.Drawing.Size(200, 22);
            this.radioGroupType.TabIndex = 2;
            this.radioGroupType.SelectedIndexChanged += new System.EventHandler(this.radioGroupType_SelectedIndexChanged);
            this.radioGroupType.KeyUp += new System.Windows.Forms.KeyEventHandler(this.radioGroupType_KeyUp);
            // 
            // checkEditChooseAll
            // 
            this.checkEditChooseAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChooseAll.EnterMoveNextControl = true;
            this.checkEditChooseAll.Location = new System.Drawing.Point(871, 145);
            this.checkEditChooseAll.Name = "checkEditChooseAll";
            this.checkEditChooseAll.Properties.Caption = "";
            this.checkEditChooseAll.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checkEditChooseAll.Size = new System.Drawing.Size(23, 19);
            this.checkEditChooseAll.TabIndex = 6;
            this.checkEditChooseAll.CheckedChanged += new System.EventHandler(this.checkEditChooseAll_CheckedChanged);
            // 
            // treeListLookUpEditWard
            // 
            this.treeListLookUpEditWard.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWard.Location = new System.Drawing.Point(442, 5);
            this.treeListLookUpEditWard.Name = "treeListLookUpEditWard";
            this.treeListLookUpEditWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditWard.Properties.NullText = "";
            this.treeListLookUpEditWard.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditWard.Properties.ValueMember = "id";
            this.treeListLookUpEditWard.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWard.TabIndex = 41;
            this.treeListLookUpEditWard.EditValueChanged += new System.EventHandler(this.treeListLookUpEditWard_EditValueChanged);
            this.treeListLookUpEditWard.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWard_KeyUp);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(72, 185);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Нэр";
            this.treeListColumn2.FieldName = "name";
            this.treeListColumn2.MinWidth = 32;
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // ConfirmForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(929, 461);
            this.Controls.Add(this.gridControlDrug);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfirmForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Эмийн түүвэр олголт";
            this.Shown += new System.EventHandler(this.ConfirmForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDrug)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDrugDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditGiveCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPatient.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNurse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWarehouse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditConfirm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChooseAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlDrug;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDrugDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMedic;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConfirmMedicine;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConfirmCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDownload;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.CheckEdit checkEditChooseAll;
        private DevExpress.XtraEditors.CheckEdit checkEditConfirm;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConfirmPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditConfirm;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPrice;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControlWarehouse;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWarehouse;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private DevExpress.XtraEditors.RadioGroup radioGroupType;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWard;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraEditors.TextEdit textEditUser;
        private DevExpress.XtraEditors.TextEdit textEditPatient;
        private DevExpress.XtraEditors.TextEdit textEditWard;
        private DevExpress.XtraEditors.TextEdit textEditNurse;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMedID;
        private DevExpress.XtraEditors.DateEdit dateEditDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIncTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIncType;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditGiveCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDupCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOrderCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnValidCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPackageName;
    }
}