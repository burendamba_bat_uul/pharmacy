﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.gui.core.journal.bal;
using Pharmacy2016.gui.core.info.patient;

namespace Pharmacy2016.gui.core.store.drug
{
    public partial class DrugUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static DrugUpForm INSTANCE = new DrugUpForm();

            public static DrugUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private DataRowView docView;

            private DataRowView currView;

            private DrugUpForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                spinEditCount.Properties.MaxValue = Decimal.MaxValue - 1;
                
                initError();
                initBinding();
                initDataSource();
                reload(false);
            }

            private void initDataSource()
            {
                gridLookUpEditUser.Properties.DataSource = PlanDataSet.SystemUserBindingSource;
                treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
                gridLookUpEditPatient.Properties.DataSource = PlanDataSet.PatientDrugBindingSource;
                gridLookUpEditPharma.Properties.DataSource = PlanDataSet.SystemOtherUserBindingSource;
                gridLookUpEditMedicine.Properties.DataSource = JournalDataSet.MedicineOtherBindingSource;
                //gridLookUpEditIncType.Properties.DataSource = InformationDataSet.IncTypeOtherBindingSource;
            }

            private void initError()
            {
                dateEditDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditUser.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditPatient.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditPharma.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                //gridLookUpEditIncType.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц

            public void showForm(int type, XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    actionType = type;
                    if (actionType == 1)
                    {
                        insert();
                    }
                    else if (actionType == 2)
                    {
                        update();
                    }

                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void initBinding()
            {
                dateEditDate.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "date", true));
                gridLookUpEditUser.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "nurseID", true));
                treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "nurseWardID", true));
                gridLookUpEditPatient.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "patientID", true));
                gridLookUpEditPharma.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "userID", true));
                checkEditIsSent.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "isSent", true));
                memoEditDescription.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "description", true));
            }

            private void insert()
            {
                //this.Text = "Захиалгын хуудас нэмэх цонх";
                string id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                PlanDataSet.Instance.DrugJournal.idColumn.DefaultValue = id;
                PlanDataSet.Instance.DrugDetail.drugJournalIDColumn.DefaultValue = id;

                docView = (DataRowView)PlanDataSet.DrugJournalBindingSource.AddNew();
                DateTime now = DateTime.Now;
                docView["id"] = id;
                docView["date"] = now;
                dateEditDate.DateTime = now;
                addRow();
            }

            private void update()
            {
                //this.Text = "Захиалгын хуудас засах цонх";
                docView = (DataRowView)PlanDataSet.DrugJournalBindingSource.Current;
                currView = DrugForm.Instance.getSelectedRow();
                PlanDataSet.Instance.DrugDetail.drugJournalIDColumn.DefaultValue = docView["id"];

                //gridLookUpEditIncType.EditValue = currView["incTypeID"];
                gridLookUpEditMedicine.EditValue = currView["medicineID"];
                spinEditCount.EditValue = currView["count"];
            }

            private void addRow()
            {
                PlanDataSet.Instance.DrugDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                currView = (DataRowView)PlanDataSet.DrugDetailBindingSource.AddNew();
                clearMedicine();
            }


            private void clearData()
            {
                clearErrorText();
                clearMedicine();
                //clearBinding();

                PlanDataSet.SystemUserBindingSource.Filter = "";
                //JournalDataSet.WarehouseOtherBindingSource.Filter = "";
                JournalDataSet.WardOtherBindingSource.Filter = "";
                PlanDataSet.SystemOtherUserBindingSource.Filter = "";

                PlanDataSet.DrugJournalBindingSource.CancelEdit();
                PlanDataSet.Instance.DrugJournal.RejectChanges();
                PlanDataSet.DrugDetailBindingSource.CancelEdit();
                PlanDataSet.Instance.DrugDetail.RejectChanges();
            }

            private void clearErrorText()
            {
                dateEditDate.ErrorText = "";
                gridLookUpEditUser.ErrorText = "";
                treeListLookUpEditWard.ErrorText = "";
                gridLookUpEditPatient.ErrorText = "";
                gridLookUpEditPharma.ErrorText = "";
                gridLookUpEditMedicine.ErrorText = "";
                spinEditCount.ErrorText = "";
            }

            private void clearMedicine()
            {
                //gridLookUpEditIncType.EditValue = null;
                gridLookUpEditMedicine.EditValue = null;
                spinEditCount.Value = 0;
            }

            private void DrugUpForm_Shown(object sender, EventArgs e)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                    PlanDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";

                if (actionType == 1)
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                    {
                        gridLookUpEditUser.EditValue = UserUtil.getUserId();
                        treeListLookUpEditWard.Focus();
                    }
                    else
                    {
                        gridLookUpEditUser.Focus();
                    }
                }
                else if (actionType == 2)
                {
                    gridLookUpEditMedicine.Focus();
                }

                ProgramUtil.IsShowPopup = true;
                ProgramUtil.IsSaveEvent = false;
            }

            #endregion

        #region Формын event

            private void gridLookUpEditUser_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditUser.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        treeListLookUpEditWard.Focus();
                    }
                }
            }

            private void treeListLookUpEditWard_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditWard.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditPatient.Focus();
                    }
                }
            }

            private void gridLookUpEditPatient_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditPatient.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        dateEditDate.Focus();
                    }
                }
            }

            private void dateEditDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditPharma.Focus();
                    }
                }

            }

            private void gridLookUpEditPharma_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditPharma.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        memoEditDescription.Focus();
                    }
                }
            }

            private void treeListLookUpEditWard_EditValueChanged(object sender, EventArgs e)
            {
                if (Visible && treeListLookUpEditWard.EditValue != DBNull.Value && treeListLookUpEditWard.EditValue != null)
                {
                    PlanDataSet.SystemOtherUserBindingSource.Filter = "wardName = '" + treeListLookUpEditWard.Text + "'";
                }
            }

            private void gridLookUpEditUser_EditValueChanged(object sender, EventArgs e)
            {
                if (Visible && gridLookUpEditUser.EditValue != DBNull.Value && gridLookUpEditUser.EditValue != null)
                {
                    JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(gridLookUpEditUser.EditValue.ToString());
                }
            }

            private void gridLookUpEditMedicine_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        if (!ProgramUtil.IsSaveEvent)
                        {
                            ProgramUtil.IsShowPopup = false;
                            gridLookUpEditMedicine.ShowPopup();
                        }
                        else
                        {
                            ProgramUtil.IsSaveEvent = false;
                        }
                    }
                    else
                    {
                        spinEditCount.Focus();
                    }
                }
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload(true);
            }

            private void simpleButtonSave_Click(object sender, EventArgs e)
            {
                save();
            }

            private void gridLookUpEditMedicine_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                //if (String.Equals(e.Button.Tag, "add"))
                //{
                //    MedicineBalExpForm.Instance.showForm(4, Instance);
                //}
            }

            private void gridLookUpEditPatient_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    PatientInUpForm.Instance.showForm(4, Instance);
                }
            }

        #endregion

        #region Формын функц

            private void reload(bool isReload)
            {
                //ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                PlanDataSet.SystemUserTableAdapter.Fill(PlanDataSet.Instance.SystemUser, HospitalUtil.getHospitalId(), SystemUtil.ALL_NURSE);
                PlanDataSet.SystemOtherUserTableAdapter.Fill(PlanDataSet.Instance.SystemOtherUser, HospitalUtil.getHospitalId(), SystemUtil.PHARMACIST);
                PlanDataSet.PatientDrugTableAdapter.Fill(PlanDataSet.Instance.PatientDrug, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                    JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.MedicineOther.Rows.Count == 0)
                    JournalDataSet.MedicineOtherTableAdapter.Fill(JournalDataSet.Instance.MedicineOther, HospitalUtil.getHospitalId(), "");
                if (isReload || InformationDataSet.Instance.IncTypeOther.Rows.Count == 0)
                    InformationDataSet.IncTypeOtherTableAdapter.Fill(InformationDataSet.Instance.IncTypeOther, HospitalUtil.getHospitalId());
                //ProgramUtil.closeWaitDialog();
            }

            private bool check()
            {
                //Instance.Validate();
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditDate.EditValue == DBNull.Value || dateEditDate.EditValue == null || dateEditDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditDate.ErrorText = errorText;
                    isRight = false;
                    dateEditDate.Focus();
                }
                if (gridLookUpEditUser.EditValue == DBNull.Value || gridLookUpEditUser.EditValue == null || gridLookUpEditUser.Text.Equals(""))
                {
                    errorText = "Сувилагчыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditUser.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditUser.Focus();
                    }
                }
                if (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals(""))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }

                if (gridLookUpEditPatient.EditValue == DBNull.Value || gridLookUpEditPatient.EditValue == null || gridLookUpEditPatient.Text.Equals(""))
                {
                    errorText = "Эмчлүүлэгчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditPatient.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditPatient.Focus();
                    }
                }
                if (gridLookUpEditPharma.EditValue == DBNull.Value || gridLookUpEditPharma.EditValue == null || gridLookUpEditPharma.Text.Equals(""))
                {
                    errorText = "Эм зүйчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditPharma.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditPharma.Focus();
                    }
                }
                //if (gridLookUpEditIncType.EditValue == DBNull.Value || gridLookUpEditIncType.EditValue == null || gridLookUpEditIncType.EditValue.ToString().Equals(""))
                //{
                //    errorText = "Орлогын төрлийг сонгоно уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    gridLookUpEditIncType.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        isRight = false;
                //        gridLookUpEditIncType.Focus();
                //    }
                //}
                if (gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditMedicine.Focus();
                    }
                }
                if (spinEditCount.Value == 0)
                {
                    errorText = "Тоо, хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditCount.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditCount.Focus();
                    }
                }
                if (isRight)
                {
                    DataRow[] rows = PlanDataSet.Instance.DrugDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND drugJournalID = '" + docView["id"] + "' AND id <> '" + currView["id"] + "'");
                    for (int i = 0; rows != null && i < rows.Length; i++)
                    {
                        if (gridLookUpEditMedicine.EditValue.ToString().Equals(rows[i]["medicineID"].ToString()))
                        {
                            errorText = "Эмийн нэр давхацсан байна";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            gridLookUpEditMedicine.ErrorText = errorText;
                            isRight = false;
                            gridLookUpEditMedicine.Focus();
                            break;
                        }
                    }
                }
                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                ProgramUtil.IsSaveEvent = true;
                ProgramUtil.IsShowPopup = true;
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    DataRowView nurse = (DataRowView)gridLookUpEditUser.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                    DataRowView patient = (DataRowView)gridLookUpEditPatient.GetSelectedDataRow();
                    DataRowView pharmacist = (DataRowView)gridLookUpEditPharma.GetSelectedDataRow();
                    DataRowView medicine = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
                    //if (actionType == 1)
                    //{
                        docView["nurseID"] = gridLookUpEditUser.EditValue;
                        docView["nurseName"] = nurse["fullName"];
                        docView["roleName"] = nurse["roleName"];
                        docView["nurseWardID"] = treeListLookUpEditWard.EditValue;
                        docView["nurseWardName"] = ward["name"];
                        docView["patientID"] = gridLookUpEditPatient.EditValue;
                        docView["patientName"] = patient["fullName"];
                        docView["userID"] = gridLookUpEditPharma.EditValue;
                        docView["pharmaName"] = pharmacist["fullName"];
                        docView["date"] = dateEditDate.EditValue;
                        docView["description"] = memoEditDescription.EditValue;
                        docView["isSent"] = checkEditIsSent.EditValue;
                        docView.EndEdit();
                    //}                             
                    //currView["date"] = dateEditDate.EditValue;
                    //currView["descOfDrug"] = memoEditDescription.EditValue;
                    currView["medicineID"] = gridLookUpEditMedicine.EditValue;
                    currView["medicineName"] = medicine["name"];
                    currView["latinName"] = medicine["latinName"];
                    currView["unitType"] = medicine["unitType"];
                    currView["shape"] = medicine["shape"];
                    currView["quantity"] = medicine["quantity"];
                    currView["validDate"] = medicine["validDate"];
                    currView["medicineTypeName"] = medicine["medicineTypeName"];
                    currView["count"] = spinEditCount.EditValue;
                    currView["price"] = medicine["price"];
                    //currView["incTypeID"] = gridLookUpEditIncType.EditValue;
                    //currView["incTypeName"] = gridLookUpEditIncType.Text;
                    currView.EndEdit();

                    for (int i = 0; i < PlanDataSet.DrugDetailBindingSource.Count; i++)
                    {
                        DataRowView row = (DataRowView)PlanDataSet.DrugDetailBindingSource[i];
                        if (row["drugJournalID"].ToString().Equals(docView["id"].ToString()) && !row["id"].ToString().Equals(currView["id"].ToString()))
                        {
                            row["patientID"] = gridLookUpEditPatient.EditValue;
                            row["nurseID"] = gridLookUpEditUser.EditValue;
                            row["userID"] = gridLookUpEditPharma.EditValue;
                            row["userName"] = patient["fullName"];  
                            row["date"] = dateEditDate.EditValue;
                            row["descOfDrug"] = memoEditDescription.EditValue;
                            row["nurseID"] = gridLookUpEditUser.EditValue;
                            row["patientID"] = gridLookUpEditPatient.EditValue;
                            row["userID"] = gridLookUpEditPharma.EditValue;
                            row["userName"] = nurse["fullName"];
                            row.EndEdit();
                        }
                    }

                    PlanDataSet.DrugJournalTableAdapter.Update(PlanDataSet.Instance.DrugJournal);
                    PlanDataSet.DrugDetailTableAdapter.Update(PlanDataSet.Instance.DrugDetail);

                    ProgramUtil.closeWaitDialog();

                    if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
                    {
                        Close();
                    }
                    else
                    {
                        addRow();
                        gridLookUpEditMedicine.Focus();
                    }
                }
            }

            public void addPatient(DataRowView view)
            {
                DataRowView row = (DataRowView)PlanDataSet.PatientDrugBindingSource.AddNew();
                row["id"] = view["id"];
                row["sirName"] = view["sirName"];
                row["lastName"] = view["lastName"];
                row["firstName"] = view["firstName"];
                row["birthday"] = view["birthday"];
                row["gender"] = view["gender"];
                row["isMarried"] = view["isMarried"];
                row["register"] = view["register"];
                row["phone"] = view["phone"];
                row["email"] = view["email"];
                row["isUse"] = view["isUse"];
                row["address"] = view["address"];
                row["emdNumber"] = view["emdNumber"];
                row["nddNumber"] = view["nddNumber"];
                row["education"] = view["education"];
                row["organizationName"] = view["organizationName"];
                row["positionName"] = view["positionName"];
                row["provinceName"] = view["provinceName"];
                row["specialtyName"] = view["specialtyName"];
                row["fullName"] = view["firstName"].ToString() + ' ' + view["lastName"].ToString();

                row.EndEdit();

                gridLookUpEditPatient.EditValue = view["id"];
            }

        #endregion                   
  
    }
}