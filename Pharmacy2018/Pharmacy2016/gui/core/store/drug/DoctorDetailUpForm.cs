﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.medicine;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.store.drug
{
    /*
     * Эмчилгээ нэмэх, засах цонх.
     * 
     */
    public partial class DoctorDetailUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц.

            private static DoctorDetailUpForm INSTANCE = new DoctorDetailUpForm();

            public static DoctorDetailUpForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private int actionType;

            private DataRowView currentDoctor;

            private int[] rowID;

            private DoctorDetailUpForm()
            {

            }


            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                dateEditTreatmentStartDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditTreatmentStartDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditTreatmentEndDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditTreatmentEndDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

                initError();
                initBinding();
                initDataSource();
                reload();
            }

            private void initDataSource()
            {
                gridLookUpEditMedicine.Properties.DataSource = UserDataSet.PackageMedicineBindingSource;
                gridControlMed.DataSource = UserDataSet.PackageMedicineBindingSource;
                popupContainerControlMed.Controls.Add(gridControlMed);
                popupContainerEditMedicineBal.Properties.PopupControl = popupContainerControlMed;
            }

            private void initBinding()
            {                
                gridLookUpEditMedicine.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugDetailOtherBindingSource, "medicineID", true));
                spinEditCount.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugDetailOtherBindingSource, "count", true));
                spinEditEachDay.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugDetailOtherBindingSource, "eachDay", true));
                spinEditDuration.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugDetailOtherBindingSource, "duration", true));
                dateEditTreatmentStartDate.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugDetailOtherBindingSource, "startDate", true));
                dateEditTreatmentEndDate.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugDetailOtherBindingSource, "endDate", true));
                checkEditIsBreak.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugDetailOtherBindingSource, "isBreak", true));
                dateEditBreakDate.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugDetailOtherBindingSource, "breakDate", true));
            }

            private void initError()
            {
                gridLookUpEditMedicine.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditEachDay.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditDuration.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditTreatmentStartDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditTreatmentEndDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                spinEditCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                popupContainerEditMedicineBal.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion

        #region Форм нээх, хаах функц.

            public void showForm(int type)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }
                    
                    actionType = type;
                    if (actionType == 1)
                    {
                        insertTreatment();
                    }
                    else if (actionType == 2)
                    {
                        updateTreatment();
                    }
                    else if (actionType == 3)
                    {
                        updateBreak();
                    }

                    ShowDialog();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void insertTreatment()
            {
                checkEditMultiSelection.Visible = true;
                isBreak(false);
                DateTime now = DoctorUpForm.Instance.inspectionDate();
                PlanDataSet.Instance.DrugDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                PlanDataSet.Instance.DrugDetail.drugJournalIDColumn.DefaultValue = DoctorUpForm.Instance.docView["id"];
                currentDoctor = (DataRowView)PlanDataSet.DrugDetailOtherBindingSource.AddNew();
                currentDoctor["startDate"] = now;
            }

            private void updateTreatment()
            {
                isBreak(false);
                currentDoctor = (DataRowView)PlanDataSet.DrugDetailOtherBindingSource.Current;
                dateEditBreakDate.Properties.ReadOnly = !Convert.ToBoolean(currentDoctor["isBreak"]);
                checkEditMultiSelection.Visible = false;
            }

            private void updateBreak()
            {
                isBreak(true);
                currentDoctor = (DataRowView)PlanDataSet.DrugDetailOtherBindingSource.Current;
                checkEditIsBreak.EditValue = currentDoctor["isBreak"];
                dateEditBreakDate.EditValue = currentDoctor["breakDate"];
                checkEditMultiSelection.Visible = false;
            }

            private void isBreak(bool isReadOnly)
            {
                dateEditTreatmentStartDate.Properties.ReadOnly = isReadOnly;
                spinEditDuration.Properties.ReadOnly = isReadOnly;
                dateEditTreatmentEndDate.Properties.ReadOnly = isReadOnly;
                gridLookUpEditMedicine.Properties.ReadOnly = isReadOnly;
                spinEditEachDay.Properties.ReadOnly = isReadOnly;
                spinEditCount.Properties.ReadOnly = isReadOnly;
            }
            
            private void clearData()
            {
                clearErrorText();
                checkEditMultiSelection.Checked = false;
                gridViewMed.ClearSelection();
                UserDataSet.Instance.PackageMedicine.Clear();
                //dateEditBreakDate.Properties.ReadOnly = true;
                PlanDataSet.DrugDetailOtherBindingSource.CancelEdit();              
            }

            private void clearErrorText()
            {
                gridLookUpEditMedicine.ErrorText = "";
                spinEditEachDay.ErrorText = "";
                spinEditDuration.ErrorText = "";
                dateEditTreatmentStartDate.ErrorText = "";
                dateEditTreatmentEndDate.ErrorText = "";
                spinEditCount.ErrorText = "";
                popupContainerEditMedicineBal.ErrorText = "";
            }

            private void DoctorDetailUpForm_Shown(object sender, EventArgs e)
            {
                dateEditTreatmentStartDate.Focus();
            }

            private void addMultiple(object start, object end, object value)
            {
                PlanDataSet.Instance.DrugDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                PlanDataSet.Instance.DrugDetail.drugJournalIDColumn.DefaultValue = DoctorUpForm.Instance.docView["id"];
                currentDoctor = (DataRowView)PlanDataSet.DrugDetailOtherBindingSource.AddNew();
                currentDoctor["startDate"] = start;
                currentDoctor["endDate"] = end;
                currentDoctor["duration"] = value;
                dateEditTreatmentStartDate.EditValue = start;
                dateEditTreatmentEndDate.EditValue = end;
                spinEditDuration.EditValue = value;
                clearAndNull();
                gridLookUpEditMedicine.Focus();
            }

            private void clearAndNull()
            {
                gridLookUpEditMedicine.EditValue = null;
                spinEditEachDay.Value = 0;
                dateEditBreakDate.EditValue = DBNull.Value;
                checkEditIsBreak.EditValue = false;
                spinEditCount.Value = 0;
            }

        #endregion

        #region Формын event

            private void gridLookUpEditMedicine_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditMedicine.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        spinEditEachDay.Focus();
                    }
                }
            }

            private void dateEditTreatmentStartDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditTreatmentStartDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        spinEditDuration.Focus();
                    }
                }
            }

            private void dateEditTreatmentEndDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditTreatmentEndDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditMedicine.Focus();
                    }
                }
            }

            private void dateEditBreakDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditBreakDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        simpleButtonSaveTreatment.Focus();
                    }
                }
            }

            private void dateEditTreatmentStartDate_EditValueChanged(object sender, EventArgs e)
            {
                changeDate();
                equalCount();
            }

            private void checkEditIsBreak_CheckedChanged(object sender, EventArgs e)
            {
                dateEditBreakDate.Properties.ReadOnly = !checkEditIsBreak.Checked;
                if (!checkEditIsBreak.Checked)
                    dateEditBreakDate.EditValue = DBNull.Value;
            }

            private void simpleButtonSaveTreatment_Click(object sender, EventArgs e)
            {
                saveTreatment();
            }

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload();
            }

            private void spinEditEachDay_EditValueChanged(object sender, EventArgs e)
            {
                equalCount();
            }

            private void gridLookUpEditMedicine_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                reload();
                if (String.Equals(e.Button.Tag, "add"))
                {
                    PackageMedicine.Instance.showForm(3, Instance);
                    Activate();
                }
            }

            private void gridView3_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
            {
               /* if (e.Column == gridColumnValidDate && !e.DisplayText.Equals(""))
                {
                    if (Convert.ToDateTime(e.CellValue) < DateTime.Now)
                    {
                        e.Appearance.BackColor = Color.Red;
                    }
                    else if ((Convert.ToDateTime(e.CellValue) - DateTime.Now).TotalDays <= 7)
                    {
                        e.Appearance.BackColor = Color.Gold;
                    }
                }*/
            }

        #endregion

        #region Формын функцууд.

            private void changeDate()
            {
                if (dateEditTreatmentStartDate.EditValue != DBNull.Value && dateEditTreatmentStartDate.EditValue != null &&
                    !dateEditTreatmentStartDate.EditValue.ToString().Equals("") && spinEditDuration.Value > 0)
                {
                    dateEditTreatmentEndDate.DateTime = dateEditTreatmentStartDate.DateTime.AddDays(Convert.ToDouble(spinEditDuration.Value));                  
                }
            }

            private void reload()
            {
                UserDataSet.PackageMedicineTableAdapter.Fill(UserDataSet.Instance.PackageMedicine, HospitalUtil.getHospitalId());
            }

            private void equalCount()
            {
                if (Visible)
                    spinEditCount.EditValue = spinEditDuration.Value * spinEditEachDay.Value;
            }

            private bool checkTreatment()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                Instance.Validate();

                if ((gridLookUpEditMedicine.EditValue == DBNull.Value || gridLookUpEditMedicine.EditValue == null || gridLookUpEditMedicine.EditValue.ToString().Equals("")) && !checkEditMultiSelection.Checked)
                {
                    errorText = "Эмийг сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditMedicine.ErrorText = errorText;
                    isRight = false;
                    gridLookUpEditMedicine.Focus();
                }
                if (spinEditEachDay.Value <= 0 && !checkEditMultiSelection.Checked)
                {
                    errorText = "Өдөрт уух хэмжээг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditEachDay.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditEachDay.Focus();
                       // text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    }
                }
                if (dateEditTreatmentStartDate.EditValue == DBNull.Value || dateEditTreatmentStartDate.EditValue == null || dateEditTreatmentStartDate.EditValue.ToString().Equals(""))
                {
                    errorText = "Эмчилгээ эхлэх огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditTreatmentStartDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditTreatmentStartDate.Focus();
                    }
                }
                if (spinEditDuration.Value <= 0)
                {
                    errorText = "Эмчилгээ үргэлжлэх хугацааг оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    spinEditDuration.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        spinEditDuration.Focus();
                    }
                }
                if (dateEditTreatmentEndDate.EditValue == DBNull.Value || dateEditTreatmentEndDate.EditValue == null || dateEditTreatmentEndDate.Text == "")
                {
                    errorText = "Эмчилгээ дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditTreatmentEndDate.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditTreatmentEndDate.Focus();
                    }
                }
                if (dateEditTreatmentStartDate.DateTime > dateEditTreatmentEndDate.DateTime)
                {
                    errorText = "Дуусах огноо, эхлэх огнооноос өмнө байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditTreatmentEndDate.ErrorText = errorText;
                    isRight = false;
                    dateEditTreatmentEndDate.Focus();
                }
                if (isRight && DoctorUpForm.Instance.docView != null && currentDoctor != null && !checkEditMultiSelection.Checked)
                {
                    DataRow[] rows = PlanDataSet.Instance.DrugDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND drugJournalID = '" + DoctorUpForm.Instance.docView["id"] + "' AND id <> '" + currentDoctor["id"] + "'");
                    for (int i = 0; rows != null && i < rows.Length; i++)
                    {
                        if (gridLookUpEditMedicine.EditValue.ToString().Equals(rows[i]["medicineID"].ToString()))
                        {
                            errorText = "Эм давхацсан байна";
                            text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                            gridLookUpEditMedicine.ErrorText = errorText;
                            isRight = false;
                            gridLookUpEditMedicine.Focus();
                            break;
                        }
                    }
                }

                if (isRight && checkEditMultiSelection.Checked)
                {
                    if (rowID.Length == 0)
                    {
                        errorText = "Эмээс сонгоно уу!";
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        popupContainerEditMedicineBal.ErrorText = errorText;
                        isRight = false;
                        popupContainerEditMedicineBal.Focus();
                    }
                    else
                    {
                        for (int i = 0; i < rowID.Length; i++)
                        {
                            DataRow[] rows_chk = PlanDataSet.Instance.DrugDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND drugJournalID = '" + DoctorUpForm.Instance.docView["id"] + "' AND id <> '" + currentDoctor["id"] + "' AND medicineName = '" + gridViewMed.GetRowCellValue(rowID[i], gridColumName) + "' AND unitType = '" + gridViewMed.GetRowCellValue(rowID[i], gridColumnUnit) + "'");
                            if (rows_chk.Length > 0)
                            {
                                errorText = "Эм давхацсан байна";
                                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                                popupContainerEditMedicineBal.ErrorText = errorText;
                                isRight = false;
                                popupContainerEditMedicineBal.Focus();
                                break;
                            }
                            if (Convert.ToDouble(gridViewMed.GetRowCellValue(rowID[i], gridColumnDupCount)) == 0 )
                            {
                                errorText = "Эмийн өдөрт уух тоог оруулна уу!";
                                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                                popupContainerEditMedicineBal.ErrorText = errorText;
                                isRight = false;
                                popupContainerEditMedicineBal.Focus();
                                break;
                            }
                        }
                    }

                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(this, Instance.Text, text);

                return isRight;
            }

            private void saveTreatment()
            {
                if (checkTreatment()) 
                {
                    if (checkEditMultiSelection.Checked)
                    {
                       // if (PlanDataSet.DrugDetailOtherBindingSource.Current != null)
                       //     PlanDataSet.DrugDetailOtherBindingSource.RemoveCurrent();
                        addMultipleRow();
                        Close();
                    }
                    else
                    {
                        DataRowView med = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();

                        currentDoctor["medicineID"] = med["id"];
                        currentDoctor["medicineName"] = gridLookUpEditMedicine.Text;
                        currentDoctor["confirmPrice"] = 0;//med["price"];
                        currentDoctor["unitType"] = med["unitName"];

                        currentDoctor["endDate"] = dateEditTreatmentEndDate.DateTime;
                        currentDoctor["startDate"] = dateEditTreatmentStartDate.EditValue;
                        currentDoctor["duration"] = spinEditDuration.EditValue;
                        currentDoctor["eachDay"] = spinEditEachDay.EditValue;
                        currentDoctor["isBreak"] = checkEditIsBreak.EditValue;
                        currentDoctor["breakDate"] = dateEditBreakDate.EditValue;
                        currentDoctor["count"] = spinEditCount.EditValue;

                        currentDoctor.EndEdit();

                        //PlanDataSet.DrugDetailOtherBindingSource.EndEdit();
                        gridLookUpEditMedicine.Properties.View.ActiveFilterString = "";

                        if (actionType != 3)
                        {
                            addMultiple(currentDoctor["startDate"], currentDoctor["endDate"], currentDoctor["duration"]);
                        }
                        else
                            Close();
                    }
                }  
            }


            private void addMultipleRow()
            {
                string start_ognoo = Convert.ToString(dateEditTreatmentStartDate.EditValue);
                string end_ognoo = Convert.ToString(dateEditTreatmentEndDate.EditValue);
                string break_ognoo = Convert.ToString(dateEditBreakDate.EditValue);
                for (int i = 0; i < rowID.Length; i++)
                {
                    //PlanDataSet.Instance.DrugDetail.idColumn.DefaultValue = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    //PlanDataSet.Instance.DrugDetail.drugJournalIDColumn.DefaultValue = DoctorUpForm.Instance.docView["id"];
                    DataRow medicine = gridViewMed.GetDataRow(rowID[i]);
                    //DataRow newRow = PlanDataSet.Instance.DrugDetail.NewRow();
                    currentDoctor["medicineID"] = medicine["id"];
                    currentDoctor["medicineName"] = medicine["name"];
                    currentDoctor["confirmPrice"] = 0;//medicine["price"];
                    currentDoctor["unitType"] = medicine["unitName"];

                    currentDoctor["endDate"] = end_ognoo;
                    currentDoctor["startDate"] = start_ognoo;
                    currentDoctor["duration"] = spinEditDuration.EditValue;
                    currentDoctor["eachDay"] = medicine["eachDay"];
                    currentDoctor["isBreak"] = checkEditIsBreak.EditValue;
                    if (break_ognoo == "")
                    {
                        currentDoctor["breakDate"] = dateEditBreakDate.EditValue;
                    }
                    else currentDoctor["breakDate"] = break_ognoo;
                    currentDoctor["count"] = medicine["count"];

                    currentDoctor.EndEdit();

                    addMultiple(currentDoctor["startDate"], currentDoctor["endDate"], currentDoctor["duration"]);
                    //PlanDataSet.Instance.DrugDetail.Rows.Add(newRow);                    
                }
            }

            // Шинээр нэмсэн эмийг авч байна
            public void addMedicine(DataRowView view)
            {
             /*   DataRowView row = (DataRowView)PlanDataSet.MedicineOtherBindingSource.AddNew();
                row["id"] = view["id"];
                row["name"] = view["name"];
                row["latinName"] = view["latinName"];
                row["unitType"] = view["unitName"];
                row["price"] = view["price"];
                row["medicineTypeName"] = view["medicineTypeName"];
                row["barcode"] = view["barcode"];
                row["shape"] = view["shapeName"];
                row["validDate"] = view["validDate"];
                row["serial"] = view["serial"];
                row["quantity"] = view["quantityName"];
                row.EndEdit();*/
                gridLookUpEditMedicine.EditValue = view["id"];
            }
        #endregion                              

            private void checkEditMultiSelection_CheckedChanged(object sender, EventArgs e)
            {
                if (checkEditMultiSelection.Checked)
                {
                    //spinEditCount.EditValue = 0;
                    spinEditCount.ReadOnly = true;
                    //spinEditEachDay.EditValue = 0;
                    spinEditEachDay.ReadOnly = true;                  
                    popupContainerEditMedicineBal.Visible = true;
                    popupContainerEditMedicineBal.Location = new Point(132, 99);
                    gridLookUpEditMedicine.Visible = false;
                }
                else
                {
                    spinEditCount.ReadOnly = false;
                    spinEditEachDay.ReadOnly = false;                 
                    gridLookUpEditMedicine.Visible = true;
                    popupContainerEditMedicineBal.Visible = false;
                    popupContainerEditMedicineBal.Location = new Point(132, 228);
                }
            }

            private void gridViewMed_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
            {
                

            }

            private void gridViewMed_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
            {
                if (e.Column == gridColumnDupCount)
                {
                    if (Convert.ToInt32(spinEditDuration.EditValue) > 0)
                    {
                        if (Convert.ToInt32(e.Value) > 0)
                        {
                            gridColumnCount.OptionsColumn.ReadOnly = false;
                            gridViewMed.SetFocusedRowCellValue(gridColumnCount, Convert.ToDouble(e.Value) * Convert.ToDouble(spinEditDuration.EditValue));
                            gridColumnCount.OptionsColumn.ReadOnly = true;
                        }
                        else if (Convert.ToInt32(e.Value) < 0)
                        {
                            gridViewMed.SetFocusedRowCellValue(gridColumnDupCount, 0);
                            XtraMessageBox.Show("Хасах утга оруулах боломжгүй ...");                            
                        }
                    }
                    else
                    {                        
                        XtraMessageBox.Show("Үргэлжлэх хугацааг оруулна уу ...");                        
                    }
                }
            }

            private void popupContainerEditMedicineBal_CloseUp(object sender, DevExpress.XtraEditors.Controls.CloseUpEventArgs e)
            {
                rowID = gridViewMed.GetSelectedRows();
            }
      
    }
}