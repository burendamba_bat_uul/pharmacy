﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.info.patient;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.store.drug
{
    /*
     * Эмийн түүвэр нэмэх, засах цонх.
     * 
     */
    public partial class DoctorUpForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static DoctorUpForm INSTANCE = new DoctorUpForm();

        public static DoctorUpForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private int actionType;

        public DataRowView docView;

        private DoctorUpForm()
        {

        }


        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            dateEditInspectionDate.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
            dateEditInspectionDate.Properties.MaxValue = SystemUtil.END_BAL_DATE;

            initError();
            initDataSource();
            initBinding();
        }

        private void initDataSource()
        {
            gridLookUpEditOrder.Properties.DataSource = PlanDataSet.Instance.OrderState;
            gridControlTreatment.DataSource = PlanDataSet.DrugDetailOtherBindingSource;
            gridLookUpEditDoctor.Properties.DataSource = PlanDataSet.SystemDoctor1BindingSource;
            gridLookUpEditNurse.Properties.DataSource = PlanDataSet.SystemNurse2BindingSource;
            gridLookUpEditPharma.Properties.DataSource = PlanDataSet.Instance.SystemOtherPharma;
            gridLookUpEditPatient.Properties.DataSource = PlanDataSet.PatientDrugBindingSource;
            treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
        }

        private void initBinding()
        {
            gridLookUpEditOrder.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "orderState", true));
            dateEditInspectionDate.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "date", true));
            gridLookUpEditDoctor.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "doctorID", true));
            gridLookUpEditNurse.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "nurseID", true));
            treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "nurseWardID", true));
            gridLookUpEditPatient.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "patientID", true));
            gridLookUpEditPharma.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "userID", true));
            checkEditIsSent.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "isSent", true));
            memoEditInspection.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "description", true));
            checkEditIsBreak.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugDoctorBindingSource, "isBreak", true));
            dateEditBreakDate.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugDoctorBindingSource, "breakDate", true));
        }

        private void initError()
        {
            gridLookUpEditDoctor.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditNurse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditPatient.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            gridLookUpEditPharma.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            memoEditInspection.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            dateEditInspectionDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
        }

        public DateTime inspectionDate()
        {
            return dateEditInspectionDate.DateTime;
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm(int type, XtraForm form)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }

                actionType = type;
                if (actionType == 1)
                {
                    isBreakChanged(false);
                    insertInspection();
                }
                else if (actionType == 2)
                {
                    isBreakChanged(false);
                    updateInspection();
                    setGridEdit(!Convert.ToBoolean(docView["isBreak"]));
                }
                else if (actionType == 3)
                {
                    isBreakChanged(true);
                    updateInspection();
                }
                reload();

                ShowDialog(form);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clearData();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
            }
        }

        private void insertInspection()
        {
            //if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
            //{
            //    StoreInUpForm.Instance.currentInspectionView["userID"] = UserUtil.getUserId();
            //    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
            //    gridLookUpEditDoctor.EditValue = UserUtil.getUserId();
            //}
            DateTime now = DateTime.Now;
            string id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            PlanDataSet.Instance.DrugJournal.idColumn.DefaultValue = id;
            docView = (DataRowView)PlanDataSet.DrugJournalBindingSource.AddNew();
            docView["date"] = now;
            docView["orderState"] = 1;
            setGridEdit(true);
        }

        private void updateInspection()
        {
            //if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
            //{
            //    InformationDataSet.SystemUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
            //}
            docView = (DataRowView)PlanDataSet.DrugJournalBindingSource.Current;
            checkEditIsBreak.EditValue = docView["isBreak"];
            dateEditBreakDate.EditValue = docView["breakDate"];
            //setGridEdit(!Convert.ToBoolean(docView["isBreak"]));
        }

        private void isBreakChanged(bool isEnable)
        {
            gridLookUpEditNurse.Properties.ReadOnly = isEnable;
            treeListLookUpEditWard.Properties.ReadOnly = isEnable;
            gridLookUpEditPatient.Properties.ReadOnly = isEnable;
            gridLookUpEditDoctor.Properties.ReadOnly = isEnable;
            gridLookUpEditPatient.Properties.ReadOnly = isEnable;
            gridLookUpEditPharma.Properties.ReadOnly = isEnable;
            dateEditInspectionDate.Properties.ReadOnly = isEnable;
            memoEditInspection.Properties.ReadOnly = isEnable;
            gridControlTreatment.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !isEnable;
            gridControlTreatment.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !isEnable;
            if (actionType != 3)
                gridControlTreatment.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !isEnable;
            else
                gridControlTreatment.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = isEnable;
        }

        private void setGridEdit(bool visible)
        {
            gridControlTreatment.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = visible;
            gridControlTreatment.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = visible;
            gridControlTreatment.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = visible;
        }


        private void clearData()
        {
            clearErrorText();
            dateEditBreakDate.Properties.ReadOnly = true;
            PlanDataSet.Instance.SystemOtherPharma.Rows.Clear();
            //InformationDataSet.SystemUserBindingSource.Filter = "";
            PlanDataSet.SystemNurse2BindingSource.Filter = "";
            PlanDataSet.SystemUserBindingSource.Filter = "";
            PlanDataSet.SystemDoctor1BindingSource.Filter = "";
            JournalDataSet.WardOtherBindingSource.Filter = "";
            PlanDataSet.SystemOtherUserBindingSource.Filter = "";
            PlanDataSet.DrugDetailOtherBindingSource.CancelEdit();
            PlanDataSet.Instance.DrugJournal.RejectChanges();
            PlanDataSet.DrugJournalBindingSource.CancelEdit();
            PlanDataSet.Instance.DrugDetail.RejectChanges();
        }

        private void clearErrorText()
        {
            memoEditInspection.ErrorText = "";
            dateEditInspectionDate.ErrorText = "";
            gridLookUpEditDoctor.ErrorText = "";
            gridLookUpEditNurse.ErrorText = "";
            treeListLookUpEditWard.ErrorText = "";
            gridLookUpEditPharma.ErrorText = "";
            gridLookUpEditPatient.ErrorText = "";
        }

        private void DoctorUpForm_Shown(object sender, EventArgs e)
        {
            if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
            {
                PlanDataSet.SystemNurse2BindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                gridLookUpEditNurse.EditValue = UserUtil.getUserId();
                treeListLookUpEditWard.Focus();
            }
            else
            {
                gridLookUpEditNurse.Focus();
            }

            if (UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
            {
                PlanDataSet.SystemDoctor1BindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                gridLookUpEditDoctor.EditValue = UserUtil.getUserId();
                DataRowView doctor = (DataRowView)gridLookUpEditDoctor.GetSelectedDataRow();
                PlanDataSet.SystemNurse2BindingSource.Filter = "wardName = '" + doctor["wardName"] + "'";
                PlanDataSet.SystemOtherUserBindingSource.Filter = "wardName = '" + doctor["wardName"] + "'";
                gridLookUpEditNurse.Focus();
            }
            else if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
            {
                PlanDataSet.SystemOtherUserBindingSource.Filter = "id = '" + UserUtil.getUserId() + "'";
                gridLookUpEditPharma.EditValue = UserUtil.getUserId();
                DataRowView pharma = (DataRowView)gridLookUpEditPharma.GetSelectedDataRow();
                if (pharma == null) return;
                //PlanDataSet.SystemNurse2BindingSource.Filter = "wardName = '" + pharma["wardName"] + "'";
                //PlanDataSet.SystemDoctor1BindingSource.Filter = "wardName = '" + pharma["wardName"] + "'";
                gridLookUpEditNurse.Focus();
            }
            if (actionType == 2)
            {
                addPharmacist();
                gridLookUpEditPharma.EditValue = docView["userID"];
                if (Visible && gridLookUpEditNurse.EditValue != DBNull.Value && gridLookUpEditNurse.EditValue != null)
                {
                    JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(gridLookUpEditNurse.EditValue.ToString());
                }
            }
        }

        #endregion

        #region Формын event

        private void gridLookUpEditOrder_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditOrder.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditNurse.Focus();
                }
            }
        }

        private void gridLookUpEditDoctor_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditDoctor.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditPatient.Focus();
                }
            }
        }

        private void gridLookUpEditNurse_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditNurse.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    treeListLookUpEditWard.Focus();
                }
            }
        }

        private void treeListLookUpEditWard_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditWard.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditDoctor.Focus();
                }
            }
        }

        private void gridLookUpEditPatient_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditPatient.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridLookUpEditPharma.Focus();
                }
            }
        }

        private void gridLookUpEditPharma_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    gridLookUpEditPharma.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    dateEditInspectionDate.Focus();
                }
            }
        }

        private void dateEditInspectionDate_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    dateEditInspectionDate.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    memoEditInspection.Focus();
                }
            }
        }

        private void dateEditBreakDate_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    dateEditBreakDate.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    gridControlTreatment.Focus();
                }
            }
        }

        private void gridControlTreatment_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Custom)
            {
                if (String.Equals(e.Button.Tag, "add"))
                {
                    addTreatment();
                }
                else if (String.Equals(e.Button.Tag, "delete") && gridViewTreatment.GetFocusedRow() != null)
                {
                    deleteTreatment();
                }
                else if (String.Equals(e.Button.Tag, "edit") && gridViewTreatment.GetFocusedRow() != null)
                {
                    editTreatment();
                }
            }
        }

        private void checkEditIsBreak_CheckedChanged(object sender, EventArgs e)
        {
            dateEditBreakDate.Properties.ReadOnly = !checkEditIsBreak.Checked;
            if (!checkEditIsBreak.Checked)
                dateEditBreakDate.EditValue = DBNull.Value;
        }

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            reload();
        }

        private void simpleButtonSaveInspection_Click(object sender, EventArgs e)
        {
            saveInspection();
        }

        private void gridLookUpEditPatient_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (String.Equals(e.Button.Tag, "add") && actionType != 3)
            {
                PatientInUpForm.Instance.showForm(4, Instance);
            }
        }

        private void gridLookUpEditNurse_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible && gridLookUpEditNurse.EditValue != DBNull.Value && gridLookUpEditNurse.EditValue != null)
            {
                JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(gridLookUpEditNurse.EditValue.ToString());
            }
        }

        private void treeListLookUpEditWard_EditValueChanged(object sender, EventArgs e)
        {
            if (Visible && treeListLookUpEditWard.EditValue != DBNull.Value && treeListLookUpEditWard.EditValue != null && (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID || UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID))
            {
                addPharmacist();
                PlanDataSet.SystemDoctor1BindingSource.Filter = "wardName = '" + treeListLookUpEditWard.Text + "'";
            }
        }

        private void simpleButtonbackInspection_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг хадгалах уу", "Эмийн түүвэр хадгалах", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                saveInspection();
            }
        }

        #endregion

        #region Эмчилгээ нэмж, засах, устгах функцууд.

        private void addTreatment()
        {
            if (checkInspection())
            {
                DoctorDetailUpForm.Instance.showForm(1);
            }
        }

        private void editTreatment()
        {
            if (checkInspection())
            {
                if (actionType == 3)
                {
                    DoctorDetailUpForm.Instance.showForm(3);
                }
                else
                {
                    DoctorDetailUpForm.Instance.showForm(2);
                }

            }
        }

        private void deleteTreatment()
        {
            DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Үзлэгийг устгах", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                gridViewTreatment.DeleteSelectedRows();
            }
        }

        #endregion

        #region Хадгалах, ачааллах функц

        private void reload()
        {
            PlanDataSet.SystemDoctor1TableAdapter.Fill(PlanDataSet.Instance.SystemDoctor1, HospitalUtil.getHospitalId(), SystemUtil.DOCTOR);
            PlanDataSet.SystemNurse2TableAdapter.Fill(PlanDataSet.Instance.SystemNurse2, HospitalUtil.getHospitalId(), SystemUtil.ALL_NURSE);
            PlanDataSet.PatientDrugTableAdapter.Fill(PlanDataSet.Instance.PatientDrug, HospitalUtil.getHospitalId());
            PlanDataSet.SystemOtherUserTableAdapter.Fill(PlanDataSet.Instance.SystemOtherUser, HospitalUtil.getHospitalId(), SystemUtil.PHARMACIST);
            if (JournalDataSet.Instance.WardOther.Rows.Count == 0)
                JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
            if ((treeListLookUpEditWard.EditValue != null || treeListLookUpEditWard.EditValue != DBNull.Value) && treeListLookUpEditWard.Text != "")
                addPharmacist();
            InformationDataSet.UserWardTableAdapter.Fill(InformationDataSet.Instance.UserWard, HospitalUtil.getHospitalId());
        }

        private bool checkInspection()
        {
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();
            Instance.Validate();

            if (gridLookUpEditDoctor.EditValue == DBNull.Value || gridLookUpEditDoctor.EditValue == null || gridLookUpEditDoctor.EditValue.ToString().Equals(""))
            {
                errorText = "Эмчийг сонгоно уу";
                text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditDoctor.ErrorText = errorText;
                isRight = false;
                gridLookUpEditDoctor.Focus();
            }
            if (gridLookUpEditNurse.EditValue == DBNull.Value || gridLookUpEditNurse.EditValue == null || gridLookUpEditNurse.EditValue.ToString().Equals(""))
            {
                errorText = "Сувилагчийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditNurse.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditNurse.Focus();
                }
            }
            if (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.EditValue.ToString().Equals(""))
            {
                errorText = "Сувилагчийн тасгийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                treeListLookUpEditWard.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    treeListLookUpEditWard.Focus();
                }
            }
            if (gridLookUpEditPatient.EditValue == DBNull.Value || gridLookUpEditPatient.EditValue == null || gridLookUpEditPatient.EditValue.ToString().Equals(""))
            {
                errorText = "Эмчлүүлэгчийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditPatient.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditPatient.Focus();
                }
            }
            if (gridLookUpEditPharma.EditValue == DBNull.Value || gridLookUpEditPharma.EditValue == null || gridLookUpEditPharma.EditValue.ToString().Equals(""))
            {
                errorText = "Эм зүйчийг сонгоно уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                gridLookUpEditPharma.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridLookUpEditPharma.Focus();
                }
            }
            if (dateEditInspectionDate.EditValue == DBNull.Value || dateEditInspectionDate.EditValue == null || dateEditInspectionDate.EditValue.ToString().Equals(""))
            {
                errorText = "Огноог оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditInspectionDate.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    dateEditInspectionDate.Focus();
                }
            }
            if (checkEditIsBreak.Checked && (dateEditBreakDate.EditValue == DBNull.Value || dateEditBreakDate.EditValue == null || (dateEditBreakDate.EditValue = dateEditBreakDate.EditValue.ToString().Trim()).Equals("")))
            {
                errorText = "Зогссон огноог оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                dateEditBreakDate.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    dateEditBreakDate.Focus();
                }
            }
            //if (gridViewTreatment.RowCount == 0 && actionType == 2)
            //{
            //    errorText = "Эмчилгээнд эм оруулна уу";
            //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
            //    //gridViewTreatment.ErrorText = errorText;
            //    if (isRight)
            //    {
            //        isRight = false;
            //        gridViewTreatment.Focus();
            //    }
            //}
            if (!isRight)
                ProgramUtil.showAlertDialog(this, Instance.Text, text);

            return isRight;
        }

        private bool isEmptyRows()
        {
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();
            Instance.Validate();

            if (gridViewTreatment.RowCount == 0)
            {
                errorText = "Эмчилгээнд эм оруулна уу";
                text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //gridViewTreatment.ErrorText = errorText;
                if (isRight)
                {
                    isRight = false;
                    gridViewTreatment.Focus();
                }
            }
            if (!isRight)
                ProgramUtil.showAlertDialog(this, Instance.Text, text);

            return isRight;
        }

        public void saveInspection() 
            {
                if (checkInspection() && isEmptyRows()) 
                {

                    if (ProgramUtil.checkLockMedicine(dateEditInspectionDate.EditValue.ToString()))
                    {
                        docView["nurseID"] = gridLookUpEditNurse.EditValue;
                        docView["doctorName"] = gridLookUpEditDoctor.Text;
                        docView["nurseName"] = gridLookUpEditNurse.Text;
                        docView["patientID"] = gridLookUpEditPatient.EditValue;
                        docView["patientName"] = gridLookUpEditPatient.Text;
                        docView["pharmaName"] = gridLookUpEditPharma.Text;
                        docView["nurseWardName"] = treeListLookUpEditWard.Text;
                        docView["isBreak"] = checkEditIsBreak.EditValue;
                        docView["userID"] = gridLookUpEditPharma.EditValue;
                        if (dateEditBreakDate.EditValue != null)
                            docView["breakDate"] = dateEditBreakDate.EditValue;
                        else
                            docView["breakDate"] = DBNull.Value;
                        docView.EndEdit();

                        PlanDataSet.DrugJournalBindingSource.EndEdit();
                        PlanDataSet.DrugJournalTableAdapter.Update(PlanDataSet.Instance.DrugJournal);
                        PlanDataSet.DrugDetailOtherBindingSource.EndEdit();
                        PlanDataSet.DrugDetailTableAdapter.Update(PlanDataSet.Instance.DrugDetail);
                        Close();
                    }
                    else
                    {
                        XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
                    } 
                    
                }         
            }

        public void addPatient(DataRowView view)
        {
            DataRowView row = (DataRowView)PlanDataSet.PatientDrugBindingSource.AddNew();
            row["id"] = view["id"];
            row["sirName"] = view["sirName"];
            row["lastName"] = view["lastName"];
            row["firstName"] = view["firstName"];
            row["birthday"] = view["birthday"];
            row["gender"] = view["gender"];
            row["isMarried"] = view["isMarried"];
            row["register"] = view["register"];
            row["phone"] = view["phone"];
            row["email"] = view["email"];
            row["isUse"] = view["isUse"];
            row["address"] = view["address"];
            row["emdNumber"] = view["emdNumber"];
            row["nddNumber"] = view["nddNumber"];
            row["education"] = view["education"];
            row["organizationName"] = view["organizationName"];
            row["positionName"] = view["positionName"];
            row["provinceName"] = view["provinceName"];
            row["specialtyName"] = view["specialtyName"];
            row["fullName"] = view["firstName"].ToString() + ' ' + view["lastName"].ToString();

            row.EndEdit();

            gridLookUpEditPatient.EditValue = view["id"];
        }

        public List<string> getAllWard(string WardId)
        {
            List<string> data = new List<string>();
            for (int i = 0; i < InformationDataSet.Instance.UserWard.Rows.Count; i++)
            {
                DataRow ward = InformationDataSet.Instance.UserWard.Rows[i];
                if (ward["wardID"].ToString().Equals(WardId.ToString()))
                {
                    data.Add(ward["userID"].ToString());
                }
            }
            return data;
        }

        private void addPharmacist()
        {
            PlanDataSet.Instance.SystemOtherPharma.Rows.Clear();
            List<string> ward = new List<string>();
            ward = getAllWard(treeListLookUpEditWard.EditValue.ToString());
            for (var ii = 0; ii < ward.Count; ii++)
            {
                for (int i = 0; i < PlanDataSet.Instance.SystemOtherUser.Rows.Count; i++)
                {
                    DataRow rows = PlanDataSet.Instance.SystemOtherUser.Rows[i];
                    if (ward[ii].ToString().Equals(rows["Id"].ToString()))
                    {
                        DataRow row = PlanDataSet.Instance.SystemOtherPharma.Rows.Add();
                        row["Id"] = rows["Id"];
                        row["lastName"] = rows["lastName"];
                        row["firstName"] = rows["firstName"];
                        row["lastName"] = rows["lastName"];
                        row["roleName"] = rows["roleName"];
                        row["wardName"] = rows["wardName"];
                        row.EndEdit();
                    }
                }
            }
        }

        #endregion

    }
}