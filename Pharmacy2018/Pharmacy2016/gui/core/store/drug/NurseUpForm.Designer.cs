﻿namespace Pharmacy2016.gui.core.store.drug
{
    partial class NurseUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NurseUpForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDownload = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gridLookUpEditDrug = new DevExpress.XtraEditors.GridLookUpEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridLookUpEditUser = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditChooseAll = new DevExpress.XtraEditors.CheckEdit();
            this.gridControlNurse = new DevExpress.XtraGrid.GridControl();
            this.gridViewNurse = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnConfirmMedicine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnBal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnConfirmCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnConfirmPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPrice = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnIncType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnEachday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnGiveCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSumPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDrugDetailID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditConfirm = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEditGiveCount = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDrug.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChooseAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlNurse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNurse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditGiveCount)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.simpleButtonReload);
            this.panelControl2.Controls.Add(this.simpleButtonDownload);
            this.panelControl2.Controls.Add(this.simpleButtonCancel);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 361);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(684, 50);
            this.panelControl2.TabIndex = 15;
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 15);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 10;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // simpleButtonDownload
            // 
            this.simpleButtonDownload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonDownload.Location = new System.Drawing.Point(546, 15);
            this.simpleButtonDownload.Name = "simpleButtonDownload";
            this.simpleButtonDownload.Size = new System.Drawing.Size(60, 23);
            this.simpleButtonDownload.TabIndex = 8;
            this.simpleButtonDownload.Text = "Хадгалах";
            this.simpleButtonDownload.ToolTip = "Хадгалах";
            this.simpleButtonDownload.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(612, 15);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(60, 23);
            this.simpleButtonCancel.TabIndex = 9;
            this.simpleButtonCancel.Text = "Гарах";
            this.simpleButtonCancel.ToolTip = "Гарах";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gridLookUpEditDrug);
            this.panelControl1.Controls.Add(this.gridLookUpEditUser);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.dateEditDate);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.checkEditChooseAll);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(684, 129);
            this.panelControl1.TabIndex = 14;
            // 
            // gridLookUpEditDrug
            // 
            this.gridLookUpEditDrug.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditDrug.EditValue = "";
            this.gridLookUpEditDrug.Location = new System.Drawing.Point(259, 81);
            this.gridLookUpEditDrug.Name = "gridLookUpEditDrug";
            this.gridLookUpEditDrug.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditDrug.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Эмийн түүвэр шүүх", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("gridLookUpEditDrug.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Эмийн түүвэр шүүх", "reload", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDrug.Properties.DisplayMember = "patientName";
            this.gridLookUpEditDrug.Properties.NullText = "";
            this.gridLookUpEditDrug.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditDrug.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridLookUpEditDrug.Properties.ValueMember = "id";
            this.gridLookUpEditDrug.Properties.View = this.gridView3;
            this.gridLookUpEditDrug.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditDrug_Properties_ButtonClick);
            this.gridLookUpEditDrug.Size = new System.Drawing.Size(200, 22);
            this.gridLookUpEditDrug.TabIndex = 3;
            this.gridLookUpEditDrug.EditValueChanged += new System.EventHandler(this.gridLookUpEditDrug_EditValueChanged);
            this.gridLookUpEditDrug.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditDrug_KeyUp);
            this.gridLookUpEditDrug.Validating += new System.ComponentModel.CancelEventHandler(this.gridLookUpEditDrug_Validating);
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn9,
            this.gridColumn7,
            this.gridColumn6,
            this.gridColumn10});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Огноо";
            this.gridColumn5.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumn5.CustomizationCaption = "Огноо";
            this.gridColumn5.FieldName = "date";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Эмчлүүлэгч";
            this.gridColumn9.CustomizationCaption = "Эмчлүүлэгч";
            this.gridColumn9.FieldName = "patientName";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Тасаг";
            this.gridColumn7.CustomizationCaption = "Тасаг";
            this.gridColumn7.FieldName = "nurseWardName";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 2;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Сувилагч";
            this.gridColumn6.CustomizationCaption = "Сувилагч";
            this.gridColumn6.FieldName = "nurseName";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Эм зүйч";
            this.gridColumn10.CustomizationCaption = "Эм зүйч";
            this.gridColumn10.FieldName = "pharmaName";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 4;
            // 
            // gridLookUpEditUser
            // 
            this.gridLookUpEditUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditUser.EditValue = "";
            this.gridLookUpEditUser.Location = new System.Drawing.Point(259, 55);
            this.gridLookUpEditUser.Name = "gridLookUpEditUser";
            this.gridLookUpEditUser.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditUser.Properties.DisplayMember = "fullName";
            this.gridLookUpEditUser.Properties.NullText = "";
            this.gridLookUpEditUser.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditUser.Properties.ValueMember = "id";
            this.gridLookUpEditUser.Properties.View = this.gridView2;
            this.gridLookUpEditUser.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditUser.TabIndex = 2;
            this.gridLookUpEditUser.EditValueChanged += new System.EventHandler(this.gridLookUpEditUser_EditValueChanged);
            this.gridLookUpEditUser.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditUser_KeyUp);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn3});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Дугаар";
            this.gridColumn16.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn16.FieldName = "id";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 0;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 75;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Нэр";
            this.gridColumn2.CustomizationCaption = "Нэр";
            this.gridColumn2.FieldName = "firstName";
            this.gridColumn2.MinWidth = 75;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Эрх";
            this.gridColumn4.CustomizationCaption = "Эрх";
            this.gridColumn4.FieldName = "roleName";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Тасаг";
            this.gridColumn3.CustomizationCaption = "Тасаг";
            this.gridColumn3.FieldName = "wardName";
            this.gridColumn3.MinWidth = 75;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl5.Location = new System.Drawing.Point(192, 58);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(61, 13);
            this.labelControl5.TabIndex = 254;
            this.labelControl5.Text = "Сувилагч* :";
            // 
            // dateEditDate
            // 
            this.dateEditDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditDate.EditValue = null;
            this.dateEditDate.Location = new System.Drawing.Point(259, 29);
            this.dateEditDate.Name = "dateEditDate";
            this.dateEditDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDate.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDate.Properties.EditFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.dateEditDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditDate.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.dateEditDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dateEditDate.Size = new System.Drawing.Size(150, 20);
            this.dateEditDate.TabIndex = 1;
            this.dateEditDate.EditValueChanged += new System.EventHandler(this.dateEditDate_EditValueChanged);
            this.dateEditDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditDate_KeyUp);
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Location = new System.Drawing.Point(509, 107);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(134, 13);
            this.labelControl4.TabIndex = 100;
            this.labelControl4.Text = "Өдөрт хэрэглэх тоо өгөх :";
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Location = new System.Drawing.Point(209, 32);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(44, 13);
            this.labelControl1.TabIndex = 249;
            this.labelControl1.Text = "Огноо* :";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl2.Location = new System.Drawing.Point(173, 84);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(80, 13);
            this.labelControl2.TabIndex = 45;
            this.labelControl2.Text = "Эмийн түүвэр* :";
            // 
            // checkEditChooseAll
            // 
            this.checkEditChooseAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChooseAll.EnterMoveNextControl = true;
            this.checkEditChooseAll.Location = new System.Drawing.Point(649, 104);
            this.checkEditChooseAll.Name = "checkEditChooseAll";
            this.checkEditChooseAll.Properties.Caption = "";
            this.checkEditChooseAll.Size = new System.Drawing.Size(23, 19);
            this.checkEditChooseAll.TabIndex = 4;
            this.checkEditChooseAll.CheckedChanged += new System.EventHandler(this.checkEditChooseAll_CheckedChanged);
            // 
            // gridControlNurse
            // 
            this.gridControlNurse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlNurse.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlNurse.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlNurse.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlNurse.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlNurse.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlNurse.Location = new System.Drawing.Point(0, 129);
            this.gridControlNurse.MainView = this.gridViewNurse;
            this.gridControlNurse.Name = "gridControlNurse";
            this.gridControlNurse.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemGridLookUpEdit2,
            this.repositoryItemCheckEditConfirm,
            this.repositoryItemSpinEditPrice,
            this.repositoryItemSpinEditGiveCount});
            this.gridControlNurse.Size = new System.Drawing.Size(684, 232);
            this.gridControlNurse.TabIndex = 5;
            this.gridControlNurse.UseEmbeddedNavigator = true;
            this.gridControlNurse.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewNurse});
            // 
            // gridViewNurse
            // 
            this.gridViewNurse.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnConfirmMedicine,
            this.gridColumnBal,
            this.gridColumnConfirmCount,
            this.gridColumnConfirmPrice,
            this.gridColumnIncType,
            this.gridColumnEachday,
            this.gridColumnGiveCount,
            this.gridColumnSumPrice,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumnDrugDetailID});
            this.gridViewNurse.GridControl = this.gridControlNurse;
            this.gridViewNurse.LevelIndent = 1;
            this.gridViewNurse.Name = "gridViewNurse";
            this.gridViewNurse.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewNurse.OptionsView.ShowFooter = true;
            this.gridViewNurse.OptionsView.ShowGroupPanel = false;
            this.gridViewNurse.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewNurse_CellValueChanged);
            this.gridViewNurse.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridViewNurse_InvalidRowException);
            // 
            // gridColumnConfirmMedicine
            // 
            this.gridColumnConfirmMedicine.AppearanceCell.BackColor = System.Drawing.Color.Transparent;
            this.gridColumnConfirmMedicine.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnConfirmMedicine.Caption = "Олгосон эм";
            this.gridColumnConfirmMedicine.CustomizationCaption = "Олгосон эм";
            this.gridColumnConfirmMedicine.FieldName = "medicineName";
            this.gridColumnConfirmMedicine.MinWidth = 75;
            this.gridColumnConfirmMedicine.Name = "gridColumnConfirmMedicine";
            this.gridColumnConfirmMedicine.OptionsColumn.ReadOnly = true;
            this.gridColumnConfirmMedicine.Visible = true;
            this.gridColumnConfirmMedicine.VisibleIndex = 0;
            this.gridColumnConfirmMedicine.Width = 112;
            // 
            // gridColumnBal
            // 
            this.gridColumnBal.Caption = "Олгосон тоо";
            this.gridColumnBal.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnBal.CustomizationCaption = "Олгосон тоо";
            this.gridColumnBal.FieldName = "dupCount";
            this.gridColumnBal.Name = "gridColumnBal";
            this.gridColumnBal.Visible = true;
            this.gridColumnBal.VisibleIndex = 3;
            this.gridColumnBal.Width = 73;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnConfirmCount
            // 
            this.gridColumnConfirmCount.AppearanceCell.BackColor = System.Drawing.Color.Transparent;
            this.gridColumnConfirmCount.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnConfirmCount.Caption = "Үлдэгдэл";
            this.gridColumnConfirmCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnConfirmCount.CustomizationCaption = "Үлдэгдэл";
            this.gridColumnConfirmCount.FieldName = "confirmCount";
            this.gridColumnConfirmCount.MinWidth = 75;
            this.gridColumnConfirmCount.Name = "gridColumnConfirmCount";
            this.gridColumnConfirmCount.OptionsColumn.ReadOnly = true;
            this.gridColumnConfirmCount.Visible = true;
            this.gridColumnConfirmCount.VisibleIndex = 4;
            // 
            // gridColumnConfirmPrice
            // 
            this.gridColumnConfirmPrice.AppearanceCell.BackColor = System.Drawing.Color.Transparent;
            this.gridColumnConfirmPrice.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnConfirmPrice.Caption = "Олгосон үнэ";
            this.gridColumnConfirmPrice.ColumnEdit = this.repositoryItemSpinEditPrice;
            this.gridColumnConfirmPrice.CustomizationCaption = "Олгосон үнэ";
            this.gridColumnConfirmPrice.FieldName = "confirmPrice";
            this.gridColumnConfirmPrice.MinWidth = 75;
            this.gridColumnConfirmPrice.Name = "gridColumnConfirmPrice";
            this.gridColumnConfirmPrice.OptionsColumn.ReadOnly = true;
            this.gridColumnConfirmPrice.Visible = true;
            this.gridColumnConfirmPrice.VisibleIndex = 2;
            // 
            // repositoryItemSpinEditPrice
            // 
            this.repositoryItemSpinEditPrice.AutoHeight = false;
            this.repositoryItemSpinEditPrice.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditPrice.DisplayFormat.FormatString = "c2";
            this.repositoryItemSpinEditPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditPrice.EditFormat.FormatString = "c2";
            this.repositoryItemSpinEditPrice.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditPrice.Mask.EditMask = "c2";
            this.repositoryItemSpinEditPrice.Name = "repositoryItemSpinEditPrice";
            // 
            // gridColumnIncType
            // 
            this.gridColumnIncType.Caption = "Орлогын төрөл";
            this.gridColumnIncType.CustomizationCaption = "Орлогын төрөл";
            this.gridColumnIncType.FieldName = "incTypeName";
            this.gridColumnIncType.MinWidth = 75;
            this.gridColumnIncType.Name = "gridColumnIncType";
            this.gridColumnIncType.OptionsColumn.ReadOnly = true;
            this.gridColumnIncType.Width = 118;
            // 
            // gridColumnEachday
            // 
            this.gridColumnEachday.Caption = "Өдөрт  хэрэглэх тоо";
            this.gridColumnEachday.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnEachday.CustomizationCaption = "Өдөрт  хэрэглэх тоо";
            this.gridColumnEachday.FieldName = "eachDay";
            this.gridColumnEachday.MinWidth = 75;
            this.gridColumnEachday.Name = "gridColumnEachday";
            this.gridColumnEachday.OptionsColumn.ReadOnly = true;
            this.gridColumnEachday.Visible = true;
            this.gridColumnEachday.VisibleIndex = 5;
            this.gridColumnEachday.Width = 86;
            // 
            // gridColumnGiveCount
            // 
            this.gridColumnGiveCount.AppearanceCell.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.gridColumnGiveCount.AppearanceCell.Options.UseBackColor = true;
            this.gridColumnGiveCount.Caption = "Өгсөн тоо";
            this.gridColumnGiveCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnGiveCount.CustomizationCaption = "Өгсөн тоо";
            this.gridColumnGiveCount.FieldName = "giveCount";
            this.gridColumnGiveCount.MinWidth = 75;
            this.gridColumnGiveCount.Name = "gridColumnGiveCount";
            this.gridColumnGiveCount.Visible = true;
            this.gridColumnGiveCount.VisibleIndex = 6;
            this.gridColumnGiveCount.Width = 84;
            // 
            // gridColumnSumPrice
            // 
            this.gridColumnSumPrice.Caption = "Дүн";
            this.gridColumnSumPrice.ColumnEdit = this.repositoryItemSpinEditPrice;
            this.gridColumnSumPrice.CustomizationCaption = "Нийт дүн";
            this.gridColumnSumPrice.FieldName = "sumPrice";
            this.gridColumnSumPrice.MinWidth = 100;
            this.gridColumnSumPrice.Name = "gridColumnSumPrice";
            this.gridColumnSumPrice.OptionsColumn.ReadOnly = true;
            this.gridColumnSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumPrice", "{0:c2}")});
            this.gridColumnSumPrice.Visible = true;
            this.gridColumnSumPrice.VisibleIndex = 7;
            this.gridColumnSumPrice.Width = 100;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "ОУ нэр";
            this.gridColumn36.CustomizationCaption = "ОУ нэр";
            this.gridColumn36.FieldName = "latinName";
            this.gridColumn36.MinWidth = 75;
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 112;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Тун хэмжээ";
            this.gridColumn37.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn37.FieldName = "unitType";
            this.gridColumn37.MinWidth = 75;
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 1;
            this.gridColumn37.Width = 85;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Хэлбэр";
            this.gridColumn38.CustomizationCaption = "Хэлбэр";
            this.gridColumn38.FieldName = "shape";
            this.gridColumn38.MinWidth = 75;
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Хэмжих нэгж";
            this.gridColumn39.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn39.FieldName = "quantity";
            this.gridColumn39.MinWidth = 75;
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            // 
            // gridColumnDrugDetailID
            // 
            this.gridColumnDrugDetailID.Caption = "drugDetailID";
            this.gridColumnDrugDetailID.FieldName = "drugDetailID";
            this.gridColumnDrugDetailID.Name = "gridColumnDrugDetailID";
            this.gridColumnDrugDetailID.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.False;
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.DisplayMember = "name";
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.ValueMember = "medicineID";
            this.repositoryItemGridLookUpEdit2.View = this.gridView1;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn29,
            this.gridColumn26,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn30});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Нэр";
            this.gridColumn21.CustomizationCaption = "Нэр";
            this.gridColumn21.FieldName = "name";
            this.gridColumn21.MinWidth = 75;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 0;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "ОУ нэр";
            this.gridColumn22.CustomizationCaption = "ОУ нэр";
            this.gridColumn22.FieldName = "latinName";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 1;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Орлогын төрөл";
            this.gridColumn29.CustomizationCaption = "Орлогын төрөл";
            this.gridColumn29.FieldName = "incTypeName";
            this.gridColumn29.MinWidth = 75;
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 2;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Ангилал";
            this.gridColumn26.CustomizationCaption = "Ангилал";
            this.gridColumn26.FieldName = "medicineTypeName";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 3;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Тун хэмжээ";
            this.gridColumn23.CustomizationCaption = "Тун хэмжээ";
            this.gridColumn23.FieldName = "unitType";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 4;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Хэлбэр";
            this.gridColumn24.CustomizationCaption = "Хэлбэр";
            this.gridColumn24.FieldName = "shape";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 5;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Хэмжих нэгж";
            this.gridColumn25.CustomizationCaption = "Хэмжих нэгж";
            this.gridColumn25.FieldName = "quantity";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 6;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Хүчинтэй хугацаа";
            this.gridColumn27.CustomizationCaption = "Хүчинтэй хугацаа";
            this.gridColumn27.FieldName = "validDate";
            this.gridColumn27.MinWidth = 75;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 8;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Үнэ";
            this.gridColumn28.CustomizationCaption = "Үнэ";
            this.gridColumn28.FieldName = "price";
            this.gridColumn28.MinWidth = 75;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 7;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Баркод";
            this.gridColumn30.CustomizationCaption = "Баркод";
            this.gridColumn30.FieldName = "barcode";
            this.gridColumn30.MinWidth = 75;
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 9;
            // 
            // repositoryItemCheckEditConfirm
            // 
            this.repositoryItemCheckEditConfirm.AutoHeight = false;
            this.repositoryItemCheckEditConfirm.Name = "repositoryItemCheckEditConfirm";
            // 
            // repositoryItemSpinEditGiveCount
            // 
            this.repositoryItemSpinEditGiveCount.AutoHeight = false;
            this.repositoryItemSpinEditGiveCount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditGiveCount.DisplayFormat.FormatString = "n2";
            this.repositoryItemSpinEditGiveCount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditGiveCount.EditFormat.FormatString = "n2";
            this.repositoryItemSpinEditGiveCount.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEditGiveCount.Mask.EditMask = "n2";
            this.repositoryItemSpinEditGiveCount.Name = "repositoryItemSpinEditGiveCount";
            // 
            // NurseUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(684, 411);
            this.Controls.Add(this.gridControlNurse);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NurseUpForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Эм олголт";
            this.Shown += new System.EventHandler(this.NurseUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDrug.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChooseAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlNurse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNurse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditGiveCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDownload;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditDate;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit checkEditChooseAll;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditUser;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDrug;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.GridControl gridControlNurse;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewNurse;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGiveCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConfirmMedicine;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConfirmCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnConfirmPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIncType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSumPrice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditConfirm;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditGiveCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDrugDetailID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEachday;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnBal;
    }
}