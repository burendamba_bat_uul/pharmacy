﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;

namespace Pharmacy2016.gui.core.store.drug
{
    /*
     * Эмийн түүвэр баталгаажуулах, эм олгох цонх.
     * 
     */
    public partial class ConfirmForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

        private static ConfirmForm INSTANCE = new ConfirmForm();

        public static ConfirmForm Instance
        {
            get
            {
                return INSTANCE;
            }
        }

        private bool isInit = false;

        private DataRowView DrugView;

        private DataRowView noExpView;
        private DataRowView noExpJournalView;

        private int actionType;

        private ConfirmForm()
        {

        }

        private void initForm()
        {
            InitializeComponent();
            this.isInit = true;
            Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

            checkEditConfirm.CheckState = CheckState.Unchecked;
            checkEditChooseAll.CheckState = CheckState.Unchecked;

            initError();
            initDataSource();
            reload(false);
        }

        private void initDataSource()
        {
            gridControlDrug.DataSource = PlanDataSet.DrugDetailOtherBindingSource;
            repositoryItemGridLookUpEdit2.DataSource = JournalDataSet.MedicineBalBindingSource;
            JournalDataSet.MedicineBalBindingSource.Filter = "count>0";
            repositoryItemGridLookUpEdit2.ValueMember = "medicinePrice";
            treeListLookUpEditWarehouse.Properties.DataSource = JournalDataSet.WarehouseOtherBindingSource;
            treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
        }

        private void initError()
        {
            treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            treeListLookUpEditWarehouse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            //spinEditSumCount.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            //dateEditDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
        }

        #endregion

        #region Форм нээх, хаах функц

        public void showForm(int type, XtraForm form)
        {
            try
            {
                if (!this.isInit)
                {
                    initForm();
                }


                actionType = type;
                if (actionType == 1)
                {
                    insert();
                }
                ShowDialog(form);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Алдаа гарлаа " + ex.Message);
            }
            finally
            {
                clearData();
                ProgramUtil.closeAlertDialog();
                ProgramUtil.closeWaitDialog();
                if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                {
                    PlanDataSet.DrugDetailTableAdapter.Fill(PlanDataSet.Instance.DrugDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), DrugForm.Instance.getStartDate().ToString(), DrugForm.Instance.getEndDate().ToString());
                }
                else
                {
                    PlanDataSet.DrugDetailTableAdapter.Fill(PlanDataSet.Instance.DrugDetail, HospitalUtil.getHospitalId(), "", DrugForm.Instance.getStartDate().ToString(), DrugForm.Instance.getEndDate().ToString());
                }
            }
        }

        private void insert()
        {
            DrugView = (DataRowView)PlanDataSet.DrugJournalBindingSource.Current;
            checkEditConfirm.EditValue = DrugView["isConfirm"];
            dateEditDate.EditValue = DrugView["date"];
            textEditNurse.Text = DrugView["nurseName"].ToString();
            textEditWard.Text = DrugView["nurseWardName"].ToString();
            textEditPatient.Text = DrugView["patientName"].ToString();
            textEditUser.Text = DrugView["pharmaName"].ToString();
            radioGroupType.EditValue = DrugView["type"];

        }

        private void clearData()
        {
            gridViewDrugDetail.ClearColumnErrors();
            JournalDataSet.WarehouseOtherBindingSource.Filter = "";
            JournalDataSet.WardOtherBindingSource.Filter = "";

            checkEditChooseAll.CheckState = CheckState.Unchecked;
            PlanDataSet.DrugDetailOtherBindingSource.CancelEdit();
            PlanDataSet.Instance.PatientDrug.RejectChanges();

            PlanDataSet.DrugJournalBindingSource.CancelEdit();
            PlanDataSet.Instance.DrugJournal.RejectChanges();
        }

        private void ConfirmForm_Shown(object sender, EventArgs e)
        {
            textEditUser.Focus();
            changeType();

            if (Visible)
            {
                reloadMedicine();
            }

            if (Visible && DrugView["userID"] != DBNull.Value && DrugView["userID"] != null)
            {
                JournalDataSet.WarehouseOtherBindingSource.Filter = UserUtil.getUserWarehouse(DrugView["userID"].ToString());
                JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(DrugView["userID"].ToString());
            }


            for (var j = 0; j < JournalDataSet.WardOtherBindingSource.Count; j++)
            {
                DataRowView row = (DataRowView)JournalDataSet.WardOtherBindingSource[j];
                if (row["id"].ToString().Equals(DrugView["nurseWardID"].ToString()))
                {
                    treeListLookUpEditWard.EditValue = DrugView["nurseWardID"];
                }
            }

            //MessageBox.Show(PlanDataSet.DrugDetailCountBindingSource.Count.ToString());

            if (radioGroupType.SelectedIndex == 0)
                treeListLookUpEditWarehouse.EditValue = DrugView["wareHouseID"];
            else if (radioGroupType.SelectedIndex == 1 && DrugView["wardID"] != null && DrugView["wardID"] != DBNull.Value)
                treeListLookUpEditWard.EditValue = DrugView["wardID"];

            if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
            {
                checkEditConfirm.ReadOnly = false;
                radioGroupType.ReadOnly = false;
                treeListLookUpEditWard.ReadOnly = false;
                treeListLookUpEditWarehouse.ReadOnly = false;

                checkEditChooseAll.Properties.ReadOnly = false;
                gridColumnConfirmCount.ColumnEdit.ReadOnly = false;
                gridColumnConfirmMedicine.ColumnEdit.ReadOnly = false;
                //gridColumnGiveCount.ColumnEdit.ReadOnly = true;
                //gridColumnGiveCount.AppearanceCell.BackColor = Color.Transparent;
                // gridColumnConfirmCount.AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                //gridColumnConfirmMedicine.AppearanceCell.BackColor = Color.LightGoldenrodYellow;
            }
            else if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
            {
                checkEditConfirm.ReadOnly = true;
                radioGroupType.ReadOnly = true;
                treeListLookUpEditWard.ReadOnly = true;
                treeListLookUpEditWarehouse.ReadOnly = true;

                checkEditChooseAll.Properties.ReadOnly = false;
                gridColumnConfirmCount.ColumnEdit.ReadOnly = true;
                gridColumnConfirmMedicine.ColumnEdit.ReadOnly = true;
                //gridColumnGiveCount.ColumnEdit.ReadOnly = false;
                //gridColumnGiveCount.AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                //gridColumnConfirmCount.AppearanceCell.BackColor = Color.Transparent;
                //gridColumnConfirmMedicine.AppearanceCell.BackColor = Color.Transparent;
            }
            else if (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
            {
                checkEditConfirm.ReadOnly = false;
                radioGroupType.ReadOnly = false;
                treeListLookUpEditWard.ReadOnly = false;
                treeListLookUpEditWarehouse.ReadOnly = false;

                checkEditChooseAll.Properties.ReadOnly = false;
                gridColumnConfirmCount.ColumnEdit.ReadOnly = false;
                gridColumnConfirmMedicine.ColumnEdit.ReadOnly = false;
                //gridColumnGiveCount.ColumnEdit.ReadOnly = false;
                //gridColumnGiveCount.AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                //gridColumnConfirmCount.AppearanceCell.BackColor = Color.LightGoldenrodYellow;
                //gridColumnConfirmMedicine.AppearanceCell.BackColor = Color.LightGoldenrodYellow;
            }
            MatchingGrid();

            ProgramUtil.IsShowPopup = true;
            ProgramUtil.IsSaveEvent = false;
        }

        #endregion

        #region Формын event

        private void treeListLookUpEditWarehouse_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditWarehouse.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    checkEditConfirm.Focus();
                }
            }
        }

        private void treeListLookUpEditWard_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (ProgramUtil.IsShowPopup)
                {
                    ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditWard.ShowPopup();
                }
                else
                {
                    ProgramUtil.IsShowPopup = true;
                    checkEditConfirm.Focus();
                }
            }
        }

        private void radioGroupType_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
            {
                if (radioGroupType.SelectedIndex == 0)
                {
                    //ProgramUtil.IsShowPopup = false;
                    treeListLookUpEditWarehouse.Focus();
                }
                else
                {
                    //ProgramUtil.IsShowPopup = true;
                    treeListLookUpEditWard.Focus();
                }
            }
        }

        private void gridViewDrugDetail_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void gridViewDrugDetail_InvalidValueException(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
        {
            //e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void simpleButtonReload_Click(object sender, EventArgs e)
        {
            reload(true);
        }

        private void simpleButtonSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void checkEditChooseAll_CheckedChanged(object sender, EventArgs e)
        {
            giveAll();
        }

        private void radioGroupType_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeType();
        }

        private void gridViewDrugDetail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == gridColumnConfirmMedicine)
            {
                DataRow medicine = getGiveMedicine(gridViewDrugDetail.GetRowCellValue(e.RowHandle, gridColumnConfirmMedicine).ToString());
                if (medicine != null)
                {
                    DataRow[] row = JournalDataSet.Instance.MedicineBal.Select("medicinePrice = '" + e.Value + "'");
                    // MessageBox.Show(Convert.ToString(row[0]["packageName"]));
                    // MessageBox.Show(gridViewDrugDetail.GetRowCellValue(e.RowHandle, gridColumnMedic).ToString());
                    if (Convert.ToString(row[0]["packageName"]) != gridViewDrugDetail.GetRowCellValue(e.RowHandle, gridColumnMedic).ToString())
                    {
                        gridViewDrugDetail.GetDataRow(e.RowHandle)["medicinePrice"] = null;
                        gridViewDrugDetail.GetDataRow(e.RowHandle)["confirmMedicineID"] = null;
                        gridViewDrugDetail.GetDataRow(e.RowHandle)["incTypeID"] = 0;
                        gridViewDrugDetail.SetRowCellValue(e.RowHandle, gridColumnIncType, "Байхгүй байна");
                        gridViewDrugDetail.GetDataRow(e.RowHandle)["confirmprice"] = 0;
                        gridViewDrugDetail.GetDataRow(e.RowHandle)["orderCount"] = 0;
                        XtraMessageBox.Show("Тухайн багцын эмнүүдээс сонгоно уу.");
                    }
                    else
                    {
                        gridViewDrugDetail.GetDataRow(e.RowHandle)["confirmMedicineID"] = row[0]["medicineID"];
                        gridViewDrugDetail.GetDataRow(e.RowHandle)["incTypeID"] = row[0]["incTypeID"];
                        gridViewDrugDetail.GetDataRow(e.RowHandle)["confirmprice"] = row[0]["price"];
                        gridViewDrugDetail.GetDataRow(e.RowHandle)["orderCount"] = row[0]["count"];
                    }
                }
            }
            //if (e.Column == gridColumnIsGive)
            //{
            //    gridViewDrugDetail.SetRowCellValue(e.RowHandle, gridColumnGiveCount, gridViewDrugDetail.GetRowCellValue(e.RowHandle, gridColumnConfirmCount));
            //}

            //if (e.Column == gridColumnConfirmMedicine)
            //{
            //    gridViewDrugDetail.SetRowCellValue(e.RowHandle, gridColumnSumPrice, Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(e.RowHandle, gridColumnConfirmCount)) * Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(e.RowHandle, gridColumnConfirmPrice)));
            //}

            //if(e.Column == gridColumnGiveCount)
            //{
            //    gridViewDrugDetail.SetRowCellValue(e.RowHandle, gridColumnSumPrice, Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(e.RowHandle, gridColumnGiveCount)) * Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(e.RowHandle, gridColumnConfirmPrice)));
            //}
        }

        private void repositoryItemGridLookUpEdit2_EditValueChanged(object sender, EventArgs e)
        {
            GridLookUpEdit gridLookUpEdit = sender as GridLookUpEdit;
            DataRowView view = (DataRowView)gridLookUpEdit.GetSelectedDataRow();

            gridViewDrugDetail.SetRowCellValue(gridViewDrugDetail.FocusedRowHandle, gridColumnConfirmPrice, view["price"]);
            gridViewDrugDetail.SetRowCellValue(gridViewDrugDetail.FocusedRowHandle, gridColumnIncTypeID, view["incTypeID"]);
            gridViewDrugDetail.SetRowCellValue(gridViewDrugDetail.FocusedRowHandle, gridColumnIncType, view["incTypeName"]);
            gridViewDrugDetail.SetRowCellValue(gridViewDrugDetail.FocusedRowHandle, gridColumnConfirmCount, 0);
            //gridViewDrugDetail.SetRowCellValue(gridViewDrugDetail.FocusedRowHandle, gridColumnSumPrice, Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(gridViewDrugDetail.FocusedRowHandle, gridColumnConfirmCount)) * Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(gridViewDrugDetail.FocusedRowHandle, gridColumnConfirmPrice)));
        }

        private void treeListLookUpEditWarehouse_EditValueChanged(object sender, EventArgs e)
        {
            reloadMedicine();
        }

        private void treeListLookUpEditWard_EditValueChanged(object sender, EventArgs e)
        {
            reloadMedicine();
        }

        private void gridViewDrugDetail_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if (e.Column == gridColumnIncType && e.DisplayText == "Байхгүй байна")
            {
                e.Appearance.BackColor = Color.Red;
            }
        }

        #endregion

        #region Формын функц

        private void reload(bool isReload)
        {
            ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
            if (PlanDataSet.Instance.DrugDetail.Rows.Count == 0)
                PlanDataSet.DrugDetailTableAdapter.Fill(PlanDataSet.Instance.DrugDetail, UserUtil.getUserId(), HospitalUtil.getHospitalId(), DrugForm.Instance.getStartDate().ToString(), DrugForm.Instance.getEndDate().ToString());
            // PlanDataSet.DrugDetailCountTableAdapter.Fill(PlanDataSet.Instance.DrugDetailCount, HospitalUtil.getHospitalId(), System.DateTime.Now.Date.ToString());
            if (UserDataSet.Instance.MedicinePackage.Rows.Count == 0)
                UserDataSet.MedicinePackageTableAdapter.Fill(UserDataSet.Instance.MedicinePackage, HospitalUtil.getHospitalId());
            if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
            if (isReload || JournalDataSet.Instance.WarehouseOther.Rows.Count == 0)
                JournalDataSet.WarehouseOtherTableAdapter.Fill(JournalDataSet.Instance.WarehouseOther, HospitalUtil.getHospitalId());
            if (isReload)
                reloadMedicine();
            ProgramUtil.closeWaitDialog();
        }

        //private void confirm()
        //{
        //    if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID)
        //    {
        //        if (checkEditChooseAll.Checked)
        //        {
        //            for (int i = 0; i < gridViewDrugDetail.RowCount; i++)
        //            {
        //                for (var j = 0; j < JournalDataSet.Instance.MedicineBal.Rows.Count; j++)
        //                {
        //                    DataRow rows = JournalDataSet.Instance.MedicineBal.Rows[j];
        //                    if (rows["medicineID"].ToString().Equals(gridViewDrugDetail.GetRowCellValue(i, gridColumnMedID).ToString()))
        //                    {
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDrugDetail.GetRowCellValue(i, gridColumnCount));
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmMedicine, gridViewDrugDetail.GetRowCellValue(i, gridColumnMedID));
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmPrice, gridViewDrugDetail.GetRowCellValue(i, gridColumnPrice));
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnIncTypeID, rows["incTypeID"]);
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnIncType, rows["incTypeName"]);
        //                        //gridViewDrugDetail.SetRowCellValue(i, gridColumnSumPrice, Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmCount)) * Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmPrice)));
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            for (int i = 0; i < gridViewDrugDetail.RowCount; i++)
        //            {
        //                for (var j = 0; j < JournalDataSet.Instance.MedicineBal.Rows.Count; j++)
        //                {
        //                    DataRow rows = JournalDataSet.Instance.MedicineBal.Rows[j];
        //                    if (rows["medicineID"].ToString().Equals(gridViewDrugDetail.GetRowCellValue(i, gridColumnMedID).ToString()))
        //                    {
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmCount, 0);
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmMedicine, null);
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmPrice, 0);
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnIncTypeID, null);
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnIncType, null);
        //                        //gridViewDrugDetail.SetRowCellValue(i, gridColumnSumPrice, 0);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    //else if(UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
        //    //{
        //    //    if (checkEditChooseAll.Checked)
        //    //    {
        //    //        for (int i = 0; i < gridViewDrugDetail.RowCount; i++)
        //    //        {
        //    //            if (gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmCount) != DBNull.Value && gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmCount) != null)
        //    //            {
        //    //                gridViewDrugDetail.SetRowCellValue(i, gridColumnGiveCount, gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmCount));
        //    //                gridViewDrugDetail.SetRowCellValue(i, gridColumnSumPrice, Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(i, gridColumnGiveCount)) * Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmPrice)));
        //    //            }                 
        //    //        }
        //    //    }
        //    //    else
        //    //    {
        //    //        for (int i = 0; i < gridViewDrugDetail.RowCount; i++)
        //    //        {
        //    //            gridViewDrugDetail.SetRowCellValue(i, gridColumnGiveCount, 0);
        //    //            gridViewDrugDetail.SetRowCellValue(i, gridColumnSumPrice, 0);
        //    //        }
        //    //    }
        //    //}
        //    else if (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
        //    {
        //        if (checkEditChooseAll.Checked)
        //        {
        //            for (int i = 0; i < gridViewDrugDetail.RowCount; i++)
        //            {
        //                for (var j = 0; j < JournalDataSet.Instance.MedicineBal.Rows.Count; j++)
        //                {
        //                    DataRow rows = JournalDataSet.Instance.MedicineBal.Rows[j];
        //                    if (rows["medicineID"].ToString().Equals(gridViewDrugDetail.GetRowCellValue(i, gridColumnMedID).ToString()))
        //                    {
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDrugDetail.GetRowCellValue(i, gridColumnCount));
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmMedicine, gridViewDrugDetail.GetRowCellValue(i, gridColumnMedID));
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmPrice, gridViewDrugDetail.GetRowCellValue(i, gridColumnPrice));
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnIncTypeID, rows["incTypeID"]);
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnIncType, rows["incTypeName"]);
        //                        //gridViewDrugDetail.SetRowCellValue(i, gridColumnGiveCount, gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmCount));
        //                        //gridViewDrugDetail.SetRowCellValue(i, gridColumnSumPrice, Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(i, gridColumnGiveCount)) * Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmPrice)));
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            for (int i = 0; i < gridViewDrugDetail.RowCount; i++)
        //            {
        //                for (var j = 0; j < JournalDataSet.Instance.MedicineBal.Rows.Count; j++)
        //                {
        //                    DataRow rows = JournalDataSet.Instance.MedicineBal.Rows[j];
        //                    if (rows["medicineID"].ToString().Equals(gridViewDrugDetail.GetRowCellValue(i, gridColumnMedID).ToString()))
        //                    {
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmCount, 0);
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmMedicine, null);
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmPrice, 0);
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnIncTypeID, null);
        //                        gridViewDrugDetail.SetRowCellValue(i, gridColumnIncType, null);
        //                        //gridViewDrugDetail.SetRowCellValue(i, gridColumnGiveCount, 0);
        //                        //gridViewDrugDetail.SetRowCellValue(i, gridColumnSumPrice, 0);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        private void giveAll()
        {
            if (checkEditChooseAll.Checked)
            {
                for (int i = 0; i < gridViewDrugDetail.RowCount; i++)
                {
                    DataRow match = getGiveMedicine(gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmMedicine).ToString());
                    if (match != null)
                    {
                        if (Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(i, gridColumnOrderCount)) <= Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(i, gridColumnCount)))
                        {
                            gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDrugDetail.GetRowCellValue(i, gridColumnOrderCount));
                        }
                        else
                        {
                            gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmCount, gridViewDrugDetail.GetRowCellValue(i, gridColumnCount));
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < gridViewDrugDetail.RowCount; i++)
                {
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmCount, 0);
                    //gridViewDrugDetail.SetRowCellValue(i, gridColumnIsGive, false);     
                }
            }
        }

        private void changeType()
        {
            if (radioGroupType.SelectedIndex == 0)
            {
                labelControlWarehouse.Location = new Point(172, 85);
                treeListLookUpEditWard.Location = new Point(144, 12);
                treeListLookUpEditWarehouse.Location = new Point(239, 82);

                treeListLookUpEditWard.Visible = false;
                treeListLookUpEditWarehouse.Visible = true;
                labelControlWarehouse.Text = "Эмийн сан*:";
            }
            else if (radioGroupType.SelectedIndex == 1)
            {
                labelControlWarehouse.Location = new Point(195, 85);
                treeListLookUpEditWard.Location = new Point(239, 82);
                treeListLookUpEditWarehouse.Location = new Point(144, 12);

                treeListLookUpEditWard.Visible = true;
                treeListLookUpEditWarehouse.Visible = false;
                labelControlWarehouse.Text = "Тасаг*:";
            }
            reloadMedicine();
        }

        private bool check()
        {
            bool isRight = true;
            string text = "", errorText;
            ProgramUtil.closeAlertDialog();
            if (radioGroupType.SelectedIndex == 0)
            {
                if (treeListLookUpEditWarehouse.EditValue == DBNull.Value || treeListLookUpEditWarehouse.EditValue == null || treeListLookUpEditWarehouse.Text.Equals(""))
                {
                    errorText = "Агуулах сонгоно уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWarehouse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWarehouse.Focus();
                    }
                }
            }
            else if (radioGroupType.SelectedIndex == 1)
            {
                if (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals(""))
                {
                    errorText = "Тасаг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }
            }

            if (isRight && checkEditConfirm.Checked)
            {
                for (int j = 0; j < gridViewDrugDetail.RowCount; j++)
                {
                    if (gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmMedicine) == null ||
                        gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmMedicine) == DBNull.Value)
                    {
                        errorText = "Олгох эм байхгүй байна!";
                        gridViewDrugDetail.SetColumnError(gridColumnConfirmMedicine, errorText);
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        isRight = false;
                        break;
                    }
                    if (gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmCount) != null &&
                        gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmCount) != DBNull.Value &&
                        Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmCount)) < 0)
                    {
                        errorText = "Олгох тоо нь 0-ээс бага байна!";
                        gridViewDrugDetail.SetColumnError(gridColumnConfirmCount, errorText);
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        isRight = false;
                        break;
                    }
                    if (gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmCount) != null &&
                        gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmCount) != DBNull.Value &&
                        Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmCount)) > 0 &&
                            Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmCount)) > Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(j, gridColumnOrderCount)))
                    {
                        errorText = "Олгох тоо нь үлдэгдлээс их байна!";
                        gridViewDrugDetail.SetColumnError(gridColumnConfirmCount, errorText);
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        isRight = false;
                        break;
                    }
                    if (gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmCount) != null &&
                        gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmCount) != DBNull.Value &&
                        Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmCount)) > 0 &&
                            Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(j, gridColumnConfirmCount)) > Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(j, gridColumnCount)))
                    {
                        errorText = "Олгох тоо нь захиалсан тооноос их байна!";
                        gridViewDrugDetail.SetColumnError(gridColumnConfirmCount, errorText);
                        text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                        isRight = false;
                        break;
                    }
                }
            }


            if (!isRight && !text.Equals(""))
                ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

            return isRight;
        }

        private void MatchingGrid()
        {
            //DataRow[] row = JournalDataSet.Instance.MedicineBal.Select("");
            for (int k = 0; k < gridViewDrugDetail.RowCount; k++)
            {
                DataRow row = gridViewDrugDetail.GetDataRow(k);
                DataRow[] medicine = JournalDataSet.Instance.MedicineBal.Select("medicineID = '" + row["confirmMedicineID"] + "'");
                if (medicine.Length > 0)
                {
                    row["medicinePrice"] = medicine[0]["medicinePrice"];
                    row["price"] = medicine[0]["price"];
                }

            }

            for (int i = 0; i < gridViewDrugDetail.RowCount; i++)
            {
                DataRow medicine = getGiveMedicine(gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmMedicine).ToString());
                if (medicine != null)
                {
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmMedicine, medicine["medicinePrice"]);
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnIncTypeID, medicine["incTypeID"]);
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnIncType, medicine["incTypeName"]);
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmPrice, medicine["price"]);
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnOrderCount, medicine["count"]);
                }
                else
                {
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmMedicine, null);
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnIncTypeID, null);
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnIncType, "Байхгүй байна");
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnConfirmPrice, 0);
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnOrderCount, 0);
                }

                DataRowView row = (DataRowView)PlanDataSet.DrugDetailOtherBindingSource[i];
                DataRow[] row_drugjournal = PlanDataSet.Instance.DrugJournal.Select("id = '" + row["drugJournalID"] + "'");
                if (row_drugjournal.Length > 0)
                {
                    DataRow[] row_orderdetail = JournalDataSet.Instance.OrderGoodsDetail.Select("orderGoodsID = '" + row_drugjournal[0]["orderJournalID"] +
                        "' AND medicineID= '" + row["medicineID"] + "'");
                    if (row_orderdetail.Length > 0)
                    {
                        gridViewDrugDetail.SetRowCellValue(i, gridColumnValidCount, row_orderdetail[0]["confirmCount"]);
                    }
                    else gridViewDrugDetail.SetRowCellValue(i, gridColumnValidCount, 0);
                }
                else gridViewDrugDetail.SetRowCellValue(i, gridColumnValidCount, 0);
            }
        }

        private DataRow getGiveMedicine(string medicinePrice)
        {
            for (int i = 0; i < JournalDataSet.Instance.MedicineBal.Rows.Count; i++)
            {
                DataRow row = JournalDataSet.Instance.MedicineBal.Rows[i];
                if (row["medicinePrice"].ToString().Equals(medicinePrice))
                    return row;
            }
            return null;
        }

        private void addNoExp()
        {
            //MessageBox.Show(DrugView["wardID"].ToString());
            // MessageBox.Show(DrugView["nurseWardID"].ToString());
            // if (DrugView["wardID"] != DrugView["nurseWardID"])
            //  {
            //DataRowView view = (DataRowView)gridLookUpEditMedicine.GetSelectedDataRow();
            noExpJournalView = (DataRowView)JournalDataSet.ExpJournalBindingSource.AddNew();
            string noExpJournalID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
            noExpJournalView["id"] = noExpJournalID;
            noExpJournalView["hospitalID"] = DrugView["hospitalID"];
            noExpJournalView["date"] = DrugView["date"];
            noExpJournalView["userID"] = DrugView["userID"];
            //noExpJournalView["type"] = DrugView["type"];
            noExpJournalView["type"] = 0;
            noExpJournalView["warehouseID"] = DrugView["warehouseID"];
            noExpJournalView["wardID"] = DrugView["wardID"];
            //noExpJournalView["expTypeID"] = null;
            if (radioGroupType.SelectedIndex == 0) noExpJournalView["expType"] = 1;
            else if (radioGroupType.SelectedIndex == 1) noExpJournalView["expType"] = 2;
            noExpJournalView["expUserID"] = DrugView["userID"];
            noExpJournalView["wtype"] = 1;
            noExpJournalView["expWareHouseID"] = DrugView["warehouseID"];
            noExpJournalView["expWardID"] = DrugView["nurseWardID"];
            //noExpJournalView["docNumber"] = null;
            noExpJournalView["description"] = @"Эмийн түүвэрээс дотоод шилжүүлэгээр дамжих";
            noExpJournalView["doctorID"] = DrugView["doctorID"];
            noExpJournalView["actionUserID"] = UserUtil.getUserId();
            noExpJournalView.EndEdit();
            JournalDataSet.ExpJournalTableAdapter.Update(JournalDataSet.Instance.ExpJournal);
            for (int i = 0; i < gridViewDrugDetail.RowCount; i++)
            {
                if (Convert.ToInt32(gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmCount)) != 0)
                {
                    noExpView = (DataRowView)JournalDataSet.ExpDetailBindingSource.AddNew();
                    string noExpDetailID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    noExpView["id"] = (Convert.ToInt64(noExpDetailID) + i).ToString();
                    noExpView["hospitalID"] = DrugView["hospitalID"];
                    noExpView["expJournalID"] = noExpJournalID;
                    noExpView["incTypeID"] = gridViewDrugDetail.GetRowCellValue(i, gridColumnIncTypeID);
                    noExpView["medicineID"] = gridViewDrugDetail.GetRowCellValue(i, gridColumn2);
                    noExpView["price"] = gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmPrice);
                    noExpView["count"] = gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmCount);
                    noExpView["description"] = @"Эмийн түүвэрээс дотоод шилжүүлэгээр дамжих";
                    noExpView["actionUserID"] = UserUtil.getUserId();
                    noExpView.EndEdit();

                    JournalDataSet.ExpDetailTableAdapter.Update(JournalDataSet.Instance.ExpDetail);
                }
            }
            //}
        }
        private void save()
        {
            if (check())
            {
                //     MessageBox.Show("Hi");
                //---------------------------------------------------------------------------------
                //hospitalID	id	            date	    userID	 type	warehouseID	  wardID	        expTypeID	expType	   expUserID	wtype	expWarehouseID	expWardID	      docNumber  	description	  doctorID
                //0001	        100000118445376	2017-05-22	1000030	 1	    NULL	      100000110370988	NULL	    4	       1000030	    1	    NULL	        100000110381204	  NULL	        NULL	      NULL
                //---------------------------------------------------------------------------------
                //hospitalID	id	            expJournalID	incTypeID	medicineID	    price	count	description
                //0001	        100000118461063	100000118445376	1	        100000110312793	150	    20	     NULL
                //0001	        100000118461064	100000118445376	1	        100000111100624	1900	10	     NULL
                //0001	        100000118461065	100000118445376	2	        100000109584625	880	    5	     NULL
                //0001	        100000118461066	100000118445376	2	        100000110005757	660	    1	     NULL
                //--------------------------------------------------------------------------------


                DataRowView warehouse = (DataRowView)treeListLookUpEditWarehouse.GetSelectedDataRow();
                DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                if (radioGroupType.SelectedIndex == 0)
                {
                    DrugView["wardID"] = null;
                    DrugView["wareHouseID"] = treeListLookUpEditWarehouse.EditValue;
                    DrugView["wardName"] = warehouse["name"];
                }
                else if (radioGroupType.SelectedIndex == 1)
                {
                    DrugView["warehouseID"] = null;
                    DrugView["wardID"] = treeListLookUpEditWard.EditValue;
                    DrugView["wardName"] = ward["name"];
                }
                if (checkEditConfirm.Checked)
                {
                    addNoExp();
                    //if (DrugView["wardID"] != null) {
                    //    DrugView["wardID"] = DrugView["nurseWardID"];
                    //    DrugView["wardName"] = textEditWard;
                    //}
                }

                DrugView["type"] = radioGroupType.EditValue;
                DrugView["isConfirm"] = checkEditConfirm.EditValue;
                DrugView.EndEdit();
                bool check_uld = false;
                for (int i = 0; i < gridViewDrugDetail.RowCount; i++)
                {
                    if (Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmCount)) > Convert.ToDouble(gridViewDrugDetail.GetRowCellValue(i, gridColumnOrderCount)))
                    {
                        check_uld = true;
                    }
                    gridViewDrugDetail.SetRowCellValue(i, gridColumnDupCount, gridViewDrugDetail.GetRowCellValue(i, gridColumnConfirmCount));
                }
                if (check_uld)
                {
                    XtraMessageBox.Show("Үлдэгдэл хүрэхгүй байгаа тул хадгалах боломжгүй.");
                }
                else
                {
                    PlanDataSet.DrugDetailOtherBindingSource.EndEdit();
                    PlanDataSet.DrugDetailTableAdapter.Update(PlanDataSet.Instance.DrugDetail);

                    PlanDataSet.DrugJournalBindingSource.EndEdit();
                    PlanDataSet.DrugJournalTableAdapter.Update(PlanDataSet.Instance.DrugJournal);
                    Close();
                }
            }
        }

        private void reloadMedicine()
        {
            if (Visible && dateEditDate.EditValue != DBNull.Value && dateEditDate.EditValue != null && !dateEditDate.Text.Equals("") &&
            DrugView["userID"] != DBNull.Value && DrugView["userID"] != null && !DrugView["userID"].ToString().Equals("") &&
            ((radioGroupType.SelectedIndex == 0 && treeListLookUpEditWarehouse.EditValue != null && treeListLookUpEditWarehouse.EditValue != DBNull.Value && !treeListLookUpEditWarehouse.Text.Equals("")) ||
            (radioGroupType.SelectedIndex == 1 && treeListLookUpEditWard.EditValue != null && treeListLookUpEditWard.EditValue != DBNull.Value && !treeListLookUpEditWard.Text.Equals(""))))
            {
                JournalDataSet.Instance.MedicineBal.Clear();
                JournalDataSet.MedicineBalTableAdapter.Fill(JournalDataSet.Instance.MedicineBal, HospitalUtil.getHospitalId(), DrugView["userID"].ToString(), radioGroupType.SelectedIndex, (radioGroupType.SelectedIndex == 0 ? treeListLookUpEditWarehouse.EditValue.ToString() : ""), (radioGroupType.SelectedIndex == 1 ? treeListLookUpEditWard.EditValue.ToString() : ""), dateEditDate.Text);
            }

            MatchingGrid();
        }

        #endregion

        private void gridViewDrugDetail_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            /*  if (gridViewDrugDetail.RowCount > 0)
              {
                  DataRow row = gridViewDrugDetail.GetFocusedDataRow();
                  JournalDataSet.MedicineBalBindingSource.Filter = "packageID = '" + row["medicineID"] + "' AND count>0";
                  //MessageBox.Show(row["id"].ToString());
              }
            */
        }

        private void gridControlDrug_Click(object sender, EventArgs e)
        {

        }


    }
}