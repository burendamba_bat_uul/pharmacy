﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.journal.plan;
using DevExpress.XtraGrid.Views.Grid;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.report.store.drug;
using Pharmacy2016.gui.report.ds;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid;
using Pharmacy2016.gui.core.journal.ds;

namespace Pharmacy2016.gui.core.store.drug
{
    /*
     * Эмийн түүвэр жагсаалт, нэмэх, засах, устгах цонх.
     * 
     */
    public partial class DrugForm : DevExpress.XtraEditors.XtraForm
    {
        #region Форм анх ачааллах функц

           private static DrugForm INSTANCE = new DrugForm();

           public static DrugForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private readonly int FORM_ID = 1005;

            private bool isInit = false;

            public DataRowView currView;

            private BaseEdit inplaceEditor;

            private DrugForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                this.MdiParent = MainForm.Instance;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                RoleUtil.addForm(FORM_ID, this);

                
                //gridControlMonthly.DataSource = PlanDataSet.PlanMonthlyBindingSource;
                gridControlDrug.DataSource = PlanDataSet.DrugJournalBindingSource;
                //gridControlDoctor.DataSource = PlanDataSet.DrugDoctorBindingSource;
                dateEditStart.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditStart.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                dateEditEnd.Properties.MinValue = SystemUtil.BEG_BAL_DATE;
                dateEditEnd.Properties.MaxValue = SystemUtil.END_BAL_DATE;
                
                DateTime date = DateTime.Now;
                dateEditStart.DateTime = new DateTime(date.Year, date.Month, 1);
                dateEditEnd.DateTime = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            }

        #endregion
        
        #region Форм нээх, хаах функц

            public void showForm()
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    if (Visible)
                    {
                        Select();
                    }
                    else
                    {                    
                        checkRole();
                        PlanDataSet.Instance.DrugJournal.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        PlanDataSet.Instance.DrugJournal.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        PlanDataSet.Instance.DrugDetail.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        PlanDataSet.Instance.DrugDetail.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        //PlanDataSet.Instance.DrugDoctor.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        //PlanDataSet.Instance.DrugDoctor.actionUserIDColumn.DefaultValue = UserUtil.getUserId();
                        //PlanDataSet.Instance.DrugDoctorDetail.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        //PlanDataSet.Instance.DrugDoctorDetail.actionUserIDColumn.DefaultValue = UserUtil.getUserId();

                        reload();
                        gridColumn3.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
                        Show();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх нээгдэхэд алдаа гарлаа " + ex.Message);
                    clearData();
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void checkRole()
            {
                bool[] roles = UserUtil.getWindowRole(FORM_ID);

                gridControlDrug.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[1];
                gridControlDrug.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[3];
                gridControlDrug.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = !SystemUtil.SELECTED_DB_READONLY && roles[2];
                dropDownButtonPrint.Visible = roles[4];
                simpleButtonLayout.Visible = !SystemUtil.SELECTED_DB_READONLY;

                if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                {
                    simpleButtonConfirm.Visible = !SystemUtil.SELECTED_DB_READONLY && true;
                }                 
                else
                {
                    simpleButtonConfirm.Visible = false;
                }

                if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                {
                    //simpleButtonDownload.Visible = true;
                    //gridControlDoctor.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    //gridControlDoctor.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    //gridControlDoctor.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;
                    //gridControlDrug.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
                    //gridControlDrug.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = true;
                    //gridControlDrug.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = true;
                    
                }
            }

            private void DrugForm_FormClosing(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                clearData();
                Hide();
            }

            private void clearData()
            {
                PlanDataSet.Instance.DrugJournal.Clear();
                PlanDataSet.Instance.DrugDetail.Clear();
                JournalDataSet.Instance.OrderGoods.Clear();
                JournalDataSet.Instance.OrderGoodsDetail.Clear();
                //PlanDataSet.Instance.DrugDoctor.Clear();
                //PlanDataSet.Instance.DrugDoctorDetail.Clear();
            }

        #endregion

        #region Формын event

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                try
                {
                    reload();
                }
                catch (System.Exception ex)
                {
                    XtraMessageBox.Show("Ачааллахад алдаа гарлаа " + ex.ToString());
                }
                finally
                {
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void simpleButtonLayout_Click(object sender, EventArgs e)
            {
                //saveLayout();
            }

            private void gridControlInc_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
            {
                if (e.Button.ButtonType == NavigatorButtonType.Custom)
                {
                    if (String.Equals(e.Button.Tag, "add"))
                    {
                        add();
                    }
                    else if (String.Equals(e.Button.Tag, "delete") && gridViewDrug.GetFocusedRow() != null)
                    {
                        try
                        {
                            delete();
                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("Устгахад алдаа гарлаа " + ex.ToString());
                        }
                        finally
                        {
                            ProgramUtil.closeWaitDialog();
                        }
                    }
                    else if (String.Equals(e.Button.Tag, "update") && gridViewDrug.GetFocusedRow() != null)
                    {
                        update();
                    }
                    //else if (String.Equals(e.Button.Tag, "confirm") && gridViewDrug.GetFocusedRow() != null)
                    //{
                    //    confirm();
                    //}
                }
            }

            private void simpleButtonConfirm_Click(object sender, EventArgs e)
            {
                if (gridViewDrug.GetFocusedRow() != null)
                    confirm();
            }

            //private void simpleButtonDownload_Click(object sender, EventArgs e)
            //{
            //    DoctorDownForm.Instance.showForm(Instance);
            //}
            private void gridViewDrug_ShownEditor(object sender, EventArgs e)
            {
                inplaceEditor = ((GridView)sender).ActiveEditor;
                inplaceEditor.DoubleClick += new EventHandler(ActiveEditor_DoubleClick);
            }

            private void ActiveEditor_DoubleClick(object sender, System.EventArgs e)
            {
                BaseEdit editor = (BaseEdit)sender;
                GridControl grid = (GridControl)editor.Parent;
                Point pt = grid.PointToClient(Control.MousePosition);
                GridView view = (GridView)grid.FocusedView;
                DoRowDoubleClick(view, pt);
            }

        #endregion

        #region Формын функц

            private void checkedChanged()
            {
                //if (xtraTabControl.SelectedTabPage == xtraTabPageDrug)
                //{
                //    if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                //    {
                //        simpleButtonConfirm.Visible = true;

                //    }
                //    if (UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                //    {
                //        simpleButtonDownload.Visible = true;
                //    }
                //}
                //else if (xtraTabControl.SelectedTabPage == xtraTabPageNurse)
                //{
                //    simpleButtonConfirm.Visible = false;
                //    simpleButtonDownload.Visible = false;
                //}
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            public void reload()
            {
                if(check())
                {
                    if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.DOCTOR_ID)
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        PlanDataSet.DrugJournalTableAdapter.Fill(PlanDataSet.Instance.DrugJournal, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                        PlanDataSet.DrugDetailTableAdapter.Fill(PlanDataSet.Instance.DrugDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);                       
                        //PlanDataSet.DrugDoctorTableAdapter.Fill(PlanDataSet.Instance.DrugDoctor, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);
                        //PlanDataSet.DrugDoctorDetailTableAdapter.Fill(PlanDataSet.Instance.DrugDoctorDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), dateEditStart.Text, dateEditEnd.Text);

                        ProgramUtil.closeWaitDialog();
                    }
                    else
                    {
                        ProgramUtil.showWaitDialog(ProgramUtil.RELOAD_TITLE);
                        PlanDataSet.DrugJournalTableAdapter.Fill(PlanDataSet.Instance.DrugJournal, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                        PlanDataSet.DrugDetailTableAdapter.Fill(PlanDataSet.Instance.DrugDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);                      
                        //PlanDataSet.DrugDoctorTableAdapter.Fill(PlanDataSet.Instance.DrugDoctor, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                        //PlanDataSet.DrugDoctorDetailTableAdapter.Fill(PlanDataSet.Instance.DrugDoctorDetail, HospitalUtil.getHospitalId(), "", dateEditStart.Text, dateEditEnd.Text);
                        ProgramUtil.closeWaitDialog();
                    }
                    if (UserUtil.getUserRoleId() == RoleUtil.PHARMACIST_ID || UserUtil.getUserRoleId() == RoleUtil.NURSE_ID || UserUtil.getUserRoleId() == RoleUtil.HEAD_NURSE_ID)
                    {
                        JournalDataSet.OrderGoodsTableAdapter.Fill(JournalDataSet.Instance.OrderGoods, HospitalUtil.getHospitalId(), UserUtil.getUserId(), System.DateTime.Now.Date.Year.ToString() + "-01-01", System.DateTime.Now.Date.ToString());
                        JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(), UserUtil.getUserId(), System.DateTime.Now.Date.Year.ToString() + "-01-01", System.DateTime.Now.Date.ToString());
                    }
                    else
                    {
                        JournalDataSet.OrderGoodsTableAdapter.Fill(JournalDataSet.Instance.OrderGoods, HospitalUtil.getHospitalId(), "", System.DateTime.Now.Date.Year.ToString() + "-01-01", System.DateTime.Now.Date.ToString());
                        JournalDataSet.OrderGoodsDetailTableAdapter.Fill(JournalDataSet.Instance.OrderGoodsDetail, HospitalUtil.getHospitalId(), "", System.DateTime.Now.Date.Year.ToString() + "-01-01", System.DateTime.Now.Date.ToString());
                    }
                }
            }

            private void add()
            {     
               DoctorUpForm.Instance.showForm(1, Instance);               
            }

            private void delete()
            {
                if (Convert.ToBoolean(gridViewDrug.GetFocusedRowCellValue("isRequest")))
                {
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, "Эмийн түүвэр захиалгын хуудсанд татагдсан тул өөрчилж болохгүй");
                    return;
                }
                object ret = UserDataSet.QueriesTableAdapter.can_delete_drug(HospitalUtil.getHospitalId(), gridViewDrug.GetFocusedDataRow()["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    //XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, "Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул өөрчилж болохгүй");
                    return;
                }
                string date = gridViewDrug.GetRowCellValue(gridViewDrug.FocusedRowHandle, gridViewDrug.Columns["date"]).ToString();
                 if (ProgramUtil.checkLockMedicine(date)) {
                     bool isDeleteDoc = false;
                     GridView detail = null;

                     if (gridViewDrug.IsFocusedView)
                     {
                         isDeleteDoc = true;
                     }
                     else
                     {
                         if (!gridViewDrug.GetMasterRowExpanded(gridViewDrug.FocusedRowHandle))
                             gridViewDrug.SetMasterRowExpanded(gridViewDrug.FocusedRowHandle, true);
                         detail = gridViewDrug.GetDetailView(gridViewDrug.FocusedRowHandle, 0) as GridView;
                         if (detail.IsFocusedView)
                         {
                             isDeleteDoc = detail.RowCount == 1;
                         }
                     }
                     if (isDeleteDoc)
                     {
                         DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн түүвэр устгах", MessageBoxButtons.YesNo);
                         if (dialogResult == DialogResult.Yes)
                         {
                             ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                             gridViewDrug.DeleteSelectedRows();
                             PlanDataSet.DrugJournalBindingSource.EndEdit();
                             PlanDataSet.DrugJournalTableAdapter.Update(PlanDataSet.Instance.DrugJournal);
                             ProgramUtil.closeWaitDialog();
                         }
                     }
                     else
                     {
                         DialogResult dialogResult = XtraMessageBox.Show("Энэ мөрийг устгах уу", "Эмийн түүврийн эм устгах", MessageBoxButtons.YesNo);
                         if (dialogResult == DialogResult.Yes)
                         {
                             ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                             detail.DeleteSelectedRows();
                             PlanDataSet.DrugDetailBindingSource.EndEdit();
                             PlanDataSet.DrugDetailTableAdapter.Update(PlanDataSet.Instance.DrugDetail);
                             ProgramUtil.closeWaitDialog();
                         }
                     }
                 }
                 else
                 {
                     XtraMessageBox.Show("Гүйлгээ түгжигдсэн байна!");
                 }       
            }

            private void update()
            {
                if (Convert.ToBoolean(gridViewDrug.GetFocusedRowCellValue("isRequest")))
                {
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, "Эмийн түүвэр захиалгын хуудсанд татагдсан тул өөрчилж болохгүй");
                    return;
                }

                if (!gridViewDrug.GetMasterRowExpanded(gridViewDrug.FocusedRowHandle))
                    gridViewDrug.SetMasterRowExpanded(gridViewDrug.FocusedRowHandle, true);

                object ret = UserDataSet.QueriesTableAdapter.can_delete_drug(HospitalUtil.getHospitalId(), gridViewDrug.GetFocusedDataRow()["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    //string msg = ret.ToString();
                    ////XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    //ProgramUtil.showAlertDialog(Instance, Instance.Text, "Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул өөрчилж болохгүй");
                     //return;
                    DoctorUpForm.Instance.showForm(3, Instance);
                }
                else
                {
                    DoctorUpForm.Instance.showForm(2, Instance);
                }
            }

            private void confirm()
            {
                object ret = UserDataSet.QueriesTableAdapter.can_delete_drug(HospitalUtil.getHospitalId(), gridViewDrug.GetFocusedDataRow()["id"].ToString());
                if (ret != DBNull.Value && ret != null)
                {
                    string msg = ret.ToString();
                    //XtraMessageBox.Show("Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул устгаж болохгүй");
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, "Энэ мөрийг " + msg.Substring(0, msg.Length - 2) + " дээр ашиглаж байгаа тул өөрчилж болохгүй");
                    return;
                }
                DataRow row = gridViewDrug.GetFocusedDataRow();
                DataRow[] row_orderdetail = JournalDataSet.Instance.OrderGoods.Select("id = '" + row["orderJournalID"] + "' AND isConfirm > 0");
                if (row_orderdetail.Length > 0)
                {
                    if (Convert.ToInt16(row["isConfirm"]) != 1)
                    {
                        ConfirmForm.Instance.showForm(1, Instance);
                    }
                    else XtraMessageBox.Show("Энэ түүврийн олголт хийгдсэн байна.");
                }
                else XtraMessageBox.Show("Энэ түүврийг баталгаажуулаагүй байна.");
            }

            public DataRowView getSelectedRow()
            {
                gridViewDrug.SetMasterRowExpanded(gridViewDrug.FocusedRowHandle, true);
                GridView detail = gridViewDrug.GetDetailView(gridViewDrug.FocusedRowHandle, 0) as GridView;
                string id = detail.GetFocusedDataRow()["id"].ToString();
                for (int i = 0; i < PlanDataSet.DrugDetailBindingSource.Count; i++)
                {
                    DataRowView view = (DataRowView)PlanDataSet.DrugDetailBindingSource[i];
                    if (view["id"].ToString().Equals(id))
                        return view;
                }
                return null;
            }

            public DateTime getStartDate()
            {
                return dateEditStart.DateTime;
            }

            public DateTime getEndDate()
            {
                return dateEditEnd.DateTime;
            }

            private void DoRowDoubleClick(GridView view, Point pt)
            {
                GridHitInfo info = view.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    if (view.GetFocusedDataRow() != null)
                        update();
                }
            } 

        #endregion              
         
        #region Хэвлэх, хөрвүүлэх функц

            private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemPrint.Caption;
                dropDownButtonPrint.Image = barButtonItemPrint.Glyph;

                if (gridViewDrug.RowCount > 0)
                    DrugReport.Instance.initAndShow(dateEditStart.Text, dateEditEnd.Text, 2);
                //ExpReport.Instance.initAndShow(1, gridViewExp.ActiveFilterString);
            }

            private void barButtonItemDrugPatient_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemDrugPatient.Caption;
                dropDownButtonPrint.Image = barButtonItemDrugPatient.Glyph;

                if (gridViewDrug.RowCount > 0)
                    DrugReport.Instance.initAndShow(dateEditStart.Text, dateEditEnd.Text, 1);
            }

            private void barButtonItemDrugMedicine_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemDrugMedicine.Caption;
                dropDownButtonPrint.Image = barButtonItemDrugMedicine.Glyph;

                if (gridViewDrug.RowCount > 0)
                    DrugReport.Instance.initAndShow(dateEditStart.Text, dateEditEnd.Text, 3);
            }

            private void barButtonItemCustomize_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                ProgramUtil.closeAlertDialog();
                if (gridViewDrug.RowCount == 0 && gridViewDrug.GetFocusedDataRow() == null)
                {
                    string errorText = "Хэвлэх мөр алга байна";
                    string text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);
                    return;
                }
                dropDownButtonPrint.Text = barButtonItemCustomize.Caption;
                dropDownButtonPrint.Image = barButtonItemCustomize.Glyph;
                List<string[]> colList = new List<string[]>();
                DataRow row = gridViewDrug.GetFocusedDataRow();
                foreach (DevExpress.XtraGrid.Columns.GridColumn col in gridViewDrug.Columns.Where(x => (x.FieldName != "userID" && x.FieldName != "warehouseDesc" && x.FieldName != "isSent" && x.FieldName != "isRequest" && x.FieldName != "isConfirm" && x.FieldName != "isBreak" && x.FieldName != "orderStateText" && x.FieldName != "date" && x.FieldName != "warehouseName" && x.FieldName != "expUserID" && x.FieldName != "wardName" && x.FieldName != "pharmaName" && x.FieldName != "nurseWardName" && x.FieldName != "description")))
                {
                    colList.Add(new string[] { col.FieldName, col.CustomizationCaption });
                }

                foreach (DevExpress.XtraGrid.Columns.GridColumn cols in gridViewDrugDetial.Columns.Where(x => (x.FieldName != "startDate" && x.FieldName != "endDate" && x.FieldName != "duration" && x.FieldName != "eachDay" && x.FieldName != "medicName" && x.FieldName != "confirmPrice" && x.FieldName != "confirmCount")))
                {
                    colList.Add(new string[] { cols.FieldName, cols.Caption });
                }

                ReportUpForm.Instance.showForm(colList, DrugForm.Instance, row, dateEditStart.DateTime, dateEditEnd.DateTime, "");
            }

            private void barButtonItemConvert_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
            {
                dropDownButtonPrint.Text = barButtonItemConvert.Caption;
                dropDownButtonPrint.Image = barButtonItemConvert.Glyph;

                    ProgramUtil.convertGrid(gridViewDrug, "Эмийн түүвэр");
                //else if (radioGroupJournal.SelectedIndex == 1)
                //    ProgramUtil.convertGrid(gridViewDetail, "Зарлагын гүйлгээ");
                //else if (radioGroupJournal.SelectedIndex == 2)
                //    ProgramUtil.convertGrid(pivotGridControl, "Зарлагын дэлгэрэнгүй");
            }

        #endregion          
     
    }
}