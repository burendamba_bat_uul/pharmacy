﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.journal.plan;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.util;
using Pharmacy2016.gui.core.info.ds;

namespace Pharmacy2016.gui.core.store.drug
{
    public partial class DoctorDownForm : DevExpress.XtraEditors.XtraForm
    {

        #region Форм анх ачааллах функц

            private static DoctorDownForm INSTANCE = new DoctorDownForm();

            public static DoctorDownForm Instance
            {
                get
                {
                    return INSTANCE;
                }
            }

            private bool isInit = false;

            private DataRowView docView;

            private DoctorDownForm()
            {

            }

            private void initForm()
            {
                InitializeComponent();
                this.isInit = true;
                Icon = global::Pharmacy2016.Properties.Resources.mainSymbol;

                initDataSource();
                initError();
                initBinding();
            }

            private void initDataSource()
            {
                gridLookUpEditNurse.Properties.DataSource = PlanDataSet.SystemNurse1BindingSource;
                treeListLookUpEditWard.Properties.DataSource = JournalDataSet.WardOtherBindingSource;
                gridLookUpEditDrug.Properties.DataSource = PlanDataSet.DrugDoctorOtherBindingSource;
                gridControlDrug.DataSource = PlanDataSet.Instance.DrugDoctorDetailOther1;
                //gridLookUpEditPatient.Properties.DataSource = PlanDataSet.PatientDoctorBindingSource;
                gridLookUpEditPharma.Properties.DataSource = PlanDataSet.SystemDoctorBindingSource;
            }

            private void initError()
            {
                dateEditDate.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditNurse.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                treeListLookUpEditWard.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditEnd.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                dateEditStart.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditDrug.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                //gridLookUpEditPatient.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
                gridLookUpEditPharma.ErrorIconAlignment = ErrorIconAlignment.MiddleRight;
            }

        #endregion
        
        #region Форм нээх, хаах функц

            public void showForm(XtraForm form)
            {
                try
                {
                    if (!this.isInit)
                    {
                        initForm();
                    }

                    string id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    PlanDataSet.Instance.DrugJournal.idColumn.DefaultValue = id;
                    PlanDataSet.Instance.DrugDetail.drugJournalIDColumn.DefaultValue = id;

                    docView = (DataRowView)PlanDataSet.DrugJournalBindingSource.AddNew();
                    DateTime now = DateTime.Now;
                    docView["id"] = id;
                    //docView["date"] = now;
                    docView["isSent"] = true;
                    //dateEditDate.DateTime = now;
                    dateEditStart.DateTime = now;
                    dateEditEnd.DateTime = now;

                    reload(false);
                    ShowDialog(form);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Цонх ачааллахад алдаа гарлаа " + ex.Message);
                }
                finally
                {
                    clearData();
                    ProgramUtil.closeAlertDialog();
                    ProgramUtil.closeWaitDialog();
                }
            }

            private void initBinding()
            {
                dateEditDate.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "date", true));
                gridLookUpEditNurse.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "nurseID", true));
                treeListLookUpEditWard.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "nurseWardID", true));
                //gridLookUpEditDrug.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "patientID", true));
                gridLookUpEditPharma.DataBindings.Add(new Binding("EditValue", PlanDataSet.DrugJournalBindingSource, "userID", true));
            }

            private void clearData()
            {
                clearOrder();

                PlanDataSet.SystemNurse1BindingSource.Filter = "";
                PlanDataSet.SystemDoctorBindingSource.Filter = "";
                JournalDataSet.WardOtherBindingSource.Filter = "";

                PlanDataSet.Instance.DrugDoctorDetailOther1.Clear();
                PlanDataSet.Instance.DrugDoctorOther.Clear();
                PlanDataSet.Instance.DrugDoctorDetailOther.Clear();
                PlanDataSet.DrugJournalBindingSource.CancelEdit();
                PlanDataSet.Instance.DrugJournal.RejectChanges();
                PlanDataSet.DrugDetailBindingSource.CancelEdit();
                PlanDataSet.Instance.DrugDetail.RejectChanges();
            }

            private void clearOrder()
            {
                dateEditDate.EditValue = null;
                gridLookUpEditDrug.EditValue = null;
                gridLookUpEditNurse.EditValue = null;
                //gridLookUpEditPatient.EditValue = null;
                gridLookUpEditPharma.EditValue = null;
                treeListLookUpEditWard.EditValue = null;
                gridLookUpEditDrug.EditValue = null;
            }

            private void DoctorDownForm_Shown(object sender, EventArgs e)
            {
                if (UserUtil.getUserRoleId() == RoleUtil.ADMIN_ID)
                {
                    gridLookUpEditNurse.Properties.ReadOnly = false;
                    gridLookUpEditNurse.Focus();
                }
                else
                {
                    gridLookUpEditNurse.Properties.ReadOnly = true;
                    gridLookUpEditNurse.EditValue = UserUtil.getUserId();
                    treeListLookUpEditWard.Focus();
                }
            }

        #endregion

        #region Формын event

            private void dateEditDate_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditDate.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditNurse.Focus();
                    }
                }
            }

            private void gridLookUpEditNurse_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditNurse.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        treeListLookUpEditWard.Focus();
                    }
                }
            }

            private void treeListLookUpEditWard_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        treeListLookUpEditWard.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditPharma.Focus();
                    }
                }
            }

            private void gridLookUpEditPharma_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditPharma.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        dateEditStart.Focus();
                    }
                }
            }

            private void dateEditStart_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditStart.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        dateEditEnd.Focus();
                    }
                }
            }

            private void dateEditEnd_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        dateEditEnd.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridLookUpEditDrug.Focus();
                    }
                }
            }

            private void gridLookUpEditOrder_KeyUp(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == ProgramUtil.KeyMoveNextControl)
                {
                    if (ProgramUtil.IsShowPopup)
                    {
                        ProgramUtil.IsShowPopup = false;
                        gridLookUpEditDrug.ShowPopup();
                    }
                    else
                    {
                        ProgramUtil.IsShowPopup = true;
                        gridControlDrug.Focus();
                    }
                }
            }

            private void gridLookUpEditOrder_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
            {
                if (Visible)
                {
                    DataRowView row = (DataRowView)gridLookUpEditDrug.GetSelectedDataRow();
                    if (e.Value != DBNull.Value && e.Value != null && row != null)
                        e.DisplayText = row["doctorName"].ToString();
                    else
                        e.DisplayText = "";
                }
            }

            private void gridLookUpEditDrug_EditValueChanged(object sender, EventArgs e)
            {
                if (Visible && gridLookUpEditDrug.EditValue != null && gridLookUpEditDrug.EditValue != DBNull.Value)
                    changeOrder();
            }

            private void gridLookUpEditOrder_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                if (String.Equals(e.Button.Tag, "reload"))
                    reloadDrug();
            }

            private void gridLookUpEditNurse_EditValueChanged(object sender, EventArgs e)
            {
                if (Visible && gridLookUpEditNurse.EditValue != DBNull.Value && gridLookUpEditNurse.EditValue != null)
                {
                    reloadDrug();
                    JournalDataSet.WardOtherBindingSource.Filter = UserUtil.getUserWard(gridLookUpEditNurse.EditValue.ToString());
                }
            }

            private void treeListLookUpEditWard_EditValueChanged(object sender, EventArgs e)
            {       
                if (Visible && treeListLookUpEditWard.EditValue != DBNull.Value && treeListLookUpEditWard.EditValue != null)
                {
                    PlanDataSet.SystemDoctorBindingSource.Filter = "wardName = '" + treeListLookUpEditWard.Text + "'";
                }
            }
            

            private void simpleButtonReload_Click(object sender, EventArgs e)
            {
                reload(true);
            }

            private void simpleButtonDownload_Click(object sender, EventArgs e)
            {
                save();
            }

        #endregion

        #region Формын функц

            private bool checkDrug()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();

                if (dateEditStart.Text == "")
                {
                    errorText = "Эхлэх огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }
                if (dateEditEnd.Text == "")
                {
                    errorText = "Дуусах огноог оруулна уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditEnd.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        dateEditEnd.Focus();
                    }
                }
                if (!gridLookUpEditNurse.Properties.ReadOnly && (gridLookUpEditNurse.EditValue == null || gridLookUpEditNurse.EditValue == DBNull.Value || gridLookUpEditNurse.Text.Equals("")))
                {
                    errorText = "Няравыг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditNurse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditNurse.Focus();
                    }
                }
                if (isRight && dateEditStart.DateTime > dateEditEnd.DateTime)
                {
                    errorText = "Эхлэх огноо, дуусах огнооноос хойно байж болохгүй";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditStart.ErrorText = errorText;
                    isRight = false;
                    dateEditStart.Focus();
                }

                if (!isRight)
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void reloadDrug()
            {
                if (checkDrug())
                {
                    string user = "";
                    if (gridLookUpEditNurse.Visible)
                        user = gridLookUpEditNurse.EditValue.ToString();
                    else
                        user = UserUtil.getUserId();

                    //int orderType = type == 1 ? 0 : 1;

                    PlanDataSet.DrugDoctorOtherTableAdapter.Fill(PlanDataSet.Instance.DrugDoctorOther, HospitalUtil.getHospitalId(), user, dateEditStart.Text, dateEditEnd.Text);
                    PlanDataSet.DrugDoctorDetailOtherTableAdapter.Fill(PlanDataSet.Instance.DrugDoctorDetailOther, HospitalUtil.getHospitalId(), user, dateEditStart.Text, dateEditEnd.Text);
                }
            }

            private void reload(bool isReload)
            {
                PlanDataSet.SystemDoctorTableAdapter.Fill(PlanDataSet.Instance.SystemDoctor, HospitalUtil.getHospitalId(), SystemUtil.PHARMACIST);
                PlanDataSet.SystemNurse1TableAdapter.Fill(PlanDataSet.Instance.SystemNurse1, HospitalUtil.getHospitalId(), SystemUtil.ALL_NURSE);
                //PlanDataSet.PatientDoctorTableAdapter.Fill(PlanDataSet.Instance.PatientDoctor, HospitalUtil.getHospitalId());
                if (isReload || JournalDataSet.Instance.WardOther.Rows.Count == 0)
                    JournalDataSet.WardOtherTableAdapter.Fill(JournalDataSet.Instance.WardOther, HospitalUtil.getHospitalId());
            }

            private void changeOrder()
            {
                PlanDataSet.Instance.DrugDoctorDetailOther1.Clear();
                DataRow[] rows = PlanDataSet.Instance.DrugDoctorDetailOther.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND drugDoctorID = '" + gridLookUpEditDrug.EditValue + "'");
                for (int i = 0; i < rows.Length; i++)
                {
                    DataRow newRow = PlanDataSet.Instance.DrugDoctorDetailOther1.NewRow();
                    newRow["drugDoctorID"] = rows[i]["drugDoctorID"];
                    newRow["id"] = rows[i]["id"];
                    newRow["medicineID"] = rows[i]["medicineID"];
                    newRow["medicineName"] = rows[i]["medicineName"];
                    newRow["medicineTypeName"] = rows[i]["medicineTypeName"];
                    newRow["latinName"] = rows[i]["latinName"];
                    newRow["unit"] = rows[i]["unit"];
                    newRow["shape"] = rows[i]["shape"];
                    newRow["quantity"] = rows[i]["quantity"];
                    newRow["count"] = rows[i]["count"];
                    newRow.EndEdit();
                    PlanDataSet.Instance.DrugDoctorDetailOther1.Rows.Add(newRow);
                }
                DataRowView date = (DataRowView)gridLookUpEditDrug.GetSelectedDataRow();
                if (date != null)
                    dateEditDate.EditValue = date["date"];
            }

            private bool check()
            {
                bool isRight = true;
                string text = "", errorText;
                ProgramUtil.closeAlertDialog();
                if (dateEditDate.EditValue == DBNull.Value || dateEditDate.EditValue == null || dateEditDate.Text.Equals(""))
                {
                    errorText = "Огноог оруулна уу";
                    text = ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    dateEditDate.ErrorText = errorText;
                    isRight = false;
                    dateEditDate.Focus();
                }
                if (gridLookUpEditNurse.EditValue == DBNull.Value || gridLookUpEditNurse.EditValue == null || gridLookUpEditNurse.Text.Equals(""))
                {
                    errorText = "Сувилагчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditNurse.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditNurse.Focus();
                    }
                }
                if (treeListLookUpEditWard.EditValue == DBNull.Value || treeListLookUpEditWard.EditValue == null || treeListLookUpEditWard.Text.Equals(""))
                {
                    errorText = "Тасгийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    treeListLookUpEditWard.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        treeListLookUpEditWard.Focus();
                    }
                }
                if (gridLookUpEditDrug.EditValue == DBNull.Value || gridLookUpEditDrug.EditValue == null || gridLookUpEditDrug.Text.Equals(""))
                {
                    errorText = "Эмчилгээ сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditDrug.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditDrug.Focus();
                    }
                }
                //if (gridLookUpEditPatient.EditValue == DBNull.Value || gridLookUpEditPatient.EditValue == null || gridLookUpEditPatient.Text.Equals(""))
                //{
                //    errorText = "Эмчлүүлэгчийг сонгоно уу";
                //    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                //    gridLookUpEditPatient.ErrorText = errorText;
                //    if (isRight)
                //    {
                //        isRight = false;
                //        gridLookUpEditPatient.Focus();
                //    }
                //}
                if (gridLookUpEditPharma.EditValue == DBNull.Value || gridLookUpEditPharma.EditValue == null || gridLookUpEditPharma.Text.Equals(""))
                {
                    errorText = "Эм зүйчийг сонгоно уу";
                    text += text.Equals("") ? ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX : ProgramUtil.ALERT_DIALOG_LINE + ProgramUtil.ALERT_DIALOG_TEXT_PREFIX + errorText + ProgramUtil.ALERT_DIALOG_TEXT_SUBFIX;
                    gridLookUpEditPharma.ErrorText = errorText;
                    if (isRight)
                    {
                        isRight = false;
                        gridLookUpEditPharma.Focus();
                    }
                }

                if (!isRight && !text.Equals(""))
                    ProgramUtil.showAlertDialog(Instance, Instance.Text, text);

                return isRight;
            }

            private void save()
            {
                if (check())
                {
                    ProgramUtil.showWaitDialog(ProgramUtil.UPDATE_TITLE);
                    DataRowView user = (DataRowView)gridLookUpEditNurse.GetSelectedDataRow();
                    DataRowView ward = (DataRowView)treeListLookUpEditWard.GetSelectedDataRow();
                    DataRowView patient = (DataRowView)gridLookUpEditDrug.GetSelectedDataRow();
                    //DataRowView patient = (DataRowView)gridLookUpEditPatient.GetSelectedDataRow();
                    DataRowView pharma = (DataRowView)gridLookUpEditPharma.GetSelectedDataRow();

                    docView["nurseName"] = user["fullName"];
                    docView["nurseWardName"] = ward["name"];
                    docView["patientID"] = patient["patientID"];
                    docView["patientName"] = patient["patientName"];
                    docView["pharmaName"] = pharma["fullName"];
                    docView["date"] = dateEditDate.EditValue;
                    //string id = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    //for (int i = 0; i < gridViewDrug.RowCount; i++)
                    //{
                    //    if (gridViewDrug.RowCount > 0)
                    //    {
                    //        PlanDataSet.Instance.DrugDetail.idColumn.DefaultValue = id;
                    //        PlanDataSet.Instance.DrugDetail.drugJournalIDColumn.DefaultValue = docView["id"];
                    //        DataRowView newRow = (DataRowView)PlanDataSet.DrugDetailOtherBindingSource.AddNew();

                    //        newRow["medicineID"] = dateEditDate.EditValue;
                    //        newRow["count"] = gridLookUpEditNurse.EditValue;
                    //        newRow["userName"] = user["fullName"];
                    //        newRow["roleName"] = user["roleName"];    
                    //    }
                    //}
                    for (int i = 0; i < gridViewDrug.RowCount; i++)
                    {
                        DataRow row = gridViewDrug.GetDataRow(i);
                        addNewRow(row);
                    }
                    PlanDataSet.DrugJournalBindingSource.EndEdit();
                    PlanDataSet.DrugJournalTableAdapter.Update(PlanDataSet.Instance.DrugJournal);
                    PlanDataSet.DrugDetailTableAdapter.Update(PlanDataSet.Instance.DrugDetail);

                    //UserDataSet.QueriesTableAdapter.update_drug_doctor_journal(HospitalUtil.getHospitalId(), patient["id"].ToString());

                    ProgramUtil.closeWaitDialog();
                    Close();
                }
            }

            private void addNewRow(DataRow row)
            {
                PlanDataSet.Instance.DrugDetail.idColumn.DefaultValue = getNotExistId();
                PlanDataSet.Instance.DrugDetail.drugJournalIDColumn.DefaultValue = docView["id"];
                DataRowView view = (DataRowView)PlanDataSet.DrugDetailOtherBindingSource.AddNew();
                view["medicineID"] = row["medicineID"];
                view["medicineName"] = row["medicineName"];
                view["latinName"] = row["latinName"];
                view["unitType"] = row["unit"];
                view["medicineTypeName"] = row["medicineTypeName"];
                view["shape"] = row["shape"];
                view["validDate"] = row["validDate"];
                view["price"] = row["price"];
                view["count"] = row["count"];
                view.EndEdit();
            }

            private string getNotExistId()
            {
                string newID = null;
                DataRow[] rows = null;
                do
                {
                    newID = JournalDataSet.QueriesTableAdapter.get_new_id(UserUtil.getUserId());
                    rows = PlanDataSet.Instance.DrugDetail.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND drugJournalID = '" + docView["id"] + "' AND id = '" + newID + "'");
                } while (rows != null && rows.Length > 0);

                return newID;
            }

        #endregion                                        
    }
}