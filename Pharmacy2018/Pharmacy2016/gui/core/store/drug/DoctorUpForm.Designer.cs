﻿namespace Pharmacy2016.gui.core.store.drug
{
    partial class DoctorUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DoctorUpForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditBreakDate = new DevExpress.XtraEditors.DateEdit();
            this.simpleButtonReload = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditIsBreak = new DevExpress.XtraEditors.CheckEdit();
            this.gridLookUpEditDoctor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControlExpUser = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridControlTreatment = new DevExpress.XtraGrid.GridControl();
            this.gridViewTreatment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumnEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMedicineName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDuration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnEachDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditInspectionDate = new DevExpress.XtraEditors.DateEdit();
            this.memoEditInspection = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButtonbackInspection = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSaveInspection = new DevExpress.XtraEditors.SimpleButton();
            this.gridLookUpEditNurse = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditPharma = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.checkEditIsSent = new DevExpress.XtraEditors.CheckEdit();
            this.gridLookUpEditPatient = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.treeListLookUpEditWard = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.labelControlWarehouse = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.gridLookUpEditOrder = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsBreak.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTreatment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTreatment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditInspection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditNurse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPharma.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsSent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl6.Location = new System.Drawing.Point(504, 58);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(38, 13);
            this.labelControl6.TabIndex = 54;
            this.labelControl6.Text = "Огноо :";
            // 
            // dateEditBreakDate
            // 
            this.dateEditBreakDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditBreakDate.EditValue = null;
            this.dateEditBreakDate.Location = new System.Drawing.Point(548, 55);
            this.dateEditBreakDate.Name = "dateEditBreakDate";
            this.dateEditBreakDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBreakDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditBreakDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBreakDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditBreakDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditBreakDate.Properties.ReadOnly = true;
            this.dateEditBreakDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditBreakDate.TabIndex = 9;
            this.dateEditBreakDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditBreakDate_KeyUp);
            // 
            // simpleButtonReload
            // 
            this.simpleButtonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonReload.Image")));
            this.simpleButtonReload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonReload.Location = new System.Drawing.Point(12, 526);
            this.simpleButtonReload.Name = "simpleButtonReload";
            this.simpleButtonReload.Size = new System.Drawing.Size(23, 23);
            this.simpleButtonReload.TabIndex = 17;
            this.simpleButtonReload.ToolTip = "Сонгодог талбаруудын өгөгдлийг сэргээх";
            this.simpleButtonReload.Click += new System.EventHandler(this.simpleButtonReload_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl5.Location = new System.Drawing.Point(442, 32);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(100, 13);
            this.labelControl5.TabIndex = 53;
            this.labelControl5.Text = "Эмчилгээг зогсоох :";
            // 
            // checkEditIsBreak
            // 
            this.checkEditIsBreak.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditIsBreak.EnterMoveNextControl = true;
            this.checkEditIsBreak.Location = new System.Drawing.Point(548, 29);
            this.checkEditIsBreak.Name = "checkEditIsBreak";
            this.checkEditIsBreak.Properties.Caption = "";
            this.checkEditIsBreak.Size = new System.Drawing.Size(20, 19);
            this.checkEditIsBreak.TabIndex = 8;
            this.checkEditIsBreak.CheckedChanged += new System.EventHandler(this.checkEditIsBreak_CheckedChanged);
            // 
            // gridLookUpEditDoctor
            // 
            this.gridLookUpEditDoctor.EditValue = "";
            this.gridLookUpEditDoctor.Location = new System.Drawing.Point(145, 81);
            this.gridLookUpEditDoctor.Name = "gridLookUpEditDoctor";
            this.gridLookUpEditDoctor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditDoctor.Properties.DisplayMember = "fullName";
            this.gridLookUpEditDoctor.Properties.NullText = "";
            this.gridLookUpEditDoctor.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditDoctor.Properties.ValueMember = "id";
            this.gridLookUpEditDoctor.Properties.View = this.gridView4;
            this.gridLookUpEditDoctor.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditDoctor.TabIndex = 3;
            this.gridLookUpEditDoctor.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditDoctor_KeyUp);
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowAutoFilterRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Овог";
            this.gridColumn1.CustomizationCaption = "Овог";
            this.gridColumn1.FieldName = "lastName";
            this.gridColumn1.MinWidth = 100;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 100;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Нэр";
            this.gridColumn21.CustomizationCaption = "Нэр";
            this.gridColumn21.FieldName = "firstName";
            this.gridColumn21.MinWidth = 100;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 0;
            this.gridColumn21.Width = 100;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Регистер";
            this.gridColumn22.CustomizationCaption = "Регистер";
            this.gridColumn22.FieldName = "id";
            this.gridColumn22.MinWidth = 75;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Эрх";
            this.gridColumn23.CustomizationCaption = "Эрх";
            this.gridColumn23.FieldName = "roleName";
            this.gridColumn23.MinWidth = 75;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Тасаг";
            this.gridColumn24.CustomizationCaption = "Тасаг";
            this.gridColumn24.FieldName = "wardName";
            this.gridColumn24.MinWidth = 75;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 4;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Ажлын газар";
            this.gridColumn25.CustomizationCaption = "Ажлын газар";
            this.gridColumn25.FieldName = "organizationName";
            this.gridColumn25.MinWidth = 75;
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 5;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Албан тушаал";
            this.gridColumn26.CustomizationCaption = "Албан тушаал";
            this.gridColumn26.FieldName = "positionName";
            this.gridColumn26.MinWidth = 75;
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 6;
            // 
            // labelControlExpUser
            // 
            this.labelControlExpUser.Location = new System.Drawing.Point(110, 84);
            this.labelControlExpUser.Name = "labelControlExpUser";
            this.labelControlExpUser.Size = new System.Drawing.Size(29, 13);
            this.labelControlExpUser.TabIndex = 52;
            this.labelControlExpUser.Text = "Эмч*:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 235);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(53, 13);
            this.labelControl1.TabIndex = 48;
            this.labelControl1.Text = "Эмчилгээ :";
            // 
            // gridControlTreatment
            // 
            this.gridControlTreatment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlTreatment.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlTreatment.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlTreatment.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlTreatment.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlTreatment.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlTreatment.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Нэмэх", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Устгах", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Засах", "edit")});
            this.gridControlTreatment.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlTreatment_EmbeddedNavigator_ButtonClick);
            this.gridControlTreatment.Location = new System.Drawing.Point(12, 254);
            this.gridControlTreatment.MainView = this.gridViewTreatment;
            this.gridControlTreatment.Name = "gridControlTreatment";
            this.gridControlTreatment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemSpinEdit1});
            this.gridControlTreatment.Size = new System.Drawing.Size(660, 261);
            this.gridControlTreatment.TabIndex = 11;
            this.gridControlTreatment.UseEmbeddedNavigator = true;
            this.gridControlTreatment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTreatment});
            // 
            // gridViewTreatment
            // 
            this.gridViewTreatment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnStartDate,
            this.gridColumnEndDate,
            this.gridColumnMedicineName,
            this.gridColumnDuration,
            this.gridColumnEachDay,
            this.gridColumnCount,
            this.gridColumn13});
            this.gridViewTreatment.GridControl = this.gridControlTreatment;
            this.gridViewTreatment.Name = "gridViewTreatment";
            this.gridViewTreatment.OptionsBehavior.ReadOnly = true;
            this.gridViewTreatment.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridViewTreatment.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnStartDate
            // 
            this.gridColumnStartDate.Caption = "Эхэлсэн хугацаа";
            this.gridColumnStartDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnStartDate.CustomizationCaption = "Эхэлсэн хугацаа";
            this.gridColumnStartDate.FieldName = "startDate";
            this.gridColumnStartDate.Name = "gridColumnStartDate";
            this.gridColumnStartDate.Visible = true;
            this.gridColumnStartDate.VisibleIndex = 0;
            this.gridColumnStartDate.Width = 88;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.EditFormat.FormatString = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // gridColumnEndDate
            // 
            this.gridColumnEndDate.Caption = "Дууссан хугацаа";
            this.gridColumnEndDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnEndDate.CustomizationCaption = "Дууссан хугацаа";
            this.gridColumnEndDate.FieldName = "endDate";
            this.gridColumnEndDate.Name = "gridColumnEndDate";
            this.gridColumnEndDate.Visible = true;
            this.gridColumnEndDate.VisibleIndex = 1;
            this.gridColumnEndDate.Width = 88;
            // 
            // gridColumnMedicineName
            // 
            this.gridColumnMedicineName.Caption = "Эмийн нэр";
            this.gridColumnMedicineName.CustomizationCaption = "Худалдааны нэр";
            this.gridColumnMedicineName.FieldName = "medicineName";
            this.gridColumnMedicineName.Name = "gridColumnMedicineName";
            this.gridColumnMedicineName.Visible = true;
            this.gridColumnMedicineName.VisibleIndex = 2;
            this.gridColumnMedicineName.Width = 88;
            // 
            // gridColumnDuration
            // 
            this.gridColumnDuration.Caption = "Үргэлжлэх хугацаа";
            this.gridColumnDuration.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnDuration.CustomizationCaption = "Үргэлжлэх хугацаа";
            this.gridColumnDuration.FieldName = "duration";
            this.gridColumnDuration.Name = "gridColumnDuration";
            this.gridColumnDuration.Visible = true;
            this.gridColumnDuration.VisibleIndex = 4;
            this.gridColumnDuration.Width = 99;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.DisplayFormat.FormatString = "n0";
            this.repositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.EditFormat.FormatString = "n0";
            this.repositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit1.Mask.EditMask = "n0";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // gridColumnEachDay
            // 
            this.gridColumnEachDay.Caption = "Өдөрт уух хэмжээ";
            this.gridColumnEachDay.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnEachDay.CustomizationCaption = "Өдөрт уух хэмжээ";
            this.gridColumnEachDay.FieldName = "eachDay";
            this.gridColumnEachDay.Name = "gridColumnEachDay";
            this.gridColumnEachDay.Visible = true;
            this.gridColumnEachDay.VisibleIndex = 3;
            this.gridColumnEachDay.Width = 98;
            // 
            // gridColumnCount
            // 
            this.gridColumnCount.Caption = "Тоо";
            this.gridColumnCount.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnCount.CustomizationCaption = "Тоо хэмжээ";
            this.gridColumnCount.FieldName = "count";
            this.gridColumnCount.MinWidth = 75;
            this.gridColumnCount.Name = "gridColumnCount";
            this.gridColumnCount.Visible = true;
            this.gridColumnCount.VisibleIndex = 5;
            this.gridColumnCount.Width = 77;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Зогссон эсэх";
            this.gridColumn13.CustomizationCaption = "Зогссон эсэх";
            this.gridColumn13.FieldName = "isBreak";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 6;
            this.gridColumn13.Width = 70;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(33, 187);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(106, 13);
            this.labelControl3.TabIndex = 45;
            this.labelControl3.Text = "Үзлэгийн тэмдэглэл :";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(98, 162);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(41, 13);
            this.labelControl2.TabIndex = 44;
            this.labelControl2.Text = "Огноо*:";
            // 
            // dateEditInspectionDate
            // 
            this.dateEditInspectionDate.EditValue = null;
            this.dateEditInspectionDate.Location = new System.Drawing.Point(145, 159);
            this.dateEditInspectionDate.Name = "dateEditInspectionDate";
            this.dateEditInspectionDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditInspectionDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditInspectionDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dateEditInspectionDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditInspectionDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateEditInspectionDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEditInspectionDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEditInspectionDate.Size = new System.Drawing.Size(100, 20);
            this.dateEditInspectionDate.TabIndex = 6;
            this.dateEditInspectionDate.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dateEditInspectionDate_KeyUp);
            // 
            // memoEditInspection
            // 
            this.memoEditInspection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEditInspection.EnterMoveNextControl = true;
            this.memoEditInspection.Location = new System.Drawing.Point(145, 185);
            this.memoEditInspection.Name = "memoEditInspection";
            this.memoEditInspection.Properties.MaxLength = 200;
            this.memoEditInspection.Size = new System.Drawing.Size(503, 63);
            this.memoEditInspection.TabIndex = 7;
            // 
            // simpleButtonbackInspection
            // 
            this.simpleButtonbackInspection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonbackInspection.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonbackInspection.Location = new System.Drawing.Point(597, 526);
            this.simpleButtonbackInspection.Name = "simpleButtonbackInspection";
            this.simpleButtonbackInspection.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonbackInspection.TabIndex = 16;
            this.simpleButtonbackInspection.Text = "Гарах";
            this.simpleButtonbackInspection.ToolTip = "Гарах";
            this.simpleButtonbackInspection.Click += new System.EventHandler(this.simpleButtonbackInspection_Click);
            // 
            // simpleButtonSaveInspection
            // 
            this.simpleButtonSaveInspection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonSaveInspection.Location = new System.Drawing.Point(516, 526);
            this.simpleButtonSaveInspection.Name = "simpleButtonSaveInspection";
            this.simpleButtonSaveInspection.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonSaveInspection.TabIndex = 15;
            this.simpleButtonSaveInspection.Text = "Хадгалах";
            this.simpleButtonSaveInspection.ToolTip = "Өгөгдлийг хадгалах";
            this.simpleButtonSaveInspection.Click += new System.EventHandler(this.simpleButtonSaveInspection_Click);
            // 
            // gridLookUpEditNurse
            // 
            this.gridLookUpEditNurse.EditValue = "";
            this.gridLookUpEditNurse.Location = new System.Drawing.Point(145, 29);
            this.gridLookUpEditNurse.Name = "gridLookUpEditNurse";
            this.gridLookUpEditNurse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditNurse.Properties.DisplayMember = "fullName";
            this.gridLookUpEditNurse.Properties.NullText = "";
            this.gridLookUpEditNurse.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditNurse.Properties.ValueMember = "id";
            this.gridLookUpEditNurse.Properties.View = this.gridView1;
            this.gridLookUpEditNurse.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditNurse.TabIndex = 1;
            this.gridLookUpEditNurse.EditValueChanged += new System.EventHandler(this.gridLookUpEditNurse_EditValueChanged);
            this.gridLookUpEditNurse.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditNurse_KeyUp);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Нэр";
            this.gridColumn3.CustomizationCaption = "Нэр";
            this.gridColumn3.FieldName = "firstName";
            this.gridColumn3.MinWidth = 100;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 100;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Овог";
            this.gridColumn2.CustomizationCaption = "Овог";
            this.gridColumn2.FieldName = "lastName";
            this.gridColumn2.MinWidth = 100;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 100;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Регистер";
            this.gridColumn4.CustomizationCaption = "Регистер";
            this.gridColumn4.FieldName = "id";
            this.gridColumn4.MinWidth = 75;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Эрх";
            this.gridColumn5.CustomizationCaption = "Эрх";
            this.gridColumn5.FieldName = "roleName";
            this.gridColumn5.MinWidth = 75;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Тасаг";
            this.gridColumn6.CustomizationCaption = "Тасаг";
            this.gridColumn6.FieldName = "wardName";
            this.gridColumn6.MinWidth = 75;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Ажлын газар";
            this.gridColumn7.CustomizationCaption = "Ажлын газар";
            this.gridColumn7.FieldName = "organizationName";
            this.gridColumn7.MinWidth = 75;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Албан тушаал";
            this.gridColumn8.CustomizationCaption = "Албан тушаал";
            this.gridColumn8.FieldName = "positionName";
            this.gridColumn8.MinWidth = 75;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 6;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(81, 32);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(58, 13);
            this.labelControl4.TabIndex = 56;
            this.labelControl4.Text = "Сувилагч*:";
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl7.Location = new System.Drawing.Point(72, 110);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(67, 13);
            this.labelControl7.TabIndex = 59;
            this.labelControl7.Text = "Эмчлүүлэгч*:";
            // 
            // gridLookUpEditPharma
            // 
            this.gridLookUpEditPharma.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditPharma.EditValue = "";
            this.gridLookUpEditPharma.Location = new System.Drawing.Point(145, 133);
            this.gridLookUpEditPharma.Name = "gridLookUpEditPharma";
            this.gridLookUpEditPharma.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPharma.Properties.DisplayMember = "fullName";
            this.gridLookUpEditPharma.Properties.NullText = "";
            this.gridLookUpEditPharma.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditPharma.Properties.ValueMember = "id";
            this.gridLookUpEditPharma.Properties.View = this.gridView3;
            this.gridLookUpEditPharma.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditPharma.TabIndex = 5;
            this.gridLookUpEditPharma.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditPharma_KeyUp);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn14,
            this.gridColumn17,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn18});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ShowAutoFilterRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Дугаар";
            this.gridColumn14.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn14.FieldName = "id";
            this.gridColumn14.MinWidth = 75;
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Овог";
            this.gridColumn17.CustomizationCaption = "Овог";
            this.gridColumn17.FieldName = "lastName";
            this.gridColumn17.MinWidth = 75;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 2;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Нэр";
            this.gridColumn15.CustomizationCaption = "Нэр";
            this.gridColumn15.FieldName = "firstName";
            this.gridColumn15.MinWidth = 75;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 1;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Эрх";
            this.gridColumn16.CustomizationCaption = "Эрх";
            this.gridColumn16.FieldName = "roleName";
            this.gridColumn16.MinWidth = 75;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 3;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Тасаг";
            this.gridColumn18.CustomizationCaption = "Тасаг";
            this.gridColumn18.FieldName = "wardName";
            this.gridColumn18.MinWidth = 75;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 4;
            // 
            // labelControl9
            // 
            this.labelControl9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl9.Location = new System.Drawing.Point(91, 136);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(48, 13);
            this.labelControl9.TabIndex = 66;
            this.labelControl9.Text = "Эм зүйч*:";
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl8.Location = new System.Drawing.Point(580, 162);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(41, 13);
            this.labelControl8.TabIndex = 258;
            this.labelControl8.Text = "Илгээх :";
            // 
            // checkEditIsSent
            // 
            this.checkEditIsSent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditIsSent.EnterMoveNextControl = true;
            this.checkEditIsSent.Location = new System.Drawing.Point(627, 159);
            this.checkEditIsSent.Name = "checkEditIsSent";
            this.checkEditIsSent.Properties.Caption = "";
            this.checkEditIsSent.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEditIsSent.Size = new System.Drawing.Size(21, 19);
            this.checkEditIsSent.TabIndex = 10;
            // 
            // gridLookUpEditPatient
            // 
            this.gridLookUpEditPatient.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gridLookUpEditPatient.EditValue = "";
            this.gridLookUpEditPatient.Location = new System.Drawing.Point(145, 107);
            this.gridLookUpEditPatient.Name = "gridLookUpEditPatient";
            this.gridLookUpEditPatient.Properties.ActionButtonIndex = 1;
            this.gridLookUpEditPatient.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Нэмэх", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditPatient.Properties.DisplayMember = "fullName";
            this.gridLookUpEditPatient.Properties.NullText = "";
            this.gridLookUpEditPatient.Properties.PopupFormSize = new System.Drawing.Size(500, 300);
            this.gridLookUpEditPatient.Properties.ValueMember = "id";
            this.gridLookUpEditPatient.Properties.View = this.gridView2;
            this.gridLookUpEditPatient.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.gridLookUpEditPatient_Properties_ButtonClick);
            this.gridLookUpEditPatient.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditPatient.TabIndex = 4;
            this.gridLookUpEditPatient.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditPatient_KeyUp);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn19});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowAutoFilterRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Дугаар";
            this.gridColumn9.CustomizationCaption = "Хэрэглэгчийн дугаар";
            this.gridColumn9.FieldName = "id";
            this.gridColumn9.MinWidth = 75;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 0;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Овог";
            this.gridColumn10.CustomizationCaption = "Овог";
            this.gridColumn10.FieldName = "lastName";
            this.gridColumn10.MinWidth = 75;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Нэр";
            this.gridColumn11.CustomizationCaption = "Нэр";
            this.gridColumn11.FieldName = "firstName";
            this.gridColumn11.MinWidth = 75;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Регистер";
            this.gridColumn19.CustomizationCaption = "Регистер";
            this.gridColumn19.FieldName = "register";
            this.gridColumn19.MinWidth = 75;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 3;
            // 
            // treeListLookUpEditWard
            // 
            this.treeListLookUpEditWard.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.treeListLookUpEditWard.Location = new System.Drawing.Point(145, 55);
            this.treeListLookUpEditWard.Name = "treeListLookUpEditWard";
            this.treeListLookUpEditWard.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.treeListLookUpEditWard.Properties.DisplayMember = "name";
            this.treeListLookUpEditWard.Properties.NullText = "";
            this.treeListLookUpEditWard.Properties.TreeList = this.treeList1;
            this.treeListLookUpEditWard.Properties.ValueMember = "id";
            this.treeListLookUpEditWard.Size = new System.Drawing.Size(200, 20);
            this.treeListLookUpEditWard.TabIndex = 2;
            this.treeListLookUpEditWard.EditValueChanged += new System.EventHandler(this.treeListLookUpEditWard_EditValueChanged);
            this.treeListLookUpEditWard.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeListLookUpEditWard_KeyUp);
            // 
            // treeList1
            // 
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.treeList1.KeyFieldName = "id";
            this.treeList1.Location = new System.Drawing.Point(142, 222);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.ParentFieldName = "pid";
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "Нэр";
            this.treeListColumn2.FieldName = "name";
            this.treeListColumn2.MinWidth = 32;
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // labelControlWarehouse
            // 
            this.labelControlWarehouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControlWarehouse.Location = new System.Drawing.Point(101, 58);
            this.labelControlWarehouse.Name = "labelControlWarehouse";
            this.labelControlWarehouse.Size = new System.Drawing.Size(38, 13);
            this.labelControlWarehouse.TabIndex = 260;
            this.labelControlWarehouse.Text = "Тасаг*:";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(81, 6);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(57, 13);
            this.labelControl10.TabIndex = 262;
            this.labelControl10.Text = "Захиалга*:";
            // 
            // gridLookUpEditOrder
            // 
            this.gridLookUpEditOrder.EditValue = "";
            this.gridLookUpEditOrder.Location = new System.Drawing.Point(145, 3);
            this.gridLookUpEditOrder.Name = "gridLookUpEditOrder";
            this.gridLookUpEditOrder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEditOrder.Properties.DisplayMember = "name";
            this.gridLookUpEditOrder.Properties.PopupFormSize = new System.Drawing.Size(100, 100);
            this.gridLookUpEditOrder.Properties.ValueMember = "id";
            this.gridLookUpEditOrder.Properties.View = this.gridLookUpEdit1View;
            this.gridLookUpEditOrder.Size = new System.Drawing.Size(200, 20);
            this.gridLookUpEditOrder.TabIndex = 0;
            this.gridLookUpEditOrder.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gridLookUpEditOrder_KeyUp);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Захиалга";
            this.gridColumn12.FieldName = "name";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 0;
            // 
            // DoctorUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonbackInspection;
            this.ClientSize = new System.Drawing.Size(684, 561);
            this.Controls.Add(this.gridLookUpEditOrder);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.treeListLookUpEditWard);
            this.Controls.Add(this.labelControlWarehouse);
            this.Controls.Add(this.gridLookUpEditPatient);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.checkEditIsSent);
            this.Controls.Add(this.gridLookUpEditPharma);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.gridLookUpEditNurse);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.dateEditBreakDate);
            this.Controls.Add(this.simpleButtonReload);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.checkEditIsBreak);
            this.Controls.Add(this.gridLookUpEditDoctor);
            this.Controls.Add(this.labelControlExpUser);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.gridControlTreatment);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.dateEditInspectionDate);
            this.Controls.Add(this.memoEditInspection);
            this.Controls.Add(this.simpleButtonbackInspection);
            this.Controls.Add(this.simpleButtonSaveInspection);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DoctorUpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Эмчилгээ";
            this.Shown += new System.EventHandler(this.DoctorUpForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditBreakDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsBreak.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditDoctor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTreatment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTreatment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditInspectionDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditInspection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditNurse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPharma.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditIsSent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditPatient.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEditWard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEditOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.DateEdit dateEditBreakDate;
        private DevExpress.XtraEditors.SimpleButton simpleButtonReload;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CheckEdit checkEditIsBreak;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditDoctor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.LabelControl labelControlExpUser;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gridControlTreatment;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTreatment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMedicineName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDuration;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEachDay;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit dateEditInspectionDate;
        private DevExpress.XtraEditors.MemoEdit memoEditInspection;
        private DevExpress.XtraEditors.SimpleButton simpleButtonbackInspection;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSaveInspection;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditNurse;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPharma;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.CheckEdit checkEditIsSent;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditPatient;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraEditors.TreeListLookUpEdit treeListLookUpEditWard;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraEditors.LabelControl labelControlWarehouse;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCount;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEditOrder;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
    }
}