﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Pharmacy2016
{
    public static class DatabaseConnection
    {
        public static DataSet SelectQuery(string query)
        {
            DataSet ds = new DataSet();
            SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
            myConn.Open();
            SqlCommand objComman = new SqlCommand(query, myConn);
            SqlDataAdapter da = new SqlDataAdapter(objComman);
            da.Fill(ds);
            myConn.Close();
            return ds;
        }
    }
}
