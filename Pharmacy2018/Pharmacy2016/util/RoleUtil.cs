﻿using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.Internal;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.conf;
using Pharmacy2016.gui.core.main;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacy2016.util
{
    /*
     * Хэрэглэгчийн үндсэн эрхүүд болон нэвтэрсэн хэрэглэгчийн цонхны эрхийг агуулах класс
     * 
     */

    public class RoleUtil
    {
        
        #region Эрхийн анхдагч тогтмол утгууд

            public static readonly int ADMIN_ID = 1;
            public static readonly int DIRECTOR_ID = 2;
            public static readonly int ACCOUNTANT_ID = 3;
            public static readonly int STOREMAN_ID = 4;
            public static readonly int PHARMACIST_ID = 5;
            public static readonly int DOCTOR_ID = 6;
            public static readonly int HEAD_NURSE_ID = 7;
            public static readonly int NURSE_ID = 8;
            public static readonly int RECEPTION_ID = 9;
            public static readonly int TREATMENT_DIRECTOR_ID = 10;

            private static Dictionary<int, string> ROLES;
            private static Dictionary<int, BarItem> MENUS = new Dictionary<int, BarItem>();
            private static Dictionary<int, XtraForm> FORMS = new Dictionary<int, XtraForm>();
            private static List<string> CONFIRMED_ROLE = new List<string>();

            private static object ACCOUNT_USER_ID = null;
            private static bool IS_USE_STORE_ORDER = false;
            private static bool IS_USE_STORE_DRUG = false;
            private static object BACK_UP_PATH = null;
            private static int LIMIT_YEAR = 0;
            private static string CONFIRM = "";
            private static int DRUG_EXP_TYPE = 0;
            private static int MEDICINE_EXP_TYPE = 0;
            private static string MEDICINE_ACCOUNT = null;
            private static object LOCK_USER_ID = null;
            
        #endregion

        #region Эрхийн мэдээллийг авах функц

            public static string getRoleName(int id)
            {
                if (ROLES.ContainsKey(id))
                    return ROLES[id];
                return "";
            }

            public static void setRole(DataRowCollection rows)
            {
                ROLES = new Dictionary<int, string>();

                ROLES.Add(ADMIN_ID, "");
                ROLES.Add(DIRECTOR_ID, "");
                ROLES.Add(ACCOUNTANT_ID, "");
                ROLES.Add(STOREMAN_ID, "");
                ROLES.Add(PHARMACIST_ID, "");
                ROLES.Add(DOCTOR_ID, "");
                ROLES.Add(HEAD_NURSE_ID, "");
                ROLES.Add(NURSE_ID, "");
                ROLES.Add(RECEPTION_ID, "");

                foreach (DataRow row in rows)
                {
                    int id = Convert.ToInt32(row["id"]);
                    if (ROLES.ContainsKey(id))
                    {
                        ROLES[id] = row["name"].ToString();
                    }
                    else
                    {
                        ROLES.Add(id, row["name"].ToString());
                    }
                }
            }

            public static void setMainConf(DataRow row)
            {
                ACCOUNT_USER_ID = row["accountUserID"];
                IS_USE_STORE_ORDER = Convert.ToBoolean(row["isUseStoreOrder"]);
                IS_USE_STORE_DRUG = Convert.ToBoolean(row["isUseStoreDrug"]);
                BACK_UP_PATH = row["backUpPath"];
                LIMIT_YEAR = Convert.ToInt32(row["limitYear"]);
                SystemUtil.END_BAL_DATE = SystemUtil.BEG_BAL_DATE.AddYears(LIMIT_YEAR);
                CONFIRM = row["configure"].ToString();
                if (row["drugExpType"] != DBNull.Value && row["drugExpType"] != null)
                    DRUG_EXP_TYPE = Convert.ToInt32(row["drugExpType"]);
                if (row["medicineExpType"] != DBNull.Value && row["medicineExpType"] != null)
                    MEDICINE_EXP_TYPE = Convert.ToInt32(row["medicineExpType"]);
                MEDICINE_ACCOUNT = row["medicineAccount"].ToString();
                LOCK_USER_ID = row["lockUserID"];

            }

            public static object getAccountUserID()
            {
                return ACCOUNT_USER_ID;
            }

            public static bool getIsUseStoreOrder()
            {
                return IS_USE_STORE_ORDER;
            }

            public static bool getIsUseStoreDrug()
            {
                return IS_USE_STORE_DRUG;
            }

            public static object getBackUpPath()
            {
                return BACK_UP_PATH;
            }

            public static int getLimitYear()
            {
                return LIMIT_YEAR;
            }

            public static string getConfirm()
            {
                return CONFIRM;
            }

            public static int getDrugExpType()
            {
                return DRUG_EXP_TYPE;
            }

            public static int getMedicineExpType()
            {
                return MEDICINE_EXP_TYPE;
            }

            public static string getMedicineAccount()
            {
                return MEDICINE_ACCOUNT;
            }

            public static object getLockUserID()
            {
                return LOCK_USER_ID;
            }

        #endregion

        #region Цонх, цэсний мэдээллийг тохируулах функц

            public static void addMenu(RibbonControl control)
            {
                foreach (RibbonPage page in control.Pages)
                {
                    foreach (RibbonPageGroup group in page.Groups)
                    {
                        RibbonPageGroupItemLinkCollection collection = group.ItemLinks;
                        for (int i = 0; i < collection.Count; i++)
                        {
                            if (collection[i].Item.GetType() == typeof(BarButtonItem) && Convert.ToInt32(((BarButtonItem)collection[i].Item).Tag) > 0)
                            {
                                MENUS.Add(Convert.ToInt32(((BarButtonItem)collection[i].Item).Tag), ((BarButtonItem)collection[i].Item));
                            }
                            //else if (collection[i].Item.GetType() == typeof(SkinRibbonGalleryBarItem))
                            //{
                            //    MENUS.Add(Convert.ToInt32(((SkinRibbonGalleryBarItem)collection[i].Item).Tag), collection[i].Item);
                            //}
                        }
                    }
                }
            }

            public static void addForm(int id, XtraForm form)
            {
                FORMS.Add(id, form);
            }

            public static void setMenu(int id, bool show)
            {
                MENUS[id].Enabled = show;
            }

            public static void closeForm()
            {
                foreach (XtraForm form in FORMS.Values)
                {
                    if(form.Visible)
                        form.Close();
                }

                if(ProgramLogForm.Instance.Visible)
                    ProgramLogForm.Instance.Close();
            }

            public static bool getAccessList(string roleID)
            {
                CONFIRMED_ROLE = CONFIRM.Split(',').ToList();
                foreach (string Conf in CONFIRMED_ROLE)
                {
                    if (Conf.Contains(roleID))
                    {
                        return true;
                    }
                }     
                return false;
            }

        #endregion

    }
}
