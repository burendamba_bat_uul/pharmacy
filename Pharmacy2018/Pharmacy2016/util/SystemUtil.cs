﻿using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.conf;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.main;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using SKM.V3;

namespace Pharmacy2016.util
{
    /*
     * Системийн хэрэглэгчийн төрөл, үгийг хувиргах, програм анх ачааллаж эхлэх функцыг агуулсан класс
     * 
     */

    public class SystemUtil
    {

        #region Хэрэглэгчийн төрлийн утгууд

            public static readonly int STOREMAN = 1;
            public static readonly int STOREMAN_AND_PHARMACIST = 2;
            public static readonly int ALL_USER = 3;
            public static readonly int PHARMACIST = 4;
            public static readonly int DIRECTOR = 5;
            public static readonly int DOCTOR = 6;
            public static readonly int ACCOUNT = 7;
            public static readonly int TREATMENT_DIRECTOR = 8;
            public static readonly int ALL_NURSE = 9;
            public static readonly int LOCK_ADMIN = 10;

        #endregion

        #region Өгөгдлийн сангийн тохиргоо

            public static int connectTimeOut = 30;

            public static string CURR_DB = "";

            public static string SELECTED_DB = "";

            public static bool SELECTED_DB_READONLY = false;

            public static DateTime BEG_BAL_DATE;

            public static DateTime END_BAL_DATE;

        #endregion

        #region Үгийг нууцлах функц

            public readonly static string dbFile = "dbConf.txt";
            private static readonly string PasswordHash = "P@sSw0rd";
            private static readonly string SaltKey = "S@LT&KeY";
            private static readonly string VIKey = "@1B8c9D4e7F5g3H4";
            //public readonly static string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            //private static readonly string PasswordHash = "YeewOu";
            //private static readonly string SaltKey = "VglmKey";
            //private static readonly string VIKey = "2>nGaKey";


            public static string encrypt(string plainText)
            {
                try
                {
                    byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

                    byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
                    var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
                    var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

                    byte[] cipherTextBytes;

                    using (var memoryStream = new MemoryStream())
                    {
                        using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                            cryptoStream.FlushFinalBlock();
                            cipherTextBytes = memoryStream.ToArray();
                            cryptoStream.Close();
                        }
                        memoryStream.Close();
                    }
                    return Convert.ToBase64String(cipherTextBytes);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.ToString());
                    return "";
                }
            }

            public static string decrypt(string encryptedText)
            {
                try
                {
                    byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
                    byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
                    var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

                    var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
                    var memoryStream = new MemoryStream(cipherTextBytes);
                    var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                    byte[] plainTextBytes = new byte[cipherTextBytes.Length];

                    int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                    memoryStream.Close();
                    cryptoStream.Close();
                    return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
                }
                catch (Exception)
                {
                    return "";
                }
            }

        #endregion

        #region Лиценз шалгах утгууд
   
            public static string PUBLIC_KEY = "";

            public static string TOKEN = "";

            public static int PRODUCT_ID = 0;

            public static bool isLicense = false;

            public static string MACHINE_CODE = SKGL.SKM.getMachineCode(SKGL.SKM.getSHA1);

            public static string LICENSE_VALID_DATE = "Хүчинтэй хугацаа: 0";

            public readonly static string licenseFile = "licensekey.skm";

            public readonly static string licenseTokenFileName = "licenseToken.txt";

            public static string LICENSE_PATH = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/" + licenseFile;

        #endregion

        #region Өгөгдлийн сантай холбогдох тохиргооны файлыг унших функц

            public static bool IsConnect = false;

            public static void setTokens()
            {
                string dir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                string file = dir + "/" + licenseTokenFileName;
                if (File.Exists(file))
                {
                    string[] lines = File.ReadAllLines(file);
                    if (lines.Length > 0)
                    {
                        TOKEN = lines[0];
                        PRODUCT_ID = Convert.ToInt32(lines[1]);
                        PUBLIC_KEY = lines[2];
                    }
                }
            }

            public static bool checkConnectionFile()
            {
                bool isConnect = false;
                try
                {
                    string dir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                    string file = dir + "/" + dbFile;
                    if (File.Exists(file))
                    {
                        string[] lines = File.ReadAllLines(file);
                        if (lines.Length > 3)
                        {
                            string pass = decrypt(lines[3]);
                            isConnect = ConfDBForm.testConnection(lines[0], lines[1], lines[2], pass);
                            if (isConnect)
                            {
                                Pharmacy2016.Properties.Settings.Default.conDB = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", lines[0], lines[1], lines[2], pass);
                                Properties.Settings.Default.Save();

                                MainDBDataSet.initialize();
                                MainDBDataSet.MainDBTableAdapter.Fill(MainDBDataSet.Instance.MainDB);
                                DataRow[] row = MainDBDataSet.Instance.MainDB.Select("isCurrDB = 1");
                                CURR_DB = row[0]["name"].ToString();
                                BEG_BAL_DATE = Convert.ToDateTime(row[0]["begBalDate"]);
                                SELECTED_DB = CURR_DB;
                                if (row != null && row.Length > 0 && ConfDBForm.testConnection(lines[0], CURR_DB, lines[2], pass))
                                {
                                    Pharmacy2016.Properties.Settings.Default.conMedicine = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", lines[0], CURR_DB, lines[2], pass);
                                    Properties.Settings.Default.Save();
                                }
                                else
                                {
                                    isConnect = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show(file + " олдсонгүй");
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Файлаас уншихад алдаа гарлаа " + ex.ToString());
                }

                IsConnect = isConnect;
                return isConnect;
            }

            public static bool  LicenseVerification()
            {
                setTokens();
                isLicense = false;
                var license = new LicenseKey().LoadFromFile(LICENSE_PATH);
                if (license != null)
                {
                    if (license.Refresh(TOKEN, true) || license.HasValidSignature(PUBLIC_KEY, 90).IsValid())
                    {
                        LICENSE_VALID_DATE = "Хүчинтэй хугацаа: " + license.DaysLeft();
                        isLicense = true;
                        if (license.Created > DateTime.Now || license.Expires < DateTime.Now || !license.HasNotExpired().IsValid())
                        {
                            isLicense = false;
                            return isLicense;
                        }
                        if (license.Block)
                        {
                            isLicense = false;
                            return isLicense;
                        }
                        if (!license.IsOnRightMachine().IsValid())
                        {
                            isLicense = false;
                            return isLicense;
                        }
                        if (license.HasFeature(1).HasNotExpired().IsValid())
                            isLicense = true;
                        else if (license.HasNotFeature(1).IsValid())
                            isLicense = true;
                        else if (!license.HasNotExpired().IsValid()){
                            isLicense = false;
                            return isLicense;
                        }  
                        else
                        {
                            //XtraMessageBox.Show("Таны лицензийн хугацаа дууссан байна ашиглаж болохгүй.");
                            isLicense = false;
                        }
                    }
                    else
                    {
                        //XtraMessageBox.Show("Таны лицензийн хугацаа дууссан байна ашиглаж болохгүй.");
                        isLicense = false;
                    }
                }
                else
                {
                    isLicense = false;
                    //XtraMessageBox.Show("Лиценз унших файл олдсонгүй.");
                }
                return isLicense;
            }

        #endregion

        #region Програм анх ачааллаж эхлэх функц

            public static void loadProgram()
            {
                try
                {
                    LocaleUtils.initialize();

                    Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
                    AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                    SplashForm.showForm();

                    SplashForm.setInitializingPercent(10);

                    DevExpress.UserSkins.BonusSkins.Register();
                    //DevExpress.UserSkins.OfficeSkins.Register();

                    SplashForm.setInitializingPercent(20);

                    LicenseVerification();

                    checkConnectionFile();

                    SplashForm.setInitializingPercent(30);

                    ProgramUtil.initDataSet();

                    SplashForm.setInitializingPercent(50);

                    ProgramUtil.initControl();

                    SplashForm.setInitializingPercent(60);

                    if (IsConnect)
                    {
                        ProgramUtil.loadHospital();
                        SplashForm.setInitializingPercent(70);
                    }

                    SplashForm.setInitializingPercent(80);

                    MainForm.Instance.initForm();

                    SplashForm.setInitializingPercent(100);

                    SplashForm.closeForm();
                }
                catch(Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Програм ажиллах явцад гарлаа \n" + ex.ToString());
                    SplashForm.closeForm();
                }
            }

            static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
            {
                Console.WriteLine("Exception : " + e.ToString());
            }

            private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
            {
                Console.WriteLine("Exception : " + e.ToString());
                // Exit to avoid unhandled exception dialog
                //Environment.Exit(-1);
            }

        #endregion

    }
}
