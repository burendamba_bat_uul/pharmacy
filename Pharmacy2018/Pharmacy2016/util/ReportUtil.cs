﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacy2016.util
{
    /*
     * Тайлангийн ерөнхий тохиргоог агуулсан класс
     * 
     */

    public class ReportUtil
    {

        #region Текстийн форматжуулалт

            // Мөнгөн тэмдэгт
            private static readonly string DISPLAY_FORMAT_CURRENCY = "{0:c2}";
            public static string DisplayFormatCurrency
            {
                get { return DISPLAY_FORMAT_CURRENCY; }
            }

            // Бутархай тоо
            private static readonly string DISPLAY_FORMAT_FLOAT = "{0:n2}";
            public static string DisplayFormatFloat
            {
                get { return DISPLAY_FORMAT_FLOAT; }
            }

            // Бүхэл тоо
            private static readonly string DISPLAY_FORMAT_NUMBER = "{0:n0}";
            public static string DisplayFormatNumber
            {
                get { return DISPLAY_FORMAT_NUMBER; }
            }

            // Хувь
            private static readonly string DISPLAY_FORMAT_PERCENT = "{0:0.00%}";
            public static string DisplayFormatPercent
            {
                get { return DISPLAY_FORMAT_PERCENT; }
            }

            // Огноо цаг
            private static readonly string DISPLAY_FORMAT_DATETIME = "{0:yyyy-MM-dd HH:mm:ss}";
            public static string DisplayFormatDateTime
            {
                get { return DISPLAY_FORMAT_DATETIME; }
            }

            // Огноо цаг
            private static readonly string DISPLAY_FORMAT_TIME = "{0:HH:mm:ss}";
            public static string DisplayFormatTime
            {
                get { return DISPLAY_FORMAT_TIME; }
            }

            // Огноо
            private static readonly string DISPLAY_FORMAT_DATE = "{0:yyyy-MM-dd}";
            public static string DisplayFormatDate
            {
                get { return DISPLAY_FORMAT_DATE; }
            }

        #endregion
        
    }
}
