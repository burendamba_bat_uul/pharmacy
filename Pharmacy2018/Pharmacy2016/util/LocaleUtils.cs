﻿using System;
using System.Globalization;
using DevExpress.XtraGrid.Localization;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars.Localization;
using DevExpress.XtraPrinting.Localization;
using DevExpress.XtraTreeList.Localization;
using DevExpress.XtraNavBar;
using System.Windows.Forms;
using DevExpress.Utils;
using System.Threading;
using Pharmacy2016.util.locale;
using DevExpress.XtraPivotGrid.Localization;

namespace Pharmacy2016.util
{
    /*
     * Системд ашиглагддаг технологийн орчуулгын класс
     * 
     */

    // Utils класс нь удамшихгүй.
    public sealed class LocaleUtils
    {
        static string numName;
        static decimal amount;
       // Control 	            Class
        // XtraBars 	        DevExpress.XtraBars.Localization.BarLocalizer
        // XtraCharts 	        DevExpress.XtraCharts.Localization.ChartLocalizer
        // XtraEditors Library 	DevExpress.XtraEditors.Controls.Localizer
        // XtraGrid 	        DevExpress.XtraGrid.Localization.GridLocalizer
        // XtraLayout 	        DevExpress.XtraLayout.Localization.LayoutLocalizer
        // XtraNavBar 	        DevExpress.XtraNavBar.NavBarLocalizer
        // XtraPrinting Library DevExpress.XtraPrinting.Localization.PreviewLocalizer
        // XtraPivotGrid 	    DevExpress.XtraPivotGrid.Localization.PivotGridLocalizer
        // XtraReports 	        DevExpress.XtraReports.Localization.ReportLocalizer
        // XtraScheduler 	    DevExpress.XtraScheduler.Localization.SchedulerLocalizer
        // XtraSpellChecker 	DevExpress.XtraSpellChecker.Localization.SpellCheckerLocalizer
        // XtraTreeList 	    DevExpress.XtraTreeList.Localization.TreeListLocalizer
        // XtraVerticalGrid 	DevExpress.XtraVerticalGrid.Localization.VGridLocalizer

        // Utils классын байгуулагч функц.
        private LocaleUtils()
        {
        }

        // Програмын locale-ийг тохируулж, ачаалах функц.
        public static void initialize()
        {
            // Програмын locale-ийг тохируулж байна.
            CultureInfo culture = new CultureInfo("mn-MN", true);
            culture.NumberFormat.CurrencySymbol = "₮";
            culture.NumberFormat.CurrencyDecimalSeparator = ".";
            culture.NumberFormat.CurrencyGroupSeparator = ",";
            culture.NumberFormat.CurrencyNegativePattern = 4; // Энэ нь -9.00₮ => (9.00₮) гэсэн формат
            /*
             * CurrencyNegativePattern-ийн Pattern-ууд:
             * 
             * 0    ($n)
             * 1    -$n
             * 2    $-n
             * 3    $n-
             * 4    (n$) ** үүнийг ашиглана
             * 5    -n$
             * 6    n-$
             * 7    n$-
             * 8    -n $
             * 9    -$ n
             * 10   n $-
             * 11   $ n-
             * 12   $ -n
             * 13   n- $
             * 14   ($ n)
             * 15   (n $) 
            */
            culture.NumberFormat.NumberDecimalSeparator = ".";
            culture.NumberFormat.NumberGroupSeparator = ",";
            culture.NumberFormat.NumberNegativePattern = 0; // Энэ нь -9.00 => (9.00) гэсэн формат
            /*
             * NumberNegativePattern-ийн Pattern-ууд:
             * 
             * 0    (n)  ** үүнийг ашиглана
             * 1    -n
             * 2    - n
             * 3    n-
             * 4    n - 
            */
            culture.NumberFormat.PercentDecimalSeparator = ".";
            culture.NumberFormat.PercentGroupSeparator = ",";

            culture.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd";
            culture.DateTimeFormat.LongDatePattern = "yyyy-MM-dd hh:mm:ss";
            culture.DateTimeFormat.DateSeparator = "-";
            culture.DateTimeFormat.ShortTimePattern = "hh:mm";
            culture.DateTimeFormat.LongTimePattern = "hh:mm:ss";
            culture.DateTimeFormat.TimeSeparator = ":";

            Thread.CurrentThread.CurrentCulture = culture;
            FormatInfo.AlwaysUseThreadFormat = true;

            // Програмын статик текстүүдийн орчуулгыг агуулсан классууд
            GridLocalizer.Active = new GridLocalizerMongolian();
            Localizer.Active = new EditorsLocalizerMongolian();
            PivotGridLocalizer.Active = new PivotGridLocalizerMongolian();
            BarLocalizer.Active = new BarLocalizerMongolian();
            PreviewLocalizer.Active = new PreviewLocalizerMongolian();
            NavBarLocalizer.Active = new NavBarLocalizerMongolian();
            TreeListLocalizer.Active = new TreeListLocalizerMongolian();
        }
        
        public static string convertNumber(string number)
        {
            string decimalMoney = number.Substring(number.IndexOf('.') + 1, 2);
            string wholeMoney = number.Substring(0, number.IndexOf('.'));

            numName = "";
            amount = Convert.ToDecimal(wholeMoney);
            if (wholeMoney == "0"){
                numName = "0 ";
            } else {
                rekursiv("" + amount);
            }
            string moneyName = numName;

            numName = "";
            amount = Convert.ToDecimal(decimalMoney);
            if (decimalMoney == "00") {
                numName = "тэг";
            } else {
                rekursiv("" + amount);
            }

            return moneyName + " төгрөг " + numName + " мөнгө";
        }

        public static string numNames(string num, bool type)
        {
            if (num == "1")
            {
                if (type)
                {
                    return "нэг";
                }
                else
                {
                    return "арван";
                }
            }
            else if (num == "2")
            {
                if (type)
                {
                    return "хоёр";
                }
                else
                {
                    return "хорин";
                }
            }
            else if (num == "3")
            {
                if (type)
                {
                    return "гурван";
                }
                else
                {
                    return "гучин";
                }
            }
            else if (num == "4")
            {
                if (type)
                {
                    return "дөрвөн";
                }
                else
                {
                    return "дөчин";
                }
            }
            else if (num == "5")
            {
                if (type)
                {
                    return "таван";
                }
                else
                {
                    return "тавин";
                }
            }
            else if (num == "6")
            {
                if (type)
                {
                    return "зургаан";
                }
                else
                {
                    return "жаран";
                }
            }
            else if (num == "7")
            {
                if (type)
                {
                    return "долоон";
                }
                else
                {
                    return "далан";
                }
            }
            else if (num == "8")
            {
                if (type)
                {
                    return "найман";
                }
                else
                {
                    return "наян";
                }
            }
            else if (num == "9")
            {
                if (type)
                {
                    return "есөн";
                }
                else
                {
                    return "ерэн";
                }
            }
            return "";
        }

        public static void rekursiv(string num)
        {
            if (num.Length > 0)
            {
                if (num.Length == 1)
                {
                    if (num != "0")
                    {
                        if (num.Equals("1") && ((amount.ToString().Length != 1 && amount.ToString().Length < 4) || (amount.ToString().Length >= 5 && Convert.ToDecimal(amount.ToString().Substring(amount.ToString().Length - 3, 2)) != 0)))
                        {
                            numName += " нэгэн";
                        }
                        else
                        {
                            numName += " " + numNames(num, true);
                        }
                    }
                }
                else if (num.Length == 2)
                {
                    if (num.Substring(0, 1) != "0")
                    {
                        numName += " " + numNames(num.Substring(0, 1), false);
                    }
                    rekursiv(num.Substring(1));
                }
                else if (num.Length == 3)
                {
                    if (num.Substring(0, 1) != "0")
                    {
                        numName += " " + numNames(num.Substring(0, 1), true) + " зуун";
                    }
                    rekursiv(num.Substring(1));
                }
                else if (num.Length == 4)
                {
                    if (num.Substring(0, 1) != "0")
                    {
                        if (num.Substring(0, 1) == "1" && numName != "")
                        {
                            numName += " нэгэн";
                        }
                        else
                        {
                            numName += " " + numNames(num.Substring(0, 1), true);
                        }
                    }
                    if (Convert.ToInt32(num.Substring(1)) == 0)
                    {
                        numName += " мянган";
                        return;
                    }
                    else
                    {
                        numName += " мянга";
                    }
                    if (amount.ToString().Length >= 9 && Convert.ToDecimal(amount.ToString().Substring(amount.ToString().Length - 6, 3)) == 0)
                    {
                        numName = numName.Replace(" мянга", "");
                    }
                    rekursiv(num.Substring(1));
                }
                else if (num.Length == 5)
                {
                    if (num.Substring(0, 1) != "0")
                    {
                        numName += " " + numNames(num.Substring(0, 1), false);
                    }
                    rekursiv(num.Substring(1));
                }
                else if (num.Length == 6)
                {
                    if (num.Substring(0, 1) != "0")
                    {
                        numName += " " + numNames(num.Substring(0, 1), true) + " зуун";
                    }
                    rekursiv(num.Substring(1));
                }
                else if (num.Length == 7)
                {
                    if (num.Substring(0, 1) != "0")
                    {
                        if (num.Substring(0, 1) == "1" && numName != "")
                        {
                            numName += " нэгэн";
                        }
                        else
                        {
                            numName += " " + numNames(num.Substring(0, 1), true);
                        }
                    }
                    numName += " сая";
                    if (amount.ToString().Length >= 9 && Convert.ToDecimal(amount.ToString().Substring(amount.ToString().Length - 9, 3)) == 0)
                    {
                        numName = numName.Replace(" сая", "");
                    }
                    if (Convert.ToInt32(num.Substring(1)) == 0)
                    {
                        return;
                    }
                    rekursiv(num.Substring(1));
                }
                else if (num.Length == 8)
                {
                    if (num.Substring(0, 1) != "0")
                    {
                        numName += " " + numNames(num.Substring(0, 1), false);
                    }
                    rekursiv(num.Substring(1));
                }
                else if (num.Length == 9)
                {
                    if (num.Substring(0, 1) != "0")
                    {
                        numName += " " + numNames(num.Substring(0, 1), true) + " зуун";
                    }
                    rekursiv(num.Substring(1));
                }
                else if (num.Length == 10)
                {
                    if (num.Substring(0, 1) != "0")
                    {
                        if (num.Substring(0, 1) == "1" && numName != "")
                        {
                            numName += " нэгэн";
                        }
                        else
                        {
                            numName += " " + numNames(num.Substring(0, 1), true);
                        }
                    }
                    numName += " тэрбум";
                    if (amount.ToString().Length > 12 && Convert.ToDecimal(amount.ToString().Substring(amount.ToString().Length - 12, 3)) == 0)
                    {
                        numName = numName.Replace(" тэрбум", "");
                    }
                    if (Convert.ToInt32(num.Substring(1)) == 0)
                    {
                        return;
                    }
                    rekursiv(num.Substring(1));
                }
                else if (num.Length == 11)
                {
                    if (num.Substring(0, 1) != "0")
                    {
                        numName += " " + numNames(num.Substring(0, 1), false);
                    }
                    rekursiv(num.Substring(1));
                }
                else if (num.Length == 12)
                {
                    if (num.Substring(0, 1) != "0")
                    {
                        numName += " " + numNames(num.Substring(0, 1), true) + " зуун";
                    }
                    rekursiv(num.Substring(1));
                }
                else if (num.Length == 13)
                {
                    numName += numNames(num.Substring(0, 1), true) + " триллион";
                    if (Convert.ToInt32(num.Substring(1)) == 0)
                    {
                        return;
                    }
                    rekursiv(num.Substring(1));
                }
            }
        }

    }
}