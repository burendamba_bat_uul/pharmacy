﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using DevExpress.Utils;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraTreeList;
using Pharmacy2016.gui.core.info.ds;
using Pharmacy2016.gui.core.journal.ds;
using Pharmacy2016.gui.core.store.ds;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.gui.report.ds;
using Pharmacy2016.gui.core.journal.plan;
using System.Data.SqlClient;
using Pharmacy2016.gui.core.info.conf;



namespace Pharmacy2016.util
{
    /*
     * Програмын ерөнхий мэдээлэл болон нэвтрэх, гарах, ачааллах функц, ерөнхий ашиглагдах функцуудыг агуулсан класс
     * 
     */

    public sealed class ProgramUtil
    {

        #region Програмын хувилбар, огноо

        public static string ProgramUpdatedVersion = "1.0.0.0";

        public static string ProgramUpdatedDate = "2016-03-01";

        #endregion

        #region Товчны утгууд

        public static bool IsShowPopup = true;
        public static bool IsSaveEvent = true;
        public static readonly Keys KeyMoveNextControl = Keys.Enter;

        #endregion

        #region Анхааруулгын цонх

        private static DevExpress.XtraBars.Alerter.AlertControl alertControlPharmacy2016;

        public static readonly string ALERT_DIALOG_TEXT_PREFIX = "- <i>";
        public static readonly string ALERT_DIALOG_TEXT_SUBFIX = "</i>";
        public static readonly string ALERT_DIALOG_CAPTION_PREFIX = "<b>";
        public static readonly string ALERT_DIALOG_CAPTION_SUBFIX = "</b>";
        public static readonly string ALERT_DIALOG_LINE = "<br>";

        #endregion


        #region Программ ачааллаж эхлэх функц

        public static void initSkin()
        {
            BonusSkins.Register();
            //OfficeSkins.Register();

            SkinManager.EnableFormSkins();
            SkinManager.EnableMdiFormSkins();
            SkinManager.EnableFormSkinsIfNotVista();

            Application.EnableVisualStyles();

            saveDialog.Filter = "Excel (2013) (.xlsx)|*.xlsx|Excel (2003)(.xls)|*.xls|RichText File (.rtf)|*.rtf|Pdf File (.pdf)|*.pdf |Html File (.html)|*.html";
            //saveDialog.FilterIndex = 1;
            saveDialog.DefaultExt = ".xlsx";
        }

        public static void initDataSet()
        {
            InformationDataSet.initialize();
            UserDataSet.initialize();
            JournalDataSet.initialize();
            StoreDataSet.initialize();
            ReportDataSet.initialize();
            PlanDataSet.initialize();
        }

        public static void changeConnection()
        {
            InformationDataSet.changeConnectionString();
            UserDataSet.changeConnectionString();
            JournalDataSet.changeConnectionString();
            StoreDataSet.changeConnectionString();
            ReportDataSet.changeConnectionString();
            PlanDataSet.changeConnectionString();
        }

        public static void initControl()
        {

            alertControlPharmacy2016 = new DevExpress.XtraBars.Alerter.AlertControl();
            alertControlPharmacy2016.AutoFormDelay = 5000;
            alertControlPharmacy2016.AllowHtmlText = true;
            alertControlPharmacy2016.ShowPinButton = false;
            alertControlPharmacy2016.AutoHeight = true;
            alertControlPharmacy2016.FormShowingEffect = DevExpress.XtraBars.Alerter.AlertFormShowingEffect.SlideHorizontal;
            // alertControlPharmacy2016.FormLocation = new Point(100,100);
        }

        public static void loadHospital()
        {
            try
            {
                InformationDataSet.CurrHospitalTableAdapter.Fill(InformationDataSet.Instance.CurrHospital);
                if (InformationDataSet.Instance.CurrHospital.Rows.Count == 0)
                {
                    XtraMessageBox.Show("Эмнэлэгийн мэдээллийг буруу тохируулсан байна. Өгөгдлийн санг шалгана уу.");
                    EXIT(true);
                }
                else
                {
                    HospitalUtil.setHospital(InformationDataSet.Instance.CurrHospital.Rows[0]);
                    InformationDataSet.MainConfTableAdapter.Fill(InformationDataSet.Instance.MainConf);
                    RoleUtil.setMainConf(InformationDataSet.Instance.MainConf.Rows[0]);
                    InformationDataSet.RoleTableAdapter.Fill(InformationDataSet.Instance.Role);
                    setRoleOther();
                    RoleUtil.setRole(InformationDataSet.Instance.Role.Rows);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Өгөгдлийн сантай холбогдоход алдаа гарлаа.\n" + ex.ToString());
            }
        }

        // Хэрэглэгчийн эрхийг тодорхойлох функц
        public static void setRoleOther()
        {
            InformationDataSet.Instance.RoleOther.Clear();
            DataRow nrow = InformationDataSet.Instance.RoleOther.NewRow();
            nrow["id"] = -1;
            nrow["name"] = "Бүгд";
            nrow["isDefault"] = true;
            nrow["hospitalID"] = HospitalUtil.getHospitalId();
            nrow.EndEdit();
            InformationDataSet.Instance.RoleOther.Rows.Add(nrow);

            foreach (DataRow row in InformationDataSet.Instance.Role.Rows)
            {
                nrow = InformationDataSet.Instance.RoleOther.NewRow();
                nrow["id"] = row["id"];
                nrow["name"] = row["name"];
                nrow["isDefault"] = row["isDefault"];
                nrow["hospitalID"] = row["hospitalID"];
                nrow.EndEdit();
                InformationDataSet.Instance.RoleOther.Rows.Add(nrow);
            }
        }

        #endregion

        #region Програмд нэвтрэх, гарах функц

        private static bool isLogin = false;

        // Програмд нэвтрэх функц
        public static void LOGIN(string id)
        {
            try
            {
                InformationDataSet.LoginUserInfoTableAdapter.Fill(InformationDataSet.Instance.LoginUserInfo, HospitalUtil.getHospitalId(), id);
                UserUtil.setUser(id, InformationDataSet.Instance.LoginUserInfo.Rows[0]);
                InformationDataSet.UserWindowRoleTableAdapter.Fill(InformationDataSet.Instance.UserWindowRole, UserUtil.getUserRoleId(), HospitalUtil.getHospitalId());
                UserUtil.setUserRole(InformationDataSet.Instance.UserWindowRole.Rows);
                UserDataSet.UserSkinColorTableAdapter.Fill(UserDataSet.Instance.UserSkinColor, HospitalUtil.getHospitalId(), id);
                UserUtil.setUserSkinColor(UserDataSet.Instance.UserSkinColor.Rows);
                UserDataSet.UserLayoutTableAdapter.Fill(UserDataSet.Instance.UserLayout, HospitalUtil.getHospitalId(), id);
                UserUtil.setUserLayout(UserDataSet.Instance.UserLayout.Rows);
                InformationDataSet.UserWardTableAdapter.Fill(InformationDataSet.Instance.UserWard, HospitalUtil.getHospitalId());
                InformationDataSet.UserWarehouseTableAdapter.Fill(InformationDataSet.Instance.UserWarehouse, HospitalUtil.getHospitalId());
                UserUtil.setUserWard(InformationDataSet.Instance.UserWard.Rows, InformationDataSet.Instance.UserWarehouse.Rows);

                SYSTEM_LOG("login", "");
                isLogin = true;
                MainForm.Instance.setUserText();
                MainForm.Instance.showMenu();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Өгөгдлийн сантай холбогдоход алдаа гарлаа.\n" + ex.ToString());
            }
        }

        // Програмаас гарах функц
        public static void LOGOUT()
        {
            SYSTEM_LOG("logout", "");
            isLogin = false;
            RoleUtil.closeForm();
            UserUtil.clearUser();
            MainForm.Instance.resetUserSkin();
            MainForm.Instance.clearUserText();
            MainForm.Instance.hideMenu();
            LoginForm.Instance.showForm();
        }

        // Програмд орсон, гарсан хугацааг өгөгдлийн санд хадгалж байна.
        public static void SYSTEM_LOG(string action, string description)
        {
            //SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(Pharmacy2016.Properties.Settings.Default.conMedicine);
            //string database = builder.InitialCatalog;
            //object ret = InformationDataSet.QueriesTableAdapter.is_read_only(database);
            //if (ret != DBNull.Value && ret != null)
            //{
            //    return;
            //}

            try
            {
                InformationDataSet.QueriesTableAdapter.insert_system_log(UserUtil.getUserId(), HospitalUtil.getHospitalId(), action, description);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Өгөгдлийн сантай холбогдоход алдаа гарлаа.\n" + ex.ToString());
            }
        }

        // Програмд хүн нэвтэрч орсон эсэхийг буцаах функц
        public static bool isLoginUser()
        {
            return isLogin;
        }

        // Програмаас бүр мөсөн гарах функц
        public static void EXIT(bool isMainForm)
        {
            if (isLogin)
            {
                SYSTEM_LOG("logout", "");
                UserUtil.saveUserSkinColor();
            }

            if (isMainForm)
                MainForm.Instance.Close();
        }

        public static void EXITFULLY()
        {
            SystemUtil.IsConnect = false;
            MainForm.Instance.Close();
        }

        #endregion

        #region Ачааллах цонхны функц

        private static WaitDialogForm waitDialog;

        public static readonly string RELOAD_TITLE = "Ачааллаж байна";

        public static readonly string UPDATE_TITLE = "Өгөгдөл хадгалж байна";

        public static readonly string EXPORT_TITLE = "Хөрвүүлж байна";

        public static readonly string PRINT_TITLE = "Хэвлэж байна";

        public static readonly string BACKUP_DB_TITLE = "Өгөгдлийн санг архивлаж байна";

        public static readonly string CREATE_DB_TITLE = "Өгөгдлийн санг үүсгэж байна";

        public static readonly string DELETE_DB_TITLE = "Өгөгдлийн санг устгаж байна";

        public static void showWaitDialog(string title)
        {
            // Хэрвээ ачаалах цонх харагдаж байсан бол устгана.
            if (waitDialog != null)
            {
                waitDialog.Close();
                waitDialog.Dispose();
                waitDialog = null;
            }
            // Ачаалах цонхыг харуулж байна.
            waitDialog = new WaitDialogForm("Та түр хүлээнэ үү...", title, new Size(200, 60));

            // Үндсэн цонхыг идэвхгүй төлөвт оруулж байна.
            setEnabledMainFormSafety(MainForm.Instance, false);
        }

        // Үндсэн цонхыг идэвхгүй төлөвт оруулах  функц.
        private delegate void setEnabledMainFormSafetyCallBack(XtraForm mainForm, bool isEnabled);

        private static void setEnabledMainFormSafety(XtraForm mainForm, bool isEnabled)
        {
            if (mainForm.InvokeRequired)
            {
                setEnabledMainFormSafetyCallBack delegateObj = new setEnabledMainFormSafetyCallBack(setEnabledMainFormSafety);
                mainForm.Invoke(delegateObj, new object[] { isEnabled });
            }
            else
            {
                mainForm.Enabled = isEnabled;
            }
        }

        public static void closeWaitDialog()
        {
            if (waitDialog != null)
            {
                // Үндсэн цонхыг идэвхтэй төлөвт оруулж, сонгож байна.
                setEnabledMainFormSafety(MainForm.Instance, true);

                // Ачаалах цонхыг устгаж байна.
                waitDialog.Close();
                waitDialog.Dispose();
                waitDialog = null;
            }
        }

        #endregion

        #region Зурагтай ажиллах функц

        public static Image byteArrayToImage(object byteArrayIn)
        {
            if (byteArrayIn != DBNull.Value && byteArrayIn != null)
            {
                MemoryStream ms = new MemoryStream((Byte[])byteArrayIn);
                return Image.FromStream(ms);
            }

            return global::Pharmacy2016.Properties.Resources.mainSymbol1;
        }

        public static byte[] imageToByteArray(Image image, System.Drawing.Imaging.ImageFormat imageFormat)
        {
            MemoryStream ms = new MemoryStream();

            image.Save(ms, imageFormat);

            return ms.ToArray();
        }

        public static Image resizeImage(Image image, Size size)
        {
            Bitmap newImage = new Bitmap(size.Width, size.Height);
            Graphics g = Graphics.FromImage((Image)newImage);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(image, 0, 0, size.Width, size.Height);
            g.Dispose();
            return (Image)newImage;
        }

        public static Image cropImage(Image image, Rectangle area)
        {
            Bitmap bmpImage = new Bitmap(image);
            Bitmap bmpCrop = bmpImage.Clone(area, bmpImage.PixelFormat);
            return (Image)(bmpCrop);
        }

        #endregion

        #region Grid, TreeList-г хөрвүүлэх функц

        private static SaveFileDialog saveDialog = new SaveFileDialog();

        public static void convertGrid(BandedGridView gridView, string fileNamePostfix)
        {
            try
            {
                saveDialog.FileName = String.Format("{0} {1:yyyy-MM-dd-HH-mm-ss}", fileNamePostfix, DateTime.Now);
                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    showWaitDialog(EXPORT_TITLE);
                    string exportFilePath = saveDialog.FileName;
                    string fileExtenstion = new FileInfo(exportFilePath).Extension;

                    switch (fileExtenstion)
                    {
                        case ".xls":
                            gridView.ExportToXls(exportFilePath);
                            break;
                        case ".xlsx":
                            gridView.ExportToXlsx(exportFilePath);
                            break;
                        case ".rtf":
                            gridView.ExportToRtf(exportFilePath);
                            break;
                        case ".pdf":
                            gridView.ExportToPdf(exportFilePath);
                            break;
                        case ".html":
                            gridView.ExportToHtml(exportFilePath);
                            break;
                        case ".mht":
                            gridView.ExportToMht(exportFilePath);
                            break;
                        default:
                            break;
                    }
                    closeWaitDialog();
                }
                saveDialog.FileName = "";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Өгөгдөл хөрвүүлэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                closeWaitDialog();
            }
        }

        public static void convertGrid(PivotGridControl gridView, string fileNamePostfix)
        {
            try
            {
                saveDialog.FileName = String.Format("{0} {1:yyyy-MM-dd HH mm ss}", fileNamePostfix, DateTime.Now);
                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    showWaitDialog(EXPORT_TITLE);
                    string exportFilePath = saveDialog.FileName;
                    string fileExtenstion = new FileInfo(exportFilePath).Extension;

                    switch (fileExtenstion)
                    {
                        case ".xls":
                            gridView.ExportToXls(exportFilePath);
                            break;
                        case ".xlsx":
                            gridView.ExportToXlsx(exportFilePath);
                            break;
                        case ".rtf":
                            gridView.ExportToRtf(exportFilePath);
                            break;
                        case ".pdf":
                            gridView.ExportToPdf(exportFilePath);
                            break;
                        case ".html":
                            gridView.ExportToHtml(exportFilePath);
                            break;
                        case ".mht":
                            gridView.ExportToMht(exportFilePath);
                            break;
                        default:
                            break;
                    }
                    closeWaitDialog();
                }
                saveDialog.FileName = "";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Өгөгдөл хөрвүүлэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                closeWaitDialog();
            }
        }

        public static void convertGrid(GridView gridView, string fileNamePostfix)
        {
            try
            {
                saveDialog.FileName = String.Format("{0} {1:yyyy-MM-dd HH mm ss}", fileNamePostfix, DateTime.Now);
                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    showWaitDialog(EXPORT_TITLE);
                    string exportFilePath = saveDialog.FileName;
                    string fileExtenstion = new FileInfo(exportFilePath).Extension;

                    switch (fileExtenstion)
                    {
                        case ".xls":
                            gridView.ExportToXls(exportFilePath);
                            break;
                        case ".xlsx":
                            gridView.ExportToXlsx(exportFilePath);
                            break;
                        case ".rtf":
                            gridView.ExportToRtf(exportFilePath);
                            break;
                        case ".pdf":
                            gridView.ExportToPdf(exportFilePath);
                            break;
                        case ".html":
                            gridView.ExportToHtml(exportFilePath);
                            break;
                        case ".mht":
                            gridView.ExportToMht(exportFilePath);
                            break;
                        default:
                            break;
                    }
                    closeWaitDialog();
                }
                saveDialog.FileName = "";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Өгөгдөл хөрвүүлэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                closeWaitDialog();
            }
        }

        public static void convertGrid(TreeList treeList, string fileNamePostfix)
        {
            try
            {
                saveDialog.FileName = String.Format("{0} {1:yyyy-MM-dd HH mm ss}", fileNamePostfix, DateTime.Now);
                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    showWaitDialog(EXPORT_TITLE);
                    string exportFilePath = saveDialog.FileName;
                    string fileExtenstion = new FileInfo(exportFilePath).Extension;

                    switch (fileExtenstion)
                    {
                        case ".xls":
                            treeList.ExportToXls(exportFilePath);
                            break;
                        case ".xlsx":
                            treeList.ExportToXlsx(exportFilePath);
                            break;
                        case ".rtf":
                            treeList.ExportToRtf(exportFilePath);
                            break;
                        case ".pdf":
                            treeList.ExportToPdf(exportFilePath);
                            break;
                        case ".html":
                            treeList.ExportToHtml(exportFilePath);
                            break;
                        //case ".mht":
                        //    treeList.ExportToMht(exportFilePath, "");
                        //    break;
                        default:
                            break;
                    }
                    closeWaitDialog();
                }
                saveDialog.FileName = "";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Өгөгдөл хөрвүүлэхэд алдаа гарлаа " + ex.Message);
            }
            finally
            {
                closeWaitDialog();
            }
        }

        #endregion

        #region Alert dialog-тай ажиллах функц


        public static void showAlertDialog(XtraForm form, string caption, string text)
        {
            alertControlPharmacy2016.Show(form, ALERT_DIALOG_CAPTION_PREFIX + caption + ALERT_DIALOG_CAPTION_SUBFIX, text);
        }

        public static void closeAlertDialog()
        {
            foreach (DevExpress.XtraBars.Alerter.AlertForm form in alertControlPharmacy2016.AlertFormList)
            {
                form.Close();
            }
        }

        #endregion

        #region Гүйлгээ түгжсэн эсэхийг шалган функц

        public static bool checkLockMedicine(string date)
        {
            bool isRight = true;
            SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
            myConn.Open();
            string query = "Select * from [lockMedicine] where '" + date + "' between startDate and endDate and hospitalID = '" + HospitalUtil.getHospitalId() + "'";
            SqlCommand oCmd = new SqlCommand(query, myConn);
            using (SqlDataReader oReader = oCmd.ExecuteReader())
            {
                while (oReader.Read())
                {
                    if (oReader.HasRows)
                    {
                        isRight = false;
                    }
                }
            }
            myConn.Close();
            return isRight;
        }

        public static bool checkLockMedicineBegBal()
        {
            bool isRight = true;
            SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
            myConn.Open();
            string query = "Select * from [lockMedicine] where hospitalID = '" + HospitalUtil.getHospitalId() + "'";
            SqlCommand oCmd = new SqlCommand(query, myConn);
            using (SqlDataReader oReader = oCmd.ExecuteReader())
            {
                while (oReader.Read())
                {
                    if (oReader.HasRows)
                    {
                        isRight = false;
                    }
                }
            }
            myConn.Close();
            return isRight;
        }

        #endregion

        #region Эм бүртгэлтэй байгаа эсэхийг шалгах функц

        public static bool checkHaveMedicine(string medicineID, string price, string validDate, string serial)
        {
            bool isHave = false;
            SqlConnection myConn = new SqlConnection(Pharmacy2016.Properties.Settings.Default.conMedicine);
            myConn.Open();
            string query = "Select * from [Medicine] where [price] = '"+price+"' and [validDate] = '"+validDate+"' and serial = '"+serial+"' and id = '"+medicineID+"' and hospitalID = '" + HospitalUtil.getHospitalId() + "'";
            SqlCommand oCmd = new SqlCommand(query, myConn);
            using (SqlDataReader oReader = oCmd.ExecuteReader())
            {
                while (oReader.Read())
                {
                    if (oReader.HasRows)
                    {
                        isHave = true;
                    }
                }
            }
            myConn.Close();
            return isHave;
        }

        #endregion

    }
}
