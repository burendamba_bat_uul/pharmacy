﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraNavBar;

namespace Pharmacy2016.util.locale
{
    class NavBarLocalizerMongolian : NavBarLocalizer
    {
        public override string Language { get { return "Mongolian"; } }
        public override string GetLocalizedString( NavBarStringId id)
        {
            string ret = "";
            switch (id)
            {

                case NavBarStringId.NavPaneMenuShowMoreButtons: return "Дэлгэрэнгүй товчнуудыг харуулах"; // "Show More Buttons"
                case NavBarStringId.NavPaneMenuShowFewerButtons: return "Хураангуй товчнуудыг харуулах"; // "Show Fewer Buttons"
                case NavBarStringId.NavPaneMenuAddRemoveButtons: return "Товчнууд нэмэх эсвэл устгах"; // "Add or Remove Buttons"
                case NavBarStringId.NavPaneChevronHint: return "Тохиргооны товчнууд"; // "Configure buttons"
                //case NavBarStringId.navPane

                default:
                    ret = "";
                    break;
            }
            return ret;
        }
    }
}
