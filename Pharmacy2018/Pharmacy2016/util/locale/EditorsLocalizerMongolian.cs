﻿using System;
using DevExpress.XtraEditors.Controls;

namespace Pharmacy2016.util.locale
{
    class EditorsLocalizerMongolian : Localizer
    {
        public override string Language { get { return "Mongolian"; } }
        public override string GetLocalizedString(StringId id)
        {
            switch (id)
            {
                // PictureEdit-ийн текстүүд
                case StringId.DataEmpty: return "Зураггүй"; // "Data Empty"
                case StringId.PictureEditMenuCut: return "Зөөх"; // "Cut"
                case StringId.PictureEditMenuCopy: return "Хуулах"; // "Copy"
                case StringId.PictureEditMenuPaste: return "Хуулж тавих"; // "Paste"
                case StringId.PictureEditMenuDelete: return "Устгах"; // "Delete"
                case StringId.PictureEditMenuLoad: return "Ачаалах"; // "Load"
                case StringId.PictureEditMenuSave: return "Хадгалах"; // "Save"
                case StringId.PictureEditCopyImageError: return "Зураг хуулахад алдаа гарлаа"; // "Could not copy image"

                // ColorEdit-ийн текстүүд
                case StringId.ColorTabCustom: return "Энгийн"; // "Custom"
                case StringId.ColorTabSystem: return "Систем"; // "System"
                case StringId.ColorTabWeb: return "Вэб"; // "Web"
                
                // Navigator-ийн текстүүд
                case StringId.NavigatorTextStringFormat: return "Нийт {1} мөрөөс {0}-р мөр"; // "Record {0} of {1}"
                case StringId.NavigatorAppendButtonHint: return "Нэмэх (Ctrl+E)"; // "Append"
                case StringId.NavigatorCancelEditButtonHint: return "Болих"; // "Cancel Edit"
                case StringId.NavigatorEditButtonHint: return "Засах"; // "Edit"
                case StringId.NavigatorEndEditButtonHint: return "Засаж дуусах"; // "End Edit"
                case StringId.NavigatorFirstButtonHint: return "Хүснэгтийн эхэнд (Ctrl+Home)"; // "First"
                case StringId.NavigatorLastButtonHint: return "Хүснэгтийн төгсгөлд (Ctrl+End)"; // "Last"
                case StringId.NavigatorPreviousPageButtonHint: return "Хуудасны эхэнд (PageUp)"; // "Previous Page"
                case StringId.NavigatorNextPageButtonHint: return "Хуудасны төгсгөлд (PageDown)"; // "Next Page"
                case StringId.NavigatorPreviousButtonHint: return "Өмнөх мөр (Up)"; // "Previous"
                case StringId.NavigatorNextButtonHint: return "Дараах мөр (Down)"; // "Last"  
                case StringId.NavigatorRemoveButtonHint: return "Устгах"; // "Delete"

                // Бусад текстүүд
                case StringId.OK: return "Тийм"; // "Ok"
                case StringId.Cancel: return "Гарах"; // "Cancel"
                case StringId.Apply: return "Хэрэглэх"; // "Apply"
                case StringId.ImagePopupEmpty: return "(Хоосон)"; // "(Empty)"
                case StringId.ImagePopupPicture: return "(Зураг)"; // "(Picture)"
                case StringId.MaskBoxValidateError: return "Таны оруулсан утга алдаатай байна. Та энэ алдааг засах уу. \r\n\r\nТийм - утгыг засаж засварлагчруу буцах.\r\nҮгүй - утгыг хэвээр нь үлдээх.\r\nБолих - өмнөх утгыг сэргээх."; // "The entered value is incomplete. Do you want to correct it?\r\n\r\nYes - return to the editor and correct the value.\r\nNo - leave the value as is.\r\nCancel - reset to the previous value."

                case StringId.CaptionError: return "Алдаа"; // "Error"
                case StringId.CheckChecked: return "Сонгогдсон"; // "Checked"
                case StringId.CheckIndeterminate: return "Тодорхойгүй"; // "Indeterminate"
                case StringId.CheckUnchecked: return "Сонгогдоогүй"; // "Unchecked"
                case StringId.ContainerAccessibleEditName: return "AccessibleEditName"; // DevExpress.XtraEditors.BaseEdit.Acce
                case StringId.InvalidValueText: return "Буруу утга"; // "Invalid Value"
                case StringId.LookUpColumnDefaultName: return "Нэр"; // "Name"
                case StringId.LookUpEditValueIsNull: return ""; // ""
                case StringId.LookUpInvalidEditValueType: return "Төрөл буруу";
                case StringId.None: return ""; // "" (empty string)
                case StringId.NotValidArrayLength: return "Урт нь буруу"; // "Not valid array length"
                case StringId.PreviewPanelText: return "Хэвлэхийн өмнө урьдчилан харах:";  // "Preview:"
                case StringId.TabHeaderButtonClose: return "Хаах"; // "Close"
                case StringId.TabHeaderButtonNext: return "Дараах"; // "Next"
                case StringId.TabHeaderButtonPrev: return "Өмнөх"; // "Previous"
                case StringId.TransparentBackColorNotSupported: return "Энэ control нь нэвтрэлттэй дэвсгэр өнгө дэмждэггүй"; // "This control does not support transparent background colors"
                case StringId.UnknownPictureFormat: return "Тодорхойлогдоогүй зургийн төрөл"; // "Unknown picture format"

                // TextEdit-ийн текстүүд
                case StringId.TextEditMenuCopy: return "Хуулах"; // "Copy"
                case StringId.TextEditMenuCut: return "Зөөх"; // "Cut"
                case StringId.TextEditMenuDelete: return "Устгах"; // "Delete"
                case StringId.TextEditMenuPaste: return "Хуулж тавих"; // "Paste"
                case StringId.TextEditMenuSelectAll: return "Бүгдийг сонгох"; // "Select All"
                case StringId.TextEditMenuUndo: return "Буцаах"; // "Undo"

                // XtraMessageBox-ийн текстүүд
                case StringId.XtraMessageBoxAbortButtonText: return "Цуцлах"; // "Abort"
                case StringId.XtraMessageBoxCancelButtonText: return "Болих"; // "Cancel"
                case StringId.XtraMessageBoxIgnoreButtonText: return "Хэрэгсэхгүй"; // "Ignore"
                case StringId.XtraMessageBoxNoButtonText: return "Үгүй"; // "No"
                case StringId.XtraMessageBoxOkButtonText: return "Ok"; // "Ok"
                case StringId.XtraMessageBoxRetryButtonText: return "Дахин"; // "Retry"
                case StringId.XtraMessageBoxYesButtonText: return "Тийм"; // "Yes"

                // DateEdit-ийн текстүүд
                case StringId.DateEditToday: return "Өнөөдөр"; // "Today"
                case StringId.DateEditClear: return "Цэвэрлэх"; // "Clear"

                // CalcEdit-ийн текстүүд
                case StringId.CalcButtonBack: return "Буц"; // "Back"
                case StringId.CalcButtonC: return "C";   // "C"
                case StringId.CalcButtonCE: return "CE"; // "CE"
                case StringId.CalcButtonMC: return "MC"; // "MC"
                case StringId.CalcButtonMR: return "MR"; // "MR"
                case StringId.CalcButtonMS: return "MS"; // "MS"
                case StringId.CalcButtonMx: return "M+"; // "M+"
                case StringId.CalcButtonSqrt: return "sqrt"; // "sqrt"
                case StringId.CalcError: return "Тооцоололтын алдаа"; // "Calculation Error"

                // Шүүлтүүрийн текстүүд
                case StringId.FilterClauseAnyOf: return "Аль нэг нь"; // "Is any of"
                case StringId.FilterClauseBeginsWith: return "Эхэндээ"; // "Begins with"
                case StringId.FilterClauseBetween: return "Хооронд"; // "Is between"
                case StringId.FilterClauseBetweenAnd: return "ба"; // "and"
                case StringId.FilterClauseContains: return "Агуулсан"; // "Contains"
                case StringId.FilterClauseDoesNotContain: return "Агуулаагүй"; // "Does not contain"
                case StringId.FilterClauseDoesNotEqual: return "Тэнцүү биш"; // "Does not equal"
                case StringId.FilterClauseEndsWith: return "Төгсгөлдөө"; // "Ends with"
                case StringId.FilterClauseEquals: return "Тэнцүү"; // "Equals"
                case StringId.FilterClauseGreater: return "Эрс их"; // "Is greater than"
                case StringId.FilterClauseGreaterOrEqual: return "Их буюу тэнцүү"; // "Is greater than or equal to"
                case StringId.FilterClauseIsNotNull: return "Хоосон биш"; // "Is not blank"
                case StringId.FilterClauseIsNull: return "Хоосон"; // "Is blank"
                case StringId.FilterClauseLess: return "Эрс бага"; // "Is less than"
                case StringId.FilterClauseLessOrEqual: return "Бага буюу тэнцүү"; // "Is less than or equal to"
                case StringId.FilterClauseLike: return "Ижилхэн"; // "Is Like"
                case StringId.FilterClauseNoneOf: return "Аль нь ч биш"; // "Is none of"
                case StringId.FilterClauseNotBetween: return "Хооронд биш"; // "Is not between"
                case StringId.FilterClauseNotLike: return "Ижилхэн биш"; // "Is not like"


                case StringId.FilterEmptyEnter: return "(Утга оруулна уу)"; // "enter a value"
                case StringId.FilterEmptyValue: return "(Хоосон утга)"; // "empty"
                case StringId.FilterGroupAnd: return "Ба"; // "And"
                case StringId.FilterGroupNotAnd: return "Ба биш"; // "Not And"
                case StringId.FilterGroupNotOr: return "Эсвэл биш"; // "Not Or"
                case StringId.FilterGroupOr: return "Эсвэл"; // "Or"
                case StringId.FilterMenuClearAll: return "Бүгдийг цэвэрлэх"; // "Clear All"
                case StringId.FilterMenuConditionAdd: return "Бүлэгрүү шинэ нөхцөл нэмэх"; // "Add Condition"
                case StringId.FilterMenuGroupAdd: return "Бүлэг нэмэх"; // "Add Group"
                case StringId.FilterMenuRowRemove: return "Бүлэг устгах"; // "Remove Group"
                case StringId.FilterOutlookDateText: return "Бүгдийг харуулах|Хоосон|Тодорхойлсон огноогоор шүүх:|Дараа жил|Энэ жилийн сүүлийн хагас|Энэ сарын сүүлийн хагас|Дараа долоо хоног|Энэ долоо хоногийн сүүлийн хагас|Маргааш|Өнөөдөр|Өчигдөр|Энэ долоо хоногийн эхний хагас|Сүүлийн долоо хоног|Энэ сарын эхний хагас|Энэ жилийн эхний хагас|Өмнөх жил"; // "Show all        |Show Empty                  |Filter by specific date:    | Beyond this year       |Later this year         |Later this month       |Next week        |Later this week                 |Tomorrow        | Today    |Yesterday  |Earlier this week             |Last week          |Earlier this month   |Earlier this year     |Prior to this year"
                case StringId.FilterShowAll: return "(Бүгдийг харуулах)"; // "(Show All)"
                case StringId.FilterToolTipElementAdd: return "Жагсаалтанд шинэ элемент нэмэх."; // "Adds a new item to the list"
                case StringId.FilterToolTipKeysAdd: return "(Гараас '+' эсвэл Insert товч хэрэглэнэ)"; // "(Use the Insert or Add button on the keyboard)"
                case StringId.FilterToolTipKeysRemove: return "(Гараас '-' эсвэл Delete товч хэрэглэнэ)"; // "(Use the Delete or Subtract on the keyboard)"
                case StringId.FilterToolTipNodeAction: return "Үйлдлүүд."; // "Actions"
                case StringId.FilterToolTipNodeAdd: return "Бүлэгрүү шинэ нөхцөл нэмэх."; // "Adds a new condition to this group"
                case StringId.FilterToolTipNodeRemove: return "Нөхцлийг устгах."; // "Removes this condition"
                case StringId.FilterToolTipValueType: return "Утгыг харьцуулах / Бусад талбарын утга."; // "Compare to a value / other field's value."

                case StringId.FilterCriteriaToStringBetween: return "хооронд"; // "Between"
                case StringId.FilterCriteriaToStringBinaryOperatorBitwiseAnd: return "@";  // "@"
                case StringId.FilterCriteriaToStringBinaryOperatorBitwiseOr: return "|"; // "|"
                case StringId.FilterCriteriaToStringBinaryOperatorBitwiseXor: return "^"; // "^"
                case StringId.FilterCriteriaToStringBinaryOperatorDivide: return "/"; // "/"
                case StringId.FilterCriteriaToStringBinaryOperatorEqual: return "="; // "="
                case StringId.FilterCriteriaToStringBinaryOperatorGreater: return ">"; // "@gt;" >
                case StringId.FilterCriteriaToStringBinaryOperatorGreaterOrEqual: return ">="; // "@gt;" >=
                case StringId.FilterCriteriaToStringBinaryOperatorLess: return "<"; // "@lt;" <
                case StringId.FilterCriteriaToStringBinaryOperatorLessOrEqual: return "<="; // "@lt;=" <=
                case StringId.FilterCriteriaToStringBinaryOperatorLike: return "агуулсан"; // "Like"
                case StringId.FilterCriteriaToStringBinaryOperatorMinus: return "-"; // "-"
                case StringId.FilterCriteriaToStringBinaryOperatorModulo: return "%"; // "%"
                case StringId.FilterCriteriaToStringBinaryOperatorMultiply: return "*"; // "*"
                case StringId.FilterCriteriaToStringBinaryOperatorNotEqual: return "<>"; // "@lt;@gt;" <>
                case StringId.FilterCriteriaToStringBinaryOperatorPlus: return "+"; // "+"
                case StringId.FilterCriteriaToStringFunctionCustom: return "дурын"; // "Custom"
                case StringId.FilterCriteriaToStringFunctionIif: return "Iif"; // "Iif"
                case StringId.FilterCriteriaToStringFunctionIsNull: return "IsNull"; // "IsNull"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalBeyondThisYear: return "Дараа жил"; // "BeyondThisYear"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalEarlierThisMonth: return "Энэ сарын эхний хагас"; // "EarlierThisMonth"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalEarlierThisWeek: return "Энэ долоо хоногийн эхний хагас"; // "EarlierThisWeek"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalEarlierThisYear: return "Энэ жилийн эхний хагас"; // "EarlierThisYear"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalLastWeek: return "Өнгөрсөн долоо хоног"; // "LastWeek"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalLaterThisMonth: return "Энэ сарын сүүлийн хагас"; // "LaterThisMonth"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalLaterThisWeek: return "Энэ долоо хоногийн сүүлийн хагас"; // "LaterThisWeek"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalLaterThisYear: return "Энэ жилийн сүүлийн хагас"; // "LaterThisYear"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalNextWeek: return "Дараа долоо хоног"; // "NextWeek"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalPriorThisYear: return "Өмнөх жил"; // "PriorThisYear"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalToday: return "Өнөөдөр"; // "Today"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalTomorrow: return "Маргааш"; // "Tomorrow"
                case StringId.FilterCriteriaToStringFunctionIsOutlookIntervalYesterday: return "Өчигдөр"; // "Yesterday"
                case StringId.FilterCriteriaToStringFunctionLen: return "Len"; // "Len"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeDayAfterTomorrow: return "Нөгөөдөр"; // "DayAfterTomorrow"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeLastWeek: return "Өнгөрсөн долоо хоног"; // "LastWeek"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeNextMonth: return "Дараа сар"; // "NextMonth"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeNextWeek: return "Дараа долоо хоног"; // "NextWeek"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeNextYear: return "Дараа жил"; // "NextYear"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeNow: return "Одоо"; // "Now"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeThisMonth: return "Энэ сар"; // "ThisMonth"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeThisWeek: return "Энэ долоо хоног"; // "ThisWeek"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeThisYear: return "Энэ жил"; // "ThisYear"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeToday: return "Өнөөдөр"; // "Today"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeTomorrow: return "Маргааш"; // "Tomorrow"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeTwoWeeksAway: return "Хоёр долоо хоногийн дараа"; // "TwoWeeksAway"
                case StringId.FilterCriteriaToStringFunctionLocalDateTimeYesterday: return "Өчигдөр"; // "Yesterday"
                case StringId.FilterCriteriaToStringFunctionLower: return "Жижиг"; // "Lower"
                case StringId.FilterCriteriaToStringFunctionNone: return "None"; // "None"
                case StringId.FilterCriteriaToStringFunctionSubstring: return "Substring"; // "Substring"
                case StringId.FilterCriteriaToStringFunctionTrim: return "Trim"; // "Trim"
                case StringId.FilterCriteriaToStringFunctionUpper: return "Upper"; // "Upper"
                case StringId.FilterCriteriaToStringGroupOperatorAnd: return "Ба"; // "And"
                case StringId.FilterCriteriaToStringGroupOperatorOr: return "Эсвэл"; // "Or"
                case StringId.FilterCriteriaToStringIn: return "Дотор"; // "In"
                case StringId.FilterCriteriaToStringIsNotNull: return "Хоосон биш"; // "Is Not Null"
                case StringId.FilterCriteriaToStringNotLike: return "Ижилхэн биш"; // "Not Like"
                case StringId.FilterCriteriaToStringUnaryOperatorBitwiseNot: return "~"; // "~"
                case StringId.FilterCriteriaToStringUnaryOperatorIsNull: return "Хоосон"; // "Is Null"
                case StringId.FilterCriteriaToStringUnaryOperatorMinus: return "-"; // "-"
                case StringId.FilterCriteriaToStringUnaryOperatorNot: return "Үгүй"; // "Not"
                case StringId.FilterCriteriaToStringUnaryOperatorPlus: return "+"; // "+"
                case StringId.FilterDateTimeConstantMenuCaption: return "Тогтмолууд"; // "DateTime constants"
                case StringId.FilterDateTimeOperatorMenuCaption: return "Операторууд"; // "DateTime operators"












/*



        None = 0, //     Return Value: "" (empty string)
        CaptionError = 1, //     Return Value: Error
        InvalidValueText = 2, //     Return Value: Invalid Value
        CheckChecked = 3,//     Return Value: Checked
        CheckUnchecked = 4,//     Return Value: Unchecked
        CheckIndeterminate = 5,//     Return Value: Indeterminate
        DateEditToday = 6,//     Return Value: Today
        DateEditClear = 7,//     Return Value: Clear
        OK = 8,//     Return Value: Ok
        Cancel = 9,//     Return Value: Cancel
        NavigatorFirstButtonHint = 10,//     Return Value: First
        NavigatorPreviousButtonHint = 11,//     Return Value: Previous
        NavigatorPreviousPageButtonHint = 12,//     Return Value: Previous Page
        NavigatorNextButtonHint = 13,//     Return Value: Last
        NavigatorNextPageButtonHint = 14,//     Return Value: Next Page
        NavigatorLastButtonHint = 15,//     Return Value: Last
        NavigatorAppendButtonHint = 16,//     Return Value: Append
        NavigatorRemoveButtonHint = 17,//     Return Value: Delete
        NavigatorEditButtonHint = 18,
        NavigatorEndEditButtonHint = 19,//     Return Value: End Edit
        NavigatorCancelEditButtonHint = 20,//     Return Value: Cancel Edit
        NavigatorTextStringFormat = 21,//     Return Value: Record {0} of {1}
        
        PictureEditMenuCut = 22,//     Return Value: Cut
        
        PictureEditMenuCopy = 23,//     Return Value: Copy
        
        PictureEditMenuPaste = 24,//     Return Value: Paste
        
        PictureEditMenuDelete = 25,//     Return Value: Delete
        
        PictureEditMenuLoad = 26,//     Return Value: Load
        
        PictureEditMenuSave = 27,//     Return Value: Save
        
        PictureEditOpenFileFilter = 28,//     Return Value: Bitmap Files (*.bmp)|*.bmp|Graphics Interchange Format (*.gif)|*.gif|JPEG
        //     File Interchange Format (*.jpg;*.jpeg)|*.jpg;*.jpeg|Icon Files (*.ico)|*.ico|All
        //     Picture Files |*.bmp;*.gif;*.jpg;*.jpeg;*.ico;*.png;*.tif|All Files |*.*
        
        PictureEditSaveFileFilter = 29,//     Return Value: Bitmap Files (*.bmp)|*.bmp|Graphics Interchange Format (*.gif)|*.gif|JPEG
        //     File Interchange Format (*.jpg)|*.jpg
        
        PictureEditOpenFileTitle = 30,//     Return Value: Open
        
        PictureEditSaveFileTitle = 31,//     Return Value: Save As
        
        PictureEditOpenFileError = 32,//     Return Value: Wrong picture format
        
        PictureEditOpenFileErrorCaption = 33,//     Return Value: Open error
        
        PictureEditCopyImageError = 34,//     Return Value: Could not copy image
        //
        // Summary:
        //     Specifies the text string displayed in the edit box of the DevExpress.XtraEditors.LookUpEdit
        //     when the edit value is null (Nothing in Visual Basic).
        LookUpEditValueIsNull = 35,
        //
        // Summary:
        //     Reserved for future use.
        LookUpInvalidEditValueType = 36,
        
        LookUpColumnDefaultName = 37,//     Return Value: Name
        
        MaskBoxValidateError = 38,//     Return Value: The entered value is incomplete. Do you want to correct it?\r\n\r\nYes
        //     - return to the editor and correct the value.\r\nNo - leave the value as
        //     is.\r\nCancel - reset to the previous value.\r\n
        
        UnknownPictureFormat = 39,//      Return Value: Unknown picture format
        
        DataEmpty = 40,//     Return Value: Data empty
        
        NotValidArrayLength = 41,//     Return Value: Not valid array length.
        
        ImagePopupEmpty = 42,//     Return Value: (Empty)
        
        ImagePopupPicture = 43,//     Return Value: (Picture)
        
        ColorTabCustom = 44,//     Return Value: Custom
        
        ColorTabWeb = 45,//     Return Value: Web
        
        ColorTabSystem = 46,//     Return Value: System
        
        CalcButtonMC = 47,//     Return Value: MC
        
        CalcButtonMR = 48,//     Return Value: MR
        
        CalcButtonMS = 49,//     Return Value: MS
        
        CalcButtonMx = 50,//     Return Value: M+
        
        CalcButtonSqrt = 51,//     Return Value: sqrt
        
        CalcButtonBack = 52,//     Return Value: Back
        
        CalcButtonCE = 53,//     Return Value: CE
        
        CalcButtonC = 54,//     Return Value: C
        
        CalcError = 55,//     Return Value: Calculation Error
        
        TabHeaderButtonPrev = 56,//     Return Value: Previous
        
        TabHeaderButtonNext = 57,//     Return Value: Next
        
        TabHeaderButtonClose = 58,//     Return Value: Close
        
        XtraMessageBoxOkButtonText = 59,//     Return Value: Ok
        
        XtraMessageBoxCancelButtonText = 60,//     Return Value: Cancel
        
        XtraMessageBoxYesButtonText = 61,//     Return Value: Yes
        
        XtraMessageBoxNoButtonText = 62,//     Return Value: No
        
        XtraMessageBoxAbortButtonText = 63,//     Return Value: Abort
        
        XtraMessageBoxRetryButtonText = 64,//     Return Value: Retry
        
        XtraMessageBoxIgnoreButtonText = 65,//     Return Value: Ignore
        
        TextEditMenuUndo = 66,//     Return Value: Undo
        
        TextEditMenuCut = 67,//     Return Value: Cut
        
        TextEditMenuCopy = 68,//     Return Value: Copy
        
        TextEditMenuPaste = 69,//     Return Value: Paste
        
        TextEditMenuDelete = 70,//     Return Value: Delete
        
        TextEditMenuSelectAll = 71,//     Return Value: Select All
        
        FilterShowAll = 72,//     Return Value: (Show All)
        
        FilterGroupAnd = 73,//     Return Value: And
        
        FilterGroupNotAnd = 74,//     Return Value: Not And
        
        FilterGroupNotOr = 75,//     Return Value: Not Or
        
        FilterGroupOr = 76,//     Return Value: Or
        
        FilterClauseAnyOf = 77,//     Return value: Is any of
        
        FilterClauseBeginsWith = 78,//     Return value: Begins with
        
        FilterClauseBetween = 79,//     Return value: Is between
        
        FilterClauseBetweenAnd = 80,//     Return value: and
        
        FilterClauseContains = 81,//     Return value: Contains
        
        FilterClauseEndsWith = 82,//     Return value: Ends with
        
        FilterClauseEquals = 83,//     Return value: Equals
        
        FilterClauseGreater = 84,//     Return value: Is greater than
        
        FilterClauseGreaterOrEqual = 85,//     Return value: Is greater than or equal to
        
        FilterClauseIsNotNull = 86,//     Return value: Is not blank
        
        FilterClauseIsNull = 87,//     Return value: Is blank
        
        FilterClauseLess = 88,//     Return value: Is less than
        
        FilterClauseLessOrEqual = 89,//     Return value: Is less than or equal to
        
        FilterClauseLike = 90,//     Return value: Is like
        
        FilterClauseNoneOf = 91,//     Return value: Is none of
        
        FilterClauseNotBetween = 92,//     Return value: Is not between
        
        FilterClauseDoesNotContain = 93,//     Return value: Does not contain
        
        FilterClauseDoesNotEqual = 94,//     Return value: Does not equal
        
        FilterClauseNotLike = 95,//     Return value: Is not like
        
        FilterEmptyEnter = 96,//     Return Value: enter a value
        
        FilterEmptyValue = 97,//     Return Value: empty
        
        FilterMenuConditionAdd = 98,//     Return Value: Add Condition
        
        FilterMenuGroupAdd = 99,//     Return Value: Add Group
        
        FilterMenuClearAll = 100,//     Return Value: Clear All
        
        FilterMenuRowRemove = 101,//     Return Value: Remove Row
        
        FilterToolTipNodeAdd = 102,//     Return Value: Adds a new condition to this group.
        
        FilterToolTipNodeRemove = 103,//     Return Value: Removes this condition.
        
        FilterToolTipNodeAction = 104,//     Return Value: Actions.
        
        FilterToolTipValueType = 105,//     Return Value: Compare to a value / other field's value.
        
        FilterToolTipElementAdd = 106,//     Return Value: Adds a new item to the list.
        
        FilterToolTipKeysAdd = 107,//     Return Value: (Use the Insert or Add button on the keyboard)
        
        FilterToolTipKeysRemove = 108,//     Return Value: (Use the Delete or Subtract button on the keyboard)
        //
        // Summary:
        //     Represents the text which identifies the name of an in-place editor within
        //     a container control.
        //     The string which is identified by this enumeration value is used to initiliaze
        //     an editor's DevExpress.XtraEditors.BaseEdit.AccessibleName property when
        //     this editor is activated for in-place editing within container controls (eg.
        //     XtraGrid, XtraTreeList, etc).
        ContainerAccessibleEditName = 109,


        
        FilterCriteriaToStringGroupOperatorAnd = 110,//     Return value: And
        
        FilterCriteriaToStringGroupOperatorOr = 111,//     Return value: Or
        
        FilterCriteriaToStringUnaryOperatorBitwiseNot = 112,//     Return value: ~
        
        FilterCriteriaToStringUnaryOperatorIsNull = 113,//     Return value: Is Null
        
        FilterCriteriaToStringUnaryOperatorMinus = 114,//     Return value: -
        
        FilterCriteriaToStringUnaryOperatorNot = 115,//     Return value: Not
        
        FilterCriteriaToStringUnaryOperatorPlus = 116,//     Return value: +
        
        FilterCriteriaToStringBinaryOperatorBitwiseAnd = 117,//     Return value: @
        
        FilterCriteriaToStringBinaryOperatorBitwiseOr = 118,//     Return value: |
        
        FilterCriteriaToStringBinaryOperatorBitwiseXor = 119,//     Return value: ^
        
        FilterCriteriaToStringBinaryOperatorDivide = 120,//     Return value: /
        
        FilterCriteriaToStringBinaryOperatorEqual = 121,//     Return value: =
        
        FilterCriteriaToStringBinaryOperatorGreater = 122,//     Return value: @gt;
        
        FilterCriteriaToStringBinaryOperatorGreaterOrEqual = 123,//     Return value: @gt;=
        
        FilterCriteriaToStringBinaryOperatorLess = 124,//     Return value: @lt;
        
        FilterCriteriaToStringBinaryOperatorLessOrEqual = 125,//     Return value: @lt;=
        
        FilterCriteriaToStringBinaryOperatorLike = 126,//     Return value: Like
        
        FilterCriteriaToStringBinaryOperatorMinus = 127,//     Return value: -
        
        FilterCriteriaToStringBinaryOperatorModulo = 128,//     Return value: %
        
        FilterCriteriaToStringBinaryOperatorMultiply = 129,//     Return value: *
        
        FilterCriteriaToStringBinaryOperatorNotEqual = 130,//     Return value: @lt;@gt;
        
        FilterCriteriaToStringBinaryOperatorPlus = 131,//     Return value: +
        
        FilterCriteriaToStringBetween = 132,//     Return value: Between
        
        FilterCriteriaToStringIn = 133,//     Return value: In
        
        FilterCriteriaToStringIsNotNull = 134,//     Return value: Is Not Null
        
        FilterCriteriaToStringNotLike = 135,//     Return value: Not Like
        
        FilterCriteriaToStringFunctionIif = 136,//     Return value: Iif
        
        FilterCriteriaToStringFunctionIsNull = 137,//     Return value: IsNull
        
        FilterCriteriaToStringFunctionLen = 138,//     Return value: Len
        
        FilterCriteriaToStringFunctionLower = 139,//     Return value: Lower
        
        FilterCriteriaToStringFunctionNone = 140,//     Return value: None
        
        FilterCriteriaToStringFunctionSubstring = 141,//     Return value: Substring
        
        FilterCriteriaToStringFunctionTrim = 142,//     Return value: Trim
        
        FilterCriteriaToStringFunctionUpper = 143,//     Return value: Upper
        
        FilterCriteriaToStringFunctionLocalDateTimeThisYear = 144,//     Return value: ThisYear
        
        FilterCriteriaToStringFunctionLocalDateTimeThisMonth = 145,//     Return value: ThisMonth
        
        FilterCriteriaToStringFunctionLocalDateTimeLastWeek = 146,//     Return value: LastWeek
        
        FilterCriteriaToStringFunctionLocalDateTimeThisWeek = 147,//     Return value: ThisWeek
        
        FilterCriteriaToStringFunctionLocalDateTimeYesterday = 148,//     Return value: Yesterday
        
        FilterCriteriaToStringFunctionLocalDateTimeToday = 149,//     Return value: Today
        
        FilterCriteriaToStringFunctionLocalDateTimeNow = 150,//     Return value: Now
        
        FilterCriteriaToStringFunctionLocalDateTimeTomorrow = 151,//     Return value: Tomorrow
        
        FilterCriteriaToStringFunctionLocalDateTimeDayAfterTomorrow = 152,//     Return value: DayAfterTomorrow
        
        FilterCriteriaToStringFunctionLocalDateTimeNextWeek = 153,//     Return value: NextWeek
        
        FilterCriteriaToStringFunctionLocalDateTimeTwoWeeksAway = 154,//     Return value: TwoWeeksAway
        
        FilterCriteriaToStringFunctionLocalDateTimeNextMonth = 155,//     Return value: NextMonth
        
        FilterCriteriaToStringFunctionLocalDateTimeNextYear = 156,//     Return value: NextYear
        
        FilterCriteriaToStringFunctionIsOutlookIntervalBeyondThisYear = 157,//     Return value: BeyondThisYear
        
        FilterCriteriaToStringFunctionIsOutlookIntervalLaterThisYear = 158,//     Return value: LaterThisYear
        
        FilterCriteriaToStringFunctionIsOutlookIntervalLaterThisMonth = 159,//     Return value: LaterThisMonth
        
        FilterCriteriaToStringFunctionIsOutlookIntervalNextWeek = 160,//     Return value: NextWeek
        
        FilterCriteriaToStringFunctionIsOutlookIntervalLaterThisWeek = 161,//     Return value: LaterThisWeek
        
        FilterCriteriaToStringFunctionIsOutlookIntervalTomorrow = 162,//     Return value: Tomorrow
        
        FilterCriteriaToStringFunctionIsOutlookIntervalToday = 163,//     Return value: Today
        
        FilterCriteriaToStringFunctionIsOutlookIntervalYesterday = 164,//     Return value: Yesterday
        
        FilterCriteriaToStringFunctionIsOutlookIntervalEarlierThisWeek = 165,//     Return value: EarlierThisWeek
        
        FilterCriteriaToStringFunctionIsOutlookIntervalLastWeek = 166,//     Return value: LastWeek
        
        FilterCriteriaToStringFunctionIsOutlookIntervalEarlierThisMonth = 167,//     Return value: EarlierThisMonth
        
        FilterCriteriaToStringFunctionIsOutlookIntervalEarlierThisYear = 168,//     Return value: EarlierThisYear
        
        FilterCriteriaToStringFunctionIsOutlookIntervalPriorThisYear = 169,//     Return value: PriorThisYear
        
        FilterCriteriaToStringFunctionCustom = 170,//     Return value: Custom
        
        Apply = 171,//     Return Value: Apply
        
        PreviewPanelText = 172,//     Return Value: Preview:
        
        TransparentBackColorNotSupported = 173,//     Return Value: This control does not support transparent background colors
        
        FilterOutlookDateText = 174,//     Return value: "Show all|Filter by a specific date:|Beyond this year|Later
        //     this year|Later this month|Next week|Later this week|Tomorrow|Today|Yesterday|Earlier
        //     this week|Last week|Earlier this month|Earlier this year|Prior to this year"
        
        FilterDateTimeConstantMenuCaption = 175,//     Return value: DateTime constants
        
        FilterDateTimeOperatorMenuCaption = 176,//     Return value: DateTime operators





*/







            }
            return "";
        }

    }
}
