﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraGrid.Localization;

namespace Pharmacy2016.util.locale
{
    class GridLocalizerMongolian : GridLocalizer
    {
        public override string Language { get { return "Mongolian"; } }
        public override string GetLocalizedString(GridStringId id)
        {
            string ret = "";
            switch (id)
            {

                #region -------------------------------------------------------------------------------
                case GridStringId.FileIsNotFoundError: return "Файл олдоогүй алдаа";
                case GridStringId.ColumnViewExceptionMessage: return "ExceptionMessage";
                case GridStringId.WindowErrorCaption: return "WindowErrorCaption";

                case GridStringId.GridGroupPanelText: return "Та бүлэглэх баганаа энд чирч тавина уу.";
                case GridStringId.GridNewRowText: return "Шинэ мөр";
                case GridStringId.GridOutlookIntervals: return "";

                case GridStringId.CustomizationCaption: return "Багана сонгогч";
                case GridStringId.CustomizationColumns: return "Баганууд";
                case GridStringId.CustomizationBands: return "Бүлэгүүд";
                case GridStringId.CustomizationFormColumnHint: return "Та энд хасах багануудаа чирч тавина уу.";
                case GridStringId.CustomizationFormBandHint: return "Та энд хасах бүлгүүдээ чирч тавина уу.";
                case GridStringId.EditFormCancelButton: return "Гарах";
                case GridStringId.EditFormUpdateButton: return "Хадгалах";
                case GridStringId.WindowWarningCaption: return "Анхааруулага!";
                case GridStringId.EditFormSaveMessage: return "Талбарын утга өөрчлөгдсөн байна, Та эхлээд хадгална уу.";
                case GridStringId.EditFormCancelMessage: return "Талбарын утга өөрчлөгдсөн байна, Та эхлээд хадгална уу.";
                #endregion

                #region PopupFilter ------------------------------------------------------------
                case GridStringId.PopupFilterAll: return "(Бүгд)";
                case GridStringId.PopupFilterCustom: return "(Дурын)";
                case GridStringId.PopupFilterBlanks: return "(Хоосон)";
                case GridStringId.PopupFilterNonBlanks: return "(Хоосон биш)";
                #endregion   

                #region CustomFilterDialog ------------------------------------------------------------
                case GridStringId.CustomFilterDialogFormCaption: return "Шүүлтүүр";
                case GridStringId.CustomFilterDialogCaption: return "Шүүлтүүр";
                case GridStringId.CustomFilterDialogRadioAnd: return "Ба";
                case GridStringId.CustomFilterDialogRadioOr: return "Эсвэл";
                case GridStringId.CustomFilterDialogOkButton: return "Шүүх";
                case GridStringId.CustomFilterDialogClearFilter: return "Цэвэрлэх";
                case GridStringId.CustomFilterDialog2FieldCheck: return "Dialog2FieldCheck";
                case GridStringId.CustomFilterDialogCancelButton: return "Болих";
                case GridStringId.FindControlFindButton: return "Хайх";
                case GridStringId.FindControlClearButton: return "Цэвэрлэх";
                //case GridStringId.CustomFilterDialogConditionEQU: return "Тэнцүү =";
                //case GridStringId.CustomFilterDialogConditionNEQ: return "Ялгаатай <>";
                //case GridStringId.CustomFilterDialogConditionGT: return "Эрс их >";
                //case GridStringId.CustomFilterDialogConditionGTE: return "Их буюу тэнцүү >=";
                //case GridStringId.CustomFilterDialogConditionLT: return "Эрс бага <";
                //case GridStringId.CustomFilterDialogConditionLTE: return "Бага буюу тэнцүү <=";
                //case GridStringId.CustomFilterDialogConditionBlanks: return "Хоосон";
                //case GridStringId.CustomFilterDialogConditionNonBlanks: return "Хоосон биш";
                //case GridStringId.CustomFilterDialogConditionLike: return "Ижилхэн";
                //case GridStringId.CustomFilterDialogConditionNotLike: return "Ижилхэн биш";
                case GridStringId.CustomFilterDialogEmptyValue: return "Хоосон утга";
                case GridStringId.CustomFilterDialogEmptyOperator: return "Хоосон оператор";
                case GridStringId.CustomFilterDialogHint: return "Шүүлтүүрийн тусламж";
                //case GridStringId.CustomFilterDialog2FieldCheck: return "Шүүлтүүрийн тусламж";
                //case GridStringId.HideAutoFilter

                #endregion

                #region MenuFooter ------------------------------------------------------------
                case GridStringId.MenuFooterSum: return "Нийлбэр";
                case GridStringId.MenuFooterMin: return "Хамгийн бага";
                case GridStringId.MenuFooterMax: return "Хамгийн их";
                case GridStringId.MenuFooterCount: return "Тоо";
                case GridStringId.MenuFooterAverage: return "Дундаж";
                case GridStringId.MenuFooterNone: return "Байхгүй";
                case GridStringId.MenuFooterSumFormat: return "∑={0}";
                case GridStringId.MenuFooterMinFormat: return "MIN={0}";
                case GridStringId.MenuFooterMaxFormat: return "MAX={0}";
                case GridStringId.MenuFooterCountFormat: return "n={0}";
                case GridStringId.MenuFooterAverageFormat: return "∑/n={0}";
                case GridStringId.MenuFooterCustomFormat: return "{0}";
                case GridStringId.MenuFooterCountGroupFormat: return "n={0}";
                #endregion

                #region MenuColumn ------------------------------------------------------------
                // Баганын цэсний текстүүд
                case GridStringId.MenuColumnSortAscending: return "Өсөхөөр эрэмбэлэх";
                case GridStringId.MenuColumnSortDescending: return "Буурахаар эрэмбэлэх";
                case GridStringId.MenuColumnShowColumn: return "Багана харуулах";
                case GridStringId.MenuColumnRemoveColumn: return "Багана нуух";
                case GridStringId.MenuColumnGroup: return "Баганаар бүлэглэх";
                case GridStringId.MenuColumnUnGroup: return "Бүлэглэлтийг цэвэрлэх";
                case GridStringId.MenuColumnColumnCustomization: return "Багана сонгогч";
                case GridStringId.MenuColumnBestFit: return "Баганын өргөнийг тохируулах";
                case GridStringId.MenuColumnFilter: return "Шүүлтүүр";
                case GridStringId.MenuColumnClearFilter: return "Шүүлтүүрийг цэвэрлэх";
                case GridStringId.MenuColumnBestFitAllColumns: return "Баганын өргөнийг тохируулах (бүх багана)";
                case GridStringId.MenuColumnResetGroupSummarySort: return "Бүлэглэсэн баганын эрэмбэлэлтийг цэвэрлэх";
                case GridStringId.MenuColumnGroupSummarySortFormat: return "\"{0}\" баганын мөрийн тоогоор \"{2}\"";
                case GridStringId.MenuColumnSortGroupBySummaryMenu: return "Бүлэглэсэн баганаар эрэмбэлэх";
                case GridStringId.MenuColumnClearSorting: return "Эрэмбэлэлтийг цэвэрлэх";
                case GridStringId.MenuColumnClearAllSorting: return "Бүх эрэмблэлийг цэвэрлэх";
                case GridStringId.MenuColumnFilterEditor: return "Шүүлтүүр үүсгэх";
                case GridStringId.MenuColumnFindFilterShow: return "Шүүлтүүрийг үзүүлэх";
                case GridStringId.MenuColumnBandCustomization: return "Багана, бүлэг сонгогч";
                case GridStringId.MenuColumnFindFilterHide: return "Шүүлтүүрийг нуух";
                case GridStringId.MenuColumnGroupBox: return "Бүлэглэлтийн талбар";
                case GridStringId.MenuColumnAutoFilterRowHide: return "Автомат мөр шүүлтүүрийг нуух";
                case GridStringId.MenuColumnAutoFilterRowShow: return "Автомат мөр шүүлтүүрийг харуулах";
                case GridStringId.MenuColumnFilterMode: return "Шүүлтүүрийн горим";
                case GridStringId.MenuColumnFilterModeDisplayText: return "Текстээр үзүүлэх";
                case GridStringId.MenuColumnFilterModeValue: return "Нэгжийн утгаар үзүүлэх";
                #endregion

                #region MenuGroupPanel ------------------------------------------------------------
                case GridStringId.MenuGroupPanelFullExpand: return "Бүлгүүдийг задлах";
                case GridStringId.MenuGroupPanelFullCollapse: return "Бүлгүүдийг хураах";
                case GridStringId.MenuGroupPanelClearGrouping: return "Бүлэглэлтийг цэвэрлэх";
                case GridStringId.MenuGroupPanelShow: return "Бүлэглэлтийн талбарыг харуулах";
                case GridStringId.MenuGroupPanelHide: return "Бүлэглэлтийн талбарыг нуух";

                #endregion

                #region PrintDesigner ------------------------------------------------------------
                case GridStringId.PrintDesignerGridView: return "PrintDesignerGridView";
                case GridStringId.PrintDesignerCardView: return "PrintDesignerCardView";
                case GridStringId.PrintDesignerLayoutView: return "PrintDesignerLayoutView";
                case GridStringId.PrintDesignerBandedView: return "PrintDesignerBandedView";
                case GridStringId.PrintDesignerBandHeader: return "PrintDesignerBandHeader";
                case GridStringId.PrintDesignerDescription: return "PrintDesignerDescription";
                #endregion

                #region CardView ------------------------------------------------------------
                case GridStringId.CardViewNewCard: return "Шинэ карт";
                case GridStringId.CardViewQuickCustomizationButton: return "Дурын";
                case GridStringId.CardViewQuickCustomizationButtonFilter: return "Шүүлтүүр";
                case GridStringId.CardViewQuickCustomizationButtonSort: return "Эрэмбэлэлт";
                case GridStringId.CardViewCaptionFormat: return "( {2} )    {6} {5}";
                #endregion

                #region FilterBuilder ------------------------------------------------------------
                case GridStringId.FilterPanelCustomizeButton: return "Шүүлтүүр өөрчлөх";
                case GridStringId.FilterBuilderOkButton: return "Шүүх";
                case GridStringId.FilterBuilderCancelButton: return "Болих";
                case GridStringId.FilterBuilderApplyButton: return "Харуулах";
                case GridStringId.FilterBuilderCaption: return "Шүүлтүүр";
                #endregion

                #region LayoutView ------------------------------------------------------------
                case GridStringId.LayoutViewSingleModeBtnHint: return "";
                case GridStringId.LayoutViewRowModeBtnHint: return "";
                case GridStringId.LayoutViewColumnModeBtnHint: return "";
                case GridStringId.LayoutViewMultiRowModeBtnHint: return "";
                case GridStringId.LayoutViewMultiColumnModeBtnHint: return "";
                case GridStringId.LayoutViewCarouselModeBtnHint: return "";
                case GridStringId.LayoutViewPanBtnHint: return "";
                case GridStringId.LayoutViewCustomizeBtnHint: return "";
                case GridStringId.LayoutViewCloseZoomBtnHintClose: return "";
                case GridStringId.LayoutViewCloseZoomBtnHintZoom: return "";
                case GridStringId.LayoutViewButtonApply: return "";
                case GridStringId.LayoutViewButtonPreview: return "";
                case GridStringId.LayoutViewButtonOk: return "";
                case GridStringId.LayoutViewButtonCancel: return "";
                case GridStringId.LayoutViewButtonSaveLayout: return "";
                case GridStringId.LayoutViewButtonLoadLayout: return "";
                case GridStringId.LayoutViewButtonCustomizeShow: return "";
                case GridStringId.LayoutViewButtonCustomizeHide: return "";
                case GridStringId.LayoutViewButtonReset: return "";
                case GridStringId.LayoutViewButtonShrinkToMinimum: return "";
                case GridStringId.LayoutViewPageTemplateCard: return "";
                case GridStringId.LayoutViewPageViewLayout: return "";
                case GridStringId.LayoutViewGroupCustomization: return "";
                case GridStringId.LayoutViewGroupCaptions: return "";
                case GridStringId.LayoutViewGroupIndents: return "";
                case GridStringId.LayoutViewGroupHiddenItems: return "";
                case GridStringId.LayoutViewGroupTreeStructure: return "";
                case GridStringId.LayoutViewGroupPropertyGrid: return "";
                case GridStringId.LayoutViewLabelTextIndent: return "";
                case GridStringId.LayoutViewLabelPadding: return "";
                case GridStringId.LayoutViewLabelSpacing: return "";
                case GridStringId.LayoutViewLabelCaptionLocation: return "";
                case GridStringId.LayoutViewLabelGroupCaptionLocation: return "";
                case GridStringId.LayoutViewLabelTextAlignment: return "";
                case GridStringId.LayoutViewGroupView: return "";
                case GridStringId.LayoutViewGroupLayout: return "";
                case GridStringId.LayoutViewGroupCards: return "";
                case GridStringId.LayoutViewGroupFields: return "";
                case GridStringId.LayoutViewLabelShowLines: return "";
                case GridStringId.LayoutViewLabelShowHeaderPanel: return "";
                case GridStringId.LayoutViewLabelShowFilterPanel: return "";
                case GridStringId.LayoutViewLabelScrollVisibility: return "";
                case GridStringId.LayoutViewLabelViewMode: return "";
                case GridStringId.LayoutViewLabelCardArrangeRule: return "";
                case GridStringId.LayoutViewLabelCardEdgeAlignment: return "";
                case GridStringId.LayoutViewGroupIntervals: return "";
                case GridStringId.LayoutViewLabelHorizontal: return "";
                case GridStringId.LayoutViewLabelVertical: return "";
                case GridStringId.LayoutViewLabelShowCardCaption: return "";
                case GridStringId.LayoutViewLabelShowCardExpandButton: return "";
                case GridStringId.LayoutViewLabelShowCardBorder: return "";
                case GridStringId.LayoutViewLabelAllowFieldHotTracking: return "";
                case GridStringId.LayoutViewLabelShowFieldBorder: return "";
                case GridStringId.LayoutViewLabelShowFieldHint: return "";
                case GridStringId.LayoutViewCustomizationFormCaption: return "";
                case GridStringId.LayoutViewCustomizationFormDescription: return "";
                case GridStringId.LayoutModifiedWarning: return "";
                case GridStringId.LayoutViewCardCaptionFormat: return "";
                case GridStringId.LayoutViewFieldCaptionFormat: return "";
                #endregion

                default:
                    ret = "";
                    break;
            }
            return ret;
        }

    }
}
