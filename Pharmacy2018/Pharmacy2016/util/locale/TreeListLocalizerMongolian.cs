﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraTreeList.Localization;

namespace Pharmacy2016.util.locale
{

    class TreeListLocalizerMongolian : TreeListLocalizer
    {
        public override string Language { get { return "Mongolian"; } }
        public override string GetLocalizedString(TreeListStringId id)
        {
            string ret = "";
            switch (id)
            {
                // Хөлийн хэсэг
                case TreeListStringId.MenuFooterSum: return "Нийлбэр"; // "Sum"
                case TreeListStringId.MenuFooterMin: return "Хамгийн бага"; // "Min"
                case TreeListStringId.MenuFooterMax: return "Хамгийн их"; // "Max"
                case TreeListStringId.MenuFooterCount: return "Тоо"; // "Count"
                case TreeListStringId.MenuFooterAverage: return "Дундаж"; // "Average"
                case TreeListStringId.MenuFooterNone: return "Байхгүй"; // "None"
                case TreeListStringId.MenuFooterAllNodes: return "Бүх зангилаа"; // "All Nodes"
                case TreeListStringId.MenuFooterSumFormat: return "SUM={0:#.##}"; // "SUM={0:#.##}"
                case TreeListStringId.MenuFooterMinFormat: return "MIN={0}"; // "MIN={0}"
                case TreeListStringId.MenuFooterMaxFormat: return "MAX={0}"; // "MAX={0}"
                case TreeListStringId.MenuFooterCountFormat: return "{0}"; // "{0}"
                case TreeListStringId.MenuFooterAverageFormat: return "AVG={0:#.##}"; // "AVG={0:#.##}"

                // Баганын менюний хэсэг
                case TreeListStringId.MenuColumnSortAscending: return "Өсөхөөр эрэмбэлэх"; // "Sort Ascending"
                case TreeListStringId.MenuColumnSortDescending: return "Буурахаар эрэмбэлэх"; // "Sort Descending"
                case TreeListStringId.MenuColumnColumnCustomization: return "Багана сонгогч"; // "Column Chooser"
                case TreeListStringId.MenuColumnClearSorting: return "Ангилалыг арилгах"; // "Clear"
                case TreeListStringId.MenuColumnBestFit: return "Баганын өргөнийг тохируулах"; // "Best Fit"
                case TreeListStringId.MenuColumnBestFitAllColumns: return "Баганын өргөнийг тохируулах (бүх багана)"; // "Best Fit (all columns)"
                case TreeListStringId.MenuColumnFilterEditor: return "Шүүлтүүр өөрчлөх";
                case TreeListStringId.MenuColumnAutoFilterRowHide: return "Авто шүүлтүүр нуух";
                case TreeListStringId.MenuColumnAutoFilterRowShow: return "Авто шүүлтүүр үзүүлэх";
                
                // Шүүлтүүр үүсгэх цонх
                case TreeListStringId.FilterPanelCustomizeButton: return "Шүүлтүүр өөрчлөх";
                case TreeListStringId.FilterEditorOkButton: return "Шүүх";
                case TreeListStringId.FilterEditorCancelButton: return "Болих";
                case TreeListStringId.FilterEditorApplyButton: return "Харуулах";
                case TreeListStringId.FilterEditorCaption: return "Шүүлтүүр";
                
                // Баганын хэсэг
                case TreeListStringId.ColumnCustomizationText: return "Баганын өөрчлөлт"; // "Customization"
                case TreeListStringId.ColumnNamePrefix: return "col"; // "col"


                // Хэвлэлтийн хэсэг
                case TreeListStringId.PrintDesignerHeader: return "Хэвлэх тохиргоо"; // "Print Settings"
                case TreeListStringId.PrintDesignerDescription: return "Модыг хэвлэх янз бүрийн тохиргоог өгөх"; // "Set up various printing options for the current treelist."
                
                // Алдааны хэсэг
                case TreeListStringId.InvalidNodeExceptionText: return "Та утгаа зөв болгохыг хүсч байна уу?"; // "Do you want to correct the value ?"
                case TreeListStringId.MultiSelectMethodNotSupported: return "OptionsBehavior.MultiSelect гэсэн сонголт идэвхгүй болоход тодорхойлогдсон функц ажиллахгүй байна."; // "Specified method will not work when OptionsBehavior.MultiSelect is inactive."

                default:
                    ret = "";
                    break;
            }
            return ret;
        }
    }
}
