﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraPivotGrid.Localization;

namespace Pharmacy2016.util.locale
{
    class PivotGridLocalizerMongolian : PivotGridLocalizer
    {
        public override string Language { get { return "Mongolian"; } }
        public override string GetLocalizedString(PivotGridStringId id)
        {
            string ret = "";
            switch (id)
            {
                     
                #region ----------------------------------------------------------------------------
                case PivotGridStringId.RowHeadersCustomization: return "RowHeadersCustomization";
                case PivotGridStringId.ColumnHeadersCustomization: return "ColumnHeadersCustomization";
                case PivotGridStringId.DataHeadersCustomization: return "DataHeadersCustomization";
                case PivotGridStringId.RowArea: return "Мөр";
                case PivotGridStringId.ColumnArea: return "Багана";
                case PivotGridStringId.DataArea: return "Хүснэгтийн өгөгдөл";
                case PivotGridStringId.CustomizationFormCaption: return "Талбаруудын жагсаалт";
                case PivotGridStringId.CustomizationFormText: return "Талбаруудаа чирч хийн үү";
                case PivotGridStringId.CustomizationFormAddTo: return "Нэмэх";
                #endregion

                #region Filter ------------------------------------------------------------
                case PivotGridStringId.FilterHeadersCustomization: return "Шүүлтүүр";
                case PivotGridStringId.FilterArea: return "Шүүлтүүр";
                case PivotGridStringId.FilterShowAll: return "(Бүгд)";
                case PivotGridStringId.FilterOk: return "Тийм";
                case PivotGridStringId.FilterCancel: return "Болих";
                case PivotGridStringId.FilterShowBlanks: return "(Хоосон)";
                #endregion   

                #region TotalFormat ------------------------------------------------------------
                case PivotGridStringId.Total: return "Нийт";
                case PivotGridStringId.GrandTotal: return "Нийт дүн";
                case PivotGridStringId.TotalFormat: return "\"{0}\" нийлбэр";
                case PivotGridStringId.TotalFormatCount: return "\"{0}\" нийт";
                case PivotGridStringId.TotalFormatSum: return "\"{0}\" нийлбэр";
                case PivotGridStringId.TotalFormatMin: return "\"{0}\" хамгийн бага";
                case PivotGridStringId.TotalFormatMax: return "\"{0}\" хамгийн их";
                case PivotGridStringId.TotalFormatAverage: return "\"{0}\" дундаж";
                case PivotGridStringId.TotalFormatStdDev: return "\"{0}\" std";
                case PivotGridStringId.TotalFormatStdDevp: return "\"{0}\" dev";
                case PivotGridStringId.TotalFormatVar: return "\"{0}\" var";
                case PivotGridStringId.TotalFormatVarp: return "\"{0}\" varp";
                case PivotGridStringId.TotalFormatCustom: return "\"{0}\" custom";
                #endregion

                #region PrintDesigner ------------------------------------------------------------
                case PivotGridStringId.PrintDesigner: return "";
                case PivotGridStringId.PrintDesignerPageOptions: return "";
                case PivotGridStringId.PrintDesignerPageBehavior: return "";
                case PivotGridStringId.PrintDesignerCategoryDefault: return "";
                case PivotGridStringId.PrintDesignerCategoryLines: return "";
                case PivotGridStringId.PrintDesignerCategoryHeaders: return "";
                case PivotGridStringId.PrintDesignerCategoryFieldValues: return "";
                case PivotGridStringId.PrintDesignerHorizontalLines: return "";
                case PivotGridStringId.PrintDesignerVerticalLines: return "";
                case PivotGridStringId.PrintDesignerFilterHeaders: return "";
                case PivotGridStringId.PrintDesignerDataHeaders: return "";
                case PivotGridStringId.PrintDesignerColumnHeaders: return "";
                case PivotGridStringId.PrintDesignerRowHeaders: return "";
                case PivotGridStringId.PrintDesignerHeadersOnEveryPage: return "";
                case PivotGridStringId.PrintDesignerUnusedFilterFields: return "";
                case PivotGridStringId.PrintDesignerMergeColumnFieldValues: return "";
                case PivotGridStringId.PrintDesignerMergeRowFieldValues: return "";
                case PivotGridStringId.PrintDesignerUsePrintAppearance: return "";
                #endregion

                #region PopupMenu ------------------------------------------------------------
                case PivotGridStringId.PopupMenuRefreshData: return "Дахин ачааллах";
                case PivotGridStringId.PopupMenuHideField: return "Нуух";
                case PivotGridStringId.PopupMenuShowFieldList: return "Талбаруудын жагсаалт нээх";
                case PivotGridStringId.PopupMenuHideFieldList: return "Талбаруудын жагсаалт хаах";
                case PivotGridStringId.PopupMenuFieldOrder: return "Байрлал өөрчлөх";
                case PivotGridStringId.PopupMenuMovetoBeginning: return "Эхэнд";
                case PivotGridStringId.PopupMenuMovetoLeft: return "Зүүн";
                case PivotGridStringId.PopupMenuMovetoRight: return "Баруун";
                case PivotGridStringId.PopupMenuMovetoEnd: return "Сүүлд";
                case PivotGridStringId.PopupMenuCollapse: return "Хураах";
                case PivotGridStringId.PopupMenuExpand: return "Задлах";
                case PivotGridStringId.PopupMenuCollapseAll: return "Бүгдийг хураах";
                case PivotGridStringId.PopupMenuExpandAll: return "Бүгдийг задлах";
                case PivotGridStringId.PopupMenuShowPrefilter: return "Шүүлтүүр нээх";
                case PivotGridStringId.PopupMenuHidePrefilter: return "Шүүлтүүр хаах";
                case PivotGridStringId.PopupMenuSortFieldByColumn: return "\"{0}\" баганаар эрэмбэлэх";
                case PivotGridStringId.PopupMenuSortFieldByRow: return "\"{0}\" мөрөөр эрэмбэлэх";
                case PivotGridStringId.PopupMenuRemoveAllSortByColumn: return "";
                #endregion

                #region ------------------------------------------------------------
                case PivotGridStringId.DataFieldCaption: return "DataFieldCaption";
                case PivotGridStringId.TopValueOthersRow: return "";
                case PivotGridStringId.CellError: return "CellError";
                

                //case PivotGridStringId.CannotCopyMultipleSelections: return "CannotCopyMultipleSelections";
                case PivotGridStringId.PrefilterFormCaption: return "Шүүлтүүр";
                case PivotGridStringId.EditPrefilter: return "Шүүлтүүр өөрчлөх";
                case PivotGridStringId.OLAPMeasuresCaption: return "";
                case PivotGridStringId.OLAPDrillDownFilterException: return "";
                case PivotGridStringId.TrendGoingUp: return "";
                case PivotGridStringId.TrendGoingDown: return "";
                case PivotGridStringId.TrendNoChange: return "";

                case PivotGridStringId.StatusBad: return "";
                case PivotGridStringId.StatusNeutral: return "";
                case PivotGridStringId.StatusGood: return "";
                #endregion

                #region Alt ------------------------------------------------------------
                case PivotGridStringId.Alt_Expand: return "Alt_Expand";
                case PivotGridStringId.Alt_Collapse: return "Alt_Collapse";
                case PivotGridStringId.Alt_SortedAscending: return "Alt_SortedAscending";
                case PivotGridStringId.Alt_SortedDescending: return "Alt_SortedDescending";
                case PivotGridStringId.Alt_FilterWindowSizeGrip: return "Alt_FilterWindowSizeGrip";
                case PivotGridStringId.Alt_FilterButton: return "Alt_FilterButton";
                case PivotGridStringId.Alt_FilterButtonActive: return "Alt_FilterButtonActive";
                case PivotGridStringId.Alt_DragHideField: return "Alt_DragHideField";
                #endregion

                default:
                    ret = "";
                    break;
            }
            return ret;
        }

    }
}
