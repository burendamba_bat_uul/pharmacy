﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraBars.Localization;

namespace Pharmacy2016.util.locale
{
    class BarLocalizerMongolian : BarLocalizer
    {
        public override string Language { get { return "Mongolian"; } }
        public override string GetLocalizedString( BarString id)
        {
            string ret = "";
            switch (id)
            {
                case BarString.None: return "";  // ""
                case BarString.PopupMenuEditor: return "Унждаг цэс";  // "Popup Menu Editor"
                case BarString.AddOrRemove: return "Товч нэмэх эсвэл устгах"; // "Add or Remove Buttons"
                case BarString.ResetBar: return "Та {0} хэрэгсэлийн мөрийн өөрчлөлтийг анхны хэлбэрт нь оруулахыг хүсэж байна уу"; // "Are you sure you want to reset the changes made to the '{0}' toolbar?"
                case BarString.ResetBarCaption: return "Өөрчлөх"; // "Customize"
                case BarString.ResetButton: return "Хэрэгсэлийн мөрийг анхны хэлбэрт нь оруулах"; // "Reset Toolbar"
                case BarString.CustomizeButton: return "Өөрчлөх ..."; // "Customize ..."
                case BarString.ToolBarMenu: return ""; // ""
                case BarString.ToolbarNameCaption: return "Хэрэгсэлийн мөрийн нэр:"; // "Toolbar Name:"
                case BarString.NewToolbarCaption: return "Шинэ хэрэгсэлийн мөр"; // "New Toolbar"
                case BarString.NewToolbarCustomNameFormat: return "Хэрэгсэлийн мөр {0}"; // "Custom {0}"
                case BarString.RenameToolbarCaption: return "Хэрэгсэлийн мөрийн нэрийг өөрчлөх"; // "Rename Toolbar"
                case BarString.CustomizeWindowCaption: return "Өөрчлөлт"; // "Customization"
                case BarString.MenuAnimationSystem: return "(Анхдагч утга)"; // "(System default)"
                case BarString.MenuAnimationNone: return "Байхгүй"; // "None"
                case BarString.MenuAnimationSlide: return "Слайд"; // "Slide"
                case BarString.MenuAnimationFade: return "Ердийн"; // "Fade"
                case BarString.MenuAnimationUnfold: return "Дэлгэх"; // "Unfold"
                case BarString.MenuAnimationRandom: return "Санамсаргүй"; // "Random"
                case BarString.RibbonToolbarAbove: return "Хэрэгсэлийн мөрийг Ribbon цэсийн дээд талд харуулах"; // "Show Quick Access Toolbar Above the Ribbon"
                case BarString.RibbonToolbarBelow: return "Хэрэгсэлийн мөрийг Ribbon цэсийн доод талд харуулах"; // "Show Quick Access Toolbar Below the Ribbon"
                case BarString.RibbonToolbarAdd: return "Хэрэгсэлийн мөрөнд нэмэх"; // "Add to Quick Access Toolbar"
                case BarString.RibbonToolbarMinimizeRibbon: return "Ribbon жижиг цэс"; // "Minimize the Ribbon"
                case BarString.RibbonToolbarRemove: return "Хэрэгсэлийн мөрөөс устгах"; // "Remove from Quick Access Toolbar"
                case BarString.RibbonGalleryFilter: return "Бүх бүлэгүүд"; // "(All Groups)"
                case BarString.RibbonGalleryFilterNone: return "Байхгүй"; // "None"
                case BarString.BarUnassignedItems: return "(Тодорхой бус хэсгүүд)"; // "(Unassigned Items)"
                case BarString.BarAllItems: return "(Бүх хэсэг)"; // "(All Items)"
                case BarString.RibbonUnassignedPages: return "(Тодорхой бус хуудсууд)"; // "(Unassigned Pages)"
                case BarString.RibbonAllPages: return "(Бүх хуудас)"; // "(All Pages)"
                case BarString.NewToolbarName: return "Хэрэгсэлүүд"; // "Tools"
                case BarString.NewMenuName: return "Үндсэн цэс"; // "Main menu"
                case BarString.NewStatusBarName: return "Төлвийн мөр"; // "Status bar"
                
                default:
                    ret = "";
                    break;
            }
            return ret;
        }
    }
}
