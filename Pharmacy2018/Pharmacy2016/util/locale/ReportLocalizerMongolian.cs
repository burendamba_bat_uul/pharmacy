﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraReports.Localization;

namespace Pharmacy2016.util.locale
{
    class ReportLocalizerMongolian : ReportLocalizer
    {
        public override string Language { get { return "Mongolian"; } }
        public override string GetLocalizedString(ReportStringId id)
        {
            string ret = "";
            switch (id)
            {
                #region ------------------------------------------------------------
                case ReportStringId.PanelDesignMsg: return "";
                case ReportStringId.MultiColumnDesignMsg1: return "";
                case ReportStringId.MultiColumnDesignMsg2: return "";
                case ReportStringId.DesignerStatus_Location: return "";
                case ReportStringId.DesignerStatus_Size: return "";
                case ReportStringId.Wizard_PageChooseFields_Msg: return "";
                #endregion

                #region Msg ------------------------------------------------------------
                case ReportStringId.Msg_WarningFontNameCantBeEmpty: return "";
                case ReportStringId.Msg_FileNotFound: return "";
                case ReportStringId.Msg_WrongReportClassName: return "";
                case ReportStringId.Msg_CreateReportInstance: return "";
                case ReportStringId.Msg_FileCorrupted: return "";
                case ReportStringId.Msg_FileContentCorrupted: return "";
                case ReportStringId.Msg_IncorrectArgument: return "";
                case ReportStringId.Msg_InvalidMethodCall: return "";
                case ReportStringId.Msg_ScriptError: return "";
                case ReportStringId.Msg_ScriptExecutionError: return "";
                case ReportStringId.Msg_InvalidReportSource: return "";
                case ReportStringId.Msg_IncorrectBandType: return "";
                //case ReportStringId.Msg_InvPropName: return "";
                case ReportStringId.Msg_CreateSomeInstance: return "";
                case ReportStringId.Msg_DontSupportMulticolumn: return "";
                case ReportStringId.Msg_FillDataError: return "";
                //case ReportStringId.Msg_CyclicBoormarks: return "";
                case ReportStringId.Msg_LargeText: return "";
                case ReportStringId.Msg_ScriptingPermissionErrorMessage: return "";
                case ReportStringId.Msg_ReportImporting: return "";
                case ReportStringId.Msg_IncorrectPadding: return "";
                case ReportStringId.Msg_WarningControlsAreOverlapped: return "";
                case ReportStringId.Msg_WarningControlsAreOutOfMargin: return "";
                case ReportStringId.Msg_ShapeRotationToolTip: return "";
                //case ReportStringId.Msg_ContainsIllegalSymbols: return "";
                case ReportStringId.Msg_WarningRemoveCalculatedFields: return "";
                case ReportStringId.Msg_WarningRemoveParameters: return "";
                case ReportStringId.Msg_ErrorTitle: return "";
                case ReportStringId.Msg_SerializationErrorTitle: return "";
                case ReportStringId.Msg_InvalidExpression: return "";
                case ReportStringId.Msg_InvalidCondition: return "";
                case ReportStringId.Msg_GroupSortWarning: return "";
                case ReportStringId.Msg_GroupSortNoDataSource: return "";
                #endregion

                #region Cmd ------------------------------------------------------------
                case ReportStringId.Cmd_InsertDetailReport: return "";
                case ReportStringId.Cmd_InsertUnboundDetailReport: return "";
                case ReportStringId.Cmd_ViewCode: return "";
                case ReportStringId.Cmd_BringToFront: return "";
                case ReportStringId.Cmd_SendToBack: return "";
                case ReportStringId.Cmd_AlignToGrid: return "";
                case ReportStringId.Cmd_TopMargin: return "";
                case ReportStringId.Cmd_BottomMargin: return "";
                case ReportStringId.Cmd_ReportHeader: return "";
                case ReportStringId.Cmd_ReportFooter: return "";
                case ReportStringId.Cmd_PageHeader: return "";
                case ReportStringId.Cmd_PageFooter: return "";
                case ReportStringId.Cmd_GroupHeader: return "";
                case ReportStringId.Cmd_GroupFooter: return "";
                case ReportStringId.Cmd_Detail: return "";
                case ReportStringId.Cmd_DetailReport: return "";
                case ReportStringId.Cmd_RtfClear: return "";
                case ReportStringId.Cmd_RtfLoad: return "";
                case ReportStringId.Cmd_TableInsert: return "";
                case ReportStringId.Cmd_TableInsertRowAbove: return "";
                case ReportStringId.Cmd_TableInsertRowBelow: return "";
                case ReportStringId.Cmd_TableInsertColumnToLeft: return "";
                case ReportStringId.Cmd_TableInsertColumnToRight: return "";
                case ReportStringId.Cmd_TableInsertCell: return "";
                case ReportStringId.Cmd_TableDelete: return "";
                case ReportStringId.Cmd_TableDeleteRow: return "";
                case ReportStringId.Cmd_TableDeleteColumn: return "";
                case ReportStringId.Cmd_TableDeleteCell: return "";
                case ReportStringId.Cmd_Cut: return "";
                case ReportStringId.Cmd_Copy: return "";
                case ReportStringId.Cmd_Paste: return "";
                case ReportStringId.Cmd_Delete: return "";
                case ReportStringId.Cmd_Properties: return "";
                case ReportStringId.Cmd_InsertBand: return "";
                case ReportStringId.Cmd_BandMoveUp: return "";
                case ReportStringId.Cmd_BandMoveDown: return "";
                //case ReportStringId.Cmd_AddCalculatedField: return "";
                //case ReportStringId.Cmd_EditCalculatedFields: return "";
                //case ReportStringId.Cmd_ClearCalculatedFields: return "";
                //case ReportStringId.Cmd_AddParameter: return "";
                //case ReportStringId.Cmd_EditParameters: return "";
                //case ReportStringId.Cmd_ClearParameters: return "";
                //case ReportStringId.Cmd_DeleteCalculatedField: return "";
                //case ReportStringId.Cmd_DeleteParameter: return "";
                //case ReportStringId.Cmd_EditExpression: return "";
                #endregion

                #region Cat ------------------------------------------------------------
                case ReportStringId.CatLayout: return "";
                case ReportStringId.CatAppearance: return "";
                case ReportStringId.CatData: return "";
                case ReportStringId.CatBehavior: return "";
                case ReportStringId.CatNavigation: return "";
                case ReportStringId.CatPageSettings: return "";
                case ReportStringId.CatUserDesigner: return "";
                case ReportStringId.CatDesign: return "";
                case ReportStringId.CatParameters: return "";
                case ReportStringId.CatStructure: return "";
                #endregion

                #region BandDsg ------------------------------------------------------------
                case ReportStringId.BandDsg_QuantityPerPage: return "";
                case ReportStringId.BandDsg_QuantityPerReport: return "";
                #endregion

                #region UD ------------------------------------------------------------
                case ReportStringId.UD_ReportDesigner: return "";
                case ReportStringId.UD_Msg_ReportChanged: return "";
                case ReportStringId.UD_TTip_FileOpen: return "";
                case ReportStringId.UD_TTip_FileSave: return "";
                case ReportStringId.UD_TTip_EditCut: return "";
                case ReportStringId.UD_TTip_EditCopy: return "";
                case ReportStringId.UD_TTip_EditPaste: return "";
                case ReportStringId.UD_TTip_Redo: return "";
                case ReportStringId.UD_TTip_AlignToGrid: return "";
                case ReportStringId.UD_TTip_AlignLeft: return "";
                case ReportStringId.UD_TTip_AlignVerticalCenters: return "";
                case ReportStringId.UD_TTip_AlignRight: return "";
                case ReportStringId.UD_TTip_AlignTop: return "";
                case ReportStringId.UD_TTip_AlignHorizontalCenters: return "";
                case ReportStringId.UD_TTip_AlignBottom: return "";
                case ReportStringId.UD_TTip_SizeToControlWidth: return "";
                case ReportStringId.UD_TTip_SizeToGrid: return "";
                case ReportStringId.UD_TTip_SizeToControlHeight: return "";
                case ReportStringId.UD_TTip_SizeToControl: return "";
                case ReportStringId.UD_TTip_HorizSpaceMakeEqual: return "";
                case ReportStringId.UD_TTip_HorizSpaceIncrease: return "";
                case ReportStringId.UD_TTip_HorizSpaceDecrease: return "";
                case ReportStringId.UD_TTip_HorizSpaceConcatenate: return "";
                case ReportStringId.UD_TTip_VertSpaceMakeEqual: return "";
                case ReportStringId.UD_TTip_VertSpaceIncrease: return "";
                case ReportStringId.UD_TTip_VertSpaceDecrease: return "";
                case ReportStringId.UD_TTip_VertSpaceConcatenate: return "";
                case ReportStringId.UD_TTip_CenterHorizontally: return "";
                case ReportStringId.UD_TTip_CenterVertically: return "";
                case ReportStringId.UD_TTip_BringToFront: return "";
                case ReportStringId.UD_TTip_SendToBack: return "";
                case ReportStringId.UD_TTip_FormatBold: return "Тод";
                case ReportStringId.UD_TTip_FormatItalic: return "Налуу";
                case ReportStringId.UD_TTip_FormatUnderline: return "Доогуур зураас";
                case ReportStringId.UD_TTip_FormatAlignLeft: return "";
                case ReportStringId.UD_TTip_FormatCenter: return "";
                case ReportStringId.UD_TTip_FormatAlignRight: return "";
                case ReportStringId.UD_TTip_FormatFontName: return "";
                case ReportStringId.UD_TTip_FormatFontSize: return "";
                case ReportStringId.UD_TTip_FormatForeColor: return "";
                case ReportStringId.UD_TTip_FormatBackColor: return "";
                case ReportStringId.UD_TTip_FormatJustify: return "";
                case ReportStringId.UD_TTip_ItemDescription: return "";
                case ReportStringId.UD_TTip_TableDescription: return "";
                case ReportStringId.UD_TTip_DataMemberDescription: return "";
                case ReportStringId.UD_TTip_Undo: return "";
                case ReportStringId.UD_FormCaption: return "";
                //case ReportStringId.VS_XtraReportsToolboxCategoryName: return "";
                case ReportStringId.UD_XtraReportsToolboxCategoryName: return "";
                case ReportStringId.UD_XtraReportsPointerItemCaption: return "";
                #endregion

                #region Verb ------------------------------------------------------------
                case ReportStringId.Verb_EditBands: return "";
                case ReportStringId.Verb_EditGroupFields: return "";
                case ReportStringId.Verb_Import: return "";
                case ReportStringId.Verb_Save: return "";
                case ReportStringId.Verb_About: return "";
                case ReportStringId.Verb_RTFClear: return "";
                case ReportStringId.Verb_RTFLoad: return "";
                case ReportStringId.Verb_FormatString: return "";
                case ReportStringId.Verb_SummaryWizard: return "";
                case ReportStringId.Verb_ReportWizard: return "";
                case ReportStringId.Verb_Insert: return "";
                case ReportStringId.Verb_Delete: return "";
                case ReportStringId.Verb_Bind: return "";
                case ReportStringId.Verb_EditText: return "";
                case ReportStringId.Verb_AddFieldToArea: return "";
                case ReportStringId.Verb_RunDesigner: return "";
                #endregion

                #region FSForm ------------------------------------------------------------
                //case ReportStringId.FSForm_Lbl_Category: return "";
                //case ReportStringId.FSForm_Lbl_Prefix: return "Угтвар";
                //case ReportStringId.FSForm_Lbl_Suffix: return "Дагавар";
                //case ReportStringId.FSForm_Lbl_CustomGeneral: return "";
                //case ReportStringId.FSForm_GrBox_Sample: return "";
                //case ReportStringId.FSForm_Tab_StandardTypes: return "";
                //case ReportStringId.FSForm_Tab_Custom: return "";
                //case ReportStringId.FSForm_Msg_BadSymbol: return "";
                //case ReportStringId.FSForm_Btn_Delete: return "Устгах";
                //case ReportStringId.FSForm_Btn_Ok: return "Тийм";
                //case ReportStringId.FSForm_Btn_Cancel: return "Болих";
                //case ReportStringId.FSForm_Text: return "";
                //case ReportStringId.FSForm_Cat_DateTime: return "";
                //case ReportStringId.FSForm_Cat_Int32: return "";
                //case ReportStringId.FSForm_Cat_Number: return "";
                //case ReportStringId.FSForm_Cat_Percent: return "";
                //case ReportStringId.FSForm_Cat_Currency: return "";
                //case ReportStringId.FSForm_Cat_Special: return "";
                //case ReportStringId.FSForm_Cat_General: return "";
                #endregion

                #region BCForm ------------------------------------------------------------
                case ReportStringId.BCForm_Lbl_Property: return "";
                case ReportStringId.BCForm_Lbl_Binding: return "";
                #endregion

                #region FRSForm ------------------------------------------------------------
                case ReportStringId.FRSForm_Caption: return "";
                case ReportStringId.FRSForm_Msg_NoRuleSelected: return "";
                case ReportStringId.FRSForm_Msg_MoreThanOneRule: return "";
                case ReportStringId.FRSForm_TTip_AddRule: return "";
                case ReportStringId.FRSForm_TTip_RemoveRule: return "";
                case ReportStringId.FRSForm_TTip_ClearRules: return "";
                case ReportStringId.FRSForm_TTip_PurgeRules: return "";
                #endregion

                #region SSForm ------------------------------------------------------------
                case ReportStringId.SSForm_Caption: return "";
                case ReportStringId.SSForm_Btn_Close: return "";
                case ReportStringId.SSForm_Msg_NoStyleSelected: return "";
                case ReportStringId.SSForm_Msg_MoreThanOneStyle: return "";
                case ReportStringId.SSForm_Msg_SelectedStylesText: return "";
                case ReportStringId.SSForm_Msg_StyleSheetError: return "";
                case ReportStringId.SSForm_Msg_InvalidFileFormat: return "";
                case ReportStringId.SSForm_Msg_StyleNamePreviewPostfix: return "";
                case ReportStringId.SSForm_Msg_FileFilter: return "";
                case ReportStringId.SSForm_TTip_AddStyle: return "";
                case ReportStringId.SSForm_TTip_RemoveStyle: return "";
                case ReportStringId.SSForm_TTip_ClearStyles: return "";
                case ReportStringId.SSForm_TTip_PurgeStyles: return "";
                case ReportStringId.SSForm_TTip_SaveStyles: return "";
                case ReportStringId.SSForm_TTip_LoadStyles: return "";
                #endregion

                #region SR ------------------------------------------------------------
                case ReportStringId.SR_Side_Margins: return "";
                case ReportStringId.SR_Top_Margin: return "";
                case ReportStringId.SR_Vertical_Pitch: return "";
                case ReportStringId.SR_Horizontal_Pitch: return "";
                case ReportStringId.SR_Width: return "";
                case ReportStringId.SR_Height: return "";
                case ReportStringId.SR_Number_Down: return "";
                case ReportStringId.SR_Number_Across: return "";
                #endregion

                #region RepTabCtl ------------------------------------------------------------
                case ReportStringId.RepTabCtl_HtmlView: return "";
                case ReportStringId.RepTabCtl_Preview: return "";
                case ReportStringId.RepTabCtl_Designer: return "";
                #endregion

                #region UD ------------------------------------------------------------
                case ReportStringId.UD_Group_File: return "";
                case ReportStringId.UD_Group_Edit: return "";
                case ReportStringId.UD_Group_View: return "";
                case ReportStringId.UD_Group_Format: return "";
                case ReportStringId.UD_Group_Font: return "";
                case ReportStringId.UD_Group_Justify: return "";
                case ReportStringId.UD_Group_Align: return "";
                case ReportStringId.UD_Group_MakeSameSize: return "";
                case ReportStringId.UD_Group_HorizontalSpacing: return "";
                case ReportStringId.UD_Group_VerticalSpacing: return "";
                case ReportStringId.UD_Group_CenterInForm: return "";
                case ReportStringId.UD_Group_Order: return "";
                case ReportStringId.UD_Group_ToolbarsList: return "";
                case ReportStringId.UD_Group_DockPanelsList: return "";
                case ReportStringId.UD_Capt_MainMenuName: return "";
                case ReportStringId.UD_Capt_ToolbarName: return "";
                case ReportStringId.UD_Capt_LayoutToolbarName: return "";
                case ReportStringId.UD_Capt_FormattingToolbarName: return "";
                case ReportStringId.UD_Capt_StatusBarName: return "";
                case ReportStringId.UD_Capt_ZoomToolbarName: return "";
                case ReportStringId.UD_Capt_NewReport: return "";
                case ReportStringId.UD_Capt_NewWizardReport: return "";
                case ReportStringId.UD_Capt_OpenFile: return "";
                case ReportStringId.UD_Capt_SaveFile: return "";
                case ReportStringId.UD_Capt_SaveFileAs: return "";
                case ReportStringId.UD_Capt_Exit: return "";
                case ReportStringId.UD_Capt_Cut: return "";
                case ReportStringId.UD_Capt_Copy: return "";
                case ReportStringId.UD_Capt_Paste: return "";
                case ReportStringId.UD_Capt_Delete: return "";
                case ReportStringId.UD_Capt_SelectAll: return "";
                case ReportStringId.UD_Capt_Undo: return "";
                case ReportStringId.UD_Capt_Redo: return "";
                case ReportStringId.UD_Capt_ForegroundColor: return "";
                case ReportStringId.UD_Capt_BackGroundColor: return "";
                case ReportStringId.UD_Capt_FontBold: return "";
                case ReportStringId.UD_Capt_FontItalic: return "";
                case ReportStringId.UD_Capt_FontUnderline: return "";
                case ReportStringId.UD_Capt_JustifyLeft: return "";
                case ReportStringId.UD_Capt_JustifyCenter: return "";
                case ReportStringId.UD_Capt_JustifyRight: return "";
                case ReportStringId.UD_Capt_JustifyJustify: return "";
                case ReportStringId.UD_Capt_AlignLefts: return "";
                case ReportStringId.UD_Capt_AlignCenters: return "";
                case ReportStringId.UD_Capt_AlignRights: return "";
                case ReportStringId.UD_Capt_AlignTops: return "";
                case ReportStringId.UD_Capt_AlignMiddles: return "";
                case ReportStringId.UD_Capt_AlignBottoms: return "";
                case ReportStringId.UD_Capt_AlignToGrid: return "";
                case ReportStringId.UD_Capt_MakeSameSizeWidth: return "";
                case ReportStringId.UD_Capt_MakeSameSizeSizeToGrid: return "";
                case ReportStringId.UD_Capt_MakeSameSizeHeight: return "";
                case ReportStringId.UD_Capt_MakeSameSizeBoth: return "";
                case ReportStringId.UD_Capt_SpacingMakeEqual: return "";
                case ReportStringId.UD_Capt_SpacingIncrease: return "";
                case ReportStringId.UD_Capt_SpacingDecrease: return "";
                case ReportStringId.UD_Capt_SpacingRemove: return "";
                case ReportStringId.UD_Capt_CenterInFormHorizontally: return "";
                case ReportStringId.UD_Capt_CenterInFormVertically: return "";
                case ReportStringId.UD_Capt_OrderBringToFront: return "";
                case ReportStringId.UD_Capt_OrderSendToBack: return "";
                case ReportStringId.UD_Capt_Zoom: return "";
                case ReportStringId.UD_Capt_ZoomIn: return "";
                case ReportStringId.UD_Capt_ZoomOut: return "";
                case ReportStringId.UD_Capt_ZoomFactor: return "";
                case ReportStringId.UD_Hint_NewReport: return "";
                case ReportStringId.UD_Hint_NewWizardReport: return "";
                case ReportStringId.UD_Hint_OpenFile: return "";
                case ReportStringId.UD_Hint_SaveFile: return "";
                case ReportStringId.UD_Hint_SaveFileAs: return "";
                case ReportStringId.UD_Hint_Exit: return "";
                case ReportStringId.UD_Hint_Cut: return "";
                case ReportStringId.UD_Hint_Copy: return "";
                case ReportStringId.UD_Hint_Paste: return "";
                case ReportStringId.UD_Hint_Delete: return "";
                case ReportStringId.UD_Hint_SelectAll: return "";
                case ReportStringId.UD_Hint_Undo: return "";
                case ReportStringId.UD_Hint_Redo: return "";
                case ReportStringId.UD_Hint_ForegroundColor: return "";
                case ReportStringId.UD_Hint_BackGroundColor: return "";
                case ReportStringId.UD_Hint_FontBold: return "";
                case ReportStringId.UD_Hint_FontItalic: return "";
                case ReportStringId.UD_Hint_FontUnderline: return "";
                case ReportStringId.UD_Hint_JustifyLeft: return "";
                case ReportStringId.UD_Hint_JustifyCenter: return "";
                case ReportStringId.UD_Hint_JustifyRight: return "";
                case ReportStringId.UD_Hint_JustifyJustify: return "";
                case ReportStringId.UD_Hint_AlignLefts: return "";
                case ReportStringId.UD_Hint_AlignCenters: return "";
                case ReportStringId.UD_Hint_AlignRights: return "";
                case ReportStringId.UD_Hint_AlignTops: return "";
                case ReportStringId.UD_Hint_AlignMiddles: return "";
                case ReportStringId.UD_Hint_AlignBottoms: return "";
                case ReportStringId.UD_Hint_AlignToGrid: return "";
                case ReportStringId.UD_Hint_MakeSameSizeWidth: return "";
                case ReportStringId.UD_Hint_MakeSameSizeSizeToGrid: return "";
                case ReportStringId.UD_Hint_MakeSameSizeHeight: return "";
                case ReportStringId.UD_Hint_MakeSameSizeBoth: return "";
                case ReportStringId.UD_Hint_SpacingMakeEqual: return "";
                case ReportStringId.UD_Hint_SpacingIncrease: return "";
                case ReportStringId.UD_Hint_SpacingDecrease: return "";
                case ReportStringId.UD_Hint_SpacingRemove: return "";
                case ReportStringId.UD_Hint_CenterInFormHorizontally: return "";
                case ReportStringId.UD_Hint_CenterInFormVertically: return "";
                case ReportStringId.UD_Hint_OrderBringToFront: return "";
                case ReportStringId.UD_Hint_OrderSendToBack: return "";
                case ReportStringId.UD_Hint_Zoom: return "";
                case ReportStringId.UD_Hint_ZoomIn: return "";
                case ReportStringId.UD_Hint_ZoomOut: return "";
                case ReportStringId.UD_Hint_ViewBars: return "";
                case ReportStringId.UD_Hint_ViewDockPanels: return "";
                case ReportStringId.UD_Hint_ViewTabs: return "";
                case ReportStringId.UD_Title_FieldList: return "";
                case ReportStringId.UD_Title_FieldList_NonePickerNodeText: return "";
                //case ReportStringId.UD_Title_FieldList_NoneNodeText: return "";
                case ReportStringId.UD_Title_FieldList_ProjectObjectsText: return "";
                case ReportStringId.UD_Title_FieldList_AddNewDataSourceText: return "";
                case ReportStringId.UD_Title_GroupAndSort: return "";
                case ReportStringId.UD_Title_ReportExplorer: return "";
                case ReportStringId.UD_Title_PropertyGrid: return "";
                case ReportStringId.UD_Title_ToolBox: return "";
                #endregion

                #region SR ------------------------------------------------------------
                case ReportStringId.STag_Name_DataBinding: return "";
                case ReportStringId.STag_Name_FormatString: return "";
                case ReportStringId.STag_Name_Checked: return "";
                case ReportStringId.STag_Name_PreviewRowCount: return "";
                //case ReportStringId.STag_Name_ShrinkSubReportIconArea: return "";
                case ReportStringId.STag_Name_Bands: return "";
                case ReportStringId.STag_Name_Height: return "";
                case ReportStringId.STag_Name_ColumnMode: return "";
                case ReportStringId.STag_Name_ColumnCount: return "";
                case ReportStringId.STag_Name_ColumnWidth: return "";
                case ReportStringId.STag_Name_ColumnSpacing: return "";
                //case ReportStringId.STag_Name_ColumnDirection: return "";
                case ReportStringId.STag_Name_FieldArea: return "";
                case ReportStringId.STag_Capt_Tasks: return "";
                #endregion

                #region RibbonXRDesign ------------------------------------------------------------
                case ReportStringId.RibbonXRDesign_PageText: return "";
                case ReportStringId.RibbonXRDesign_HtmlPageText: return "";
                case ReportStringId.RibbonXRDesign_PageGroup_Report: return "";
                case ReportStringId.RibbonXRDesign_PageGroup_Edit: return "";
                case ReportStringId.RibbonXRDesign_PageGroup_Font: return "";
                case ReportStringId.RibbonXRDesign_PageGroup_Alignment: return "";
                case ReportStringId.RibbonXRDesign_PageGroup_SizeAndLayout: return "";
                case ReportStringId.RibbonXRDesign_PageGroup_Zoom: return "";
                case ReportStringId.RibbonXRDesign_PageGroup_View: return "";
                case ReportStringId.RibbonXRDesign_PageGroup_HtmlNavigation: return "";
                case ReportStringId.RibbonXRDesign_AlignToGrid_Caption: return "";
                case ReportStringId.RibbonXRDesign_AlignLeft_Caption: return "";
                case ReportStringId.RibbonXRDesign_AlignVerticalCenters_Caption: return "";
                case ReportStringId.RibbonXRDesign_AlignRight_Caption: return "";
                case ReportStringId.RibbonXRDesign_AlignTop_Caption: return "";
                case ReportStringId.RibbonXRDesign_AlignHorizontalCenters_Caption: return "";
                case ReportStringId.RibbonXRDesign_AlignBottom_Caption: return "";
                case ReportStringId.RibbonXRDesign_SizeToControlWidth_Caption: return "";
                case ReportStringId.RibbonXRDesign_SizeToGrid_Caption: return "";
                case ReportStringId.RibbonXRDesign_SizeToControlHeight_Caption: return "";
                case ReportStringId.RibbonXRDesign_SizeToControl_Caption: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceMakeEqual_Caption: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceIncrease_Caption: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceDecrease_Caption: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceConcatenate_Caption: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceMakeEqual_Caption: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceIncrease_Caption: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceDecrease_Caption: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceConcatenate_Caption: return "";
                case ReportStringId.RibbonXRDesign_CenterHorizontally_Caption: return "";
                case ReportStringId.RibbonXRDesign_CenterVertically_Caption: return "";
                case ReportStringId.RibbonXRDesign_BringToFront_Caption: return "";
                case ReportStringId.RibbonXRDesign_SendToBack_Caption: return "";
                case ReportStringId.RibbonXRDesign_FontBold_Caption: return "";
                case ReportStringId.RibbonXRDesign_FontItalic_Caption: return "";
                case ReportStringId.RibbonXRDesign_FontUnderline_Caption: return "";
                case ReportStringId.RibbonXRDesign_ForeColor_Caption: return "";
                case ReportStringId.RibbonXRDesign_BackColor_Caption: return "";
                case ReportStringId.RibbonXRDesign_JustifyLeft_Caption: return "";
                case ReportStringId.RibbonXRDesign_JustifyCenter_Caption: return "";
                case ReportStringId.RibbonXRDesign_JustifyRight_Caption: return "";
                case ReportStringId.RibbonXRDesign_JustifyJustify_Caption: return "";
                case ReportStringId.RibbonXRDesign_NewReport_Caption: return "";
                case ReportStringId.RibbonXRDesign_NewReportWizard_Caption: return "";
                case ReportStringId.RibbonXRDesign_OpenFile_Caption: return "";
                case ReportStringId.RibbonXRDesign_SaveFile_Caption: return "";
                case ReportStringId.RibbonXRDesign_SaveFileAs_Caption: return "";
                case ReportStringId.RibbonXRDesign_Cut_Caption: return "";
                case ReportStringId.RibbonXRDesign_Copy_Caption: return "";
                case ReportStringId.RibbonXRDesign_Paste_Caption: return "";
                case ReportStringId.RibbonXRDesign_Undo_Caption: return "";
                case ReportStringId.RibbonXRDesign_Redo_Caption: return "";
                case ReportStringId.RibbonXRDesign_Zoom_Caption: return "";
                case ReportStringId.RibbonXRDesign_ZoomIn_Caption: return "";
                case ReportStringId.RibbonXRDesign_ZoomOut_Caption: return "";
                case ReportStringId.RibbonXRDesign_ZoomExact_Caption: return "";
                case ReportStringId.RibbonXRDesign_Windows_Caption: return "";
                case ReportStringId.RibbonXRDesign_HtmlHome_Caption: return "";
                case ReportStringId.RibbonXRDesign_HtmlBackward_Caption: return "";
                case ReportStringId.RibbonXRDesign_HtmlForward_Caption: return "";
                case ReportStringId.RibbonXRDesign_HtmlRefresh_Caption: return "";
                case ReportStringId.RibbonXRDesign_HtmlFind_Caption: return "";
                case ReportStringId.RibbonXRDesign_AlignToGrid_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_AlignLeft_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_AlignVerticalCenters_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_AlignRight_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_AlignTop_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_AlignHorizontalCenters_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_AlignBottom_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_SizeToControlWidth_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_SizeToGrid_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_SizeToControlHeight_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_SizeToControl_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceMakeEqual_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceIncrease_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceDecrease_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceConcatenate_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceMakeEqual_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceIncrease_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceDecrease_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceConcatenate_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_CenterHorizontally_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_CenterVertically_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_BringToFront_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_SendToBack_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_FontBold_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_FontItalic_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_FontUnderline_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_ForeColor_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_BackColor_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_JustifyLeft_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_JustifyCenter_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_JustifyRight_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_JustifyJustify_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_NewReport_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_NewReportWizard_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_OpenFile_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_SaveFile_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_SaveFileAs_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_Cut_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_Copy_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_Paste_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_Undo_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_Redo_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_Zoom_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_ZoomIn_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_ZoomOut_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_FontName_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_FontSize_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_Windows_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_HtmlHome_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_HtmlBackward_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_HtmlForward_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_HtmlRefresh_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_HtmlFind_STipTitle: return "";
                case ReportStringId.RibbonXRDesign_AlignToGrid_STipContent: return "";
                case ReportStringId.RibbonXRDesign_AlignLeft_STipContent: return "";
                case ReportStringId.RibbonXRDesign_AlignVerticalCenters_STipContent: return "";
                case ReportStringId.RibbonXRDesign_AlignRight_STipContent: return "";
                case ReportStringId.RibbonXRDesign_AlignTop_STipContent: return "";
                case ReportStringId.RibbonXRDesign_AlignHorizontalCenters_STipContent: return "";
                case ReportStringId.RibbonXRDesign_AlignBottom_STipContent: return "";
                case ReportStringId.RibbonXRDesign_SizeToControlWidth_STipContent: return "";
                case ReportStringId.RibbonXRDesign_SizeToGrid_STipContent: return "";
                case ReportStringId.RibbonXRDesign_SizeToControlHeight_STipContent: return "";
                case ReportStringId.RibbonXRDesign_SizeToControl_STipContent: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceMakeEqual_STipContent: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceIncrease_STipContent: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceDecrease_STipContent: return "";
                case ReportStringId.RibbonXRDesign_HorizSpaceConcatenate_STipContent: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceMakeEqual_STipContent: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceIncrease_STipContent: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceDecrease_STipContent: return "";
                case ReportStringId.RibbonXRDesign_VertSpaceConcatenate_STipContent: return "";
                case ReportStringId.RibbonXRDesign_CenterHorizontally_STipContent: return "";
                case ReportStringId.RibbonXRDesign_CenterVertically_STipContent: return "";
                case ReportStringId.RibbonXRDesign_BringToFront_STipContent: return "";
                case ReportStringId.RibbonXRDesign_SendToBack_STipContent: return "";
                case ReportStringId.RibbonXRDesign_FontBold_STipContent: return "";
                case ReportStringId.RibbonXRDesign_FontItalic_STipContent: return "";
                case ReportStringId.RibbonXRDesign_FontUnderline_STipContent: return "";
                case ReportStringId.RibbonXRDesign_ForeColor_STipContent: return "";
                case ReportStringId.RibbonXRDesign_BackColor_STipContent: return "";
                case ReportStringId.RibbonXRDesign_JustifyLeft_STipContent: return "";
                case ReportStringId.RibbonXRDesign_JustifyCenter_STipContent: return "";
                case ReportStringId.RibbonXRDesign_JustifyRight_STipContent: return "";
                case ReportStringId.RibbonXRDesign_JustifyJustify_STipContent: return "";
                case ReportStringId.RibbonXRDesign_NewReport_STipContent: return "";
                case ReportStringId.RibbonXRDesign_NewReportWizard_STipContent: return "";
                case ReportStringId.RibbonXRDesign_OpenFile_STipContent: return "";
                case ReportStringId.RibbonXRDesign_SaveFile_STipContent: return "";
                case ReportStringId.RibbonXRDesign_SaveFileAs_STipContent: return "";
                case ReportStringId.RibbonXRDesign_Cut_STipContent: return "";
                case ReportStringId.RibbonXRDesign_Copy_STipContent: return "";
                case ReportStringId.RibbonXRDesign_Paste_STipContent: return "";
                case ReportStringId.RibbonXRDesign_Undo_STipContent: return "";
                case ReportStringId.RibbonXRDesign_Redo_STipContent: return "";
                case ReportStringId.RibbonXRDesign_Zoom_STipContent: return "";
                case ReportStringId.RibbonXRDesign_ZoomIn_STipContent: return "";
                case ReportStringId.RibbonXRDesign_ZoomOut_STipContent: return "";
                case ReportStringId.RibbonXRDesign_FontName_STipContent: return "";
                case ReportStringId.RibbonXRDesign_FontSize_STipContent: return "";
                case ReportStringId.RibbonXRDesign_Windows_STipContent: return "";
                case ReportStringId.RibbonXRDesign_HtmlHome_STipContent: return "";
                case ReportStringId.RibbonXRDesign_HtmlBackward_STipContent: return "";
                case ReportStringId.RibbonXRDesign_HtmlForward_STipContent: return "";
                case ReportStringId.RibbonXRDesign_HtmlRefresh_STipContent: return "";
                case ReportStringId.RibbonXRDesign_HtmlFind_STipContent: return "";
                #endregion

                #region XRSubreport ------------------------------------------------------------
                case ReportStringId.XRSubreport_NameInfo: return "";
                case ReportStringId.XRSubreport_NullReportSourceInfo: return "";
                case ReportStringId.XRSubreport_ReportSourceInfo: return "";
                #endregion

                #region PivotGridForm ------------------------------------------------------------
                case ReportStringId.PivotGridForm_GroupMain_Caption: return "";
                case ReportStringId.PivotGridForm_GroupMain_Description: return "";
                case ReportStringId.PivotGridForm_ItemFields_Caption: return "";
                case ReportStringId.PivotGridForm_ItemFields_Description: return "";
                case ReportStringId.PivotGridForm_ItemLayout_Caption: return "";
                case ReportStringId.PivotGridForm_ItemLayout_Description: return "";
                case ReportStringId.PivotGridForm_GroupPrinting_Caption: return "";
                case ReportStringId.PivotGridForm_GroupPrinting_Description: return "";
                case ReportStringId.PivotGridForm_ItemAppearances_Caption: return "";
                case ReportStringId.PivotGridForm_ItemAppearances_Description: return "";
                case ReportStringId.PivotGridForm_ItemSettings_Caption: return "";
                case ReportStringId.PivotGridForm_ItemSettings_Description: return "";
                case ReportStringId.PivotGridFrame_Fields_ColumnsText: return "";
                case ReportStringId.PivotGridFrame_Fields_DescriptionText1: return "";
                case ReportStringId.PivotGridFrame_Fields_DescriptionText2: return "";
                case ReportStringId.PivotGridFrame_Layouts_DescriptionText: return "";
                case ReportStringId.PivotGridFrame_Layouts_SelectorCaption1: return "";
                case ReportStringId.PivotGridFrame_Layouts_SelectorCaption2: return "";
                case ReportStringId.PivotGridFrame_Appearances_DescriptionText: return "";
                #endregion

                #region ParametersRequest ------------------------------------------------------------
                //case ReportStringId.ParametersRequest_Submit: return "Илгээх";
                //case ReportStringId.ParametersRequest_Reset: return "Дахин";
                //case ReportStringId.ParametersRequest_Caption: return "Хүсэлт";
                #endregion

                #region PropGrid ------------------------------------------------------------
                //case ReportStringId.PropGrid_TTip_Alphabetical: return "";
                //case ReportStringId.PropGrid_TTip_Categorized: return "";
                #endregion

                #region GroupSort ------------------------------------------------------------
                case ReportStringId.GroupSort_AddGroup: return "Бүлэг нэмэх";
                case ReportStringId.GroupSort_AddSort: return "Эрэмбэлэлт нэмэх";
                case ReportStringId.GroupSort_MoveUp: return "Дээшээ зөөх";
                case ReportStringId.GroupSort_MoveDown: return "Доошоо зөөх";
                case ReportStringId.GroupSort_Delete: return "Устгах";
                #endregion

                default:
                    ret = "";
                    break;
            }
            return ret;
        }
    }
}
