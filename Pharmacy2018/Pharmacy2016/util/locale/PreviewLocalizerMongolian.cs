﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraPrinting.Localization;

namespace Pharmacy2016.util.locale
{
    class PreviewLocalizerMongolian : PreviewLocalizer
    {
        public override string Language { get { return "Mongolian"; } }
        public override string GetLocalizedString(PreviewStringId id)
        {
            string ret = "";
            switch (id)
            {

                #region ---------------------------------------------------------------
                //case PreviewStringId.EmptyString: return ""; // ""
                case PreviewStringId.Button_Cancel: return "Болих"; // Cancel
                case PreviewStringId.Button_Ok: return "Тийм"; // OK
                case PreviewStringId.Button_Help: return "Тусламж"; // Help
                case PreviewStringId.Button_Apply: return "Хэрэглэх"; // Apply
                case PreviewStringId.PreviewForm_Caption: return "Хэвлэхийн өмнө урьдчилан харах цонх";
                case PreviewStringId.EMail_From: return "Е-Майл"; // From
                case PreviewStringId.BarText_Toolbar: return "Хэрэгсэлийн мөр"; // Toolbar
                case PreviewStringId.BarText_MainMenu: return "Үндсэн цэс"; // Main Menu
                case PreviewStringId.BarText_StatusBar: return "Төлвийн мөр"; // Status Bar
                case PreviewStringId.ScrollingInfo_Page: return "Хуудас"; // Page
                #endregion   

                #region TB ------------------------------------------------------------
                case PreviewStringId.TB_TTip_Customize: return "Дурын"; // Customize
                case PreviewStringId.TB_TTip_Print: return "Хэвлэх"; // Print
                case PreviewStringId.TB_TTip_PrintDirect: return "Шууд хэвлэх"; // Quick Print
                case PreviewStringId.TB_TTip_PageSetup: return "Хуудасны тохиргоо"; // Page Setup
                case PreviewStringId.TB_TTip_Magnifier: return "Томруулагч"; // Magnifier
                case PreviewStringId.TB_TTip_ZoomIn: return "Томруулалтыг нэмэгдүүлэх"; // Zoom In
                case PreviewStringId.TB_TTip_ZoomOut: return "Томруулалтыг хорогдуулах"; // Zoom Out
                case PreviewStringId.TB_TTip_Zoom: return "Харагдах томруулалт"; // Zoom
                case PreviewStringId.TB_TTip_Search: return "Хайлт"; // Search
                case PreviewStringId.TB_TTip_FirstPage: return "Эхний хуудас"; // First Page
                case PreviewStringId.TB_TTip_PreviousPage: return "Өмнөх хуудас"; // Previous Page
                case PreviewStringId.TB_TTip_NextPage: return "Дараагийн хуудас"; // Next Page
                case PreviewStringId.TB_TTip_LastPage: return "Сүүлийн хуудас"; // Last Page
                case PreviewStringId.TB_TTip_MultiplePages: return "Олон хуудас"; // Multiple Pages
                case PreviewStringId.TB_TTip_Backgr: return "Дэвсгэр өнгө"; // Background
                case PreviewStringId.TB_TTip_Close: return "Хаах"; // Close Preview
                case PreviewStringId.TB_TTip_EditPageHF: return "Толгой ба Хөл"; // Header And Footer
                case PreviewStringId.TB_TTip_HandTool: return "Гар хэрэгсэл"; // Hand Tool
                case PreviewStringId.TB_TTip_Export: return "Экспортлох"; // Export Document...
                case PreviewStringId.TB_TTip_Send: return "Email-ээр илгээх"; // Send via E-Mail...
                case PreviewStringId.TB_TTip_Map: return "Хавчуурга"; // Document Map
                case PreviewStringId.TB_TTip_Parameters: return ""; // Parameters
                case PreviewStringId.TB_TTip_Watermark: return "Бичиг баримтыг тамгалах"; // Watermark
                case PreviewStringId.TB_TTip_Scale: return "Масштаб"; // Scale
                case PreviewStringId.TB_TTip_Open: return "Нээх"; // Open a document
                case PreviewStringId.TB_TTip_Save: return "Хадгалах"; // Save the document
                #endregion

                #region SB ------------------------------------------------------------
                case PreviewStringId.SB_PageOfPages: return "Нийт {1} хуудасны {0}-р хуудас"; // Page {0} of {1}
                case PreviewStringId.SB_ZoomFactor: return "Томруулагч:"; // Zoom Factor: 
                case PreviewStringId.SB_PageNone: return "Хуудас байхгүй"; // none
                case PreviewStringId.SB_PageInfo: return "{1} хуудасны {0}-р хуудас"; // {0} of {1}
                case PreviewStringId.SB_TTip_Stop: return "Зогс"; // Stop
                #endregion

                #region Msg -----------------------------------------------------------
                case PreviewStringId.MPForm_Lbl_Pages: return "";
                case PreviewStringId.Msg_EmptyDocument: return "Тайланг хэвлэхийн өмнө урьдчилан харах цонх"; // The document does not contain any pages.
                case PreviewStringId.Msg_CreatingDocument: return "Тайлан үүсгэж байна..."; // Creating the document...
                case PreviewStringId.Msg_UnavailableNetPrinter: return "Сүлжээгээр хэвлэх боломжгүй бйана."; // The net printer is unavailable.
                case PreviewStringId.Msg_NeedPrinter: return "Таны компьютер принтергүй байна."; // No printers installed.
                case PreviewStringId.Msg_WrongPrinter: return "Принтерийн нэр буруу байна. Принтерийн тохиргоог шалгана уу!"; // The printer name is invalid. Please check the printer settings.
                case PreviewStringId.Msg_WrongPrinting: return "Тайланг хэвлэх үед алдаа гарлаа."; // An error occurred during printing a document.
                case PreviewStringId.Msg_WrongPageSettings: return "Одоогийн принтер сонгосон хуудасны хэмжээг дэмжихгүй байна. Та өөр принтерээр хэвлэх үү?"; // The current printer doesn't support the selected paper size. Proceed with printing anyway?
                case PreviewStringId.Msg_CustomDrawWarning: return "Анхааруулга!"; // Warning!
                case PreviewStringId.Msg_PageMarginsWarning: return "Хуудасны ирмэгийн хэмжээ хуудасны хэвлэгдэх боломжтой бүсээс хэтэрсэн байна. Үргэлжлүүлэх үү?"; // One or more margins are set outside the printable area of the page. Continue?
                case PreviewStringId.Msg_IncorrectPageRange: return "Хуудасны хязгаар буруу байна."; // This is not a valid page range
                case PreviewStringId.Msg_FontInvalidNumber: return "Үсгийн фонтын хэмжээ нь тэг эсвэл хасах тэмдэгтэй тоо байж болохгүй."; // The font size cannot be set to zero or a negative number
                case PreviewStringId.Msg_NotSupportedFont: return "Дэмждэггүй фонт."; // This font is not yet supported
                case PreviewStringId.Msg_IncorrectZoomFactor: return "Томруулалтын хэмжээ {0} - {1} хооронд утга авах боломжтой."; // The number must be beetween {0} and {1}.
                case PreviewStringId.Msg_InvalidMeasurement: return "Хэмжээ буруу байна."; // This is not a valid measurement.
                case PreviewStringId.Msg_CannotAccessFile: return "\"{0}\" файлыг өөр програм ашиглаж байгаа тул хандах боломжгүй байна."; // The process cannot access the file "{0}" because it is being used by another process.
                case PreviewStringId.Msg_FileReadOnly: return "{0} файл нь зөвхөн унших боломжтой байна. Та өөр файл нээх боломжтой."; // File "{0}" is set to read-only, try again with a different file name.
                case PreviewStringId.Msg_OpenFileQuestion: return "Энэ файлыг нээх үү?"; // Do you want to open this file?
                case PreviewStringId.Msg_OpenFileQuestionCaption: return "Экпорт"; // Export
                case PreviewStringId.Msg_CantFitBarcodeToControlBounds: return "BARCODE-ийн контролын хязгаар хэтэрхий бага байна."; // Control's boundaries are too small for the barcode
                case PreviewStringId.Msg_InvalidBarcodeText: return "Текстийн тэмдэгт алдаатай байна."; // There are invalid characters in the text
                case PreviewStringId.Msg_InvalidBarcodeTextFormat: return "Текстийн хэвжүүлэлт алдаатай байна."; // Invalid text format
                //case PreviewStringId.Msg_SearchDialogFinishedSearching: return "Тайлангаас хайлт хийж дууслаа."; // Finished searching through the document
                //case PreviewStringId.Msg_SearchDialogTotalFound: return "Олдсон тоо:"; // Total found: 
                //case PreviewStringId.Msg_SearchDialogReady: return "Хайхад бэлэн."; // Ready
                case PreviewStringId.Msg_NoDifferentFilesInStream: return "Тайланг 'Ялгаатай файлууд' төлвөөр экспортлож чадсангүй. 'Нэг файл' эсвэл 'Хуудаслагдсан нэг файл' төлвүүдийг хэрэглэнэ үү."; // A document can't be exported to a stream in the DifferentFiles mode. Use the SingleFile or SingleFilePageByPage mode instead.
                case PreviewStringId.Msg_BigFileToCreate: return "Файлын хэмжээ хэтэрхий том байна. Хуудасны тоог багасгах эсвэл хуудаснуудыг ижилхэн хэвжүүлэлтээр тус тусад нь экспортлох боломжтой."; // The output file is too big. Try to reduce the number of its pages, or split it into several documents.
                case PreviewStringId.Msg_BigFileToCreateJPEG: return "JPG файлын хэмжээ хэтэрхий том байна. Өөр зургийн хэвжүүлэлт сонгох эсвэл өөр файлаар экспортлох боломжтой."; // The output file is too big to create a JPEG file. Please choose another image format or another export mode.
                case PreviewStringId.Msg_BigBitmapToCreate: return "Файлын хэмжээ хэтэрхий том байна. Зургийн нарийвчлалыг багасгах эсвэл өөр файлаар экспортлох боломжтой."; // The output file is too big. Please try to reduce the image resolution, or choose another export mode.
                case PreviewStringId.Msg_XlsMoreThanMaxRows: return "Excel файл үүсгэхэд алдаа гарлаа. Мөрийн тоо 65536-с их байна. Өөр файлруу экспортлох эсвэл мөрийн тоог багасгаж дахин XLS файлруу экспортлох боломжтой."; // The created XLS file is too big for the XLS format, because it contains more than 65536 rows. Please try to reduce the amount of rows in your report and export the report to XLS again.
                case PreviewStringId.Msg_XlsxMoreThanMaxRows: return "Excel файл үүсгэхэд алдаа гарлаа. Мөрийн тоо 65536-с их байна. Өөр файлруу экспортлох эсвэл мөрийн тоог багасгаж дахин XLSX файлруу экспортлох боломжтой."; // The created XLSX file is too big for the XLSX format, because it contains more than 65536 rows. Please try to reduce the amount of rows in your report and export the report to XLSX again.
                case PreviewStringId.Msg_XlsMoreThanMaxColumns: return "Excel файл үүсгэхэд алдаа гарлаа. Баганын тоо 256-с их байна. Өөр файлруу экспортлох эсвэл баганын тоог багасгаж дахин XLS файлруу экспортлох боломжтой."; // The created XLS file is too big for the XLS format, because it contains more than 256 columns. Please try to reduce the amount of columns in your report and export the report to XLS again.
                case PreviewStringId.Msg_XlsxMoreThanMaxColumns: return "Excel файл үүсгэхэд алдаа гарлаа. Баганын тоо 256-с их байна. Өөр файлруу экспортлох эсвэл баганын тоог багасгаж дахин XLSX файлруу экспортлох боломжтой."; // The created XLSX file is too big for the XLSX format, because it contains more than 256 columns. Please try to reduce the amount of columns in your report and export the report to XLSX again.
                //case PreviewStringId.Msg_FileDosntHavePrnxExtention: return "Сонгосон файл PRNX өртгөтгөлгүй байна. Та өөр файл сонгох уу?"; // The specified file doesn't have a PRNX extension. Proceed anyway?
                //case PreviewStringId.Msg_FileDosntContainValidXml: return "PRNX файлын XML нь алдаатай байгаа учраас ачаалах үйлдэл зогслоо."; // The specified file doesn't contain valid XML data in the PRNX format. Loading is stopped.
                case PreviewStringId.Msg_Caption: return "Мэдээлэл"; // XtraPrinting
                #endregion

                #region Margin
                case PreviewStringId.Margin_Inch: return "Инч";
                case PreviewStringId.Margin_Millimeter: return "Миллиметр";
                case PreviewStringId.Margin_TopMargin: return "Дээд ирмэг";
                case PreviewStringId.Margin_BottomMargin: return "Доод ирмэг";
                case PreviewStringId.Margin_LeftMargin: return "Зүүн ирмэг";
                case PreviewStringId.Margin_RightMargin: return "Баруун ирмэг";
                #endregion

                #region Shapes
                case PreviewStringId.Shapes_Rectangle: return "Тэгш өнцөгт";
                case PreviewStringId.Shapes_Ellipse: return "Эллипс";
                case PreviewStringId.Shapes_Arrow: return "Сум";
                case PreviewStringId.Shapes_TopArrow: return "Дээд сум";
                case PreviewStringId.Shapes_BottomArrow: return "Доод сум";
                case PreviewStringId.Shapes_LeftArrow: return "Зүүн сум";
                case PreviewStringId.Shapes_RightArrow: return "Баруун сум";
                case PreviewStringId.Shapes_Polygon: return "Олон өнцөгт";
                case PreviewStringId.Shapes_Triangle: return "Гурван өнцөгт";
                case PreviewStringId.Shapes_Square: return "Дөрвөн өнцөгт";
                case PreviewStringId.Shapes_Pentagon: return "Таван өнцөгт";
                case PreviewStringId.Shapes_Hexagon: return "Зургаан өнцөгт";
                case PreviewStringId.Shapes_Octagon: return "Найман өнцөгт";
                case PreviewStringId.Shapes_Star: return "Од";
                case PreviewStringId.Shapes_ThreePointStar: return "Гурван өнцөгт од";
                case PreviewStringId.Shapes_FourPointStar: return "Дөрвөн өнцөгт од";
                case PreviewStringId.Shapes_FivePointStar: return "Таван өнцөгт од";
                case PreviewStringId.Shapes_SixPointStar: return "Зургаан өнцөгт од";
                case PreviewStringId.Shapes_EightPointStar: return "Найман өнцөгт од";
                case PreviewStringId.Shapes_Line: return "Шугам";
                case PreviewStringId.Shapes_SlantLine: return "Ташуу шугам";
                case PreviewStringId.Shapes_BackslantLine: return "Буруу ташуу шугам";
                case PreviewStringId.Shapes_HorizontalLine: return "Хөндлөн шугам";
                case PreviewStringId.Shapes_VerticalLine: return "Босоо шугам";
                case PreviewStringId.Shapes_Cross: return "Хэрээс, огтлол, муруй";
                case PreviewStringId.Shapes_Brace: return "Хашилт";
                case PreviewStringId.Shapes_Bracket: return "Хаалт";
                #endregion

                #region WMForm
                case PreviewStringId.WMForm_PictureDlg_Title: return "Зураг"; // Select Picture
                case PreviewStringId.WMForm_ImageStretch: return "Хуудсыг дүүргэж харуулах"; // Stretch
                case PreviewStringId.WMForm_ImageClip: return "Зургийн хэмжээгээр харуулах"; // Clip
                case PreviewStringId.WMForm_ImageZoom: return "Хуудсанд тааруулж харуулах"; // Zoom
                case PreviewStringId.WMForm_Watermark_Asap: return "ШУУРХАЙ МАТЕРИАЛ"; // ASAP
                case PreviewStringId.WMForm_Watermark_Confidential: return "НУУЦ МАТЕРИАЛ"; // CONFIDENTIAL - ИТГЭМЖЛЭГДСЭН
                case PreviewStringId.WMForm_Watermark_Copy: return "ХУУЛБАР"; // COPY
                case PreviewStringId.WMForm_Watermark_DoNotCopy: return "ХУУЛБАРЛАХ ХОРИОТОЙ"; // DO NOT COPY
                case PreviewStringId.WMForm_Watermark_Draft: return "НООРОГ"; // DRAFT
                case PreviewStringId.WMForm_Watermark_Evaluation: return "ТОДОРХОЙЛОЛТ"; // EVALUATION
                case PreviewStringId.WMForm_Watermark_Original: return "ЭХ ХУВЬ"; // ORIGINAL
                case PreviewStringId.WMForm_Watermark_Personal: return "ХУВИЙН"; // PERSONAL
                case PreviewStringId.WMForm_Watermark_Sample: return "ЖИШЭЭ"; // SAMPLE
                case PreviewStringId.WMForm_Watermark_TopSecret: return "МАШ НУУЦ МАТЕРИАЛ"; // TOP SECRET
                case PreviewStringId.WMForm_Watermark_Urgent: return "ЧУХАЛ МАТЕРИАЛ"; // URGENT
                case PreviewStringId.WMForm_Direction_Horizontal: return "Хэвтээ"; // Horizontal
                case PreviewStringId.WMForm_Direction_Vertical: return "Босоо"; // Vertical
                case PreviewStringId.WMForm_Direction_BackwardDiagonal: return "Баруун налуу"; // BackwardDiagonal
                case PreviewStringId.WMForm_Direction_ForwardDiagonal: return "Зүүн налуу"; // ForwardDiagonal
                case PreviewStringId.WMForm_VertAlign_Bottom: return "Доод"; // Bottom
                case PreviewStringId.WMForm_VertAlign_Middle: return "Дунд"; // Middle
                case PreviewStringId.WMForm_VertAlign_Top: return "Дээд"; // Top
                case PreviewStringId.WMForm_HorzAlign_Left: return "Зүүн"; // Left
                case PreviewStringId.WMForm_HorzAlign_Center: return "Төв"; // Center
                case PreviewStringId.WMForm_HorzAlign_Right: return "Баруун"; // Right
                //case PreviewStringId.WMForm_ZOrderRgrItem_InFront: return "Нүүр талд"; // In &front
                //case PreviewStringId.WMForm_ZOrderRgrItem_Behind: return "Ар талд"; // &Behind
                //case PreviewStringId.WMForm_PageRangeRgrItem_All: return "Бүгд"; // &All
                //case PreviewStringId.WMForm_PageRangeRgrItem_Pages: return "Нүүр:"; // &Pages:
                #endregion

                #region SaveDlg
                case PreviewStringId.SaveDlg_Title: return "Хадгалах"; // Save As
                case PreviewStringId.SaveDlg_FilterPdf: return "PDF файл"; // PDF Document
                case PreviewStringId.SaveDlg_FilterHtm: return "HTML файл"; // HTML Document
                case PreviewStringId.SaveDlg_FilterMht: return "MHT файл"; // MHT Document
                case PreviewStringId.SaveDlg_FilterRtf: return "Rich Text Format файл"; // Rich Text Document
                case PreviewStringId.SaveDlg_FilterXls: return "Excel файл"; // Excel Document
                case PreviewStringId.SaveDlg_FilterXlsx: return "Excel файл"; // Excel Document
                case PreviewStringId.SaveDlg_FilterCsv: return "CSV файл"; // CSV Document
                case PreviewStringId.SaveDlg_FilterTxt: return "Text файл"; // Text Document
                case PreviewStringId.SaveDlg_FilterBmp: return "BMP Bitmap Format файл"; // BMP Bitmap Format
                case PreviewStringId.SaveDlg_FilterGif: return "GIF Graphics Interchange Format файл"; // GIF Graphics Interchange Format
                case PreviewStringId.SaveDlg_FilterJpeg: return "JPEG File Interchange Format файл"; // JPEG File Interchange Format
                case PreviewStringId.SaveDlg_FilterPng: return "PNG Portable Network Graphics Format файл"; // PNG Portable Network Graphics Format
                case PreviewStringId.SaveDlg_FilterTiff: return "TIFF Tag Image File Format файл"; // TIFF Tag Image File Format
                case PreviewStringId.SaveDlg_FilterEmf: return "EMF Enhanced Windows Metafile файл"; // EMF Enhanced Windows Metafile
                case PreviewStringId.SaveDlg_FilterWmf: return "WMF Windows Metafile файл"; // WMF Windows Metafile
                case PreviewStringId.SaveDlg_FilterNativeFormat: return "Тайлан файл"; // Native Format
                #endregion

                #region MenuItem
                case PreviewStringId.MenuItem_File: return "Файл";
                case PreviewStringId.MenuItem_View: return "Харах";
                case PreviewStringId.MenuItem_Background: return "Дэсвгэр";
                case PreviewStringId.MenuItem_PageSetup: return "Хуудасны тохиргоо";
                case PreviewStringId.MenuItem_Print: return "Хэвлэх";
                case PreviewStringId.MenuItem_PrintDirect: return "Шууд хэвлэх";
                case PreviewStringId.MenuItem_Export: return "Экпортлох";
                case PreviewStringId.MenuItem_Send: return "Еmail-ээр илгээх";
                case PreviewStringId.MenuItem_Exit: return "Гарах";
                case PreviewStringId.MenuItem_ViewToolbar: return "Хэрэгсэлийн мөр";
                case PreviewStringId.MenuItem_ViewStatusbar: return "Төлвийн мөр";
                case PreviewStringId.MenuItem_ViewContinuous: return "Үргэлжлүүлэн харах";
                case PreviewStringId.MenuItem_ViewFacing: return "Хуудас хуудсаар харах";
                case PreviewStringId.MenuItem_BackgrColor: return "Дэвсгэр өнгө";
                case PreviewStringId.MenuItem_Watermark: return "Тамга";
                case PreviewStringId.MenuItem_ZoomPageWidth: return "Хуудасны өргөнөөр";
                case PreviewStringId.MenuItem_ZoomTextWidth: return "Текстийн өргөнөөр";
                case PreviewStringId.MenuItem_ZoomTwoPages: return "Хоёр хуудсаар";
                case PreviewStringId.MenuItem_ZoomWholePage: return "Хуудсыг бүтнээр";

                case PreviewStringId.MenuItem_PdfDocument: return "PDF файл";
                case PreviewStringId.MenuItem_PageLayout: return "Хуудасны байрлал";
                case PreviewStringId.MenuItem_TxtDocument: return "TXT файл";
                case PreviewStringId.MenuItem_GraphicDocument: return "Зурган файл";
                case PreviewStringId.MenuItem_CsvDocument: return "CSV файл";
                case PreviewStringId.MenuItem_MhtDocument: return "MHT файл";
                case PreviewStringId.MenuItem_XlsDocument: return "Excel файл";
                case PreviewStringId.MenuItem_XlsxDocument: return "Excel файл";
                case PreviewStringId.MenuItem_RtfDocument: return "RTF файл";
                case PreviewStringId.MenuItem_HtmDocument: return "HTML файл";
                #endregion

                #region PageInfo
                case PreviewStringId.PageInfo_PageNumber: return "Хуудасны тоо";
                case PreviewStringId.PageInfo_PageNumberOfTotal: return "Нийт тоо";
                case PreviewStringId.PageInfo_PageDate: return "Огноо";
                case PreviewStringId.PageInfo_PageTime: return "Цаг";
                case PreviewStringId.PageInfo_PageUserName: return "Хэрэглэгчийн нэр";
                #endregion

                #region ScalePopup
                //case PreviewStringId.ScalePopup_GroupText: return "Масштабын хэлбэр";
                case PreviewStringId.ScalePopup_AdjustTo: return "Хуудасны хэмжээг";
                case PreviewStringId.ScalePopup_NormalSize: return "хувиар масштаблах";
                case PreviewStringId.ScalePopup_FitTo: return "Хуудасны хэмжээг";
                case PreviewStringId.ScalePopup_PagesWide: return "хуудасаар масштаблах";
                #endregion

                #region ExportOption
                case PreviewStringId.ExportOption_PdfPageRange: return "Хуудасны завсар:";
                case PreviewStringId.ExportOption_PdfCompressed: return "Шахсан PDF файл";
                case PreviewStringId.ExportOption_PdfShowPrintDialogOnOpen: return "Хэвлэх диалогийг харуулах";
                case PreviewStringId.ExportOption_PdfNeverEmbeddedFonts: return "Эдгээр фонтуудыг хэрэглэхгүй";
                case PreviewStringId.ExportOption_PdfImageQuality: return "Зургийн чанар:";
                case PreviewStringId.ExportOption_PdfImageQuality_Lowest: return "Маш муу";
                case PreviewStringId.ExportOption_PdfImageQuality_Low: return "Муу";
                case PreviewStringId.ExportOption_PdfImageQuality_Medium: return "Дундаж";
                case PreviewStringId.ExportOption_PdfImageQuality_High: return "Сайн";
                case PreviewStringId.ExportOption_PdfImageQuality_Highest: return "Маш сайн";
                case PreviewStringId.ExportOption_PdfDocumentAuthor: return "Зохиогч:";
                case PreviewStringId.ExportOption_PdfDocumentApplication: return "Application:";
                case PreviewStringId.ExportOption_PdfDocumentTitle: return "Гарчиг:";
                case PreviewStringId.ExportOption_PdfDocumentSubject: return "Агуулга:";
                case PreviewStringId.ExportOption_PdfDocumentKeywords: return "Түлхүүр үгс:";

                case PreviewStringId.ExportOption_HtmlExportMode: return "Экспортлох төлөв:";
                case PreviewStringId.ExportOption_HtmlExportMode_SingleFile: return "Нэг файл";
                case PreviewStringId.ExportOption_HtmlExportMode_SingleFilePageByPage: return "Хуудаслагдсан нэг файл";
                case PreviewStringId.ExportOption_HtmlExportMode_DifferentFiles: return "Ялгаатай файлууд";                
                case PreviewStringId.ExportOption_HtmlCharacterSet: return "Character set";
                case PreviewStringId.ExportOption_HtmlTitle: return "Гарчиг:";
                case PreviewStringId.ExportOption_HtmlRemoveSecondarySymbols: return "Хэвжүүлэлтийг арилгах";
                case PreviewStringId.ExportOption_HtmlPageRange: return "Хуудасны завсар:";
                case PreviewStringId.ExportOption_HtmlPageBorderWidth: return "Хуудасны хүрээний өргөн:";
                case PreviewStringId.ExportOption_HtmlPageBorderColor: return "Хуудасны хүрээний өнгө:";

                case PreviewStringId.ExportOption_RtfExportMode: return "Экспортлох төлөв:";
                case PreviewStringId.ExportOption_RtfExportMode_SingleFile: return "Нэг файлруу";
                case PreviewStringId.ExportOption_RtfExportMode_SingleFilePageByPage: return "Хуудаслагдсан нэг файлруу";
                case PreviewStringId.ExportOption_RtfPageRange: return "Хуудасны завсар:";
                case PreviewStringId.ExportOption_RtfExportWatermarks: return "Тамгатай экспорлох";

                case PreviewStringId.ExportOption_TextSeparator: return "Тусгаарлагч:";
                case PreviewStringId.ExportOption_TextSeparator_TabAlias: return "";
                case PreviewStringId.ExportOption_TextEncoding: return "Шифрлэлт:";
                case PreviewStringId.ExportOption_TextQuoteStringsWithSeparators: return "Хашилтаар тусгаарлагдах";

                case PreviewStringId.ExportOption_XlsShowGridLines: return "Хүснэгтийн зураасыг харуулах";
                case PreviewStringId.ExportOption_XlsUseNativeFormat: return "Одоогийн хэвжүүлэлтэйгээр экспортлох";
                case PreviewStringId.ExportOption_XlsExportHyperlinks: return "Холбоосийг экспортлох";
                case PreviewStringId.ExportOption_XlsSheetName: return "Sheet-ийн нэр:";

                case PreviewStringId.ExportOption_ImageExportMode: return "Экспортлох төлөв:";
                case PreviewStringId.ExportOption_ImageExportMode_SingleFile: return "Нэг файлруу";
                case PreviewStringId.ExportOption_ImageExportMode_SingleFilePageByPage: return "Хуудаслагдсан нэг файлруу";
                case PreviewStringId.ExportOption_ImageExportMode_DifferentFiles: return "Ялгаатай файлуудруу";
                case PreviewStringId.ExportOption_ImagePageRange: return "Хуудасны завсар:";
                case PreviewStringId.ExportOption_ImagePageBorderWidth: return "Хуудасны хүрээний өргөн:";
                case PreviewStringId.ExportOption_ImagePageBorderColor: return "Хуудасны хүрээний өнгө:";
                case PreviewStringId.ExportOption_ImageFormat: return "Зургийн хэвжүүлэлт:";
                case PreviewStringId.ExportOption_ImageResolution: return "Нарийвчлал(dpi):";

                case PreviewStringId.ExportOption_NativeFormatCompressed: return "Шахсан";
                case PreviewStringId.FolderBrowseDlg_ExportDirectory: return "Экпортлох зам:"; // Select a folder to save the exported document to:

                case PreviewStringId.ExportOptionsForm_CaptionPdf: return "PDF файлруу экпортлох";
                case PreviewStringId.ExportOptionsForm_CaptionXlsx: return "Excel файлруу экпортлох";
                case PreviewStringId.ExportOptionsForm_CaptionXls: return "Excel файлруу экпортлох";
                case PreviewStringId.ExportOptionsForm_CaptionTxt: return "TXT файлруу экпортлох";
                case PreviewStringId.ExportOptionsForm_CaptionCsv: return "CSV файлруу экпортлох";
                case PreviewStringId.ExportOptionsForm_CaptionImage: return "Зурган файлруу экпортлох";
                case PreviewStringId.ExportOptionsForm_CaptionHtml: return "HTML файлруу экпортлох";
                case PreviewStringId.ExportOptionsForm_CaptionMht: return "MHT файлруу экпортлох";
                case PreviewStringId.ExportOptionsForm_CaptionRtf: return "RTF файлруу экпортлох";
                case PreviewStringId.ExportOptionsForm_CaptionNativeOptions: return "Экспортлох";
                #endregion

                #region RibbonPreview
                case PreviewStringId.RibbonPreview_PageText: return "Хэвлэхийн өмнө урьчилан харах цонх"; // Print Preview
                case PreviewStringId.RibbonPreview_PageGroup_Print: return "Хэвлэх"; // Print
                case PreviewStringId.RibbonPreview_PageGroup_PageSetup: return "Хуудасны тохиргоо"; // Page Setup
                case PreviewStringId.RibbonPreview_PageGroup_PageSetup_STipTitle: return "Хуудасны тохиргоо"; // Page Setup
                case PreviewStringId.RibbonPreview_PageGroup_PageSetup_STipContent: return "Хуудасны тохиргоо хийх диалог цонхыг харуулна. Уг цонхон дээр тайлангийн хуудасны тохиргоог өөрчлөх боломжтой."; // Show the Page Setup dialog.
                case PreviewStringId.RibbonPreview_PageGroup_Navigation: return "Удирдлага"; // Navigation
                case PreviewStringId.RibbonPreview_PageGroup_Zoom: return "Томруулалт"; // Zoom
                case PreviewStringId.RibbonPreview_PageGroup_Background: return "Дэвсгэр"; // Page Background
                case PreviewStringId.RibbonPreview_PageGroup_Export: return "Экспорт"; // Export
                case PreviewStringId.RibbonPreview_PageGroup_Document: return "Тайлан"; // Document

                case PreviewStringId.RibbonPreview_DocumentMap_Caption: return "Хавчуурга"; // Bookmarks
                case PreviewStringId.RibbonPreview_DocumentMap_STipTitle: return "Хавчуурга"; // Document Map
                case PreviewStringId.RibbonPreview_DocumentMap_STipContent: return "Тайланд хавчуурга ашиглах бөгөөд тайлангийн бүтцийг удирдах боломжтой болно."; // Open the Document Map, which allows you to navigate through a structural view of the document.
                case PreviewStringId.RibbonPreview_Parameters_Caption: return "Параметрүүд"; // Parameters
                case PreviewStringId.RibbonPreview_Parameters_STipTitle: return "Параметрүүд"; // Parameters
                case PreviewStringId.RibbonPreview_Parameters_STipContent: return "Параметрүүд цонхыг нээх ба тайланруу утга дамжуулах боломжтой болно."; // Open the Parameters pane, which allows you to enter values for report parameters.
                case PreviewStringId.RibbonPreview_Find_Caption: return "Хайлт"; // Find
                case PreviewStringId.RibbonPreview_Find_STipTitle: return "Хайлт (Ctrl+F)"; //Find
                case PreviewStringId.RibbonPreview_Find_STipContent: return "Тайлангаас текст хайж харуулна."; // Show the Find dialog to find text in the document.
                case PreviewStringId.RibbonPreview_Pointer_Caption: return "Заагч"; // Pointer
                case PreviewStringId.RibbonPreview_Pointer_STipTitle: return "Хулганы заагч"; // Mouse Pointer
                case PreviewStringId.RibbonPreview_Pointer_STipContent: return "Хулганы заагчыг харуулана."; // Show the mouse pointer.
                case PreviewStringId.RibbonPreview_HandTool_Caption: return "Гар хэрэгсэл"; // Hand Tool
                case PreviewStringId.RibbonPreview_HandTool_STipTitle: return "Гар хэрэгсэл"; // Hand Tool
                case PreviewStringId.RibbonPreview_HandTool_STipContent: return "Гар хэрэгсэлийг ашиглан scroll-ийг гараар гүйлгэх боломжтой болно."; // Invoke the Hand tool to manually scroll through pages.
                case PreviewStringId.RibbonPreview_Customize_Caption: return "Тохиргоо"; // Options
                case PreviewStringId.RibbonPreview_Customize_STipTitle: return "Тохиргоо"; // Options
                case PreviewStringId.RibbonPreview_Customize_STipContent: return "Хэвлэх тохиргоо цонхийг нээх ба хэвлэх тохиргоог өөрчлөх боломжтой болно."; // Open the Print Options dialog, in which you can change printing options.
                case PreviewStringId.RibbonPreview_Print_Caption: return "Хэвлэх"; // Print
                case PreviewStringId.RibbonPreview_Print_STipTitle: return "Хэвлэх (Ctrl+P)"; // Print (Ctrl+P)
                case PreviewStringId.RibbonPreview_Print_STipContent: return "Тайлан хэвлэхийн өмнө принтер, хэвлэх хувь, хэвлэх хуудас зэргийг тохируулан хэвлэх боломжтой."; // Select a printer, number of copies and other printing options before printing.
                case PreviewStringId.RibbonPreview_PrintDirect_Caption: return "Шууд хэвлэх"; // Quick Print
                case PreviewStringId.RibbonPreview_PrintDirect_STipTitle: return "Шууд хэвлэх"; // Quick Print
                case PreviewStringId.RibbonPreview_PrintDirect_STipContent: return "Тайланг өгөгдсөн принтерээр, өгөгдсөн хэвлэх хувиар, бүх хуудсыг шууд хэвлэнэ."; // Send the document directly to the default printer without making changes.
                case PreviewStringId.RibbonPreview_PageSetup_Caption: return "Ирмэгийн зайг өөрийн хүссэнээр тохируулах..."; // Custom Margins...
                case PreviewStringId.RibbonPreview_PageSetup_STipTitle: return "Хуудасны тохиргоо"; // Page Setup
                case PreviewStringId.RibbonPreview_PageSetup_STipContent: return "Хуудасны тохиргооны цонхыг нээнэ."; // Show the Page Setup dialog.
                case PreviewStringId.RibbonPreview_EditPageHF_Caption: return "Толгой ба Хөл"; // Header/Footer
                case PreviewStringId.RibbonPreview_EditPageHF_STipTitle: return "Толгой ба Хөл"; // Header and Footer
                case PreviewStringId.RibbonPreview_EditPageHF_STipContent: return "Тайлангийн толгой ба хөлийг өөрчлөнө."; // Edit the header and footer of the document.
                case PreviewStringId.RibbonPreview_Magnifier_Caption: return "Томруулагч"; // Magnifier
                case PreviewStringId.RibbonPreview_Magnifier_STipTitle: return "Томруулагч"; // Magnifier
                case PreviewStringId.RibbonPreview_Magnifier_STipContent: return "Тайлангийн томруулалтыг өөрчлөнө. Хэвийн хэмжээ 100%."; // Invoke the Magnifier tool. Clicking once on a document zooms it so that a single page becomes entirely visible, while clicking another time zooms it to 100% of the normal size.
                case PreviewStringId.RibbonPreview_ZoomOut_Caption: return "Томруулалтыг хорогдуулах"; // Zoom Out
                case PreviewStringId.RibbonPreview_ZoomOut_STipTitle: return "Тайлангийн томруулалтыг багасгах"; // Zoom Out
                case PreviewStringId.RibbonPreview_ZoomOut_STipContent: return "Тайлангийн харагдах томруулалтын хэмжээг хорогдуулсанаар дэлгэцэнд тайлангийн хэд хэдэн хуудас зэрэгцэн харагдах боломжтой болно. Хэтэрхий багассанаар үсэг жижгэрч ялган харахад бэрх болно."; // Zoom out to see more of the page at a reduced size.
                case PreviewStringId.RibbonPreview_ZoomExact_Caption: return "Тохируулах:"; // Exact:
                case PreviewStringId.RibbonPreview_ZoomIn_Caption: return "Томруулалтыг нэмэгдүүлэх"; // Zoom In
                case PreviewStringId.RibbonPreview_ZoomIn_STipTitle: return "Томруулалтыг нэмэгдүүлэх"; // Zoom In
                case PreviewStringId.RibbonPreview_ZoomIn_STipContent: return "Тайлангийн харагдах томруулалтын хэмжээг нэмэгдүүлсэнээр тайлан дэлгэц дүүрэн харагдах буюу тайлангийн тодорхой нэг хэсэг нь томрон харагдах боломжтой болно."; // Zoom in to get a close-up view of the document.
                case PreviewStringId.RibbonPreview_ShowFirstPage_Caption: return "Эхний хуудас"; // First Page
                case PreviewStringId.RibbonPreview_ShowFirstPage_STipTitle: return "Эхний хуудас (Ctrl+Home)"; // First Page (Ctrl+Home)
                case PreviewStringId.RibbonPreview_ShowFirstPage_STipContent: return "Тайлангийн хамгийн эхний хуудсанд шилжин очно."; // Navigate to the first page of the document.
                case PreviewStringId.RibbonPreview_ShowPrevPage_Caption: return "Өмнөх хуудас"; // Previous Page
                case PreviewStringId.RibbonPreview_ShowPrevPage_STipTitle: return "Өмнөх хуудас (PageUp)"; // Previous Page (PageUp)
                case PreviewStringId.RibbonPreview_ShowPrevPage_STipContent: return "Одоогийн хуудасны өмнөх хуудсанд шилжин очно."; // Navigate to the previous page of the document.
                case PreviewStringId.RibbonPreview_ShowNextPage_Caption: return "Дараах хуудас"; // Next Page 
                case PreviewStringId.RibbonPreview_ShowNextPage_STipTitle: return "Дараах хуудас (PageDown)"; // Next Page (PageDown)
                case PreviewStringId.RibbonPreview_ShowNextPage_STipContent: return "Одоогийн хуудасны дараах хуудсанд шилжин очно."; // Navigate to the next page of the document.
                case PreviewStringId.RibbonPreview_ShowLastPage_Caption: return "Сүүлийн хуудас"; // Last Page 
                case PreviewStringId.RibbonPreview_ShowLastPage_STipTitle: return "Сүүлийн хуудас (Ctrl+End)"; // Last Page (Ctrl+End)
                case PreviewStringId.RibbonPreview_ShowLastPage_STipContent: return "Тайлангийн хамгийн сүүлийн хуудсанд шилжин очно."; // Navigate to the last page of the document.
                case PreviewStringId.RibbonPreview_MultiplePages_Caption: return "Олон хуудас"; // Many Pages
                case PreviewStringId.RibbonPreview_MultiplePages_STipTitle: return "Олон хуудас"; // View Many Pages
                case PreviewStringId.RibbonPreview_MultiplePages_STipContent: return "Тайланг сонгогдсон тооны хуудсаар нь зэрэгцүүлэн харуулна."; // Choose the page layout to arrange the document pages in preview.
                case PreviewStringId.RibbonPreview_FillBackground_Caption: return "Хуудасны өнгө"; // Page Color
                case PreviewStringId.RibbonPreview_FillBackground_STipTitle: return "Дэвсгэр өнгө"; // Background Color
                case PreviewStringId.RibbonPreview_FillBackground_STipContent: return "Тайлангийн хуудасны дэвсгэр өнгийг сонгоно."; // Choose a color for the background of the document pages.
                case PreviewStringId.RibbonPreview_Watermark_Caption: return "Тамга"; // Watermark
                case PreviewStringId.RibbonPreview_Watermark_STipTitle: return "Тамга"; // Watermark
                case PreviewStringId.RibbonPreview_Watermark_STipContent: return "Тайланг текстэн болон зурган тамгаар тамгалана."; // Insert ghosted text or image behind the content of a page. This is often used to indicate that a document is to be treated specially.
                case PreviewStringId.RibbonPreview_ExportFile_Caption: return "Файл экспортлох"; // Export To
                case PreviewStringId.RibbonPreview_ExportFile_STipTitle: return "Файл экспортлох..."; // Export To...
                case PreviewStringId.RibbonPreview_ExportFile_STipContent: return "Тайланг сонгогдсон өргөтгөлтэй файлруу экспортлоно."; // Export the current document in one of the available formats, and save it to the file on a disk.
                case PreviewStringId.RibbonPreview_SendFile_Caption: return "E-Mail-ээр файл илгээх"; // E-Mail As
                case PreviewStringId.RibbonPreview_SendFile_STipTitle: return "E-Mail-ээр файл илгээх..."; // E-Mail As...
                case PreviewStringId.RibbonPreview_SendFile_STipContent: return "Тайланг сонгогдсон өргөтгөлтэй файл болгон, E-Mail-ээр хавсарган илгээнэ."; // Export the current document in one of the available formats, and attach it to the e-mail.
                case PreviewStringId.RibbonPreview_ClosePreview_Caption: return "Цонх хаах"; // Close Print Preview
                case PreviewStringId.RibbonPreview_ClosePreview_STipTitle: return "Цонх хаах (Alt+F4)"; // Close Print Preview
                case PreviewStringId.RibbonPreview_ClosePreview_STipContent: return "Хэвлэхийн өмнө урьчилан харах цонхийг хаана."; // Close Print Preview of the document.
                case PreviewStringId.RibbonPreview_Scale_Caption: return "Масштаб"; // Scale
                case PreviewStringId.RibbonPreview_Scale_STipTitle: return "Масштаб"; // Scale
                case PreviewStringId.RibbonPreview_Scale_STipContent: return "Тайлангийн бодит хэмжээг хувиар болон хуудсаар машстаблан харна."; // Stretch or shrink the printed output to a percentage of its actual size.
                case PreviewStringId.RibbonPreview_PageOrientation_Caption: return "Чиглэл"; // Orientation
                case PreviewStringId.RibbonPreview_PageOrientation_STipTitle: return "Хуудасны чиглэл"; // Page Orientation
                case PreviewStringId.RibbonPreview_PageOrientation_STipContent: return "Хуудасны босоо эсвэл хэвтээ чиглэлийн хэлбэрийг сонгоно."; // Switch the pages between portrait and landscape layouts.
                case PreviewStringId.RibbonPreview_PaperSize_Caption: return "Хэмжээ"; // Size
                case PreviewStringId.RibbonPreview_PaperSize_STipTitle: return "Хуудасны хэмжээ"; // Page Size
                case PreviewStringId.RibbonPreview_PaperSize_STipContent: return "Тайлангийн хуудасны хэмжээг сонгоно."; // Choose the paper size of the document.
                case PreviewStringId.RibbonPreview_PageMargins_Caption: return "Ирмэгийн зай"; // Margins
                case PreviewStringId.RibbonPreview_PageMargins_STipTitle: return "Хуудасны ирмэгийн зай"; // Page Margins
                case PreviewStringId.RibbonPreview_PageMargins_STipContent: return "Хуудасны ирмэгийн зайг тохируулна. Тохируулахдаа өгөгдсөн хэмжээнүүдээр тохируулж болно эсвэл өөрийн хүссэн хэмжээгээр тохируулж болно."; // Select the margin sizes for the entire document. To apply specific margin sizes to the document, click Custom Margins.
                case PreviewStringId.RibbonPreview_Zoom_Caption: return "Томруулалт"; // Zoom
                case PreviewStringId.RibbonPreview_Zoom_STipTitle: return "Томруулалт"; // Zoom
                case PreviewStringId.RibbonPreview_Zoom_STipContent: return "Тайлангийн харагдах томруулалтын хэмжээг өөрчлөнө."; // Change the zoom level of the document preview.
                case PreviewStringId.RibbonPreview_Save_Caption: return "Хадгалах"; // Save
                case PreviewStringId.RibbonPreview_Save_STipTitle: return "Хадгалах (Ctrl + S)"; // Save (Ctrl + S)
                case PreviewStringId.RibbonPreview_Save_STipContent: return "Тайланг файл болгон хадгална."; // Save the document.
                case PreviewStringId.RibbonPreview_Open_Caption: return "Нээх"; // Open
                case PreviewStringId.RibbonPreview_Open_STipTitle: return "Нээх (Ctrl + O)"; // Open (Ctrl + O)
                case PreviewStringId.RibbonPreview_Open_STipContent: return "Өмнө нь файл болгон хадгалсан тайланг нээнэ."; // Open a document.

                case PreviewStringId.RibbonPreview_ExportPdf_Caption: return "PDF файл"; // PDF File
                case PreviewStringId.RibbonPreview_ExportPdf_Description: return "Adobe-ийн Portable(авсаархан, зөөврийн) хэвжүүлэлттэй файл"; // Adobe Portable Document Format
                case PreviewStringId.RibbonPreview_ExportPdf_STipTitle: return "PDF файлруу экспортлох"; // Export to PDF
                case PreviewStringId.RibbonPreview_ExportPdf_STipContent: return "Тайланг компьютерын hard дискэн дээр PDF файл болгон хадгална."; // Export the document to PDF and save it to the file on a disk.
                case PreviewStringId.RibbonPreview_ExportHtm_Caption: return "HTML файл"; // HTML File
                case PreviewStringId.RibbonPreview_ExportHtm_Description: return "Вэб хуудас"; // Web Page
                case PreviewStringId.RibbonPreview_ExportHtm_STipTitle: return "HTML файлруу экспортлох"; // Export to HTML
                case PreviewStringId.RibbonPreview_ExportHtm_STipContent: return "Тайланг компьютерын hard дискэн дээр HTML файл болгон хадгална."; // Export the document to HTML and save it to the file on a disk.
                case PreviewStringId.RibbonPreview_ExportMht_Caption: return "MHT файл"; // MHT File
                case PreviewStringId.RibbonPreview_ExportMht_Description: return "Нэг файлаас бүрдэх вэб хуудас"; // Single File Web Page
                case PreviewStringId.RibbonPreview_ExportMht_STipTitle: return "MHT файлруу экспортлох"; // Export to MHT
                case PreviewStringId.RibbonPreview_ExportMht_STipContent: return "Тайланг компьютерын hard дискэн дээр MHT файл болгон хадгална."; // Export the document to MHT and save it to the file on a disk.
                case PreviewStringId.RibbonPreview_ExportRtf_Caption: return "RTF файл"; // RTF File
                case PreviewStringId.RibbonPreview_ExportRtf_Description: return "Rich Text Format файл"; // Rich Text Format
                case PreviewStringId.RibbonPreview_ExportRtf_STipTitle: return "RTF файлруу экспортлох"; // Export to RTF
                case PreviewStringId.RibbonPreview_ExportRtf_STipContent: return "Тайланг компьютерын hard дискэн дээр RTF файл болгон хадгална."; // Export the document to RTF and save it to the file on a disk.
                case PreviewStringId.RibbonPreview_ExportXls_Caption: return "Excel файл"; // Excel File
                case PreviewStringId.RibbonPreview_ExportXls_Description: return "Microsoft Office Excel 2003 файл"; // Microsoft Excel Workbook
                case PreviewStringId.RibbonPreview_ExportXls_STipTitle: return "XLS файлруу экспортлох"; //Export to XLS
                case PreviewStringId.RibbonPreview_ExportXls_STipContent: return "Тайланг компьютерын hard дискэн дээр XLS файл болгон хадгална."; // Export the document to XLS and save it to the file on a disk.
                case PreviewStringId.RibbonPreview_ExportXlsx_Caption: return "Excel файл"; // Excel File
                case PreviewStringId.RibbonPreview_ExportXlsx_Description: return "Microsoft Office Excel 2007 файл"; // Microsoft Excel Workbook
                case PreviewStringId.RibbonPreview_ExportXlsx_STipTitle: return "XLSX файлруу экспортлох"; //Export to XLS
                case PreviewStringId.RibbonPreview_ExportXlsx_STipContent: return "Тайланг компьютерын hard дискэн дээр XLSX файл болгон хадгална."; // Export the document to XLS and save it to the file on a disk.
                case PreviewStringId.RibbonPreview_ExportCsv_Caption: return "CSV файл"; // CSV File
                case PreviewStringId.RibbonPreview_ExportCsv_Description: return "Comma-Separated Values файл"; // Comma-Separated Values Text
                case PreviewStringId.RibbonPreview_ExportCsv_STipTitle: return "CSV файлруу экспортлох"; // Export to CSV
                case PreviewStringId.RibbonPreview_ExportCsv_STipContent: return "Тайланг компьютерын hard дискэн дээр CSV файл болгон хадгална."; // Export the document to CSV and save it to the file on a disk.
                case PreviewStringId.RibbonPreview_ExportTxt_Caption: return "Text файл"; // Text File
                case PreviewStringId.RibbonPreview_ExportTxt_Description: return "Текстэн файл"; // Plain Text
                case PreviewStringId.RibbonPreview_ExportTxt_STipTitle: return "Text файлруу экспортлох"; // Export to Text
                case PreviewStringId.RibbonPreview_ExportTxt_STipContent: return "Тайланг компьютерын hard дискэн дээр Text файл болгон хадгална."; // Export the document to Text and save it to the file on a disk.
                case PreviewStringId.RibbonPreview_ExportGraphic_Caption: return "Зурган файл"; // Image File
                case PreviewStringId.RibbonPreview_ExportGraphic_Description: return "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF - өргөтгөлтэй зурган файл"; // BMP, GIF, JPEG, PNG, TIFF, EMF, WMF
                case PreviewStringId.RibbonPreview_ExportGraphic_STipTitle: return "Зурган файлруу экспортлох"; // Export to Image
                case PreviewStringId.RibbonPreview_ExportGraphic_STipContent: return "Тайланг компьютерын hard дискэн дээр Зурган файл болгон хадгална."; // Export the document to Image and save it to the file on a disk.

                case PreviewStringId.RibbonPreview_SendPdf_Caption: return "PDF файл"; // PDF File
                case PreviewStringId.RibbonPreview_SendPdf_Description: return "Adobe-ийн Portable(авсаархан, зөөврийн) хэвжүүлэлттэй файл"; // Adobe Portable Document Format
                case PreviewStringId.RibbonPreview_SendPdf_STipTitle: return "E-Mail-ээр PDF файл илгээх"; // E-Mail As PDF
                case PreviewStringId.RibbonPreview_SendPdf_STipContent: return "Тайланг PDF файл болгон, E-Mail-ээр хавсарган илгээнэ."; // Export the document to PDF and attach it to the e-mail.
                case PreviewStringId.RibbonPreview_SendMht_Caption: return "MHT файл"; // MHT File
                case PreviewStringId.RibbonPreview_SendMht_Description: return "Нэг файлаас бүрдэх вэб хуудас"; // Single File Web Page
                case PreviewStringId.RibbonPreview_SendMht_STipTitle: return "E-Mail-ээр MHT файл илгээх"; // E-Mail As MHT
                case PreviewStringId.RibbonPreview_SendMht_STipContent: return "Тайланг MHT файл болгон, E-Mail-ээр хавсарган илгээнэ."; // Export the document to MHT and attach it to the e-mail.
                case PreviewStringId.RibbonPreview_SendRtf_Caption: return "RTF файл"; // RTF File
                case PreviewStringId.RibbonPreview_SendRtf_Description: return "Rich Text Format файл"; // Rich Text Format
                case PreviewStringId.RibbonPreview_SendRtf_STipTitle: return "E-Mail-ээр RTF файл илгээх"; // E-Mail As RTF
                case PreviewStringId.RibbonPreview_SendRtf_STipContent: return "Тайланг RTF файл болгон, E-Mail-ээр хавсарган илгээнэ."; // Export the document to RTF and attach it to the e-mail.
                case PreviewStringId.RibbonPreview_SendXls_Caption: return "Excel файл"; // Excel File
                case PreviewStringId.RibbonPreview_SendXls_Description: return "Microsoft Office Excel 2003 файл"; // Microsoft Excel Workbook
                case PreviewStringId.RibbonPreview_SendXls_STipTitle: return "E-Mail-ээр XLS файл илгээх"; // E-Mail As XLS
                case PreviewStringId.RibbonPreview_SendXls_STipContent: return "Тайланг XLS файл болгон, E-Mail-ээр хавсарган илгээнэ."; // Export the document to XLS and attach it to the e-mail.
                case PreviewStringId.RibbonPreview_SendXlsx_Caption: return "Excel файл"; // Excel File
                case PreviewStringId.RibbonPreview_SendXlsx_Description: return "Microsoft Office Excel 2007 файл"; // Microsoft Excel Workbook
                case PreviewStringId.RibbonPreview_SendXlsx_STipTitle: return "E-Mail-ээр XLSX файл илгээх"; // E-Mail As XLS
                case PreviewStringId.RibbonPreview_SendXlsx_STipContent: return "Тайланг XLSX файл болгон, E-Mail-ээр хавсарган илгээнэ."; // Export the document to XLSX and attach it to the e-mail.
                case PreviewStringId.RibbonPreview_SendCsv_Caption: return "CSV файл"; // CSV File
                case PreviewStringId.RibbonPreview_SendCsv_Description: return "Comma-Separated Values файл"; // Comma-Separated Values Text
                case PreviewStringId.RibbonPreview_SendCsv_STipTitle: return "E-Mail-ээр CSV файл илгээх"; // E-Mail As CSV
                case PreviewStringId.RibbonPreview_SendCsv_STipContent: return "Тайланг CSV файл болгон, E-Mail-ээр хавсарган илгээнэ."; // Export the document to CSV and attach it to the e-mail.
                case PreviewStringId.RibbonPreview_SendTxt_Caption: return "Text файл"; // Text File
                case PreviewStringId.RibbonPreview_SendTxt_Description: return "Текстэн файл"; // Plain Text
                case PreviewStringId.RibbonPreview_SendTxt_STipTitle: return "E-Mail-ээр Text файл илгээх"; // E-Mail As Text
                case PreviewStringId.RibbonPreview_SendTxt_STipContent: return "Тайланг Text файл болгон, E-Mail-ээр хавсарган илгээнэ."; // Export the document to Text and attach it to the e-mail.
                case PreviewStringId.RibbonPreview_SendGraphic_Caption: return "Зурган файл"; // Image File
                case PreviewStringId.RibbonPreview_SendGraphic_Description: return "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF - өргөтгөлтэй зурган файл"; // BMP, GIF, JPEG, PNG, TIFF, EMF, WMF
                case PreviewStringId.RibbonPreview_SendGraphic_STipTitle: return "E-Mail-ээр Зурган файл илгээх"; // E-Mail As Image
                case PreviewStringId.RibbonPreview_SendGraphic_STipContent: return "Тайланг Зурган файл болгон, E-Mail-ээр хавсарган илгээнэ."; // Export the document to Image and attach it to the e-mail.


                case PreviewStringId.RibbonPreview_GalleryItem_PageOrientationPortrait_Caption: return "Босоо"; // Portrait
                case PreviewStringId.RibbonPreview_GalleryItem_PageOrientationPortrait_Description: return "Хуудсыг босоо болгох";
                case PreviewStringId.RibbonPreview_GalleryItem_PageOrientationLandscape_Caption: return "Хэвтээ"; // Landscape
                case PreviewStringId.RibbonPreview_GalleryItem_PageOrientationLandscape_Description: return "Хуудсыг хэвтээ болгох";
                case PreviewStringId.RibbonPreview_GalleryItem_PageMarginsNormal_Caption: return "Хэвийн"; // Normal
                case PreviewStringId.RibbonPreview_GalleryItem_PageMarginsNormal_Description: return "Хэвийн"; // Normal
                case PreviewStringId.RibbonPreview_GalleryItem_PageMarginsNarrow_Caption: return "Нарийн"; // Narrow
                case PreviewStringId.RibbonPreview_GalleryItem_PageMarginsNarrow_Description: return "Нарийн"; // Narrow
                case PreviewStringId.RibbonPreview_GalleryItem_PageMarginsModerate_Caption: return "Дунд зэргийн"; // Moderate
                case PreviewStringId.RibbonPreview_GalleryItem_PageMarginsModerate_Description: return "Дунд зэргийн"; // Moderate
                case PreviewStringId.RibbonPreview_GalleryItem_PageMarginsWide_Caption: return "Өргөн"; // Wide
                case PreviewStringId.RibbonPreview_GalleryItem_PageMarginsWide_Description: return "Өргөн"; // Wide

                case PreviewStringId.RibbonPreview_GalleryItem_PageMargins_Description: return "Дээд:\t{0}\tДоод:   \t{1}\nЗүүн: \t{2}\tБаруун:\t{3}"; // Top:{0} Bottom:{1} Left:{2} Right:{3}
                case PreviewStringId.RibbonPreview_GalleryItem_PaperSize_Description: return "{0} x {1}"; // {0} x {1}
                
                #endregion

                #region OpenFileDialog
                case PreviewStringId.OpenFileDialog_Filter: return "Тайлан файл (*.prnx)|*.prnx"; // Preview Document Files (*{0})|*{0}|All Files (*.*)|*.*
                case PreviewStringId.OpenFileDialog_Title: return "Нээх"; // Open
                #endregion

                default:
                    ret = "";
                    break;
            }
            return ret;
        }
    }
}
