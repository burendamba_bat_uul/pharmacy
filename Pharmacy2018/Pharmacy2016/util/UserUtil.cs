﻿using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using Pharmacy2016.gui.core.info.ds;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacy2016.util
{
    /*
     * Програмд нэвтэрч орсон хэрэглэгчийн мэдээллийг авах, тохируулах класс
     * 
     */

    public class UserUtil
    {

        #region Хэрэглэгчийн мэдээллийн утгууд

            private static string USER_ID;
            private static int USER_ROLE_ID;
            private static string USER_ROLE_NAME;
            private static string USER_WARD_ID;
            private static string USER_WARD_NAME;

            private static string USER_SIR_NAME;
            private static string USER_LAST_NAME;
            private static string USER_FIRST_NAME;
            private static string USER_FULL_NAME;
            private static DateTime USER_BIRTHDAY;
            private static bool USER_GENDER;
            private static bool USER_IS_MARRIED;
            private static string USER_REGISTER;
            private static string USER_EMD_NUMBER;
            private static string USER_NDD_NUMBER;
            private static string USER_PHONE;
            private static string USER_EMAIL;
            private static string USER_ADDRESS;

            private static int USER_EDUCATION_ID;
            private static string USER_EDUCATION_NAME;
            private static string USER_ORGANIZATION_ID;
            private static string USER_ORGANIZATION_NAME;
            private static string USER_POSITION_ID;
            private static string USER_POSITION_NAME;
            private static int USER_PROVINCE_ID;
            private static string USER_PROVINCE_NAME;

            private static string USER_SKIN_NAME;
            private static int USER_SKIN_MASK_COLOR;
            private static int USER_SKIN_MASK_COLOR_2;

            private static Dictionary<int, bool[]> USER_ROLE = new Dictionary<int, bool[]>();
            private static Dictionary<string, string> USER_LAYOUT = new Dictionary<string, string>();

            private static List<UserWard> USER_WARD = new List<UserWard>();
            private static List<UserWard> USER_WAREHOUSE = new List<UserWard>();

            private class UserWard
            {
                private string userID = null;
                private string wardID = null;

                public UserWard(string userID, string wardID)
                {
                    this.userID = userID;
                    this.wardID = wardID;
                }

                public string getUserID()
                {
                    return userID;
                }

                public string getWardID()
                {
                    return wardID;
                }

                public void setWardID(string wardID)
                {
                    this.wardID = wardID;
                }
            }

        #endregion
        
        #region Хэрэглэгчийн мэдээллийг тохируулах функц    

            public static void setUser(string id, DataRow row)
            {
                USER_ID = id;
                USER_ROLE_ID = Convert.ToInt32(row["roleId"]);
                USER_ROLE_NAME = row["roleName"].ToString();
                USER_WARD_ID = row["wardID"].ToString();
                USER_WARD_NAME = row["wardName"].ToString();

                USER_SIR_NAME = row["sirName"].ToString();
                USER_LAST_NAME = row["lastName"].ToString();
                USER_FIRST_NAME = row["firstName"].ToString();
                USER_BIRTHDAY = Convert.ToDateTime(row["birthday"]);
                USER_GENDER = Convert.ToBoolean(row["gender"]);
                USER_IS_MARRIED = Convert.ToBoolean(row["isMarried"]);
                USER_REGISTER = row["register"].ToString();
                USER_EMD_NUMBER = row["emdNumber"].ToString();
                USER_NDD_NUMBER = row["nddNumber"].ToString();
                USER_PHONE = row["phone"].ToString();
                USER_EMAIL = row["email"].ToString();
                USER_ADDRESS = row["address"].ToString();
                USER_FULL_NAME = row["lastName"].ToString() + " " + row["firstName"].ToString();

                if (row["education"] != null && row["education"] != DBNull.Value)
                {
                    USER_EDUCATION_ID = Convert.ToInt32(row["education"]);
                    USER_EDUCATION_NAME = InformationDataSet.getEducationName(USER_EDUCATION_ID);
                }
                if (row["organizationID"] != null && row["organizationID"] != DBNull.Value)
                {
                    USER_ORGANIZATION_ID = row["organizationID"].ToString();
                    USER_ORGANIZATION_NAME = row["organizationName"].ToString();
                }
                if (row["positionID"] != null && row["positionID"] != DBNull.Value)
                {
                    USER_POSITION_ID = row["positionID"].ToString();
                    USER_POSITION_NAME = row["positionName"].ToString();
                }
                if (row["provinceID"] != null && row["provinceID"] != DBNull.Value)
                {
                    USER_PROVINCE_ID = Convert.ToInt32(row["provinceID"]);
                    USER_PROVINCE_NAME = row["provinceName"].ToString();
                }   
            }

            public static void setUserRole(DataRowCollection rows)
            {
                foreach (DataRow row in rows)
                {
                    USER_ROLE.Add(Convert.ToInt32(row["windowID"]), new bool[] { Convert.ToBoolean(row["show"]), Convert.ToBoolean(row["append"]), Convert.ToBoolean(row["edit"]), Convert.ToBoolean(row["remove"]), Convert.ToBoolean(row["printing"]) });
                    RoleUtil.setMenu(Convert.ToInt32(row["windowID"]), Convert.ToBoolean(row["show"]));
                }

                // Ерөнхий тохиргоон дээр засвар оруулах эрхтэй нягтангийн эрхийг тохируулж байна.
                if (getUserRoleId() == RoleUtil.ACCOUNTANT_ID)
                {
                    bool isAccountUser = (RoleUtil.getAccountUserID() != DBNull.Value && RoleUtil.getAccountUserID() != null && UserUtil.getUserId().Equals(RoleUtil.getAccountUserID().ToString()));
                    RoleUtil.setMenu(3008, isAccountUser);
                }
            }

            public static void setUserSkinColor(DataRowCollection rows)
            {
                if (rows.Count > 0)
                {
                    USER_SKIN_NAME = rows[0]["skinName"].ToString();
                    USER_SKIN_MASK_COLOR = Convert.ToInt32(rows[0]["skinMaskColor"]);
                    USER_SKIN_MASK_COLOR_2 = Convert.ToInt32(rows[0]["skinMaskColor2"]);
                }
                else
                {
                    USER_SKIN_NAME = "";
                    USER_SKIN_MASK_COLOR = 0;
                    USER_SKIN_MASK_COLOR_2 = 0;
                }

                if (!USER_SKIN_NAME.Equals(""))
                    UserLookAndFeel.Default.SkinName = USER_SKIN_NAME;

                if (USER_SKIN_MASK_COLOR != 0)
                    UserLookAndFeel.Default.SkinMaskColor = System.Drawing.Color.FromArgb(USER_SKIN_MASK_COLOR);

                if (USER_SKIN_MASK_COLOR_2 != 0)
                    UserLookAndFeel.Default.SkinMaskColor2 = System.Drawing.Color.FromArgb(USER_SKIN_MASK_COLOR_2);
            }

            public static void setUserLayout(DataRowCollection rows)
            {
                foreach (DataRow row in rows)
                {
                    USER_LAYOUT.Add(row["layoutName"].ToString(), row["layout"].ToString());
                }
            }

            public static void setUserWard(DataRowCollection ward, DataRowCollection warehouse)
            {
                foreach (DataRow row in ward)
                {
                    USER_WARD.Add(new UserWard(row["userID"].ToString(), row["wardID"].ToString()));
                }

                foreach (DataRow row in warehouse)
                {
                    USER_WAREHOUSE.Add(new UserWard(row["userID"].ToString(), row["warehouseID"].ToString()));
                }
            }

            // Шинээр нэмсэн устгасан тасгийг авч байна
            public static void addUserWard(DataRow userWard)
            {
                USER_WARD.Add(new UserWard(userWard["userID"].ToString(), userWard["wardID"].ToString()));
            }   

            public static void removeUserWard(DataRowView userWard)
            {
                USER_WARD.RemoveAll(r => r.getUserID() == userWard["userID"].ToString() && r.getWardID() == userWard["wardID"].ToString());
            }

            // Шинээр нэмсэн хэрэглэгчийн тасгийг авч байна
            public static void addEachUserWard(DataRowView userWard, object userID)
            {
                USER_WARD.Add(new UserWard(userID.ToString(), userWard["id"].ToString()));
            }

            // Шинээр нэмсэн устгасан агуулахыг авч байна
            public static void addUserWareHouse(DataRow userWare)
            {
                USER_WAREHOUSE.Add(new UserWard(userWare["userID"].ToString(), userWare["warehouseID"].ToString()));
            }

            public static void removeUserWareHouse(DataRowView userWare)
            {
                USER_WAREHOUSE.RemoveAll(r => r.getUserID() == userWare["userID"].ToString() && r.getWardID() == userWare["warehouseID"].ToString());
            }

            public static void saveUserSkinColor()
            {
                if(!USER_SKIN_NAME.Equals(UserLookAndFeel.Default.SkinName) || USER_SKIN_MASK_COLOR != UserLookAndFeel.Default.SkinMaskColor.ToArgb() || USER_SKIN_MASK_COLOR_2 != UserLookAndFeel.Default.SkinMaskColor2.ToArgb())
                {
                    if (UserDataSet.Instance.UserSkinColor.Rows.Count > 0)
                    {
                        UserDataSet.Instance.UserSkinColor.Rows[0]["skinName"] = UserLookAndFeel.Default.SkinName;
                        UserDataSet.Instance.UserSkinColor.Rows[0]["skinMaskColor"] = UserLookAndFeel.Default.SkinMaskColor.ToArgb();
                        UserDataSet.Instance.UserSkinColor.Rows[0]["skinMaskColor2"] = UserLookAndFeel.Default.SkinMaskColor2.ToArgb();
                    }
                    else
                    {
                        UserDataSet.Instance.UserSkinColor.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                        UserDataSet.Instance.UserSkinColor.userIDColumn.DefaultValue = USER_ID;
                        DataRow row = UserDataSet.Instance.UserSkinColor.NewRow();
                        row["skinName"] = UserLookAndFeel.Default.SkinName;
                        row["skinMaskColor"] = UserLookAndFeel.Default.SkinMaskColor.ToArgb();
                        row["skinMaskColor2"] = UserLookAndFeel.Default.SkinMaskColor2.ToArgb();
                        row.EndEdit();
                        UserDataSet.Instance.UserSkinColor.Rows.Add(row);
                    }
                    UserDataSet.UserSkinColorTableAdapter.Update(UserDataSet.Instance.UserSkinColor);
                }
            }

            public static void saveUserLayout(string name, string layout)
            {
                if (!USER_LAYOUT.ContainsKey(name))
                {
                    UserDataSet.Instance.UserLayout.hospitalIDColumn.DefaultValue = HospitalUtil.getHospitalId();
                    UserDataSet.Instance.UserLayout.userIDColumn.DefaultValue = USER_ID;
                    UserDataSet.Instance.UserLayout.layoutNameColumn.DefaultValue = name;
                    DataRow row = UserDataSet.Instance.UserLayout.NewRow();
                    row["layout"] = layout;
                    row.EndEdit();
                    UserDataSet.Instance.UserLayout.Rows.Add(row);
                    USER_LAYOUT.Add(name, layout);
                }
                else
                {
                    USER_LAYOUT[name] = layout;
                    DataRow[] row = UserDataSet.Instance.UserLayout.Select("hospitalID = '" + HospitalUtil.getHospitalId() + "' AND userID = '" + USER_ID + "' AND layoutName = '" + name + "'");
                    if (row.Length > 0)
                        row[0]["layout"] = layout;
                }
                UserDataSet.UserLayoutTableAdapter.Update(UserDataSet.Instance.UserLayout);
            }

            public static void clearUser()
            {
                saveUserSkinColor();

                USER_ID = "";
                USER_ROLE_ID = 0;
                USER_ROLE_NAME = "";
                USER_WARD_ID = "";
                USER_WARD_NAME = "";

                USER_SIR_NAME = "";
                USER_LAST_NAME = "";
                USER_FIRST_NAME = "";
                USER_BIRTHDAY = new DateTime();
                USER_GENDER = false;
                USER_IS_MARRIED = false;
                USER_REGISTER = "";
                USER_EMD_NUMBER = "";
                USER_NDD_NUMBER = "";
                USER_PHONE = "";
                USER_EMAIL = "";
                USER_ADDRESS = "";
                USER_FULL_NAME = "";

                USER_EDUCATION_ID =  0;
                USER_EDUCATION_NAME = "";
                USER_ORGANIZATION_ID = "";
                USER_ORGANIZATION_NAME = "";
                USER_POSITION_ID = "";
                USER_POSITION_NAME = "";
                USER_PROVINCE_ID = 0;
                USER_PROVINCE_NAME = "";

                USER_SKIN_NAME = "";
                USER_SKIN_MASK_COLOR = 0;
                USER_SKIN_MASK_COLOR_2 = 0;

                USER_ROLE.Clear();
                USER_LAYOUT.Clear();

                USER_WARD.Clear();
                USER_WAREHOUSE.Clear();
                //USER_ROLE = new Dictionary<int, bool[]>();
                //USER_LAYOUT = new Dictionary<string, string>();

                InformationDataSet.Instance.LoginUserInfo.Clear();
                InformationDataSet.Instance.UserWindowRole.Clear();
                UserDataSet.Instance.UserSkinColor.Clear();
                UserDataSet.Instance.UserLayout.Clear();
            }

        #endregion

        #region Хэрэглэгчийн мэдээллийг авах функц

            public static string getUserId()
            {
                return USER_ID;
            }

            public static int getUserRoleId()
            {
                return USER_ROLE_ID;
            }

            public static string getUserRoleName()
            {
                return USER_ROLE_NAME;
            }

            public static string getUserWardID()
            {
                return USER_WARD_ID;
            }

            public static string getUserWardName()
            {
                return USER_WARD_NAME;
            }


            public static string getUserSirName()
            {
                return USER_SIR_NAME;
            }

            public static string getUserLastName()
            {
                return USER_LAST_NAME;
            }

            public static string getUserFirstName()
            {
                return USER_FIRST_NAME;
            }

            public static DateTime getUserBirthday()
            {
                return USER_BIRTHDAY;
            }

            public static bool getUserGender()
            {
                return USER_GENDER;
            }

            public static bool getUserIsMarried()
            {
                return USER_IS_MARRIED;
            }

            public static string getUserRegister()
            {
                return USER_REGISTER;
            }

            public static string getUserEmdNumber()
            {
                return USER_EMD_NUMBER;
            }

            public static string getUserNddNumber()
            {
                return USER_NDD_NUMBER;
            }

            public static string getUserphone()
            {
                return USER_PHONE;
            }

            public static string getUserEmail()
            {
                return USER_EMAIL;
            }

            public static string getUserAddress()
            {
                return USER_ADDRESS;
            }

            public static string getUserFullName()
            {
                return USER_FULL_NAME;
            }


            public static int getUserEducationID()
            {
                return USER_EDUCATION_ID;
            }

            public static string getUserEducationName()
            {
                return USER_EDUCATION_NAME;
            }

            public static string getUserOrganizationID()
            {
                return USER_ORGANIZATION_ID;
            }

            public static string getUserOrganizationName()
            {
                return USER_ORGANIZATION_NAME;
            }

            public static string getUserPositionID()
            {
                return USER_POSITION_ID;
            }

            public static string getUserPositionName()
            {
                return USER_POSITION_NAME;
            }

            public static int getUserProvinceID()
            {
                return USER_PROVINCE_ID;
            }

            public static string getUserProvinceName()
            {
                return USER_PROVINCE_NAME;
            }

            public static bool[] getWindowRole(int id)
            {
                if (USER_ROLE.ContainsKey(id))
                    return USER_ROLE[id];

                return null;
            }

            public static string getLayout(string id)
            {
                if (USER_LAYOUT.ContainsKey(id))
                    return USER_LAYOUT[id];

                return null;
            }

            public static string getUserWard(string id)
            {
                string str = "";
                for(int i = 0; i < USER_WARD.Count; i++){
                    UserWard ward = USER_WARD[i];
                    if (ward.getUserID().Equals(id))
                        str += "id = '" + ward.getWardID() + "' OR ";
                }
                if (!str.Equals(""))
                    str = str.Substring(0, str.Length - 4);
                else
                    str = "id = '-1'";

                return str;
            }

            public static string getUserWarehouse(string id)
            {
                string str = "";
                for (int i = 0; i < USER_WAREHOUSE.Count; i++)
                {
                    UserWard ward = USER_WAREHOUSE[i];
                    if (ward.getUserID().Equals(id))
                        str += "id = '" + ward.getWardID() + "' OR ";
                }
                if (!str.Equals(""))
                    str = str.Substring(0, str.Length - 4);
                else
                    str = "id = '-1'";

                return str;
            }

        #endregion

        #region Хэрэглэгчийн нууц үгийг хувиргах функц

            public static string convertMD5Hash(string pass)
            {
                MD5 md5 = new MD5CryptoServiceProvider();

                md5.ComputeHash(UnicodeEncoding.UTF8.GetBytes(pass));

                byte[] result = md5.Hash;

                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < result.Length; i++)
                {
                    strBuilder.Append(result[i].ToString("x2"));
                }

                return strBuilder.ToString();
            }

        #endregion

    }
}
