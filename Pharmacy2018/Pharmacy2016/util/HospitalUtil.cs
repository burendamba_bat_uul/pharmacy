﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pharmacy2016.util
{
    /*
     * Эмнэлэгийн мэдээллийг авах, тохируулах класс
     * 
     */

    public class HospitalUtil
    {
        
        #region Эмнэлэгийн мэдээллийн талбарууд

            private static string HOSPITAL_ID = "";
            private static string HOSPITAL_PID = "";
            private static string HOSPITAL_NAME = "";
            private static string HOSPITAL_DIRECTOR_ID = "";
            private static string HOSPITAL_DIRECTOR_FIRST_NAME = "";
            private static string HOSPITAL_DIRECTOR_LAST_NAME = "";
            private static string HOSPITAL_DIRECTOR_FULL_NAME = "";
            private static string HOSPITAL_DIRECTOR_SHORT_NAME = "";
            private static string HOSPITAL_SUB_DIRECTOR_ID = "";
            private static string HOSPITAL_SUB_DIRECTOR_FIRST_NAME = "";
            private static string HOSPITAL_SUB_DIRECTOR_LAST_NAME = "";
            private static string HOSPITAL_SUB_DIRECTOR_FULL_NAME = "";
            private static string HOSPITAL_SUB_DIRECTOR_SHORT_NAME = "";
            private static string HOSPITAL_TREATMENT_DIRECTOR_ID = "";
            private static string HOSPITAL_TREATMENT_DIRECTOR_FIRST_NAME = "";
            private static string HOSPITAL_TREATMENT_DIRECTOR_LAST_NAME = "";
            private static string HOSPITAL_TREATMENT_DIRECTOR_FULL_NAME = "";
            private static string HOSPITAL_TREATMENT_DIRECTOR_SHORT_NAME = "";
            private static string HOSPITAL_DESCRIPTION = "";
            private static string HOSPITAL_ADDRESS = "";
            private static string HOSPITAL_PHONE = "";
            private static string HOSPITAL_POSTBOX = "";
            private static string HOSPITAL_FAX = "";
            private static string HOSPITAL_WEBSITE = "";
            private static string HOSPITAL_FACEBOOK = "";
            private static string HOSPITAL_TWITTER = "";
            private static string HOSPITAL_EMAIL = "";
            private static Image HOSPITAL_PICTURE = null;
            private static string HOSPITAL_REGISTER_ID = "";
            private static string HOSPITAL_BANK_NAME = "";
            private static string HOSPITAL_BANK_ACCOUNT = "";
            private static string HOSPITAL_ACCOUNTANT_NAME = "";

        #endregion

        #region Эмнэлэгийн мэдээллийг тохируулах функц

            public static void setHospital(DataRow row)
            {
                HOSPITAL_ID = row["id"].ToString();
                HOSPITAL_PID = row["hospitalID"].ToString();
                HOSPITAL_NAME = row["name"].ToString();
                HOSPITAL_DIRECTOR_ID = row["directorID"].ToString();
                HOSPITAL_DIRECTOR_LAST_NAME = row["directorLastName"].ToString();
                HOSPITAL_DIRECTOR_FIRST_NAME = row["directorFirstName"].ToString();
                HOSPITAL_DIRECTOR_FULL_NAME = HOSPITAL_DIRECTOR_LAST_NAME + " " + HOSPITAL_DIRECTOR_FIRST_NAME;
                if (HOSPITAL_DIRECTOR_LAST_NAME.Length > 1)
                    HOSPITAL_DIRECTOR_SHORT_NAME = HOSPITAL_DIRECTOR_LAST_NAME.Substring(0, 1) + "." + HOSPITAL_DIRECTOR_FIRST_NAME;
                else
                    HOSPITAL_DIRECTOR_SHORT_NAME = HOSPITAL_DIRECTOR_FIRST_NAME;
                if (row["subDirectorID"] != DBNull.Value && row["subDirectorID"] != null)
                {
                    HOSPITAL_SUB_DIRECTOR_ID = row["subDirectorID"].ToString();
                    HOSPITAL_SUB_DIRECTOR_LAST_NAME = row["subDirectorLastName"].ToString();
                    HOSPITAL_SUB_DIRECTOR_FIRST_NAME = row["subDirectorFirstName"].ToString();
                    HOSPITAL_SUB_DIRECTOR_FULL_NAME = HOSPITAL_SUB_DIRECTOR_LAST_NAME + " " + HOSPITAL_SUB_DIRECTOR_FIRST_NAME;
                    if (HOSPITAL_SUB_DIRECTOR_LAST_NAME.Length > 1)
                        HOSPITAL_SUB_DIRECTOR_SHORT_NAME = HOSPITAL_SUB_DIRECTOR_LAST_NAME.Substring(0, 1) + "." + HOSPITAL_SUB_DIRECTOR_FIRST_NAME;
                    else
                        HOSPITAL_SUB_DIRECTOR_SHORT_NAME = HOSPITAL_SUB_DIRECTOR_FIRST_NAME;
                }
                if (row["treatmentDirectorID"] != DBNull.Value && row["treatmentDirectorID"] != null)
                {
                    HOSPITAL_TREATMENT_DIRECTOR_ID = row["treatmentDirectorID"].ToString();
                    HOSPITAL_TREATMENT_DIRECTOR_LAST_NAME = row["treatmentDirectorLastName"].ToString();
                    HOSPITAL_TREATMENT_DIRECTOR_FIRST_NAME = row["treatmentDirectorFirstName"].ToString();
                    HOSPITAL_TREATMENT_DIRECTOR_FULL_NAME = HOSPITAL_TREATMENT_DIRECTOR_LAST_NAME + " " + HOSPITAL_TREATMENT_DIRECTOR_FIRST_NAME;
                    if (HOSPITAL_TREATMENT_DIRECTOR_LAST_NAME.Length > 1)
                        HOSPITAL_TREATMENT_DIRECTOR_SHORT_NAME = HOSPITAL_TREATMENT_DIRECTOR_LAST_NAME.Substring(0, 1) + "." + HOSPITAL_TREATMENT_DIRECTOR_FIRST_NAME;
                    else
                        HOSPITAL_TREATMENT_DIRECTOR_SHORT_NAME = HOSPITAL_TREATMENT_DIRECTOR_FIRST_NAME;
                }
                HOSPITAL_DESCRIPTION = row["description"].ToString();
                HOSPITAL_ADDRESS = row["address"].ToString();
                HOSPITAL_PHONE = row["phone"].ToString();
                HOSPITAL_POSTBOX = row["postBox"].ToString();
                HOSPITAL_FAX = row["fax"].ToString();
                HOSPITAL_WEBSITE = row["website"].ToString();
                HOSPITAL_FACEBOOK = row["facebook"].ToString();
                HOSPITAL_TWITTER = row["twitter"].ToString();
                HOSPITAL_EMAIL = row["email"].ToString();
                HOSPITAL_REGISTER_ID = row["register_id"].ToString();
                HOSPITAL_BANK_NAME = row["bank_name"].ToString();
                HOSPITAL_BANK_ACCOUNT = row["bank_account"].ToString();
                HOSPITAL_ACCOUNTANT_NAME = row["accountant_name"].ToString();
                // byte[] data = (byte[])HospitalUtil.getHospitalPicture();
                if (row["picture"] != null && row["picture"] != DBNull.Value)
                {
                    System.IO.MemoryStream stream = new System.IO.MemoryStream((byte[])row["picture"]);
                    HOSPITAL_PICTURE = Image.FromStream(stream);
                }
            }

            public static void clearHospital()
            {
                HOSPITAL_ID = "";
                HOSPITAL_PID = "";
                HOSPITAL_NAME = "";
                HOSPITAL_DIRECTOR_ID = "";
                HOSPITAL_DIRECTOR_LAST_NAME = "";
                HOSPITAL_DIRECTOR_FIRST_NAME = "";
                HOSPITAL_DIRECTOR_FULL_NAME = "";
                HOSPITAL_DIRECTOR_SHORT_NAME = "";
                HOSPITAL_SUB_DIRECTOR_ID = "";
                HOSPITAL_SUB_DIRECTOR_LAST_NAME = "";
                HOSPITAL_SUB_DIRECTOR_FIRST_NAME = "";
                HOSPITAL_SUB_DIRECTOR_FULL_NAME = "";
                HOSPITAL_SUB_DIRECTOR_SHORT_NAME = "";
                HOSPITAL_TREATMENT_DIRECTOR_ID = "";
                HOSPITAL_TREATMENT_DIRECTOR_LAST_NAME = "";
                HOSPITAL_TREATMENT_DIRECTOR_FIRST_NAME = "";
                HOSPITAL_TREATMENT_DIRECTOR_FULL_NAME = "";
                HOSPITAL_TREATMENT_DIRECTOR_SHORT_NAME = "";
                HOSPITAL_DESCRIPTION = "";
                HOSPITAL_ADDRESS = "";
                HOSPITAL_PHONE = "";
                HOSPITAL_POSTBOX = "";
                HOSPITAL_FAX = "";
                HOSPITAL_WEBSITE = "";
                HOSPITAL_FACEBOOK = "";
                HOSPITAL_TWITTER = "";
                HOSPITAL_EMAIL = "";
                HOSPITAL_PICTURE = null;
                HOSPITAL_REGISTER_ID = "";
                HOSPITAL_BANK_NAME = "";
                HOSPITAL_BANK_ACCOUNT = "";
                HOSPITAL_ACCOUNTANT_NAME = "";
            }

        #endregion

        #region Эмнэлэгийн мэдээллийг авах функц

            public static string getHospitalId()
            {
                return HOSPITAL_ID;
            }

            public static string getHospitalPid()
            {
                return HOSPITAL_PID;
            }

            public static string getHospitalName()
            {
                return HOSPITAL_NAME;
            }

            public static string getHospitalDirectorID()
            {
                return HOSPITAL_DIRECTOR_ID;
            }

            public static string getHospitalDirectorFullName()
            {
                return HOSPITAL_DIRECTOR_FULL_NAME;
            }

            public static string getHospitalDirectorShortName()
            {
                return HOSPITAL_DIRECTOR_SHORT_NAME;
            }

            public static string getHospitalSubDirectorID()
            {
                return HOSPITAL_SUB_DIRECTOR_ID;
            }

            public static string getHospitalSubDirectorFullName()
            {
                return HOSPITAL_SUB_DIRECTOR_FULL_NAME;
            }

            public static string getHospitalSubDirectorShortName()
            {
                return HOSPITAL_SUB_DIRECTOR_SHORT_NAME;
            }


            public static string getHospitalTreatmentDirectorID()
            {
                return HOSPITAL_TREATMENT_DIRECTOR_ID;
            }

            public static string getHospitalTreatmentDirectorFullName()
            {
                return HOSPITAL_TREATMENT_DIRECTOR_FULL_NAME;
            }

            public static string getHospitalTreatmentDirectorShortName()
            {
                return HOSPITAL_TREATMENT_DIRECTOR_SHORT_NAME;
            }

            public static string getHospitalDescription()
            {
                return HOSPITAL_DESCRIPTION;
            }

            public static string getHospitalAddress()
            {
                return HOSPITAL_ADDRESS;
            }

            public static string getHospitalPhone()
            {
                return HOSPITAL_PHONE;
            }

            public static string getHospitalPostBox()
            {
                return HOSPITAL_POSTBOX;
            }

            public static string getHospitalFax()
            {
                return HOSPITAL_FAX;
            }

            public static string getHospitalWebsite()
            {
                return HOSPITAL_WEBSITE;
            }

            public static string getHospitalFacebook()
            {
                return HOSPITAL_FACEBOOK;
            }

            public static string getHospitalTwitter()
            {
                return HOSPITAL_TWITTER;
            }

            public static string getHospitalEmail()
            {
                return HOSPITAL_EMAIL;
            }

            public static Image getHospitalPicture()
            {
                return HOSPITAL_PICTURE;
            }

            public static string getHospitalRegister()
            {
                return HOSPITAL_REGISTER_ID;
            }

            public static string getHospitalBank()
            {
                return HOSPITAL_BANK_NAME;
            }

            public static string getHospitalAccount()
            {
                return HOSPITAL_BANK_ACCOUNT;
            }

            public static string getHospitalAccountantName()
            {
                return HOSPITAL_ACCOUNTANT_NAME;
            }

        #endregion

    }
}
