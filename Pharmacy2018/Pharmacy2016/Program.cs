﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Pharmacy2016.gui.core.main;
using Pharmacy2016.util;
using System.Threading;

namespace Pharmacy2016
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        private static string appGuid = "c0a76b5a-12ab-45c5-b9d9-d693faa6e7b9";

        [STAThread]
        static void Main()
        {
            try
            {
                using (Mutex mutex = new Mutex(false, "Global\\" + appGuid))
                {
                    if (!mutex.WaitOne(0, false))
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Програм аль хэдийн ачааллаж байна!");
                        return;
                    } 
                SystemUtil.loadProgram();
                Application.Run(MainForm.Instance);
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Програм ачааллахад алдаа гарлаа \n" + ex.ToString());
            }
        }
    }
}
